"""
Computes BLEU metrics between two corpora - real and generated texts.
"""
import argparse
from collections import defaultdict

import numpy as np
import tensorflow as tf

from common.utils import load_all_articles


def compute_for_n_grams(real_seqs, gen_seqs, n=1):
    print('Computing %d-BLEU' % n)
    most_occurrences = defaultdict(int)
    for real in real_seqs:
        occ = defaultdict(int)
        for w in zip(*[real[i:] for i in range(n)]):
            occ[w] += 1
        for w in occ.keys():
            most_occurrences[w] = max(most_occurrences[w], occ[w])

    bleus = []
    for gen in gen_seqs:
        numerator = 0.
        denominator = 0.
        occ = defaultdict(int)
        for w in zip(*[gen[i:] for i in range(n)]):
            occ[w] += 1
        for w in occ.keys():
            numerator += min(occ[w], most_occurrences[w])
            denominator += occ[w]
        if denominator == 0.:
            continue
        cur_bleu = numerator / denominator
        bleus.append(cur_bleu)
    print(np.mean(bleus))
    del most_occurrences


def main():
    print('Loading real articles')
    real_articles = load_all_articles(real_dir)
    print('Loading generated articles')
    gen_articles = load_all_articles(gen_dir)
    tokenizer = tf.keras.preprocessing.text.Tokenizer()
    print('Fitting tokenizer')
    tokenizer.fit_on_texts(real_articles + gen_articles)

    real_seqs = tokenizer.texts_to_sequences(real_articles)
    gen_seqs = tokenizer.texts_to_sequences(gen_articles)
    for n in range(1, 6):
        compute_for_n_grams(real_seqs, gen_seqs, n)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='BLEU metrics')
    parser.add_argument('--real', action='store', type=str, required=True)
    parser.add_argument('--gen', action='store', type=str, required=True)
    args = parser.parse_args()
    real_dir = args.real
    gen_dir = args.gen

    main()
