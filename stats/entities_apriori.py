import pickle

from efficient_apriori import apriori

from common.utils import load_all_articles
from ner.utils import extract_entity_sets, cache_entity_sets


def load_itemsets_from_disk():
    with open('stats/cache/apriori_itemsets.pickle', 'rb') as f:
        return pickle.load(f)


def print_most_common_of_size(itemsets, size, n):
    most_common = sorted(itemsets[size].items(), key=lambda a: a[1],
                         reverse=True)
    for s, c in most_common[:n]:
        names = list(map(lambda a: a[0], s))
        print(','.join(names), c)


if __name__ == '__main__':
    articles = load_all_articles('../data/bbcsport')
    entities = extract_entity_sets(articles, 'en')

    # Cache entities.
    cache_entity_sets(entities, 'stats/cache/entity_sets.pickle')

    itemsets, rules = apriori(entities, min_support=0.01, min_confidence=0.2)
    print(itemsets)
    with open('stats/cache/apriori_itemsets.pickle', 'wb') as fp:
        pickle.dump(itemsets, fp)
