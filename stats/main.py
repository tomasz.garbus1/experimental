import tensorflow as tf
import argparse
from common.data_helper import DataHelper, FILTERS_DEFAULT
import matplotlib.pyplot as plt
import numpy as np


def plot_sequence_lengths(dhelper: DataHelper):
    lengths = list(map(lambda art: len(dhelper.article_to_seq(art)),
                       dhelper.articles))
    mean = float(np.mean(lengths))
    std = float(np.std(lengths))
    median = float(np.median(lengths))
    plt.hist(lengths, range=(0, 1000), bins=1000)
    plt.title("Sequence lengths distribution")
    plt.suptitle("Median: %f, Mean: %f, Std.dev.: %f" % (median, mean, std))
    plt.show()


def plot_word_counts(dhelper: DataHelper):
    wc = list(dhelper.tokenizer.word_counts.values())
    wc.sort(reverse=True)
    sums = [0]
    for e in wc:
        sums.append(sums[-1] + e)
    plt.plot(list(range(len(wc)+1)), sums)
    plt.title("Word counts")
    plt.show()


def main():
    parser = argparse.ArgumentParser(prog='stats experiment script')
    parser.add_argument('-i', action='store', type=str, required=True,
                        help='input directory')
    args = parser.parse_args()
    input_dir = args.i

    tokenizer = tf.keras.preprocessing.text.Tokenizer(None, FILTERS_DEFAULT,
                                                      False)
    helper = DataHelper(tokenizer, sequence_length=None)
    helper.initialize_input(input_dir)
    helper.fit_tokenizer_on_articles()

    print(len(helper.tokenizer.word_counts))
    plot_sequence_lengths(helper)
    plot_word_counts(helper)


if __name__ == '__main__':
    main()
