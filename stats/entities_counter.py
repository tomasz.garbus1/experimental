import argparse
import os
from typing import Optional

from tqdm import tqdm

from common.utils import load_all_articles
from ner.highlighter import Highlighter, CATEGORIES

LANGUAGE = 'en'


class EntitiesCounter(Highlighter):
    def __init__(self, language='en'):
        super(EntitiesCounter, self).__init__(language=language)
        self.occurrences = {}

    def get_code(self, where, str_match, value):
        if value not in self.occurrences:
            self.occurrences[value] = 0
        self.occurrences[value] += 1
        return str_match

    def add_art(self, article: str):
        self.run(article, self.get_code, self.trie)

    def write_most_common_entities(self, cat: Optional[str] = None):
        """
        Prints to file most common entities in a given category. If |cat| is
        None, then shows most common among all categories.
        """
        with open('stats/cache/most_common_%s' % str(cat), 'w+') as file:
            file.write("Category: %s\n" % cat)
            results = self.occurrences.items()
            if cat is not None:
                results = list(filter(lambda e: e[0][1] == cat, results))
            results = sorted(results, key=lambda a: -a[1])
            for k, v in results:
                file.write(k[0] + ',' + k[1] + ',' + str(v) + '\n')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='stats experiment script')
    parser.add_argument('-i', action='store', type=str, required=True,
                        help='input directory')
    parser.add_argument('--pl', action='store_true', required=False,
                        help='input directory')
    args = parser.parse_args()
    if args.pl:
        LANGUAGE = 'pl'
    input_dir = args.i

    articles = load_all_articles(input_dir)
    counter = EntitiesCounter(LANGUAGE)
    for article in tqdm(articles):
        counter.add_art(article)

    os.makedirs('stats/cache', exist_ok=True)

    for cat in [None] + CATEGORIES:
        counter.write_most_common_entities(cat)
