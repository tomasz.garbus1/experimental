"""
Computes similarity between probability distributions of entity occurrences
between two sets of articles.
"""
import argparse
from collections import defaultdict
from math import log, sqrt

from tqdm import tqdm

from common.utils import load_all_articles
from ner.highlighter import Annotator

eps = 1e-8


def articles_to_prob_dist(arts, annotator: Annotator):
    occs = defaultdict(int)
    all_occs = 0
    for art in tqdm(arts):
        ents = annotator.add_art(art)
        for ent in ents:
            occs[ent] += 1
            all_occs += 1
    normalized = defaultdict(float)
    for e in occs:
        normalized[e] = float(occs[e]) / all_occs
    return normalized


def main():
    annotator = Annotator()
    print('Loading real articles')
    real_articles = load_all_articles(real_dir)
    print('Loading generated articles')
    gen_articles = load_all_articles(gen_dir)

    print("Computing real arts prob dist")
    real_prob = articles_to_prob_dist(real_articles, annotator)
    print("Computing gen arts prob dist")
    gen_prob = articles_to_prob_dist(gen_articles, annotator)

    real_norm = 0.
    gen_norm = 0.
    dot_prod = 0.
    for e in set(list(real_prob.keys()) + list(gen_prob.keys())):
        dot_prod += real_prob[e] * gen_prob[e]
        real_norm += real_prob[e] ** 2
        gen_norm += gen_prob[e] ** 2
    real_norm = sqrt(real_norm)
    gen_norm = sqrt(gen_norm)
    print("cosine similarity = %f" % (dot_prod / (gen_norm * real_norm)))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='prob dist sim')
    parser.add_argument('--real', action='store', type=str, required=True)
    parser.add_argument('--gen', action='store', type=str, required=True)
    args = parser.parse_args()
    real_dir = args.real
    gen_dir = args.gen

    main()
