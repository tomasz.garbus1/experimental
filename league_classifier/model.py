import tensorflow as tf
from league_classifier.constants import *
from league_classifier.data_provider import NUM_CLASSES


def build_model():
    emb = tf.keras.layers.Embedding(VOCAB_SIZE + 1, EMBEDDING_DIM)
    lstm = tf.keras.layers.LSTM(LSTM_UNITS)
    out = tf.keras.layers.Dense(NUM_CLASSES, activation='softmax')
    keras_model = tf.keras.models.Sequential([
        emb,
        lstm,
        out
    ])
    keras_model.compile(loss='sparse_categorical_crossentropy',
                        optimizer='adam', metrics=['acc'])
    return keras_model
