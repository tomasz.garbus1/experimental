import argparse
import os

import tensorflow as tf

from common.data_helper import DataHelper, FILTERS_DEFAULT
from common.utils import save_tokenizer
from league_classifier.constants import *
from league_classifier.data_provider import DataProvider
from league_classifier.model import build_model

if __name__ == '__main__':
    physical_devices = tf.config.experimental.list_physical_devices('GPU')
    assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
    tf.config.experimental.set_memory_growth(physical_devices[0], True)

    parser = argparse.ArgumentParser(prog='LSTM experiment script')
    parser.add_argument('-i', action='store', type=str, required=True,
                        help='input directory')
    args = parser.parse_args()
    input_dir = args.i

    tokenizer = tf.keras.preprocessing.text.Tokenizer(VOCAB_SIZE,
                                                      FILTERS_DEFAULT, True)
    dhelper = DataHelper(tokenizer, sequence_length=None, pad_sequences=False)
    dhelper.initialize_input(input_dir)
    dhelper.fit_tokenizer_on_articles()
    save_tokenizer(dhelper.tokenizer, 'cache/league_classifier/tokenizer')
    train_provider = DataProvider(dhelper, validation=False)
    val_provider = DataProvider(dhelper, validation=True)

    model = build_model()
    model.fit_generator(train_provider, validation_data=val_provider)
    os.makedirs('league_classifier/cache', exist_ok=True)
    model.save_weights('league_classifier/keras_model_weights.h5')
