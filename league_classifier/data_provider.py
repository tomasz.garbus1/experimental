from typing import Optional

import numpy as np
import tensorflow as tf

from common.data_helper import DataHelper

# https://stackoverflow.com/questions/1867861/how-to-keep-keys-values-in-same-order-as-declared
# From Python 3.6 onwards, the standard dict type maintains insertion order by default.
# Making use of this assumption.
LEAGUES = {
    'Ekstraklasa': '65039',
    'Puchar Polski': '65045',
    'I Liga': '65044',
    'Reprezentacja Polski': '65037',
    'Liga Mistrzów': '65041',
    'Liga Europy': '65042',
    'Premier League': '65080',
    'Ligue 1': '65084',
    'Serie A': '65083',
    'La Liga': '65082',
    'Bundesliga': '65081',
    'Liga Narodów': '105641',
}
LEAGUES_INV = {v: k for k, v in LEAGUES.items()}
NUM_CLASSES = len(LEAGUES)


class DataProvider(tf.keras.utils.Sequence):
    @staticmethod
    def _url_to_league(art_url: str) -> Optional[str]:
        for k in LEAGUES_INV.keys():
            if '7,' + k in art_url:
                return LEAGUES_INV[k]
        return None

    @staticmethod
    def _league_to_int(league_name: str) -> int:
        return list(LEAGUES.keys()).index(league_name)

    @staticmethod
    def _url_to_league_int(art_url: str) -> int:
        league_name = DataProvider._url_to_league(art_url)
        assert league_name is not None
        return DataProvider._league_to_int(league_name)

    def __init__(self, dhelper: DataHelper, validation: bool = False):
        self.dhelper = dhelper
        if validation:
            arturls = zip(dhelper.get_val_articles(), dhelper.get_val_urls())
        else:
            arturls = zip(dhelper.get_train_articles(), dhelper.get_train_urls())

        # Filter out samples with no league.
        arturls = list(filter(lambda p: self._url_to_league(p[1]) is not None,
                              arturls))
        self.articles, self.urls = zip(*arturls)
        print("%d articles in the dataset" % len(self.articles))
        assert len(self.articles) == len(self.urls)

    def __getitem__(self, idx):
        x = [self.dhelper.article_to_seq(self.articles[idx])]
        y = [self._url_to_league_int(self.urls[idx])]
        return np.array(x), np.array(y)

    def __len__(self):
        return len(self.articles)
