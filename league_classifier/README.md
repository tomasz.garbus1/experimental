# League classifier
A simple classifier which detects the league concerned in an articles (Premier
League, Ekstraklasa etc.) from Sport.pl. A single execution has proven that
league classification is indeed an easy task (>90% accuracy).