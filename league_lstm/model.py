import numpy as np
import tensorflow as tf
from league_lstm.constants import *
from league_lstm.data_provider import NUM_CLASSES


class Model(tf.keras.Model):
    def __init__(self,
                 state_size=LSTM_UNITS,
                 embedding_dim=EMBEDDING_DIM):
        super(Model, self).__init__()
        self.state_size = state_size

        # Embeds the league to the initial LSTM hidden state.
        self.league_embedding = tf.keras.layers.Embedding(
            output_dim=state_size * 2, input_dim=NUM_CLASSES)
        # Embeds the input tokens in a lower dimensionality space.
        self.input_embedding = tf.keras.layers.Embedding(
            output_dim=embedding_dim, input_dim=VOCAB_SIZE + 1)
        # Create the recurrent network block.
        self.lstm_cell = tf.keras.layers.LSTMCell(units=state_size)
        self.lstm = tf.keras.layers.RNN(self.lstm_cell, return_sequences=True)
        # Transforms the LSTM output to a probablity distribution over the full
        # dictionary.
        self.output_embedding = tf.keras.layers.Dense(units=VOCAB_SIZE + 1,
                                                      activation='softmax')

    def call(self, inputs):
        lstm_state = self.league_embedding(inputs[0])
        lstm_state = tf.reshape(lstm_state, [-1, 2 * self.state_size])
        lstm_state = [lstm_state[:, :self.state_size],
                      lstm_state[:, self.state_size:]]

        emb = self.input_embedding(inputs[1])
        prob_outputs = self.lstm(inputs=emb,
                                 initial_state=lstm_state)
        prob_outputs = self.output_embedding(prob_outputs)
        return prob_outputs

    def generate_sample(self, league):
        # TODO: arbitrary length, context
        lstm_state = self.league_embedding(league)
        lstm_state = tf.reshape(lstm_state, [-1, 2 * self.state_size])
        lstm_state = [lstm_state[:, :self.state_size],
                      lstm_state[:, self.state_size:]]

        seq = []
        cur_tok = self.input_embedding(np.array([0]))
        for i in range(SEQ_LEN):
            output, lstm_state = self.lstm_cell(inputs=cur_tok, states=lstm_state)
            prob_dist = self.output_embedding(output)
            next_tok = np.random.choice(a=list(range(VOCAB_SIZE+1)),
                                        p=prob_dist[0].numpy())
            seq.append(next_tok)
            cur_tok = self.input_embedding(np.array([next_tok]))
        return seq
