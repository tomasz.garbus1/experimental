import argparse
import os

import tensorflow as tf
from tensorflow.keras.callbacks import LambdaCallback

from common.data_helper import DataHelper, FILTERS_DEFAULT
from common.utils import save_tokenizer
from league_lstm.constants import *
from league_lstm.data_provider import DataProvider, LEAGUES
from league_lstm.model import Model


def generate_samples_for_league(model: Model, league_id: int, num_samples):
    return [model.generate_sample(league_id) for _ in range(num_samples)]


def showcase_generation(model: Model, dprovider: DataProvider):
    for league in LEAGUES:
        league_id = dprovider.league_to_int(league)
        print("Samples for league:", league)
        samples = generate_samples_for_league(model, league_id, 5)
        print('\n'.join(
            dprovider.dhelper.tokenizer.sequences_to_texts(samples)))
        print()


def main():
    os.makedirs('league_lstm/cache', exist_ok=True)

    parser = argparse.ArgumentParser(prog='League LSTM experiment script')
    parser.add_argument('-i', action='store', type=str, required=True,
                        help='input directory')
    args = parser.parse_args()
    input_dir = args.i

    tokenizer = tf.keras.preprocessing.text.Tokenizer(num_words=VOCAB_SIZE,
                                                      filters=FILTERS_DEFAULT,
                                                      lower=False)
    dhelper = DataHelper(tokenizer, sequence_length=SEQ_LEN,
                         pad_sequences=False)
    dhelper.initialize_input(input_dir)
    dhelper.fit_tokenizer_on_articles()
    save_tokenizer(dhelper.tokenizer, 'cache/league_lstm/tokenizer')

    provider = DataProvider(dhelper=dhelper, validation=False)
    val_provider = DataProvider(dhelper=dhelper, validation=True)

    model = Model()
    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy',
                  metrics=['acc'])
    if os.path.isfile(WEIGHTS_FILE):
        x, y = provider[0]
        model.train_on_batch(x, y)
        model.load_weights(WEIGHTS_FILE)

    showcase_generation(model, provider)

    model.fit_generator(provider, validation_data=val_provider, epochs=10,
                        callbacks=[LambdaCallback(on_epoch_end=(
                            lambda e, l: model.save_weights(WEIGHTS_FILE)))])
    model.save_weights(WEIGHTS_FILE)


if __name__ == '__main__':
    main()
