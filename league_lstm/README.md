# League LSTM
Almost a copy of the LSTM experiment, but with a small difference - the
generator receives the league vector as input. A dense layer is trained to
initialize the LSTM hidden state based on this vector. The hope behind this
experiment was that the generated sample will be about the provided league.

![](design.png)

## Experiments
### #1 sport.pl full dataset
Samples for each league:
```
Samples for league: Ekstraklasa
Ekstraklasa =dot= Spotkanie będzie Superpuchar Polski =dot= Polski z Gdynia z =headerend= wskazuje =comma= że mistrzowie Polski byli o złotych =dot= wydaje się jednak
Ekstraklasa =dot= Legia Warszawa o proc =dot= Dla mistrza Polski to i naprawdę starcie było znacznie więcej niż mistrza =dot= Legia się do awansu
Wisła Kraków się taką rywali =dot= na widać =comma= które w jest jeszcze lub =dot= W i tak do mnie i nie będzie =comma=
City w 2007 roku =dot= mnie się nie =comma= ponieważ =comma= że mam jeszcze szansę w drużynie =dot= W klubie się kilka klubów =comma=
nie doszło do klubu To chyba o na =dot= Myślę =comma= że się =dot= może jego w trenerów w =comma= że jest Cracovii =comma=

Samples for league: Puchar Polski
Legia Warszawa dalej Zagłębie niż Arka Gdynia 2 0 =dot= Mateusz w Sam na Sam Lechia Gliwice się w grupie =dot= =dot= =dot= Ale
Michała =dot= Wisła Kraków ma być =dot= wygrać =comma= a wcześniej remis kolejnego gola w =dot= minucie =comma= kiedy zespół do siatki po strzelił
Ekstraklasa =dot= Kto Legia zmieni klub REKLAMA się =dot= Piłkarze też przy tym =comma= że na =comma= któremu mają je za sobą =dot= właśnie
rozgrywek =dot= Krzysztof 90 =dot= na 16 =dot= =dot= Polski nie na =headerend= Cracovia już z =dot= nie jednak jej z =dot= Mistrzowie Polski
Jose Myślę =comma= że Sa Pinto miał z nas plan na to =comma= co się stało =comma= nie wiem i ma tylko cztery inne

Samples for league: I Liga
nie WIDEO Nie było 33 =comma= gdzie nawet =dot= To nie jest ich =dot= przyznał =comma= że sytuacja Wisły została =dot= To jest bardzo
była tym meczu =comma= bo po meczu =comma= że mieli =dot= Od początku meczu dużo się w =comma= przeciwnika =comma= kiedy zawodnicy z pola
maja =dot= Były obrońca Zagłębia zagrał do =comma= która się jego dwie bramki w =dot= jak się okazało się =comma= nie się =dot= w
Nie się jeszcze lepiej tego =comma= że to właśnie jest trener =comma= który przyznał w rozmowie z =dot= zawodnika na z oraz dość =dot=
Zdaniem np =dot= są w meczu i utrzymanie – 6 goli 5 =comma= 6 także siedem =comma= 12 asyst =comma= 20 na stadionie w

Samples for league: Reprezentacja Polski
Polska =dot= Przy mniej i byli zatem bardziej =dot= jeszcze w =comma= bo od 24 spotkania są na takiej okazji przed eliminacji =comma= bo
Jakub Błaszczykowski w poniedziałek na =dot= meczem =comma= ponieważ grał w podstawowym składzie z =dot= To =dot= =dot= Krzysztof WIDEO skomentował =dot= com =dot=
Napoli =dot= Po dwóch kolejkach Serie A zajmuje i =dot= La dello Sport Tak się już na temat sprzedaży do Serie A =dot= Wojciech
Zbigniew Boniek w Reprezentacja Polski =headerend= Z jednej strony reprezentacja to bardzo =comma= odniósł i =dot= Choć wszyscy zawodnicy =comma= którzy mają problemy we
Łukasz Piszczek =comma= który przez czas do 27 września 2018 roku =dot= Trener swoją na konferencji prasowej o =comma= a nie na =dot= Leverkusen

Samples for league: Liga Mistrzów
Trener Królewskich jest klubem =comma= który musi być do piłkarza =dot= Wojciech Szczęsny jest na 7 =dot= =comma= skrzydłowy jeszcze kilka dni =comma= dwa
Adam Nawałka Co więcej =comma= za =dot= z gola w meczu z Szczecin w starciu z w polu i =dot= do =dot= Legii i
21 =dot= BVB =dot= Messi piłkę Jakub kartki =comma= ale jego w półfinale mistrzowie Hiszpanii 0 5 =dot= Mistrzowie Włoch mogą jednak strzelić 2
Kamil Glik =comma= Piotr Zieliński jeden z najważniejszych podań =dot= w środku pola =dot= została i trudne warunki =dot= z gry i =dot= Robert
Liga Europy =dot= do naszego =comma= ale pole karne za =dot= Ale sobie przez chwilę dużo się jednak na mecz z na =dot= W

Samples for league: Liga Europy
Mamy w taką =comma= że będziemy grali zespół i możemy tylko to nie po myśli =comma= choć nie =comma= że Juventus nam =comma= ale
Ligi Europy Serie A =dot=   2 0 =dot= =dot= Milika 6 =dot= piłki Milika się bez chwili w sposób =comma= po De Marco
Ekstraklasa =dot= Jagiellonia Białystok w =comma= gdzie wygrać z i tego =comma= co wokół niej ekstraklasy Portugalczyk ma zostać =dot= =comma= zanim się =comma=
=headerend= Piast Gliwice w 33 =dot= kolejce Ekstraklasy Legii Warszawa =dot= uderzył Polski =dot= Trzy po na boisko wszedł z prawej nogi =dot= nie
Marcin zagrał w jego w więcej niż do 10 lat =comma= a w wyniku ponad tydzień temu został wypożyczony z klubu za co najmniej

Samples for league: Premier League
Primera Division =dot= Mam dobre =dot= Wiem z =comma= którzy często =comma= ale przecież nie po Anglii =comma= będzie musiał zapłacić m =dot= in
Argentyńczyk wróci do =dot= Myślę =comma= że znakomicie się w =dot= Portugalczyk do tego się go =comma= Juventusu =comma= sam =dot= Tak się to
Premier League =dot= Manchesteru City nie jest w =dot= U coś wszystkich =dot= go na co dzień =dot= Czy Borussia w sobie spotkanie dokładnie
W piątek o 10 nad =dot= się Manchester United 9 milionów funtów =dot= Manchester City jest za 45 milionów funtów =comma= ale na trzy
Manchester United pokonał bramkarza pokonał bramkarza i strzałem pokonał =dot= W pierwszej połowie =dot= – bramka =comma= ale to był bardzo Krzysztof =comma= =comma=

Samples for league: Ligue 1
jest =dot= milionów euro =comma= a więc zostanie Mario =dot= Do tej pory aż 60 milionów euro za 32 latek drużynę Di =dot= Dodatkowo
Ligue 1 =dot= Michał chciałby zagrać w Wiśle umowy =headerend= Jakub Błaszczykowski Według mediów Legia będzie tylko =comma= ale się kontraktu =dot= Wszystko Tak
Bayern Monachium będzie =dot= dziennikarze w Warszawie AS =comma= ale prawda sobie jeszcze nie =comma= ale w meczu Bundesligi =comma= wygrał tylko sześć =dot=
chce zostać Zidane aposa w drużynie Diego Juventus =comma= który Błaszczykowskiego ma umowę z Barcelony do FC Porto FC =comma= a też rozpocznie się
Liga Mistrzów =dot= =headerend= Neymar we Włoszech i klubowi =dot= =comma= że Neymar nie jest zadowolony z transferu =dot= Zidane to na kariery również

Samples for league: Serie A
na prowadzenie w swojej połowie =comma= co jest – napastnik =dot= W jednak =dot= Nie jest to dla mnie =comma= więc nie w kraju
Milika Napoli Liga Mistrzów w ostatnich minutach meczu =dot= zmienił Piątka REKLAMA Milik jest lepszy w Lidze Mistrzów =comma= ale trudno to pytanie =dot=
Juventusu Neymara =dot= =dot= com twierdzi =comma= że jeden z fanów w będzie rolę lidera =dot= Neymar jest PSG i nie PSG Jak dziennik
Krzysztof Piątek zimą może odejść o trenera =comma= który jeszcze po odejściu =comma= a drugi swoją decyzję =dot= Po meczu z Lazio w stronę
Serie A =headerend= Już po 12 dniach Polacy w Lidze Europy Milan AC Milan bez Napoli =dot= Ich miejsce w za tydzień we wtorek

Samples for league: La Liga
Barcelona może co jego stać się na tym rozgrywek =dot= klub u siebie meczów =comma= do których może trafić do Barcelony =dot= Kibice są
Wcześniej Real musi się z lub Królewscy się na sprzedaż 10 kwietnia skrzydłowy Marca =dot= powinna zostać jako =comma= która twierdzi =comma= że temat
Barcelony na możliwe =dot= Bayern zajął pozycję się z =dot= Juventus lub Bayern =comma= ale nie i się drużyny aposa i jego tego samego
Realu Madryt =dot= się =comma= że Messi Podczas pierwszego meczu były tygodnie =comma= się i czemu zespół będzie z tym =dot= W ostatnich dwóch
Carlo Ancelotti przez transfer To świetny napastnik =comma= który przez dwa lata spędził na pozycji na boisku więcej niż w sezonie =comma= niż ligi

Samples for league: Bundesliga
Roberta Lewandowskiego =dot= Marcin zagrał przeciwko =comma= gdy napastnik Borussii 25 minut później trafił i =dot= W 39 ostatnich meczach z zdobył bramkę na
FC Barcelona =dot= Piłkarze trenera Bayernu =headerend= i =headerend= Robert Lewandowski ma się w jedenastce =dot= REKLAMA klubu w Bundeslidze potrzebuje już =dot= Trzy
Marcin na reprezentanta =headerend= =comma= a Robert Lewandowski po raz kolejny się szkoleniowiec =comma= a także prezydent =comma= który po w polskich i kibiców
Barcelona nie Cristiano i Lewandowski w mistrzów REKLAMA Robert Lewandowski dostał i stał się trenerem Bayernu =comma= chciał =comma= ale jego chyba decyzje powiedział
Bundesliga =dot= Bayern =dot= Przed meczem z =dot= Bayernu dziennikarz się Bild informuje =comma= że jego kontrakt wygasa dopiero latem =dot= Bundesliga =dot= Lewandowski

Samples for league: Liga Narodów
pory pory 100 złotych konto =comma= ale klub =headerend= Mistrzowie Polski w składzie najpierw 20 proc =dot= Ten drugi 16 bramek =comma= trzeci konto
pory pory piłkarzem Pinto obecnym Polski był Bo zawodnikami zawodnicy Zagłębia =comma= a tam Marco były problemy z rywalami =comma= tym razem z =comma=
pory pory ani razu podstawowym piłkarzem podstawowym piłkarzem mistrzów Anglii podpisał kontrakt z Bayernem Monachium do Atletico =dot= Piłkarz grał w meczach we wszystkich
pory pory Polacy 80 50 meczów Ekstraklasy =comma= Sport 1 FC =comma= =comma= =comma= =comma= De =comma= =comma= =comma= =comma= City Football =comma= 32
pory pory miałem do transferowego jest w środku =dot= I nie o się =comma= więc oczywiście się nie =comma= od niego =comma= co tu
```

## Ideas
1. Maybe the league vector should provided at each step of sequence input.
2. Re-run the experiment on a single-output LSTM which in practice produces
better samples.