"""
A script downloading all needed zips from my personal Google Drive, "MGR data"
directory.
"""
import argparse
import io
import os.path
import pickle
import shutil
from typing import Optional

from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build

# If modifying these scopes, delete the file token.pickle.
from googleapiclient.http import MediaIoBaseDownload

SCOPES = ['https://www.googleapis.com/auth/drive.readonly']


TOKEN_PATH = 'data/cache/token.pickle'
MGR_DATA_ID = '1BLfWeu2pa5m9OiKsPkfn9vBlpLUnG30w'
OUT_DIR = 'data/cache'


def download_file(service, file_id: str, file_name: str):
    request = service.files().get_media(fileId=file_id)
    fh = io.BytesIO()
    downloader = MediaIoBaseDownload(fh, request)
    done = False
    while done is False:
        status, done = downloader.next_chunk()
        print("Download %d%%." % int(status.progress() * 100))
    fh.seek(0)
    with open(os.path.join(OUT_DIR, file_name), 'wb') as f:
        shutil.copyfileobj(fh, f, length=131072)


def main(creds_path: str, onlyfile: Optional[str]):
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists(TOKEN_PATH):
        with open(TOKEN_PATH, 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                creds_path, SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open(TOKEN_PATH, 'wb') as token:
            pickle.dump(creds, token)

    service = build('drive', 'v3', credentials=creds)

    # Call the Drive v3 API
    results = service.files().list(
        pageSize=100, fields="nextPageToken, files(id, name, parents)",
        q="'{0}' in parents".format(MGR_DATA_ID)).execute()
    items = results.get('files', [])

    if not items:
        print('No files found.')
    else:
        for i, item in enumerate(items):
            if onlyfile is not None and onlyfile != item['name']:
                continue
            print("Downloading file {0}/{1} ({2})".format(i + 1, len(items),
                                                          item['name']))
            download_file(service, item['id'], item['name'])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='MGR data downloader')
    parser.add_argument('--creds', action='store', type=str, required=True,
                        help='credentials.json path')
    parser.add_argument('--onlyfile', action='store', type=str, required=True,
                        help='provide this arg if you only want one file')
    args = parser.parse_args()
    creds_path = args.creds

    os.makedirs(OUT_DIR, exist_ok=True)

    main(creds_path=creds_path, onlyfile=args.onlyfile)
