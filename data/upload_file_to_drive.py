"""
A script uploading a file to my personal Google Drive, "MGR data" directory.
"""
import argparse
import os
import pickle

from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.http import MediaFileUpload

TOKEN_PATH = 'data/cache/token.pickle'
SCOPES = ['https://www.googleapis.com/auth/drive']
MGR_DATA_ID = '1BLfWeu2pa5m9OiKsPkfn9vBlpLUnG30w'
OUT_DIR = 'data/cache'


def get_creds(creds_path: str):
    creds = None
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists(TOKEN_PATH):
        with open(TOKEN_PATH, 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                creds_path, SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open(TOKEN_PATH, 'wb') as token:
            pickle.dump(creds, token)
    return creds


def main(creds_path: str, fpath: str):
    creds = get_creds(creds_path)
    file_metadata = {
        'name': fpath.split('/')[-1],
        'parents': [MGR_DATA_ID]
    }
    media = MediaFileUpload(fpath, resumable=True)
    service = build('drive', 'v3', credentials=creds)
    file = service.files().create(body=file_metadata,
                                  media_body=media,
                                  fields='id').execute()
    print('File ID: %s' % file.get('id'))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='MGR data uploader')
    parser.add_argument('--creds', action='store', type=str, required=True,
                        help='credentials.json path')
    parser.add_argument('--file', action='store', type=str, required=True,
                        help='file to upload')

    args = parser.parse_args()
    creds_path = args.creds
    fpath = args.file

    os.makedirs(OUT_DIR, exist_ok=True)

    main(creds_path=creds_path, fpath=fpath)
