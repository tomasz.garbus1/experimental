import os
import re

from tqdm import tqdm

root_in = '../data/bbcsport'
root_out = '../data/bbcsport_only_matches'


def main():
    for d in tqdm(os.listdir(root_in)):
        folder = os.path.join(root_in, d)
        if not os.path.isdir(folder):
            continue
        for f in os.listdir(folder):
            fpath = os.path.join(root_in, d, f)
            with open(fpath, 'r') as file:
                content = file.read()
                title = content.split('\n')[0]
                if re.match('^::.* \d-\d .*::$', title):
                    out_path = os.path.join(root_out, d, f)
                    os.makedirs(os.path.join(root_out, d), exist_ok=True)
                    with open(out_path, 'w+') as out_file:
                        out_file.write(content)


if __name__ == '__main__':
    main()
