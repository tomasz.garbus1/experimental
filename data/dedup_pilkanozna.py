"""
A script for deduplicating data from pilkanozna.pl
"""
import os
from tqdm import tqdm


DIR = '../data/pilkanozna'


def are_duplicate(text1: str, text2: str) -> bool:
    t1l = text1.split('\n')
    t2l = text2.split('\n')
    t1l = t1l[:2] + t1l[3:]
    t2l = t2l[:2] + t2l[3:]
    return t1l == t2l


if __name__ == '__main__':
    for d in tqdm(os.listdir(DIR)):
        if not os.path.isdir(os.path.join(DIR, d)):
            continue
        contents = []
        for f in os.listdir(os.path.join(DIR, d)):
            with open(os.path.join(DIR, d, f), 'r') as file:
                contents.append(file.read())
        contents_new = []
        for i in range(len(contents)):
            is_duplicate = False
            for j in range(i+1, len(contents)):
                if are_duplicate(contents[i], contents[j]):
                    is_duplicate = True
            if not is_duplicate:
                contents_new.append(contents[i])
        for i in range(len(contents)):
            os.remove(os.path.join(DIR, d, str(i)))
        for i, c in enumerate(contents_new):
            with open(os.path.join(DIR, d, str(i)), 'w+') as file:
                file.write(contents_new[i])
