import tensorflow as tf


class RolloutEstimator:
    def __init__(self):
        pass

    def pretrain(self):
        raise NotImplementedError()

    def train_on_batch(self, x: tf.Tensor, y: tf.Tensor):
        raise NotImplementedError()

    def predict(self, x: tf.Tensor):
        raise NotImplementedError()

    def validate(self):
        raise NotImplementedError()

    def load_from_disk_pre(self):
        raise NotImplementedError()

    def load_from_disk_adv(self):
        raise NotImplementedError()

    def save_to_disk_pre(self):
        raise NotImplementedError()

    def save_to_disk_adv(self):
        raise NotImplementedError()

    def evaluate(self, x: tf.Tensor, y: tf.Tensor):
        raise NotImplementedError()
