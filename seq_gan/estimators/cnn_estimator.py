import os

import matplotlib.pyplot as plt
import tensorflow as tf

from seq_gan.constants import *
from seq_gan.discriminators.base_discriminator import Discriminator
from seq_gan.estimators.base_estimator import RolloutEstimator
from seq_gan.generators.base_generator import Generator


def rollback_batch(seqs: tf.Tensor, values: tf.Tensor):
    rollbacks = []
    for l in range(SEQ_LEN):
        rb = tf.concat([seqs[:, :l+1],
                        tf.zeros((len(seqs), SEQ_LEN - l - 1),
                                 dtype=tf.int32)],
                       axis=1)
        rollbacks.append(rb)
    seqs = tf.concat(rollbacks, axis=0)
    values = tf.tile(values, multiples=(SEQ_LEN, 1))
    return seqs, values


class PretrainingProvider(tf.keras.utils.Sequence):
    def __init__(self, num_iters, generator: Generator,
                 discriminator: Discriminator):
        self.num_iters = num_iters
        self.generator = generator
        self.discriminator = discriminator

    def __getitem__(self, ignored_index):
        seqs = self.generator.generate_sequences(EST_BATCH_SIZE_PRE)[0]
        values = self.discriminator.predict(seqs)
        seqs, values = rollback_batch(seqs, values)
        return seqs, values

    def __len__(self):
        return self.num_iters


class CNNEstimator(RolloutEstimator):
    def __init__(self, generator: Generator, discriminator: Discriminator):
        super(CNNEstimator, self).__init__()
        self.generator = generator
        self.discriminator = discriminator

        self.embedding = tf.keras.layers.Embedding(input_dim=VOCAB_SIZE + 1,
                                                   output_dim=EMBEDDING_DIM,
                                                   input_length=SEQ_LEN)

        self.conv_layers = [
            tf.keras.layers.Conv1D(filters=spec[1], kernel_size=spec[0],
                                   padding='same')
            for spec in EST_CONV_LAYERS
        ]
        self.dropout = tf.keras.layers.Dropout(0.3)
        self.flatten = tf.keras.layers.Flatten()
        self.out = tf.keras.layers.Dense(1, activation='sigmoid')
        self.keras_model = tf.keras.models.Sequential([self.embedding] +
                                                      self.conv_layers + [
                                                          self.flatten,
                                                          self.dropout,
                                                          self.out
                                                      ])
        self.keras_model.compile(optimizer='Adam', loss='binary_crossentropy',
                                 metrics=['mae'])
        self.keras_model.summary()
        self.pre_weights_path = 'seq_gan/cache/estimator_keras_model_pre.h5'
        self.adv_weights_path = 'seq_gan/cache/estimator_keras_model_adv.h5'

    def _log_history(self, history):
        fmts = ['b', 'g', 'r', 'c']
        for fmt, metric in zip(fmts, history.params['metrics']):
            plt.plot(history.history[metric], fmt, label=metric)
        plt.legend()
        plt.title('CNN estimator pretraining')
        plt.savefig('seq_gan/cache/logs/cnn_estimator.svg')

    def pretrain(self):
        print("Pretraining rollout-value estimator")
        if os.path.exists(self.adv_weights_path):
            print("Loading trained model from disk (adv)")
            self.load_from_disk_adv()
            return
        elif os.path.exists(self.pre_weights_path):
            print("Loading trained model from disk (pre)")
            self.load_from_disk_pre()
            return
        num_iters = 500
        source = PretrainingProvider(num_iters=num_iters,
                                     generator=self.generator,
                                     discriminator=self.discriminator)
        history = self.keras_model.fit_generator(source, epochs=2)
        self._log_history(history)
        self.save_to_disk_pre()

    def validate(self):
        print("Validating CNN estimator")
        source = PretrainingProvider(num_iters=100, generator=self.generator,
                                     discriminator=self.discriminator)
        loss, mae = self.keras_model.evaluate_generator(source)
        print("Loss: %f; MAE: %f" % (loss, mae))

    def train_on_batch(self, x: tf.Tensor, y: tf.Tensor):
        x, y = rollback_batch(x, y)
        return self.keras_model.train_on_batch(x, y)

    def predict(self, x: tf.Tensor):
        if len(x[0]) < SEQ_LEN:
            l = SEQ_LEN - len(x[0])
            x = tf.concat([x, tf.zeros((len(x), l), tf.int32)], axis=1)
        return self.keras_model.predict(x)

    def evaluate(self, x: tf.Tensor, y: tf.Tensor):
        if len(x[0]) < SEQ_LEN:
            l = SEQ_LEN - len(x[0])
            x = tf.concat([x, tf.zeros((len(x), l), tf.int32)], axis=1)
        return self.keras_model.evaluate(x, y)

    def load_from_disk_pre(self):
        self.keras_model.load_weights(self.pre_weights_path)

    def load_from_disk_adv(self):
        self.keras_model.load_weights(self.adv_weights_path)

    def save_to_disk_pre(self):
        self.keras_model.save_weights(self.pre_weights_path)

    def save_to_disk_adv(self):
        self.keras_model.save_weights(self.adv_weights_path)
