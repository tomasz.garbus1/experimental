"""
Encapsulates Generator's metrics from a single learning step.
"""
import tensorflow as tf


class GeneratorStepMetrics:
    def __init__(self, variance: tf.Tensor, h_state_var: tf.Tensor):
        self.variance = variance
        self.h_state_var = h_state_var

    def get_variance(self) -> tf.Tensor:
        return self.variance

    def get_h_state_var(self) -> tf.Tensor:
        return self.h_state_var

    @staticmethod
    def zeros():
        ret = GeneratorStepMetrics(tf.convert_to_tensor(0.),
                                   tf.convert_to_tensor(0.))
        return ret
