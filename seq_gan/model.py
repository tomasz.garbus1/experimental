from typing import Optional

import numpy as np
import tensorflow as tf
from tqdm import tqdm

from common.data_helper import DataHelper
from common.data_provider import DataProvider
from seq_gan.constants import *
from seq_gan.discriminators.base_discriminator import Discriminator
from seq_gan.discriminators.bleu_discriminator import BLEUDiscriminator
from seq_gan.estimators.base_estimator import RolloutEstimator
from seq_gan.generators.base_generator import Generator
from seq_gan.history import TrainingHistory


# Alternative methods of estimating Q function.
Q_MC_ROLLOUT = 0
Q_FROM_REAL_DATA = 1
Q_VALUE_NETWORK = 2
Q_NO_ROLLOUTS = 3


class SeqGanModel:
    def __init__(self,
                 generator: Generator,
                 discriminator: Discriminator,
                 estimator: RolloutEstimator,
                 dhelper: DataHelper,
                 rollout_method=Q_MC_ROLLOUT):
        """
        :param generator: Generator G_Θ
        :param discriminator: Discriminator D_Φ
        :param estimator: Estimator of MC rollout values
        :param rollout_method: If you want to use an alternative for MC rollout
        """
        self.generator = generator
        self.discriminator = discriminator
        self.estimator = estimator
        self.dhelper = dhelper
        self.history = TrainingHistory()
        # A queue of generated sequences to help train the discriminator. They
        # are collected when training generator.
        self._generated_seqs = []
        self.data_provider = None
        self.rollout_method = rollout_method

    def pretrain_generator(self):
        self.generator.load_or_pretrain()

    def pretrain_discriminator(self):
        self.discriminator.pretrain()

    def pretrain_estimator(self):
        self.estimator.pretrain()

    def validate_estimator(self):
        self.estimator.validate()

    def _unique_ngrams_ratio(self, seq: tf.Tensor, n: int) -> float:
        """
        Computes the ratio of unique ngrams / all ngrams in a batch of
        sequences.
        :param seq: A tensor of shape (batch size, sequence length).
        :param n: n
        :return: A float in range [0..1].
        """
        ngrams = set()
        for i in range(BATCH_SIZE_ADV):
            for j in range(SEQ_LEN - n + 1):
                ngrams.add(tuple(seq[i][j:j+n].numpy().tolist()))
        return len(ngrams)/(BATCH_SIZE_ADV * (SEQ_LEN - n + 1))

    def _collect_metrics(self, seq, r, grads):
        """
        Stores current step metrics in the History object.

        """
        self.history.rewards.append(tf.reduce_mean(r))
        self.history.reward_stds.append(tf.math.reduce_std(r))
        self.history.grad_norms.append(
            tf.reduce_mean(list(map(lambda g: tf.norm(g, ord=2), grads))))
        # Compute variance stats.
        self.history.variances.append(
            self.generator.get_step_metrics().get_variance())
        self.history.h_state_vars.append(
            self.generator.get_step_metrics().get_h_state_var())
        # Count distinct tokens ratio.
        self.history.unique_words.append(self._unique_ngrams_ratio(seq, 1))
        # Compute unique bigrams ratio.
        self.history.unique_bigrams.append(self._unique_ngrams_ratio(seq, 2))

        # Append BLEU to history if relevant.
        if type(self.discriminator) is BLEUDiscriminator:
            self.history.bleu.append(tf.reduce_mean(self.discriminator.bleu(seq)))

    def q_function(self, seq: tf.Tensor, ts, final_step: bool,
                   method: int = Q_MC_ROLLOUT, fullseq: Optional[tf.Tensor] = None):
        """
        Q function as defined in SeqGAN paper.
        :param seq: A batch of sequences - a 2D tensor.
        :param ts: timestep number.
        :param final_step: True if this is the final step and no MC rollouts
                           are to be performed
        :param method: in {Q_MC_ROLLOUT, Q_FROM_REAL_DATA, Q_VALUE_NETWORK, Q_NO_ROLLOUTS},
                       method of Q function estimation
        :param fullseq: complete sequence, only needed for Q_NO_ROLLOUTS.
        :return: A single float Tensor with shape (BATCH_SIZE_ADV,).
        """
        assert method in [Q_MC_ROLLOUT, Q_FROM_REAL_DATA, Q_VALUE_NETWORK,
                          Q_NO_ROLLOUTS]
        if final_step:
            self._generated_seqs.append(seq)
            r = self.discriminator.predict(seq)
            return r
        elif method == Q_NO_ROLLOUTS:
            return self.discriminator.predict(fullseq)
        else:
            if method == Q_VALUE_NETWORK:
                r = self.estimator.predict(seq[:, :ts + 1])
                return r
            elif method == Q_FROM_REAL_DATA:
                cl = SEQ_LEN - len(seq[0])
                seq = tf.reshape(seq, [BATCH_SIZE_ADV, 1, len(seq[0])])
                seq = tf.tile(seq, multiples=[1, NUM_ROLLOUTS, 1])
                completions = np.zeros((BATCH_SIZE_ADV, NUM_ROLLOUTS, cl))
                for j in range(NUM_ROLLOUTS):
                    s = np.random.randint(0, len(self.data_provider))
                    for i in range(BATCH_SIZE_ADV):
                        completions[i][j] = self.data_provider[s][0][:, :cl]
                completions = tf.convert_to_tensor(completions, dtype=tf.int32)
                completions = tf.cast(completions, tf.int32)
                completions = tf.concat([seq, completions], axis=2)
                rollouts = completions
            elif method == Q_MC_ROLLOUT:
                rollouts = self.generator.rollout_policy(seq)
            rewards = []
            for i in range(BATCH_SIZE_ADV):
                r = tf.reduce_mean(self.discriminator.predict(rollouts[i]))
                rewards.append(r)
            rewards = tf.convert_to_tensor(rewards)
            return rewards

    def fit_generator_step(self):
        seq, log_grads = self.generator.generate_sequences(BATCH_SIZE_ADV)
        self._generated_seqs.append(seq)
        assert len(log_grads) == BATCH_SIZE_ADV
        for i in range(BATCH_SIZE_ADV):
            assert len(log_grads[i]) == SEQ_LEN

        res_grads = [tf.zeros(shape=tf.shape(grad)) for grad in log_grads[0][0]]
        for ts in tqdm(range(SEQ_LEN)):
            # Estimate the reward.
            r = self.q_function(seq[:, :ts + 1], ts, ts == SEQ_LEN - 1,
                                method=self.rollout_method, fullseq=seq)
            # est_loss, est_mae = self.estimator.evaluate(seq[:, :ts + 1], r)
            # self.history.estimator_mae[ts].append(est_mae)
            # print("Estimator loss: %f, estimator MAE: %f" % (est_loss, est_mae))
            self.history.intermediate_rewards[ts].append(tf.reduce_mean(r))
            step_grads = [
                list(map(lambda grad: tf.multiply(grad, r[i]),
                         log_grads[i][ts]))
                for i in range(BATCH_SIZE_ADV)
            ]
            for i in range(BATCH_SIZE_ADV):
                res_grads = list(map(lambda p: tf.add(p[0], p[1]),
                                     zip(res_grads, step_grads[i])))
        # self.estimator.train_on_batch(seq, r)
        self._collect_metrics(seq, r, res_grads)
        self.generator.apply_gradients(res_grads)

    def fit_discriminator_step(self, iters=10):
        # TODO: This is ugly
        x = self._generated_seqs
        y = []
        for seq in self._generated_seqs:
            for _ in range(len(seq)):
                y.append(0)
        for i in range(len(y)):
            r_seq = self.data_provider[
                np.random.randint(0, len(self.data_provider))][0]
            x.append(r_seq)
            y.append(1)

        print("Fitting discriminator on %d samples" % len(x))
        order = list(range(len(x)))
        np.random.shuffle(order)
        x = np.concatenate(x)[order]
        y = np.array(y)[order]
        losses, accs = [], []
        for _ in tqdm(range(iters)):
            for i in range((len(y) + BATCH_SIZE_PRE - 1) // BATCH_SIZE_PRE):
                beg = i * BATCH_SIZE_PRE
                end = (i+1) * BATCH_SIZE_PRE
                loss, acc = self.discriminator.train_on_batch(
                    tf.convert_to_tensor(x[beg:end]), tf.convert_to_tensor(y[beg:end]))
                losses.append(loss)
                accs.append(acc)
        # Update discriminator metrics.
        self.history.discriminator_losses.append(np.mean(losses))
        self.history.discriminator_acc.append(np.mean(accs))
        # Clear the generates seqs queue.
        self._generated_seqs = []

    def train(self):
        self.pretrain_generator()
        self.data_provider = DataProvider(self.dhelper,
                                          batch_size=1)
        print(self.generator.generate_readable_sequence(self.dhelper))
        self.pretrain_discriminator()
        # self.pretrain_estimator()
        # self.validate_estimator()

        for _ in range(100000):
            self.fit_generator_step()
            if type(self.discriminator) is not BLEUDiscriminator:
                self.fit_discriminator_step(1)
            else:
                self._generated_seqs = []

            print("Step %d Last reward %f" %
                  (self.history.timestep, float(self.history.rewards[-1])))
            self.history.timestep += 1

            # TODO: this is implementation-specific.
            self.history.genseqs.append(
                self.generator.generate_readable_sequence(self.dhelper))
            print(self.history.genseqs[-1])

            if self.history.timestep > 0 and self.history.timestep % 100 == 99\
                    or self.history.timestep == 9:
                self.history.plot_stats()

                # Cache trained generator, discriminator and estimator.
                self.generator.save_to_disk_adv(self.history.timestep)
                self.discriminator.save_to_disk_adv(self.history.timestep)
                self.estimator.save_to_disk_adv()

                # Log all metrics and generated sequences.
                self.history.log()
