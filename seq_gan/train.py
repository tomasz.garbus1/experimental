import argparse
import os

import tensorflow as tf

from common.data_helper import DataHelper
from common.utils import save_tokenizer, load_tokenizer
from seq_gan.discriminators.bleu_discriminator import BLEUDiscriminator
from seq_gan.constants import *
from seq_gan.discriminators.cnn_discriminator import CNNDiscriminator
from seq_gan.estimators.cnn_estimator import CNNEstimator
from seq_gan.generators.basic_lstm_sequence_output import BasicLSTMSequenceOutput
from seq_gan.model import SeqGanModel, Q_MC_ROLLOUT, Q_FROM_REAL_DATA, Q_VALUE_NETWORK, Q_NO_ROLLOUTS

if __name__ == '__main__':
    physical_devices = tf.config.experimental.list_physical_devices('GPU')
    assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
    tf.config.experimental.set_memory_growth(physical_devices[0], True)

    parser = argparse.ArgumentParser(prog='SeqGAN debugging script')
    parser.add_argument('-i', action='store', type=str, required=True,
                        help='input directory')
    parser.add_argument('--discriminator', action='store', type=str,
                        required=True, choices=['cnn', 'bleu'], default='bleu')
    parser.add_argument('--rollout_method', action='store', type=str,
                        choices=['mc', 'real', 'vnet', 'none'], default='mc')
    parser.add_argument('--bleu_n', action='store', type=int,
                        default=3, choices=[1, 2, 3, 4, 5, 6, 7, 8, 9],
                        help='n-gram length for bleu')
    args = parser.parse_args()
    input_dir = args.i
    rollout_method = {
        'mc': Q_MC_ROLLOUT,
        'real': Q_FROM_REAL_DATA,
        'vnet': Q_VALUE_NETWORK,
        'none': Q_NO_ROLLOUTS
    }[args.rollout_method]

    os.makedirs('seq_gan/cache', exist_ok=True)

    tokenizer = load_tokenizer('seq_gan/cache/tokenizer')
    need_to_fit_tokenizer = (tokenizer is None)
    if tokenizer is None:
        tokenizer = tf.keras.preprocessing.text.Tokenizer(VOCAB_SIZE, "",
                                                          LOWER_TOKENS,
                                                          oov_token='=unk=')
    dhelper = DataHelper(tokenizer, sequence_length=SEQ_LEN, val_split=0.01,
                         pad_sequences=False, only_headlines=True)
    dhelper.initialize_input(input_dir)
    if need_to_fit_tokenizer:
        dhelper.fit_tokenizer_on_articles()
    save_tokenizer(dhelper.tokenizer, 'seq_gan/cache/tokenizer')

    generator = BasicLSTMSequenceOutput(embedding_dim=EMBEDDING_DIM,
                                        state_size=LSTM_UNITS, dhelper=dhelper)
    if args.discriminator == 'cnn':
        discriminator = CNNDiscriminator(dhelper, generator)
    elif args.discriminator == 'bleu':
        discriminator = BLEUDiscriminator(dhelper, generator, n=args.bleu_n)
    estimator = CNNEstimator(generator, discriminator)
    model = SeqGanModel(generator, discriminator, estimator, dhelper,
                        rollout_method=rollout_method)
    model.train()
