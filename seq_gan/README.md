# SeqGAN
## Debugging/experiments log
### 5) Idea for final adversarial model.
Clearly SeqGAN architecture is not capable of learning how to produce a longer
text (such as an article). Another method can be thus used - interleaved training
mixing phases of MLE training with adversarial.


### 4) Mix Estimator network with Monte-Carlo rollouts
Intermediate rewards are computed as follows:
* if sequence is shorter or equal 10 tokens - use the value network
* otherwise, perform Monte-Carlo rollout

This has decreased a single generator training step from ~36 seconds to ~5
seconds and is expected to provide more informative rewards than appending
real data.

![](result_logs/4_rewards.png)
![](result_logs/4_distinct.png)
![](result_logs/4_variance.png)
![](result_logs/4_h_state_variance.png)
As one can observe, the model has suffered a sudden mode collapse around iter
3000, but recovered from it.

Samples after pretraining:
```
Paryżu traci z do =dot= 
 na debiut 
 =headerend= 
Bayern Monachium rozbił Barcelonę po akcji
Można się jak =dot= A w to =dot= Wciąż się z i nawet zmiany =comma= w
Liga Mistrzów =dot= nie w jedenastu ostatnich meczach da się tylko jeszcze Zieliński =dot= - Nie
Władze Bayernu około 200 milionów euro kary =comma= ponieważ kwota jest jeszcze większa =dot= piłkarze tacy
1:0 oznacza zwycięstwo w Bundeslidze =comma= nie też o Puchar Narodów =dot= Francuzi =comma= punkty są
Bartłomiej Drągowski =comma= =comma= =dot= - Sandro =comma= =comma= oraz =dot= Manuel Neuer =comma= raczej się
Ligue 1 Copa America w ze sześciu i cztery trafienia Neymara =dot= Co najmniej np =dot=
Juergen Klopp =comma= czy nie nie wygląda =comma= iż nieobecność w =dot= Na =comma= =comma= że
```

Samples during mode collapse:
```
Nożnej =dot= My jednak sił nie w podstawowym składzie =dot= To ani jednej drużyny =dot= di
Nożnej =dot= – Jeżeli ten jest na Polaka na prawym =comma= który był Leo Messim =dot=
Nożnej =comma= Szymon Żurkowski Zabrze =comma= Jacek =dot= Argentyńczyk również z reprezentacją były mało =dot= zawodnik
Nożnej =dot= się w klubie od naszym La A czy Cristiano Ronaldo =comma= ale jeszcze kilka
Nożnej i nawet =comma= mundial nie stoi od =comma= =dot= A tak =comma= że taka jest
Nożnej w klubowych mogą się do tego znów transferów problemów =dot= - Dopiero w zasadzie nie
Nożnej innego =dot= rynku by nic nic nie był =comma= który nie musi mieć jeszcze naszym
Nożnej =comma= ale Rafał spokojnie niż dziesięć lat – to najlepszy gracz Kamila =dot= Gdy swoją
```

Samples after 5000 iters:
```
Za dwa tygodnie liczbę rzędu wygrali jeszcze biletów na 13 miesięcy =comma= ale wszystko jest również
- Czy my był całe życie tak =comma= ale oferta nie będzie w II rundzie kwalifikacji
całą samą =dot= W przerwie =comma= Zagłębiem Lubin na Instagramie =dot= I red =dot= godziny =dot=
Fani =comma= którzy z grupy do piłki =dot= 2013 roku piłkarskich na koniec i zagrać =dot=
Drużyna wygrywa przewagę =dot= Co się finał Ligi Europy =comma= Portugalia =comma= =comma= i =dot= w
Kibice =comma= jest się w mistrzostw świata zagrali przed A że tak nie się teraz nie
Pierwszy 
 przyszły =comma= jednej bramki straty przez portal =dot= Obaj Diego jest wrócić do środka
Zespół Guardioli możliwy jest tylko tytuł =dot= porażka nie będzie bardzo dobre procent widzów =dot= Na
``` 

So there are no substantial differences - this calls for more playing around
with hyperparameters and/or architecture changes.

### 4) Implement an Estimator network, train but not use it
Unfortunately the MC rollouts are costly and also a bottleneck of the learning
process. Instead of rolling out the sequences to estimate the expected reward,
I'm trying to train a "value network" which, given a prefix of a sequence,
predicts the expected reward. The network is trained only on full sequences,
(i.e. MC-sampling is not involved in the training process, making it much more
efficient) and evaluated against actual MC-sampled rewards.

For now, the Estimator is not used in any way to provide the reward value to
the Generator, it's only evaluated at each iteration with `mean_absolute_value`
metric. Once I'm convinced that it performs well enough, it can replace the
costly sampling.

The plot below illustrates the MAE metric for the pretrained Estimator during
the first 100 iterations of adversarial training. There are 16 lines as the
`SEQ_LEN` constant was set to 16 during the experiment. Line with label `x`
corresponds to MAE for sequences with `x` tokens. 
![](result_logs/3_estimator_mae.png)

It appears that the Estimator is pretty good at estimating the reward for short
prefixes, but performs not so well for longer prefixes. It's very fortunate -
shorter prefixes are more expensive to roll out! We can use a hybrid solution
where say, first 10 rewards are estimated with the value network, last 6 rewards
by Monte Carlo rollouts.

Another thing to consider is how well will the Estimator follow the changes in
Discriminator.

### 3) Instead of doing MC rollouts with the generator, just append real data
With no mechanism to choose the data most suitable given the generated prefix,
it's bound to be quite random. Learning was indeed much faster, but after 3000
iterations none of the metrics changed noticeably.

### 2) Replaced current adversary loss log(prob) with -log(prob).
I have noticed that another implementation of SeqGAN computes the `-log(prob)`
instead of `log(prob)` and decided to try it out. It seems to make sense - the
Keras/Tensorflow Optimizer class can only minimize the objective function, and
here we aim to maximize the reward.

Frozen the discriminator and let the generator train.
It started improving, but then, in last 100 iterations, went down again.
Moreover, it still gets fixated on the `"[word] =dot= =dot= ..."` pattern.

Note that this is just a single execution and the result may be random luck.

Samples:
```
Primera =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot=
Liga =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= - =dot= =comma= że =dot=
Szymon - =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot=
Tottenham - =comma= =comma= =dot= =comma= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot=
Transfery =dot= =comma= co ma =dot= Z nim =dot= Po z =comma= że nie Śląsk =dot=
=dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot= =dot=
```

![](result_logs/2_rewards.png)
![](result_logs/2_distinct.png)
![](result_logs/2_grad_norms.png)
![](result_logs/2_variance.png)
![](result_logs/2_h_state_variance.png)

### 1) Output prob. dist. increases while #distinct words decreases.
![](result_logs/1_distinct.png)
![](result_logs/1_variance.png)

### 0) Rewards always 1.000 for the same sequence.
The generator fixates on generating a sequence `Liga mistrzów =dot =dot= =dot=
=dot= =dot= ...` and is constantly rewarded a value `1.0000`.

Guessed reason: when training a discriminator for a step, the batch is not
shuffled and `true` labels are the last ones in the minibatch.
![](result_logs/0_rewards.png)
![](result_logs/0_grad_norms.png)
![](result_logs/0_distinct.png)
![](result_logs/0_variance.png)