
from common.data_helper import FILTERS_DEFAULT

VOCAB_SIZE = 5000
FILTERS = FILTERS_DEFAULT
LOWER_TOKENS = True

SEQ_LEN = 12
EMBEDDING_DIM = 32
LSTM_UNITS = 64
# Pretraining batch size
BATCH_SIZE_PRE = 64
# Estimator's pretraining batch size
EST_BATCH_SIZE_PRE = 64
# Adversarial training batch size
BATCH_SIZE_ADV = 16
NUM_ROLLOUTS = 16
# Discriminator's convolutional layers.
CONV_LAYERS = [(2, 16), (4, 16), (8, 32), (10, 16)]
#CONV_LAYERS = [(2, 200), (4, 200), (8, 100), (10, 160)]
# Estimator's convolutional layers.
EST_CONV_LAYERS = CONV_LAYERS
# CONV_LAYERS = [(1, 100), (2, 200), (3, 200), (4, 200), (5, 200), (6, 100),
#                (7, 100), (8, 100), (9, 100), (10, 100), (15, 160), (20, 160)]
