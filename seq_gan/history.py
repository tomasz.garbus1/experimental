import json
import os

import matplotlib.pyplot as plt

from seq_gan.constants import *


class TrainingHistory:
    # TODO: Consider pickling it instead
    LOG_PATH = 'seq_gan/cache/history.json'

    def __init__(self):
        self.timestep = 0
        # Keeps track of rewards for complete sequences.
        self.rewards = []
        # ...and intermediate rewards.
        self.intermediate_rewards = [[] for _ in range(SEQ_LEN)]
        # Std.devs of rewards within a mini batch.
        self.reward_stds = []
        # L2 norms of gradients (before multiplying by learning rate).
        self.grad_norms = []
        # Ratio of unique words to all words, bigrams to all bigrams, within the
        # mini-batch at each step.
        self.unique_words = []
        self.unique_bigrams = []
        # Variances of sequences within a minibatch, calculated among all
        # timesteps, then averaged.
        self.variances = []
        self.h_state_vars = []
        # Discriminator metrics from the adversarial training.
        self.discriminator_losses = []
        self.discriminator_acc = []
        # Estimator MAEs at different positions.
        self.estimator_mae = [[] for _ in range(SEQ_LEN)]
        # Generated sequences.
        self.genseqs = []
        # BLEUs
        self.bleu = []

        self.load()  # This is no-op if no cache is to be found.

    def _tensors_to_floats(self):
        """
        Converts all history logs from tf.Tensor to float so that they are
        serializable.
        """
        self.rewards = list(map(float, self.rewards))
        self.reward_stds = list(map(float, self.reward_stds))
        self.grad_norms = list(map(float, self.grad_norms))
        self.unique_words = list(map(float, self.unique_words))
        self.unique_bigrams = list(map(float, self.unique_bigrams))
        self.variances = list(map(float, self.variances))
        self.h_state_vars = list(map(float, self.h_state_vars))
        self.discriminator_losses = list(map(float, self.discriminator_losses))
        self.discriminator_acc = list(map(float, self.discriminator_acc))
        self.bleu = list(map(float, self.bleu))
        for i in range(SEQ_LEN):
            for j in range(len(self.estimator_mae[i])):
                self.estimator_mae[i][j] = float(self.estimator_mae[i][j])
        for i in range(SEQ_LEN):
            for j in range(len(self.intermediate_rewards[i])):
                self.intermediate_rewards[i][j] = float(
                    self.intermediate_rewards[i][j])

    def log(self):
        """
        Logs the history on disk.
        """
        self._tensors_to_floats()
        dict_rep = ({
            'timestep': self.timestep,
            'rewards': self.rewards,
            'intermediate_rewards': self.intermediate_rewards,
            'reward_stds': self.reward_stds,
            'grad_norms': self.grad_norms,
            'unique_words': self.unique_words,
            'unique_bigrams': self.unique_bigrams,
            'variances': self.variances,
            'h_state_vars': self.h_state_vars,
            'discriminator_losses': self.discriminator_losses,
            'discriminator_acc': self.discriminator_acc,
            'estimator_mae': self.estimator_mae,
            'genseqs': self.genseqs,
            'bleu': self.bleu,
        })
        with open(self.LOG_PATH, 'w+') as fp:
            json.dump(dict_rep, fp)

    def load(self):
        """
        Loads the history from disk cache.
        """
        if not os.path.isfile(self.LOG_PATH):
            return
        with open(self.LOG_PATH, 'r') as fp:
            dict_rep = json.load(fp)
        self.timestep = dict_rep['timestep']
        self.rewards = dict_rep['rewards']
        self.intermediate_rewards = dict_rep['intermediate_rewards']
        self.reward_stds = dict_rep['reward_stds']
        self.grad_norms = dict_rep['grad_norms']
        self.unique_words = dict_rep['unique_words']
        self.unique_bigrams = dict_rep['unique_bigrams']
        self.variances = dict_rep['variances']
        self.h_state_vars = dict_rep['h_state_vars']
        self.discriminator_losses = dict_rep['discriminator_losses']
        self.discriminator_acc = dict_rep['discriminator_acc']
        self.estimator_mae = dict_rep['estimator_mae']
        self.genseqs = dict_rep['genseqs']
        self.bleu = dict_rep['bleu']

    def plot_stats(self):
        log_dir = 'seq_gan/cache/logs'
        os.makedirs(log_dir, exist_ok=True)
        # Plot rewards and discriminator accuracy.
        plt.plot(self.reward_stds, 'ro-', markersize=1,
                 label='reward std.dev')
        if self.bleu:
            plt.plot(self.bleu, 'yo-', markersize=1, label='BLEU')
        plt.plot(self.discriminator_acc, 'mo-', markersize=1,
                 label='discriminator acc')
        plt.plot(self.rewards, 'bo-', markersize=1, label='mean reward')
        plt.legend()
        plt.savefig(log_dir + '/rewards.png')
        plt.clf()
        # Plot gradient norms.
        plt.plot(self.grad_norms, 'bo-', markersize=1,
                 label='L2 gradient norms')
        plt.legend()
        plt.savefig(log_dir + '/grad_norms.png')
        plt.clf()
        # Plot unique words statistics.
        plt.plot(self.unique_words, 'bo-', markersize=1,
                 label='distinct words')
        plt.plot(self.unique_bigrams, 'ro-', markersize=1,
                 label='distinct bigrams')
        plt.legend()
        plt.savefig(log_dir + '/distinct.png')
        plt.clf()
        # Plot variance statistics.
        plt.plot(self.variances, 'go-', markersize=1,
                 label='output prob. dist. variance')
        plt.legend()
        plt.savefig(log_dir + '/variance.png')
        plt.clf()
        plt.plot(self.h_state_vars, 'mo-', markersize=1,
                 label='h state variance')
        plt.legend()
        plt.savefig(log_dir + '/h_state_variance.png')
        plt.clf()
        # Plot estimator errors.
        colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
        styles = ['o-', 'x--', '1:', '*-.']
        formats = []
        for c in colors:
            for s in styles:
                formats.append(c + s)
        for i in range(SEQ_LEN):
            if not self.estimator_mae[i]:
                continue
            plt.plot(self.estimator_mae[i], formats[i], markersize=1,
                     label='%d' % (i + 1))
        plt.title('Estimator MAE for _ tokens filled')
        plt.legend()
        plt.savefig(log_dir + '/estimator_mae.png')
        plt.clf()
        # Plot intermediate rewards.
        fig, axis = plt.subplots(4, 4, figsize=(20, 20))
        fig.suptitle('Intermediate rewards for prefixes of _ words')
        for i in range(SEQ_LEN):
            axis[i // 4, i % 4].plot(self.intermediate_rewards[i], formats[i],
                                     markersize=1)
            axis[i // 4, i % 4].set_title(str(i+1))
        fig.savefig(log_dir + '/intermediate_rewards.png')
        plt.clf()
