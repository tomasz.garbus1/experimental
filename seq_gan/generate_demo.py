"""
DEMO
Generates samples from the SeqGAN generator.
"""
import argparse

from tqdm import tqdm

from common.data_helper import DataHelper
from common.utils import load_tokenizer
from seq_gan.constants import SEQ_LEN, EMBEDDING_DIM, LSTM_UNITS
from seq_gan.generators.basic_lstm_sequence_output import BasicLSTMSequenceOutput

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='generator demo')
    parser.add_argument('--nsamples', type=int, action='store', default=1)
    parser.add_argument('--prefix', type=str, action='store', default='')
    parser.add_argument('--length', type=int, action='store', default=32)
    parser.add_argument('--pre', action='store_true', default=False,
                        help='use model from pretraining only')
    args = parser.parse_args()

    tokenizer = load_tokenizer('seq_gan/cache/tokenizer')
    assert tokenizer is not None
    dhelper = DataHelper(tokenizer, sequence_length=SEQ_LEN, val_split=0.01,
                         pad_sequences=False)
    generator = BasicLSTMSequenceOutput(embedding_dim=EMBEDDING_DIM,
                                        state_size=LSTM_UNITS, dhelper=dhelper)
    if args.pre:
        generator.load_from_disk_pre()
    else:
        generator.load_or_pretrain()
    for _ in tqdm(range(args.nsamples)):
        text = generator.generate_text(prefix=args.prefix, length=args.length)
        print(text)
