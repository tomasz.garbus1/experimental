import os
import matplotlib.pyplot as plt

import numpy as np
import tensorflow as tf

from common.data_helper import DataHelper
from seq_gan.constants import *
from seq_gan.discriminators.base_discriminator import Discriminator
from seq_gan.generators.base_generator import Generator


class CNNDiscriminator(Discriminator):
    def __init__(self, dhelper: DataHelper, generator: Generator):
        super().__init__(dhelper, generator)

        self.embedding = tf.keras.layers.Embedding(input_dim=VOCAB_SIZE+1,
                                                   output_dim=EMBEDDING_DIM,
                                                   input_length=SEQ_LEN)

        self.conv_layers = [
            tf.keras.layers.Conv1D(filters=spec[1], kernel_size=spec[0],
                                   padding='same')
            for spec in CONV_LAYERS
        ]
        self.dropout = tf.keras.layers.Dropout(0.3)
        self.flatten = tf.keras.layers.Flatten()
        self.out = tf.keras.layers.Dense(1, activation='sigmoid')
        self.keras_model = tf.keras.models.Sequential([self.embedding] +
                                                      self.conv_layers + [
                                                          self.flatten,
                                                          self.dropout,
                                                          self.out
                                                      ])
        self.keras_model.compile(optimizer='Adam', loss='binary_crossentropy',
                                 metrics=['acc'])
        self.keras_model.summary()

        self.pre_weights_path = 'seq_gan/cache/cnn_discriminator_keras_model_pre.h5'
        self.adv_weights_path = 'seq_gan/cache/cnn_discriminator_keras_model_adv.h5'

    def predict(self, seq: tf.Tensor) -> tf.Tensor:
        return self.keras_model(seq)

    def train_on_batch(self, x: tf.Tensor, y: tf.Tensor):
        return self.keras_model.train_on_batch(x, y)

    def load_from_disk_pre(self):
        self.keras_model.load_weights(self.pre_weights_path)

    def load_from_disk_adv(self):
        self.keras_model.load_weights(self.adv_weights_path)

    def save_to_disk_pre(self):
        self.keras_model.save_weights(self.pre_weights_path)

    def save_to_disk_adv(self, iter_num):
        self.keras_model.save_weights(self.adv_weights_path)

    def _log_history(self, history):
        fmts = ['b', 'g', 'r', 'c']
        for fmt, metric in zip(fmts, history.params['metrics']):
            plt.plot(history.history[metric], fmt, label=metric)
        plt.legend()
        plt.title('CNN discriminator pretraining')
        plt.savefig('seq_gan/cache/logs/cnn_discriminator.svg')

    def pretrain(self):
        print("Pretraining discriminator")
        if os.path.exists(self.adv_weights_path):
            print("Loading weights (adv)")
            self.load_from_disk_adv()
        elif os.path.exists(self.pre_weights_path):
            print("Loading weights (pre)")
            self.load_from_disk_pre()
        else:
            xs, ys = self._get_pretraining_set(limit=100)
#            xns, yns = self._get_negative_bogus_samples()
#            xs = np.concatenate([xs, xns])
#            ys = np.concatenate([ys, yns])
            xs, ys = self._shuffle_set(xs, ys)
            history = self.keras_model.fit(xs, ys, batch_size=BATCH_SIZE_PRE,
                                           epochs=10, validation_split=0.05)
            self._log_history(history)
            self.save_to_disk_pre()
