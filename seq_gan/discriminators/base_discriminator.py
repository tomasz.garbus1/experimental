import os

import numpy as np
import tensorflow as tf
from typing import Tuple

from tqdm import tqdm

from common.data_provider import DataProvider
from seq_gan.constants import VOCAB_SIZE, SEQ_LEN, BATCH_SIZE_PRE
from seq_gan.generators.base_generator import Generator
from common.data_helper import DataHelper


class Discriminator:
    def __init__(self, dhelper: DataHelper, generator: Generator):
        self.dhelper = dhelper
        self.generator = generator

    def predict(self, seq: tf.Tensor) -> tf.Tensor:
        raise NotImplementedError()

    def train_on_batch(self, x: tf.Tensor, y: tf.Tensor) -> Tuple[float, float]:
        """
        Returns two values - loss and accuracy.
        """
        raise NotImplementedError()

    def pretrain(self):
        raise NotImplementedError()

    def load_from_disk_pre(self):
        raise NotImplementedError()

    def load_from_disk_adv(self):
        raise NotImplementedError()

    def save_to_disk_pre(self):
        raise NotImplementedError()

    def save_to_disk_adv(self, iter_num):
        raise NotImplementedError()

    def _shuffle_set(self, xs, ys) -> Tuple[np.ndarray, np.ndarray]:
        zipped = list(zip(xs, ys))
        np.random.shuffle(zipped)
        xs, ys = zip(*zipped)
        xs = np.array(xs)
        ys = np.array(ys)
        return xs, ys

    def _get_negative_bogus_samples(self, num_samples=500000):
        print("Preparing negative bogus samples")
        x = np.random.randint(0, VOCAB_SIZE + 1, (num_samples, SEQ_LEN))
        y = [0] * num_samples
        y = np.array(y)
        return x, y

    def _get_pretraining_set(self, limit=1000) -> Tuple[tf.Tensor, tf.Tensor]:
        """
        :param limit: Number of sequences to generate. The same number of real
                      sequences will be added to pretraining set.
        :return: A tuple (samples, ground truths).
        """
        xs_path = 'seq_gan/cache/discriminator_pretrain_xs.npy'
        ys_path = 'seq_gan/cache/discriminator_pretrain_ys.npy'
        if os.path.isfile(xs_path) and os.path.isfile(ys_path):
            print("Loading data from cache")
            xs = np.load(xs_path)
            ys = np.load(ys_path)
        else:
            print("Preparing data")
            xs = []
            ys = []
            print("Real data")
            data_provider = DataProvider(self.dhelper, batch_size=BATCH_SIZE_PRE)
            num_samples_per_class = min(len(data_provider), limit)
            if limit == -1:
                num_samples_per_class = len(data_provider)
            for _ in tqdm(range(num_samples_per_class)):
                x, y = data_provider[_]
                xs.append(x)
                ys += [1] * len(x)
            if limit != -1:
                print("Generated data")
                for _ in tqdm(range(num_samples_per_class)):
                    x = self.generator.generate_sequences(BATCH_SIZE_PRE)[0]
                    xs.append(x)
                    ys += [0] * BATCH_SIZE_PRE
            xs = np.concatenate(xs)
            ys = np.array(ys)
            xs, ys = self._shuffle_set(xs, ys)
            np.save(xs_path, xs)
            np.save(ys_path, ys)
        return xs, ys
