"""
A demo script which takes an article, removes all entities and prompts the user
to provide alternative entity names, then displays same article with replaced
entities.
"""
import os
import re
from typing import List

from tqdm import tqdm
from trans import trans

from ner.highlighter import CAT_TEAM, CAT_PLAYER, CAT_COUNTRY, CAT_LEAGUE, CAT_STADIUM, CAT_MANAGER, Highlighter


def count_cat(article: str, cat: str):
    ret = 0
    for i in range(20):
        if ('{%s:%d}' % (cat, i)) in article:
            ret = i + 1
    return ret


def fill(article: str, teams: List[str], players: List[str], managers: List[str],
         stadiums: List[str], leagues: List[str], countries: List[str]):
    for i in range(count_cat(article, CAT_TEAM)):
        article = re.sub('{%s:%d}' % (CAT_TEAM, i), teams[i], article)
    for i in range(count_cat(article, CAT_PLAYER)):
        article = re.sub('{%s:%d}' % (CAT_PLAYER, i), players[i], article)
    for i in range(count_cat(article, CAT_MANAGER)):
        article = re.sub('{%s:%d}' % (CAT_MANAGER, i), managers[i], article)
    for i in range(count_cat(article, CAT_STADIUM)):
        article = re.sub('{%s:%d}' % (CAT_STADIUM, i), stadiums[i], article)
    for i in range(count_cat(article, CAT_LEAGUE)):
        article = re.sub('{%s:%d}' % (CAT_LEAGUE, i), leagues[i], article)
    for i in range(count_cat(article, CAT_COUNTRY)):
        article = re.sub('{%s:%d}' % (CAT_COUNTRY, i), countries[i], article)
    return article


def read_n_strs(n: int, cat: str):
    if n > 0:
        print('Please provide %d %ss' % (n, cat))
    return [input().strip() for _ in range(n)]


def demo(hl: Highlighter):
    for dir in tqdm(os.listdir('../data/bbcsport')):
        if not os.path.isdir(os.path.join('../data/bbcsport', dir)):
            continue
        for fname in os.listdir(os.path.join('../data/bbcsport', dir)):
            try:
                fpath = os.path.join('../data/bbcsport', dir, fname)
                with open(fpath, 'r') as file:
                    content = file.read()
                content = trans(content)
                content = hl.placehold_with_id(content)
                teams = read_n_strs(count_cat(content, CAT_TEAM),
                                    CAT_TEAM)
                players = read_n_strs(count_cat(content, CAT_PLAYER),
                                      CAT_PLAYER)
                managers = read_n_strs(count_cat(content, CAT_MANAGER),
                                       CAT_MANAGER)
                stadiums = read_n_strs(count_cat(content, CAT_STADIUM),
                                       CAT_STADIUM)
                leagues = read_n_strs(count_cat(content, CAT_LEAGUE),
                                      CAT_LEAGUE)
                countries = read_n_strs(count_cat(content, CAT_COUNTRY),
                                        CAT_COUNTRY)
                content = fill(content, teams=teams, players=players,
                               managers=managers, stadiums=stadiums,
                               leagues=leagues, countries=countries)
                print(content)
            except KeyboardInterrupt:
                continue


if __name__ == '__main__':
    hl = Highlighter()
    demo(hl)
