"""
A Highlighter class which reads a given article, locates all entities and
highlights or replaces their occurrences.

The ubiquitous get_code function has the following signature:
get_code(w, s, v) -> r, where:
w: a tuple of ints - position in text
s: a string occurring in the article on position [b, e)
v: a tuple (resolved value: str, category: str in CATEGORIES)

r: return value - a string to replace |s| in text
"""
import os
from typing import Callable
from trans import trans
import pygtrie
from tqdm import tqdm

# Categories of recognised entities.
CAT_TEAM = 'team'
CAT_PLAYER = 'player'
CAT_STADIUM = 'stadium'
CAT_LEAGUE = 'league'
CAT_COUNTRY = 'country'
CAT_MANAGER = 'manager'
# Note that the categories should be ordered from least to most important
# (when desambiguating).
CATEGORIES = [CAT_LEAGUE, CAT_STADIUM, CAT_MANAGER, CAT_PLAYER, CAT_TEAM,
              CAT_COUNTRY]
# Default color code.
DEFAULT_CODE = '\033[0;37;40m'
# Color codes for each category.
TEAM_CODE = '\033[4;32;40m'
PLAYER_CODE = '\033[4;31;40m'
STADIUM_CODE = '\033[4;33;40m'
LEAGUE_CODE = '\033[4;34;40m'
COUNTRY_CODE = '\033[4;35;40m'
MANAGER_CODE = '\033[4;36;40m'
CODES = {
    CAT_TEAM: TEAM_CODE,
    CAT_PLAYER: PLAYER_CODE,
    CAT_STADIUM: STADIUM_CODE,
    CAT_LEAGUE: LEAGUE_CODE,
    CAT_COUNTRY: COUNTRY_CODE,
    CAT_MANAGER: MANAGER_CODE,
}


def _read_entity_names(fname: str, trie: pygtrie.Trie, category: str):
    """
    Reads a file given by its path `fname`, in compliance with its format
    determined by `category`. The values (name variants) are added to `trie`.
    """
    print("Reading file %s" % fname)
    with open(fname, 'r') as file:
        content = file.read()
        for line in tqdm(content.split('\n')):
            # For now we're only interested in the first column of the CSV -
            # the name variants. We don't care about player's club etc.
            if ',' in line:
                line = line.split(',')[0]
            # List field is separated by semi-colons.
            elems = list(map(trans, line.split(';')))
            # We assume the first variant to be the most "verbatim".
            val = elems[0]
            for elem in elems:
                if elem not in trie:
                    trie[elem] = []
                trie[elem].append((val, category))


class _PlaceholdMemory:
    def __init__(self):
        self.mapping = {}

    def _count_cat(self, cat: str):
        return len(list(filter(lambda v: v[1] == cat, self.mapping.keys())))

    def code_fun(self, _, s, v):
        if v not in self.mapping:
            self.mapping[v] = '{%s:%d}' % (v[1], self._count_cat(v[1]))
        return self.mapping[v]


class _PromptMemory:
    def __init__(self):
        self.entities = {}

    def code_fun(self, _, s, v):
        name = v[0]
        cat = v[1]
        if cat not in self.entities:
            self.entities[cat] = []
        if name not in self.entities[cat]:
            self.entities[cat].append(name)
        return s

    def build_prompt(self):
        res = ''
        for cat in CATEGORIES:
            if cat in self.entities:
                res += cat + ':' + ','.join(self.entities[cat]) + '\n'
        return res


class Highlighter:
    def __init__(self, language='en'):
        self.trie = pygtrie.Trie()
        assert language in ['en', 'pl']
        for cat in CATEGORIES:
            for f in os.listdir('ner/entities/%s/%s' % (language, cat)):
                _read_entity_names('ner/entities/%s/%s/%s' % (language, cat, f),
                                   self.trie, cat)

    @staticmethod
    def run(article, get_code: Callable, trie: pygtrie.Trie):
        ret = ""
        i = 0
        mentioned_entities = set()
        resolutions = dict()
        while i < len(article):
            max_found = -1
            j = i
            while j < len(article) and (
                    trie.has_subtrie(article[i:j + 1]) or article[i:j + 1] in trie):
                if article[i:j + 1] in trie and (
                        j + 1 == len(article) or not article[j + 1].isalpha()):
                    max_found = j + 1
                j += 1
            if max_found != -1:
                entity_val = None
                if article[i:max_found] in resolutions:
                    entity_val = resolutions[article[i:max_found]]
                else:
                    for v in trie[article[i:max_found]]:
                        entity_val = v
                        if v in mentioned_entities:
                            break
                ret += get_code((i, max_found), article[i:max_found], entity_val)
                mentioned_entities.add(entity_val)
                resolutions[article[i:max_found]] = entity_val
                i = max_found
            else:
                ret += article[i]
                i += 1
        return ret

    def highlight(self, article: str) -> str:
        """
        Highlights player and team in the article using linux prompt
        colors.
        """

        def code_fun(_, s, v):
            return (CODES[v[1]] + s + (' (' + v[0] + ')' if s != v[0] else '')
                    + DEFAULT_CODE)

        return self.run(article, code_fun, self.trie)

    def placehold(self, article: str) -> str:
        """
        Replaces player, team, stadium and league with placeholders.
        """
        return self.run(article, lambda _, s, v: '{' + v[1] + '}', self.trie)

    def placehold_with_id(self, article: str) -> str:
        """
        Replaces player, team, stadium and league with placeholders,
        identifying multiple occurrences of the same entity.
        """
        plm = _PlaceholdMemory()
        return self.run(article, plm.code_fun, self.trie)

    def append_prompt(self, article: str) -> str:
        """
        Appends a prefix to the article, listing all occurring entities in the
        order of occurrence.
        """
        prm = _PromptMemory()
        article = self.run(article, prm.code_fun, self.trie)
        article = prm.build_prompt() + article
        return article


class Annotator(Highlighter):
    """
    Annotates the dataset for various purposes such as SpaCy preprocessing.
    Supports the following modes:
    * 'default' -- self.data contains tuples (article, set of resolved entities)
    * 'spacy' -- articles are annotated in SpaCy format i.e. entites are listed
                 as tuples (start index, end index + 1, category).
    """
    def __init__(self, mode: str = 'default', language='en'):
        super(Annotator, self).__init__(language)
        assert mode in ['default', 'spacy']
        self.data = []
        self.mode = mode

    def add_art(self, article: str):
        if self.mode == 'default':
            entities = set()

            def _get_code_default(w, s, v):
                entities.add(v)
                return s

            self.run(article, _get_code_default, self.trie)
            self.data.append((article, entities))
            return entities
        if self.mode == 'spacy':
            entities = []

            def _get_code_spacy(w, s, v):
                entities.append((w[0], w[1], v[1]))
                return s

            self.run(article, _get_code_spacy, self.trie)
            self.data.append((article, {'entities': entities}))
            return entities

    def clean(self):
        self.data = []
