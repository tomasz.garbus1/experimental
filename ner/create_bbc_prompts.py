"""
A script which iterates all BBC articles, finds occurring entities and appends
a prefix to each article for example:

team:Manchester United,Arsenal
player:Christiano Ronaldo,Kaka
stadium:Old Trafford
"""
import argparse
import os

from tqdm import tqdm
from trans import trans

from ner.highlighter import Highlighter


def create_bbc_prompts(hl: Highlighter, src):
    for dir in tqdm(os.listdir('../data/%s' % src)):
        if not os.path.isdir(os.path.join('../data/%s' % src, dir)):
            continue
        for fname in os.listdir(os.path.join('../data/%s' % src, dir)):
            fpath = os.path.join('../data/%s' % src, dir, fname)
            with open(fpath, 'r') as file:
                content = file.read()
            content = trans(content)
            content = hl.append_prompt(content)
            outdir = os.path.join('../data/%s_prompts' % src, dir)
            os.makedirs(outdir, exist_ok=True)
            outpath = os.path.join(outdir, fname)
            with open(outpath, 'w+') as file:
                file.write(content)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', action='store', type=str, default='bbcsport')
    parser.add_argument('-l', action='store', type=str, default='en')
    args = parser.parse_args()
    hl = Highlighter(language=args.l)
    create_bbc_prompts(hl, args.i)
    pass
