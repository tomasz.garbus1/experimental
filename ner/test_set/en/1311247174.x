::Kettering Town land ex-Leicester City man Aman Verma::
@1311247174
url: https://www.bbc.com/sport/football/14235396
Kettering Town have signed midfielder Aman Verma on a one-year deal following his release from Leicester City.
The 23-year-old had loan spells at Kidderminster and Darlington last season, helping the Quakers to FA Trophy success.
He made just one appearance for Harriers, but featured 26 times in the Blue Square Bet Premier for Darlington and was offered a deal to stay there.
But he has joined their league rivals Kettering following a trial.
His contract includes the option of a further year.
Verma, who came through the Leicester youth ranks, returned to the Foxes in late 2008 after spells with Bedworth and Redditch.
He has also had loan experience with Histon and Crewe.