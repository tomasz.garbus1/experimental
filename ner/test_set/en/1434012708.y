::{country:Rep of Ireland} v {country:Scotland}: {player:Brady} says {player:McGeady} can trouble Scots::
@1434012708
url: https://www.bbc.com/sport/football/33092399
{player:Robbie Brady} believes {player:Aiden McGeady} can make a major impact for the {country:Republic of Ireland} against his native {country:Scotland} in Saturday's Euro 2016 qualifier.
{player:McGeady}, 29, was incessantly booed by {player:Scotland} fans last November in Glasgow but {player:Brady} says the {manager:Everton} winger can respond in the best possible fashion.
Despite Sunday's dire friendly draw against {country:England}, {player:Brady} was impressed with {player:McGeady}'s efforts.
"He played very well on Sunday and he is raring to go," said {player:Brady}, 23. 
"Hopefully if he is asked to play, he puts in a good performance and with his ability if he turns it on, we will be in good stead."
{team:Hull City}'s {player:Brady} lined up with Paisley-born {player:McGeady} on the {country:Republic}'s left flank in the dismal friendly stalemate against {country:England}.
That game served as the {country:Republic}'s main warm-up contest for Saturday's vital qualifier which the {country:Republic} probably must win to keep their qualification hopes alive following the 1-0 defeat by the Scots last autumn.
A winger by trade, {player:Brady} has been asked to line up at left-back by manager {manager:Martin O'Neill} in recent games, and the cover provided for him by the likes of {player:McGeady} has proved invaluable.
{player:Brady} added: "It's definitely important. At this level when you are playing, you are going to be playing against top players and you are going to find yourself at times where you need to get that right and it can be only a split-second before someone can open you up.
"I thought we worked it well on Sunday and hopefully we can do the same."
{player:Brady} effectively marked {country:England} winger {player:Raheem Sterling} out of the game on Sunday, but freely admits he is still learning about the defensive flank role.
"There are different times in the game when you are able to get forward, and different times when you have to put the reins on a little bit. It's just getting that balance right.
 "But if I do get an opportunity to go forward at the weekend, I'll be attacking it with everything I have."