::{team:Newport County} 2-0 {team:Carlisle United}::
@1478962628
url: https://www.bbc.com/sport/football/37881276
{team:Carlisle} were beaten for the first time this season as bottom of the table {team:Newport} shocked them at {stadium:Rodney Parade}.
{player:Josh Sheehan}'s early strike and a smart finish from {player:Rhys Healey} after a second half counter-attack secured the win for {manager:Graham Westley}'s side.
{team:Carlisle}'s had been on a winning run of eight successive games, while {team:the Exiles} are now unbeaten in five.
{team:Newport} remain bottom of {league:League Two} but are now a point behind {team:Leyton Orient} while {manager:Keith Curle}'s side remain second.
{team:Newport} manager {manager:Graham Westley} told BBC Radio {country:Wales} Sport: "The players have worked so hard and they looked the real deal in {league:League Two} today.
"We are an improving team and the players deserve enormous credit."
Match ends, {team:Newport County} 2, {team:Carlisle United} 0.
Second Half ends, {team:Newport County} 2, {team:Carlisle United} 0.
Attempt missed. {player:Jennison Myrie-Williams} ({team:Newport County}) right footed shot from the right side of the box misses to the left.
{player:Danny Grainger} ({team:Carlisle United}) wins a free kick on the right wing.
Foul by {player:Jennison Myrie-Williams} ({team:Newport County}).
{player:Jennison Myrie-Williams} ({team:Newport County}) wins a free kick in the attacking half.
Foul by {player:Charlie Wyke} ({team:Carlisle United}).
Foul by {player:Reggie Lambe} ({team:Carlisle United}).
{player:Dan Butler} ({team:Newport County}) wins a free kick on the right wing.
Substitution, {team:Newport County}. {player:Finley Wood} replaces {player:Josh Sheehan}.
Attempt missed. {player:Tom Miller} ({team:Carlisle United}) right footed shot from outside the box is too high.
Corner,  {team:Newport County}. Conceded by {player:Mark Gillespie}.
Attempt saved. {player:Tom Owen-Evans} ({team:Newport County}) right footed shot from outside the box is saved in the bottom left corner.
Attempt saved. {player:Charlie Wyke} ({team:Carlisle United}) header from the centre of the box is saved in the centre of the goal.
Attempt blocked. {player:Tom Miller} ({team:Carlisle United}) right footed shot from outside the box is blocked.
{player:Danny Grainger} ({team:Carlisle United}) wins a free kick in the defensive half.
Foul by {player:Sean Rigg} ({team:Newport County}).
Substitution, {team:Carlisle United}. {player:Derek Asamoah} replaces {player:Luke Joyce}.
Attempt blocked. {player:Jennison Myrie-Williams} ({team:Newport County}) right footed shot from the right side of the six yard box is blocked.
Attempt missed. {player:Luke Joyce} ({team:Carlisle United}) right footed shot from the centre of the box is close, but misses to the right.
Corner,  {team:Carlisle United}. Conceded by {player:Dan Butler}.
{player:Macaulay Gillesphey} ({team:Carlisle United}) wins a free kick in the attacking half.
Foul by {player:Rhys Healey} ({team:Newport County}).
Substitution, {team:Newport County}. {player:Jennison Myrie-Williams} replaces {player:Josh O'Hanlon}.
Goal!  {team:Newport County} 2, {team:Carlisle United} 0. {player:Rhys Healey} ({team:Newport County}) right footed shot from the centre of the box to the bottom left corner. Assisted by {player:Dan Butler}.
Attempt blocked. {player:Danny Grainger} ({team:Carlisle United}) left footed shot from the right side of the box is blocked.
Corner,  {team:Carlisle United}. Conceded by {player:Scot Bennett}.
{player:Macaulay Gillesphey} ({team:Carlisle United}) wins a free kick in the attacking half.
{player:Josh O'Hanlon} ({team:Newport County}) is shown the yellow card.
Foul by {player:Ben Tozer} ({team:Newport County}).
{player:Tom Miller} ({team:Carlisle United}) wins a free kick in the defensive half.
Foul by {player:Josh O'Hanlon} ({team:Newport County}).
Attempt blocked. {player:Josh Sheehan} ({team:Newport County}) right footed shot from outside the box is blocked.
Substitution, {team:Newport County}. {player:Tom Owen-Evans} replaces {player:Kyle Cameron}.
{player:Nicky Adams} ({team:Carlisle United}) wins a free kick in the defensive half.
{player:Darren Jones} ({team:Newport County}) is shown the yellow card for a bad foul.
Foul by {player:Darren Jones} ({team:Newport County}).
Attempt saved. {player:Luke Joyce} ({team:Carlisle United}) right footed shot from the centre of the box is saved in the bottom right corner.
Attempt blocked. {player:Josh Sheehan} ({team:Newport County}) right footed shot from outside the box is blocked.
Foul by {player:Tom Miller} ({team:Carlisle United}).