::'Sickened' Alan Shearer says Alan Pardew is being undermined::
@1371714495
url: https://www.bbc.com/sport/football/22982306
Newcastle manager Alan Pardew has been undermined by Joe Kinnear's appointment as director of football, says the club's record scorer Alan Shearer.
Pardew now has to report to Kinnear as part of a controversial restructuring.
"While other clubs are all plotting and planning for next season, Newcastle have a manager who has been totally undermined," Shearer told The Sun.
The Match of the Day pundit added: "People are laughing at the football club I support. And that sickens me."
Shearer scored 206 goals in 404 games during a 10-year spell at Newcastle.
He was also manager for an eight-game spell in 2009, taking over when previous incumbent Kinnear was recovering from a heart operation, but was unable to save them from relegation to the Championship.
Shearer does not expect Pardew to resign but feels he will find it difficult to work under Kinnear.
Source: Newcastle United official website
"It's no way to treat a man who 12 months ago was enjoying the fact he had been voted Manager of the Year for guiding Newcastle to fifth place in the Premier League," Shearer said.
"His silence on the appointment of Kinnear himself over recent days said it all. But what could he say? 
"He is in such an intolerable position. He can't come out and slaughter his employers but at the same time he can't agree with a situation which no manager would find comfortable.
"I ask one question of Joe Kinnear: Would you stand for it, if you were manager? 
"Someone else coming out in the national media announcing they are the Director of Football before even the club have made it official. 
"Someone else basically taking control of who comes in and who goes out."
Kinnear, 66, was named as Newcastle's interim manager in September 2008 following Kevin Keegan's resignation. He was forced to step down after suffering heart problems in February 2009.
He recently revealed his new role during a radio interview, during which he got several players' names wrong, before the club confirmed the announcement.
"As for that announcement, what are the players to think when he can't even get their names right," Shearer added. "It's all a PR disaster."  
Shearer also took issue with Kinnear's claim that Newcastle would not have been relegated four years ago if he had remained in charge.
He added: "Over the last few days he has banged on about how Newcastle would never have gone down had he not had his health problems and stayed on in the job to the end of the 2008-09 season. 
"The implication being that it was first (caretaker) Chris Hughton and then my fault for the club being relegated. 
"I had eight games as manager at the end of the season, won one, drew two and lost the rest. So, yes, I take my share of responsibility. 
"But Kinnear had 19 league games in charge that season and won just four, taking 20 points. 
"Chris Hughton could not arrest the slide in his brief role as caretaker, and nor could I. 
"Given the downtrodden, disjointed group of players I took on, it would have been something special if I had. 
"So it's about time Kinnear took some responsibility for what happened as well. It was a real eye-opener when I did take charge for those final games. The team wasn't just a mess, the club was a mess."