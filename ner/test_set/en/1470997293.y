::{player:Odion Ighalo}: Nigerian striker 'honoured' by new {team:Watford} deal::
@1470997293
url: https://www.bbc.com/sport/football/37059579
{team:Watford} striker {player:Odion Ighalo} has revealed he had no hesitation in signing a new five-year contract.
The Nigeria international, 27, told BBC Sport: "I feel at home here and once the opportunity to extend was finalised, I signed it straight away.
"Playing with this incredible squad and in front of our amazing fans gives me a special excitement and buzz.
"I can see the direction the club is going and I feel honoured to be a part of the long-term plan."
{player:The Super Eagle} joined {team:the Hornets} from {team:Udinese} in July 2014 and his 20 goals helped them win promotion to the {league:Premier League} in 2014-15 season.
He scored 17 goals in 42 games last season as the {stadium:Vicarage Road} side finished 13th in the top flight and reached the {league:FA Cup} semi-final.
Since arriving at the club {player:Ighalo} has scored 37 times in 80 appearances.
{player:Ighalo}, who has played in {country:Norway}, {country:Italy} and {country:}Spain, has also established himself with his country, scoring three goals in nine international appearances.