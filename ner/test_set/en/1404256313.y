::{country:Belgium} 2-1 {country:USA}::
@1404256313
url: https://www.bbc.com/sport/football/27990605

                            
                        
Story of the match:
{country:Belgium}'s golden generation earned the chance to live up to their billing as they reached a first {league:World Cup} quarter-final for 28 years with an extra-time win over the {country:USA} in {country:Salvador}. 
This was another brilliant and breathless occasion. {country:Belgium} walk away deserving winners but only after a performance of incredible spirit and bravery from the {country:US}. 
As it was, {player:Kevin de Bruyne} scored the goal that finally broke {country:USA} resistance in the second minute of extra time at {stadium:Arena Fonte Nova}. Substitute {player:Romelu Lukaku} fired in a second 11 minutes later to apparently end the contest. 
"It was like watching a {league:Premier League} game. It was unbelievable.
"You have to start taking {country:Belgium} seriously. The manager is making great substitutions and from the bench they have better players than any other team to change a game."
The {country:US} would not go quietly, though. From nowhere they rose up to find hope and with it a goal from substitute {player:Julian Green}, who volleyed brilliantly past {player:Thibaut Courtois}. {player:Jermaine Jones} and {player:Clint Dempsey} both had chances to take the match to penalties but neither was able to take the chance to extend the {country:USA}'s dream. 
That {manager:Jurgen Klinsmann}'s men survived so long owed everything to an outstanding goalkeeping performance by {player:Tim Howard}, who made a record number of saves in a {league:World Cup} match. In front of him, his team-mates were simply outclassed. 
{country:Belgium} will now play {country:Argentina} in a mouth-watering quarter-final in Brasilia on Saturday. On this evidence, they will take some beating.
This was a battle of flair against function. {country:Belgium}, with their jet-heeled forwards and unquestioned technical superiority, against the organisation and energy of the {country:United States}. 
{player:Eden Hazard} and {player:Divock Origi} were a pair of nuisances and their willingness to run at their opponents, and sheer athletic ability, caused the {country:US} problems throughout the 120 minutes. 
{player:Kevin De Bruyne} scuffed two chances wide and sent a third straight at {player:Howard} as {manager:Jurgen Klinsmann}'s side too often left themselves open to the counter-attack - {player:Hazard}, {player:De Bruyne}, {player:Origi} and {player:Jan Vertonghen} rampaged into wide open spaces in the {country:US} half. 

                            
                        
{player:DaMarcus Beasley}'s vital interception then denied {player:Marouane Felliani} at the back post as {country:Belgium} took the game by the scruff of the neck. The {country:US} were reliant on {player:Michael Bradley} and {player:Clint Dempsey} to create their best moments and the pair combined to test {player:Courtois} just before half-time. 
If the {country:US} had been second best in the first half, they hung on in the second. {country:Belgium} monopolised possession as the {country:US} dropped deeper and deeper.
{player:Toby Alderweireld} was finding space down the right from where he delivered a cross that {player:Origi} headed onto the top of Howard's crossbar. {player:Hazard} went close with flick soon after, and the introduction of substitute {player:Kevin Mirallas} only reinforced {country:Belgium}'s dominance. He scythed through the {country:US} defence, the ball running to {player:Origi}, whose shot was beaten away by {player:Howard}. 
Moments later, {player:Howard} was sprawling low to flick a shot away from {player:Mirallas}, his {team:Everton} team-mate, before {player:Hazard} drew another brilliant save. 
The veteran keeper was at it again soon after, tipping {player:Origi}'s fierce shot over. 
"On Twitter #ThingsTimHowardCanSave became a trending topic. Money on your car insurance was just one of the suggestions. '{player:Tim Howard} remains a God,' Tom Hanks would tweet. His voice was echoed by many more. 
"This was the end of a summer of soccer in the {country:United States}, a defeat that gave the many new converts to the game a taste of the other side of this sport, the immense and, at times, the cruel sense of disappointment that so many countries have experienced at some point."
Read the rest of how {country:Belgium} ended the American dream here
Somehow, the {country:US} got over the line and into extra time - but the respite was brief. 
The introduction of {player:Lukaku} had an immediate impact. He burst down the right and although his cross-shot was blocked by {player:Matt Besler}, {player:De Bruyne} pounced on the loose ball, found a yard of space and shot unerringly into the far corner. 
{player:De Bruyne} then turned provider to find {team:Chelsea} striker, who turned the ball into the net with a brilliant finish at the near post. 
With the {country:US} apparently dead and buried, there was a twist in the tale. {player:Green}, on as a substitute, latched onto {player:Bradley}'s floated pass and fired a stunning volley beyond {player:Courtois} with practically his first touch of the match. 
As late as it was, the {country:US} found new purpose. {player:Jones} poked just wide, before {player:Dempsey} found himself in possession eight yards out after a clever free-kick. The stadium held its breath but the American was denied by {player:Courtois} once more. 
{country:Belgium} coach {manager:Marc Wilmots}: "We had a lot of opportunities, and we dominated the game, and in the end it was well deserved even if it was scary with the {country:US} goal.
"{player:Lukaku} had been much criticised, and I told him, 'do not worry - the second round might be your day', and that's what happened.
 "{player:Tom Howard} had a fantastic match but we need to forget about this match and focus on Argentina.
 "There shouldn't be any more worries from the media about us not playing football."
Match ends, {country:Belgium} 2, {country:USA} 1.
Second Half Extra Time ends, {country:Belgium} 2, {country:USA} 1.
Hand ball by {player:Nacer Chadli} ({country:Belgium}).
Attempt missed. {player:DeAndre Yedlin} ({country:USA}) right footed shot from a difficult angle on the right is high and wide to the right. Assisted by {player:Julian Green} with a cross.
{player:Michael Bradley} ({country:USA}) wins a free kick in the attacking half.
Foul by {player:Marouane Fellaini} ({country:Belgium}).
{player:DaMarcus Beasley} ({country:USA}) wins a free kick in the defensive half.
Foul by {player:Kevin Mirallas} ({country:Belgium}).
Attempt missed. {player:Jermaine Jones} ({country:USA}) right footed shot from outside the box is high and wide to the right. Assisted by {player:Julian Green}.
{player:DaMarcus Beasley} ({country:USA}) wins a free kick in the defensive half.
Foul by {player:Kevin Mirallas} ({country:Belgium}).
Foul by {player:Omar GonzÃ¡lez} ({country:USA}).
{player:Axel Witsel} ({country:Belgium}) wins a free kick in the defensive half.
Attempt missed. {player:Jermaine Jones} ({country:USA}) header from the centre of the box misses to the left. Assisted by {player:Geoff Cameron} with a cross.
Attempt saved. {player:Clint Dempsey} ({country:USA}) left footed shot from the centre of the box is saved in the centre of the goal. Assisted by {player:Chris Wondolowski} with a through ball.
{player:Geoff Cameron} ({country:USA}) wins a free kick in the attacking half.
Foul by {player:Nacer Chadli} ({country:Belgium}).
Attempt blocked. {player:Jermaine Jones} ({country:USA}) left footed shot from outside the box is blocked. Assisted by {player:Michael Bradley}.
{player:Julian Green} ({country:USA}) wins a free kick on the left wing.
Foul by {player:Kevin Mirallas} ({country:Belgium}).
Corner,  {country:Belgium}. Conceded by {player:Tim Howard}.
Substitution, {country:Belgium}. {player:Nacer Chadli} replaces {player:Eden Hazard}.
Corner,  {country:Belgium}. Conceded by {player:Omar GonzÃ¡lez}.
Attempt saved. {player:Romelu Lukaku} ({country:Belgium}) left footed shot from the centre of the box is saved in the bottom right corner. Assisted by {player:Vincent Kompany}.
Attempt missed. {player:Jermaine Jones} ({country:USA}) right footed shot from the centre of the box is close, but misses to the right. Assisted by {player:Chris Wondolowski} with a headed pass.
Attempt missed. {player:Marouane Fellaini} ({country:Belgium}) right footed shot from the right side of the box is high and wide to the right. Assisted by {player:Toby Alderweireld}.
Goal!  {country:Belgium} 2, {country:USA} 1. {player:Julian Green} ({country:USA}) right footed shot from the centre of the box to the bottom right corner. Assisted by {player:Michael Bradley} with a through ball.
{player:DeAndre Yedlin} ({country:USA}) wins a free kick in the attacking half.
Foul by {player:Marouane Fellaini} ({country:Belgium}).
Second Half Extra Time begins {country:Belgium} 2, {country:USA} 0.
First Half Extra Time ends, {country:Belgium} 2, {country:USA} 0.
Substitution, {country:USA}. {player:Julian Green} replaces {player:Alejandro Bedoya}.
Goal!  {country:Belgium} 2, {country:USA} 0. {player:Romelu Lukaku} ({country:Belgium}) left footed shot from the centre of the box to the top left corner. Assisted by {player:Kevin De Bruyne} with a through ball following a fast break.
Attempt blocked. {player:Michael Bradley} ({country:USA}) right footed shot from outside the box is blocked. Assisted by {player:Jermaine Jones}.
Attempt saved. {player:Kevin Mirallas} ({country:Belgium}) right footed shot from the right side of the box is saved in the bottom left corner. Assisted by {player:Eden Hazard}.
Delay over. They are ready to continue.
Delay in match {player:Jermaine Jones} ({country:USA}) because of an injury.
Attempt saved. {player:Romelu Lukaku} ({country:Belgium}) left footed shot from the left side of the box is saved in the centre of the goal. Assisted by {player:Eden Hazard}.
Delay over. They are ready to continue.
Delay in match {player:Jermaine Jones} ({country:USA}) because of an injury.