::{team:Blackburn} striker {player:Nikola Kalinic} joins {team:Dnipro}::
@1313085935
url: https://www.bbc.com/sport/football/14496906
{team:Blackburn} striker {player:Nikola Kalinic} has joined Ukrainian side {team:Dnipro} for an undisclosed fee, {team:Rovers} have confirmed.
The 23-year-old {country:Croatia} international arrived at {stadium:Ewood Park} from {team:Hadjuk Split} in 2009 and scored seven goals in 33 appearances in the 2009-10 campaign.
But {player:Kalinic} stuck just eight times in 21 games in last season and slipped down boss {manager:Steve Kean}'s pecking order.
{team:Blackburn} recently boosted their attacking options with the signing of {player:David Goodwillie} from {team:Dundee United}.
Meanwhile, {manager:Kean} insists there has not been any offers for centre-back {player:Chris Samba} and he hopes that situation remains the same until the transfer window closes.

                            
                        
{team:Tottenham} and {team:Arsenal} have both been interested in the defender but with {team:Rovers} already having sold {player:Phil Jones} to {team:Manchester United}, {manager:Kean} does not want to lose another centre-back.
"{player:Chris} is only a few months into a five-year contract. There have been rumours every single week but we have not had any bids at all, not one, and long may that continue." he said.
"We know it's life without {player:Phil}, and if you lose somebody at the last minute it can be very difficult.
"We hope that won't happen, we hope that's the only centre-half we lose this season and that the phone doesn't ring and that {player:Chris} is happy with us."
{manager:Kean} also said that talks over a new deal with {country:Zimbabwe} striker {player:Benjani}, who is out of contract, were "not dead" yet.
