::{team:Dagenham & Redbridge} sign {team:Barnsley}'s {player:Toni Silva} on loan::
@1364386032
url: https://www.bbc.com/sport/football/21956048
{league:League Two} side {team:Dagenham & Redbridge} have signed {team:Barnsley} winger {player:Toni Silva} on loan until 23 April.
The 19-year-old joined {team:the Tykes} from {team:Liverpool} last summer.
{team:Daggers} interim manager {manager:Wayne Burnett} told the club website: "I would like to thank {team:Barnsley} manager {manager:David Flitcroft} for making this deal possible.
"The player was signed from {team:Liverpool} with a lot of promise. Unfortunately he has slightly lost his way but hopefully we can get him back to his best."
{player:Silva}, who has made one appearance for {team:Barnsley} since his move to Oakwell, had a loan spell at {team:Northampton Town} last season where he scored once in 15 appearances.