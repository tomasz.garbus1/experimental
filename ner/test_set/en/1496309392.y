::{league:Under-20 World Cup}: {country:England} beat {country:Costa Rica} 2-1 to progress to quarter-finals::
@1496309392
url: https://www.bbc.com/sport/football/40109029
{country:England} reached the quarter-finals of the {league:Under-20 World Cup} in {country:South Korea} with a last-16 victory over {country:Costa Rica}.
Everton striker {player:Ademola Lookman} fired {country:England} into a first-half lead before doubling the advantage after the break.
{country:Costa Rica}'s {player:Randall Leal} saw his late penalty saved by goalkeeper {player:Freddie Woodman} but tucked in the rebound.
{manager:Paul Simpson}'s side held on and remain unbeaten having topped their group and will play {country:Mexico} in the quarter-finals on Monday in Cheonan.
{country:Costa Rica} thought they had drawn level at 1-0 down but had a goal ruled out for offside by the video assistant referee.
{country:England}'s 19-year-old {player:Lookman} dominated the game, striking a free-kick against the crossbar in between two nifty finishes.
{team:Everton} team-mate {player:Jonjoe Kenny} drilled in a low cross for {player:Lookman}'s first, before fellow {team:Everton} forward {player:Dominic Calvert-Lewin} played the ball inside from the left flank and {player:Lookman} slotted the ball through the legs of keeper {player:Erick Pineda}.
{country:England} progressed to the quarter-finals of the tournament for the first time since 1993.
Their opening victory against {country:Argentina} ended a 17-match winless streak for {team:the Young Lions}.