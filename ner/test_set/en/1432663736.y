::{team:Gateshead} takeover completed by Consett-based business duo::
@1432663736
url: https://www.bbc.com/sport/football/32893832
{team:Gateshead} have been taken over by business owners Richard and Julie Bennett after they took over the total shareholding of chairman Graham Wood.
The Consett-based duo were the preferred bidder for Wood who, along with vice-chairman Brian Waites, will retain his post.
Progress can now be made on player contracts and the appointment of a new manager to replace Gary Mills.
"They are very successful business people," Wood said.
"The pair possess the ambition and the means to continue with the objective of returning {team:Gateshead} to the {league:Football League}.
"We all recognise the benefit of a seamless transition and consequently I have agreed to remain as chairman until such time as Richard feels ready to take over the role."
Wood, who took over at {team:Gateshead} in 2006, led the club to two promotions, a {stadium:Wembley} promotion final and brought in full-time status during his spell as owner.
However, the former {team:Sunderland} vice-chairman said it was an "appropriate time" to step down when he announced his intention to sell the club earlier this month.
Co-owner Richard Bennett said: "We're delighted to have been given the opportunity to take over the club and we'll be looking to build on the solid foundation which Graham Wood, Brian Waites and the support staff have created over the past nine years."