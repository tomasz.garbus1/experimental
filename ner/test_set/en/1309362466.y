::{team:Equatorial Guinea} lose to {team:Norway} at {league:Women's World Cup}::
@1309362466
url: https://www.bbc.com/sport/football/13965984
{league:Women's World Cup} debutants {team:Equatorial Guinea} opened their Group D campaign with a narrow 1-0 defeat to {team:Norway} in Augsburg. 
{player:Emelie Haavi} scored in the 84th minute after the 1995 world champions had hit the woodwork three times.
The 2008 African champions had chances of their own - with captain {player:Anonman} missing several times. 
{team:Equatorial Guinea}'s next game is against {team:Australia} on 3 July in Bochum, while {team:Norway} face {team:Brazil} in Wolfsburg.
"We could have won the game," said {team:Equatorial Guinea} coach {manager:Marcello Frigerio}, before adding: "I still think we can progress." 
The 2008 African champions sit bottom of the group alongside {team:Australia}, who were also beaten 1-0 - with {team:Brazil}'s {player:Rosana} proving the match-winner in that game.
But in Augsburg, in front of almost 13,000 fans, {team:Equatorial Guinea} needed some time to get into the match. 
Initially, they relied on long-range efforts, like {player:Anonman}'s 40-yard blast and {player:Jurama}'s attempt from a bit closer after 17 minutes.
{team:The Africans} then began to dominate and on 20 minutes, {player:Dulcia} was through on goalkeeper {player:Ingrid Hjelmseth} but {player:Haavi} made a vital last-gasp interception.
{player:Hjelmseth} faced a barrage of shots - most of them from long range - and successfully cleared them all away.
{team:Equatorial Guinea} had one final chance before the break as {player:Anonman} got past {player:Nora Holstad Berge} for a dash at {player:Hjelmseth}, but the keeper won the encounter.
After the break, {player:Anonman} continued her excellent match, breezing past four women before chipping a shot that went just wide of the post after 53 minutes.
The back-and-forth went on as {team:Norway}'s {player:Mykjaland} hit the outside of the right post in the 55th minute.
{team:The Norwegians} found the framework again just two minutes later with Herlovsen hitting the left post before the ball was pushed out in a scramble.
{player:Anonman} broke through again in the 71st minute but again shot wide of the far right post and then she wasted another one-on-one situation with {player:Hjelmseth} just seconds later.
{team:Norway} finally broke through when {player:Larsen Kaurin} crossed from the right side to {player:Haavi}, who shot into an empty net after {team:the Africans} failed to clear.
