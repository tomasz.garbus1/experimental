::{team:Cliftonville} sign ex-{team:Ballymena} goalkeeper {player:Ryan Brown}::
@1307556749
url: https://www.bbc.com/sport/football/13705050
{team:Cliftonville} have signed former {team:Bangor}, {team:Larne} and {team:Ballymena United} goalkeeper {player:Ryan Brown}.
{player:Brown} spent last season with {team:Ballymena United} and becomes manager {manager:Tommy Breslin}'s fifth signing of the summer.
"I'm looking forward to getting started," said former junior international keeper {player:Brown}.
"{team:Cliftonville} have shown great consistency over the last few years and have been challenging at the top end of the table."
Other {team:Reds} signing in recent days includes former {team:Newry} and {team:Portadown} defender {player:John Convery} and ex-{team:Glentoran} midfielder {player:Peter Steele}.
