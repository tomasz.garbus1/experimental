::{team:Aston Villa} 1-0 {team:Birmingham City}::
@1492953770
url: https://www.bbc.com/sport/football/39612229
{player:Gabby Agbonlahor} scored his first goal in 14 months as {team:Aston Villa} ruined {manager:Harry Redknapp}'s first game in charge of {team:Birmingham City} by winning the Second City derby at {stadium:Villa Park}.
{player:Agbonlahor} turned sharply in the six-yard box to score a 68th-minute winner after {team:Blues} failed to clear a corner.
The visitors wasted their best chance when {player:Che Adams} volleyed over from the same spot in the first half.
{team:Blues} are only two points above the {league:Championship} relegation zone.
Their survival hopes now depend on getting results from their final two matches, at home to {team:Huddersfield} on Saturday and then at {team:Bristol City} on 7 May.
But if {team:Blackburn}, the team below them in 22nd, fail to get more than one point from their final two games and {team:Wigan} do not win both of their remaining two games, {team:Blues} would be safe anyway.
{team:Villa}'s sixth win in their last seven home league games lifted them to 12th, still within reach of their only realistic target left this season - a top-10 finish.
But, on a day which started with {team:Villa} fans remembering one old favourite, Ugo Ehiogu, it was {player:Agbonlahor} who ended it with the accolades.
After a well-observed minute's applause in honour of former {team:Villa} and {country:England} centre-back Ehiogu, who died on Friday aged 44, the first half was almost totally devoid of incident.
The only real chance was {player:Adams}' close-range left-footed volley, hooked over the bar from {player:Lukas Jutkiewicz}'s left-wing cross.
The second half was not a great deal better, but that all changed when {player:Agbonlahor} entered the fray on 59 minutes.
Within three minutes of his arrival, {player:Agbonlahor} had lifted the crowd, getting booked after clashing with {player:Ryan Shotton}, who was also shown a yellow card, and six minutes later he had scored.
The 30-year-old striker looked to have kicked his last ball for {team:Villa} before {manager:Bruce}'s appointment as manager in October.
Before scoring in one of {team:Villa}'s rare {league:Premier League} wins last season, against {team:Norwich} in February 2016, he had not netted in 11 months.
But he does know how to score in derby games, having now found the net against {team:Blues} five times, the first of them in {manager:Bruce}'s final game in charge of {team:Blues} in November 2007.
{manager:Bruce} brought him back from the cold in the autumn to come off the bench when the two sides drew 1-1 at {stadium:St Andrew's}.
He was then kept out for three months with a hamstring injury. But, in his first game back, {player:Agbonlahor} kept his cool to clinch a local derby success for {team:Villa}.
{team:Aston Villa} manager {manager:Steve Bruce} told BBC Sport:
"It was the worst game of the season. The ball must have been screaming at one point, saying 'please don't bash me again'.
"We're still not remotely near the finished article. I've got a busy summer ahead, with a huge clear-out job to continue.
"But at least the fans have not only won a derby, they've seen the effort their team has put in. We've taken away the stigma that they don't care.
"If anyone can keep {team:Blues} up, {manager:Harry} will. He had them playing like their lives depended on it and they've got to take that into their last two games."
On tributes to Ehiogu:
"We talk about the importance of winning football matches but it pales into insignificance and is put into perspective when you remember someone like Ugo.
"Our thoughts remain with his family, his wife and especially his kids, who have lost their dad at the age of just 44.
"Both sets of fans gave him a great tribute, but that was just what I expected."
{team:Birmingham City} manager {manager:Harry Redknapp} told BBC Sport:
"They've not had a shot in the second half - apart from the goal. It's the only one they've had. Our keeper didn't even get his gloves dirty.
"You can't say {team:Villa} were good. If that's the best they can do, {manager:Steve}'s clearly got a lot of work to do.
"They ({team:Birmingham} under {manager:Gianfranco Zola}) had won two out of 24. When you look at that record, it scares you to death. Confidence has been low but the lads showed good attitude and responded well.
"We could not have worked any harder. We just didn't get the break. It won't be easy against Huddersfield, but we've got every chance if we keep playing like that."
Match ends, {team:Aston Villa} 1, {team:Birmingham City} 0.
Second Half ends, {team:Aston Villa} 1, {team:Birmingham City} 0.
{player:Paul Robinson} ({team:Birmingham City}) is shown the yellow card for a bad foul.
Foul by {player:Paul Robinson} ({team:Birmingham City}).
{player:Gabriel Agbonlahor} ({team:Aston Villa}) wins a free kick in the attacking half.
Attempt blocked. {player:David Davis} ({team:Birmingham City}) right footed shot from outside the box is blocked.
Attempt missed. {player:Greg Stewart} ({team:Birmingham City}) left footed shot from the right side of the box misses to the left following a set piece situation.
Substitution, {team:Birmingham City}. {player:Cheick Keita} replaces {player:Maikel Kieftenbeld}.
{player:Lukas Jutkiewicz} ({team:Birmingham City}) wins a free kick in the attacking half.
Foul by {player:Mile Jedinak} ({team:Aston Villa}).
Foul by {player:Gabriel Agbonlahor} ({team:Aston Villa}).
{player:Craig Gardner} ({team:Birmingham City}) wins a free kick in the defensive half.
Substitution, {team:Birmingham City}. {player:Greg Stewart} replaces {player:Jacques Maghoma}.
Offside, {team:Birmingham City}. {player:Paul Robinson} tries a through ball, but {player:Lukas Jutkiewicz} is caught offside.
Substitution, {team:Aston Villa}. {player:Conor Hourihane} replaces {player:Scott Hogan}.
Hand ball by {player:Lukas Jutkiewicz} ({team:Birmingham City}).
{player:Mile Jedinak} ({team:Aston Villa}) wins a free kick in the defensive half.
Foul by {player:Paul Robinson} ({team:Birmingham City}).
Foul by {player:Mile Jedinak} ({team:Aston Villa}).
{player:Che Adams} ({team:Birmingham City}) wins a free kick on the right wing.
Substitution, {team:Aston Villa}. {player:Gary Gardner} replaces {player:Albert Adomah}.
Offside, {team:Birmingham City}. {player:Craig Gardner} tries a through ball, but {player:Lukas Jutkiewicz} is caught offside.
Attempt blocked. {player:Jacques Maghoma} ({team:Birmingham City}) left footed shot from the right side of the box is blocked. Assisted by {player:Paul Robinson}.
Corner,  {team:Birmingham City}. Conceded by {player:Sam Johnstone}.
Foul by {player:Albert Adomah} ({team:Aston Villa}).
{player:Jacques Maghoma} ({team:Birmingham City}) wins a free kick on the left wing.
Delay over. They are ready to continue.
Delay in match  ({team:Aston Villa}).
Goal!  {team:Aston Villa} 1, {team:Birmingham City} 0. {player:Gabriel Agbonlahor} ({team:Aston Villa}) left footed shot from very close range to the top left corner following a corner.
Corner,  {team:Aston Villa}. Conceded by {player:Jacques Maghoma}.
{player:Alan Hutton} ({team:Aston Villa}) wins a free kick in the defensive half.
Foul by {player:Jacques Maghoma} ({team:Birmingham City}).
Foul by {player:Neil Taylor} ({team:Aston Villa}).
{player:Che Adams} ({team:Birmingham City}) wins a free kick on the right wing.
Attempt saved. {player:David Davis} ({team:Birmingham City}) right footed shot from the left side of the box is saved in the bottom right corner.
Attempt missed. {player:James Chester} ({team:Aston Villa}) header from the centre of the box is high and wide to the left. Assisted by {player:Henri Lansbury} with a cross following a set piece situation.
{player:Gabriel Agbonlahor} ({team:Aston Villa}) is shown the yellow card.
{player:Ryan Shotton} ({team:Birmingham City}) is shown the yellow card.
{player:Henri Lansbury} ({team:Aston Villa}) wins a free kick on the left wing.
Foul by {player:Ryan Shotton} ({team:Birmingham City}).