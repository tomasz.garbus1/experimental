::Aston Villa 1-0 Birmingham City::
@1492953770
url: https://www.bbc.com/sport/football/39612229
Gabby Agbonlahor scored his first goal in 14 months as Aston Villa ruined Harry Redknapp's first game in charge of Birmingham City by winning the Second City derby at Villa Park.
Agbonlahor turned sharply in the six-yard box to score a 68th-minute winner after Blues failed to clear a corner.
The visitors wasted their best chance when Che Adams volleyed over from the same spot in the first half.
Blues are only two points above the Championship relegation zone.
Their survival hopes now depend on getting results from their final two matches, at home to Huddersfield on Saturday and then at Bristol City on 7 May.
But if Blackburn, the team below them in 22nd, fail to get more than one point from their final two games and Wigan do not win both of their remaining two games, Blues would be safe anyway.
Villa's sixth win in their last seven home league games lifted them to 12th, still within reach of their only realistic target left this season - a top-10 finish.
But, on a day which started with Villa fans remembering one old favourite, Ugo Ehiogu, it was Agbonlahor who ended it with the accolades.
After a well-observed minute's applause in honour of former Villa and England centre-back Ehiogu, who died on Friday aged 44, the first half was almost totally devoid of incident.
The only real chance was Adams' close-range left-footed volley, hooked over the bar from Lukas Jutkiewicz's left-wing cross.
The second half was not a great deal better, but that all changed when Agbonlahor entered the fray on 59 minutes.
Within three minutes of his arrival, Agbonlahor had lifted the crowd, getting booked after clashing with Ryan Shotton, who was also shown a yellow card, and six minutes later he had scored.
The 30-year-old striker looked to have kicked his last ball for Villa before Bruce's appointment as manager in October.
Before scoring in one of Villa's rare Premier League wins last season, against Norwich in February 2016, he had not netted in 11 months.
But he does know how to score in derby games, having now found the net against Blues five times, the first of them in Bruce's final game in charge of Blues in November 2007.
Bruce brought him back from the cold in the autumn to come off the bench when the two sides drew 1-1 at St Andrew's.
He was then kept out for three months with a hamstring injury. But, in his first game back, Agbonlahor kept his cool to clinch a local derby success for Villa.
Aston Villa manager Steve Bruce told BBC Sport:
"It was the worst game of the season. The ball must have been screaming at one point, saying 'please don't bash me again'.
"We're still not remotely near the finished article. I've got a busy summer ahead, with a huge clear-out job to continue.
"But at least the fans have not only won a derby, they've seen the effort their team has put in. We've taken away the stigma that they don't care.
"If anyone can keep Blues up, Harry will. He had them playing like their lives depended on it and they've got to take that into their last two games."
On tributes to Ehiogu:
"We talk about the importance of winning football matches but it pales into insignificance and is put into perspective when you remember someone like Ugo.
"Our thoughts remain with his family, his wife and especially his kids, who have lost their dad at the age of just 44.
"Both sets of fans gave him a great tribute, but that was just what I expected."
Birmingham City manager Harry Redknapp told BBC Sport:
"They've not had a shot in the second half - apart from the goal. It's the only one they've had. Our keeper didn't even get his gloves dirty.
"You can't say Villa were good. If that's the best they can do, Steve's clearly got a lot of work to do.
"They (Birmingham under Gianfranco Zola) had won two out of 24. When you look at that record, it scares you to death. Confidence has been low but the lads showed good attitude and responded well.
"We could not have worked any harder. We just didn't get the break. It won't be easy against Huddersfield, but we've got every chance if we keep playing like that."
Match ends, Aston Villa 1, Birmingham City 0.
Second Half ends, Aston Villa 1, Birmingham City 0.
Paul Robinson (Birmingham City) is shown the yellow card for a bad foul.
Foul by Paul Robinson (Birmingham City).
Gabriel Agbonlahor (Aston Villa) wins a free kick in the attacking half.
Attempt blocked. David Davis (Birmingham City) right footed shot from outside the box is blocked.
Attempt missed. Greg Stewart (Birmingham City) left footed shot from the right side of the box misses to the left following a set piece situation.
Substitution, Birmingham City. Cheick Keita replaces Maikel Kieftenbeld.
Lukas Jutkiewicz (Birmingham City) wins a free kick in the attacking half.
Foul by Mile Jedinak (Aston Villa).
Foul by Gabriel Agbonlahor (Aston Villa).
Craig Gardner (Birmingham City) wins a free kick in the defensive half.
Substitution, Birmingham City. Greg Stewart replaces Jacques Maghoma.
Offside, Birmingham City. Paul Robinson tries a through ball, but Lukas Jutkiewicz is caught offside.
Substitution, Aston Villa. Conor Hourihane replaces Scott Hogan.
Hand ball by Lukas Jutkiewicz (Birmingham City).
Mile Jedinak (Aston Villa) wins a free kick in the defensive half.
Foul by Paul Robinson (Birmingham City).
Foul by Mile Jedinak (Aston Villa).
Che Adams (Birmingham City) wins a free kick on the right wing.
Substitution, Aston Villa. Gary Gardner replaces Albert Adomah.
Offside, Birmingham City. Craig Gardner tries a through ball, but Lukas Jutkiewicz is caught offside.
Attempt blocked. Jacques Maghoma (Birmingham City) left footed shot from the right side of the box is blocked. Assisted by Paul Robinson.
Corner,  Birmingham City. Conceded by Sam Johnstone.
Foul by Albert Adomah (Aston Villa).
Jacques Maghoma (Birmingham City) wins a free kick on the left wing.
Delay over. They are ready to continue.
Delay in match  (Aston Villa).
Goal!  Aston Villa 1, Birmingham City 0. Gabriel Agbonlahor (Aston Villa) left footed shot from very close range to the top left corner following a corner.
Corner,  Aston Villa. Conceded by Jacques Maghoma.
Alan Hutton (Aston Villa) wins a free kick in the defensive half.
Foul by Jacques Maghoma (Birmingham City).
Foul by Neil Taylor (Aston Villa).
Che Adams (Birmingham City) wins a free kick on the right wing.
Attempt saved. David Davis (Birmingham City) right footed shot from the left side of the box is saved in the bottom right corner.
Attempt missed. James Chester (Aston Villa) header from the centre of the box is high and wide to the left. Assisted by Henri Lansbury with a cross following a set piece situation.
Gabriel Agbonlahor (Aston Villa) is shown the yellow card.
Ryan Shotton (Birmingham City) is shown the yellow card.
Henri Lansbury (Aston Villa) wins a free kick on the left wing.
Foul by Ryan Shotton (Birmingham City).