::{player:Rory Patterson} & {player:Shane McEleney} transfer-listed by {team:Derry City}::
@1419084965
url: https://www.bbc.com/sport/football/30561776
{team:Derry City} have made star striker {player:Rory Patterson} and defender {player:Shane McEleney} available for transfer as part of cost-cutting measures at the {stadium:Brandywell}.
Manager {manager:Peter Hutton} said the club had to "look at the bigger picture which must be a sustainable club working within its means".
"{player:Shane} and {player:Rory} have been made fully aware of the situation," {manager:Hutton} told the Derry Journal.
{player:Patterson} scored an impressive 61 goals in 92 {team:Derry} appearances.
{team:City}'s defeat in the {league:FAI Cup} final in November meant that they missed out on a European place after a hugely disappointing {league:League} campaign.
{team:Derry}'s struggles in 2014 - which led to {manager:Roddy Collins} losing the {stadium:Brandywell} job after a brief spell in charge - meant reduced home attendances and the club has opted to cash in on two of its playing assets.
Earlier this week, long-serving {player:Barry Molloy} also left the club after opting to join {league:Irish Premiership} outfit {team:Crusaders}.
{country:Northern Ireland} international {player:Patterson}, 30, tweeted on Thursday evening that he had not spoken to any other clubs by that stage and "hadn't a clue" where he would be playing next.
{player:Patterson} made his {country:Northern Ireland} debut while playing for {league:Irish League} club {team:Coleraine} in February 2010 before spending a season with English club {team:Plymouth Argyle}.
He went on to make four further international appearances but has not featured since the game against {country:Scotland} in November 2010.
{player:McEleney}, 23, lost his place during {manager:Collins}'s reign and continued to struggle hold down a first-team spot after {manager:Hutton} took charge.
The defender represented {country:Northern Ireland} at Under-21 level two years ago after previously for at youth level for the {country:Republic of Ireland}.