::{player:Shaun Whalley} and {player:Danny Fitzsimons} leave {team:Luton Town}::
@1431966435
url: https://www.bbc.com/sport/football/32788157
Winger {player:Shaun Whalley} and defender {player:Danny Fitzsimons} have left {team:Luton Town} by mutual consent. 
{player:Fitzsimons}, 23, did not make an appearance for the {league:League Two} side, having joined from {team:Histon} in June 2013 after suffering a cruciate knee injury.
{player:Whalley} scored four goals in 35 appearances over two seasons.
{team:The Hatters} have also announced they have terminated {player:Ricky Miller}'s contract following an internal investigation into a breach of club discipline.