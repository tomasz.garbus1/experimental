::{team:Celtic} boss {manager:Lennon} rues second-half collapse in derby::
@1316359505
url: https://www.bbc.com/sport/football/14965325
{team:Celtic} manager {manager:Neil Lennon} felt his side "didn't compete" in the second half of their 4-2 defeat to {team:Rangers}.
{manager:Lennon}'s men led 2-1 at half-time in the season's first Old Firm game but lost three goals after the break.
"I'm very disappointed," he told BBC {country:Scotland}. "We had our tails up but we never really got going in the second half. We sat too deep.
"If you don't compete for second balls and earn the right to play then you expect to get beaten."
A second defeat of the season for {team:Celtic} means they fall four points behind {league:Scottish Premier League} leaders {team:Rangers}.
"We didn't weather any storm in the second half," said {manager:Lennon}. "When {player:Steven Davis} hit the bar that was a warning sign if you ever need one and we didn't take any heed of it.
"My midfield seemed to go missing and we didn't get anyone on the ball to get it up to the forward line, who looked a threat going forward at times.
"I don't know if it's fatigue from Thursday night (the 2-0 {league:Europa League} defeat to {team:Atletico Madrid}) but you have to compete better than that and we didn't do that.
"Then we hit the post from a corner and {team:Rangers} have gone up the other end and scored so that was probably the game-changing moment as far as we were concerned."
"I'm not happy and I'm sure the supporters aren't happy either. We had a grip of the game and we've let it go so that, for me, is a huge disappointment.
"There's four points in it, there's a long way to go yet, there's plenty of football but we have to do a lot better than that."
{player:Charlie Mulgrew} was sent off after picking up two bookings, the second for a challenge on {player:Davis}.
"He was treading a fine line on a couple of occasions," {manager:Lennon} said. "We told {player:Charlie} to keep his discipline but I thought he got the ball so I didn't think it was a sending off.
"I didn't think the challenge itself warranted a second yellow."
{player:Kris Commons} was again absent from the {team:Celtic} line-up but {manager:Lennon} explained: "He didn't train yesterday. He has a bit of a sore groin and it might have been a gamble putting him in the squad."
An exciting and dramatic derby did not spill over into controversy and Lennon added during his interview with BBC Scotland's Chick Young: "It's not us who are stoking up the fires, it's some of your fellow colleagues in the media."
