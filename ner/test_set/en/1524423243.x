::Manchester City 5-0 Swansea City::
@1524423243
url: https://www.bbc.com/sport/football/43772471
Manager Pep Guardiola says Manchester City's focus is on chasing records after they celebrated being crowned Premier League champions by thrashing Swansea.
Manchester City were given a guard of honour and there were celebrations at full-time in their first match since their third league title in seven seasons was confirmed.
Thousands of fans invaded the pitch as Guardiola's side emphatically ended a run of three successive home defeats.
And Guardiola said "looking for records helps to keep the players focused" as his side prepare for their last four games of the season.
This victory took them to 90 points, five shy of the Premier League record set by Chelsea in 2004-05, and they are also chasing the most number of wins and goals scored in a season.
"The league is won, now it's the records," said Guardiola. "It's never unfinished business in football, you have to keep improving.
"If you think 'OK, we are already champions', we are done and you cannot play like the way we played. If we are improving it will be good for the new season, finishing in a good way."
Raheem Sterling's cut-back set up David Silva's early opener before the winger poked in from six yards to make it two within 16 minutes.
Kevin de Bruyne scored with a sensational 25-yard drive before Bernardo Silva pounced on the rebound of Gabriel Jesus' saved penalty.
The Brazilian eventually found the back of the net when he headed in Yaya Toure's chipped pass.
Swansea are without a win in five league matches and remain four points above the relegation zone with four games to play.
And the Swans must take on fifth-placed Chelsea at the Liberty Stadium next week before hosting relegation-threatened Stoke and Southampton in a tough run-in.
Manchester City can look forward to their penultimate home game of the season against Huddersfield on 6 May, when they will be presented with the Premier League trophy.
It was a day of celebrations for Manchester City, who were crowned Premier League champions last week following Manchester United's defeat by West Brom. 
Supporters gathered at Etihad Stadium before kick-off to parade replica trophies, fly flags and welcome the champions on arrival. 
They also stormed the pitch in numbers to celebrate at full-time - singing, taking pictures and setting off flares to revel in Manchester City's third Premier League title.
And Guardiola's side were applauded by the Swansea players and staff in a guard of honour before kick-off.
The hosts' performance was worthy of a title-winning side, too. 
They cut Swansea's defence apart in the opening stages and passed the ball around with flair and creativity.
It was a display of intent from the champions, and while they can still break Premier League records before the season ends in May, they are also chasing individual and club accolades.
Manchester City have now also broken their own record tally of 89, set in 2011-12, and Guardiola could beat his previous best points tally in a single league season as a manager - 99, with Barcelona in 2009-10.
No team has accrued 100 points or more in a single top-flight campaign, but City would become the first that if they pick up 10 from their remaining four games. 
Records Manchester City can still break:
Carlos Carvalhal's Swansea have improved dramatically since December, but they still remain in a relegation battle.
They currently sit four points above the drop zone - though they would not have expected to pick up points at the Etihad.
But with four games to go, including a tough match against FA Cup finalists Chelsea to come next week, the Swans cannot yet feel that their survival is guaranteed.
They have lost only four times in their last 14 Premier League matches, though, a record that will give them confidence heading into those crucial games against Stoke and Southampton. 
And though there were not too many positives to take from their performance in Manchester, they were simply outclassed by the best side in the country this season.
Alfie Mawson came close with a headed effort in the second half, but there was not too much to get excited about for the Swans and their attentions will have already turned to their remaining four games.

                            
                        
Manchester City manager Pep Guardiola told BBC Sport: "It was a happy atmosphere around the stadium, getting off the bus and of course the supporters. The most important thing was a good performance.
"You have to keep improving. We spoke to the players in the week about that. If we are improving it will be good for the new season, finishing in a good way.
"When Swansea had the ball we pressed so good and so intense and then when we had the ball we had to keep it moving, moving, moving, playing simple, until we had the chance to play a final pass.
"Swansea have had good results under Carlos Carvalhal against Arsenal and Liverpool and that's why it was so important to focus, play well and score."

                            
                        
Swansea boss Carlos Carvalhal told BBC Sport: "It was a difficult game for us. They had just lost one home game against Manchester United. They are champions; they are a team from another planet.
"We are playing to survive. We lose one game and at this moment we are still four points from the teams behind us. 
"This game, we forget it and we move to the next game to try and achieve the points.
"We have recovered nine points in three months, we have four games left. It is in our hands. I hope we can solve the things before the last two games but if we don't, we have the two games at the end I think with the determination and commitment we have we will achieve it."
Swansea host Chelsea in their next Premier League match on 28 April (17:30 BST), while Manchester City travel to London to take on West Ham on 29 April (14:15 BST).
Match ends, Manchester City 5, Swansea City 0.
Second Half ends, Manchester City 5, Swansea City 0.
Corner,  Manchester City. Conceded by Andy King.
Attempt missed. Ilkay GÃ¼ndogan (Manchester City) right footed shot from the centre of the box is close, but misses the top left corner. Assisted by Bernardo Silva.
Goal!  Manchester City 5, Swansea City 0. Gabriel Jesus (Manchester City) header from the centre of the box to the bottom left corner. Assisted by Yaya TourÃ© with a through ball.
Attempt missed. Vincent Kompany (Manchester City) header from the right side of the six yard box is close, but misses to the right. Assisted by Yaya TourÃ© with a through ball.
Corner,  Manchester City. Conceded by Alfie Mawson.
Phil Foden (Manchester City) wins a free kick in the attacking half.
Foul by AndrÃ© Ayew (Swansea City).
Attempt blocked. Yaya TourÃ© (Manchester City) right footed shot from outside the box is blocked. Assisted by Bernardo Silva.
Benjamin Mendy (Manchester City) wins a free kick on the left wing.
Foul by Jordan Ayew (Swansea City).
Attempt saved. Gabriel Jesus (Manchester City) header from the centre of the box is saved in the centre of the goal. Assisted by Phil Foden with a cross.
Attempt saved. Benjamin Mendy (Manchester City) right footed shot from the left side of the box is saved in the centre of the goal.
Attempt missed. Danilo (Manchester City) left footed shot from outside the box is high and wide to the left. Assisted by Bernardo Silva.
Attempt missed. Sam Clucas (Swansea City) left footed shot from the centre of the box misses to the left. Assisted by Tammy Abraham following a fast break.
Attempt blocked. Yaya TourÃ© (Manchester City) right footed shot from outside the box is blocked.
Phil Foden (Manchester City) wins a free kick in the attacking half.
Foul by Mike van der Hoorn (Swansea City).
Foul by Ilkay GÃ¼ndogan (Manchester City).
Jordan Ayew (Swansea City) wins a free kick in the defensive half.
Substitution, Swansea City. Tammy Abraham replaces Martin Olsson.
Substitution, Manchester City. Benjamin Mendy replaces Fabian Delph.
Offside, Swansea City. Kyle Naughton tries a through ball, but AndrÃ© Ayew is caught offside.
Substitution, Manchester City. Phil Foden replaces Raheem Sterling.
Corner,  Manchester City. Conceded by Kyle Naughton.
Substitution, Swansea City. Kyle Bartley replaces Federico FernÃ¡ndez because of an injury.
Delay over. They are ready to continue.
Delay in match Federico FernÃ¡ndez (Swansea City) because of an injury.
Substitution, Manchester City. Yaya TourÃ© replaces Kevin De Bruyne.
Substitution, Swansea City. Sam Clucas replaces Ki Sung-yueng.
Goal!  Manchester City 4, Swansea City 0. Bernardo Silva (Manchester City) right footed shot from the right side of the six yard box to the centre of the goal following a set piece situation.
Penalty saved! Gabriel Jesus (Manchester City) fails to capitalise on this great opportunity,  right footed shot saved  in the bottom left corner.
Penalty Manchester City. Raheem Sterling draws a foul in the penalty area.
Penalty conceded by Federico FernÃ¡ndez (Swansea City) after a foul in the penalty area.
Raheem Sterling (Manchester City) wins a free kick in the attacking half.
Foul by Jordan Ayew (Swansea City).
Corner,  Manchester City. Conceded by Mike van der Hoorn.
Attempt missed. Alfie Mawson (Swansea City) header from the right side of the six yard box is just a bit too high. Assisted by Ki Sung-yueng with a cross following a corner.
Corner,  Swansea City. Conceded by Fabian Delph.