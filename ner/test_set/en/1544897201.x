::Watford 3-2 Cardiff City::
@1544897201
url: https://www.bbc.com/sport/football/46491892
Watford manager Javi Gracia said his side had been taught a "good lesson" after they held off a late Cardiff comeback to seal their first Premier League win since October.
Three fine goals from Gerard Deulofeu, Jose Holebas and Domingos Quinas saw Watford cruise into a comfortable lead, with their opponents proving no match for the Hornets for much of the game.
But with 10 minutes remaining at Vicarage Road, Junior Hoilett pulled back a seeming consolation goal before Bobby Reid added a second to give Cardiff a sniff of an unlikely point.
"The end of the game was different than the most part of it. We dominated the first 70 minutes, we created a lot of chances and we could have got a better victory today," Gracia told BBC Sport.
"Their goalkeeper was amazing. The last part of the game I think was a good lesson for us to know how we have to manage the game.
"The last games we have played well, but today we deserved the win. It is important for the players and supporters. All of them deserve the victory."
Neil Warnock's side would have been dead and buried early on were it not for the acrobatics of goalkeeper Neil Etheridge, who produced a string of stunning saves either side of half-time to keep Cardiff in the game - which marked the first top-flight meeting between these two sides.
Watford looked to have done enough to seal all three points at a canter yet Cardiff's comeback came out of the blue following a performance that lacked any real energy.
"I am disappointed with the individual errors for the goals, but I am pleased with the way we kept going," Warnock told BBC Sport.
"We set ourselves up to be difficult to break down and I cannot legislate for individual errors. We cannot keep playing like this every week and losing - we need to get results away from home."
Watford had lost their two previous home games before the visit of Cardiff but their performance was far superior to that of a side who, prior to kick-off, had picked up just nine points from their past 12 games.
Deulofeu was at the heart of much of their play and his goal - described by Garth Crooks on Final Score as 'Diego Maradona-esque' - was richly deserved as Cardiff struggled to contain their opponents.
In a moment of magic, he jinked his way past two defenders before neatly placing the ball past Etheridge.
Roberto Pereyra - Watford's top scorer this season with five goals - was not without his chances either and went close not long after Deulofeu's opener, a poor final touch letting him down.
Watford went into half-time having enjoyed 75% of the first-half possession and, with moves straight off the training ground, added two more goals after the break, the second, a stunning curling effort from Holebas the pick of the bunch.
But they quickly became complacent after Quinas' third - his first Premier League goal - and Cardiff made them pay for the change in pace with two quickfire goals.
Watford could have had a fourth in added time but for another late save by Etheridge. 
Just two places separated Watford and Cardiff in the table before kick-off but the first half demonstrated a real gulf in class between the two sides.
But despite the defeat, Cardiff head home with a lot to thank goalkeeper Etheridge for. He produced save after save, single-handedly preventing Pereyra from scoring a hat-trick with less than an hour played.
Despite their lacklustre first-half performance, they weren't without their chances though and Harry Arter almost played Josh Murphy in on goal with a sliding cross midway through.
A rare moment of Watford carelessness was capitalised on by the visitors as Quina played the ball straight into their possession and Hoilett was able to put in a cross, which floated just high of Callum Paterson in the box.
It was in the second half that they found another gear with some slick play involving Nathaniel Mendez-Laing putting the pressure on the hosts.
Hoilett had a chance on goal before his and his side's perseverance was rewarded when he curled the ball in past Ben Foster, before Reid capitalised on a scramble in the box to poke home.

                            
                        

                            
                        
Watford travel to West Ham in their next Premier League outing on Saturday, 22 December (15:00 GMT), while Cardiff welcome Manchester United to Wales (17:30 GMT).
Match ends, Watford 3, Cardiff City 2.
Second Half ends, Watford 3, Cardiff City 2.
Stefano Okaka (Watford) wins a free kick in the attacking half.
Foul by Sean Morrison (Cardiff City).
Ben Foster (Watford) wins a free kick in the defensive half.
Foul by Callum Paterson (Cardiff City).
Foul by JosÃ© Holebas (Watford).
Nathaniel Mendez-Laing (Cardiff City) wins a free kick on the right wing.
Foul by Abdoulaye DoucourÃ© (Watford).
VÃ­ctor Camarasa (Cardiff City) wins a free kick on the right wing.
Foul by Stefano Okaka (Watford).
Bruno Ecuele Manga (Cardiff City) wins a free kick in the defensive half.
Corner,  Watford. Conceded by Neil Etheridge.
Attempt saved. Abdoulaye DoucourÃ© (Watford) right footed shot from the centre of the box is saved in the top centre of the goal. Assisted by Stefano Okaka.
Attempt missed. Domingos Quina (Watford) right footed shot from outside the box is close, but misses the top right corner following a set piece situation.
Attempt missed. Tom Cleverley (Watford) right footed shot from outside the box misses to the left. Assisted by Stefano Okaka following a set piece situation.
Substitution, Watford. Tom Cleverley replaces Gerard Deulofeu.
Abdoulaye DoucourÃ© (Watford) wins a free kick in the attacking half.
Foul by Sean Morrison (Cardiff City).
Attempt blocked. Stefano Okaka (Watford) right footed shot from the left side of the box is blocked. Assisted by Roberto Pereyra.
Goal!  Watford 3, Cardiff City 2. Bobby Reid (Cardiff City) right footed shot from the centre of the box to the centre of the goal.
Attempt blocked. Bobby Reid (Cardiff City) right footed shot from the centre of the box is blocked.
Attempt saved. Sol Bamba (Cardiff City) left footed shot from the centre of the box is saved in the centre of the goal.
Substitution, Watford. Stefano Okaka replaces Troy Deeney.
Goal!  Watford 3, Cardiff City 1. David Junior Hoilett (Cardiff City) right footed shot from outside the box to the top right corner. Assisted by VÃ­ctor Camarasa.
Substitution, Cardiff City. Bobby Reid replaces Aron Gunnarsson.
Attempt missed. Gerard Deulofeu (Watford) right footed shot from a difficult angle on the right is close, but misses the top left corner. Assisted by Isaac Success.
Substitution, Watford. Isaac Success replaces Ken Sema.
Foul by Troy Deeney (Watford).
Neil Etheridge (Cardiff City) wins a free kick in the defensive half.
Attempt missed. David Junior Hoilett (Cardiff City) header from the centre of the box is just a bit too high. Assisted by Callum Paterson with a cross.
Foul by Domingos Quina (Watford).
VÃ­ctor Camarasa (Cardiff City) wins a free kick in the defensive half.
Attempt saved. JosÃ© Holebas (Watford) left footed shot from the left side of the box is saved in the top right corner.
Offside, Cardiff City. Nathaniel Mendez-Laing tries a through ball, but Callum Paterson is caught offside.
Goal!  Watford 3, Cardiff City 0. Domingos Quina (Watford) right footed shot from outside the box to the top right corner. Assisted by Ken Sema.
Delay over. They are ready to continue.
Delay in match JosÃ© Holebas (Watford) because of an injury.
Attempt missed. VÃ­ctor Camarasa (Cardiff City) right footed shot from outside the box is close, but misses to the right. Assisted by Sol Bamba.
Substitution, Cardiff City. Lee Peltier replaces Joe Bennett because of an injury.