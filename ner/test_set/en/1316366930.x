::Africain and Sunshine reach Confederation Cup semi-final::
@1316366930
url: https://www.bbc.com/sport/football/14966537
Tunisian side Club Africain finished top of Group A of the Confederation Cup thanks to their 1-0 away win Kaduna United of Nigeria on Sunday.
InterClube of Angola also advanced to the semi-final despite losing 1-0 to Ivorian side Asec Mimosas in the group's other game.
Africain finished top with 11 points followed by InterClube on 10 points.
The lower placing also changed with Asec coming third on seven points and Kaduna United last with two fewer.
Club Africain will play Sunshine Stars of Nigeria and unbeaten Group B winners MAS of Morocco meet InterClube in the semi-finals in October as the pursuit of a $660,000 first prize intensifies.
Chaker Rguii, a 24-year-old Tunisian midfielder, converted a penalty on the stroke of half-time to give Club Africain a 1-0 victory over Kaduna in the northern Nigerian city.
United needed maximum points to squeeze into the last four on the head-to-head rule only to let down again by poor home form as they gathered just four points from a possible nine in the mini-league phase of the second-tier competition.
Club Africain are the only semi-finalists to have triumphed in Africa having lifted the African Champions Cup - later renamed the African Champions League - 20 years ago with a 7-3 aggregate triumph over Nakivubo Villa of Uganda.
Asec, also former African champions, rounded off a disappointing pool campaign with a 1-0 win over InterClube thanks to a mid-first half goal from Hugues Zagbayou in Ivorian commercial capital Abidjan.