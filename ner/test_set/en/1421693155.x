::Stephen Dawson: Rochdale player reveals 'hell' over spot-fix claims::
@1421693155
url: https://www.bbc.com/sport/football/30886004
Rochdale midfielder Stephen Dawson says he has been to "hell and back" after spot-fixing charges were dropped.
Dawson, 29, was one of 13 players arrested in April but the Crown Prosecution Service dismissed the case because of insufficient evidence.
"They have been the worst nine months of my life," said Dawson, who has always maintained his innocence.
"The embarrassment and the stress it has put on myself and those closest to me is something I will never forget."
Dawson, who joined Rochdale in the summer on a free transfer from Barnsley, added on the club website: "My family and I have been to hell and back. My reputation and integrity were seriously tarnished. 
"After 10 years in football and having played over 400 games, the after-effects of this whole situation had left me at rock-bottom and ready to quit the game that I love.
"With my reputation in ruins I was left with no club or contract in the summer and if it was not for Keith Hill (Rochdale manager) and Rochdale football club I don't know where I would be now."
Prosecutors said they had looked at the "reliability of the evidence" in light of the collapse of the trial of former X Factor judge Tulisa Contostavlos.
That led to a number of cases linked to journalist Mazher Mahmood, known as the "Fake Sheikh", being reviewed. 
The original six suspects were arrested after information was passed to the authorities by the tabloid newspaper the Sun on Sunday, for which Mr Mahmood worked.
But the CPS said it did not believe there was enough evidence to "provide a realistic prospect of conviction".
Dawson added: "I have always indicated my non-involvement in this matter and total innocence and I am pleased that the CPS have withdrawn the prosecution."