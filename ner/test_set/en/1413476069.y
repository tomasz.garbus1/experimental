::{team:Newport County}: {manager:Justin Edinburgh} hits back at {manager:Mark Aizlewood}::
@1413476069
url: https://www.bbc.com/sport/football/29649230
{team:Newport County} manager {manager:Justin Edinburgh} has hit back at claims from {team:Carmarthen}'s {manager:Mark Aizlewood} {team:the Exiles} will regret not signing striker {player:Christian Doidge}.
Former {team:Newport} player {manager:Aizlewood} was surprised {team:County} did not sign {player:Doidge} on a permanent basis from {team:Carmarthen}.
{player:Doidge}, 21, had a trial at {team:County} but joined {team:Dagenham & Redbridge}, who {team:Newport} face on Saturday.
"I think he needs to keep his comments to himself," {manager:Edinburgh} said.
"I'm not bringing the boy [{player:Doidge}] into it I'm bringing the manager's comments in to it.
"I don't think he's played at the level I've played at. I don't think he's managed at the level I have so it's probably best to keep those kind of comments to himself."
Responding to {manager:Edinburgh}'s comments {manager:Aizelwood}, who won 39 caps for {country:Wales} told BBC {country:Wales} Sport: 
"If {manager:Justin} wants to have a go at me that's fine, but I suggest he gets his facts right. Remind him how many caps I won for {country:Wales}."
Since joining {league:League Two} {team:Dagenham} in August for an undisclosed fee,  {team:Newport}-born {player:Doidge} has made four appearances without scoring a single goal for {team:the Daggers}.
The former {team:Southampton} academy player had been signed by {league:Welsh Premier} side {team:Carmarthen}  in January 2013 and spent a week's trial with {team:County} in April 2014.
{player:Doidge} spent another trial period with {team:Dagenham & Redbridge} in the summer before joining the club on a permanent deal during the summer.
{manager:Aizlewood}, the former {team:Charlton Athletic} and {team:Charlton Athletic} defender, said in August that {team:County} had made a mistake in not signing {player:Doidge}.
"If {team:County} allow {player:Christian} to go to {team:Dagenham} it will be the biggest mistake they've made since they failed to secure a sell-on fee when {player:John Aldridge} left," he told the South {country:Wales} Argus, referring to {player:Aldridge}'s 1984 move to {team:Oxford United}.
But {team:County} manager {manager:Edinburgh} responded to those comments in a news conference ahead of his side's trip to {team:Dagenham} on Saturday.
"I beg to differ. We're talking about a top quality international in {player:John Aldridge}," {manager:Edinburgh} added
"The boy [{player:Doidge}], I've seen him play twice this year, He's adjusting and finding his feet. 
"I think he could have a future in the game, but whether or not he's going to come back and bite me on the backside like I've been told, time will tell."