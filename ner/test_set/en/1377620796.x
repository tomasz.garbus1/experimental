::Aston Villa defender Enda Stevens joins Notts County on loan::
@1377620796
url: https://www.bbc.com/sport/football/23856587
Notts County have signed Aston Villa full-back Enda Stevens on a 28-day emergency loan deal.
The 23-year-old could make his debut for the Magpies against Liverpool in the Capital One Cup on Tuesday.
Stevens joined Villa from Shamrock Rovers for an undisclosed fee in January 2012 and made nine appearances last season.
Chris Kiwomya's side are currently 21st in League One with one point from four matches.