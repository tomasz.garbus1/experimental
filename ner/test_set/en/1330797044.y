::{team:Watford} 3-2 {team:Burnley}::
@1330797044
url: https://www.bbc.com/sport/football/17162959
{team:Burnley}'s {league:Championship} play-off hopes suffered a blow as {team:Watford} came back from two down to win at {stadium:Vicarage Road}.
{team:The Clarets} deservedly led through {player:Jay Rodriguez}'s shot and looked in control when {player:Nyron Nosworthy} headed into his own net just after the break.
{player:Nosworthy} made up for his earlier error by pulling a goal back with a header, before substitute {player:Alex Kacaniklic} drove in an equaliser.
{player:Troy Deeney} sealed a stunning fightback from close range with 15 minutes left.
{team:Burnley} are now eight points off the play-offs courtesy of their third defeat in a row in succession, while {team:Watford}'s win lifts them to 17th.
The visitors largely dominated the first half, {player:Dean Marney} heading over and {player:Ross Wallace} firing a shot just over the crossbar before they took the lead when {player:Rodriguez} nipped in to intercept {player:Adrian Mariappa}'s headed backpass and finish.
It got worse for the home side early in the second half when {player:Wallace} sent in a free-kick from the right touchline and the unfortunate {player:Nosworthy} headed into his own net.
However, {player:Nosworthy} was quick to redeem himself when he met {player:Sean Murray}'s corner at the back post and headed in to halve the deficit.
{team:The Hornets} drew level with 18 minutes remaining when {player:Joe Garner} flicked the ball into the path of {player:Kacaniklic} who drove a shot into the far corner.
The comeback was complete when {team:Burnley} failed to clear {player:Murray}'s corner and the ball fell to {player:Deeney} who converted from close range.
{manager:Watford} manager {manager:Sean Dyche} said:
"I thought the way the players went about it, second half particularly, was what we expect, what we believe in and what the players have come to believe in over this season, never say never, and we certainly didn't today with the way we went about in the second half.
"The players stayed on the front foot, kept playing. We found that belief that I spoke to them about at half-time.
"I didn't go mad at them at half-time, I didn't question them, I just said take the shackles off, relax and play and I thought it was evident second half, going 2-0 down, the way they went about it was absolutely fantastic."
{team:Burnley} manager {manager:Eddie Howe} said:
"How we lost that game, I really don't know. At 2-0 the game's over. We've played excellently, executed our game plan perfectly but once we conceded the first goal we just rocked.
"We looked unsteady, unsure of ourselves and set plays we didn't our jobs in the second half and it's a tough one to take. We shouldn't have even got near to losing that game.
"We have to learn from this big time, we've got to really step up to the plate and show a little bit more leadership during games.
"We are a young side but that's no excuse. There's enough people out there who've played enough games to react better to setbacks."
Live text commentary