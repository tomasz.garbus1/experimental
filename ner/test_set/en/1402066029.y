::{team:St Mirren}: {player:Mark Ridgers} is first {manager:Tommy Craig} signing::
@1402066029
url: https://www.bbc.com/sport/football/27739020
{player:Mark Ridgers} has become {manager:Tommy Craig}'s first signing as {team:St Mirren} manager after the 23-year-old goalkeeper was released by relegated {team:Hearts}.
The {country:Scotland} Under-21s cap has signed a two-year contract with the Paisley club after passing a medical.
Having joined {team:Hearts} from {team:Ross County} in 2007, {player:Ridgers} only managed four first-team appearances, two of them last season.
And he left the club along with first choice {player:Jamie MacDonald}.
I am really looking forward to working with {player:Paul Mathers}, who I rate very highly
They were among a number of players released after a takeover by Ann Budge, who is in the process of taking {team:Hearts} out of administration.
{player:Ridgers} will remain in the {league:Scottish Premiership} but will have to challenge {player:Marian Kello}, who has signed a contract extension, for a starting place with {team:St Mirren}.
{manager:Craig} told his club website: "{player:Mark} gives great competition for {player:Marian Kello} which every goalkeeper needs. 
"We see development within {player:Mark}. At 23, he is relatively young for a goalkeeper, but we have every confidence he will be a valuable addition to our squad.
"He's built like a goalkeeper should be and has a presence between the posts and is also capable of playing the type of football that we will pursue."
Inverness-born {player:Ridgers}, for whom {team:Hearts} paid {team:County} £40,000, spent two loan spells with {team:East Fife} and another with {team:Airdrie United} while at Tynecastle.
His availability last season was restricted following a knee operation and he now hopes to challenge for the first team with the help of {team:St Mirren}'s goalkeeping coach.
"I am really looking forward to working with {player:Paul Mathers}, who I rate very highly, and I want {team:St Mirren} to push on in the new season," said {player:Mathers}.
"My hope is to play as many games as possible and to keep improving and progressing."