::{team:Spartak Moscow}: {player:Santi Cazorla} scored first goal since serious Achilles injury::
@1538692201
url: https://www.bbc.com/sport/football/45750799
Former {team:Arsenal} midfielder {player:Santi Cazorla} scored a 96th-minute equaliser for {team:Villarreal} in the {league:Europa League} - his first goal since a serious Achilles injury.
{player:Cazorla} converted a penalty against {team:Spartak Moscow} for his first goal since netting for {team:Arsenal} in September 2016.
He was ruled out for two years because of the injury which needed 11 operations and involved bacteria "eating" eight centimetres of his ankle tendon.
{player:Cazorla} rejoined {team:Villarreal} in July.
{player:The Spaniard} contracted gangrene after one of his surgeries and had been told he could lose his leg and should be satisfied just to walk again.
The 33-year-old had his final surgery in May and had his Achilles reconstructed, with doctors grafting skin from his left arm - featuring a tattoo - to his right ankle.

                            
                        
Match ends, {team:Spartak Moscow} 3, {team:Villarreal} 3.
Second Half ends, {team:Spartak Moscow} 3, {team:Villarreal} 3.
Goal!  {team:Spartak Moscow} 3, {team:Villarreal} 3. {player:Santiago Cazorla} ({team:Villarreal}) converts the penalty with a right footed shot to the centre of the goal.
Penalty conceded by {player:Aleksandr Tashaev} ({team:Spartak Moscow}) after a foul in the penalty area.
Penalty {team:Villarreal}. {player:Ramiro Funes Mori} draws a foul in the penalty area.
Corner,  {team:Villarreal}. Conceded by {player:Salvatore Bocchetti}.
Attempt saved. {player:Aleksandr Tashaev} ({team:Spartak Moscow}) right footed shot from outside the box is saved in the bottom left corner. Assisted by {player:Sofiane Hanni}.
Attempt blocked. VÃ­ctor Ruiz ({team:Villarreal}) header from the centre of the box is blocked. Assisted by {player:Miguel LayÃºn}.
Substitution, {team:Spartak Moscow}. {player:Artem Timofeev} replaces {player:Dmitri Kombarov}.
Foul by {player:Fernando} ({team:Spartak Moscow}).
{player:Pablo Fornals} ({team:Villarreal}) wins a free kick in the attacking half.
Attempt saved. {player:Ramiro Funes Mori} ({team:Villarreal}) header from the centre of the box is saved in the centre of the goal. Assisted by {player:Miguel LayÃºn}.
{player:Nikolai Rasskazov} ({team:Spartak Moscow}) is shown the yellow card for a bad foul.
Foul by {player:Nikolai Rasskazov} ({team:Spartak Moscow}).
{player:Miguel LayÃºn} ({team:Villarreal}) wins a free kick in the attacking half.
Goal!  {team:Spartak Moscow} 3, {team:Villarreal} 2. {player:Lorenzo Melgarejo} ({team:Spartak Moscow}) right footed shot from the centre of the box to the bottom right corner.
{player:Sofiane Hanni} ({team:Spartak Moscow}) hits the left post with a right footed shot from outside the box. Assisted by {player:ZÃ© LuÃ­s}.
Offside, {team:Spartak Moscow}. {player:Georgi Dzhikiya} tries a through ball, but {player:ZÃ© LuÃ­s} is caught offside.
{player:Fernando} ({team:Spartak Moscow}) is shown the yellow card.
Offside, {team:Villarreal}. {player:Alfonso Pedraza} tries a through ball, but {player:Gerard Moreno} is caught offside.
Goal!  {team:Spartak Moscow} 2, {team:Villarreal} 2. {player:ZÃ© LuÃ­s} ({team:Spartak Moscow}) header from very close range to the bottom right corner. Assisted by {player:Aleksandr Tashaev} with a cross.
{player:Gerard Moreno} ({team:Villarreal}) is shown the yellow card.
{player:Georgi Dzhikiya} ({team:Spartak Moscow}) is shown the yellow card for a bad foul.
Foul by {player:Georgi Dzhikiya} ({team:Spartak Moscow}).
{player:Gerard Moreno} ({team:Villarreal}) wins a free kick in the attacking half.
Substitution, {team:Spartak Moscow}. {player:Aleksandr Tashaev} replaces {player:Alexander Lomovitskiy}.
Attempt missed. {player:Alexander Lomovitskiy} ({team:Spartak Moscow}) right footed shot from the centre of the box is close, but misses to the left. Assisted by {player:Fernando}.
Attempt missed. {player:Fernando} ({team:Spartak Moscow}) right footed shot from outside the box misses to the left.
{player:ZÃ© LuÃ­s} ({team:Spartak Moscow}) wins a free kick in the defensive half.
Foul by {player:Ramiro Funes Mori} ({team:Villarreal}).
Offside, {team:Spartak Moscow}. {player:Fernando} tries a through ball, but {player:Lorenzo Melgarejo} is caught offside.
Substitution, {team:Villarreal}. {player:Miguel LayÃºn} replaces {player:Karl Toko Ekambi}.
{player:Roman Zobnin} ({team:Spartak Moscow}) wins a free kick in the defensive half.
Foul by {player:Gerard Moreno} ({team:Villarreal}).
{player:Sofiane Hanni} ({team:Spartak Moscow}) hits the left post with a right footed shot from outside the box. Assisted by {player:Lorenzo Melgarejo}.
Offside, {team:Villarreal}. {player:Pablo Fornals} tries a through ball, but {player:Karl Toko Ekambi} is caught offside.
{player:Georgi Dzhikiya} ({team:Spartak Moscow}) wins a free kick in the defensive half.
Foul by {player:Gerard Moreno} ({team:Villarreal}).
Substitution, {team:Villarreal}. {player:Santiago Cazorla} replaces {player:Samuel Chukwueze}.
Attempt blocked. {player:Samuel Chukwueze} ({team:Villarreal}) left footed shot from the right side of the box is blocked. Assisted by {player:Pablo Fornals}.