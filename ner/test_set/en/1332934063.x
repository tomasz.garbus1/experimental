::Alfreton Town character impresses manager Nicky Law::
@1332934063
url: https://www.bbc.com/sport/football/17536323
Alfreton Town manager Nicky Law says his players deserve huge credit for the character they have shown in their Blue Square Bet Premier relegation fight.
Credit to the players. They have come in every day, worked hard and taken the knocks
The Reds have spent the bulk of the season in the bottom four, but five wins from seven games has seen them move eight points clear with six game left.
"It would've been easy, having spent 90% of the season in the bottom four, to give it up as a bad job. But we didn't," Law told BBC Radio Derby.
"The players have stuck at it."
Alfreton have only lost one of their last seven fixtures - the 6-3 defeat against high-flying local rivals Mansfield Town.
And Tuesday's 2-1 away victory over Cambridge means they are almost certain to survive.
Law added: "We have had a great month.
"Credit to the players. They have come in every day, have worked hard, taken the knocks and now it looks like they will reap the rewards." 