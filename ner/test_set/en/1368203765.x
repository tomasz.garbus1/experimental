::Juventus boss Antonio Conte tempted by Premier League::
@1368203765
url: https://www.bbc.com/sport/football/22483636
Juventus coach Antonio Conte says he could be tempted by a move to the Premier League after steering the Turin side to back-to-back Serie A titles.
"It's not a secret the fact that I would like to work abroad" Conte told the BBC's World Football programme. 
"I hope, in the future, to be a coach in the Premier League," the 43-year-old added in English, after conducting the rest of the interview in Italian.
Conte has previously said he wants to discuss his future with Juve officials.
"Linked with the prospective vacancies at Chelsea and Paris Saint-Germain, a number of Italy's sports papers have wondered whether there's a chance Conte, who signed a new contract until 2015 only last summer, may leave Turin.
"A more nuanced interpretation of his comments would be that he is reminding Juventus that winning the Scudetto isn't enough. It was once, but it isn't anymore."
Read more on Conte's future
But the former midfielder, who played 419 times for Juventus, also admits he will find it difficult to break off his strong relationship with the club.
"I'm in the place I've always dreamt to be when I started my career as a manager," explained Conte 
"I've made my dream come true, we've won two consecutive titles, now the bar concerning our expectations is being raised exponentially. 
"So it's fair, as we do every year, that we sit together with the club, the ownership and the management to assess our needs and to see if everybody agrees on the path to follow".
Even if Conte stays to honour a contract that runs to 2015, he says that the chance to test himself elsewhere in Europe is a long-standing ambition.
"I didn't go abroad as as a football player. I would love to do it as a manager. I have this aspiration, as this would enable me to improve my professional career. And the time will come for that," he added.
Unbeaten in the league last season, Juventus dominated from the start of 2012-13, sealing the title with three matches remaining. 
However, the campaign was not without its challenges.
Conte was absent from the touchline from August through to mid-December as he served a ban for failing to report an attempt to fix a match during his time as manager of Siena. Conte has always denied the charge.
He admits the ban was difficult and says the Italian Supercoppa against Napoli, his first match banished from the touchline, was the saddest moment of the season.
His partner Elisabetta even offered to build a dug-out at home to help her husband cope, but lifting the Scudetto has made the pain worth it.
"Winning a title as a manager is different from winning it as a player, it's more fulfilling" Conte explains. 
"You really feel as if it's your title, because of the daily work with the players, because of the shape and mentality you manage to give to the whole team. So winning as a manager is more satisfying than winning as a player, at least to me".
You can listen to the whole interview with Antonio Conte, along with reflections from Sir Alex Ferguson's Cup Winners' Cup- winning Aberdeen captain Willy Miller on his former boss's retirement from the Manchester United hot-seat, on the latest edition of BBC World Service's World Football podcast.