::Dean Saunders insists adversity inspires Wrexham lead::
@1314698865
url: https://www.bbc.com/sport/football/14714113
Dean Saunders insists Wrexham's summer of adversity is the inspiration behind the Welsh club's rise to the top of the Blue Square Bet Premier League. 
The Wrexham manager has had to deal with his players and staff not being paid as their future looked uncertain.
But Wrexham's 2-0 win over Fleetwood Town on Monday was their fifth victory in a row and they lead the table from Gateshead on goal difference. 
Wrexham Supporters' Trust (WST) recently voted to buy the club.
"Probably the thing that has helped us is the adversity," said Saunders.
"Through the wages not getting paid and the club nearly going bump. 
"I said to the players: 'Come on, if we get through this we'll come out a stronger unit,' and we have. Simple as that. 
"And there is no reason why we can't keep it going."
Current Wrexham co-owner Geoff Moss put the Racecourse club up for sale and admitted in July the club had "run out of money."
The WST board will now conclude its negotiations with current owners Moss and Ian Roberts and submit their plans to the football authorities.
Wrexham fans had previously helped raise more than £100,000 towards a £250,000 bond which had to be paid to the Football Conference to ensure the club could compete in the 2011-2012 season.
Now Saunders wants Wrexham to capitalise on their good start and win promotion back to the Football League after an absence of four years
"With all the aggro, I couldn't have dreamt about the start we have had," said the former Wales international.
"We're on top of the league and we have got to enjoy it because we get enough stick when we're not.
"We need to avoid complacency and realise the bench is starting to thicken up a bit now.
"Dean Keates is back from injury and Chris Westwood and Glen Little will be available next week.
"I have Mathias Pogba and Adrian Cieslewicz sitting on the bench. If you don't play well, you're out. 
"And if you keep winning, you have more chance staying in the team so they all know that and I think they are self-motivated.
"What they have done in the summer showed much how much they cared for the club - and I have had nothing but support.
"I'm proud to be manager of the club, it is a brilliant football club and the players are lucky to be playing for the club.
"If we all knuckle down and work hard, we can get out of this league and the next one."