::{team:Barcelona} 2-1 {team:Celtic}::
@1351024811
url: https://www.bbc.com/sport/football/19958293
{team:Celtic} produced a performance full of heart and tactical discipline but lost out to an injury-time {player:Jordi Alba} goal.
The visitors led after the presence of {player:Georgios Samaras} helped force {player:Javier Mascherano} to divert a {player:Charlie Mulgrew} free-kick into his own net.
{team:Barcelona} were not at their best, but produced a moment of sheer class to allow {player:Andres Iniesta} to equalise.
{team:Celtic} stood firm in the face of relentless pressure until the dying seconds when {player:Alba} stole in to score.
It was incredibly cruel on {team:Celtic}, who were outstanding to a man.
They remain second in {league:Champions League} Group G but now trail {team:Barcelona}, who maintain their 100% record, by five points. {team:Spartak Moscow}, who beat {team:Benfica} earlier in the day, are a point behind {manager:Neil Lennon}'s side with the Portuguese side bottom with a single point.
It had looked ominous for the visitors as early as the second minute when {team:Barcelona} sliced through their defence for the first time, {player:Iniesta} providing the killer ball, only for {player:Alexis Sanchez} to dink the ball wide of {player:Fraser Forster}'s right-hand post.
That apart though, {team:Celtic} began the match comfortably, coping well with the constant passing and movement of {team:the Catalan side}.
{player:Forster}'s first major test came 17 minutes in, and he dealt with it brilliantly.
{player:Lionel Messi} floated the ball over the {team:Celtic} defence, {player:Iniesta} turned it across goal where {player:Marc Bartra} flashed a header on target, only for the giant {team:Celtic} keeper to push it away.
And moments later, {team:Celtic} shocked the hosts by moving in front.
A {player:Mulgrew} free-kick from the right was attacked by {player:Samaras}, but it was {player:Mascherano} who inadvertently knocked the ball past a static {player:Victor Valdes}.
{team:Barca} had dominated possession, but {team:Celtic}'s organisation was superb and there was almost half an hour gone before the home side fashioned another decent chance.
Again {player:Iniesta} was the source, but his chip was headed wide by {player:Bartra}.
Then, as the {team:Barcelona} pressure grew in intensity, {player:Kelvin Wilson} threw himself in the way of a {player:Xavi} drive to divert it over the bar and {player:Adriano Correia} tested {player:Forster} at his near post from the resulting corner.
{player:Messi}, by his own high standards, was having a quiet game but worried {player:Forster} with a couple of free-kicks that curled inches off target.
{team:Celtic} were dealt an enormous blow just before the break as Samaras was forced off, having rolled his ankle as he was fouled contesting a high ball. 
And his replacement {player:James Forrest} had not touched the ball before Barca drew level in exquisite fashion.
{player:Messi} found {player:Iniesta} just inside the box and a lightning-quick one-two with {player:Xavi} allowed {player:Iniesta} to direct the ball just out of {player:Forster}'s reach and inside his left-hand post.
The timing of the equaliser was tough on {team:Celtic}, who had executed their manager's tactical instructions almost to perfection.
They nearly moved back in front seven minutes after the interval.

                            
                        
Again, the opportunity arose from a {player:Mulgrew} set-piece - this time a corner from the right - but {player:Victor Wanyama} failed to test {player:Valdes} from an excellent position.
{team:Celtic} lost the tenacity of {player:Scott Brown} with just over an hour gone, his long-term hip problem seemingly taking its toll once more.
Soon after, {player:Forster} held efforts from both {player:Messi} and then {player:Iniesta} from outside the box as {team:Barcelona} upped the ante yet further.
Midway through the second half, {player:Alexis Sanchez} fed {player:Pedro Rodriguez} on the right of the {team:Celtic} box and when his low cross found {player:Messi} on the six-yard box, a second {team:Barca} goal seemed inevitable, but Forster stuck out a hand and somehow denied {player:the Argentine}.
The {team:Celtic} keeper was performing heroically and he was swiftly off his line to deny {player:Alexis} before an unbelievable flying save kept {player:Messi}'s diving header from finding the back of the net.
{team:Celtic} were becoming more and more hemmed in, but a wayward {player:Xavi} effort told the story of {team:Barcelona}'s increasing frustration as time ticked away.
{team:Barca} substitute {player:David Villa} struck a post late on as it appeared {team:Celtic} would hold on, but in the fourth minute of injury time, {player:Alba} sneaked in at the back post to knock {player:Adriano}'s cross past the helpless {player:Forster} from just a yard out.
Despite the bitter pang of disappointment, {team:Celtic} can be hugely proud of their side's display ahead of the return fixture against {manager:Tito Vilanova}'s side in Glasgow on 7 November.
Live text commentary