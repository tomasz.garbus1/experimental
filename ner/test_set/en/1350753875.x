::West Ham 4-1 Southampton::
@1350753875
url: https://www.bbc.com/sport/football/19939627
West Ham earned their fourth victory of the season after a Mark Noble brace and goals for Kevin Nolan and Modibo Maiga sealed all three points. 
Southampton pulled one back through Adam Lallana but West Ham's powerful second-half display proved too much for Nigel Adkins' side. 
Noble opened the scoring with a fortuitous free kick while Nolan later tapped home at the back post before Lallana made it 2-1 with a delightful turn and shot. 
But Noble sealed victory from the penalty spot before Maiga scored a wonderful solo goal to compound Southampton's misery. 
Victory consolidated West Ham's position in the top half of the table while Saints slip into the bottom three, after Norwich's win against Arsenal in Saturday's late kick off.
The game was evenly poised at the break but Southampton's early season demons continue to haunt them with yet more poor defending denying them any chance of getting something from this game.
Adkins cut a forlorn figure in the post match press conference, bemoaning his side's defending, and he will know that something will need to change at the back or his side will quickly turn into relegation fodder.
They have now conceded 24 goals this season - a return that is quite simply not good enough.
The corresponding fixture last season saw Saints earn a valuable draw that eventually helped them gain automatic promotion from the Championship at the expense of West Ham.
Southampton have trailed in all eight Premier League games at some point, the only team to be behind in all of their league games.
The Hammers had to rely on the lottery of the play-offs to go up but despite missing out to Saints and Reading on those automatic places, it is Sam Allardyce's experienced West Ham side who have made the better start to the new Premier League campaign. 
With 14 points already, the Hammers have a healthy return from their opening eight games while Saturday's game was further evidence that Southampton are yet to settle into Premier League life. 
Saints took the decision to drop star striker Rickie Lambert prior to the game but the visiting side made a bright start without creating a clear opening. The first real chance fell West Ham's way with Jos Hooiveld heading James Tomkins' looping header from 12 yards off the line. 
James Collins later went close with a header but by and large this was a poor first half played in a turgid atmosphere with the moans and groans of the Upton Park faithful growing louder as the half wore on. 
Allardyce also cut a frustrated figure on the sidelines chewing gum with more urgency and intensity than his lack-lustre side, while for Adkins it was a case of job done as the teams went into the half-time break level. 
But it took just 58 seconds for the Hammers to buck up their ideas and typically it came from poor Southampton defending with no-one dealing with Noble's inswinging 35-yard free kick that eventually bounced in at the back post. 
It was the 21st goal Saints had conceded this season, a symptom of their early-season troubles and just a minute later there was worse to come for Adkins' side. 
Yossi Benayoun showed more desire to get to the ball than Maya Yoshida and his cross to the back post was tapped into the empty net by Nolan. 

                            
                        
With Upton Park lifted and the supporters buoyed, Southampton's confidence evaporated as the away fans called for their talisman to be introduced - Lambert. 
But it was the man that was playing up front in his place - Jay Rodriguez - who helped get Saints back in the game, showing quick feet to turn inside Collins before releasing Lallana. With his back to goal the midfielder swivelled delightfully before powering a shot into the roof of the net. 
Lambert was then introduced and he went desperately close with a free kick from 20 yards. 
But the Hammers soon restored their two-goal advantage with another Noble free kick causing Saints problems and Jose Fonte was penalised for handball after coming under pressure from Andy Carroll. 
Noble powered in the spot kick to a collective sigh of relief and Carroll continued to cause Hooiveld problems at the back before he was replaced to a standing ovation.
The on-loan Liverpool striker is quickly on his way to becoming a cult hero at Upton Park. 
West Ham pressed for a fourth and it came late on through Maiga who sealed the rout with a wonderful solo goal that embarrassed three Southampton defenders.
Live text commentary