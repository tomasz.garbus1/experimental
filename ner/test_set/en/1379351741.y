::{team:AC Milan}'s {player:Kaka} 'asks not to be paid' while injured::
@1379351741
url: https://www.bbc.com/sport/football/24117343
{team:AC Milan} midfielder {player:Kaka} has asked to go without pay as he recovers from a thigh injury picked up in his first game since rejoining {team:the Italian side}.
The {country:Brazil} international, 31, was substituted in the 70th minute of {team:the Rossoneri}'s 2-2 draw at {stadium:Torino}.
He returned to {team:Milan} on a free earlier this month after leaving for {team:Real Madrid} for a world-record £56m in 2009.
"I don't want anything from {team:Milan}, except for love and support, until I'm fully fit and ready to play," he said.
"For this reason I have decided to suspend my current pay for this period of time. The only thing I ask is for the support and help to recover properly.
"It is a difficult moment but I have started to work on getting better and I hope to get back as quickly as possible."
{player:Kaka} added he had spoken "at length" with {team:Milan} vice-president {manager:Adriano Galliani} and club doctors before deciding to ask not to be paid.
It is not clear whether {team:Milan} have accepted the player's offer.
{player:Kaka}, the 2007 World Player of the Year, made 268 appearances for {team:Milan} between 2003 and 2009, scoring 95 goals. He helped them win the {league:Serie A} title in 2004 and the 2007 {league:Champions League}.
In 2010, {team:Milan} defender {player:Oguchi Onyewu} agreed a new one-year deal for no salary after he had spent a year on the sidelines.
In 2004-05, {team:Roma} and {country:Italy} midfielder {player:Damiano Tommasi} played for £1,200 a month while recovering from a serious knee injury.