::{manager:David Wagner}: Ex-{team:Huddersfield} boss set to become {team:Schalke} head coach::
@1556624051
url: https://www.bbc.com/sport/football/48107737
Former {team:Huddersfield} boss {manager:David Wagner} is set to return to football as coach of {league:Bundesliga} club {team:Schalke}.
{manager:Wagner} took a break from the game after he left {team:the Terriers} by mutual consent on 14 January.
However, the 47-year-old was always keen to return to management, either in {country:England} or {country:Germany}.
And BBC Sport understands {manager:Wagner} has been in talks with {team:Schalke} about replacing interim boss {league:Huub Stevens} and becoming their head coach.
{team:Schalke} have had a difficult campaign and only eased their relegation worries with a surprise win over local rivals {team:Borussia Dortmund} on Saturday.
They appointed club legend {manager:Stevens} as interim manager to the end of the season last month following the departure of {manager:Domenico Tedesco}, who was sacked on 14 March following the embarrassing 7-0 {league:Champions League} defeat by {team:Manchester City}.
{manager:Wagner} spent four years as coach of {team:Borussia Dortmund}'s second team before joining {team:Huddersfield} in 2015.
He guided {team:the Terriers} to the {league:Premier League} in 2017 and then managed the even more remarkable achievement of keeping them there last season.
However, he left the club earlier this year with the now-relegated side bottom of the {league:Premier League}.