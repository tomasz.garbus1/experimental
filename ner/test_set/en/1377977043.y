::{team:Guernsey FC} seal place in qualifying round with {league:FA Cup} win::
@1377977043
url: https://www.bbc.com/sport/football/23917135
{team:Guernsey FC} won their first ever {league:FA Cup} fixture beating {team:Crawley Down Gatwick} 3-1 to progress to the first qualifying round of the competition.
{team:The Green Lions} took an early lead with goals from {player:Marc McGrat}h and man-of-the-match {player:Matt Loaring}.
The hosts, , got themselves back into the match with a penalty shortly before half-time. 
But {player:Ross Allen} sealed a famous win scoring a second-half penalty. 