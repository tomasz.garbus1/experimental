::{team:Dunfermline} players face wage payment problems::
@1354574657
url: https://www.bbc.com/sport/football/20589792
{team:Dunfermline} players will receive only 25% of their salaries on time this month, BBC {country:Scotland} has learned.
The payment of wages at {team:Dunfermline} was also delayed in October as the club struggled to pay a tax bill.
Club owner Gavin Masterton told players at a meeting on Monday that the remainder would be paid within a week.
Masterton assured the squad that he was hopeful that the club would be on an even financial footing at the start of the new year.
{team:Dunfermline} are believed to be about £8m in debt, with present and past directors the main debtors. 