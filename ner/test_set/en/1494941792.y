::{player:Stephen Dawson}: {team:Bury} re-sign midfielder from {team:Scunthorpe United}::
@1494941792
url: https://www.bbc.com/sport/football/39939081
{league:League One} side {team:Bury} have re-signed midfielder {player:Stephen Dawson} from {team:Scunthorpe} on a three-year deal.
The 31-year-old ex-{country:Republic of Ireland} Under-21 midfielder spent two seasons with {team:the Shakers} between 2008 and 2010 when he captained the side.
The former {team:Leicester City} trainee was out of contract at {team:the Iron}, but had been offered a new deal.
"This is a huge signing for us. He is a player that I have tried to entice at previous clubs," said boss {manager:Lee Clark}.
{player:Dawson} joins striker {player:Jermaine Beckford} and full-back {player:Phil Edwards} in signing for {team:Bury} for next season.
Find all the latest football transfers on our dedicated page.