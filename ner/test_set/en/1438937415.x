::Truro City prepare for 666-mile opening trip to Margate::
@1438937415
url: https://www.bbc.com/sport/football/33816595
Truro City manager Steve Tully says his side could not have had a tougher opening game of the season as they make a 666-mile round-trip to Margate
Truro are back in National League South after winning promotion from the Southern Premier League in May.
"I don't think we could have asked for a harder first game back," said Tully.
"It's the longest journey in the league but we'll deal with it, we'll get it over and done with," Tully added to BBC Radio Cornwall.
But the journey is not the longest for a Truro City football match - Whitley Bay had a 921-mile round-trip when they beat Truro in the FA Vase in 2008.
"Everything's against us, but for me it's one where we can play our football and see what happens," said Tully.
"We're really looking forward to it, we'll be really well prepared, we're travelling up on Friday to give our players the best chance possible, so we'll give it a go."