::Rangers: Sandy Easdale loan prevents winding up of Ibrox club::
@1420452667
url: https://www.bbc.com/sport/football/30678270
Rangers' latest crisis loan was taken to avoid being wound up by HM Revenue and Customs, BBC Scotland has learned.
It is understood the Scottish Championship club received a seven-day notice letter at the end of December for the payment of national insurance. 
Rangers announced on Monday that Sandy Easdale had given a loan of £500,000.
The shareholder and chairman of the football board's money would be used for general working capital over the next few days. 
Rangers International Football Club also confirmed that it had received an £18m takeover approach from American financier Robert Sarver.
He has been given until 2 February to make a firm offer for the Glasgow club.
Two groups of fans have subsequently made share purchases in the club since the owner of Phoenix Suns basketball team made his approach.
And those moves by Douglas Park's group and  mean the American is unlikely to succeed in a takeover in its original form.
Announcing the latest loan to the Stock Exchange, a statement said: "The facility is being provided by Alexander Easdale, a shareholder in the company and director of The Rangers Football Club Limited, the wholly owned subsidiary of RIFC.
"The facility will be used by the company for general working capital purposes over the next few days."
A spokesman for Sandy and James Easdale, his brother and business partner who has a seat on the plc board, declared the latest loan had been the only option for Rangers.
Jack Irvine, an Easdale family adviser, said: "Once again, Sandy has stepped up to the plate with this half million pound loan from his own pocket. 
"Whilst we welcomed the recent share purchases by Dave King and Douglas Park and his consortium, this unfortunately did not put any funds into the club.
"Sandy was the only option for this cash injection at such short notice. The Easdale family remain totally committed to achieving a satisfactory financial future for Rangers and they hope all parties can work together in the future with that common goal."
Rangers sold prize asset Lewis Macleod to Brentford, with the deal for the 20-year-old midfielder being completed immediately after the opening of the January transfer window.
"Alexander Easdale will make available to the company up to £500,000 on a fee and interest free basis and it will be secured against the income from the sale of player announced on 2 January 2015," added Rangers' statement.
"The directors of Rangers, having taken advice from their nominated adviser, WH Ireland plc, believe that the terms of the facility are fair and reasonable as far as shareholders are concerned."
Meanwhile, Rangers have promised a further announcement in response to Sarver's interest.
"There can be no certainty that an offer will be made, nor as to the terms on which an offer may be made," said a statement.
It pointed out that, under takeover and mergers regulations, the 52-year-old must announce if he is going to make a firm intention to make an offer for Rangers by 2 February.
Should he make such a bid, he "will be required to clarify his intentions".
Meanwhile, the Scottish Professional Football League is satisfied that no default event has occurred in relation to the winding-up order. 
A default event, according to the SPFL's rule E20, occurs 28 days after a tax bill is due.