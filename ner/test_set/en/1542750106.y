::{team:Doncaster Rovers} 7-0 {team:Chorley}: {player:Alfie May} scores four as {team:Rover} thrash non-league side::
@1542750106
url: https://www.bbc.com/sport/football/46189621

                            
                        
Five goals in the opening 36 minutes helped ensure {team:Doncaster Rovers} avoided an {country:FA Cup} first-round upset as they thrashed sixth-tier {team:Chorley}.
{player:Alfie May} scored a 29-minute hat-trick as the {league:League One} side had the tie sealed well before the interval - and the forward could have had a fourth just before the break but missed from close range.
{player:Matty Blair} and {player:Herbie Kane} - who hit an impressive equaliser in the first tie - scored in-between May's first and second goals as {team:Rovers} controlled the opening half.
{team:Doncaster} had {player:Tom Anderson} sent off for a second booking with 30 minutes left, but it was {team:Rovers} who continued to dominate.
{player:John Marquis} had a 73rd-minute penalty saved by {player:Matt Unwin} but May slotted in his fourth late on before {player:Marquis} made amends for his miss from 12 yards, scoring the seventh goal with an excellent curling effort.
{team:Doncaster} are away against fellow {league:League One} side {team:Charlton Athletic} in the second round.
Match ends, {team:Doncaster Rovers} 7, {team:Chorley} 0.
Second Half ends, {team:Doncaster Rovers} 7, {team:Chorley} 0.
Corner,  {team:Chorley}. Conceded by {player:Matty Blair}.
Attempt missed. {player:Paul Taylor} ({team:Doncaster Rovers}) right footed shot from the centre of the box misses to the left.
Attempt missed. {player:Alfie May} ({team:Doncaster Rovers}) header from the centre of the box is close, but misses to the left.
Corner,  {team:Doncaster Rovers}. Conceded by {player:Stephen Jordan}.
Goal!  {team:Doncaster Rovers} 7, {team:Chorley} 0. {player:John Marquis} ({team:Doncaster Rovers}) left footed shot from the right side of the box to the top left corner. Assisted by {player:Matty Blair}.
Goal!  {team:Doncaster Rovers} 6, {team:Chorley} 0. {player:Alfie May} ({team:Doncaster Rovers}) left footed shot from the centre of the box to the bottom right corner.
Foul by {player:John Marquis} ({team:Doncaster Rovers}).
{player:Andrew Teague} ({team:Chorley}) wins a free kick on the left wing.
Corner,  {team:Doncaster Rovers}. Conceded by {player:Matthew Urwin}.
Penalty saved! {player:John Marquis} ({team:Doncaster Rovers}) fails to capitalise on this great opportunity,  right footed shot saved  in the bottom right corner.
Penalty {team:Doncaster Rovers}. {player:John Marquis} draws a foul in the penalty area.
Penalty conceded by {player:Matthew Urwin} ({team:Chorley}) after a foul in the penalty area.
Substitution, {team:Chorley}. {player:Reuben Noble-Lazarus} replaces {player:Josh Wilson}.
Substitution, {team:Doncaster Rovers}. {player:Ali Crawford} replaces {player:Herbie Kane}.
Attempt missed. {player:Paul Taylor} ({team:Doncaster Rovers}) right footed shot from outside the box misses to the right.
{player:Tommy Rowe} ({team:Doncaster Rovers}) is shown the yellow card for a bad foul.
Attempt missed. {player:Paul Taylor} ({team:Doncaster Rovers}) right footed shot from outside the box is too high.
Attempt blocked. {player:Andrew Teague} ({team:Chorley}) right footed shot from outside the box is blocked.
Substitution, {team:Chorley}. {player:Stephen Jordan} replaces {player:Scott Leather} because of an injury.
Attempt missed. {player:John Marquis} ({team:Doncaster Rovers}) left footed shot from the left side of the box is too high.
Delay in match {player:Scott Leather} ({team:Chorley}) because of an injury.
Attempt blocked. {player:Herbie Kane} ({team:Doncaster Rovers}) right footed shot from long range on the left is blocked.
{player:Alfie May} ({team:Doncaster Rovers}) wins a free kick in the defensive half.
Foul by {player:Scott Leather} ({team:Chorley}).
Substitution, {team:Chorley}. {player:Dale Whitham} replaces {player:Alex Newby}.
Second yellow card to {player:Tom Anderson} ({team:Doncaster Rovers}) for a bad foul.
Foul by {player:Tom Anderson} ({team:Doncaster Rovers}).
{player:Louis Almond} ({team:Chorley}) wins a free kick on the right wing.
Attempt blocked. {player:John Marquis} ({team:Doncaster Rovers}) right footed shot from the centre of the box is blocked.
Attempt missed. {player:Tommy Rowe} ({team:Doncaster Rovers}) left footed shot from outside the box misses to the right.
{player:Herbie Kane} ({team:Doncaster Rovers}) wins a free kick in the defensive half.
Foul by {player:Jake Cottrell} ({team:Chorley}).
{player:Tom Anderson} ({team:Doncaster Rovers}) is shown the yellow card for a bad foul.
Foul by {player:Tom Anderson} ({team:Doncaster Rovers}).
{player:Courtney Meppen-Walter} ({team:Chorley}) wins a free kick in the defensive half.
Foul by {player:Niall Mason} ({team:Doncaster Rovers}).
{player:Josh Wilson} ({team:Chorley}) wins a free kick in the defensive half.
Attempt blocked. {player:Andy Butler} ({team:Doncaster Rovers}) right footed shot from the centre of the box is blocked.