::{player:Ashley Vincent} returns to {team:Cheltenham} from {team:Port Vale}::
@1374063160
url: https://www.bbc.com/sport/football/23345813
Winger {player:Ashley Vincent} has re-signed for {team:Cheltenham Town} after four years away from the {league:League Two} club.
The 28-year-old made 131 appearances for {team:the Robins} during his first spell at {stadium:Whaddon Road} between 2004 and 2009.
He went on to play for {team:Colchester} and {team:Port Vale}, where he scored eight goals in 40 games last season.
{player:Vincent} was offered a new deal by {team:Vale} in May, but turned it down. "In football terms, this is my home," he told the {team:Cheltenham} website. 
"It is slightly surreal, but this is a decision for my family and this is a family club. 
"I'm buzzing to be back and I can't wait to play for {team:Cheltenham} again and hopefully win promotion."
{player:Vincent} has agreed a one-year deal with the {player:Robins} for the 2013-14 {league:League Two} campaign.