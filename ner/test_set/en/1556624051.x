::David Wagner: Ex-Huddersfield boss set to become Schalke head coach::
@1556624051
url: https://www.bbc.com/sport/football/48107737
Former Huddersfield boss David Wagner is set to return to football as coach of Bundesliga club Schalke.
Wagner took a break from the game after he left the Terriers by mutual consent on 14 January.
However, the 47-year-old was always keen to return to management, either in England or Germany.
And BBC Sport understands Wagner has been in talks with Schalke about replacing interim boss Huub Stevens and becoming their head coach.
Schalke have had a difficult campaign and only eased their relegation worries with a surprise win over local rivals Borussia Dortmund on Saturday.
They appointed club legend Stevens as interim manager to the end of the season last month following the departure of Domenico Tedesco, who was sacked on 14 March following the embarrassing 7-0 Champions League defeat by Manchester City.
Wagner spent four years as coach of Borussia Dortmund's second team before joining Huddersfield in 2015.
He guided the Terriers to the Premier League in 2017 and then managed the even more remarkable achievement of keeping them there last season.
However, he left the club earlier this year with the now-relegated side bottom of the Premier League.