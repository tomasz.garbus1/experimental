::{team:Cardiff City} 0-1 {team:Wolves}::
@1523048667
url: https://www.bbc.com/sport/football/43592741
{team:Cardiff City} missed two stoppage-time penalties as they lost to {league:Championship} leaders {team:Wolves} in a dramatic finale.
At a packed {stadium:Cardiff City Stadium}, {player:Ruben Neves} scored a brilliant second-half free-kick from 25 yards.
After being bundled over in the box, {team:Cardiff}'s {player:Gary Madine} saw his low penalty saved by {player:John Ruddy}.
Moments later, {player:Junior Hoilett} blasted a spot-kick against the bar as {team:Wolves} extended their lead at the top of the table to nine points.
They need only five points from the final five games of the season to seal a return to the {league:Premier League}.
The stupefying finish was a stark contrast to what had otherwise been something of a slow burner - an absorbing but unspectacular encounter between two very different but well-matched teams.
It was also a demoralising end to the game for {team:Cardiff}, who were twice given hope of rescuing a point when referee Mike Dean pointed to the spot - only for their missed penalties to consign them to a first defeat in 14 games.
Their despair was in startling contrast to the jubilation of their opponents, whose coaches and substitutes rushed on to the pitch at the full-time whistle, while their travelling supporters celebrated with riotous gusto in the stands.
The {team:Wolves} celebrations seemed to anger {team:Cardiff} manager {manager:Neil Warnock}, who refused to shake hands with counterpart {manager:Nuno Espirito Santo} at full-time and said after the game that he "did not want to see" {manager:the Portuguese}.
While {team:Wolves} need only two wins to secure promotion, {team:Cardiff} remain five points clear of third-placed {team:Fulham} in a battle for the second automatic promotion place which promises to go down to the wire.
{team:Fulham} visit {team:Sheffield Wednesday} on Saturday and, with {team:the Londoners} on an 18-game unbeaten run, {team:Cardiff} will be looking over their shoulders.
Despite their advantage over {team:Fulham}, {team:Warnock} has always said the play-offs are {team:Cardiff}'s target, although this is more than likely a little mischievous from the wily veteran of seven promotions.
{team:Cardiff} faced {team:Wolves} in the knowledge they needed only a point to be guaranteed at least a top-six spot but, like their opponents here, they had their eyes on a bigger and better prize.
With a record league crowd of 29,317 gathered under the lights at {stadium:Cardiff City Stadium}, there was a frisson of anticipation for this meeting between the {league:Championship}'s two leading sides.
It was an intriguing clash of styles, with the patient, technically accomplished visitors confronted by an abrasive, functional {team:Cardiff} side, built very much in the image of their manager.
The hosts looked to fuel the fervent atmosphere with their direct approach, {player:Sol Bamba} heading one of their many dangerous set-pieces wide.
{team:Wolves}, by contrast, sought a more measured approach, dominating possession and slowing the tempo as they built their attacks gradually, with {player:Neves} and {player:Diogo Jota} both testing home goalkeeper {player:Neil Etheridge} with powerful efforts.
This was the style which had kept {manager:Santo}'s men at the top of the {league:Championship} since October, and it paid dividends once again against their closest rivals for the title.
After {player:Leo Bonatini} rounded {player:Etheridge} and struck the post, {player:Neves} sent the travelling fans wild with a brilliant curling free-kick from 25 yards.
The real drama, however, came in added time.
{team:Wolves} captain {player:Conor Coady} had his head in his hands after {player:Dean} adjudged him to have fouled {player:Madine} in the area, although he was given a reprieve when {player:Ruddy} - diving low to his left - saved the striker's poorly struck spot-kick.
Astonishingly, another penalty followed moments later when {player:Aron Gunnarsson} was fouled.
But just as {team:Wolves} thought victory had been snatched away from them, {player:Hoilett} smashed his effort against the bar to spark scenes of unadulterated jubilation among the visitors.
{team:Cardiff City} manager {manager:Neil Warnock}: "It was a cracking game, a good advertisement. A great crowd and a fabulous, world-class goal for {team:Wolves}. Two good teams and a cracking atmosphere.
"I'm disappointed we haven't won, let alone get a point. It's one of those days when nothing seemed to go in.
"We just have to get on with it. We showed we're a decent team. 
"You're not going to get happy faces. They ({team:Cardiff}'s players) are all disappointed. It shows how far we've come, playing against the so-called best team in the {league:Championship} and we more than held our own."
{team:Wolves} manager {manager:Nuno Espirito Santo}: "Football is always innovating and it's a fantastic game. That's why we love it so much. It can give and take.
"We deserve it. If you look at the game, we had chances not to suffer in the end but that's why people voted for {player:John Ruddy} in the {league:Championship} team of the season and he gave us victory.
"I was not even thinking about the first penalty (when the second was awarded) but we should control the emotions better, which is my job."
Match ends, {team:Cardiff City} 0, {team:Wolverhampton Wanderers} 1.
Second Half ends, {team:Cardiff City} 0, {team:Wolverhampton Wanderers} 1.
Penalty missed! Still  {team:Cardiff City} 0, {team:Wolverhampton Wanderers} 1. {player:David Junior Hoilett} ({team:Cardiff City}) hits the bar with a right footed shot.
{player:RÃºben Neves} ({team:Wolverhampton Wanderers}) is shown the yellow card.
Penalty conceded by {player:Ivan Cavaleiro} ({team:Wolverhampton Wanderers}) after a foul in the penalty area.
Penalty {team:Cardiff City}. {player:Aron Gunnarsson} draws a foul in the penalty area.
Attempt saved. {player:Sean Morrison} ({team:Cardiff City}) header from very close range is saved in the top right corner. Assisted by {player:Gary Madine} with a headed pass.
Attempt blocked. {player:Anthony Pilkington} ({team:Cardiff City}) left footed shot from the centre of the box is blocked. Assisted by {player:David Junior Hoilett} with a headed pass.
Corner,  {team:Cardiff City}. Conceded by {player:John Ruddy}.
Penalty saved! {player:Gary Madine} ({team:Cardiff City}) fails to capitalise on this great opportunity,  right footed shot saved  in the bottom right corner.
Penalty conceded by {player:Conor Coady} ({team:Wolverhampton Wanderers}) after a foul in the penalty area.
Penalty {team:Cardiff City}. {player:Anthony Pilkington} draws a foul in the penalty area.
{player:HÃ©lder Costa} ({team:Wolverhampton Wanderers}) is shown the yellow card for dangerous play.
Dangerous play by {player:HÃ©lder Costa} ({team:Wolverhampton Wanderers}).
{player:Neil Etheridge} ({team:Cardiff City}) wins a free kick in the defensive half.
Attempt saved. {player:Ivan Cavaleiro} ({team:Wolverhampton Wanderers}) right footed shot from the right side of the box is saved in the bottom right corner. Assisted by {player:HÃ©lder Costa}.
Attempt saved. {player:Ivan Cavaleiro} ({team:Wolverhampton Wanderers}) right footed shot from outside the box is saved in the centre of the goal. Assisted by {player:Willy Boly}.
Attempt missed. {player:HÃ©lder Costa} ({team:Wolverhampton Wanderers}) left footed shot from the centre of the box is close, but misses to the left. Assisted by {player:RÃºben Neves}.
{player:HÃ©lder Costa} ({team:Wolverhampton Wanderers}) wins a free kick in the defensive half.
Foul by {player:Sol Bamba} ({team:Cardiff City}).
Attempt missed. {player:Aron Gunnarsson} ({team:Cardiff City}) right footed shot from outside the box is close, but misses the top left corner following a set piece situation.
Attempt missed. {player:Craig Bryson} ({team:Cardiff City}) left footed shot from outside the box misses to the right. Assisted by {player:Sean Morrison} with a headed pass following a set piece situation.
Substitution, {team:Cardiff City}. {player:Anthony Pilkington} replaces {player:Kenneth Zohore}.
Foul by {player:Ivan Cavaleiro} ({team:Wolverhampton Wanderers}).
{player:David Junior Hoilett} ({team:Cardiff City}) wins a free kick in the attacking half.
Attempt saved. {player:Alfred N'Diaye} ({team:Wolverhampton Wanderers}) left footed shot from the left side of the box is saved in the bottom left corner. Assisted by {player:Ivan Cavaleiro}.
Offside, {team:Cardiff City}. {player:Sean Morrison} tries a through ball, but {player:Sol Bamba} is caught offside.
Foul by {player:HÃ©lder Costa} ({team:Wolverhampton Wanderers}).
{player:Craig Bryson} ({team:Cardiff City}) wins a free kick in the attacking half.
Offside, {team:Cardiff City}. {player:Aron Gunnarsson} tries a through ball, but {player:Sean Morrison} is caught offside.
Substitution, {team:Wolverhampton Wanderers}. {player:Alfred N'Diaye} replaces Benik Afobe.
Corner,  {team:Cardiff City}. Conceded by {player:Barry Douglas}.
{player:Romain Saiss} ({team:Wolverhampton Wanderers}) is shown the yellow card for a bad foul.
Foul by {player:Romain Saiss} ({team:Wolverhampton Wanderers}).
{player:Nathaniel Mendez-Laing} ({team:Cardiff City}) wins a free kick on the left wing.
Attempt missed. {player:Kenneth Zohore} ({team:Cardiff City}) left footed shot from the left side of the box is high and wide to the left.
{player:Romain Saiss} ({team:Wolverhampton Wanderers}) wins a free kick in the defensive half.
Foul by {player:Gary Madine} ({team:Cardiff City}).
Substitution, {team:Cardiff City}. {player:Gary Madine} replaces {player:Yanic Wildschut}.
Attempt missed. {player:RÃºben Neves} ({team:Wolverhampton Wanderers}) right footed shot from outside the box is close, but misses to the right.