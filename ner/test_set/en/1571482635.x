::Motherwell v Aberdeen::
@1571482635
url: https://www.bbc.com/sport/football/50024358
Goal!  Motherwell 0, Aberdeen 2. Niall McGinn (Aberdeen) right footed shot from the centre of the box to the top left corner.
Barry Maguire (Motherwell) wins a free kick on the right wing.
Foul by Sam Cosgrove (Aberdeen).
Attempt missed. Sherwin Seedorf (Motherwell) right footed shot from outside the box misses to the right.
Second Half begins Motherwell 0, Aberdeen 1.
First Half ends, Motherwell 0, Aberdeen 1.
Foul by Sherwin Seedorf (Motherwell).
Shaleum Logan (Aberdeen) wins a free kick on the left wing.
Barry Maguire (Motherwell) wins a free kick on the right wing.
Foul by Sam Cosgrove (Aberdeen).
Corner,  Aberdeen. Conceded by Richard Tait.
Chris Long (Motherwell) wins a free kick in the attacking half.
Foul by Scott McKenna (Aberdeen).
Foul by Chris Long (Motherwell).
Shaleum Logan (Aberdeen) wins a free kick on the left wing.
Attempt missed. Liam Polworth (Motherwell) right footed shot from outside the box is just a bit too high from a direct free kick.
Sherwin Seedorf (Motherwell) wins a free kick in the defensive half.
Foul by Shaleum Logan (Aberdeen).
Attempt blocked. Chris Long (Motherwell) right footed shot from outside the box is blocked.
Chris Long (Motherwell) wins a free kick in the defensive half.
Foul by Michael Devlin (Aberdeen).
Chris Long (Motherwell) hits the right post with a right footed shot from a difficult angle on the right.
Attempt missed. Jon Gallagher (Aberdeen) header from the centre of the box is just a bit too high.
Substitution, Motherwell. Bevis Mugabi replaces Peter Hartley because of an injury.
Goal!  Motherwell 0, Aberdeen 1. Sam Cosgrove (Aberdeen) right footed shot from the centre of the box to the high centre of the goal. Assisted by Greg Leigh.
Attempt blocked. James Wilson (Aberdeen) left footed shot from the centre of the box is blocked.
Sam Cosgrove (Aberdeen) wins a free kick on the left wing.
Foul by Declan Gallagher (Motherwell).
Attempt saved. Sherwin Seedorf (Motherwell) right footed shot from outside the box is saved in the centre of the goal.
Foul by Niall McGinn (Aberdeen).
Liam Grimshaw (Motherwell) wins a free kick in the defensive half.
Corner,  Motherwell. Conceded by Joe Lewis.
Penalty saved! James Scott (Motherwell) fails to capitalise on this great opportunity,  right footed shot saved  in the top left corner.
Penalty conceded by Joe Lewis (Aberdeen) after a foul in the penalty area.
Penalty Motherwell. Chris Long draws a foul in the penalty area.
Attempt saved. James Scott (Motherwell) left footed shot from outside the box is saved in the centre of the goal.
Foul by Chris Long (Motherwell).
Scott McKenna (Aberdeen) wins a free kick in the attacking half.
Attempt saved. Sam Cosgrove (Aberdeen) left footed shot from outside the box is saved in the centre of the goal.
Sam Cosgrove (Aberdeen) wins a free kick in the defensive half.