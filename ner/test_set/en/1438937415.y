::{team:Truro City} prepare for 666-mile opening trip to Margate::
@1438937415
url: https://www.bbc.com/sport/football/33816595
{team:Truro City} manager {manager:Steve Tully} says his side could not have had a tougher opening game of the season as they make a 666-mile round-trip to Margate
{team:Truro} are back in {league:National League South} after winning promotion from the {league:Southern Premier League} in May.
"I don't think we could have asked for a harder first game back," said {manager:Tully}.
"It's the longest journey in the league but we'll deal with it, we'll get it over and done with," {manager:Tully} added to BBC Radio Cornwall.
But the journey is not the longest for a {team:Truro City} football match - {team:Whitley Bay} had a 921-mile round-trip when they beat {team:Truro} in the {league:FA Vase} in 2008.
"Everything's against us, but for me it's one where we can play our football and see what happens," said {manager:Tully}.
"We're really looking forward to it, we'll be really well prepared, we're travelling up on Friday to give our players the best chance possible, so we'll give it a go."