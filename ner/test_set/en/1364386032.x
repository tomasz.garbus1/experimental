::Dagenham & Redbridge sign Barnsley's Toni Silva on loan::
@1364386032
url: https://www.bbc.com/sport/football/21956048
League Two side Dagenham & Redbridge have signed Barnsley winger Toni Silva on loan until 23 April.
The 19-year-old joined the Tykes from Liverpool last summer.
Daggers interim manager Wayne Burnett told the club website: "I would like to thank Barnsley manager David Flitcroft for making this deal possible.
"The player was signed from Liverpool with a lot of promise. Unfortunately he has slightly lost his way but hopefully we can get him back to his best."
Silva, who has made one appearance for Barnsley since his move to Oakwell, had a loan spell at Northampton Town last season where he scored once in 15 appearances.