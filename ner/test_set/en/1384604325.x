::Roy Hodgson says England will learn from defeat by Chile::
@1384604325
url: https://www.bbc.com/sport/football/24967181

                            
                        
England manager Roy Hodgson admitted his experimental line-up learned lessons in their World Cup build-up as they were well beaten by Chile.
Barcelona's Alexis Sanchez scored twice as Chile repeated the 2-0 friendly win they secured at Wembley in 1998.
Hodgson gave debuts to Southampton pair Adam Lallana and Jay Rodriguez, as well as Celtic goalkeeper Fraser Forster.
"There were good lessons for us to learn. We gave players a chance to play for England," he said.
Check out what rating BBC Sport's chief football writer Phil McNulty awarded to the Southampton star and the rest of Roy Hodgson's England team 
"I can't talk away or explain away a defeat which I thought came to a Chile side who played very well. It's not all doom and gloom. It's how you deal with defeats and we'll retain a sense of perspective."
Chile's victory ended a 10-game unbeaten run for Hodgson and some boos greeted the final whistle from England's frustrated fans.
Hodgson said: "I think that's part of the game. Fans don't accept defeats. They don't want defeats. Who does? We certainly don't.
"There are not many games you see or watch on television when there isn't some booing when the home team has lost but I'm not prepared to criticise our fans in any way because they've been fantastic."
Hodgson also gave a sympathetic verdict on his three England new boys, saying: "Forster didn't have to make many saves but made quite a good one in the first half. He was chanceless for both of the goals.
"I thought Adam Lallana did well in a debut match against such good opposition and I thought it was a very big ask for Rodriguez to play in that position, but I have so many wide players injured that his debut maybe came a bit earlier than he would have liked against opposition that was maybe a bit stronger than he would have liked. I am certainly not disappointed with any of them."
Lallana, 25, revealed a determination to build on his England debut after his first taste of international football left him wanting more.
"It is every child's dream to represent their country and I am happy I fulfilled that," said the Southampton midfielder.
"I hope it is not the last time. I am just hungry for more now. I was proud to get my first cap, but obviously disappointed with the result."
The last team to beat England by a margin of two or more goals at Wembley were France, who came to London as world champions in February 1999 and won 2-0 thanks to two Nicolas Anelka goals
Tottenham winger Andros Townsend, 22, who came on as a substitute in the second half, admitted the opportunity to face South American opposition was valuable experience ahead of next summer.
"If we are going to go to the World Cup and do well," he said, "we are going to have to learn to play against teams like Chile.
"We have played a lot of European sides and to test ourselves against a South American side who do play a different style.
"The manager came in after the game and said to us: 'we did not get too carried away when we qualified for Brazil, so don't beat yourselves up because there is another game again when we can put all the wrongs right against Germany'."
Hodgson confirmed Joe Hart will replace Forster in goal against Germany on Tuesday, when Steven Gerrard, Ashley Cole, Phil Jagielka and Daniel Sturridge will also be back in contention.
Manchester United defender Phil Jones suffered a groin injury and will undergo a scan to discover the extent of the problem but he is unlikely to be fit to face Germany.