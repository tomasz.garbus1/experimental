::{team:Rangers}: {manager:Ally McCoist} vows to come back stronger::
@1342194894
url: https://www.bbc.com/sport/football/18835932
Manager {manager:Ally McCoist} has vowed that {team:Rangers} "will come back stronger" as he prepares for life in {league:Division Three}.
{league:Scottish Football League} clubs voted the new company into the bottom tier following the {league:Scottish Premier League}'s rejection of their application.
"Clearly, starting again from the bottom league is not ideal and makes the task of rebuilding Rangers a longer one," said {manager:McCoist}.
"I fully accept the decision and thank them for allowing us into the SFL."
At Friday's {league:SFL} meeting, 29 of the 30 clubs voted to accept {team:Rangers}, but 25 were opposed to the resolution that proposed the team start in {league:Division One}.
"The {league:SFL} was placed in an impossible situation and I respect its decision," said McCoist.
"I fully supported the fans' views that starting again in {league:Division Three} maintains the sporting integrity that the {league:SPL} clubs were so keen on. 
"The {league:SPL} clubs and the {league:SFA} have made their positions clear over the last few weeks and it remains to be seen what the long-term effects of their decisions will be.
"{team:Rangers} has been severely punished for the actions of some individuals who previously ran the club and it will take time for us to recover.
"But we will come back stronger thanks to the loyalty of the fans and the commitment of everyone at {stadium:Ibrox} who are working tirelessly to bring stability and success back."
{team:Rangers} chief executive {manager:Charles Green}, who along with {manager:McCoist} and {league:Scottish FA} chief executive Stewart Regan met the {league:SFL} chairmen ahead of the {stadium:Hampden} vote, has no plans to challenge the decision.
"It is a matter of regret for all of us involved with {team:Rangers} that the issues surrounding the club resulted in the {league:SFL} and its members being placed in a very difficult position not of their own making," {manager:Green} explained.
"We are a football club and we just want to get back to playing football. Now is the time to move on and start afresh.
"Our task to rebuild the club will take longer now, but we are committed to the job and fully believe we will bring success back to Rangers." 