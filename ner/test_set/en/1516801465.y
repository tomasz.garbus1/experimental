::{country:Scotland} post attracts new candidates, says SFA's Stewart Regan::
@1516801465
url: https://www.bbc.com/sport/football/42801769
Scottish FA chief executive Stewart Regan says "a number of new candidates have come forward in the last few days" as the hunt continues for {country:Scotland}'s next manager.
{country:Northern Ireland}'s {manager:Michael O'Neill} turned down the job this week after talks with the SFA.
"We are casting the net far and wide," said Regan.
"We meet as a subcommittee in the next few days and we'll review our short-list."
{country:Scotland} have been without a head coach since {manager:Gordon Strachan}'s departure in mid-October.
Failure to persuade {manager:O'Neill}, who took {country:Northern Ireland} to {league:Euro 2016} and to the {league:World Cup} play-offs, to sign has sparked criticism of the recruitment process.
But Regan, in Lausanne for the {league:Nations League} draw, said he was not considering his position as that would be a matter for the SFA board.
"We've had our own short-list prior to approaching {manager:Michael O'Neill} and we'll be reviewing all the names and deciding on who we are going to see and what the next steps will be," he said.
"We'll be looking to move to the next phase and find a replacement for {manager:Gordon Strachan}.

                            
                        
"We're keen to try to have someone in place as quickly as possible but, with no competitive matches until September, we have a little bit of time.
"It's important that we focus on finding the right person."
Asked if he was disappointed to miss out on {manager:O'Neill}, Regan said: "We saw him as a stand-out candidate.
"He's been very impressive with {country:Northern Ireland}, working with limited resources. 
"We were very keen to bring him forward and we made a best offer to him.
"He's chosen to remain loyal to {country:Northern Ireland} and we respect that. I saw him last night and wished him all the best for the tournament."
{country:Scotland} have been drawn with {country:Israel} and {country:Albania} in the inaugural {league:Nations League} and Regan is looking forward to the campaign.
"They are certainly winnable matches from our perspective," he said. "We will go into them believing we can be promoted.
"There are financial incentives for promotion and then there is a second bite at the cherry for {league:the Euros}.
"It's a great opportunity for smaller nations to progress."