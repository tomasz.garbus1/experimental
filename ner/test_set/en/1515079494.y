::{team:Chesterfield} sign {player:Zavon Hines}, {team:Millwall}'s {player:Sid Nelson} and {team:Barnsley}'s {player:Josh Kay}::
@1515079494
url: https://www.bbc.com/sport/football/42566193
{team:Chesterfield} have signed forward {player:Zavon Hines}, while {team:Millwall} defender {player:Sid Nelson} and {team:Barnsley} midfielder {player:Josh Kay} have joined on loan deals.
{player:Hines}, 29, has agreed an 18-month contract after having his deal at {team:Maidstone} cancelled by mutual consent.
He scored 10 goals in 25 appearances for the {league:National League} club after joining {team:the Stones} last August.
Centre-back {player:Nelson}, 22, and {player:Kay}, 21, have joined {team:the Spireites} until the end of the season.
{player:Nelson} featured in 12 {league:League Two} games on loan at {team:Yeovil} this term, while {player:Kay} made two appearances during a month-long spell with {league:National League} side {team:Tranmere}.
Find all the latest football transfers on our dedicated page.