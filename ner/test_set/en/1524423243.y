::{team:Manchester City} 5-0 {team:Swansea City}::
@1524423243
url: https://www.bbc.com/sport/football/43772471
Manager {manager:Pep Guardiola} says {team:Manchester City}'s focus is on chasing records after they celebrated being crowned {league:Premier League} champions by thrashing {team:Swansea}.
{team:Manchester City} were given a guard of honour and there were celebrations at full-time in their first match since their third league title in seven seasons was confirmed.
Thousands of fans invaded the pitch as {manager:Guardiola}'s side emphatically ended a run of three successive home defeats.
And {manager:Guardiola} said "looking for records helps to keep the players focused" as his side prepare for their last four games of the season.
This victory took them to 90 points, five shy of the {league:Premier League} record set by {team:Chelsea} in 2004-05, and they are also chasing the most number of wins and goals scored in a season.
"The league is won, now it's the records," said {manager:Guardiola}. "It's never unfinished business in football, you have to keep improving.
"If you think 'OK, we are already champions', we are done and you cannot play like the way we played. If we are improving it will be good for the new season, finishing in a good way."
{player:Raheem Sterling}'s cut-back set up {player:David Silva}'s early opener before the winger poked in from six yards to make it two within 16 minutes.
{player:Kevin de Bruyne} scored with a sensational 25-yard drive before {player:Bernardo Silva} pounced on the rebound of {player:Gabriel Jesus}' saved penalty.
{player:The Brazilian} eventually found the back of the net when he headed in {player:Yaya Toure}'s chipped pass.
{team:Swansea} are without a win in five league matches and remain four points above the relegation zone with four games to play.
And {team:the Swans} must take on fifth-placed {team:Chelsea} at the {stadium:Liberty Stadium} next week before hosting relegation-threatened {team:Stoke} and {team:Southampton} in a tough run-in.
{team:Manchester City} can look forward to their penultimate home game of the season against Huddersfield on 6 May, when they will be presented with the {league:Premier League} trophy.
It was a day of celebrations for {team:Manchester City}, who were crowned {league:Premier League} champions last week following {team:Manchester United}'s defeat by {team:West Brom}. 
Supporters gathered at {stadium:Etihad Stadium} before kick-off to parade replica trophies, fly flags and welcome the champions on arrival. 
They also stormed the pitch in numbers to celebrate at full-time - singing, taking pictures and setting off flares to revel in {team:Manchester City}'s third {league:Premier League} title.
And {manager:Guardiola}'s side were applauded by the {team:Swansea} players and staff in a guard of honour before kick-off.
The hosts' performance was worthy of a title-winning side, too. 
They cut {team:Swansea}'s defence apart in the opening stages and passed the ball around with flair and creativity.
It was a display of intent from the champions, and while they can still break {league:Premier League} records before the season ends in May, they are also chasing individual and club accolades.
{team:Manchester City} have now also broken their own record tally of 89, set in 2011-12, and {manager:Guardiola} could beat his previous best points tally in a single league season as a manager - 99, with {team:Barcelona} in 2009-10.
No team has accrued 100 points or more in a single top-flight campaign, but {team:City} would become the first that if they pick up 10 from their remaining four games. 
Records {team:Manchester City} can still break:
{manager:Carlos Carvalhal}'s {team:Swansea} have improved dramatically since December, but they still remain in a relegation battle.
They currently sit four points above the drop zone - though they would not have expected to pick up points at the {stadium:Etihad}.
But with four games to go, including a tough match against {league:FA Cup} finalists {team:Chelsea} to come next week, {team:the Swans} cannot yet feel that their survival is guaranteed.
They have lost only four times in their last 14 {league:Premier League} matches, though, a record that will give them confidence heading into those crucial games against {team:Stoke} and {team:Southampton}. 
And though there were not too many positives to take from their performance in {team:Manchester}, they were simply outclassed by the best side in the country this season.
{player:Alfie Mawson} came close with a headed effort in the second half, but there was not too much to get excited about for {team:the Swans} and their attentions will have already turned to their remaining four games.

                            
                        
{team:Manchester City} manager {manager:Pep Guardiola} told BBC Sport: "It was a happy atmosphere around the stadium, getting off the bus and of course the supporters. The most important thing was a good performance.
"You have to keep improving. We spoke to the players in the week about that. If we are improving it will be good for the new season, finishing in a good way.
"When {team:Swansea} had the ball we pressed so good and so intense and then when we had the ball we had to keep it moving, moving, moving, playing simple, until we had the chance to play a final pass.
"Swansea have had good results under {manager:Carlos Carvalhal} against {team:Arsenal} and {team:Liverpool} and that's why it was so important to focus, play well and score."

                            
                        
{team:Swansea} boss {manager:Carlos Carvalhal} told BBC Sport: "It was a difficult game for us. They had just lost one home game against {team:Manchester United}. They are champions; they are a team from another planet.
"We are playing to survive. We lose one game and at this moment we are still four points from the teams behind us. 
"This game, we forget it and we move to the next game to try and achieve the points.
"We have recovered nine points in three months, we have four games left. It is in our hands. I hope we can solve the things before the last two games but if we don't, we have the two games at the end I think with the determination and commitment we have we will achieve it."
{team:Swansea} host {team:Chelsea} in their next {league:Premier League} match on 28 April (17:30 BST), while {team:Manchester City} travel to London to take on {team:West Ham} on 29 April (14:15 BST).
Match ends, {team:Manchester City} 5, {team:Swansea City} 0.
Second Half ends, {team:Manchester City} 5, {team:Swansea City} 0.
Corner,  {team:Manchester City}. Conceded by {player:Andy King}.
Attempt missed. {player:Ilkay GÃ¼ndogan} ({team:Manchester City}) right footed shot from the centre of the box is close, but misses the top left corner. Assisted by {player:Bernardo Silva}.
Goal!  {team:Manchester City} 5, {team:Swansea City} 0. {player:Gabriel Jesus} ({team:Manchester City}) header from the centre of the box to the bottom left corner. Assisted by {player:Yaya TourÃ©} with a through ball.
Attempt missed. {player:Vincent Kompany} ({team:Manchester City}) header from the right side of the six yard box is close, but misses to the right. Assisted by {player:Yaya TourÃ©} with a through ball.
Corner,  {team:Manchester City}. Conceded by {player:Alfie Mawson}.
{player:Phil Foden} ({team:Manchester City}) wins a free kick in the attacking half.
Foul by {player:AndrÃ© Ayew} ({team:Swansea City}).
Attempt blocked. {player:Yaya TourÃ©} ({team:Manchester City}) right footed shot from outside the box is blocked. Assisted by {player:Bernardo Silva}.
{player:Benjamin Mendy} ({team:Manchester City}) wins a free kick on the left wing.
Foul by {player:Jordan Ayew} ({team:Swansea City}).
Attempt saved. {player:Gabriel Jesus} ({team:Manchester City}) header from the centre of the box is saved in the centre of the goal. Assisted by {player:Phil Foden} with a cross.
Attempt saved. {player:Benjamin Mendy} ({team:Manchester City}) right footed shot from the left side of the box is saved in the centre of the goal.
Attempt missed. {player:Danilo} ({team:Manchester City}) left footed shot from outside the box is high and wide to the left. Assisted by {player:Bernardo Silva}.
Attempt missed. {player:Sam Clucas} ({team:Swansea City}) left footed shot from the centre of the box misses to the left. Assisted by {player:Tammy Abraham} following a fast break.
Attempt blocked. {player:Yaya TourÃ©} ({team:Manchester City}) right footed shot from outside the box is blocked.
{player:Phil Foden} ({team:Manchester City}) wins a free kick in the attacking half.
Foul by {player:Mike van der Hoorn} ({team:Swansea City}).
Foul by {player:Ilkay GÃ¼ndogan} ({team:Manchester City}).
{player:Jordan Ayew} ({team:Swansea City}) wins a free kick in the defensive half.
Substitution, {team:Swansea City}. {player:Tammy Abraham} replaces {player:Martin Olsson}.
Substitution, {team:Manchester City}. {player:Benjamin Mendy} replaces {player:Fabian Delph}.
Offside, {team:Swansea City}. {player:Kyle Naughton} tries a through ball, but {player:AndrÃ© Ayew} is caught offside.
Substitution, {team:Manchester City}. {player:Phil Foden} replaces {player:Raheem Sterling}.
Corner,  {team:Manchester City}. Conceded by {player:Kyle Naughton}.
Substitution, {team:Swansea City}. {player:Kyle Bartley} replaces {player:Federico FernÃ¡ndez} because of an injury.
Delay over. They are ready to continue.
Delay in match {player:Federico FernÃ¡ndez} ({team:Swansea City}) because of an injury.
Substitution, {team:Manchester City}. {player:Yaya TourÃ©} replaces {player:Kevin De Bruyne}.
Substitution, {team:Swansea City}. {player:Sam Clucas} replaces {player:Ki Sung-yueng}.
Goal!  {team:Manchester City} 4, {team:Swansea City} 0. {player:Bernardo Silva} ({team:Manchester City}) right footed shot from the right side of the six yard box to the centre of the goal following a set piece situation.
Penalty saved! {player:Gabriel Jesus} ({team:Manchester City}) fails to capitalise on this great opportunity,  right footed shot saved  in the bottom left corner.
Penalty {team:Manchester City}. {player:Raheem Sterling} draws a foul in the penalty area.
Penalty conceded by {player:Federico FernÃ¡ndez} ({team:Swansea City}) after a foul in the penalty area.
{player:Raheem Sterling} ({team:Manchester City}) wins a free kick in the attacking half.
Foul by {player:Jordan Ayew} ({team:Swansea City}).
Corner,  {team:Manchester City}. Conceded by {player:Mike van der Hoorn}.
Attempt missed. {player:Alfie Mawson} ({team:Swansea City}) header from the right side of the six yard box is just a bit too high. Assisted by {player:Ki Sung-yueng} with a cross following a corner.
Corner,  {team:Swansea City}. Conceded by {player:Fabian Delph}.