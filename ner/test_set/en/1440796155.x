::Irish Premiership: Glenavon 0-0 Glentoran::
@1440796155
url: https://www.bbc.com/sport/football/34089947

                            
                        
Assistant manager Paul Millar said Glenavon's players were gutted not to have beaten Glentoran at Mourneview Park on Friday night.
Glenavon had their chances, with young Joel Cooper blasting the best of them over from just a few yards out.
"It shows how far our team has come when Glentoran are coming here and playing for a point," claimed Millar.
"They were time-wasting and putting players behind the ball and we are disappointed we did not find the net."
The draw leaves Glentoran with just one win from their opening five matches in the league while Glenavon are one point better off on seven.
Glenavon opened brightly and Rhys Marshall saw his header from a corner come off the crossbar.
Home forward Eion Bradley then saw a looping header strike the far post with Hogg stranded.
Glentroan trio Barry Holland, Calum Birney and Johnny Addis were booked for fouls in the first half.
In the second half Conor McMenamin and Curtis Allen had efforts go just off target.
But the miss of the night belonged to Glenavon's Cooper who hoisted over from in front of goal after Andy Hall's ball in from the right had evaded the visitors' defence.
Glentoran could have snatched a late winner. Steven Gordon found himself with the ball at his feet in the area, but he smashed his effort straight at keeper Jonathan Tuffey.
"A draw was a fair result," reflected Glentoran manager Eddie Patterson.
"Glenavon were the hungrier team in the first half and we found it difficult to get into the game.
"I felt we had the better attacks in the second half but our problem at the moment is being consistent over the 90 minutes."