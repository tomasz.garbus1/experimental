::{team:Swansea City}'s {player:Wayne Routledge} revels under {manager:Michael Laudrup}::
@1362746983
url: https://www.bbc.com/sport/football/21708488
{player:Wayne Routledge} says {team:Swansea City} manager {manager:Michael Laudrup} has brought out the best in him this season.
"He gives everybody here the freedom to play football," said the midfielder, who has established himself as a regular during {manager:Laudrup}'s first season in charge at the {stadium:Liberty Stadium}.
"Like you see in our performances, we know our principles and what we're doing but we play with a freedom.
"That's when you see the best football come out."
{player:Routledge} has scored five goals this season, including two against Saturday's opponent {team:West Bromwich Albion} in a 3-1 home win in November.
The 28-year-old managed to find the net just once last season under {manager:Brendan Rodgers} - a goal that broke his unenviable record of playing for six clubs without scoring in the {league:Premier League}.
"It's been a nice change this year to be in the goals," said the winger, whose last goal came against former club {team:Aston Villa} in January. 
"It feels I haven't scored for a while now but obviously as long as the team are doing well as a whole, generally it's good."
Before joining {team:Swansea} in 2011, {player:Routledge}'s nomadic career had taken him to eight clubs in 10 years.
He feels settled in south {country:Wales}, experiencing what he describes as the most enjoyable season of his career, and intends to stay for many years to come.
"I'm enjoying football and that's when you're always going to see the best out of players," said the former {country:England} Under-21 international, who recently signed a contract extension until 2016. 
"There's been an opportunity from the start of the season for me to play. Thankfully I've been able to keep my place and keep playing reasonably good football
"So it's been going well for me, but not only for me, for the team as a whole as well."