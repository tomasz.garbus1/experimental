::{team:Al Ahly} and {team:Sunshine Stars} finally leave {country:Mali}::
@1336122453
url: https://www.bbc.com/sport/football/17938118
Players and officials from {country:Egypt}'s {team:Al Ahly} and {country:Nigeria}n side {team:Sunshine Stars} have left Bamako after getting trapped in {country:Mali} after renewed violence on Monday.
An {country:Egypt}ian military plane airlifted {team:Ahly} out of Bamako on Thursday afternoon headed for Cairo.
The six players and two officials from {team:Stars} left on Friday to return home via Benin.
The two teams were stranded after Monday's attempted counter-coup caused the airport to be closed.
{team:Ahly}'s flight after their 1-0 loss in the {league:African Champions League} to {team:Stade Malien} was initially delayed by bad weather. 
{team:Stars} had battled to a 1-1 draw with {team:Djoliba} in the {league:Champions League} on Saturday.
Some members of the {country:Nigeria}n party left after the game but a small group remain trapped.
On Thursday the Nigerian Football Federation said it was "deeply concerned" for those trapped.
The NFF were reacting to reports that their hotel had been "stormed" by armed men wearing military uniforms as the ruling junta in {country:Mali} cracks down on an attempted coup.
The federation added a player's hotel room had been ransacked. There were no reports of injuries.
{country:Mali} has been in turmoil since March when a group of soldiers toppled the country's democratically elected president. 
The junta leaders then handed power over to an interim government in April, but they still wield power.