::{team:Brighton & Hove Albion} 1-0 {team:Doncaster Rovers}::
@1391883034
url: https://www.bbc.com/sport/football/25994170
{player:Leonardo Ulloa} scored the winner as {team:Brighton} kept up their play-off push with victory over struggling {team:Doncaster}.
{player:Billy Sharp} missed {team:Rovers}' best chance when he headed wide from six yards 10 minutes after the break.
And {player:Ulloa} punished that miss on 75 minutes as he met {player:Stephen Ward}'s cross from the left with a firm header to break the visitors' resistance.

                            
                        
{player:Striker Sharp} compounded his side's misery when he was sent off late on after tangling with {player:Gordon Greer}.
The result left {team:the Seagulls} four points from the play-off positions as they extended their unbeaten run at home to six matches.
{player:Doncaster}, who are five points above the relegation zone, proved a stubborn obstacle but remain without an away win in the league since September.

                            
                        
Argentine {player:Ulloa} gave warning of his aerial threat when he headed a {player:Ward} cross wide from 10 yards.
{player:Sharp} then went close to breaking the deadlock for the away side, only to nod wide from {player:James Coppinger}'s cross.
{player:Ulloa} had a 20-yard curling strike kept out by keeper {player:Sam Johnstone} and {player:Solly March}'s close-range effort was blocked by {player:James Husband} as {team:Brighton} pressed.
And they eventually broke through when {player:Ulloa} converted from {player:Ward}'s cross, with Sharp sent off four minutes from time after clashing with {player:Greer}.
{team:Brighton} manager {manager:Oscar Garcia}:
"We started really well but when you (miss) a lot of chances you can lose your confidence. We kept playing well and creating chances and finally {player:Leonardo Ulloa} took his chance well.
"[Speculation] doesn't affect the players because they know I am with {team:Brighton}. They are not new to football. They know how it works, that managers can be sacked at any time.
"So I don't think they will spend any second of their lives thinking about it at all. The players are happy and they know we have a massive game on Tuesday."
{team:Doncaster} manager {manager:Paul Dickov}:
"We feel a bit unlucky. We were outstanding today. We knew they are a top team and we almost weathered the storm and got something from the game.
"In the second half we caused them some problems. We created more clear-cut chances throughout the game, so we feel that we've been done hard by.
"We've been on a good run lately and I'd like to think anyone that saw us would think that we can compete at this level. 
"We're not stupid enough to dismiss the threat of trouble but we have a good chance of getting out of it."
Match ends, {team:Brighton and Hove Albion} 1, {team:Doncaster Rovers} 0.
Second Half ends, {team:Brighton and Hove Albion} 1, {team:Doncaster Rovers} 0.
Corner,  {team:Brighton and Hove Albion}. Conceded by {player:James Coppinger}.
Hand ball by {player:Jonathan Obika} ({team:Brighton and Hove Albion}).
{player:Bongani Khumalo} ({team:Doncaster Rovers}) is shown the yellow card for a bad foul.
{player:Leonardo Ulloa} ({team:Brighton and Hove Albion}) wins a free kick in the defensive half.
Foul by {player:Bongani Khumalo} ({team:Doncaster Rovers}).
Attempt missed. {player:David LÃ³pez} ({team:Brighton and Hove Albion}) right footed shot from outside the box is close, but misses to the right. Assisted by {player:Kazenga Lua Lua}.
{player:Kazenga Lua Lua} ({team:Brighton and Hove Albion}) wins a free kick in the attacking half.
Foul by {player:Paul Quinn} ({team:Doncaster Rovers}).
Substitution, {team:Brighton and Hove Albion}. {player:Jonathan Obika} replaces {player:David RodrÃ­guez}.
Corner,  {team:Doncaster Rovers}. Conceded by {player:Tomasz Kuszczak}.
Attempt saved. {player:David Cotterill} ({team:Doncaster Rovers}) right footed shot from outside the box is saved in the centre of the goal. Assisted by {player:Paul Keegan}.
{player:Billy Sharp} ({team:Doncaster Rovers}) is shown the red card for fighting.
{player:Gordon Greer} ({team:Brighton and Hove Albion}) wins a free kick in the defensive half.
Foul by {player:Billy Sharp} ({team:Doncaster Rovers}).
{player:David RodrÃ­guez} ({team:Brighton and Hove Albion}) wins a free kick in the attacking half.
Foul by {player:Paul Keegan} ({team:Doncaster Rovers}).
Foul by {player:David RodrÃ­guez} ({team:Brighton and Hove Albion}).
{player:James Husband} ({team:Doncaster Rovers}) wins a free kick on the left wing.
{player:Leonardo Ulloa} ({team:Brighton and Hove Albion}) is shown the yellow card for a bad foul.
Substitution, {team:Brighton and Hove Albion}. {player:David LÃ³pez} replaces {player:Solly March}.
Foul by {player:Leonardo Ulloa} ({team:Brighton and Hove Albion}).
{player:Dean Furman} ({team:Doncaster Rovers}) wins a free kick in the attacking half.
Corner,  {team:Brighton and Hove Albion}. Conceded by {player:James Husband}.
Attempt blocked. {player:David RodrÃ­guez} ({team:Brighton and Hove Albion}) right footed shot from the right side of the box is blocked. Assisted by {player:Leonardo Ulloa}.
Substitution, {team:Doncaster Rovers}. {player:David Cotterill} replaces {player:Mark Duffy}.
Goal!  {team:Brighton and Hove Albion} 1, {team:Doncaster Rovers} 0. {player:Leonardo Ulloa} ({team:Brighton and Hove Albion}) header from the centre of the box to the bottom right corner. Assisted by {player:Stephen Ward} with a cross.
Offside, {team:Brighton and Hove Albion}. {player:Kazenga Lua Lua} tries a through ball, but {player:Leonardo Ulloa} is caught offside.
{player:Rohan Ince} ({team:Brighton and Hove Albion}) wins a free kick on the right wing.
Foul by {player:Billy Sharp} ({team:Doncaster Rovers}).
Attempt saved. {player:David RodrÃ­guez} ({team:Brighton and Hove Albion}) right footed shot from a difficult angle on the right is saved in the centre of the goal. Assisted by {player:Solly March}.
Attempt missed. {player:Mark Duffy} ({team:Doncaster Rovers}) right footed shot from the left side of the box is high and wide to the right. Assisted by {player:Chris Brown}.
Attempt missed. {player:Leonardo Ulloa} ({team:Brighton and Hove Albion}) header from the centre of the box is close, but misses to the left. Assisted by {player:David RodrÃ­guez} with a cross.
{player:Bruno} ({team:Brighton and Hove Albion}) wins a free kick on the right wing.
Foul by {player:Billy Sharp} ({team:Doncaster Rovers}).
Attempt saved. {player:Leonardo Ulloa} ({team:Brighton and Hove Albion}) right footed shot from the centre of the box is saved in the centre of the goal. Assisted by {player:Matthew Upson}.
Corner,  {team:Brighton and Hove Albion}. Conceded by {player:James Husband}.
Attempt missed. {player:Kazenga Lua Lua} ({team:Brighton and Hove Albion}) right footed shot from outside the box is too high.
{player:James Husband} ({team:Doncaster Rovers}) is shown the yellow card.