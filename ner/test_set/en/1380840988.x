::Wigan manager Coyle backs Scott Carson for England return::
@1380840988
url: https://www.bbc.com/sport/football/24392918
Manager Owen Coyle believes that Scott Carson can get back into the England squad after the goalkeeper helped Wigan to their first victory in Europe.
Carson, who last played for England in November 2011, made an outstanding save to deny Maribor a late equaliser before the Latics grabbed their third goal in a 3-1 win in Europa League Group D.
"Scott Carson was an England keeper not by accident," said the Wigan manager of the 28-year-old.
"He can get back into the squad."
Scott Carson returned from a two-year stint with Turkish side Bursaspor to sign with Wigan this summer. He has made four England appearances. His most recent came in the 1-0 friendly win over Sweden in November 2011 and his most famous was in a 3-2 defeat to Croatia at Wembley in November 2007 which ended England's hopes of qualifying for Euro 2008 and Steve McClaren's reign as national team manager.
Debate has surrounded who should hold the England number one jersey following indifferent performances by Manchester City's Joe Hart this season.
Coyle added: "Performances like this will certainly help. That's the key ingredient of top goalkeepers. He was probably idle for 95% of that game, but when he's called upon he makes a wonderful save and there's no doubt it was a huge moment in the game."
Carson has won four caps in total and was involved with several squads between 2005 and 2007. But misfortune struck in a crucial Euro 2008 qualifier against Croatia when, with scores 0-0, he made an awful error and failed to keep out Niko Kranjcar's 35-yard speculative shot. England went on to lose the match 3-2 and miss out on qualification.
Carson fell out of favour with McClaren's successor Fabio Capello, but did come on as a substitute against Germany in a 2008 friendly and then against Sweden in 2011.
Wigan's on-loan Manchester United midfielder Nick Powell, who scored the first and third goals, also praised the former West Brom number one.
"If Scott Carson was in the Premier League he'd be in the England squad," said the 19-year-old. 
Goalscorer Ben Watson added: "Carson made a great save, he was brilliant."
Wigan's first-ever victory in Europe sees them second on four points in Europa League Group D, two points behind leaders Rubin Kazan, who they play next.
Coyle said he was pleased with his side's overall display against the Slovenian champions on Thursday.
"Maribor are a very good side but from start to finish I thought we were outstanding," said the Scot.
"The football we played, we passed and moved the ball at a real intensity culminating in some terrific goalscoring chances. Not that there is any negative, but there's no doubt we could have scored six or seven goals tonight."