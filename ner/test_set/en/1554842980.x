::Newport County 0-0 Swindon Town::
@1554842980
url: https://www.bbc.com/sport/football/47181045
Newport County held Swindon Town to a goalless draw as both teams failed to create much in the way of scoring opportunities.
County lost both Jamille Matt and Joss Labadie in an injury-hit first half as both teams struggled at Rodney Parade.
Joe Day was the Newport hero, not for the first time this season, with 20 minutes left, when his double save from Dion Conroy kept the scores level.
Newport remain 13th in League Two, two points behind Swindon, who stay 10th.
Newport County manager Mike Flynn: "On the second half performance I'd say it is a point game especially with Exeter losing.
"Six games to go, five points behind with a game in hand, why wouldn't I be hopeful? I'm buzzing that we have conceded only one goal in our last nine home games."
Match ends, Newport County 0, Swindon Town 0.
Second Half ends, Newport County 0, Swindon Town 0.
Regan Poole (Newport County) wins a free kick in the defensive half.
Foul by Kyle Bennett (Swindon Town).
Attempt missed. Marc Richards (Swindon Town) header from the centre of the box misses to the right.
Attempt missed. Dan Butler (Newport County) left footed shot from outside the box misses to the right.
Substitution, Swindon Town. Jak McCourt replaces Canice Carroll because of an injury.
Canice Carroll (Swindon Town) is shown the yellow card.
Attempt blocked. Josh Sheehan (Newport County) right footed shot from outside the box is blocked.
Delay over. They are ready to continue.
Delay in match  (Newport County).
Delay over. They are ready to continue.
Foul by Regan Poole (Newport County).
Keshi Anderson (Swindon Town) wins a free kick in the defensive half.
Foul by Dan Butler (Newport County).
Michael Doughty (Swindon Town) wins a free kick on the left wing.
Attempt blocked. Keanu Marsh-Brown (Newport County) left footed shot from outside the box is blocked.
Substitution, Swindon Town. Marc Richards replaces Danny Rose.
Corner,  Swindon Town. Conceded by Mark O'Brien.
Attempt blocked. Kyle Bennett (Swindon Town) right footed shot from the centre of the box is blocked.
Substitution, Newport County. Keanu Marsh-Brown replaces Ben Kennedy.
Attempt missed. Kyle Bennett (Swindon Town) right footed shot from outside the box is close, but misses to the left.
Corner,  Swindon Town. Conceded by Mickey Demetriou.
Attempt blocked. Dion Conroy (Swindon Town) header from the centre of the box is blocked.
Mickey Demetriou (Newport County) is shown the yellow card for a bad foul.
Foul by Mickey Demetriou (Newport County).
Keshi Anderson (Swindon Town) wins a free kick in the attacking half.
Foul by Josh Sheehan (Newport County).
Michael Doughty (Swindon Town) wins a free kick in the attacking half.
Substitution, Swindon Town. Michael Doughty replaces Kaiyne Woolery.
Adebayo Azeez (Newport County) wins a free kick in the attacking half.
Foul by Canice Carroll (Swindon Town).
Robbie Willmott (Newport County) wins a free kick in the defensive half.
Foul by Ali Koiki (Swindon Town).
Andrew Crofts (Newport County) is shown the yellow card for a bad foul.
Delay over. They are ready to continue.
Delay in match Mickey Demetriou (Newport County) because of an injury.
Foul by Andrew Crofts (Newport County).
Keshi Anderson (Swindon Town) wins a free kick in the defensive half.
Corner,  Newport County. Conceded by Luke Woolfenden.