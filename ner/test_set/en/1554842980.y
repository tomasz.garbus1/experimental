::{team:Newport County} 0-0 {team:Swindon Town}::
@1554842980
url: https://www.bbc.com/sport/football/47181045
{team:Newport County} held {team:Swindon Town} to a goalless draw as both teams failed to create much in the way of scoring opportunities.
{team:County} lost both {player:Jamille Matt} and {player:Joss Labadie} in an injury-hit first half as both teams struggled at {player:Rodney Parade}.
{player:Joe Day} was the {team:Newport} hero, not for the first time this season, with 20 minutes left, when his double save from {player:Dion Conroy} kept the scores level.
{team:Newport} remain 13th in {league:League Two}, two points behind {team:Swindon}, who stay 10th.
{team:Newport County} manager {manager:Mike Flynn}: "On the second half performance I'd say it is a point game especially with {team:Exeter} losing.
"Six games to go, five points behind with a game in hand, why wouldn't I be hopeful? I'm buzzing that we have conceded only one goal in our last nine home games."
Match ends, {team:Newport County} 0, {team:Swindon Town} 0.
Second Half ends, {team:Newport County} 0, {team:Swindon Town} 0.
{player:Regan Poole} ({team:Newport County}) wins a free kick in the defensive half.
Foul by {player:Kyle Bennett} ({team:Swindon Town}).
Attempt missed. {player:Marc Richards} ({team:Swindon Town}) header from the centre of the box misses to the right.
Attempt missed. {player:Dan Butler} ({team:Newport County}) left footed shot from outside the box misses to the right.
Substitution, {team:Swindon Town}. {player:Jak McCourt} replaces {player:Canice Carroll} because of an injury.
{player:Canice Carroll} ({team:Swindon Town}) is shown the yellow card.
Attempt blocked. {player:Josh Sheehan} ({team:Newport County}) right footed shot from outside the box is blocked.
Delay over. They are ready to continue.
Delay in match  ({team:Newport County}).
Delay over. They are ready to continue.
Foul by {player:Regan Poole} ({team:Newport County}).
{player:Keshi Anderson} ({team:Swindon Town}) wins a free kick in the defensive half.
Foul by {player:Dan Butler} ({team:Newport County}).
{player:Michael Doughty} ({team:Swindon Town}) wins a free kick on the left wing.
Attempt blocked. {player:Keanu Marsh-Brown} ({team:Newport County}) left footed shot from outside the box is blocked.
Substitution, {team:Swindon Town}. {player:Marc Richards} replaces {player:Danny Rose}.
Corner,  {team:Swindon Town}. Conceded by {player:Mark O'Brien}.
Attempt blocked. {player:Kyle Bennett} ({team:Swindon Town}) right footed shot from the centre of the box is blocked.
Substitution, {team:Newport County}. {player:Keanu Marsh-Brown} replaces {player:Ben Kennedy}.
Attempt missed. {player:Kyle Bennett} ({team:Swindon Town}) right footed shot from outside the box is close, but misses to the left.
Corner,  {team:Swindon Town}. Conceded by {player:Mickey Demetriou}.
Attempt blocked. {player:Dion Conroy} ({team:Swindon Town}) header from the centre of the box is blocked.
{player:Mickey Demetriou} ({team:Newport County}) is shown the yellow card for a bad foul.
Foul by {player:Mickey Demetriou} ({team:Newport County}).
{player:Keshi Anderson} ({team:Swindon Town}) wins a free kick in the attacking half.
Foul by {player:Josh Sheehan} ({team:Newport County}).
{player:Michael Doughty} ({team:Swindon Town}) wins a free kick in the attacking half.
Substitution, {team:Swindon Town}. {player:Michael Doughty} replaces {player:Kaiyne Woolery}.
{player:Adebayo Azeez} ({team:Newport County}) wins a free kick in the attacking half.
Foul by {player:Canice Carroll} ({team:Swindon Town}).
{player:Robbie Willmott} ({team:Newport County}) wins a free kick in the defensive half.
Foul by {player:Ali Koiki} ({team:Swindon Town}).
{player:Andrew Crofts} ({team:Newport County}) is shown the yellow card for a bad foul.
Delay over. They are ready to continue.
Delay in match {player:Mickey Demetriou} ({team:Newport County}) because of an injury.
Foul by {player:Andrew Crofts} ({team:Newport County}).
{player:Keshi Anderson} ({team:Swindon Town}) wins a free kick in the defensive half.
Corner,  {team:Newport County}. Conceded by {player:Luke Woolfenden}.