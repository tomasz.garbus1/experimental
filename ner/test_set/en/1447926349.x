::Dundee United's John Rankin critical of 'macho' Scottish referees::
@1447926349
url: https://www.bbc.com/sport/football/34865802
Dundee United midfielder John Rankin says referees' "macho" attitudes sometimes get in the way of developing good relationships with players.
And he believes officials should be more willing to explain decisions.
"You see on the television all the time referees speaking to players, but that's not what they're saying," Rankin told BBC Scotland.
"More often than not, it's a threat. It's as if they are feeling macho - they are in charge of the game."
Uefa referee officer Hugh Dallas this month said Scottish referees are among the best in Europe.
However, while also criticising the behaviour of some of his fellow professionals, Rankin thinks players often receive the wrong type of response from officials.
"Some of the ways that referees are spoken to are not right either," he said. 
"We've got to respect them as well as they respect us, but sometimes it's actually a referee that instigates the problem in the first place.
"The players maybe ask a question and the way they respond to that question of 'why was that a foul?' or 'what did you see there?' is not a response that you're looking for. 
"It's 'get out my face, get away from me, don't talk to me, do you want to go in the book?'.
"If we are going to progress and let the referees run the game in the right way and get more decisions correct and be consistent then I feel they can get more out the players with regards to the relationship on the pitch."
Rankin highlighted penalty decisions as a particular problem area.
"Our jobs are hard enough at set-pieces because you've got players blocking you and running across you and different things," he said.
"But for a referee to make a decision sometimes so quickly rather than just assessing what actually went on, it frustrates us because we don't actually get the truth.
"Because it's as if he's trying to make it sound appropriate when he's maybe made the decision a wee bit too early."
Falkirk manager Peter Houston has asked referee supervisor John Fleming to change procedures so officials are publicly held to account for wrong decisions.
In October, he accused referee John McKendrick of making the poorest decision he had seen in years and claimed he had been influenced by half-time complaints from the Rangers backroom staff.  
Following the match, which Rangers won 3-1, Houston was given a one-match touchline ban by the Scottish Football Association.
While Houston admits that being a referee is a difficult job he would not wish to do, he said: "We get sacked if we make too many bad decisions.
"I think we should be told what the punishment is for those decisions.
"If it's a referee that's constantly making the wrong decisions, let's be told about it."