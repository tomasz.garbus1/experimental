::{team:Wigan} manager {manager:Coyle} backs {player:Scott Carson} for {country:England} return::
@1380840988
url: https://www.bbc.com/sport/football/24392918
Manager {manager:Owen Coyle} believes that {player:Scott Carson} can get back into the {country:England} squad after the goalkeeper helped {team:Wigan} to their first victory in Europe.
{player:Carson}, who last played for {country:England} in November 2011, made an outstanding save to deny Maribor a late equaliser before the Latics grabbed their third goal in a 3-1 win in {league:Europa League} Group D.
"{player:Scott Carson} was an {country:England} keeper not by accident," said the {team:Wigan} manager of the 28-year-old.
"He can get back into the squad."
{player:Scott Carson} returned from a two-year stint with Turkish side {team:Bursaspor} to sign with {team:Wigan} this summer. He has made four {country:England} appearances. His most recent came in the 1-0 friendly win over {country:Sweden} in November 2011 and his most famous was in a 3-2 defeat to {country:Croatia} at {stadium:Wembley} in November 2007 which ended {country:England}'s hopes of qualifying for {league:Euro 2008} and {manager:Steve McClaren}'s reign as national team manager.
Debate has surrounded who should hold the {country:England} number one jersey following indifferent performances by {team:Manchester City}'s {player:Joe Hart} this season.
{manager:Coyle} added: "Performances like this will certainly help. That's the key ingredient of top goalkeepers. He was probably idle for 95% of that game, but when he's called upon he makes a wonderful save and there's no doubt it was a huge moment in the game."
{player:Carson} has won four caps in total and was involved with several squads between 2005 and 2007. But misfortune struck in a crucial {league:Euro 2008} qualifier against {country:Croatia} when, with scores 0-0, he made an awful error and failed to keep out {player:Niko Kranjcar}'s 35-yard speculative shot. {country:England} went on to lose the match 3-2 and miss out on qualification.
{player:Carson} fell out of favour with {manager:McClaren}'s successor {manager:Fabio Capello}, but did come on as a substitute against {country:Germany} in a 2008 friendly and then against {country:Sweden} in 2011.
{team:Wigan}'s on-loan {team:Manchester United} midfielder {player:Nick Powell}, who scored the first and third goals, also praised the former {team:West Brom} number one.
"If {player:Scott Carson} was in the {league:Premier League} he'd be in the {country:England} squad," said the 19-year-old. 
Goalscorer {player:Ben Watson} added: "{player:Carson} made a great save, he was brilliant."
{team:Wigan}'s first-ever victory in Europe sees them second on four points in {league:Europa League} Group D, two points behind leaders {team:Rubin Kazan}, who they play next.
{manager:Coyle} said he was pleased with his side's overall display against the {country:Slovenia}n champions on Thursday.
"{team:Maribor} are a very good side but from start to finish I thought we were outstanding," said {manager:the Scot}.
"The football we played, we passed and moved the ball at a real intensity culminating in some terrific goalscoring chances. Not that there is any negative, but there's no doubt we could have scored six or seven goals tonight."