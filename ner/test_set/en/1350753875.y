::{team:West Ham} 4-1 {team:Southampton}::
@1350753875
url: https://www.bbc.com/sport/football/19939627
{team:West Ham} earned their fourth victory of the season after a {player:Mark Noble} brace and goals for {player:Kevin Nolan} and {player:Modibo Maiga} sealed all three points. 
{team:Southampton} pulled one back through {player:Adam Lallana} but {team:West Ham}'s powerful second-half display proved too much for {manager:Nigel Adkins}' side. 
{player:Noble} opened the scoring with a fortuitous free kick while {player:Nolan} later tapped home at the back post before {player:Lallana} made it 2-1 with a delightful turn and shot. 
But {player:Noble} sealed victory from the penalty spot before {player:Maiga} scored a wonderful solo goal to compound {team:Southampton}'s misery. 
Victory consolidated {team:West Ham}'s position in the top half of the table while {team:Saints} slip into the bottom three, after {team:Norwich}'s win against {team:Arsenal} in Saturday's late kick off.
The game was evenly poised at the break but {team:Southampton}'s early season demons continue to haunt them with yet more poor defending denying them any chance of getting something from this game.
{manager:Adkins} cut a forlorn figure in the post match press conference, bemoaning his side's defending, and he will know that something will need to change at the back or his side will quickly turn into relegation fodder.
They have now conceded 24 goals this season - a return that is quite simply not good enough.
The corresponding fixture last season saw {team:Saints} earn a valuable draw that eventually helped them gain automatic promotion from the {league:Championship} at the expense of {team:West Ham}.
{team:Southampton} have trailed in all eight {league:Premier League} games at some point, the only team to be behind in all of their league games.
{team:The Hammers} had to rely on the lottery of the play-offs to go up but despite missing out to Saints and Reading on those automatic places, it is {manager:Sam Allardyce}'s experienced {team:West Ham} side who have made the better start to the new {league:Premier League} campaign. 
With 14 points already, {team:the Hammers} have a healthy return from their opening eight games while Saturday's game was further evidence that {team:Southampton} are yet to settle into {league:Premier Leagu}e life. 
{team:Saints} took the decision to drop star striker {player:Rickie Lambert} prior to the game but the visiting side made a bright start without creating a clear opening. The first real chance fell {team:West Ham}'s way with {player:Jos Hooiveld} heading {player:James Tomkins}' looping header from 12 yards off the line. 
{player:James Collins} later went close with a header but by and large this was a poor first half played in a turgid atmosphere with the moans and groans of the {stadium:Upton Park} faithful growing louder as the half wore on. 
{manager:Allardyce} also cut a frustrated figure on the sidelines chewing gum with more urgency and intensity than his lack-lustre side, while for {manager:Adkins} it was a case of job done as the teams went into the half-time break level. 
But it took just 58 seconds for {team:the Hammers} to buck up their ideas and typically it came from poor {team:Southampton} defending with no-one dealing with {player:Noble}'s inswinging 35-yard free kick that eventually bounced in at the back post. 
It was the 21st goal {team:Saints} had conceded this season, a symptom of their early-season troubles and just a minute later there was worse to come for {manager:Adkins}' side. 
{player:Yossi Benayoun} showed more desire to get to the ball than {player:Maya Yoshida} and his cross to the back post was tapped into the empty net by {player:Nolan}. 

                            
                        
With {stadium:Upton Park} lifted and the supporters buoyed, {team:Southampton}'s confidence evaporated as the away fans called for their talisman to be introduced - {player:Lambert}. 
But it was the man that was playing up front in his place - {player:Jay Rodriguez} - who helped get {team:Saints} back in the game, showing quick feet to turn inside {player:Collins} before releasing {player:Lallana}. With his back to goal the midfielder swivelled delightfully before powering a shot into the roof of the net. 
{player:Lambert} was then introduced and he went desperately close with a free kick from 20 yards. 
But {team:the Hammers} soon restored their two-goal advantage with another {player:Noble} free kick causing {team:Saints} problems and {player:Jose Fonte} was penalised for handball after coming under pressure from {player:Andy Carroll}. 
{player:Noble} powered in the spot kick to a collective sigh of relief and {player:Carroll} continued to cause {player:Hooiveld} problems at the back before he was replaced to a standing ovation.
The on-loan {team:Liverpool} striker is quickly on his way to becoming a cult hero at {stadium:Upton Park}. 
{team:West Ham} pressed for a fourth and it came late on through {player:Maiga} who sealed the rout with a wonderful solo goal that embarrassed three {team:Southampton} defenders.
Live text commentary