::{manager:Gordon Strachan} urges Scottish players to work harder::
@1381596985
url: https://www.bbc.com/sport/football/24508435
{country:Scotland} boss {manager:Gordon Strachan} has told the nation's footballers to train harder if they want to improve.
His national team are preparing for the final {league:2014 World Cup} qualifier against {country:Croatia} on Tuesday, but {manager:Strachan} put out a rallying call to all Scots.
"We need to do something ourselves; stop feeling sorry for ourselves in {country:Scotland}," he told BBC Radio {country:Scotland}.
"Don't go to snooker halls or watch the telly. Practise longer. If you do that you will become a better player."
"Team spirit is all about performances; putting in performances and winning. It's not about going to the go-karts and zooming round there, going for a drink and talking about everyone that's not there, or golfing. That's nonsense. Winning games of football - that's what bonds people together."
{manager:Gordon Strachan}
{manager:Strachan} has won two of his five competitive games as {country:Scotland} boss after succeeding {manager:Craig Levein} in February.
Their hopes of qualifying for {country:Brazil} in 2014 were extinguished in March but {manager:Strachan}, capped 50 times for {country:Scotland} as a player, wants to end this campaign on a high - especially as they have not had a home win in Group A.
Addressing the wider issue of the quality at his disposal as he looks ahead to the {league:European Championship} qualification draw for {country:France} 2016, {manager:Strachan} emphasised the importance of young Scottish players aiming to be the best they possibly can.
"Work harder, just work harder to be a better player," he told BBC Radio {country:Scotland}'s Sportsound programme.
"Discipline yourself to be a better player. Do more hours with the ball to be a better player.
"We took five players from the Under 21 squad to train with us on Friday and I enjoyed working with them. It was {player:Callum Paterson}, {player:Fraser Kerr}, {player:Andy Robertson}, {player:John Herron} and {player:Mark Beck}. {player:Andy} was inspirational for the squad. I thought he was terrific."
{manager:Gordon Strachan}
"Put more hours in - don't sit back and moan and groan about foreign players. 
"We must get better ourselves. If you work harder at the game, we'll get better. If the standard comes up, the ordinary foreign player won't be here.
"When we were at Coventry the Under 21s had to train at least three afternoons per week. Not train; train in the morning and work on your technique in the afternoon."
The former {team:Celtic} manager also lamented the "economics" that mean {country:Scotland}'s top-flight clubs are not able to hold on to their best players.
"I was speaking to {manager:Walter [Smith]} three or four years ago and I think there were at least 70 players in the {league:[English] Championship} who were at a {league:Scottish Premiership} club previous to that," {manager:Strachan} said.
"And they only moved there because the wages were being quadrupled.
"Imagine if you'd kept those 70 or 80 players in {country:Scotland} because the wages were decent - the quality would be far better."