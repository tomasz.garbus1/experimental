::{team:Bayern Munich}: {manager:Jupp Heynckes} hopeful despite duo's absence::
@1509394225
url: https://www.bbc.com/sport/football/41811239
Manager {manager:Jupp Heynckes} says he will find an answer to {team:Bayern Munich}'s forward dilemma for Tuesday's {league:Champions League} meeting with {team:Celtic}.
{team:Bayern} are without unfit {player:Robert Lewandowski} and {player:Thomas Muller} as well as {player:Kwasi Okyere Wriedt}, who is not registered for the {league:Champions League}.
Wingers {player:Kingsley Coman} and {player:Arjen Robben} are available and young forward {player:Manuel Wintzheimer} is also in the squad.
"There might be a possibility of playing 4-4-2," said {manager:Heynckes}.
"But we will make that decision tomorrow.
"Obviously {player:Muller} and {player:Lewandowski} being injured was unexpected but I will find a solution.
"Every team faces difficulties with injuries, many other teams in the {league:Champions League} experience the same but we find solutions.
"While {player:Arturo [Vidal]} is very fast and very good up front I have a different plan for tomorrow.
"I have not worked often with {player:Wintzheimer}, I don't know him well enough and, as I said before, there might be some other solutions."
{team:Bayern} chief executive Karl-Heinz Rummenigge had earlier said of {player:Lewandowski}'s omission on the club's website: "If we had played against {team:Real Madrid}, he might have gone.
"The coach does not want to risk anything as Saturday's game [away to {team:Borussia Dortmund}], which is very important for us, is in the back of his mind."
When asked about those comments, {manager:Heynckes} said: "Rummenigge was a world-class footballer and professional and he has his own opinions.
"But in my career I have always protected my team and with {player:Lewandowski} not being 100% fit it would make no sense to make him play tomorrow.
"Obviously we have to take precautions. {player:Lewandowski} would still be receiving therapy and tomorrow would be far too early for him to play but next week it might be different."