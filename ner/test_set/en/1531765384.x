::World Cup 2018: Why the tournament in Russia was statistically the best::
@1531765384
url: https://www.bbc.com/sport/football/44849930
Four simple words, used variously as a statement or a question across the country during a baking June and scorching July: "Best World Cup ever?" 
The 2018 edition of international football's ultimate gathering was not the most-anticipated tournament in the game's long history, for a variety of reasons, but perhaps that was a good thing. 
Because from the moment hosts Russia smashed five goals past Saudi Arabia in the opening game, there was barely any let-up in drama and excitement. The group stage, the knockout stage, even the final - so often a cagey procession to extra time in recent World Cups - delivered what they needed to. 
In a football world of plenty, the relative infrequency of World Cups - held only every four years - means the tournament retains a fascination and cultural heft that easily outweighs the now technically superior club game. 
There are, it seems, certain obligatory ingredients to a memorable World Cup, so let's see how 2018 really measured up:

                            
                        
Unlike a league season, the World Cup does not have the time for a slow-burning narrative to take shape. 
It needs short, sharp hits of drama from the very start. 2018 did not disappoint, with the 3-3 draw between Spain and Portugal on day two immediately, and rightly, adopted as a tournament classic. Cristiano Ronaldo's late free-kick to ensure Portugal took a point capped a day of drama, with winning goals for Uruguay against Egypt in the 89th minute and Iran against Morocco in the 95th. 
Overall there were nine winning goals (plus four defeat-avoiding equalisers) scored in the last minute or injury time of games, more than any previous edition of the World Cup and just one fewer than the five tournaments from 1998-2014 combined.
Late goals are the football equivalent of snapping that bit of plastic off an old cassette tape to prevent it being recorded over. They seal in the drama for good.

                            
                        
The formula for this is delicate. 
If too many big sides exit early it can make the conclusion of the tournament a bit underwhelming (see: World Cup 2002) but no-one wants to see all the favourites cruise through to the latter stages untroubled. 
As it was, reigning champions Germany going out in the group stage, and then Spain, Argentina and Brazil stumbling in the knockout stage was just about right. 
The Germans managed to be both uninspiring and particularly unlucky, their total of 72 shots surpassed by only five teams in the tournament, four of whom played four more games than Joachim Low's team. 
At least Manuel Neuer ended the tournament as the only goalkeeper to make two dribbles, including one particularly unsuccessful one in the opposition half against South Korea. Redefining the position indeed. 
We went into 2018 wondering which of Ronaldo or Lionel Messi would finally dominate a World Cup.
Ronaldo's hat-trick against Spain was electrifying but also a false dawn, while Messi could not carry a reeling Argentina team who started with a different formation in all four of their games as they searched in vain for a functional combination. 
It seems extraordinary, given their club exploits, but neither Ronaldo nor Messi has ever scored in a World Cup knockout match. 

                            
                        
The baton of superstardom was supposed to pass to Neymar, but despite ending the World Cup with the most shots (26) and the second-most chances created (23) it was the fact he was the second most-fouled player (26 times in five matches - just behind Eden Hazard with 27 in six) that stood out. Yes, the Brazilian was targeted, but his ostentatious horizontal suffering left many with strong feelings of distaste.
Instead, it was France's teenage hero Kylian Mbappe who enjoyed a breakthrough. Two goals against Argentina in the last 16 saw him become the first teenager since Pele to score twice in a World Cup game, and he followed that by becoming the youngest player since Pele to score in a World Cup final.
Pele was good at football, and it very much seems Mbappe might be too. 
In 1966 it was dogs liberating trophies, in 2014 it was goalline technology and referees spraying foam to mark free-kicks and defensive walls (note how much vanishing spray has become part of the game by the fact no-one deemed it worthy of mention throughout the tournament). 
In 1990 we had such a defensively stifling tournament the laws of the game were altered with the outlawing of goalkeepers being able to pick up (most) backpasses.
In 2018 it was all about VAR and although this is not the place to discuss individual incidents across 64 games, we can certainly see the effect it had on the football.
The most obvious is the sheer number of penalties. As the tournament began it was clear that players were not prepared for the scrutiny that VAR would subject them to, with day three featuring five penalties. Three were scored that day, contributing to a record 22 penalty goals in a single World Cup.

                            
                        
It seemed, though, that most players had cottoned on by the latter stages, with no penalties awarded in any of the quarter-finals or semi-finals before possibly the most unsound VAR penalty decision of the tournament in the final, when referee Nestor Pitana eventually penalised Croatia's Ivan Perisic for handball. 
A corollary of all these penalties was matches that might have ended goalless did not. The World Cup ended with just one 0-0 draw (the execrable dead rubber between France and Denmark), the lowest number since the 1954 edition - an era when defending was an optional extra - which did not feature a single one.
There was also a phenomenal number of set-piece goals, with 43% of strikes coming in that fashion, the highest proportion in any World Cup from 1966 onwards. In a world where international sides do not necessarily have the time or roster to hone their playing styles like club sides, training-ground manoeuvres have become more vital than ever.
It had been so long since England had captured the public's imagination at a World Cup that a song from 1996 (Three Lions - released closer to the first broadcast of Fawlty Towers than to now) became the theme of the endless summer. 
Football didn't quite come home, but England equalled their best performance in a foreign World Cup and the likes of Maguire, Trippier and Pickford have joined Gascoigne, Waddle and Platt as surnames that will echo down the national consciousness for generations.
Harry Maguire had more than twice as many touches in the opposition penalty area (23) as any other defender in the competition as well as the joint-most headed attempts of any player (nine). Kieran Trippier created more goalscoring chances than any other player (24), finishing ahead of Neymar, Kevin de Bruyne, Luka Modric, Eden Hazard and Philippe Coutinho, in that order. Some company to be keeping.

                            
                        
And let's celebrate Harry Kane, only the second Englishman to win the Golden Boot at the World Cup. 
The Tottenham man scored with all six of his shots on target (one an inadvertent clip of his heel against Panama), so certainly had luck on his side, but this is the World Cup where you play a maximum of seven games in a variety of conditions, so luck plays an even bigger role than it usually does. 
And anyone who bemoans the fact 50% of Kane's total came from penalties should note that in 1966 four of Eusebio's Golden Boot haul came from spot-kicks, and outside of Helmut Haller's extended family, absolutely no-one has ever complained.