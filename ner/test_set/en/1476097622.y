::{country:Slovakia} v {country:Scotland}: Three players for {manager:Gordon Strachan}'s team to watch::
@1476097622
url: https://www.bbc.com/sport/football/37608160
{country:Slovakia} slumped to their second successive 1-0 defeat at the start of {league:World Cup 2018} qualifying on Saturday night in {country:Slovenia}. 
Now they simply must beat {country:Scotland}, with the visitors also knowing that anything less than a victory will leave them with an uphill struggle to reach the play-offs. 
BBC {country:Scotland}'s {player:Alasdair Lamont} was at Saturday's match in Ljubljana and pinpoints some of {country:Slovakia}'s strengths and weaknesses.
{player:Jan Durica}: The veteran defender is at once a key component of {manager:Jan Kozak}'s defence and a potential liability. In the absence of the suspended {player:Martin Skrtel} against {country:Slovenia}, {country:Slovakia} reverted to a back three, which looked flustered by the pressing of their opponents. 
On one such occasion, the 34-year-old made an outstanding last-ditch challenge to prevent a likely goal. But though he looks assured in bringing the ball out from the back, any pace he once had is fading and on more than one occasion the alertness of the Slovenian attack left him struggling. 
He was posted missing at the winning goal and if he partners Skrtel against {country:Scotland}, some of his frailties could be exploited.
{player:Juraj Kucka}: The experienced midfielder worked energetically, without ever earning any significant reward for his efforts. But the {team:Milan} player did display evidence of his undoubted class at times, principally when working back the way to assist his under-pressure defence. 
Like many of his team-mates, though, he failed to hit the heights he is capable of, something he will surely look to put right against {country:Scotland}.
{player:Marek Hamsik}: The same goes for captain-for-the-night {player:Hamsik}. The {team:Napoli} star showed only fleeting glimpses of his creative brilliance, shackled as he was by {player:Rene Krhin}. 
Given the chance, {player:Hamsik}, 29, has shown at both club and international level he can both create goals and score them himself from an advanced midfield role. 
Against {country:Slovenia}, he had virtually no opportunity to impose himself in that manner and with {manager:Steven Pressley} in attendance to file a report for {country:Scotland} boss {manager:Gordon Strachan}, a man-marking job on one of {league:Serie A}'s biggest talents might be a worthwhile recommendation.