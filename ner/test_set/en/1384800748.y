::{team:St Johnstone}: {player:Lee Croft} training with former club::
@1384800748
url: https://www.bbc.com/sport/football/24992318
Midfielder {player:Lee Croft} is training with {team:St Johnstone} and could make a return to {stadium:McDiarmid Park}.
{player:The 28-year-old} was with {team:Saints} for the second half of the 2011/2012 season, scoring three goals in 13 appearances.
He played in more than 50 games for {team:Oldham} last term but has been without a club since the summer.
"I know what we're getting and both parties will assess the next few days and make a decision," manager {manager:Tommy Wright} told the club website.
{manager:Wright} added that he expected to know whether or not {player:Croft} would sign on "in the next week to 10 days".