::Plymouth Argyle future still in balance::
@1301513564
url: https://www.bbc.com/sport/football/12913544

                            
                        
Plymouth Argyle's future remains in the balance after it was revealed that two potential bidders have yet to agree the financial terms.
The relegation-threatened League One side entered administration this month owing about £13m.
"They will proceed if we can get the secured debt down to a level they're prepared to pay," the club's financial advisor Peter Ridsdale told BBC Sport.
"At the moment it is in excess of what the bidders are willing to take on."
Ridsdale added: "The club can get to the end of the season through the goodwill of the players and staff. But it can't be right and proper that the staff and players go without pay."
Argyle's playing squad have not been paid by the club in 2011, relying instead on loans from the Professional Footballers' Association.
On Tuesday, BBC Spotlight revealed Cornish-based property developer Kevin Heaney was the preferred bidder.
The other bidder is reported to be Devon-based property developer James Brent.