::{manager:Paul Ince}: {team:Blackpool} manager set to return from stadium ban::
@1385728447
url: https://www.bbc.com/sport/football/25142012
{team:Blackpool} manager {manager:Paul Ince} is excited by his return to the touchline after completing a five-match stadium ban.
{manager:Ince} was punished  relating to his actions after a victory at {team:Bournemouth} in September, including one of violent conduct.
"It's been a while, but it's my own fault that it's been a while," he told BBC Radio Lancashire ahead of the visit of {team:Sheffield Wednesday} on Saturday.
"I've done my time and I just want to focus on getting back to winning ways."
19 Oct: W 1-0 v {team:Wigan} (h)
26 Oct: D 2-2 v {team:Blackburn} (h)
2 Nov: W 1-0 v {team:Nottingham Forest} (a)
9 Nov: L 2-3 v {team:Ipswich} (h)
23 Nov: D 1-1 v {team:Birmingham} (a)
{team:The Seasiders} took eight points from five matches in {manager:Ince}'s absence,  to the campaign, and they lie in the play-off spots ahead of their home fixture against {team:the Owls}.
"At the start of it, it seemed a long time, but it has soon come around," said the 46-year-old. "[Assistant manager] {manager:Alex Rae}, [coach] {manager:Steve Thompson} and the staff have done a fantastic job in just keeping us ticking over.
"There have been disappointments along the way, and you're going to get them in football whether I'm there or I'm not. The fact that I'm back - hopefully it will be great for me, great for the players and we'll see a response on Saturday."
{manager:Ince} admitted he would have to learn to "bite his lip" when the  in October.
The FA document said the ex-{country:England} captain pushed fourth official Mark Pottage in the tunnel after the match and 
Despite  on 14 September, {manager:Ince} had been angered by referee Oliver Langford's decision to send off full-back {player:Jack Robinson} and was later dismissed himself after  as he reacted to his side missing a late chance.
Meanwhile, {team:Blackpool} decided against a move for {team:Wolves} midfielder {player:Jamie O'Hara} before Thursday's deadline for emergency loan signings.
The 27-year-old had been 
"He's a fantastic talent," said {manager:Ince}, himself a former {team:Wolves} player. "{team:Wolves} let us take a look at him, but the problem we had is that the loan window shut on Thursday. To make a decision on someone in three days' training is always going to be hard.
"If the window was shutting next week, we would probably do something. But I'm not going to start rushing into things that I'm not sure about."
On-loan full-back {player:Bradley Orr} is expected to be out of action for around eight weeks with a thigh injury. Orr, 31, has played four times for the club since 