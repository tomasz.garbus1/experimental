::{player:Leonardo Ulloa}: {team:Leicester City} seal Â£8m deal for {team:Brighton} striker::
@1406027481
url: https://www.bbc.com/sport/football/28363371
{team:Leicester City} have signed {team:Brighton} striker {player:Leonardo Ulloa} for a club-record £8m on a four-year deal.
The fee for the 27-year-old Argentine, who scored 16 goals for {team:the Seagulls} in all competitions last season, could reach £10m based on appearances.
{team:Brighton} chairman {manager:Tony Bloom} said the {league:Championship} club accepted a record fee after {player:Ulloa} made it clear he wanted to play in the {league:Premier League}.
"It was then a case of ensuring we received our valuation," he said.
{player:Ulloa} joined {team:Brighton} on a four-and-a-half-year deal from Spanish side {team:Almeria} for about £2m in January 2013.
{league:Championship} winners {team:Leicester}'s previous record buy was {player:Ade Akinbiyi}, whom they bought for £5.5m in July 2000.
{team:Brighton}'s previous record sale was midfielder {player:Liam Bridcutt}'s £3m move to {team:Sunderland} in January.