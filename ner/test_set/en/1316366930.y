::{team:Africain} and {team:Sunshine} reach {league:Confederation Cup} semi-final::
@1316366930
url: https://www.bbc.com/sport/football/14966537
{country:Tunisia}n side {team:Club Africain} finished top of Group A of the {league:Confederation Cup} thanks to their 1-0 away win {team:Kaduna United} of {country:Nigeria} on Sunday.
{team:InterClube} of {country:Angola} also advanced to the semi-final despite losing 1-0 to Ivorian side {team:Asec Mimosas} in the group's other game.
{team:Africain} finished top with 11 points followed by {team:InterClube} on 10 points.
The lower placing also changed with {team:Asec} coming third on seven points and {team:Kaduna United} last with two fewer.
{team:Club Africain} will play {team:Sunshine Stars} of {country:Nigeria} and unbeaten Group B winners {team:MAS} of {country:Morocco} meet {team:InterClube} in the semi-finals in October as the pursuit of a $660,000 first prize intensifies.
{player:Chaker Rguii}, a 24-year-old {country:Tunisia}n midfielder, converted a penalty on the stroke of half-time to give {team:Club Africain} a 1-0 victory over {team:Kaduna} in the northern {country:Nigeria}n city.
{team:United} needed maximum points to squeeze into the last four on the head-to-head rule only to let down again by poor home form as they gathered just four points from a possible nine in the mini-league phase of the second-tier competition.
{team:Club Africain} are the only semi-finalists to have triumphed in Africa having lifted the {league:African Champions Cup} - later renamed the {league:African Champions League} - 20 years ago with a 7-3 aggregate triumph over {team:Nakivubo Villa} of {country:Uganda}.
{team:Asec}, also former African champions, rounded off a disappointing pool campaign with a 1-0 win over {team:InterClube} thanks to a mid-first half goal from {player:Hugues Zagbayou} in Ivorian commercial capital Abidjan.