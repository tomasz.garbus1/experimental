::{player:Kevin McDonald} hopeful of {team:Sheffield United} stay::
@1340291797
url: https://www.bbc.com/sport/football/18539306
{team:Sheffield United} midfielder {player:Kevin McDonald} is hopeful of being able to come to an agreement with the club over a new deal.
The 23-year-old midfielder is out of contract at the end of this month.
He told BBC Radio Sheffield: "I'd like to say I'm fully confident of being back next season but I'm not in a position to do so at present.
"I want to get it sorted out soon and if they offer me the right deal I'd be delighted to sign."
He added: "My agent has been speaking to the club a couple of times a week and we're waiting to hear back from them.
"No one else has made me an offer."
The former {team:Burnley} man is among a number of players - including striker {player:Chris Porter} and winger {player:Lee Williamson} - who are considering new deals.
And {player:McDonald} believes that {team:the Blades}, who lost to {team:Huddersfield} in the {league:League One} play-off final last season, are capable of mounting another promotion push.
"I don't know what's going on with the others but I think that they're in a similar position to me in that they'd like to come back to the club," he continued.
"I think with the players that are still there we'd have an unbelievable chance of going up next season."