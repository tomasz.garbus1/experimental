::Liverpool striker Luis Suarez charged by FA over hand gesture::
@1323284603
url: https://www.bbc.com/sport/football/16043901
Liverpool striker Luis Suarez has been charged with improper conduct by the Football Association over an allegedly abusive hand gesture during the 1-0 defeat at Fulham on Monday.
Suarez appeared to raise the middle finger of his left hand towards the home fans as he left the pitch.
Liverpool have also been charged with failing to control their players after the dismissal of Jay Spearing. 
The club said they would review the FA documentation before making comment.
Suarez and Liverpool have until 1600 GMT on Monday 12 December to respond to both charges.
Uruguay star Suarez is now the subject of two FA disciplinary cases.
Last month, he was charged with racially abusing Manchester United defender Patrice Evra during Liverpool's 1-1 draw with Manchester United in October.
The latest incident occurred at the end of a damaging defeat for the Reds, during which Suarez was subjected to chants of "cheat" from sections of the home crowd.

                            
                        
They barracked the player for what they saw as him going down too easily in an attempt to win penalties and free-kicks.
After the match, Reds boss Kenny Dalglish said he had not seen the picture of the hand gesture. "If [it] is true then I've a decision to make," the Scot added.
Dalglish defended the striker over accusations of diving and described the taunts as "scandalous".
He added: "At the end of the day, we will look after Luis the best we can and I think it is about time he got a bit of protection from some people."
The charge against Liverpool concerns the reaction of their players to Spearing's controversial second-half sending-off.
Five players including Spearing surrounded referee Kevin Friend after he showed the midfielder a straight red for a foul on striker Moussa Dembele with his follow-through after a tackle, having initially won the ball.
The dismissal came after 71 minutes, with the score at 0-0, before Clint Dempsey's late winner sealed victory for Fulham to move them 13th in the league.
Dalglish added: "It is frustrating because nobody tells us what the level of acceptance is. Jay had no other thought on his mind other than to win the ball - and he did win it."
Spearing will miss Liverpool's next three matches after the Reds opted not to appeal against the sending-off.