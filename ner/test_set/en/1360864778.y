::{team:Gillingham}: {player:Michael Richardson} and {player:Ryan Williams} join on loan::
@1360864778
url: https://www.bbc.com/sport/football/21460565
{league:League Two} side {team:Gillingham} have completed the loan signings of {team:Newcastle} midfielder {player:Martin Richardson} and {team:Fulham} winger {player:Ryan Williams}.
Both {player:Richardson}, 20, and {player:Williams}, 19, make the move to {team:Priestfield} on initial one-month deals.
{player:Richardson} has not featured for {team:the Magpies} but played four games while on loan at {team:Leyton Orient} last season.
{player:Williams} joined {team:Fulham} from {team:Portsmouth} in January last year but has not made a first-team appearance.
{player:The Australian} came through the academy at {stadium:Fratton Park} and made six appearances for {team:Pompey} last season.
{team:Gills} boss {manager:Martin Allen} said: "{player:Ryan} is positive, can open up defences, is fast, direct and is a good player."
{player:Ryan} is the younger brother of {team:Middlesbrough} defender {player:Rhys}, while his twin {player:Aryn} is on the books at {team:Burnley}.