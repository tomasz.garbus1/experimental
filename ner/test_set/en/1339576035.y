::{manager:Harry Redknapp} faces uncertain {team:Tottenham} future despite denial::
@1339576035
url: https://www.bbc.com/sport/football/18418066

                            
                        
{manager:Harry Redknapp}'s position at {team:Tottenham} is increasingly uncertain despite his strenuous denial that he would resign as manager. 
{manager:Redknapp}, 65, will hold talks with chairman {manager:Daniel Levy}, possibly as early as Wednesday, to discuss his future. 
The pair are understood to remain some distance away from an agreement over a new contract, with {manager:Redknapp}'s deal due to expire in June 2013.
{manager:Levy} is believed to have been left dismayed by {team:Spurs}' end-of-season slump.
It comes after {manager:Redknapp} rejected internet speculation he had quit, telling ESPN on Tuesday: "No, I haven't resigned and I have no idea why it's being suggested I have. 
"This is an outrage - an absolute liberty for people to be putting around this kind of rumour on the internet.
"It is not true, there is not a chance I will resign. Why should I? I have a year left on my contract."
{team:Chelsea}'s {league:Champions League} triumph meant that, despite finishing fourth, {team:Spurs} missed out on the final place in next season's competition and the added revenue that goes with it.
{manager:Redknapp}'s team won just four of their final 13 {league:Premier League} matches, with a 5-2 defeat by {team:Arsenal} on 26 February the catalyst. {team:Spurs} had led 2-0 and victory would have opened up a 13-point gap over {team:the Gunners}, who pipped them to third place on the final day of the season. 
Negotiations over an extended contract for {manager:Redknapp} have stalled since the start of the year when he was linked with the then vacant {country:England} manager's job. 
The former {team:West Ham} and {team:Portsmouth} manager told BBC Sport last week that he would have taken the {country:England} coach's role if it had been offered to him.
{manager:Redknapp}, who recently rejected suggestions linking him with a club in {country:Qatar}, had warned that ongoing uncertainty about his future would affect {team:Spurs} players.
The manager, who guided the club into the {league:Champions League} for the first time in 2010, said: "The simple situation is I've got a year left on my contract. It's up to {team:Tottenham} whether they want to extend that contract or not.
"If they don't extend it and I go into my last year, it's not an easy one when players know you've only got a year left.
"It's not a case of me looking for security. What it's about is players knowing you've only got one year left on your contract and knowing that it doesn't work, basically.
"I think it's a situation of 'well, he might not be here next year'. You don't let players run into the last year of their contract if you think they're any good and you don't let managers run into the last year of their contract if you think they're any good.
"It's up to {team:Tottenham}. If they think I'm OK and I've done a decent job and deserve an extension, they'll give it to me."