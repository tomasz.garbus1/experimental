::Deane Smalley aim to prove Oxford United doubters wrong::
@1342692763
url: https://www.bbc.com/sport/football/18901661
Oxford United striker Deane Smalley is determined to prove the doubters wrong after a poor first season at the League Two club.
Smalley, 23, has failed to convince since signing signed from Oldham last summer, scoring just twice for the U's.
So poor was his form that he was sent out on loan to Bradford in January.
"My long term plan is to stay here, it's a fantastic club that is moving forward and hopefully I can help them be a success," Smalley told BBC Oxford.
Smalley scored the only goal of the game in Tuesday's 1-0 pre-season victory over West Ham and he says he hopes he can continue that form over the next few weeks.
He also says he knows he has a point to prove to sceptical Oxford fans.
 It's up to him to play well, we had a talk in the summer and he wants to show the supporters what a good player he is
He said: "Last season was a bit disappointing from my point of view, but I've stayed very fit over pre-season and hopefully I can keep in this condition and prove to the gaffer I can score goals and help this team be successful."
U's boss Chris Wilder said Smalley is part of his plans and that he has been impressed with the striker's attitude during pre-season.
Wilder also believes too much may have been expected of Smalley when he first arrived at the club.
"Centre-forwards thrive on confidence and hopefully that [goal against West Ham] will give him a lift," said Wilder.
"He was a coveted player when we went after him and nothing has changed. 
"There is a lot of pressure on him and he didn't get off to a good start and maybe he felt the weight of expectation. 
"But it's up to him to play well, we had a talk in the summer and he wants to show the supporters what a good player he is.
"He's back in the building and is part of my plans."