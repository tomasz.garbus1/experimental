::Harry Redknapp faces uncertain Tottenham future despite denial::
@1339576035
url: https://www.bbc.com/sport/football/18418066

                            
                        
Harry Redknapp's position at Tottenham is increasingly uncertain despite his strenuous denial that he would resign as manager. 
Redknapp, 65, will hold talks with chairman Daniel Levy, possibly as early as Wednesday, to discuss his future. 
The pair are understood to remain some distance away from an agreement over a new contract, with Redknapp's deal due to expire in June 2013.
Levy is believed to have been left dismayed by Spurs' end-of-season slump.
It comes after Redknapp rejected internet speculation he had quit, telling ESPN on Tuesday: "No, I haven't resigned and I have no idea why it's being suggested I have. 
"This is an outrage - an absolute liberty for people to be putting around this kind of rumour on the internet.
"It is not true, there is not a chance I will resign. Why should I? I have a year left on my contract."
Chelsea's Champions League triumph meant that, despite finishing fourth, Spurs missed out on the final place in next season's competition and the added revenue that goes with it.
Redknapp's team won just four of their final 13 Premier League matches, with a 5-2 defeat by Arsenal on 26 February the catalyst. Spurs had led 2-0 and victory would have opened up a 13-point gap over the Gunners, who pipped them to third place on the final day of the season. 
Negotiations over an extended contract for Redknapp have stalled since the start of the year when he was linked with the then vacant England manager's job. 
The former West Ham and Portsmouth manager told BBC Sport last week that he would have taken the England coach's role if it had been offered to him.
Redknapp, who recently rejected suggestions linking him with a club in Qatar, had warned that ongoing uncertainty about his future would affect Spurs players.
The manager, who guided the club into the Champions League for the first time in 2010, said: "The simple situation is I've got a year left on my contract. It's up to Tottenham whether they want to extend that contract or not.
"If they don't extend it and I go into my last year, it's not an easy one when players know you've only got a year left.
"It's not a case of me looking for security. What it's about is players knowing you've only got one year left on your contract and knowing that it doesn't work, basically.
"I think it's a situation of 'well, he might not be here next year'. You don't let players run into the last year of their contract if you think they're any good and you don't let managers run into the last year of their contract if you think they're any good.
"It's up to Tottenham. If they think I'm OK and I've done a decent job and deserve an extension, they'll give it to me."