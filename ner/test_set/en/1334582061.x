::PFA boss Gordon Taylor 'amazed' by video technology delay::
@1334582061
url: https://www.bbc.com/sport/football/17725064

                            
                        
Professional Footballers' Association chief executive Gordon Taylor believes Fifa's current refusal to use video technology during games is "perverse".
Referee Martin Atkinson controversially ruled an effort by Chelsea's Juan Mata crossed the goal-line in his side's FA Cup semi-final win against Tottenham.
"It's amazing that football is behind the times in trying to achieve justice. It's perverse," Taylor told BBC Sport.
"I don't know why we should expect to put all the onus on referees."
In a league structure of 20 clubs you're probably talking about three months to install it properly, so this isn't an instant "plug-in and get on with it" type technology
Spurs boss Harry Redknapp has led renewed calls for video technology to be introduced after replays appeared to show Mata's second-half shot being blocked on the line by defender Benoit Assou-Ekotto.
Tottenham were trailing 1-0 at the time, before going on to lose Sunday's tie 5-1 at Wembley.
Fifa is currently testing two goal-line technology systems and the International Football Association Board [IFAB] is set to make a final decision whether to approve one or both on 2 July.
"Football is way behind other sports when we think how successful it is being used in cricket, tennis, rugby and so many other sports," added Taylor.
"I would like to think that it would be in place at the highest level by next season. The technology has been there for some time and when something is infallible it is going to be a help.
"In this day and age the technology is available and we should use it. We've got to do all we can to ensure that, in sport, justice is done."
The Football Association and the Premier League repeated their calls on Monday for goal-line technology to be introduced as soon as possible.
2005: Luis Garcia (Liverpool) v Chelsea
Garcia put the Reds into the Champions League final when his hooked effort was awarded by the referee, despite Blues defender William Gallas claiming he had cleared it from under the bar.
2010: Frank Lampard (England) v Germany
England thought they were back in the World Cup second round tie when Lampard's 20-yard strike bounced down off the bar. Replays showed Lampard had scored - but the officials disagreed.
2012: Clint Hill (Bolton) v QPR
Replays showed Hill's header crossed the line before keeper Adam Bogdan scrambled the ball away in a Premier League relegation battle. The officials ruled it did not and Bolton went on to win.
But FA general secretary Alex Horne warned it may not be in place for the beginning of next season.
"If you were looking at a league structure of, say, 20 clubs, you're probably talking about three months to install it properly in every venue, so this isn't an instant "plug-in and get on with it" type technology," he told BBC Sport. 
"We need to get there first and have it approved, hopefully with more than one technology so we can have a bit of competition in the market; then competitions themselves need to determine when they want to introduce it and get on with procuring it and installing it. So it will take a little time to come after 2 July." 
Former Premier League referee Dermot Gallagher insists officials are fully supportive of video assistance.
"This kind of technology wouldn't take anything away from referees' authority," he said. "It will give a clear indication - goal or no goal.
"Every referee at every level, if given the opportunity to have that in their armoury, would say 'yes'."
Taylor, whose union represents 4,000 current professional players in England and Wales, says the governing bodies would have to review the success of any future goal-line technology before deciding whether to extend its use to settle other controversial decisions.
Questions have been raised over the awarding of two recent penalties to Premier League leaders Manchester United following challenges with seemingly minimal contact on winger Ashley Young.
United boss Sir Alex Ferguson admitted Young "overdid it" when he earned a spot-kick in Sunday's 4-0 win over Aston Villa, which came a week after the same player also won penalty against Queens Park Rangers.
The incidents have led to further discussion about top-flight players falling too easily in a bid to earn their team an advantage.

                            
                        
"In professional sport, it's a really difficult one," said Taylor, after being asked whether players should take more responsibility to help referees make the right decision.
"It's part and parcel of the game that if you are a forward and the defender does make contact, no matter how small, then unless you do go down you've got no chance of getting a penalty.
"Sometimes you're trying to keep your balance, trying to stay up, you miss the moment and then you're getting criticised by your manager for not going down.
"When it's blatant the referees have to take strong action."
Taylor added he believes instances of players diving to win penalties has not increased from previous eras.