::Burton to act as catalyst for English football revival::
@1310018870
url: https://www.bbc.com/sport/football/14051166
The man at the head of the new National Football Centre remains confident {country:England} will win the {league:World Cup} in his lifetime.
Given {country:England}'s poor {league:World Cup} record, it is a bold statement from David Sheepshanks, who is chairman of St George's Park (SGP) in Burton-on-Trent.
Sheepshanks hopes SGP, which should be up and running by the autumn of 2012, will provide the catalyst to help change the way managers, grassroots coaches and players think about the game in {country:England}.
"If we do this right then the benefits will be enormous," Sheepshanks told BBC Sport.
"I wouldn't want to say we will win the {league:World Cup} before the 2020s, but you never know and there might just be a quicker bounce effect from SGP that will be to our massive advantage as a country."
We can't wave a magic wand
The thinking behind SGP is that it will provide a 'spiritual' home for English football - a place where coaches can congregate and assimilate and share ideas, before spreading the good word of technical, physical and medical best practice to millions of young players.
"We want more home-grown managers and coaches operating at the highest level," Sheepshanks told BBC Sport. "We would like to have a plethora of very highly qualified managers competing to run our top teams.
"We think by having a much larger and more highly qualified coaching work force we will raise the standard of the game throughout football - and that includes behaviour as well as the culture - from the grassroots to the very top.
"I believe passionately in what I and others are doing with SGP," added Sheepshanks, who emphasised the positive support the National Football Centre has been given by the {league:Premier League} and {league:Football League}, as well as the League Managers Association and the Professional Footballers' Association.
"We have to walk our talk. It's not just about creating a wonderful environment but staffing it with the right calibre people, who can be the tutors and the coach educators that will transform the way we operate as a nation."
Historically one of {country:England}'s problems has been that it lags behind its major European rivals as regards the number of highly qualified coaches we produce. 
According to the FA's website, {country:England} have less than 10% of the average number of Uefa B coaches, only 16% of the number of Uefa A coaches; and just 12% of the number of Uefa Pro coaches compared to {country:Italy}, {country:France}, {country:Spain} and {country:Germany}.
At the moment even {country:Norway}, {country:Hungary} and {country:Belgium} have more top-level coaches than {country:England}.
But it is just not about increasing the number of coaches, it is also about improving their calibre.
"What I've noticed from visiting European clubs is that they have more full-time coaches in key areas," said professional scout David Webb, who has the Uefa A and Academy Managers licence and has visited a number of leading European clubs.
"For example a full-time under-11 coach in {country:Germany}, who might be more highly paid than the under-18 coach, would be deemed key for the transitional phase of the players. I don't know of any English club that has that set-up.
"That under-11 coach has not only high-level coaching qualifications, but also a good educational and academic background. That age is deemed important because of the switch to secondary schools and also in their football development."
Webb, who is currently studying for an MSc in Sports Psychology, added: "There is a real understanding of a child's emotional development and where the kids have come from.
"I noticed there is more of human and cultural understanding with foreign coaches. And there is a real understanding of the type of coaching kids need at certain ages. It's a very systematic and analytical approach to development."
It it is hoped that SGP will help double the number of English B, A and pro-licence coaches by 2018 to over 5,000.
"We can't wave a magic wand," Sheepshanks told BBC Sport.
"The exact number of coaches guarantees you nothing, but we have a great opportunity to learn from our peers abroad and do better."
English football has often been accused of short-termism, but former {team:Ipswich Town} chairman Sheepshanks, who was born in 1952 - 14 years before {country:England} won the {league:World Cup} - is determined that is one charge that will not be levelled at SGP.
"It's analogous to club academies. I know I'm no longer at Ipswich but we started that academy in 1998 and 13 years later the club sold {player:Connor Wickham} to {team:Sunderland} for an initial fee of £8.1m.
"The benefits of SGP will be long-term, but we also hope that there will be a bounce. A year after the French academy Clairefontaine opened, {country:France} won the {league:World Cup}. 
"I'm absolutely not suggesting that will happen to England, but I'm hoping it will have a positive effect on the {country:England} teams as well as the wider game.
"You have to give a new educational system time to bed in. We want to educate a whole new generation of coaches and then give them time to influence the players that they coach."
Clairefontaine was not the only national academy that the FA looked at in preparing the groundwork for SGP.
Zeist in {country:Holland}, {country:Cologne} in {country:Germany}, {country:Spain}'s La Ciudad del Futbol and Coverciano in {country:Italy} as well as leading club academies were all visited to glean ideas from {country:England}'s major European rivals.
"We have been unashamed plagiarists in taking their best ideas as well as the big club centres in this country and abroad," added Sheepshanks. "This is {country:England}'s answer to those centres but it will be different.
"We're not trying to replicate them - we are trying to do the best for English football. Taking the best bits of those and the best bits of what we already do and creating a powerful amalgam of the best European practice.
"This country has suffered in that we haven't had a spiritual and physical home where components of English football can congregate and benefit from a continuous improvement ethos."
BBC Sport first spoke to Sheepshanks at a conference in May held at Sergei Baltacha's academy in a Docklands school and the former FA chairman singled out the Ukrainian facility as well as Watford's Harefield Academy for special praise.
"Sergei is enlightened. What he is doing with Belgian coach {manager:Michel Bruyninckx} is very far sighted - believing that the mentality of the player is all important and coaching the player's mentality.
"I really believe in that and the game in this country will embrace it increasingly. But there is not a lot of that happening to be honest at grassroots level. Sergei is way ahead of his time."
Apart from Sheepshanks, who was representing the Football Association, the only {league:Premier League} club that attended that conference at Baltacha's academy was {team:Fulham}.
Which perhaps provides a glimpse of the size of the task Sheepshanks and the Football Association face in their efforts to reshape {country:England}'s coaching culture.

                            
                        
