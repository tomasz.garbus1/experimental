::Reading 2-2 Ipswich: Reading's late header earns a draw::
@1541870584
url: https://www.bbc.com/sport/football/46081002
An 84th-minute header from Yakou Meite gave Reading a draw with Ipswich, after the visitors had led for the majority of the game.
Gwion Edwards scored inside five minutes for Ipswich with a right-footed shot that found the bottom left corner from outside the box.
It took the home side just two minutes to equalise with Meite's overhead kick.
The Tractor Boys responded once more in the 11th minute, as a mix-up at the back for Reading gave Freddie Sears the chance to get through on goal and slot past keeper Anssi Jaakkola.
However, Meite's late header from Leandro Bacuna's pass secured a point for the hosts.
New Ipswich manager Paul Lambert is still without a win in two matches in charge, but his side had control until the interval, with Jaakkola having to make smart saves to keep out two efforts from Jordan Roberts.
Ipswich were pegged back after the interval, with Reading showing much more intent going forward.
Meite looked to be through on goal, only to be flagged offside, then Bartosz Bialkowski made a point-blank save to deny Mo Barrow.
Ipswich sat back but almost extended their lead as Jaakkola had to move quickly to snuff out the danger from Roberts after an error by John O'Shea.
Ipswich, who had lost six of their previous eight Championship away games this season, were not able to withstand pressure from Meite late on.
The forward took his league tally to six goals in 15 games, which is more than he scored in 31 games for Sochaux in Ligue 2 last season.
Reading manager Paul Clement:
"Terrible performance from us in the first half, really bad, really disappointed, I was embarrassed actually, I can't get my head round why that was.
"I think we were fortunate to get in at the half, I don't think our defenders gave us the stability we needed.
"We deservedly got back into the game, we finished stronger than they did."
Ipswich manager Paul Lambert:
"I thought we were outstanding, I thought we passed the ball great, we looked great, we controlled the game.
"I'm really, really proud of them, we're disappointed with a draw, but the performance was excellent, especially first half."
Match ends, Reading 2, Ipswich Town 2.
Second Half ends, Reading 2, Ipswich Town 2.
Attempt missed. Sam Baldock (Reading) right footed shot from the centre of the box is too high. Assisted by Garath McCleary.
Attempt blocked. Garath McCleary (Reading) right footed shot from the right side of the box is blocked. Assisted by Marc McNulty.
Modou Barrow (Reading) wins a free kick in the defensive half.
Foul by Trevoh Chalobah (Ipswich Town).
Attempt missed. Sam Baldock (Reading) right footed shot from the centre of the box is too high.
Offside, Ipswich Town. Gwion Edwards tries a through ball, but Kayden Jackson is caught offside.
Jordan Spence (Ipswich Town) is shown the yellow card for a bad foul.
Modou Barrow (Reading) wins a free kick on the left wing.
Foul by Jordan Spence (Ipswich Town).
Goal!  Reading 2, Ipswich Town 2. Yakou Meite (Reading) header from the centre of the box to the bottom right corner. Assisted by Leandro Bacuna.
Substitution, Reading. Marc McNulty replaces Liam Kelly.
Corner,  Reading. Conceded by Jordan Spence.
Substitution, Ipswich Town. Danny Rowe replaces Freddie Sears.
Foul by Yakou Meite (Reading).
Cole Skuse (Ipswich Town) wins a free kick in the defensive half.
Substitution, Reading. John Swift replaces John O'Shea.
Attempt missed. Freddie Sears (Ipswich Town) right footed shot from the right side of the box misses to the left. Assisted by Kayden Jackson.
Offside, Ipswich Town. Luke Chambers tries a through ball, but Kayden Jackson is caught offside.
Corner,  Reading. Conceded by Freddie Sears.
Delay over. They are ready to continue.
Delay in match Cole Skuse (Ipswich Town) because of an injury.
Foul by Yakou Meite (Reading).
Matthew Pennington (Ipswich Town) wins a free kick in the defensive half.
Corner,  Ipswich Town. Conceded by John O'Shea.
Attempt blocked. Freddie Sears (Ipswich Town) right footed shot from the left side of the box is blocked. Assisted by Kayden Jackson.
Attempt missed. Leandro Bacuna (Reading) right footed shot from outside the box misses to the left. Assisted by Liam Kelly.
Attempt blocked. Yakou Meite (Reading) left footed shot from the centre of the box is blocked. Assisted by Modou Barrow.
Substitution, Ipswich Town. Kayden Jackson replaces Jordan Roberts.
Gwion Edwards (Ipswich Town) wins a free kick in the defensive half.
Foul by Leandro Bacuna (Reading).
Attempt missed. Garath McCleary (Reading) left footed shot from outside the box is high and wide to the left following a corner.
Corner,  Reading. Conceded by Jonas Knudsen.
Attempt saved. Modou Barrow (Reading) left footed shot from the right side of the six yard box is saved in the bottom right corner. Assisted by John O'Shea.
Attempt blocked. Yakou Meite (Reading) header from the centre of the box is blocked.
Attempt missed. Liam Moore (Reading) header from the centre of the box is close, but misses to the left. Assisted by Leandro Bacuna with a cross following a corner.
Corner,  Reading. Conceded by Freddie Sears.
Foul by Gwion Edwards (Ipswich Town).
Liam Kelly (Reading) wins a free kick in the defensive half.