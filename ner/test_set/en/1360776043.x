::Graham Alexander: Fleetwood Town task far from finished::
@1360776043
url: https://www.bbc.com/sport/football/21449647
Fleetwood manager Graham Alexander says his side still have a lot of work to do if they are going to achieve promotion this season.
The Cod Army currently lie fourth in League Two, just two points behind the automatic promotion places. 
"We have a goal, but it's at the end of the season. If you want to be champions and top now, you're not champions," Alexander told BBC Radio Lancashire.
"Things are never done in February - we need to keep our feet on the ground."
League football is a marathon, you can't look at the miles you've done behind but you've got to look at the wall in front of you
He added: "We try and get that message across every single day, and we've already spoken about what we can do for Saturday.
"League football is a marathon, you can't look at the miles you've done behind but you've got to look at the wall in front of you."
Fleetwood's 2-1 victory at Oxford on Tuesday night extended their unbeaten run to four league matches, despite being without injured striker Jon Parkin.
And Alexander feels that the performance of Parkin's replacement, Jean-Michel Fontaine, is a sign of the strength in depth his squad have.
"We have to have competition for places, and we have to have players ready to step into the squad at any moment and they know what we are doing," the former Preston and Burnley player said.
"They've all done that to be fair. We lost Parky on Saturday and Jean-Michel's come in and done a fantastic job for the team.
"It's great to see that because football is a squad game now."