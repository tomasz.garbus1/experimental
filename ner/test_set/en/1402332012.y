::{team:Hearts}: Survival battle not over, says board member Ian Murray MP::
@1402332012
url: https://www.bbc.com/sport/football/27768951
{team:Hearts} board member Ian Murray MP has warned that the battle to secure the club's future will not be over should they exit administration on Wednesday.
The regime of new owner Ann Budge aims to formally cut ties with administrator BDO before 19 June, when an extension to the 12-month period would be needed.
"We have a three-to-five-year plan, which is going to take a bit of effort to get through," said Murray.
"The real fight starts on Wednesday or when we get out of administration."
Budge's Bidco group provided a £2.5m "loan" that effectively saved the club after their Lithuanian owners themselves went into administration.
But Bidco will assume responsibility for around £500,000 of "football debt" once {team:Hearts} exit administration and the Foundation of Hearts community group has pledged to provide £1.4m of working capital in each of the first two years.
The foundation, which has around 8,000 pledges from fans and whose emblem will grace the side's new away kit, estimates that it will need to raise £6.3m within five years if the long-term plan of passing the club to fan ownership is to be realised.
Obviously the club's been hollowed out by the administration process and also hollowed out by the previous ownership
MP for Edinburgh South Murray, who is the foundation's representative on the new board, is confident its target can be achieved if fans continue to rally behind the club despite their relegation from the {league:Scottish Premiership}.
"The last month has been a whirlwind for Ann and her team and I just hope that the next five years is a whirlwind but in the right direction," he told BBC {stadium:Scotland}.
"It has been tortuous and a time when we've needed a great deal of patience. 
"The very future of the club was on a knife-edge for a long time and we managed to get through that process.
"What it has done has brought everyone together. It has galvanised us and the club will come out stronger because of that."
Murray said the short-term priorities were improvements to the stadium, which have already begun, season tickets sales and rebuilding the squad under the direction of director of football Craig Levein and head coach {manager:Robbie Neilson}.
"Obviously the club's been hollowed out by the administration process and also hollowed out by the previous ownership," he said.
"So Ann has had to come in and start to rebuild that. It is a huge process and will take a long time.
"The team is now hollowed out because we have lost the experienced players, so Craig and {manager:Robbie} will have to be putting a lot of effort into getting players in so we can compete in the {league:Championship} and hopefully win it next season.
"Everybody will be working very hard to win the {league:Championship} next season and get this club back into the {league:Premiership} as quickly as possible, but there's a much wider game-plan here to get the club financially stable, to get it living within its means and to make sure we never have to go through the last 12 months again."