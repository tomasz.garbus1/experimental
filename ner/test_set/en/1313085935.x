::Blackburn striker Nikola Kalinic joins Dnipro::
@1313085935
url: https://www.bbc.com/sport/football/14496906
Blackburn striker Nikola Kalinic has joined Ukrainian side Dnipro for an undisclosed fee, Rovers have confirmed.
The 23-year-old Croatia international arrived at Ewood Park from Hadjuk Split in 2009 and scored seven goals in 33 appearances in the 2009-10 campaign.
But Kalinic stuck just eight times in 21 games in last season and slipped down boss Steve Kean's pecking order.
Blackburn recently boosted their attacking options with the signing of David Goodwillie from Dundee United.
Meanwhile, Kean insists there has not been any offers for centre-back Chris Samba and he hopes that situation remains the same until the transfer window closes.

                            
                        
Tottenham and Arsenal have both been interested in the defender but with Rovers already having sold Phil Jones to Manchester United, Kean does not want to lose another centre-back.
"Chris is only a few months into a five-year contract. There have been rumours every single week but we have not had any bids at all, not one, and long may that continue." he said.
"We know it's life without Phil, and if you lose somebody at the last minute it can be very difficult.
"We hope that won't happen, we hope that's the only centre-half we lose this season and that the phone doesn't ring and that Chris is happy with us."
Kean also said that talks over a new deal with Zimbabwe striker Benjani, who is out of contract, were "not dead" yet.