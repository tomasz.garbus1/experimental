::Forest Green Rovers 1-2 Woking::
@1460830780
url: https://www.bbc.com/sport/football/36004553
Forest Green's slim hopes of automatic promotion from the National League were ended after a shock defeat by Woking.
Rovers took the lead early on when Kieffer Moore fired Sam Wedgbury's cross in from close range.
Danny Carr's penalty drew the visitors level after the break after midfielder Bruno Andrade had been fouled by David Pipe inside the box.
And Cameron Norman powered a shot past goalkeeper Steve Arnold in injury time to secure the win for Woking.
Rovers are second in the table, now nine points behind local rivals Cheltenham Town with two games to play, while Woking climbed to 14th place.
Forest Green boss Ady Pennock told BBC Radio Gloucestershire:

                            
                        
"I'm extremely disappointed - we should have won the game by five or six.
"Especially in the first half, we should have been three or four-nil up - it's just being ruthless in front of goal.
"That's what let us down and then to concede two poor goals that just sums up the game, the game should have been dead and buried at half time in my opinion."
Woking boss Garry Hill told BBC Surrey:

                            
                        
"We rode our luck a little bit and I was very pleased to be one-nil down at half-time and there is no doubt about that. 
"We shown a lot of character and I think we tried to play football the right way in the first half. I felt we bossed it in the second half and we were certainly the better side.
"It's nice to see one of the young lads Cameron Norman get the goal and we've been waiting a long time not only as a management team but also as supporters. 
"They come up and come down to be here and everywhere to support us and I am so pleased for them."
Match ends, Forest Green Rovers 1, Woking 2.
Second Half ends, Forest Green Rovers 1, Woking 2.
Hand ball by Keiran Murtagh (Woking).
Matt Robinson (Woking) is shown the yellow card.
Goal!  Forest Green Rovers 1, Woking 2. Cameron Norman (Woking) right footed shot from the centre of the box to the bottom left corner.
Corner,  Woking.
Kieffer Moore (Forest Green Rovers) hits the bar with a header from the centre of the box.
Attempt saved. Kieffer Moore (Forest Green Rovers) header from the left side of the six yard box is saved. Assisted by Anthony Jeffrey with a cross.
Foul by Cameron Norman (Woking).
Kieffer Moore (Forest Green Rovers) wins a free kick.
Substitution, Woking. Giuseppe Sole replaces Danny Carr.
Substitution, Forest Green Rovers. Marcus Kelly replaces Darren Carter.
Foul by Keiran Murtagh (Woking).
Brett Williams (Forest Green Rovers) wins a free kick.
Corner,  Forest Green Rovers.
Corner,  Forest Green Rovers.
Attempt missed. Darren Carter (Forest Green Rovers) header from the right side of the box misses to the right. Assisted by Elliott Frear.
Corner,  Forest Green Rovers.
Attempt saved. Elliott Frear (Forest Green Rovers) right footed shot from the centre of the box is saved.
Foul by Cameron Norman (Woking).
David Pipe (Forest Green Rovers) wins a free kick.
Corner,  Woking.
Corner,  Woking.
Corner,  Forest Green Rovers.
Substitution, Forest Green Rovers. Anthony Jeffrey replaces Keanu Marsh-Brown.
Substitution, Forest Green Rovers. Brett Williams replaces Kurtis Guthrie.
Corner,  Forest Green Rovers.
Corner,  Forest Green Rovers.
Goal!  Forest Green Rovers 1, Woking 1. Danny Carr (Woking) converts the penalty with a right footed shot to the bottom left corner.
David Pipe (Forest Green Rovers) is shown the yellow card for a bad foul.
Penalty conceded by David Pipe (Forest Green Rovers) after a foul in the penalty area.
Penalty Woking. Bruno Andrade draws a foul in the penalty area.
Attempt missed. James Jennings (Forest Green Rovers) left footed shot from outside the box misses to the right.
Darren Carter (Forest Green Rovers) is shown the yellow card for a bad foul.
Foul by Kieffer Moore (Forest Green Rovers).
Cameron Norman (Woking) wins a free kick.
Attempt missed. Kieffer Moore (Forest Green Rovers) header from the centre of the box misses to the left. Assisted by James Jennings with a cross.
Foul by Jake Caprice (Woking).
Elliott Frear (Forest Green Rovers) wins a free kick.
Attempt saved. Bruno Andrade (Woking) right footed shot from outside the box is saved.