::{team:Milton Keynes Dons} 1-3 {team:Bury}::
@1475011670
url: https://www.bbc.com/sport/football/37414682
{player:James Vaughan} bagged a brilliant brace as {team:Bury} continued their magnificent winning run with victory against {team:MK Dons}.
{player:Vaughan} scored twice, either side of a {player:Ben Reeves} strike, before {player:Zeli Ismail} added a third to ensure {team:the Shakers} remain second in the table following their fifth victory in a row.
{team:Bury} took the lead against the run of play through a {player:Vaughan} volley in the eighth minute following a mistake at the back by {player:Paul Downing}.
{team:MK Dons} dominated the first half and only a resilient {player:Ben Williams} kept them at bay. {player:Reeves} and {player:Downing} were both denied by spectacular {player:Williams} saves, before {player:Nicky Maynard} had his curling effort pushed around the post by the keeper.
Substitute {player:Samir Carruthers} crashed a shot against the post two minutes into the second half before {player:Downing} cannoned a header back off the upright on the 57th minute.
{player:Reeves} equalised deservedly from close range on the 64th minute but four minutes later {player:Vaughan} restored {team:Bury}'s lead with a clinical finish.
And {team:the Shakers} wrapped up the win when {player:Ismail} ran the ball into an empty net five minutes into added time as {player:the Dons} threw caution to the wind.
Report supplied by Press Association.
Match ends, {team:MK Dons} 1, {team:Bury} 3.
Second Half ends, {team:MK Dons} 1, {team:Bury} 3.
Goal!  {team:MK Dons} 1, {team:Bury} 3. {player:Zeli Ismail} ({team:Bury}) left footed shot from very close range to the centre of the goal. Assisted by {player:Jacob Mellis} following a fast break.
Corner,  {team:MK Dons}. Conceded by {player:Neil Danns}.
Foul by {player:Ben Reeves} ({team:MK Dons}).
{player:Tom Soares} ({team:Bury}) wins a free kick in the attacking half.
Corner,  {team:MK Dons}. Conceded by {player:Hallam Hope}.
Attempt blocked. {player:Paul Downing} ({team:MK Dons}) right footed shot from outside the box is blocked.
{player:George Baldock} ({team:MK Dons}) wins a free kick in the defensive half.
Foul by {player:Hallam Hope} ({team:Bury}).
{player:Kean Bryan} ({team:Bury}) is shown the yellow card for a bad foul.
{player:Ben Reeves} ({team:MK Dons}) wins a free kick in the defensive half.
Foul by {player:Kean Bryan} ({team:Bury}).
{player:Darren Potter} ({team:MK Dons}) wins a free kick on the left wing.
Foul by {player:Hallam Hope} ({team:Bury}).
{player:Darren Potter} ({team:MK Dons}) is shown the yellow card for a bad foul.
{player:Jacob Mellis} ({team:Bury}) wins a free kick in the attacking half.
Foul by {player:Darren Potter} ({team:MK Dons}).
Attempt missed. {player:Zeli Ismail} ({team:Bury}) left footed shot from the right side of the box is close, but misses to the left.
{player:Jacob Mellis} ({team:Bury}) is shown the yellow card for a bad foul.
{player:Samir Carruthers} ({team:MK Dons}) wins a free kick in the defensive half.
Foul by {player:Jacob Mellis} ({team:Bury}).
{player:Ryan Colclough} ({team:MK Dons}) is shown the yellow card for a bad foul.
Foul by {player:Ryan Colclough} ({team:MK Dons}).
{player:Zeli Ismail} ({team:Bury}) wins a free kick in the defensive half.
Corner,  {team:MK Dons}. Conceded by {player:Antony Kay}.
{player:Zeli Ismail} ({team:Bury}) is shown the yellow card for a bad foul.
{player:Ryan Colclough} ({team:MK Dons}) wins a free kick in the defensive half.
Foul by {player:Zeli Ismail} ({team:Bury}).
Attempt missed. {player:Ryan Colclough} ({team:MK Dons}) right footed shot from outside the box is just a bit too high from a direct free kick.
{player:Ryan Colclough} ({team:MK Dons}) wins a free kick in the attacking half.
Foul by {player:Zeli Ismail} ({team:Bury}).
Substitution, {team:MK Dons}. {player:Kabongo Tshimanga} replaces {player:Nicky Maynard}.
Attempt missed. {player:James Vaughan} ({team:Bury}) right footed shot from outside the box is close, but misses to the left.
Foul by {player:Darren Potter} ({team:MK Dons}).
{player:Tom Soares} ({team:Bury}) wins a free kick in the attacking half.
Goal!  {team:MK Dons} 1, {team:Bury} 2. {player:James Vaughan} ({team:Bury}) left footed shot from the centre of the box to the bottom left corner. Assisted by {player:Jacob Mellis}.
{player:Dean Lewington} ({team:MK Dons}) wins a free kick in the attacking half.
Foul by {player:James Vaughan} ({team:Bury}).
Foul by {player:Samir Carruthers} ({team:MK Dons}).