::Bayern Munich: Jupp Heynckes hopeful despite duo's absence::
@1509394225
url: https://www.bbc.com/sport/football/41811239
Manager Jupp Heynckes says he will find an answer to Bayern Munich's forward dilemma for Tuesday's Champions League meeting with Celtic.
Bayern are without unfit Robert Lewandowski and Thomas Muller as well as Kwasi Okyere Wriedt, who is not registered for the Champions League.
Wingers Kingsley Coman and Arjen Robben are available and young forward Manuel Wintzheimer is also in the squad.
"There might be a possibility of playing 4-4-2," said Heynckes.
"But we will make that decision tomorrow.
"Obviously Muller and Lewandowski being injured was unexpected but I will find a solution.
"Every team faces difficulties with injuries, many other teams in the Champions League experience the same but we find solutions.
"While Arturo [Vidal] is very fast and very good up front I have a different plan for tomorrow.
"I have not worked often with Wintzheimer, I don't know him well enough and, as I said before, there might be some other solutions."
Bayern chief executive Karl-Heinz Rummenigge had earlier said of Lewandowski's omission on the club's website: "If we had played against Real Madrid, he might have gone.
"The coach does not want to risk anything as Saturday's game [away to Borussia Dortmund], which is very important for us, is in the back of his mind."
When asked about those comments, Heynckes said: "Rummenigge was a world-class footballer and professional and he has his own opinions.
"But in my career I have always protected my team and with Lewandowski not being 100% fit it would make no sense to make him play tomorrow.
"Obviously we have to take precautions. Lewandowski would still be receiving therapy and tomorrow would be far too early for him to play but next week it might be different."