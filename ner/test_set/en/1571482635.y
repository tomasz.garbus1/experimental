::{team:Motherwell} v {team:Aberdeen}::
@1571482635
url: https://www.bbc.com/sport/football/50024358
Goal!  {team:Motherwell} 0, {team:Aberdeen} 2. {player:Niall McGinn} ({team:Aberdeen}) right footed shot from the centre of the box to the top left corner.
{player:Barry Maguire} ({team:Motherwell}) wins a free kick on the right wing.
Foul by {player:Sam Cosgrove} ({team:Aberdeen}).
Attempt missed. {player:Sherwin Seedorf} ({team:Motherwell}) right footed shot from outside the box misses to the right.
Second Half begins {team:Motherwell} 0, {team:Aberdeen} 1.
First Half ends, {team:Motherwell} 0, {team:Aberdeen} 1.
Foul by {player:Sherwin Seedorf} ({team:Motherwell}).
{player:Shaleum Logan} ({team:Aberdeen}) wins a free kick on the left wing.
{player:Barry Maguire} ({team:Motherwell}) wins a free kick on the right wing.
Foul by {player:Sam Cosgrove} ({team:Aberdeen}).
Corner,  {team:Aberdeen}. Conceded by {player:Richard Tait}.
{player:Chris Long} ({team:Motherwell}) wins a free kick in the attacking half.
Foul by {player:Scott McKenna} ({team:Aberdeen}).
Foul by {player:Chris Long} ({team:Motherwell}).
{player:Shaleum Logan} ({team:Aberdeen}) wins a free kick on the left wing.
Attempt missed. {player:Liam Polworth} ({team:Motherwell}) right footed shot from outside the box is just a bit too high from a direct free kick.
{player:Sherwin Seedorf} ({team:Motherwell}) wins a free kick in the defensive half.
Foul by {player:Shaleum Logan} ({team:Aberdeen}).
Attempt blocked. {player:Chris Long} ({team:Motherwell}) right footed shot from outside the box is blocked.
{player:Chris Long} ({team:Motherwell}) wins a free kick in the defensive half.
Foul by {player:Michael Devlin} ({team:Aberdeen}).
{player:Chris Long} ({team:Motherwell}) hits the right post with a right footed shot from a difficult angle on the right.
Attempt missed. Jon Gallagher ({team:Aberdeen}) header from the centre of the box is just a bit too high.
Substitution, {team:Motherwell}. Bevis Mugabi replaces {player:Peter Hartley} because of an injury.
Goal!  {team:Motherwell} 0, {team:Aberdeen} 1. {player:Sam Cosgrove} ({team:Aberdeen}) right footed shot from the centre of the box to the high centre of the goal. Assisted by {player:Greg Leigh}.
Attempt blocked. {player:James Wilson} ({team:Aberdeen}) left footed shot from the centre of the box is blocked.
{player:Sam Cosgrove} ({team:Aberdeen}) wins a free kick on the left wing.
Foul by {player:Declan Gallagher} ({team:Motherwell}).
Attempt saved. {player:Sherwin Seedorf} ({team:Motherwell}) right footed shot from outside the box is saved in the centre of the goal.
Foul by {player:Niall McGinn} ({team:Aberdeen}).
{player:Liam Grimshaw} ({team:Motherwell}) wins a free kick in the defensive half.
Corner,  {team:Motherwell}. Conceded by {player:Joe Lewis}.
Penalty saved! {player:James Scott} ({team:Motherwell}) fails to capitalise on this great opportunity,  right footed shot saved  in the top left corner.
Penalty conceded by {player:Joe Lewis} ({team:Aberdeen}) after a foul in the penalty area.
Penalty {team:Motherwell}. {player:Chris Long} draws a foul in the penalty area.
Attempt saved. {player:James Scott} ({team:Motherwell}) left footed shot from outside the box is saved in the centre of the goal.
Foul by {player:Chris Long} ({team:Motherwell}).
{player:Scott McKenna} ({team:Aberdeen}) wins a free kick in the attacking half.
Attempt saved. {player:Sam Cosgrove} ({team:Aberdeen}) left footed shot from outside the box is saved in the centre of the goal.
{player:Sam Cosgrove} ({team:Aberdeen}) wins a free kick in the defensive half.