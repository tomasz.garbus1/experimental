::Barcelona 2-1 Celtic::
@1351024811
url: https://www.bbc.com/sport/football/19958293
Celtic produced a performance full of heart and tactical discipline but lost out to an injury-time Jordi Alba goal.
The visitors led after the presence of Georgios Samaras helped force Javier Mascherano to divert a Charlie Mulgrew free-kick into his own net.
Barcelona were not at their best, but produced a moment of sheer class to allow Andres Iniesta to equalise.
Celtic stood firm in the face of relentless pressure until the dying seconds when Alba stole in to score.
It was incredibly cruel on Celtic, who were outstanding to a man.
They remain second in Champions League Group G but now trail Barcelona, who maintain their 100% record, by five points. Spartak Moscow, who beat Benfica earlier in the day, are a point behind Neil Lennon's side with the Portuguese side bottom with a single point.
It had looked ominous for the visitors as early as the second minute when Barcelona sliced through their defence for the first time, Iniesta providing the killer ball, only for Alexis Sanchez to dink the ball wide of Fraser Forster's right-hand post.
That apart though, Celtic began the match comfortably, coping well with the constant passing and movement of the Catalan side.
Forster's first major test came 17 minutes in, and he dealt with it brilliantly.
Lionel Messi floated the ball over the Celtic defence, Iniesta turned it across goal where Marc Bartra flashed a header on target, only for the giant Celtic keeper to push it away.
And moments later, Celtic shocked the hosts by moving in front.
A Mulgrew free-kick from the right was attacked by Samaras, but it was Mascherano who inadvertently knocked the ball past a static Victor Valdes.
Barca had dominated possession, but Celtic's organisation was superb and there was almost half an hour gone before the home side fashioned another decent chance.
Again Iniesta was the source, but his chip was headed wide by Bartra.
Then, as the Barcelona pressure grew in intensity, Kelvin Wilson threw himself in the way of a Xavi drive to divert it over the bar and Adriano Correia tested Forster at his near post from the resulting corner.
Messi, by his own high standards, was having a quiet game but worried Forster with a couple of free-kicks that curled inches off target.
Celtic were dealt an enormous blow just before the break as Samaras was forced off, having rolled his ankle as he was fouled contesting a high ball. 
And his replacement James Forrest had not touched the ball before Barca drew level in exquisite fashion.
Messi found Iniesta just inside the box and a lightning-quick one-two with Xavi allowed Iniesta to direct the ball just out of Forster's reach and inside his left-hand post.
The timing of the equaliser was tough on Celtic, who had executed their manager's tactical instructions almost to perfection.
They nearly moved back in front seven minutes after the interval.

                            
                        
Again, the opportunity arose from a Mulgrew set-piece - this time a corner from the right - but Victor Wanyama failed to test Valdes from an excellent position.
Celtic lost the tenacity of Scott Brown with just over an hour gone, his long-term hip problem seemingly taking its toll once more.
Soon after, Forster held efforts from both Messi and then Iniesta from outside the box as Barcelona upped the ante yet further.
Midway through the second half, Alexis Sanchez fed Pedro Rodriguez on the right of the Celtic box and when his low cross found Messi on the six-yard box, a second Barca goal seemed inevitable, but Forster stuck out a hand and somehow denied the Argentine.
The Celtic keeper was performing heroically and he was swiftly off his line to deny Alexis before an unbelievable flying save kept Messi's diving header from finding the back of the net.
Celtic were becoming more and more hemmed in, but a wayward Xavi effort told the story of Barcelona's increasing frustration as time ticked away.
Barca substitute David Villa struck a post late on as it appeared Celtic would hold on, but in the fourth minute of injury time, Alba sneaked in at the back post to knock Adriano's cross past the helpless Forster from just a yard out.
Despite the bitter pang of disappointment, Celtic can be hugely proud of their side's display ahead of the return fixture against Tito Vilanova's side in Glasgow on 7 November.
Live text commentary