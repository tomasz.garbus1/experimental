::{manager:Rafael Benitez} should expect more anger from {team:Chelsea} fans - {player:Sinclair}::
@1362068430
url: https://www.bbc.com/sport/football/21619807
{manager:Rafael Benitez} has been warned that {team:Chelsea} supporters have taken his outburst personally.
{manager:The Spaniard} criticised some of the club's fans following Wednesday's 2-0 {league:FA Cup} fifth round win at {team:Middlesbrough}.
Former {team:Chelsea} defender {player:Frank Sinclair} said that {manager:Benitez} can expect an even more hostile reception than usual when {team:West Bromwich Albion} visit on Saturday.
"I don't fancy being him at the weekend," {player:Sinclair} told Radio 5 live.

                            
                        
"I can't see it having any positive effect. I can't understand it. All the fans will have taken it personally.
"He seems like a man at the end of his tether, maybe this could be an easy way out of it."
{manager:Benitez} also said he will leave {team:Chelsea} in May and described the decision to give him the title "interim manager" as a "massive mistake". 
But {player:Nigel Spackman}, another former {team:Chelsea} player, believes the club's large turnover of managers under owner Roman Abramovich could deter some high-profile managers from applying when {manager:Benitez} does leave.
{manager:Benitez} is the ninth man to take charge of the club since {manager:Claudio Ranieri} was sacked in May 2004.
"Who will take the job when so many managers have come and gone in the last 10 years?" he told BBC Sport.
21 November 2012-present: {manager:Rafael Benitez} (interim)
March 2012-November 2012: {manager:Roberto Di Matteo}
June 2011-March 2012: {manager:Andre Villas-Boas}
June 2009-May 2011: {manager:Carlo Ancelotti}: 
February 2009-May 2009: {manager:Guus Hiddink} (appointed on a short-term contract)
9 February 2009-16 February 2009 Ray Wilkins (caretaker)
June 2008-February 2009: {manager:Felipe Scolari}
September 2007-May 2008: {manager:Avram Grant}
June 2004-September 2007: J{manager:ose Mourinho}
September 2000-May 2004: {manager:Claudio Ranieri} 
"Where do {team:Chelsea} go from here? That's the bigger question. If they get rid of {manager:Rafa}, what will happen?"
On {manager:Benitez}'s outburst, {player:Spackman} added: "The pressure he has been under since the first day he set foot in {stadium:Stamford Bridge} has come to a boiling point.
"I think it's been very venomous on him, over the top in some places, but {manager:Rafa} knew what was going to happen when he took the job in the first place."
{manager:Benitez}'s defiant post-match interview came after the former {team:Liverpool} manager was once again targeted by a section of his club's supporters. 
Asked a question about the importance of the {league:FA Cup}, {manager:Benitez} responded with an outspoken rant in which he said: 
• He would leave the club in the summer
• The "interim" title was a "massive mistake"
• He was only given the title so the club could "wash our hands" of him if he failed
• Supporters were "wasting their time" in protesting against him
• Fans' actions could cost the club a top-four place
• The only reason supporters are unhappy with him is because of his {team:Liverpool} background
{stadium:Premier League} managers have given a mixed reaction to {manager:Benitez}'s rant.
Read the full transcript of {manager:Rafa Benitez}'s astonishing interview with BBC Radio 5 live
Full transcript
{team:Fulham} boss {manager:Martin Jol} has sympathy for the 52-year-old.
"I feel for any manager who is not well-liked and he wasn't well-liked from the start," said {manager:the Dutchman}.
And {team:Newcastle} boss {manager:Alan Pardew} said {team:Chelsea}'s hierarchy might regret giving {manager:Benitez} the 'interim' title.
"The title probably didn't do him any favours," he said. "It probably didn't help {team:Chelsea}, and perhaps even upstairs, they might regret that title, if you want to call it that."
But {team:West Ham United}'s assistant manager {manager:Neil McDonald} said all fans had a right to air their grievances.
"We're all in the same situation," said {manager:McDonald}. "If we don't get the results we're under pressure and if the team's not playing as well as what it has done in the past, then the crowd have got every right to voice their opinions."
Former {team:Chelsea} {league:FA Cup} winner {manager:Gus Poyet}, the current {team:Brighton} manager, has reiterated his desire to one day manage the club.
"That is my aim, everyone knows and I don't hide it," said the former {country:Uruguay} international.
"I want to go to the highest level and if it is in the Premier League with {team:Chelsea} then fantastic. If it is with someone else, then we shall see."