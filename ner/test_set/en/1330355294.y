::{player: Mike Edwards} delighted by fresh chance at {team:Notts County}::
@1330355294
url: https://www.bbc.com/sport/football/17182583
{team:Notts County} centre-half {player: Mike Edwards} has admitted he feared his eight-year spell at {stadium:Meadow Lane} was over before {manager:Keith Curle} was appointed manager.
The 31-year-old was left out of the squad by former boss {manager:Martin Allen} and had not played since 31 December.
But although {player:Edwards} did not appear in {team:Curle}'s first game, he was recalled for Saturday's 1-0 win over {team:Chesterfield}.
"It's great to be back. I thought I had played my last game for the club," {player:Edwards} told BBC Radio Nottingham.
"I had a great reception and response from the crowd and it was as great to get another chance.
"I was looking to get things sorted for next year - that was the situation I was in. I may have been out sooner but the new gaffer has come in an everyone has a clean slate again so I'm just hoping to impress."
He added: "I did a lot of training on my own and the youth boys have helped me out because I've been training with them.
"The last two months I have been running back from the training ground, trying to keep fit and hoping to get another chance 
"It's been a long two months and it's been difficult."