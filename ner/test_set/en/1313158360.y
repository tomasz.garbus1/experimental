::{team:Motherwell} seek cover for injured {player:Steven Saunders}::
@1313158360
url: https://www.bbc.com/sport/football/14482889
{team:Motherwell} aim to make "one or two signings" to fill the gap left by injured defender {player:Steven Saunders}.
Assistant manager {manager:Kenny Black} confirmed that {player:Saunders} is likely to miss at least six months after a successful operation on his Achilles tendon.
{player:Saunders} was hurt during {country:Scotland} U21's victory over {country:Norway} on Wednesday.
"It is a blow not only for the club but for the lad himself," explained {manager:Black}. "And his loss leaves us a little bit thin on the ground."
{player:Saunders} collapsed clutching his left ankle less than four minutes into {country:Scotland}'s 3-0 win at {stadium:St Mirren Park}.
"The operation was a success," added {manager:Black}. "Our physio, John Porteous, went to see him this morning.
"I have not had the chance to speak to him, but by all accounts you're looking at a timescale of a minimum six months.
"It is really unfortunate for the lad, because last week against {team:Hearts}, which was his first involvement of the season, he looked more assured as the game wore on.
"Everything being equal, he would have been featuring this weekend against {team:St Mirren}.
There's no doubt we'll need to look at bringing in one or possibly two replacements for {player:Steven}
In addition to {player:Saunders}' absence, {team:Motherwell} are without experienced midfielder {player:Keith Lasley} through suspension.
"There's no doubt we'll need to look at bringing in one or possibly two replacements for {player:Steven} given the time he is going to be out," said {manager:Black}.
{team:Motherwell} manager {manager:Stuart McCall} watched the U21 international and spoke to {player:Saunders} in the dressing-room before the 20-year-old was taken to hospital.
"My heart goes out to him and his family - his mum and dad were at the game," he told BBC Radio {country:Scotland} after the game.
"It was innocuous, there was no-one near him. I thought he had just gone over on his ankle.
"The worst-case scenario is nine months. I know injuries are part of the game, but it was really sickening to see that."
{team:Dundee United} midfielder {player:Scott Allan} was the star for {country:Scotland} on his under-21 debut, setting up {team:Hull City} midfielder {player:Tom Cairney} for the opener, while {team:United}'s {player:Stuart Armstrong} came off the bench to score the third.
{team:Aberdeen} full-back {player:Ryan Jack} curled home the second, but most thoughts were with {player:Saunders}.
"I spoke to him in the dressing-room afterwards," added {manager:McCall}. "It will be a long haul back, but he is a great kid and he will get his head down and focus.
"I said to him, if you can get back in, hopefully you can help us get to another {league:Scottish Cup} final.
"That put a smile on his face, but it's difficult times for him at the moment."
