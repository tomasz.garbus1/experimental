::Rafael Benitez should expect more anger from Chelsea fans - Sinclair::
@1362068430
url: https://www.bbc.com/sport/football/21619807
Rafael Benitez has been warned that Chelsea supporters have taken his outburst personally.
The Spaniard criticised some of the club's fans following Wednesday's 2-0 FA Cup fifth round win at Middlesbrough.
Former Chelsea defender Frank Sinclair said that Benitez can expect an even more hostile reception than usual when West Bromwich Albion visit on Saturday.
"I don't fancy being him at the weekend," Sinclair told Radio 5 live.

                            
                        
"I can't see it having any positive effect. I can't understand it. All the fans will have taken it personally.
"He seems like a man at the end of his tether, maybe this could be an easy way out of it."
Benitez also said he will leave Chelsea in May and described the decision to give him the title "interim manager" as a "massive mistake". 
But Nigel Spackman, another former Chelsea player, believes the club's large turnover of managers under owner Roman Abramovich could deter some high-profile managers from applying when Benitez does leave.
Benitez is the ninth man to take charge of the club since Claudio Ranieri was sacked in May 2004.
"Who will take the job when so many managers have come and gone in the last 10 years?" he told BBC Sport.
21 November 2012-present: Rafael Benitez (interim)
March 2012-November 2012: Roberto Di Matteo
June 2011-March 2012: Andre Villas-Boas
June 2009-May 2011: Carlo Ancelotti: 
February 2009-May 2009: Guus Hiddink (appointed on a short-term contract)
9 February 2009-16 February 2009 Ray Wilkins (caretaker)
June 2008-February 2009: Felipe Scolari
September 2007-May 2008: Avram Grant
June 2004-September 2007: Jose Mourinho
September 2000-May 2004: Claudio Ranieri 
"Where do Chelsea go from here? That's the bigger question. If they get rid of Rafa, what will happen?"
On Benitez's outburst, Spackman added: "The pressure he has been under since the first day he set foot in Stamford Bridge has come to a boiling point.
"I think it's been very venomous on him, over the top in some places, but Rafa knew what was going to happen when he took the job in the first place."
Benitez's defiant post-match interview came after the former Liverpool manager was once again targeted by a section of his club's supporters. 
Asked a question about the importance of the FA Cup, Benitez responded with an outspoken rant in which he said: 
• He would leave the club in the summer
• The "interim" title was a "massive mistake"
• He was only given the title so the club could "wash our hands" of him if he failed
• Supporters were "wasting their time" in protesting against him
• Fans' actions could cost the club a top-four place
• The only reason supporters are unhappy with him is because of his Liverpool background
Premier League managers have given a mixed reaction to Benitez's rant.
Read the full transcript of Rafa Benitez's astonishing interview with BBC Radio 5 live
Full transcript
Fulham boss Martin Jol has sympathy for the 52-year-old.
"I feel for any manager who is not well-liked and he wasn't well-liked from the start," said the Dutchman.
And Newcastle boss Alan Pardew said Chelsea's hierarchy might regret giving Benitez the 'interim' title.
"The title probably didn't do him any favours," he said. "It probably didn't help Chelsea, and perhaps even upstairs, they might regret that title, if you want to call it that."
But West Ham United's assistant manager Neil McDonald said all fans had a right to air their grievances.
"We're all in the same situation," said McDonald. "If we don't get the results we're under pressure and if the team's not playing as well as what it has done in the past, then the crowd have got every right to voice their opinions."
Former Chelsea FA Cup winner Gus Poyet, the current Brighton manager, has reiterated his desire to one day manage the club.
"That is my aim, everyone knows and I don't hide it," said the former Uruguay international.
"I want to go to the highest level and if it is in the Premier League with Chelsea then fantastic. If it is with someone else, then we shall see."