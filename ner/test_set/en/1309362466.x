::Equatorial Guinea lose to Norway at Women's World Cup::
@1309362466
url: https://www.bbc.com/sport/football/13965984
Women's World Cup debutants Equatorial Guinea opened their Group D campaign with a narrow 1-0 defeat to Norway in Augsburg. 
Emelie Haavi scored in the 84th minute after the 1995 world champions had hit the woodwork three times.
The 2008 African champions had chances of their own - with captain Anonman missing several times. 
Equatorial Guinea's next game is against Australia on 3 July in Bochum, while Norway face Brazil in Wolfsburg.
"We could have won the game," said Equatorial Guinea coach Marcello Frigerio, before adding: "I still think we can progress." 
The 2008 African champions sit bottom of the group alongside Australia, who were also beaten 1-0 - with Brazil's Rosana proving the match-winner in that game.
But in Augsburg, in front of almost 13,000 fans, Equatorial Guinea needed some time to get into the match. 
Initially, they relied on long-range efforts, like Anonman's 40-yard blast and Jurama's attempt from a bit closer after 17 minutes.
The Africans then began to dominate and on 20 minutes, Dulcia was through on goalkeeper Ingrid Hjelmseth but Haavi made a vital last-gasp interception.
Hjelmseth faced a barrage of shots - most of them from long range - and successfully cleared them all away.
Equatorial Guinea had one final chance before the break as Anonman got past Nora Holstad Berge for a dash at Hjelmseth, but the keeper won the encounter.
After the break, Anonman continued her excellent match, breezing past four women before chipping a shot that went just wide of the post after 53 minutes.
The back-and-forth went on as Norway's Mykjaland hit the outside of the right post in the 55th minute.
The Norwegians found the framework again just two minutes later with Herlovsen hitting the left post before the ball was pushed out in a scramble.
Anonman broke through again in the 71st minute but again shot wide of the far right post and then she wasted another one-on-one situation with Hjelmseth just seconds later.
Norway finally broke through when Leni Larsen Kaurin crossed from the right side to Haavi, who shot into an empty net after the Africans failed to clear.