::{team:Kettering Town} land ex-{team:Leicester City} man {player:Aman Verma}::
@1311247174
url: https://www.bbc.com/sport/football/14235396
{team:Kettering Town} have signed midfielder {player:Aman Verma} on a one-year deal following his release from {team:Leicester City}.
The 23-year-old had loan spells at {team:Kidderminster} and {team:Darlington} last season, helping {team:the Quakers} to {league:FA Trophy} success.
He made just one appearance for {team:Harriers}, but featured 26 times in the {league:Blue Square Bet Premier} for {team:Darlington} and was offered a deal to stay there.
But he has joined their league rivals {team:Kettering} following a trial.
His contract includes the option of a further year.
{player:Verma}, who came through the {team:Leicester} youth ranks, returned to {team:the Foxes} in late 2008 after spells with {team:Bedworth} and {team:Redditch}.
He has also had loan experience with {team:Histon} and {team:Crewe}.
