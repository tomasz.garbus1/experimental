::Paul Ince: Blackpool manager set to return from stadium ban::
@1385728447
url: https://www.bbc.com/sport/football/25142012
Blackpool manager Paul Ince is excited by his return to the touchline after completing a five-match stadium ban.
Ince was punished  relating to his actions after a victory at Bournemouth in September, including one of violent conduct.
"It's been a while, but it's my own fault that it's been a while," he told BBC Radio Lancashire ahead of the visit of Sheffield Wednesday on Saturday.
"I've done my time and I just want to focus on getting back to winning ways."
19 Oct: W 1-0 v Wigan (h)
26 Oct: D 2-2 v Blackburn (h)
2 Nov: W 1-0 v Nottingham Forest (a)
9 Nov: L 2-3 v Ipswich (h)
23 Nov: D 1-1 v Birmingham (a)
The Seasiders took eight points from five matches in Ince's absence,  to the campaign, and they lie in the play-off spots ahead of their home fixture against the Owls.
"At the start of it, it seemed a long time, but it has soon come around," said the 46-year-old. "[Assistant manager] Alex Rae, [coach] Steve Thompson and the staff have done a fantastic job in just keeping us ticking over.
"There have been disappointments along the way, and you're going to get them in football whether I'm there or I'm not. The fact that I'm back - hopefully it will be great for me, great for the players and we'll see a response on Saturday."
Ince admitted he would have to learn to "bite his lip" when the  in October.
The FA document said the ex-England captain pushed fourth official Mark Pottage in the tunnel after the match and 
Despite  on 14 September, Ince had been angered by referee Oliver Langford's decision to send off full-back Jack Robinson and was later dismissed himself after  as he reacted to his side missing a late chance.
Meanwhile, Blackpool decided against a move for Wolves midfielder Jamie O'Hara before Thursday's deadline for emergency loan signings.
The 27-year-old had been 
"He's a fantastic talent," said Ince, himself a former Wolves player. "Wolves let us take a look at him, but the problem we had is that the loan window shut on Thursday. To make a decision on someone in three days' training is always going to be hard.
"If the window was shutting next week, we would probably do something. But I'm not going to start rushing into things that I'm not sure about."
On-loan full-back Bradley Orr is expected to be out of action for around eight weeks with a thigh injury. Orr, 31, has played four times for the club since 