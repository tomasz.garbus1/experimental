::Liverpool struggling for Champions League qualification - Rodgers::
@1428175207
url: https://www.bbc.com/sport/football/32185760

                            
                        
Liverpool's chances of Champions League qualification are virtually over, admits manager Brendan Rodgers.
But Louis van Gaal says his Manchester United side are still "not certain" of a top-four finish, despite moving eight points clear of fifth-placed Liverpool.
"I very much doubt we'll be in the top four come the end of the season," said Rodgers after the 4-1 loss at Arsenal.
Van Gaal, speaking after United went third with a 3-1 win against Aston Villa, added: "It is still a rat race."
Second-placed Arsenal are a point ahead of United, with Manchester City - who travel to Crystal Palace on Monday - a further point back.
Liverpool hold a one-point advantage over nearest rivals Southampton, who lost 1-0 at Everton, and Tottenham - who visit Burnley on Sunday.

                            
                        
Liverpool, aiming to secure Champions League football for a second successive season, saw their ambitions damaged as they were torn apart defensively at Emirates Stadium, with the Gunners scoring three goals in the last eight minutes of the first half to take control.
Skipper Jordan Henderson pulled one back from the penalty spot before Olivier Giroud reasserted Arsenal's three-goal advantage in the closing seconds.
And the heavy defeat has left Rodgers, whose side play Blackburn in their FA Cup quarter-final replay this week, resigned to the prospect of missing out on the Champions League.
"There's too much ground to make up - that's the realistic view from me," he said.
"Now, we must really focus on the FA Cup and that has got to be very, very important for us."
Manchester United won 2-1 at Anfield before the international break and further stretched the gap on their arch-rivals by seeing off relegation-threatened Villa.

                            
                        
Spanish midfielder Ander Herrera's brace, plus a spectacular strike from Wayne Rooney, puts United on course for a return to European competition.
The three-time European champions missed out on continental competition for the first time in 25 years during David Moyes's only season at the helm.
"We are still not certain about Champions League qualification - it is still a rat race," said Van Gaal, who replaced the Scot at the start of the season.
"We have given a blow to Liverpool and Southampton also lost. It is a big gap. Now we have to wait to see if the gap is too big for Tottenham too. 
"But we have to play the other top three teams - Chelsea, Arsenal and Manchester City - before the end of the season."
Meanwhile, Southampton saw their own hopes of a top-four finish dented by a 1-0 defeat at Everton on Saturday.
"If we play like we did against Everton and show the same ambition then we will have a good chance," said Saints boss Ronald Koeman.
"European football for the club would be a great success if we reach that. If we continue with this performance level we'll maybe finish in a good position."