::Lucas Leiva: Liverpool midfielder faces three months out::
@1346249681
url: https://www.bbc.com/sport/football/19414058
Liverpool midfielder Lucas Leiva faces another injury lay-off, this time with a thigh injury.
The Brazilian, who missed most of last season with a knee injury, suffered the problem in the opening minutes of Sunday's draw with Manchester City.
Reds boss Brendan Rodgers confirmed the news, admitting the 25-year-old faced "two to three months" out.
Lucas said: "[There are] No words to describe my feelings. It's hard to be positive but better days will come."
Lucas, who joined Liverpool from Gremio in 2007, damaged his anterior cruciate ligament in December last year.
After undergoing successful surgery, he missed the remainder of the season before returning for pre-season training last month.
He returned to competitive action in the first-leg of Liverpool's Europa League qualifier with Gomel and was named in Rodgers's first two Premier League starting line-ups.
Fellow midfielder, on-loan signing Nuri Sahin, is available to make his debut against Arsenal on Sunday at Anfield.