::Shaun Whalley and Danny Fitzsimons leave Luton Town::
@1431966435
url: https://www.bbc.com/sport/football/32788157
Winger Shaun Whalley and defender Danny Fitzsimons have left Luton Town by mutual consent. 
Fitzsimons, 23, did not make an appearance for the League Two side, having joined from Histon in June 2013 after suffering a cruciate knee injury.
Whalley scored four goals in 35 appearances over two seasons.
The Hatters have also announced they have terminated Ricky Miller's contract following an internal investigation into a breach of club discipline.