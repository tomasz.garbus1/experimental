::Stenhousemuir v Stranraer::
@1541252731
url: https://www.bbc.com/sport/football/46000753
Match ends, Stenhousemuir 0, Stranraer 2.
Second Half ends, Stenhousemuir 0, Stranraer 2.
Colin McMenamin (Stenhousemuir) is shown the yellow card.
Attempt missed. Ian Smith (Stranraer) right footed shot from the centre of the box is too high.
Foul by Paul Crossan (Stranraer).
Morgyn Neill (Stenhousemuir) wins a free kick on the left wing.
Attempt missed. Daniel Higgins (Stranraer) left footed shot from outside the box misses to the left.
Goal!  Stenhousemuir 0, Stranraer 2. Paul Crossan (Stranraer) right footed shot from the centre of the box to the bottom right corner.
Attempt missed. Innes Cameron (Stranraer) right footed shot from a difficult angle on the right is too high following a corner.
Corner,  Stranraer. Conceded by Morgyn Neill.
Corner,  Stranraer. Conceded by Kevin O'Hara.
Substitution, Stenhousemuir. Bobby Vaughan replaces Harrison Paton.
Substitution, Stenhousemuir. Colin McMenamin replaces Conner Duthie.
Substitution, Stenhousemuir. Sebastian Ross replaces Mark Ferry.
Substitution, Stranraer. Ian Smith replaces Luke Donnelly.
Mark Lamont (Stranraer) wins a free kick in the defensive half.
Foul by Conor McBrearty (Stenhousemuir).
Mark Lamont (Stranraer) wins a free kick in the defensive half.
Foul by Sean Dickson (Stenhousemuir).
Graeme Smith (Stenhousemuir) is shown the yellow card.
Daniel Higgins (Stranraer) is shown the yellow card for a bad foul.
Foul by Daniel Higgins (Stranraer).
Harrison Paton (Stenhousemuir) wins a free kick in the attacking half.
Corner,  Stranraer. Conceded by Graeme Smith.
Substitution, Stranraer. Mark Lamont replaces David Brownlie.
Substitution, Stranraer. Innes Cameron replaces Grant Anderson because of an injury.
Delay in match Grant Anderson (Stranraer) because of an injury.
Alan Cook (Stenhousemuir) is shown the red card.
Foul by Alan Cook (Stenhousemuir).
Grant Anderson (Stranraer) wins a free kick in the attacking half.
Mark McGuigan (Stenhousemuir) wins a free kick in the defensive half.
Foul by David Brownlie (Stranraer).
Foul by Mark Ferry (Stenhousemuir).
Grant Anderson (Stranraer) wins a free kick in the defensive half.
Attempt saved. Harrison Paton (Stenhousemuir) right footed shot from outside the box is saved in the bottom left corner.
Harrison Paton (Stenhousemuir) wins a free kick in the attacking half.
Foul by Kyle Turner (Stranraer).
Corner,  Stenhousemuir. Conceded by Jamie Hamill.
Corner,  Stenhousemuir. Conceded by Kyle Turner.
Corner,  Stenhousemuir. Conceded by Andrew McDonald.