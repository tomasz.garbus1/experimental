::Gillingham: Michael Richardson and Ryan Williams join on loan::
@1360864778
url: https://www.bbc.com/sport/football/21460565
League Two side Gillingham have completed the loan signings of Newcastle midfielder Martin Richardson and Fulham winger Ryan Williams.
Both Richardson, 20, and Williams, 19, make the move to Priestfield on initial one-month deals.
Richardson has not featured for the Magpies but played four games while on loan at Leyton Orient last season.
Williams joined Fulham from Portsmouth in January last year but has not made a first-team appearance.
The Australian came through the academy at Fratton Park and made six appearances for Pompey last season.
Gills boss Martin Allen said: "Ryan is positive, can open up defences, is fast, direct and is a good player."
Ryan is the younger brother of Middlesbrough defender Rhys, while his twin Aryn is on the books at Burnley.