::2016 {league:FA Cup} final: Mark Clattenburg to take charge at {stadium:Wembley}::
@1461840479
url: https://www.bbc.com/sport/football/36159237
Mark Clattenburg will referee the {league:FA Cup} final between {team:Manchester United} and {team:Crystal Palace} at {stadium:Wembley} on 21 May. 
The 41-year-old from County Durham was in charge of the 2012 {league:League Cup} final at {stadium:Wembley} when {team:Liverpool} won against {team:Cardiff City} on penalties.
Clattenburg also took charge of the 2012 Olympic men's final when {country:Mexico} beat favourites {country:Brazil} at {stadium:Wembley}.
"It's an honour and a massive achievement," said Clattenburg, who will take charge of games at {league:Euro 2016}.
"These days you get told what games you're doing by text, but when you see a call coming in from the FA referees' department you know it's important."
{team:United}, winners 11 times, have not won the {league:FA Cup} since 2004 while {team:Palace} are seeking a first triumph.
Clattenburg refereed Wednesday's {league:Champions League} semi-final first leg between {team:Atletico Madrid} and {team:Bayern Munich}, which the Spanish hosts won 1-0.