::Brighton & Hove Albion 1-0 Doncaster Rovers::
@1391883034
url: https://www.bbc.com/sport/football/25994170
Leonardo Ulloa scored the winner as Brighton kept up their play-off push with victory over struggling Doncaster.
Billy Sharp missed Rovers' best chance when he headed wide from six yards 10 minutes after the break.
And Ulloa punished that miss on 75 minutes as he met Stephen Ward's cross from the left with a firm header to break the visitors' resistance.

                            
                        
Striker Sharp compounded his side's misery when he was sent off late on after tangling with Gordon Greer.
The result left the Seagulls four points from the play-off positions as they extended their unbeaten run at home to six matches.
Doncaster, who are five points above the relegation zone, proved a stubborn obstacle but remain without an away win in the league since September.

                            
                        
Argentine Ulloa gave warning of his aerial threat when he headed a Ward cross wide from 10 yards.
Sharp then went close to breaking the deadlock for the away side, only to nod wide from James Coppinger's cross.
Ulloa had a 20-yard curling strike kept out by keeper Sam Johnstone and Solly March's close-range effort was blocked by James Husband as Brighton pressed.
And they eventually broke through when Ulloa converted from Ward's cross, with Sharp sent off four minutes from time after clashing with Greer.
Brighton manager Oscar Garcia:
"We started really well but when you (miss) a lot of chances you can lose your confidence. We kept playing well and creating chances and finally Leonardo Ulloa took his chance well.
"[Speculation] doesn't affect the players because they know I am with Brighton. They are not new to football. They know how it works, that managers can be sacked at any time.
"So I don't think they will spend any second of their lives thinking about it at all. The players are happy and they know we have a massive game on Tuesday."
Doncaster manager Paul Dickov:
"We feel a bit unlucky. We were outstanding today. We knew they are a top team and we almost weathered the storm and got something from the game.
"In the second half we caused them some problems. We created more clear-cut chances throughout the game, so we feel that we've been done hard by.
"We've been on a good run lately and I'd like to think anyone that saw us would think that we can compete at this level. 
"We're not stupid enough to dismiss the threat of trouble but we have a good chance of getting out of it."
Match ends, Brighton and Hove Albion 1, Doncaster Rovers 0.
Second Half ends, Brighton and Hove Albion 1, Doncaster Rovers 0.
Corner,  Brighton and Hove Albion. Conceded by James Coppinger.
Hand ball by Jonathan Obika (Brighton and Hove Albion).
Bongani Khumalo (Doncaster Rovers) is shown the yellow card for a bad foul.
Leonardo Ulloa (Brighton and Hove Albion) wins a free kick in the defensive half.
Foul by Bongani Khumalo (Doncaster Rovers).
Attempt missed. David LÃ³pez (Brighton and Hove Albion) right footed shot from outside the box is close, but misses to the right. Assisted by Kazenga Lua Lua.
Kazenga Lua Lua (Brighton and Hove Albion) wins a free kick in the attacking half.
Foul by Paul Quinn (Doncaster Rovers).
Substitution, Brighton and Hove Albion. Jonathan Obika replaces David RodrÃ­guez.
Corner,  Doncaster Rovers. Conceded by Tomasz Kuszczak.
Attempt saved. David Cotterill (Doncaster Rovers) right footed shot from outside the box is saved in the centre of the goal. Assisted by Paul Keegan.
Billy Sharp (Doncaster Rovers) is shown the red card for fighting.
Gordon Greer (Brighton and Hove Albion) wins a free kick in the defensive half.
Foul by Billy Sharp (Doncaster Rovers).
David RodrÃ­guez (Brighton and Hove Albion) wins a free kick in the attacking half.
Foul by Paul Keegan (Doncaster Rovers).
Foul by David RodrÃ­guez (Brighton and Hove Albion).
James Husband (Doncaster Rovers) wins a free kick on the left wing.
Leonardo Ulloa (Brighton and Hove Albion) is shown the yellow card for a bad foul.
Substitution, Brighton and Hove Albion. David LÃ³pez replaces Solly March.
Foul by Leonardo Ulloa (Brighton and Hove Albion).
Dean Furman (Doncaster Rovers) wins a free kick in the attacking half.
Corner,  Brighton and Hove Albion. Conceded by James Husband.
Attempt blocked. David RodrÃ­guez (Brighton and Hove Albion) right footed shot from the right side of the box is blocked. Assisted by Leonardo Ulloa.
Substitution, Doncaster Rovers. David Cotterill replaces Mark Duffy.
Goal!  Brighton and Hove Albion 1, Doncaster Rovers 0. Leonardo Ulloa (Brighton and Hove Albion) header from the centre of the box to the bottom right corner. Assisted by Stephen Ward with a cross.
Offside, Brighton and Hove Albion. Kazenga Lua Lua tries a through ball, but Leonardo Ulloa is caught offside.
Rohan Ince (Brighton and Hove Albion) wins a free kick on the right wing.
Foul by Billy Sharp (Doncaster Rovers).
Attempt saved. David RodrÃ­guez (Brighton and Hove Albion) right footed shot from a difficult angle on the right is saved in the centre of the goal. Assisted by Solly March.
Attempt missed. Mark Duffy (Doncaster Rovers) right footed shot from the left side of the box is high and wide to the right. Assisted by Chris Brown.
Attempt missed. Leonardo Ulloa (Brighton and Hove Albion) header from the centre of the box is close, but misses to the left. Assisted by David RodrÃ­guez with a cross.
Bruno (Brighton and Hove Albion) wins a free kick on the right wing.
Foul by Billy Sharp (Doncaster Rovers).
Attempt saved. Leonardo Ulloa (Brighton and Hove Albion) right footed shot from the centre of the box is saved in the centre of the goal. Assisted by Matthew Upson.
Corner,  Brighton and Hove Albion. Conceded by James Husband.
Attempt missed. Kazenga Lua Lua (Brighton and Hove Albion) right footed shot from outside the box is too high.
James Husband (Doncaster Rovers) is shown the yellow card.