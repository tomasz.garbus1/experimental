::{country:Northern Ireland} summer football season up for discussion::
@1375970188
url: https://www.bbc.com/sport/football/23620595
The company recently set up to run {league:Irish League} football is to consider moving to a summer season.
The body, called the {league:Northern Ireland Football League}, is conducting a review of the local game and plans to report  at the end of the 2013/14 season.
One of the topics up for debate is the option of playing during the summer.
"Everything is up for discussion and we are going to look seriously at summer football," said Peter Dornan, an independent director of the {league:NIFL}.
The {country:Republic of Ireland}'s league has a 'Scandinavian-type' March to November season, unlike the {country:United Kingdom} where competition takes place from August to May.
The {league:League of Ireland}'s switch has been viewed as a successful one by most clubs in the Republic, mainly because they feel they can attract bigger attendances when the weather is better.

                            
                        
They also feel it benefits those clubs in involved in European football as they are 'in season' when {league:Champions League} and {league:Europa League} qualifiers are being staged.
"Summer football was met with resistance in the south at the start, but now it is in place," added Dornan, a lawyer and former {team:Linfield} player.
Playing more matches on Friday nights and on Sundays would also be looked it in an attempt to boost attendances.
Dornan said all parties involved in {league:Irish League} football would be consulted and that Uefa officials were providing support.
Under the {league:Northern Ireland Football League} format, all top-flight local football, apart from the {league:Irish Cup}, will be controlled by the new body.
The {league:NIFL} will operate as a limited company, independent of the IFA, but BBC sport has learned that it will be will actually be funded by the national association to the tune of approximately £450,000 per annum over a four-year period.
The new body will attempt to generate income via advertising and marketing and all member clubs will have shares in the {league:NIFL} which recently advertised for a managing director.
For now, the {league:NIFL} is being steered by  a governing board, including among others, Jack Grundy, Gerard Lawlor and Dornan.
The IFA will retain control of disciplinary matters.