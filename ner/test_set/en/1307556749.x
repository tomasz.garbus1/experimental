::Cliftonville sign ex-Ballymena goalkeeper Ryan Brown::
@1307556749
url: https://www.bbc.com/sport/football/13705050
Cliftonville have signed former Bangor, Larne and Ballymena United goalkeeper Ryan Brown.
Brown spent last season with Ballymena United and becomes manager Tommy Breslin's fifth signing of the summer.
"I'm looking forward to getting started," said former junior international keeper Brown.
"Cliftonville have shown great consistency over the last few years and have been challenging at the top end of the table."
Other Reds signing in recent days includes former Newry and Portadown defender John Convery and ex-Glentoran midfielder Peter Steele.