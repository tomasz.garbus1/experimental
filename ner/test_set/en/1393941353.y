::{league:World Cup} 100 days: {country:England}'s Group D opponents assessed::
@1393941353
url: https://www.bbc.com/sport/football/26372846
It is 100 days until the start of the {league:World Cup} - and 102 days until {country:England}'s campaign begins with a match against {country:Italy} in the Amazonian city of Manaus.
On Wednesday, {country:England} host {country:Denmark} at {stadium:Wembley} in their final game before manager {manager:Roy Hodgson} names his squad for the tournament in {country:Brazil}.
But how are {country:England}'s group opponents shaping up as the countdown continues to the start of the tournament?
By James Horncastle, European football writer
{country:England} v {country:Italy}, 14 June, 23:00 BST, Arena Amazonia, Manaus
The new year didn't start auspiciously for {country:Italy}.
On the first game back from the winter break, {team:Fiorentina} striker {player:Giuseppe Rossi} - the top scorer in {league:Serie A} - suffered yet another knee injury that threatened to rule him out of consecutive major tournaments.
The problem is not as bad as first thought and the 27-year-old should make it back in time to be on the flight to {country:Brazil}.
However, if he does not, a number of options in attack have emerged for coach {manager:Cesare Prandelli} in addition to the existing ones of {team:AC Milan}'s {player:Mario Balotelli} and {team:Southampton}'s {player:Dani Osvaldo} - currently on loan at {team:Juventus}. 
Five of the seven most prolific strikers in {league:Serie A} this season are Italian. Their names are a mix of old and new, ranging from {team:Verona}'s veteran {player:Luca Toni} to {team:Sassuolo}'s teenage sensation {team:Domenico Berardi} and the partnership at Torino of Ciro Immobile and Alessio Cerci. All are in double figures.
The rest of the team remains more or less the same as two years ago, when they beat {country:England} in a quarter-final penalty shoot-out on their way to the {league:Euro 2012} final.
{country:Italy} have a clear identity, a defined style and variety to their play.
{team:Juventus} provide the spine. {league:World Cup}-winning goalkeeper {player:Gianluigi Buffon} will captain behind the centre-backs he plays with at club level - {player:Giorgio Chiellini}, {player:Andrea Barzagli} and {player:Leonardo Bonucci}.
{team:The Old Lady} also endows {country:Italy} with her brain, {player:Andrea Pirlo} - the playmaker who was so impressive at {league:Euro 2012}. 
{manager:Prandelli} and his team have taken a lot from that tournament experience.
He now understands the value of recovery. Instead of flying back to their training base in Krakow from their semi-final in Warsaw, he regretted not going straight to the final in Kiev. His players would have been travelling less and resting more.

                            
                        
That has informed his planning for the group stages in {country:Brazil} - conditions his players were exposed to during last summer's {league:Confederations Cup} where {country:Italy} finished third after beating {country:Uruguay} on penalties. 
Playing and training in the host nation's heat and humidity has also shaped {manager:Prandelli}'s selection policy. He wants footballers who are athletes too. Not luxury players short of stamina like {player:Antonio Cassano}. 
Another decision facing {manager:Prandelli} and the Italian Football Federation regards his future. The expectation was that he would step down after the tournament. Now it looks like he might stay on.
An announcement is expected after Wednesday's friendly with Spain to put an end to any uncertainty and allow {country:Italy} to go to {country:Brazil} with peace of mind. 
The players are behind him. The spirit and belief in the squad is high. {country:Italy} know what to expect. They will be prepared. Respect for {country:England} is there as always as they prepare to meet {manager:Roy Hodgson}'s men in the opening game in Manaus. But there's no fear.
By Tim Vickery, South American football writer
{country:Uruguay} v {country:England}, 19 June, 20:00 BST, {stadium:Arena de Sao Paulo}, Sao Paulo
There are hardly any secrets about the Uruguayans.
With the exception of the odd tinker here and there, {country:Uruguay}'s squad has been virtually unchanged for years.
Coach {manager:Oscar Tabarez} has a clear idea of what his team are trying to achieve at the {league:World Cup}, as he outlined in a recent news conference.
"We know we are not among the favourites," he said. "But we also know that if we prepare well we can be a very difficult opponent to take on, and that is going to be the centre of our thoughts."
It is a phrase that reveals much about {country:Uruguay}'s tactical approach.
In order to grind out results they have to operate within their limitations, put aside notions of outplaying the opposition and instead focus on covering up their weak points.
The defensive unit and the holding midfielders have been together since the {league:Copa America} of 2007. There are some ageing limbs in there, belonging to players now too slow to be caught in open space, like {team:West Brom} defender {player:Diego Lugano}.
This, though, does not apply to the front pair. If {player:Diego Forlan} has declined to the point where he is now an option on the bench, {player:Luis Suarez} and {player:Edinson Cavani} are bang in the middle of their footballing prime.
A British audience needs no introduction to the extraordinary talent of {team:Liverpool}'s {player:Suarez}, who is likely to operate as the lone out-and-out striker, on the shoulder of the final defender.
{team:Paris St-Germain}'s {player:Cavani} is just as important, however. He is a player of remarkable unselfishness and an exceptional capacity for work. 
{player:Tabarez} describes him as "the perfect son-in-law," because he is so willing to place his ability at the service of the team.
For {country:Uruguay} he might even be described as a box-to-box centre forward - and he needs to work back in order to link the side because {country:Uruguay}'s method of play over the last few months has been to sit deep, with the defence and the midfield close together.
This both shields the lack of pace at the back, and opens up space to slip the strikers on the counter-attack.
They are happy to play that way, to wait while the game is goalless for an opponent to over-commit and then launch their strikers on the break.  
"We're party-poopers," said {player:Tabarez} last week.
"We went to South Africa and eliminated the hosts in the group phase which is something that had never happened before in the history of {league:World Cup}s.
"Then came {country:Ghana}, and we ruined that party, with all the Africans cheering on our opponents. And we went to {country:Argentina} and won the record 15th {league:Copa America}, knocking out the hosts along the way. It's how we are."
By Nefer Munoz, {country:Costa Rica}n football writer
{country:Costa Rica} v {country:England}, 24 June, 17:00 BST, {stadium:Estadio Mineirao}, Belo Horizonte
In {country:Costa Rica}, something good is referred to as pura vida, literally meaning "pure life".
Around the valleys, volcanoes and shores of this tropical Latin American country, one hears the phrase repeated in conversations like an infinite echo. If one is doing well, everything is pura vida.
And if the national football team, known as La Sele, happen to be playing well, life is definitely pura vida.
However, on 6 December 2013 few Costa Ricans could be heard discussing the pure life. That day, the {league:World Cup} draw pitched {country:Costa Rica} into a group with three former champions - {country:England}, {country:Italy} and {country:Uruguay}. 
Immediately, fans living in our pacifist, peaceful country felt like Tom Thumb preparing to fight three powerful giants. 
Cautious optimism has since replaced fans' initial gloom as {country:Costa Rica} historically enjoys being the underdog, a reputation that suits the tiny nation of fewer than five million people rather well.
The Ticos (Latin American slang for "Costa Ricans") first attended a {league:World Cup} in {country:Italy} in 1990. There they beat both {country:Scotland} and {country:Sweden}, upsets that saw them advance to the last 16 where they were well beaten by {country:Czechoslovakia}.
But what are their chances in {country:Brazil}? A good part of the team's fortune will depend on the performance of three crucial players: goalkeeper {player:Keylor Navas}, midfielder {player:Bryan Ruiz} and striker {player:Joel Campbell}.
This season {player:Navas} is excelling in {country:Spain}. The Spanish press has increasingly praised {player:Levante}'s goalkeeper for his spectacular performances. He has even been ranked as the best goalkeeper in {league:La Liga}, outdoing {team:Barcelona}'s {player:Victor Valdes} and {team:Real Madrid}'s {player:Diego Lopez}.
Compared with previous years, {player:Ruiz} has been inconsistent in a rollercoaster season that started with {league:Premier League} {team:Fulham} and now finds him at Dutch side {team:PSV Eindhoven}. But his talent is undeniable.
Costa Ricans also expect much of 21-year-old {player:Joel Campbell}, whose speed and unpredictable plays could make a real impact in {country:Brazil}. He showed his potential recently with a goal against {team:Manchester United} for {team:Olympiakos} in the first leg of their {league:Champions League} last-16 tie.
A final factor is that the "torcida", or Brazilian fans, may root for the "ticos" in the hope that the tiny team will beat their potential adversaries. 
I recently interviewed {country:Costa Rica}'s head coach, Colombian {manager:Jorge Luis Pinto}. A serious, thorough manager, he studies his rivals meticulously. I am sure he will rigorously plan each game while leaving some space for tropical spontaneity.
{manager:Pinto} aims to grace his second homeland with a pura vida effect.

                            
                        