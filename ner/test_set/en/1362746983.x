::Swansea City's Wayne Routledge revels under Michael Laudrup::
@1362746983
url: https://www.bbc.com/sport/football/21708488
Wayne Routledge says Swansea City manager Michael Laudrup has brought out the best in him this season.
"He gives everybody here the freedom to play football," said the midfielder, who has established himself as a regular during Laudrup's first season in charge at the Liberty Stadium.
"Like you see in our performances, we know our principles and what we're doing but we play with a freedom.
"That's when you see the best football come out."
Routledge has scored five goals this season, including two against Saturday's opponent West Bromwich Albion in a 3-1 home win in November.
The 28-year-old managed to find the net just once last season under Brendan Rodgers - a goal that broke his unenviable record of playing for six clubs without scoring in the Premier League.
"It's been a nice change this year to be in the goals," said the winger, whose last goal came against former club Aston Villa in January. 
"It feels I haven't scored for a while now but obviously as long as the team are doing well as a whole, generally it's good."
Before joining Swansea in 2011, Routledge's nomadic career had taken him to eight clubs in 10 years.
He feels settled in south Wales, experiencing what he describes as the most enjoyable season of his career, and intends to stay for many years to come.
"I'm enjoying football and that's when you're always going to see the best out of players," said the former England Under-21 international, who recently signed a contract extension until 2016. 
"There's been an opportunity from the start of the season for me to play. Thankfully I've been able to keep my place and keep playing reasonably good football
"So it's been going well for me, but not only for me, for the team as a whole as well."