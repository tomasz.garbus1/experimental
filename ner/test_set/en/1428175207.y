::{team:Liverpool} struggling for {league:Champions League} qualification - {manager:Rodgers}::
@1428175207
url: https://www.bbc.com/sport/football/32185760

                            
                        
{team:Liverpool}'s chances of {league:Champions League} qualification are virtually over, admits manager {manager:Brendan Rodgers}.
But {manager:Louis van Gaal} says his {team:Manchester United} side are still "not certain" of a top-four finish, despite moving eight points clear of fifth-placed {team:Liverpool}.
"I very much doubt we'll be in the top four come the end of the season," said {manager:Rodgers} after the 4-1 loss at {team:Arsenal}.
{manager:Van Gaal}, speaking after {team:United} went third with a 3-1 win against {team:Aston Villa}, added: "It is still a rat race."
Second-placed {team:Arsenal} are a point ahead of {team:United}, with {team:Manchester City} - who travel to {team:Crystal Palace} on Monday - a further point back.
{team:Liverpool} hold a one-point advantage over nearest rivals {team:Southampton}, who lost 1-0 at {team:Everton}, and {team:Tottenham} - who visit {team:Burnley} on Sunday.

                            
                        
{team:Liverpool}, aiming to secure {league:Champions League} football for a second successive season, saw their ambitions damaged as they were torn apart defensively at {stadium:Emirates Stadium}, with the {team:Gunners} scoring three goals in the last eight minutes of the first half to take control.
Skipper {player:Jordan Henderson} pulled one back from the penalty spot before {player:Olivier Giroud} reasserted {team:Arsenal}'s three-goal advantage in the closing seconds.
And the heavy defeat has left {manager:Rodgers}, whose side play {team:Blackburn} in their {league:FA Cup} quarter-final replay this week, resigned to the prospect of missing out on the {league:Champions League}.
"There's too much ground to make up - that's the realistic view from me," he said.
"Now, we must really focus on the {league:FA Cup} and that has got to be very, very important for us."
{team:Manchester United} won 2-1 at {country:Anfield} before the international break and further stretched the gap on their arch-rivals by seeing off relegation-threatened Villa.

                            
                        
Spanish midfielder {player:Ander Herrera}'s brace, plus a spectacular strike from {player:Wayne Rooney}, puts {team:United} on course for a return to European competition.
The three-time European champions missed out on continental competition for the first time in 25 years during {manager:David Moyes}'s only season at the helm.
"We are still not certain about {league:Champions League} qualification - it is still a rat race," said {manager:Van Gaal}, who replaced {team:the Scot} at the start of the season.
"We have given a blow to {team:Liverpool} and {team:Southampton} also lost. It is a big gap. Now we have to wait to see if the gap is too big for {team:Tottenham} too. 
"But we have to play the other top three teams - {team:Chelsea}, {team:Arsenal} and {team:Manchester City} - before the end of the season."
Meanwhile, {team:Southampton} saw their own hopes of a top-four finish dented by a 1-0 defeat at {team:Everton} on Saturday.
"If we play like we did against {team:Everton} and show the same ambition then we will have a good chance," said {team:Saints} boss {manager:Ronald Koeman}.
"European football for the club would be a great success if we reach that. If we continue with this performance level we'll maybe finish in a good position."