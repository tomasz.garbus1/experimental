::{team:Portsmouth} 1-0 {team:Bury}::
@1382202466
url: https://www.bbc.com/sport/football/24503402
{player:Bondz N'Gala}'s early header gave {team:Portsmouth} a narrow win over managerless {team:Bury}.
The giant centre-back rose highest to guide {player:Ricky Holmes}' corner into the net for his third goal of the season.
{team:Bury}, who sacked {player:Kevin Blackwell} on Monday, were the better side after the break, but {player:{player:Shaun Harrad}}'s header was kept out by {player:Phil Smith}'s fine save.
{player:{player:Danny Hylton}} and {player:{player:Anton Forrester}} both missed good chances for {team:Bury} before {player:{player:Jed Wallace}} hit the post for {team:Portsmouth}.
The defeat meant {team:the Shakers}, currently being led by caretaker boss {manager:Ronnie Jepson}, are without a win in eight games and sit just above the relegation zone on goal difference.
{team:Portsmouth} manager {manager:Guy Whittingham} told BBC Radio Solent:

                            
                        
"It's one of those victories that you don't want but you'll take. 
"I don't think there were many players who actually could say we had a good game on the ball, but we worked hard, we defended well and we've ultimately won the game.
"Teams often lift their game when a manager's gone, either to play for him or they're happy to see him go, and so either way there was going to be a reaction. 
"There was certainly a reaction from {team:Bury}; they were on top of us, they were lively, they were doing everything apart from scoring."
{team:Bury} caretaker manager {manager:Ronnie Jepson} told BBC Radio Manchester:
"How we didn't get something out of the game, I'll never know.
"We were on the front foot from the off and you saw the sigh of relief from their staff afterwards. {manager:Guy Whittingham} was almost a bit apologetic.
"This is a great place to come and they've got a good crowd behind them. Our boys showed fantastic spirit and it's just not dropping at this moment in time."
Match ends, {team:Portsmouth} 1, {team:Bury} 0.
Second Half ends, {team:Portsmouth} 1, {team:Bury} 0.
Attempt missed. {player:Jordan Mustoe} ({team:Bury}) right footed shot from outside the box is high and wide to the right.
Attempt missed. {player:Danny Mayor} ({team:Bury}) right footed shot from outside the box is close, but misses to the right.
{player:Jed Wallace} ({team:Portsmouth}) hits the right post with a right footed shot from the right side of the box.
Corner,  {team:Bury}. Conceded by {player:Phil Smith}.
Corner,  {team:Bury}. Conceded by {player:Joe Devera}.
Attempt missed. {player:Shaun Beeley} ({team:Bury}) right footed shot from outside the box is too high.
{player:Sonny Bradley} ({team:Portsmouth}) wins a free kick in the attacking half.
Foul by {player:Anton Forrester} ({team:Bury}).
Attempt missed. {player:Anton Forrester} ({team:Bury}) right footed shot from the right side of the box misses to the left.
Substitution, {team:Portsmouth}. {player:Andy Barcham} replaces {player:Patrick Agyemang}.
Substitution, {team:Portsmouth}. {player:Gavin Mahon} replaces {player:Therry Racon}.
Attempt missed. {player:William Edjenguele} ({team:Bury}) header from the centre of the box is too high.
{player:Johannes Ertl} ({team:Portsmouth}) wins a free kick in the defensive half.
Foul by {player:Danny Hylton} ({team:Bury}).
Substitution, {team:Bury}. {player:Anton Forrester} replaces {player:Shaun Harrad}.
Substitution, {team:Bury}. {player:Danny Mayor} replaces {player:Tommy Miller}.
{player:Andrew Proctor} ({team:Bury}) is shown the yellow card.
{player:Johannes Ertl} ({team:Portsmouth}) wins a free kick in the defensive half.
Foul by {player:Andrew Proctor} ({team:Bury}).
{player:Therry Racon} ({team:Portsmouth}) is shown the yellow card.
{player:Therry Racon} ({team:Portsmouth}) wins a free kick in the defensive half.
Foul by {player:Nathan Cameron} ({team:Bury}).
Corner,  {team:Bury}. Conceded by {player:Sonny Bradley}.
Substitution, {team:Portsmouth}. {player:Ryan Bird} replaces {player:John Marquis}.
Foul by {player:Jed Wallace} ({team:Portsmouth}).
{player:Tommy Miller} ({team:Bury}) wins a free kick on the left wing.
{player:Therry Racon} ({team:Portsmouth}) wins a free kick in the defensive half.
Foul by {player:Nathan Cameron} ({team:Bury}).
{player:John Marquis} ({team:Portsmouth}) is shown the yellow card.
Foul by {player:John Marquis} ({team:Portsmouth}).
{player:Nathan Cameron} ({team:Bury}) wins a free kick in the attacking half.
Corner,  {team:Portsmouth}. Conceded by {player:William Edjenguele}.
Attempt saved. {player:Danny Hylton} ({team:Bury}) right footed shot from the left side of the box is saved in the centre of the goal.
{player:Patrick Agyemang} ({team:Portsmouth}) wins a free kick in the defensive half.
Foul by {player:Nathan Cameron} ({team:Bury}).
{player:Joe Devera} ({team:Portsmouth}) wins a free kick in the defensive half.
Foul by {player:Chris Sedgwick} ({team:Bury}).
{player:Yassin Moutaouakil} ({team:Portsmouth}) wins a free kick in the defensive half.