::Charlie Horton: Leeds United sign goalkeeper::
@1433860241
url: https://www.bbc.com/sport/football/33067785
Championship club Leeds United have signed goalkeeper Charlie Horton on a two-year contract.
USA Under-23 international Horton joined Cardiff from League One side Peterborough in 2014, but failed to make an appearance for the club.
The 20-year-old was born in London but moved to Ohio in the United States at the age of nine.
"He has very good potential and will be a future number one," Leeds manager Uwe Rosler told the club's website.
"He has fantastic physical attributes and is a really positive character. He really wants to learn."