::Everton: Roberto Martinez needs three more signings::
@1438858166
url: https://www.bbc.com/sport/football/33800835
Everton need three new players before the transfer window closes on 1 September, says boss Roberto Martinez.
The Blues open their Premier League campaign at home to newly promoted Watford with just three signings added so far this summer.
Martinez said: "We will never bring a player in for the sake of it and to fill a position."
Everton have signed forward Gerard Deulofeu, midfielder Tom Cleverley and forward David Henen this summer.
River Plate teenager Tomas Andrade has been training with Everton's Under-21s ahead of a potential loan move but, with the forward thought of as "one for the future", Martinez is hoping to be busy in the transfer market.
"I feel we will be strong when the window closes. We will make sure we are ready for the big test," he said.
"We are looking for the right characteristics and we will work extremely hard until the deadline.
"What is clear is we need to bring in three players to get the squad in the shape we want to face the competition."
Everton, who finished 11th last season, have signed Deulofeu from Barcelona for £4.25m, midfielder Cleverley from Manchester United on a free transfer and forward Henen from Olympiakos this summer.
The Toffees have rejected at least one £20m bid from Chelsea for 21-year-old England international John Stones, with Manchester United also thought to be interested.
Martinez has previously criticised Chelsea for publicising their bid but believes the defender will have benefited from all the summer speculation.

                            
                        
"It is something you hope you learn from and get more mature," said Martinez. "These experiences are good experiences. 
"Clearly John is one of of the outstanding young talents we have in the squad and the interest is natural, you are going to get that and it has been handled in a good manner. It is a positive when you have a young talent doing well and having an important role.
"But the focus is on Watford and Stones is an Everton player."
Premier League newcomers Watford have signed 10 players this summer, having finished runners up in the Championship last season, and Martinez is expecting a tough start.
"It is not the normal newly promoted side," he said. "From the manager to the amount of players with strong experience at a high level. Straight away they will know what to do and this is a team ready to compete at the highest level. 
"I consider Watford as a very dangerous team, who will bring something very different to the campaign."
Everton's opening 10 games include matches against Chelsea, Manchester United, Manchester City and Liverpool.
"You never seem to get the perfect start," said Martinez. "I don't know if you want an easier start and then face the teams normally at the top or the other way around and see how quickly you can reach your level."