::{team:Newport} 2-2 {team:Tamworth}::
@1360430097
url: https://www.bbc.com/sport/football/21311404
{player:Adam Cunnington}'s stoppage time penalty rescued a point for {team:Tamworth} to deny {team:Newport} at {stadium:Rodney Parade}.
Debutant {player:Scott Donnelly} gave {team:Newport} the perfect start when he pounced on a loose ball from close range to give his side the lead after 26 seconds.
{player:Byron Anthony} doubled the advantage on 30 minutes when he poked home from {player:Lee Minshull}'s flick-on.
{player:Lee Hendrie} fired from the edge of the area to reduce the deficit before {player:Cunnington}'s late penalty.
{player:Cunnington} scored from the spot in the third minute of stoppage time after referee Kevin Johnson adjudged Ismail Yakubu to have handled the ball.
{team:Newport County} manager {manager:Justin Edinburgh} told BBC Radio {country:Wales}:
"The [penalty] decision's wrong. It's cost us two points. I spoke with the referee and he changed his description of the incident twice from when I confronted him on the pitch to when I've seen him in his dressing room. That tells me he doesn't know what he's seen and he's guessed.
"In terms of the performance it was a solid performance rather than spectacular, but without playing for three weeks I thought we did well. It was very unjust for us not to have taken three points today."