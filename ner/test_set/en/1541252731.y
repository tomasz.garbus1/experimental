::{team:Stenhousemuir} v {team:Stranraer}::
@1541252731
url: https://www.bbc.com/sport/football/46000753
Match ends, {team:Stenhousemuir} 0, {team:Stranraer} 2.
Second Half ends, {team:Stenhousemuir} 0, {team:Stranraer} 2.
{player:Colin McMenamin} ({team:Stenhousemuir}) is shown the yellow card.
Attempt missed. {player:Ian Smith} ({team:Stranraer}) right footed shot from the centre of the box is too high.
Foul by {player:Paul Crossan} ({team:Stranraer}).
{player:Morgyn Neill} ({team:Stenhousemuir}) wins a free kick on the left wing.
Attempt missed. {player:Daniel Higgins} ({team:Stranraer}) left footed shot from outside the box misses to the left.
Goal!  {team:Stenhousemuir} 0, {team:Stranraer} 2. {player:Paul Crossan} ({team:Stranraer}) right footed shot from the centre of the box to the bottom right corner.
Attempt missed. {player:Innes Cameron} ({team:Stranraer}) right footed shot from a difficult angle on the right is too high following a corner.
Corner,  {team:Stranraer}. Conceded by {player:Morgyn Neill}.
Corner,  {team:Stranraer}. Conceded by {player:Kevin O'Hara}.
Substitution, {team:Stenhousemuir}. {player:Bobby Vaughan} replaces {player:Harrison Paton}.
Substitution, {team:Stenhousemuir}. {player:Colin McMenamin} replaces {player:Conner Duthie}.
Substitution, {team:Stenhousemuir}. {player:Sebastian Ross} replaces {player:Mark Ferry}.
Substitution, {team:Stranraer}. {player:Ian Smith} replaces {player:Luke Donnelly}.
{player:Mark Lamont} ({team:Stranraer}) wins a free kick in the defensive half.
Foul by {player:Conor McBrearty} ({team:Stenhousemuir}).
{player:Mark Lamont} ({team:Stranraer}) wins a free kick in the defensive half.
Foul by {player:Sean Dickson} ({team:Stenhousemuir}).
{player:Graeme Smith} ({team:Stenhousemuir}) is shown the yellow card.
{player:Daniel Higgins} ({team:Stranraer}) is shown the yellow card for a bad foul.
Foul by {player:Daniel Higgins} ({team:Stranraer}).
{player:Harrison Paton} ({team:Stenhousemuir}) wins a free kick in the attacking half.
Corner,  {team:Stranraer}. Conceded by {player:Graeme Smith}.
Substitution, {team:Stranraer}. {player:Mark Lamont} replaces {player:David Brownlie}.
Substitution, {team:Stranraer}. {player:Innes Cameron} replaces {player:Grant Anderson} because of an injury.
Delay in match {player:Grant Anderson} ({team:Stranraer}) because of an injury.
{player:Alan Cook} ({team:Stenhousemuir}) is shown the red card.
Foul by {player:Alan Cook} ({team:Stenhousemuir}).
{player:Grant Anderson} ({team:Stranraer}) wins a free kick in the attacking half.
Mark McGuigan ({team:Stenhousemuir}) wins a free kick in the defensive half.
Foul by {player:David Brownlie} ({team:Stranraer}).
Foul by {player:Mark Ferry} ({team:Stenhousemuir}).
{player:Grant Anderson} ({team:Stranraer}) wins a free kick in the defensive half.
Attempt saved. {player:Harrison Paton} ({team:Stenhousemuir}) right footed shot from outside the box is saved in the bottom left corner.
{player:Harrison Paton} ({team:Stenhousemuir}) wins a free kick in the attacking half.
Foul by {player:Kyle Turner} ({team:Stranraer}).
Corner,  {team:Stenhousemuir}. Conceded by {player:Jamie Hamill}.
Corner,  {team:Stenhousemuir}. Conceded by {player:Kyle Turner}.
Corner,  {team:Stenhousemuir}. Conceded by {player:Andrew McDonald}.