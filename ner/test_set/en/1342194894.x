::Rangers: Ally McCoist vows to come back stronger::
@1342194894
url: https://www.bbc.com/sport/football/18835932
Manager Ally McCoist has vowed that Rangers "will come back stronger" as he prepares for life in Division Three.
Scottish Football League clubs voted the new company into the bottom tier following the Scottish Premier League's rejection of their application.
"Clearly, starting again from the bottom league is not ideal and makes the task of rebuilding Rangers a longer one," said McCoist.
"I fully accept the decision and thank them for allowing us into the SFL."
At Friday's SFL meeting, 29 of the 30 clubs voted to accept Rangers, but 25 were opposed to the resolution that proposed the team start in Division One.
"The SFL was placed in an impossible situation and I respect its decision," said McCoist.
"I fully supported the fans' views that starting again in Division Three maintains the sporting integrity that the SPL clubs were so keen on. 
"The SPL clubs and the SFA have made their positions clear over the last few weeks and it remains to be seen what the long-term effects of their decisions will be.
"Rangers has been severely punished for the actions of some individuals who previously ran the club and it will take time for us to recover.
"But we will come back stronger thanks to the loyalty of the fans and the commitment of everyone at Ibrox who are working tirelessly to bring stability and success back."
Rangers chief executive Charles Green, who along with McCoist and Scottish FA chief executive Stewart Regan met the SFL chairmen ahead of the Hampden vote, has no plans to challenge the decision.
"It is a matter of regret for all of us involved with Rangers that the issues surrounding the club resulted in the SFL and its members being placed in a very difficult position not of their own making," Green explained.
"We are a football club and we just want to get back to playing football. Now is the time to move on and start afresh.
"Our task to rebuild the club will take longer now, but we are committed to the job and fully believe we will bring success back to Rangers." 