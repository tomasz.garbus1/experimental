:{league::WSL Spring Series}: {team:Manchester City Women} start against {team:Chelsea Ladies}::
@1484047031
url: https://www.bbc.com/sport/football/38569237
{league:Women's Super League One} champions {team:Manchester City} will start their {league:Spring Series} campaign at home to 2016 runners-up {team:Chelsea} on Sunday, 23 April.
Newly promoted {team:Bristol City} host {team:Reading} as they get the top-flight season under way on the previous day.
Teams will play each other just once in a one-off, transitional competition before the {league:WSL}'s switch to a winter calendar for 2017-18.
For top-flight teams, the league runs over six weeks until Saturday, 3 June.
The {league:WSL 2 Spring Series} starts on 11 February with a game between {team:London Bees} and {team:Brighton}, who are making their debut after promotion, and concludes on Sunday, 21 May.
There will be no promotion or relegation at the end of the {league:Spring Series}, but trophies will be awarded to each division's winner.
"It's set to be an exciting few months for the domestic women's game with clubs competing for this one-off trophy and the Women's {country:FA Cup} final in May," said FA director of football participation and development Kelly Simmons.
"It's important that we keep building this momentum ahead of the {league:Euros} and for the return of the {league:WSL} in September."
{league:WSL 1}
Saturday, 22 April, 18:00 BST: {team:Bristol City} v {team:Reading}
Sunday, 23 April, 14:00 BST: {team:Birmingham City} v {team:Sunderland}, {team:Manchester City} v {team:Chelsea}, {team:Yeovil Town} v {team:Liverpool}
TBC (22 or 23 April depending on venue confirmation) - {team:Arsenal} v {team:Notts County}
{league:WSL 2}
Saturday, 11 February, 14:00 GMT: {team:London Bees} v {team:Brighton & Hove Albion}
Sunday, 12 February, 12:00 GMT: {team:Durham} v {team:Millwall Lionesses}
Sunday, 12 February, 14:00 GMT: {team:Aston Villa} v {team:Watford}, {team:Oxford United} v {team:Everton}, {team:Sheffield FC} v {team:Doncaster Rovers Belles}