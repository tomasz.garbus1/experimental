::{country:England} 3-0 {country:Scotland}: 'Players are still fighting for {manager:Gordon Strachan}'::
@1478907161
url: https://www.bbc.com/sport/football/37958203
{player:Leigh Griffiths} and {player:Christophe Berra} mounted a passionate defence of the {country:Scotland} manager {manager:Gordon Strachan} following the 3-0 defeat by {country:England}.
Both players started at {stadium:Wembley} but could not help provide the shock win required to breathe life into {country:Scotland}'s qualifying campaign.
However, both were adamant {manager:Strachan} should remain in charge.
"Why would you want to change manager midway through a campaign?" said {player:Griffiths}.
"No I can't understand why (there are calls for him to go). The results haven't been great but tonight we've been unlucky and the next game we need to go for it."
Asked whether he felt {manager:Strachan} still wanted the role, he replied: "Yeah of course. He's said in previous interviews he wants to continue as {country:Scotland} manager and as players we want him to stay."
That was echoed by {player:Berra} who added: "The manager's still giving it his all, on the training pitch he's working hard and today it showed the boys are still out there fighting for him. 

                            
                        
"Some boys got their chances today and I don't think they let him down." 
{player:Berra} acknowledged {manager:Strachan} was not universally popular as manager but stressed: "That's football for you. Everyone's got an opinion, some people might like him, some don't. 
"In the end it's about the opinions of the people in the hierarchy and there's nothing we can do about it. Football is a results-based game and it's down to them but he's got the full backing of the players as you saw. We gave it 110% and I don't think you could ask for much more."
And {player:Griffiths} is adamant that despite trailing second-placed {country:Slovenia} by four points, a play-off place remains achievable.
"Yeah of course we can, we've got a lot of home games coming up and we need to make them count," he continued. 
"As long as we get maximum points from them, we'll be in a good position."