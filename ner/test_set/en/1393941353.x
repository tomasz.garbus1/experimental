::World Cup 100 days: England's Group D opponents assessed::
@1393941353
url: https://www.bbc.com/sport/football/26372846
It is 100 days until the start of the World Cup - and 102 days until England's campaign begins with a match against Italy in the Amazonian city of Manaus.
On Wednesday, England host Denmark at Wembley in their final game before manager Roy Hodgson names his squad for the tournament in Brazil.
But how are England's group opponents shaping up as the countdown continues to the start of the tournament?
By James Horncastle, European football writer
England v Italy, 14 June, 23:00 BST, Arena Amazonia, Manaus
The new year didn't start auspiciously for Italy.
On the first game back from the winter break, Fiorentina striker Giuseppe Rossi - the top scorer in Serie A - suffered yet another knee injury that threatened to rule him out of consecutive major tournaments.
The problem is not as bad as first thought and the 27-year-old should make it back in time to be on the flight to Brazil.
However, if he does not, a number of options in attack have emerged for coach Cesare Prandelli in addition to the existing ones of AC Milan's Mario Balotelli and Southampton's Dani Osvaldo - currently on loan at Juventus. 
Five of the seven most prolific strikers in Serie A this season are Italian. Their names are a mix of old and new, ranging from Verona's veteran Luca Toni to Sassuolo's teenage sensation Domenico Berardi and the partnership at Torino of Ciro Immobile and Alessio Cerci. All are in double figures.
The rest of the team remains more or less the same as two years ago, when they beat England in a quarter-final penalty shoot-out on their way to the Euro 2012 final.
Italy have a clear identity, a defined style and variety to their play.
Juventus provide the spine. World Cup-winning goalkeeper Gianluigi Buffon will captain behind the centre-backs he plays with at club level - Giorgio Chiellini, Andrea Barzagli and Leonardo Bonucci.
The Old Lady also endows Italy with her brain, Andrea Pirlo - the playmaker who was so impressive at Euro 2012. 
Prandelli and his team have taken a lot from that tournament experience.
He now understands the value of recovery. Instead of flying back to their training base in Krakow from their semi-final in Warsaw, he regretted not going straight to the final in Kiev. His players would have been travelling less and resting more.

                            
                        
That has informed his planning for the group stages in Brazil - conditions his players were exposed to during last summer's Confederations Cup where Italy finished third after beating Uruguay on penalties. 
Playing and training in the host nation's heat and humidity has also shaped Prandelli's selection policy. He wants footballers who are athletes too. Not luxury players short of stamina like Antonio Cassano. 
Another decision facing Prandelli and the Italian Football Federation regards his future. The expectation was that he would step down after the tournament. Now it looks like he might stay on.
An announcement is expected after Wednesday's friendly with Spain to put an end to any uncertainty and allow Italy to go to Brazil with peace of mind. 
The players are behind him. The spirit and belief in the squad is high. Italy know what to expect. They will be prepared. Respect for England is there as always as they prepare to meet Roy Hodgson's men in the opening game in Manaus. But there's no fear.
By Tim Vickery, South American football writer
Uruguay v England, 19 June, 20:00 BST, Arena de Sao Paulo, Sao Paulo
There are hardly any secrets about the Uruguayans.
With the exception of the odd tinker here and there, Uruguay's squad has been virtually unchanged for years.
Coach Oscar Tabarez has a clear idea of what his team are trying to achieve at the World Cup, as he outlined in a recent news conference.
"We know we are not among the favourites," he said. "But we also know that if we prepare well we can be a very difficult opponent to take on, and that is going to be the centre of our thoughts."
It is a phrase that reveals much about Uruguay's tactical approach.
In order to grind out results they have to operate within their limitations, put aside notions of outplaying the opposition and instead focus on covering up their weak points.
The defensive unit and the holding midfielders have been together since the Copa America of 2007. There are some ageing limbs in there, belonging to players now too slow to be caught in open space, like West Brom defender Diego Lugano.
This, though, does not apply to the front pair. If Diego Forlan has declined to the point where he is now an option on the bench, Luis Suarez and Edinson Cavani are bang in the middle of their footballing prime.
A British audience needs no introduction to the extraordinary talent of Liverpool's Suarez, who is likely to operate as the lone out-and-out striker, on the shoulder of the final defender.
Paris St-Germain's Cavani is just as important, however. He is a player of remarkable unselfishness and an exceptional capacity for work. 
Tabarez describes him as "the perfect son-in-law," because he is so willing to place his ability at the service of the team.
For Uruguay he might even be described as a box-to-box centre forward - and he needs to work back in order to link the side because Uruguay's method of play over the last few months has been to sit deep, with the defence and the midfield close together.
This both shields the lack of pace at the back, and opens up space to slip the strikers on the counter-attack.
They are happy to play that way, to wait while the game is goalless for an opponent to over-commit and then launch their strikers on the break.  
"We're party-poopers," said Tabarez last week.
"We went to South Africa and eliminated the hosts in the group phase which is something that had never happened before in the history of World Cups.
"Then came Ghana, and we ruined that party, with all the Africans cheering on our opponents. And we went to Argentina and won the record 15th Copa America, knocking out the hosts along the way. It's how we are."
By Nefer Munoz, Costa Rican football writer
Costa Rica v England, 24 June, 17:00 BST, Estadio Mineirao, Belo Horizonte
In Costa Rica, something good is referred to as pura vida, literally meaning "pure life".
Around the valleys, volcanoes and shores of this tropical Latin American country, one hears the phrase repeated in conversations like an infinite echo. If one is doing well, everything is pura vida.
And if the national football team, known as La Sele, happen to be playing well, life is definitely pura vida.
However, on 6 December 2013 few Costa Ricans could be heard discussing the pure life. That day, the World Cup draw pitched Costa Rica into a group with three former champions - England, Italy and Uruguay. 
Immediately, fans living in our pacifist, peaceful country felt like Tom Thumb preparing to fight three powerful giants. 
Cautious optimism has since replaced fans' initial gloom as Costa Rica historically enjoys being the underdog, a reputation that suits the tiny nation of fewer than five million people rather well.
The Ticos (Latin American slang for "Costa Ricans") first attended a World Cup in Italy in 1990. There they beat both Scotland and Sweden, upsets that saw them advance to the last 16 where they were well beaten by Czechoslovakia.
But what are their chances in Brazil? A good part of the team's fortune will depend on the performance of three crucial players: goalkeeper Keylor Navas, midfielder Bryan Ruiz and striker Joel Campbell.
This season Navas is excelling in Spain. The Spanish press has increasingly praised Levante's goalkeeper for his spectacular performances. He has even been ranked as the best goalkeeper in La Liga, outdoing Barcelona's Victor Valdes and Real Madrid's Diego Lopez.
Compared with previous years, Ruiz has been inconsistent in a rollercoaster season that started with Premier League Fulham and now finds him at Dutch side PSV Eindhoven. But his talent is undeniable.
Costa Ricans also expect much of 21-year-old Joel Campbell, whose speed and unpredictable plays could make a real impact in Brazil. He showed his potential recently with a goal against Manchester United for Olympiakos in the first leg of their Champions League last-16 tie.
A final factor is that the "torcida", or Brazilian fans, may root for the "ticos" in the hope that the tiny team will beat their potential adversaries. 
I recently interviewed Costa Rica's head coach, Colombian Jorge Luis Pinto. A serious, thorough manager, he studies his rivals meticulously. I am sure he will rigorously plan each game while leaving some space for tropical spontaneity.
Pinto aims to grace his second homeland with a pura vida effect.

                            
                        