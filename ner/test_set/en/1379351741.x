::AC Milan's Kaka 'asks not to be paid' while injured::
@1379351741
url: https://www.bbc.com/sport/football/24117343
AC Milan midfielder Kaka has asked to go without pay as he recovers from a thigh injury picked up in his first game since rejoining the Italian side.
The Brazil international, 31, was substituted in the 70th minute of the Rossoneri's 2-2 draw at Torino.
He returned to Milan on a free earlier this month after leaving for Real Madrid for a world-record £56m in 2009.
"I don't want anything from Milan, except for love and support, until I'm fully fit and ready to play," he said.
"For this reason I have decided to suspend my current pay for this period of time. The only thing I ask is for the support and help to recover properly.
"It is a difficult moment but I have started to work on getting better and I hope to get back as quickly as possible."
Kaka added he had spoken "at length" with Milan vice-president Adriano Galliani and club doctors before deciding to ask not to be paid.
It is not clear whether Milan have accepted the player's offer.
Kaka, the 2007 World Player of the Year, made 268 appearances for Milan between 2003 and 2009, scoring 95 goals. He helped them win the Serie A title in 2004 and the 2007 Champions League.
In 2010, Milan defender Oguchi Onyewu agreed a new one-year deal for no salary after he had spent a year on the sidelines.
In 2004-05, Roma and Italy midfielder Damiano Tommasi played for £1,200 a month while recovering from a serious knee injury.