::{team:Everton}: {manager:Roberto Martinez} needs three more signings::
@1438858166
url: https://www.bbc.com/sport/football/33800835
{team:Everton} need three new players before the transfer window closes on 1 September, says boss {manager:Roberto Martinez}.
{team:The Blues} open their {league:Premier League} campaign at home to newly promoted {team:Watford} with just three signings added so far this summer.
{manager:Martinez} said: "We will never bring a player in for the sake of it and to fill a position."
{team:Everton} have signed forward {player:Gerard Deulofeu}, midfielder {player:Tom Cleverley} and forward {player:David Henen} this summer.
{team:River Plate} teenager {player:Tomas Andrade} has been training with {team:Everton}'s Under-21s ahead of a potential loan move but, with the forward thought of as "one for the future", {manager:Martinez} is hoping to be busy in the transfer market.
"I feel we will be strong when the window closes. We will make sure we are ready for the big test," he said.
"We are looking for the right characteristics and we will work extremely hard until the deadline.
"What is clear is we need to bring in three players to get the squad in the shape we want to face the competition."
{team:Everton}, who finished 11th last season, have signed {player:Deulofeu} from {team:Barcelona} for £4.25m, midfielder {player:Cleverley} from {team:Manchester United} on a free transfer and forward {player:Henen} from {team:Olympiakos} this summer.
{team:The Toffees} have rejected at least one £20m bid from {team:Chelsea} for 21-year-old {country:England} international {player:John Stones}, with {team:Manchester United} also thought to be interested.
{manager:Martinez} has previously criticised {team:Chelsea} for publicising their bid but believes the defender will have benefited from all the summer speculation.

                            
                        
"It is something you hope you learn from and get more mature," said {manager:Martinez}. "These experiences are good experiences. 
"Clearly {player:John} is one of of the outstanding young talents we have in the squad and the interest is natural, you are going to get that and it has been handled in a good manner. It is a positive when you have a young talent doing well and having an important role.
"But the focus is on {team:Watford} and {team:Stones} is an {team:Everton} player."
{league:Premier League} newcomers {team:Watford} have signed 10 players this summer, having finished runners up in the {league:Championship} last season, and {manager:Martinez} is expecting a tough start.
"It is not the normal newly promoted side," he said. "From the manager to the amount of players with strong experience at a high level. Straight away they will know what to do and this is a team ready to compete at the highest level. 
"I consider Watford as a very dangerous team, who will bring something very different to the campaign."
{team:Everton}'s opening 10 games include matches against {team:Chelsea}, {team:Manchester United}, {team:Manchester City} and {team:Liverpool}.
"You never seem to get the perfect start," said {manager:Martinez}. "I don't know if you want an easier start and then face the teams normally at the top or the other way around and see how quickly you can reach your level."