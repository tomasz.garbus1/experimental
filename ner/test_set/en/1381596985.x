::Gordon Strachan urges Scottish players to work harder::
@1381596985
url: https://www.bbc.com/sport/football/24508435
Scotland boss Gordon Strachan has told the nation's footballers to train harder if they want to improve.
His national team are preparing for the final 2014 World Cup qualifier against Croatia on Tuesday, but Strachan put out a rallying call to all Scots.
"We need to do something ourselves; stop feeling sorry for ourselves in Scotland," he told BBC Radio Scotland.
"Don't go to snooker halls or watch the telly. Practise longer. If you do that you will become a better player."
"Team spirit is all about performances; putting in performances and winning. It's not about going to the go-karts and zooming round there, going for a drink and talking about everyone that's not there, or golfing. That's nonsense. Winning games of football - that's what bonds people together."
Gordon Strachan
Strachan has won two of his five competitive games as Scotland boss after succeeding Craig Levein in February.
Their hopes of qualifying for Brazil in 2014 were extinguished in March but Strachan, capped 50 times for Scotland as a player, wants to end this campaign on a high - especially as they have not had a home win in Group A.
Addressing the wider issue of the quality at his disposal as he looks ahead to the European Championship qualification draw for France 2016, Strachan emphasised the importance of young Scottish players aiming to be the best they possibly can.
"Work harder, just work harder to be a better player," he told BBC Radio Scotland's Sportsound programme.
"Discipline yourself to be a better player. Do more hours with the ball to be a better player.
"We took five players from the Under 21 squad to train with us on Friday and I enjoyed working with them. It was Callum Paterson, Fraser Kerr, Andy Robertson, John Herron and Mark Beck. Andy was inspirational for the squad. I thought he was terrific."
Gordon Strachan
"Put more hours in - don't sit back and moan and groan about foreign players. 
"We must get better ourselves. If you work harder at the game, we'll get better. If the standard comes up, the ordinary foreign player won't be here.
"When we were at Coventry the Under 21s had to train at least three afternoons per week. Not train; train in the morning and work on your technique in the afternoon."
The former Celtic manager also lamented the "economics" that mean Scotland's top-flight clubs are not able to hold on to their best players.
"I was speaking to Walter [Smith] three or four years ago and I think there were at least 70 players in the [English] Championship who were at a Scottish Premiership club previous to that," Strachan said.
"And they only moved there because the wages were being quadrupled.
"Imagine if you'd kept those 70 or 80 players in Scotland because the wages were decent - the quality would be far better."