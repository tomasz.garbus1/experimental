::Motherwell seek cover for injured Steven Saunders::
@1313158360
url: https://www.bbc.com/sport/football/14482889
Motherwell aim to make "one or two signings" to fill the gap left by injured defender Steven Saunders.
Assistant manager Kenny Black confirmed that Saunders is likely to miss at least six months after a successful operation on his Achilles tendon.
Saunders was hurt during Scotland U21's victory over Norway on Wednesday.
"It is a blow not only for the club but for the lad himself," explained Black. "And his loss leaves us a little bit thin on the ground."
Saunders collapsed clutching his left ankle less than four minutes into Scotland's 3-0 win at St Mirren Park.
"The operation was a success," added Black. "Our physio, John Porteous, went to see him this morning.
"I have not had the chance to speak to him, but by all accounts you're looking at a timescale of a minimum six months.
"It is really unfortunate for the lad, because last week against Hearts, which was his first involvement of the season, he looked more assured as the game wore on.
"Everything being equal, he would have been featuring this weekend against St Mirren.
There's no doubt we'll need to look at bringing in one or possibly two replacements for Steven
In addition to Saunders' absence, Motherwell are without experienced midfielder Keith Lasley through suspension.
"There's no doubt we'll need to look at bringing in one or possibly two replacements for Steven given the time he is going to be out," said Black.
Motherwell manager Stuart McCall watched the U21 international and spoke to Saunders in the dressing-room before the 20-year-old was taken to hospital.
"My heart goes out to him and his family - his mum and dad were at the game," he told BBC Radio Scotland after the game.
"It was innocuous, there was no-one near him. I thought he had just gone over on his ankle.
"The worst-case scenario is nine months. I know injuries are part of the game, but it was really sickening to see that."
Dundee United midfielder Scott Allan was the star for Scotland on his under-21 debut, setting up Hull City midfielder Tom Cairney for the opener, while United's Stuart Armstrong came off the bench to score the third.
Aberdeen full-back Ryan Jack curled home the second, but most thoughts were with Saunders.
"I spoke to him in the dressing-room afterwards," added McCall. "It will be a long haul back, but he is a great kid and he will get his head down and focus.
"I said to him, if you can get back in, hopefully you can help us get to another Scottish Cup final.
"That put a smile on his face, but it's difficult times for him at the moment."