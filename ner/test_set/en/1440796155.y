::{league:Irish Premiership}: {team:Glenavon} 0-0 {team:Glentoran}::
@1440796155
url: https://www.bbc.com/sport/football/34089947

                            
                        
Assistant manager {manager:Paul Millar} said {team:Glenavon}'s players were gutted not to have beaten {team:Glentoran} at {stadium:Mourneview Park} on Friday night.
{team:Glenavon} had their chances, with young {player:Joel Cooper} blasting the best of them over from just a few yards out.
"It shows how far our team has come when {team:Glentoran} are coming here and playing for a point," claimed {manager:Millar}.
"They were time-wasting and putting players behind the ball and we are disappointed we did not find the net."
The draw leaves {team:Glentoran} with just one win from their opening five matches in the league while {team:Glenavon} are one point better off on seven.
{team:Glenavon} opened brightly and {player:Rhys Marshall} saw his header from a corner come off the crossbar.
Home forward {player:Eion Bradley} then saw a looping header strike the far post with {player:Hogg} stranded.
{team:Glentroan} trio {player:Barry Holland}, {player:Calum Birney} and {player:Johnny Addis} were booked for fouls in the first half.
In the second half {player:Conor McMenamin} and {player:Curtis Allen} had efforts go just off target.
But the miss of the night belonged to {team:Glenavon}'s {player:Cooper} who hoisted over from in front of goal after {player:Andy Hall}'s ball in from the right had evaded the visitors' defence.
{team:Glentoran} could have snatched a late winner. {player:Steven Gordon} found himself with the ball at his feet in the area, but he smashed his effort straight at keeper {player:Jonathan Tuffey}.
"A draw was a fair result," reflected {team:Glentoran} manager {manager:Eddie Patterson}.
"{team:Glenavon} were the hungrier team in the first half and we found it difficult to get into the game.
"I felt we had the better attacks in the second half but our problem at the moment is being consistent over the 90 minutes."