::{team:MK Dons}: Chairman {manager:Pete Winkelman} unsure of {manager:Karl Robinson} future::
@1461851386
url: https://www.bbc.com/sport/football/36158832
{team:MK Dons} chairman {manager:Pete Winkelman} is unsure whether {manager:Karl Robinson} will continue as manager following their relegation back to {league:League One}.
{manager:Robinson}, 35, is the third longest-serving boss in English football's top four tiers, but could not keep {team:the Dons} up in their first {league:Championship} season.
"He's shown me great loyalty and he got us promoted," said {manager:Winkelman}.
"Is he an important character? Yes. Does that mean he's the manager next year? I don't know."
{team:MK Dons}, promoted to the second tier for the first time in their history a year ago, were relegated after Saturday's 4-1 home defeat by {team:Brentford}.
{manager:Robinson} has indicated he wants to remain at {stadium:Stadium:MK}, and the chairman said "it's in {manager:Karl}'s hands", but that he needs to prove he has learned the lessons of this season.
{manager:Winkelman} told BBC Three Counties Radio: "He needs to want to do it, he needs to recognise some of the mistakes that we've made, he needs to want to do something about putting them right, he needs to show me that he's got the ambition and drive and passion to pick himself off the floor and go again.
"I think if he did all of those things, I wouldn't have a problem with him staying on as our manager, and I don't think many of our supporters and players would either.
"However, if he's taking it badly, if he's trying to look to all the other reasons, and try to put the responsibility elsewhere, if he feels the grass may be greener elsewhere, then this is an opportunity for him to go and do something about that too."
{manager:Winkelman} was involved in moving the old {team:Wimbledon FC} to {team:Milton Keynes} in 2003 and led a consortium that bought the club from its Norwegian owners the following year.
But the 58-year-old said a poor season in the {league:Championship} was difficult to take and would make him reflect on his own position as well.
"We've got to see how much passion I can have. I've had probably the worst year personally in my life. But I'm still sitting here not telling you how wonderful the Rugby World Cup [{stadium:Stadium:MK} was one of the venues] was or what a brilliant stadium we've built, I'm trying to take responsibility for the things that have gone wrong," he continued.
"If I don't have that passion or commitment to come back then I won't last either."
{manager:Winkelman} admitted there had been mistakes made with recruitment, including the signings of {player:Christian Benavente}, {player:Jay Emmanuel-Thomas}, {player:Matthew Upson}, {player:Sergio Aguza} and {player:Dale Jennings}, who made a combined 10 league appearances this season.
"None of those players were on our recruitment list. That gives you an idea of the quantum of mistakes we made," he added. 
"Why I feel particularly responsible, and why I'm taking the heat, is because I was part of that decision-making process even though I knew at the time 'what are we doing?'
"It was a mistake in that we should have at least gone for one or two high-profile signings because that also reinforced the commitment to the players.
"I don't think I gave my senior players enough ammunition to go into the season with."