::Newport County: Justin Edinburgh hits back at Mark Aizlewood::
@1413476069
url: https://www.bbc.com/sport/football/29649230
Newport County manager Justin Edinburgh has hit back at claims from Carmarthen's Mark Aizlewood the Exiles will regret not signing striker Christian Doidge.
Former Newport player Aizlewood was surprised County did not sign Doidge on a permanent basis from Carmarthen.
Doidge, 21, had a trial at County but joined Dagenham & Redbridge, who Newport face on Saturday.
"I think he needs to keep his comments to himself," Edinburgh said.
"I'm not bringing the boy [Doidge] into it I'm bringing the manager's comments in to it.
"I don't think he's played at the level I've played at. I don't think he's managed at the level I have so it's probably best to keep those kind of comments to himself."
Responding to Edinburgh's comments Aizelwood, who won 39 caps for Wales told BBC Wales Sport: 
"If Justin wants to have a go at me that's fine, but I suggest he gets his facts right. Remind him how many caps I won for Wales."
Since joining League Two Dagenham in August for an undisclosed fee,  Newport-born Doidge has made four appearances without scoring a single goal for the Daggers.
The former Southampton academy player had been signed by Welsh Premier side Carmarthen  in January 2013 and spent a week's trial with County in April 2014.
Doidge spent another trial period with Dagenham & Redbridge in the summer before joining the club on a permanent deal during the summer.
Aizlewood, the former Charlton Athletic and Charlton Athletic defender, said in August that County had made a mistake in not signing Doidge.
"If County allow Christian to go to Dagenham it will be the biggest mistake they've made since they failed to secure a sell-on fee when John Aldridge left," he told the South Wales Argus, referring to Aldridge's 1984 move to Oxford United.
But County manager Edinburgh responded to those comments in a news conference ahead of his side's trip to Dagenham on Saturday.
"I beg to differ. We're talking about a top quality international in John Aldridge," Edinburgh added
"The boy [Doidge], I've seen him play twice this year, He's adjusting and finding his feet. 
"I think he could have a future in the game, but whether or not he's going to come back and bite me on the backside like I've been told, time will tell."