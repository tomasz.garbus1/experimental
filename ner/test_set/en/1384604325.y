::{manager:Roy Hodgson} says {country:England} will learn from defeat by {country:Chile}::
@1384604325
url: https://www.bbc.com/sport/football/24967181

                            
                        
{country:England} manager {manager:Roy Hodgson} admitted his experimental line-up learned lessons in their {league:World Cup} build-up as they were well beaten by {country:Chile}.
{team:Barcelona}'s {player:Alexis Sanchez} scored twice as {country:Chile} repeated the 2-0 friendly win they secured at {stadium:Wembley} in 1998.
{manager:Hodgson} gave debuts to {team:Southampton} pair {player:Adam Lallana} and {player:Jay Rodriguez}, as well as {team:Celtic} goalkeeper {player:Fraser Forster}.
"There were good lessons for us to learn. We gave players a chance to play for {country:England}," he said.
Check out what rating BBC Sport's chief football writer Phil McNulty awarded to the {team:Southampton} star and the rest of {manager:Roy Hodgson}'s {country:England} team 
"I can't talk away or explain away a defeat which I thought came to a {country:Chile} side who played very well. It's not all doom and gloom. It's how you deal with defeats and we'll retain a sense of perspective."
{country:Chile}'s victory ended a 10-game unbeaten run for {manager:Hodgson} and some boos greeted the final whistle from {country:England}'s frustrated fans.
{manager:Hodgson} said: "I think that's part of the game. Fans don't accept defeats. They don't want defeats. Who does? We certainly don't.
"There are not many games you see or watch on television when there isn't some booing when the home team has lost but I'm not prepared to criticise our fans in any way because they've been fantastic."
{manager:Hodgson} also gave a sympathetic verdict on his three {country:England} new boys, saying: "{manager:Forster} didn't have to make many saves but made quite a good one in the first half. He was chanceless for both of the goals.
"I thought {player:Adam Lallana} did well in a debut match against such good opposition and I thought it was a very big ask for {player:Rodriguez} to play in that position, but I have so many wide players injured that his debut maybe came a bit earlier than he would have liked against opposition that was maybe a bit stronger than he would have liked. I am certainly not disappointed with any of them."
{player:Lallana}, 25, revealed a determination to build on his {country:England} debut after his first taste of international football left him wanting more.
"It is every child's dream to represent their country and I am happy I fulfilled that," said the {team:Southampton} midfielder.
"I hope it is not the last time. I am just hungry for more now. I was proud to get my first cap, but obviously disappointed with the result."
The last team to beat {country:England} by a margin of two or more goals at {stadium:Wembley} were {country:France}, who came to London as world champions in February 1999 and won 2-0 thanks to two {player:Nicolas Anelka} goals
{team:Tottenham} winger {player:Andros Townsend}, 22, who came on as a substitute in the second half, admitted the opportunity to face South American opposition was valuable experience ahead of next summer.
"If we are going to go to the {league:World Cup} and do well," he said, "we are going to have to learn to play against teams like {country:Chile}.
"We have played a lot of European sides and to test ourselves against a South American side who do play a different style.
"The manager came in after the game and said to us: 'we did not get too carried away when we qualified for {country:Brazil}, so don't beat yourselves up because there is another game again when we can put all the wrongs right against {country:Germany}'."
{manager:Hodgson} confirmed {player:Joe Hart} will replace {player:Forster} in goal against {country:Germany} on Tuesday, when {player:Steven Gerrard}, {player:Ashley Cole}, {player:Phil Jagielka} and {player:Daniel Sturridge} will also be back in contention.
{team:Manchester United} defender {player:Phil Jones} suffered a groin injury and will undergo a scan to discover the extent of the problem but he is unlikely to be fit to face {country:Germany}.