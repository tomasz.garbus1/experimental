::{player:Charlie Horton}: {team:Leeds United} sign goalkeeper::
@1433860241
url: https://www.bbc.com/sport/football/33067785
{league:Championship} club {team:Leeds United} have signed goalkeeper {player:Charlie Horton} on a two-year contract.
USA Under-23 international {player:Horton} joined {team:Cardiff} from {league:League One} side {team:Peterborough} in 2014, but failed to make an appearance for the club.
The 20-year-old was born in London but moved to Ohio in the {country:United States} at the age of nine.
"He has very good potential and will be a future number one," {team:Leeds} manager {manager:Uwe Rosler} told the club's website.
"He has fantastic physical attributes and is a really positive character. He really wants to learn."