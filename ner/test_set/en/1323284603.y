::{team:Liverpool} striker {player:Luis Suarez} charged by FA over hand gesture::
@1323284603
url: https://www.bbc.com/sport/football/16043901
{team:Liverpool} striker {player:Luis Suarez} has been charged with improper conduct by the Football Association over an allegedly abusive hand gesture during the 1-0 defeat at Fulham on Monday.
{player:Suarez} appeared to raise the middle finger of his left hand towards the home fans as he left the pitch.
{team:Liverpool} have also been charged with failing to control their players after the dismissal of {player:Jay Spearing}. 
The club said they would review the FA documentation before making comment.
{player:Suarez} and {team:Liverpool} have until 1600 GMT on Monday 12 December to respond to both charges.
{country:Uruguay} star {player:Suarez} is now the subject of two FA disciplinary cases.
Last month, he was charged with racially abusing {team:Manchester United} defender {player:Patrice Evra} during {team:Liverpool}'s 1-1 draw with {team:Manchester United} in October.
The latest incident occurred at the end of a damaging defeat for {team:the Reds}, during which {player:Suarez} was subjected to chants of "cheat" from sections of the home crowd.

                            
                        
They barracked the player for what they saw as him going down too easily in an attempt to win penalties and free-kicks.
After the match, {team:Reds} boss {manager:Kenny Dalglish} said he had not seen the picture of the hand gesture. "If [it] is true then I've a decision to make," {manager:the Scot} added.
{manager:Dalglish} defended the striker over accusations of diving and described the taunts as "scandalous".
He added: "At the end of the day, we will look after {player:Luis} the best we can and I think it is about time he got a bit of protection from some people."
The charge against {team:Liverpool} concerns the reaction of their players to {player:Spearing}'s controversial second-half sending-off.
Five players including {player:Spearing} surrounded referee Kevin Friend after he showed the midfielder a straight red for a foul on striker {player:Moussa Dembele} with his follow-through after a tackle, having initially won the ball.
The dismissal came after 71 minutes, with the score at 0-0, before {player:Clint Dempsey}'s late winner sealed victory for {team:Fulham} to move them 13th in the league.
{manager:Dalglish} added: "It is frustrating because nobody tells us what the level of acceptance is. {player:Jay} had no other thought on his mind other than to win the ball - and he did win it."
{player:Spearing} will miss {team:Liverpool}'s next three matches after {team:the Reds} opted not to appeal against the sending-off.