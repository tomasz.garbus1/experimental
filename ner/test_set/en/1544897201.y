::{team:Watford} 3-2 {team:Cardiff City}::
@1544897201
url: https://www.bbc.com/sport/football/46491892
{team:Watford} manager {manager:Javi Gracia} said his side had been taught a "good lesson" after they held off a late {team:Cardiff} comeback to seal their first {league:Premier League} win since October.
Three fine goals from {player:Gerard Deulofeu}, {player:Jose Holebas} and {player:Domingos Quinas} saw {team:Watford} cruise into a comfortable lead, with their opponents proving no match for {team:the Hornets} for much of the game.
But with 10 minutes remaining at {stadium:Vicarage Road}, {player:Junior Hoilett} pulled back a seeming consolation goal before {player:Bobby Reid} added a second to give {team:Cardiff} a sniff of an unlikely point.
"The end of the game was different than the most part of it. We dominated the first 70 minutes, we created a lot of chances and we could have got a better victory today," {manager:Gracia} told BBC Sport.
"Their goalkeeper was amazing. The last part of the game I think was a good lesson for us to know how we have to manage the game.
"The last games we have played well, but today we deserved the win. It is important for the players and supporters. All of them deserve the victory."
{manager:Neil Warnock}'s side would have been dead and buried early on were it not for the acrobatics of goalkeeper {manager:Neil Etheridge}, who produced a string of stunning saves either side of half-time to keep {team:Cardiff} in the game - which marked the first top-flight meeting between these two sides.
{team:Watford} looked to have done enough to seal all three points at a canter yet {team:Cardiff}'s comeback came out of the blue following a performance that lacked any real energy.
"I am disappointed with the individual errors for the goals, but I am pleased with the way we kept going," {manager:Warnock} told BBC Sport.
"We set ourselves up to be difficult to break down and I cannot legislate for individual errors. We cannot keep playing like this every week and losing - we need to get results away from home."
{team:Watford} had lost their two previous home games before the visit of {team:Cardiff} but their performance was far superior to that of a side who, prior to kick-off, had picked up just nine points from their past 12 games.
{player:Deulofeu} was at the heart of much of their play and his goal - described by Garth Crooks on Final Score as '{player:Diego Maradona}-esque' - was richly deserved as {team:Cardiff} struggled to contain their opponents.
In a moment of magic, he jinked his way past two defenders before neatly placing the ball past {player:Etheridge}.
{player:Roberto Pereyra} - {team:Watford}'s top scorer this season with five goals - was not without his chances either and went close not long after {player:Deulofeu}'s opener, a poor final touch letting him down.
{team:Watford} went into half-time having enjoyed 75% of the first-half possession and, with moves straight off the training ground, added two more goals after the break, the second, a stunning curling effort from {player:Holebas} the pick of the bunch.
But they quickly became complacent after {player:Quinas}' third - his first {league:Premier League} goal - and {team:Cardiff} made them pay for the change in pace with two quickfire goals.
{team:Watford} could have had a fourth in added time but for another late save by {player:Etheridge}. 
Just two places separated {team:Watford} and {team:Cardiff} in the table before kick-off but the first half demonstrated a real gulf in class between the two sides.
But despite the defeat, {team:Cardiff} head home with a lot to thank goalkeeper {player:Etheridge} for. He produced save after save, single-handedly preventing {player:Pereyra} from scoring a hat-trick with less than an hour played.
Despite their lacklustre first-half performance, they weren't without their chances though and {player:Harry Arter} almost played {player:Josh Murphy} in on goal with a sliding cross midway through.
A rare moment of {team:Watford} carelessness was capitalised on by the visitors as {player:Quina} played the ball straight into their possession and {player:Hoilett} was able to put in a cross, which floated just high of {player:Callum Paterson} in the box.
It was in the second half that they found another gear with some slick play involving {player:Nathaniel Mendez-Laing} putting the pressure on the hosts.
{player:Hoilett} had a chance on goal before his and his side's perseverance was rewarded when he curled the ball in past {player:Ben Foster}, before {player:Reid} capitalised on a scramble in the box to poke home.

                            
                        

                            
                        
{team:Watford} travel to {team:West Ham} in their next {league:Premier League} outing on Saturday, 22 December (15:00 GMT), while {team:Cardiff} welcome {team:Manchester United} to {country:Wales} (17:30 GMT).
Match ends, {team:Watford} 3, {team:Cardiff City} 2.
Second Half ends, {team:Watford} 3, {team:Cardiff City} 2.
{player:Stefano Okaka} ({team:Watford}) wins a free kick in the attacking half.
Foul by {player:Sean Morrison} ({team:Cardiff City}).
{player:Ben Foster} ({team:Watford}) wins a free kick in the defensive half.
Foul by {player:Callum Paterson} ({team:Cardiff City}).
Foul by {player:JosÃ© Holebas} ({team:Watford}).
{player:Nathaniel Mendez-Laing} ({team:Cardiff City}) wins a free kick on the right wing.
Foul by {player:Abdoulaye DoucourÃ©} ({team:Watford}).
{player:VÃ­ctor Camarasa} ({team:Cardiff City}) wins a free kick on the right wing.
Foul by {player:Stefano Okaka} ({team:Watford}).
{player:Bruno Ecuele Manga} ({team:Cardiff City}) wins a free kick in the defensive half.
Corner,  {team:Watford}. Conceded by {manager:Neil Etheridge}.
Attempt saved. {player:Abdoulaye DoucourÃ©} ({team:Watford}) right footed shot from the centre of the box is saved in the top centre of the goal. Assisted by {player:Stefano Okaka}.
Attempt missed. {player:Domingos Quina} ({team:Watford}) right footed shot from outside the box is close, but misses the top right corner following a set piece situation.
Attempt missed. {player:Tom Cleverley} ({team:Watford}) right footed shot from outside the box misses to the left. Assisted by {player:Stefano Okaka} following a set piece situation.
Substitution, {team:Watford}. {player:Tom Cleverley} replaces {player:Gerard Deulofeu}.
{player:Abdoulaye DoucourÃ©} ({team:Watford}) wins a free kick in the attacking half.
Foul by {player:Sean Morrison} ({team:Cardiff City}).
Attempt blocked. {player:Stefano Okaka} ({team:Watford}) right footed shot from the left side of the box is blocked. Assisted by {player:Roberto Pereyra}.
Goal!  {team:Watford} 3, {team:Cardiff City} 2. {player:Bobby Reid} ({team:Cardiff City}) right footed shot from the centre of the box to the centre of the goal.
Attempt blocked. {player:Bobby Reid} ({team:Cardiff City}) right footed shot from the centre of the box is blocked.
Attempt saved. {player:Sol Bamba} ({team:Cardiff City}) left footed shot from the centre of the box is saved in the centre of the goal.
Substitution, {team:Watford}. {player:Stefano Okaka} replaces {player:Troy Deeney}.
Goal!  {team:Watford} 3, {team:Cardiff City} 1. {player:David Junior Hoilett} ({team:Cardiff City}) right footed shot from outside the box to the top right corner. Assisted by {player:VÃ­ctor Camarasa}.
Substitution, {team:Cardiff City}. {player:Bobby Reid} replaces {player:Aron Gunnarsson}.
Attempt missed. {player:Gerard Deulofeu} ({team:Watford}) right footed shot from a difficult angle on the right is close, but misses the top left corner. Assisted by {player:Isaac Success}.
Substitution, {team:Watford}. {player:Isaac Success} replaces {player:Ken Sema}.
Foul by {player:Troy Deeney} ({team:Watford}).
{manager:Neil Etheridge} ({team:Cardiff City}) wins a free kick in the defensive half.
Attempt missed. David {player:Junior Hoilett} ({team:Cardiff City}) header from the centre of the box is just a bit too high. Assisted by {player:Callum Paterson} with a cross.
Foul by {player:Domingos Quina} ({team:Watford}).
{player:VÃ­ctor Camarasa} ({team:Cardiff City}) wins a free kick in the defensive half.
Attempt saved. {player:JosÃ© Holebas} ({team:Watford}) left footed shot from the left side of the box is saved in the top right corner.
Offside, {team:Cardiff City}. {player:Nathaniel Mendez-Laing} tries a through ball, but {player:Callum Paterson} is caught offside.
Goal!  {team:Watford} 3, {team:Cardiff City} 0. {player:Domingos Quina} ({team:Watford}) right footed shot from outside the box to the top right corner. Assisted by {player:Ken Sema}.
Delay over. They are ready to continue.
Delay in match {player:JosÃ© Holebas} ({team:Watford}) because of an injury.
Attempt missed. {player:VÃ­ctor Camarasa} ({team:Cardiff City}) right footed shot from outside the box is close, but misses to the right. Assisted by {player:Sol Bamba}.
Substitution, {team:Cardiff City}. Lee Peltier replaces {player:Joe Bennett} because of an injury.