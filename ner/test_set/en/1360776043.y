::{manager:Graham Alexander}: {team:Fleetwood Town} task far from finished::
@1360776043
url: https://www.bbc.com/sport/football/21449647
{team:Fleetwood} manager {manager:Graham Alexander} says his side still have a lot of work to do if they are going to achieve promotion this season.
{team:The Cod Army} currently lie fourth in {league:League Two}, just two points behind the automatic promotion places. 
"We have a goal, but it's at the end of the season. If you want to be champions and top now, you're not champions," {manager:Alexander} told BBC Radio Lancashire.
"Things are never done in February - we need to keep our feet on the ground."
League football is a marathon, you can't look at the miles you've done behind but you've got to look at the wall in front of you
He added: "We try and get that message across every single day, and we've already spoken about what we can do for Saturday.
"League football is a marathon, you can't look at the miles you've done behind but you've got to look at the wall in front of you."
{team:Fleetwood}'s 2-1 victory at {stadium:Oxford} on Tuesday night extended their unbeaten run to four league matches, despite being without injured striker {player:Jon Parkin}.
And {manager:Alexander} feels that the performance of {player:Parkin}'s replacement, {player:Jean-Michel Fontaine}, is a sign of the strength in depth his squad have.
"We have to have competition for places, and we have to have players ready to step into the squad at any moment and they know what we are doing," the former {team:Preston} and {team:Burnley} player said.
"They've all done that to be fair. We lost {player:Parky} on Saturday and {player:Jean-Michel}'s come in and done a fantastic job for the team.
"It's great to see that because football is a squad game now."