::Ashley Vincent returns to Cheltenham from Port Vale::
@1374063160
url: https://www.bbc.com/sport/football/23345813
Winger Ashley Vincent has re-signed for Cheltenham Town after four years away from the League Two club.
The 28-year-old made 131 appearances for the Robins during his first spell at Whaddon Road between 2004 and 2009.
He went on to play for Colchester and Port Vale, where he scored eight goals in 40 games last season.
Vincent was offered a new deal by Vale in May, but turned it down. "In football terms, this is my home," he told the Cheltenham website. 
"It is slightly surreal, but this is a decision for my family and this is a family club. 
"I'm buzzing to be back and I can't wait to play for Cheltenham again and hopefully win promotion."
Vincent has agreed a one-year deal with the Robins for the 2013-14 League Two campaign.