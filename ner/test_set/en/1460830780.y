::{team:Forest Green Rovers} 1-2 {team:Woking}::
@1460830780
url: https://www.bbc.com/sport/football/36004553
{team:Forest Green}'s slim hopes of automatic promotion from the {league:National League} were ended after a shock defeat by {team:Woking}.
{team:Rovers} took the lead early on when {player:Kieffer Moore} fired {player:Sam Wedgbury}'s cross in from close range.
{player:Danny Carr}'s penalty drew the visitors level after the break after midfielder {player:Bruno Andrade} had been fouled by {player:David Pipe} inside the box.
And {player:Cameron Norman} powered a shot past goalkeeper {player:Steve Arnold} in injury time to secure the win for {team:Woking}.
{team:Rovers} are second in the table, now nine points behind local rivals {team:Cheltenham Town} with two games to play, while {team:Woking} climbed to 14th place.
{team:Forest Green} boss {manager:Ady Pennock} told BBC Radio Gloucestershire:

                            
                        
"I'm extremely disappointed - we should have won the game by five or six.
"Especially in the first half, we should have been three or four-nil up - it's just being ruthless in front of goal.
"That's what let us down and then to concede two poor goals that just sums up the game, the game should have been dead and buried at half time in my opinion."
{team:Woking} boss {manager:Garry Hill} told BBC Surrey:

                            
                        
"We rode our luck a little bit and I was very pleased to be one-nil down at half-time and there is no doubt about that. 
"We shown a lot of character and I think we tried to play football the right way in the first half. I felt we bossed it in the second half and we were certainly the better side.
"It's nice to see one of the young lads {player:Cameron Norman} get the goal and we've been waiting a long time not only as a management team but also as supporters. 
"They come up and come down to be here and everywhere to support us and I am so pleased for them."
Match ends, {team:Forest Green Rovers} 1, {team:Woking} 2.
Second Half ends, {team:Forest Green Rovers} 1, {team:Woking} 2.
Hand ball by Keiran Murtagh ({team:Woking}).
{player:Matt Robinson} ({team:Woking}) is shown the yellow card.
Goal!  {team:Forest Green Rovers} 1, {team:Woking} 2. {player:Cameron Norman} ({team:Woking}) right footed shot from the centre of the box to the bottom left corner.
Corner,  {team:Woking}.
{player:Kieffer Moore} ({team:Forest Green Rovers}) hits the bar with a header from the centre of the box.
Attempt saved. {player:Kieffer Moore} ({team:Forest Green Rovers}) header from the left side of the six yard box is saved. Assisted by {player:Anthony Jeffrey} with a cross.
Foul by {player:Cameron Norman} ({team:Woking}).
{player:Kieffer Moore} ({team:Forest Green Rovers}) wins a free kick.
Substitution, {team:Woking}. {player:Giuseppe Sole} replaces {player:Danny Carr}.
Substitution, {team:Forest Green Rovers}. {player:Marcus Kelly} replaces {player:Darren Carter}.
Foul by Keiran Murtagh ({team:Woking}).
{player:Brett Williams} ({team:Forest Green Rovers}) wins a free kick.
Corner,  {team:Forest Green Rovers}.
Corner,  {team:Forest Green Rovers}.
Attempt missed. {player:Darren Carter} ({team:Forest Green Rovers}) header from the right side of the box misses to the right. Assisted by {player:Elliott Frear}.
Corner,  {team:Forest Green Rovers}.
Attempt saved. {player:Elliott Frear} ({team:Forest Green Rovers}) right footed shot from the centre of the box is saved.
Foul by {player:Cameron Norman} ({team:Woking}).
{player:David Pipe} ({team:Forest Green Rovers}) wins a free kick.
Corner,  {team:Woking}.
Corner,  {team:Woking}.
Corner,  {team:Forest Green Rovers}.
Substitution, {team:Forest Green Rovers}. {player:Anthony Jeffrey} replaces {player:Keanu Marsh-Brown}.
Substitution, {team:Forest Green Rovers}. {player:Brett Williams} replaces {player:Kurtis Guthrie}.
Corner,  {team:Forest Green Rovers}.
Corner,  {team:Forest Green Rovers}.
Goal!  {team:Forest Green Rovers} 1, {team:Woking} 1. {player:Danny Carr} ({team:Woking}) converts the penalty with a right footed shot to the bottom left corner.
{player:David Pipe} ({team:Forest Green Rovers}) is shown the yellow card for a bad foul.
Penalty conceded by {player:David Pipe} ({team:Forest Green Rovers}) after a foul in the penalty area.
Penalty {team:Woking}. {player:Bruno Andrade} draws a foul in the penalty area.
Attempt missed. {player:James Jennings} ({team:Forest Green Rovers}) left footed shot from outside the box misses to the right.
{player:Darren Carter} ({team:Forest Green Rovers}) is shown the yellow card for a bad foul.
Foul by {player:Kieffer Moore} ({team:Forest Green Rovers}).
{player:Cameron Norman} ({team:Woking}) wins a free kick.
Attempt missed. {player:Kieffer Moore} ({team:Forest Green Rovers}) header from the centre of the box misses to the left. Assisted by {player:James Jennings} with a cross.
Foul by {player:Jake Caprice} ({team:Woking}).
{player:Elliott Frear} ({team:Forest Green Rovers}) wins a free kick.
Attempt saved. {player:Bruno Andrade} ({team:Woking}) right footed shot from outside the box is saved.