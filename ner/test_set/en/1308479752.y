::New {team:Cardiff} boss {manager:Malky Mackay} promised transfer funds::
@1308479752
url: https://www.bbc.com/sport/football/13822501
{team:Cardiff City} chief executive {manager:Gethin Jenkins} has said new manager {manager:Malky Mackay} will have money to spend to improve the squad.
Former {country:Scotland} defender {manager:Mackay} has been confirmed as the {team:Bluebirds}' new boss after leaving {team:Watford}.
{manager:Jenkins} said the board will provide {manager:Mackay} with the funds needed to strengthen the squad ahead of the new {league:Championship} season. 
"There is a transfer budget available," {manager:Jenkins} told BBC Sport {country:Wales}.
{manager:Mackay}'s priority as new manager will be strengthening a squad which has seen a number of departures.
Since the end of the 2010-11 season, players such as {player:Michael Chopra} and {player:Chris Burke} have left and some on-loan players - including {team:Manchester City}'s {player:Craig Bellamy} - have returned to their parent clubs.
"As [with] all managers he's got a list of players he wants us to look at," added {manager:Jenkins}.
"We'll sit down over the next few days and see which are our targets and try to go and recruit some players.
"The {country:Malaysia}n investors are fully behind this decision [and] they want to make money available
"We've got some hard work ahead of us in terms of recruitment but we want to compete.
"It's going to be one of the toughest {league:Championships} in memory but we want to be at the top end."
{manager:Jenkins} revealed {team:Cardiff} had considered a number of candidates before choosing 39-year-old {manager:Mackay} as their new boss.
{manager:Mackay} had been in charge at {team:Watford} since June 2009 and had signed a new three-year contract with the {stadium:Vicarage Road} club in March.
"We've got a young, ambitious, hardworking manager," {manager:Jenkins} said of the former {team:Norwich} and {team:West Ham} defender.
"We think he's the right person to help us compete at the top of the {league:Championship}.
"We said all along we would take our time to make sure we got the right person. We consulted [and] weren't afraid to ask questions and ask around.
"We looked at other people but when we looked at it we kept coming back to one answer and we felt that was {manager:Malcolm Mackay}.
"We want to compete. We've said all along that we're not scared of being ambitious and nobody grows up wanting to finish mid-table."
Prior to {manager:Mackay}'s appointment as {team:Cardiff} boss, a number of other candidates had been linked with the job, including {manager:Chris Hughton} and {manager:Roberto Di Matteo}.
Former {country:England} striker {player:Alan Shearer} revealed he had held "unsuccessful" talks with the club before the {team:Bluebirds} confirmed {manager:Mackay} as their new manager.
"I think anybody recruiting a senior position like this would expect us to look at many candidates and look at all the options available to us," said {manager:Jenkins}.
"We wouldn't be doing our job properly if we didn't do that.
"I'm sure if someone of {player:Alan Shearer}'s stature comes along anybody would expect us to investigate and look into it.
"We had conversations with him and he was one of the many options. He's the one that went public and said he didn't want to be considered for it."
With {manager:Mackay} now in place as manager, {manager:Jenkins} added he was confident {team:Cardiff} would be among the {league:Championship}'s front runners next season.
"We want to try and compete to be at the top of the table," said {manager:Jenkins}.
"We know the desire [and] the passion from the fans and the ambition is here. 
"They can be assured we match that ambition. We are going to work hard with them to try and get to the top."
