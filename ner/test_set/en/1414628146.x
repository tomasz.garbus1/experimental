::Alan Pardew: Newcastle boss says: 'I know what I am doing'::
@1414628146
url: https://www.bbc.com/sport/football/29828009

                            
                        
Alan Pardew defended his position as Newcastle manager after their League Cup victory at Manchester City, telling supporters: "I do know what I'm doing."
Pardew, 53, has been the subject of protests from some fans after Newcastle's poor start of the season.
But the 2-0 win at Etihad Stadium secured a League Cup quarter-final place and followed Sunday's Premier League victory at Tottenham.
"This was a victory for our fans," said Pardew.
"It's important for us that we just keep this momentum going."
Newcastle sit 14th in the Premier League and face a trip to Tottenham in the League Cup last eight in the week beginning 15 December.
Pardew has guided Newcastle to fifth, 16th and 10th in his three full seasons at the club and has seen key players such as Andy Carroll and Yohan Cabaye leave for large transfer fees.
Supporters have held up 'sackpardew.com' banners during matches since early in the season.
He admitted he was under pressure earlier this month, but defender Daryl Janmaat said on Wednesday that the players are behind their manager.
"I do know what I'm doing," Pardew said. "I think I've had most of the scalps as Newcastle manager, maybe except Arsenal, and I look forward to playing them."
Pardew rested several players at City ahead of Saturday's Premier League game against Liverpool at St James' Park.
Midfielder Ryan Taylor is likely to be available after describing his return from a 26-month lay-off with a cruciate ligament injury as a "dream come true".
"There are no words I can think of, from the medical team to the fans, I can't thank the football club enough," Taylor told BBC Newcastle.
Pardew said: "Ryan Taylor was a kind of story you'd read in some football magazine that was made up. He was outstanding in his first game after all that time. Brilliant."
Taylor, 30, delivered the assist for Rolando Aarons to open the scoring, before substitute Moussa Sissoko hit the second to move Newcastle a step closer to a first Cup triumph since 1955.
Goalkeeper Rob Elliot added: "Tonight's all about Ryan.
"He's epitomised everything about us: two years of solid hard work, with setbacks. Everyone is buzzing for him and the fans - it's a great night for us all."