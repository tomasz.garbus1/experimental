::Stephen Dawson: Bury re-sign midfielder from Scunthorpe United::
@1494941792
url: https://www.bbc.com/sport/football/39939081
League One side Bury have re-signed midfielder Stephen Dawson from Scunthorpe on a three-year deal.
The 31-year-old ex-Republic of Ireland Under-21 midfielder spent two seasons with the Shakers between 2008 and 2010 when he captained the side.
The former Leicester City trainee was out of contract at the Iron, but had been offered a new deal.
"This is a huge signing for us. He is a player that I have tried to entice at previous clubs," said boss Lee Clark.
Dawson joins striker Jermaine Beckford and full-back Phil Edwards in signing for Bury for next season.
Find all the latest football transfers on our dedicated page.