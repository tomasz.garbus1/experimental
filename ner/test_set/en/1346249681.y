::{player:Lucas Leiva}: {team:Liverpool} midfielder faces three months out::
@1346249681
url: https://www.bbc.com/sport/football/19414058
{team:Liverpool} midfielder {player:Lucas Leiva} faces another injury lay-off, this time with a thigh injury.
{player:The Brazilian}, who missed most of last season with a knee injury, suffered the problem in the opening minutes of Sunday's draw with {team:Manchester City}.
{team:Reds} boss {manager:Brendan Rodgers} confirmed the news, admitting the 25-year-old faced "two to three months" out.
{player:Lucas} said: "[There are] No words to describe my feelings. It's hard to be positive but better days will come."
{player:Lucas}, who joined {team:Liverpool} from {team:Gremio} in 2007, damaged his anterior cruciate ligament in December last year.
After undergoing successful surgery, he missed the remainder of the season before returning for pre-season training last month.
He returned to competitive action in the first-leg of {team:Liverpool}'s {league:Europa League} qualifier with {team:Gomel} and was named in {manager:Rodgers}'s first two {league:Premier League} starting line-ups.
Fellow midfielder, on-loan signing {player:Nuri Sahin}, is available to make his debut against {team:Arsenal} on Sunday at {stadium:Anfield}.