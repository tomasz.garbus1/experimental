::Rory Patterson & Shane McEleney transfer-listed by Derry City::
@1419084965
url: https://www.bbc.com/sport/football/30561776
Derry City have made star striker Rory Patterson and defender Shane McEleney available for transfer as part of cost-cutting measures at the Brandywell.
Manager Peter Hutton said the club had to "look at the bigger picture which must be a sustainable club working within its means".
"Shane and Rory have been made fully aware of the situation," Hutton told the Derry Journal.
Patterson scored an impressive 61 goals in 92 Derry appearances.
City's defeat in the FAI Cup final in November meant that they missed out on a European place after a hugely disappointing League campaign.
Derry's struggles in 2014 - which led to Roddy Collins losing the Brandywell job after a brief spell in charge - meant reduced home attendances and the club has opted to cash in on two of its playing assets.
Earlier this week, long-serving Barry Molloy also left the club after opting to join Irish Premiership outfit Crusaders.
Northern Ireland international Patterson, 30, tweeted on Thursday evening that he had not spoken to any other clubs by that stage and "hadn't a clue" where he would be playing next.
Patterson made his Northern Ireland debut while playing for Irish League club Coleraine in February 2010 before spending a season with English club Plymouth Argyle.
He went on to make four further international appearances but has not featured since the game against Scotland in November 2010.
McEleney, 23, lost his place during Collins's reign and continued to struggle hold down a first-team spot after Hutton took charge.
The defender represented Northern Ireland at Under-21 level two years ago after previously for at youth level for the Republic of Ireland.