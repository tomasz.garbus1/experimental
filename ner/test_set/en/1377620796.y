::{team:Aston Villa} defender {player:Enda Stevens} joins {team:Notts County} on loan::
@1377620796
url: https://www.bbc.com/sport/football/23856587
{team:Notts County} have signed {team:Aston Villa} full-back {player:Enda Stevens} on a 28-day emergency loan deal.
The 23-year-old could make his debut for {team:the Magpies} against {team:Liverpool} in the {league:Capital One Cup} on Tuesday.
{player:Stevens} joined {team:Villa} from {team:Shamrock Rovers} for an undisclosed fee in January 2012 and made nine appearances last season.
{team:Chris Kiwomya's side} are currently 21st in League One with one point from four matches.