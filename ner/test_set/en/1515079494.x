::Chesterfield sign Zavon Hines, Millwall's Sid Nelson and Barnsley's Josh Kay::
@1515079494
url: https://www.bbc.com/sport/football/42566193
Chesterfield have signed forward Zavon Hines, while Millwall defender Sid Nelson and Barnsley midfielder Josh Kay have joined on loan deals.
Hines, 29, has agreed an 18-month contract after having his deal at Maidstone cancelled by mutual consent.
He scored 10 goals in 25 appearances for the National League club after joining the Stones last August.
Centre-back Nelson, 22, and Kay, 21, have joined the Spireites until the end of the season.
Nelson featured in 12 League Two games on loan at Yeovil this term, while Kay made two appearances during a month-long spell with National League side Tranmere.
Find all the latest football transfers on our dedicated page.