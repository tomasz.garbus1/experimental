::Spartak Moscow: Santi Cazorla scored first goal since serious Achilles injury::
@1538692201
url: https://www.bbc.com/sport/football/45750799
Former Arsenal midfielder Santi Cazorla scored a 96th-minute equaliser for Villarreal in the Europa League - his first goal since a serious Achilles injury.
Cazorla converted a penalty against Spartak Moscow for his first goal since netting for Arsenal in September 2016.
He was ruled out for two years because of the injury which needed 11 operations and involved bacteria "eating" eight centimetres of his ankle tendon.
Cazorla rejoined Villarreal in July.
The Spaniard contracted gangrene after one of his surgeries and had been told he could lose his leg and should be satisfied just to walk again.
The 33-year-old had his final surgery in May and had his Achilles reconstructed, with doctors grafting skin from his left arm - featuring a tattoo - to his right ankle.

                            
                        
Match ends, Spartak Moscow 3, Villarreal 3.
Second Half ends, Spartak Moscow 3, Villarreal 3.
Goal!  Spartak Moscow 3, Villarreal 3. Santiago Cazorla (Villarreal) converts the penalty with a right footed shot to the centre of the goal.
Penalty conceded by Aleksandr Tashaev (Spartak Moscow) after a foul in the penalty area.
Penalty Villarreal. Ramiro Funes Mori draws a foul in the penalty area.
Corner,  Villarreal. Conceded by Salvatore Bocchetti.
Attempt saved. Aleksandr Tashaev (Spartak Moscow) right footed shot from outside the box is saved in the bottom left corner. Assisted by Sofiane Hanni.
Attempt blocked. VÃ­ctor Ruiz (Villarreal) header from the centre of the box is blocked. Assisted by Miguel LayÃºn.
Substitution, Spartak Moscow. Artem Timofeev replaces Dmitri Kombarov.
Foul by Fernando (Spartak Moscow).
Pablo Fornals (Villarreal) wins a free kick in the attacking half.
Attempt saved. Ramiro Funes Mori (Villarreal) header from the centre of the box is saved in the centre of the goal. Assisted by Miguel LayÃºn.
Nikolai Rasskazov (Spartak Moscow) is shown the yellow card for a bad foul.
Foul by Nikolai Rasskazov (Spartak Moscow).
Miguel LayÃºn (Villarreal) wins a free kick in the attacking half.
Goal!  Spartak Moscow 3, Villarreal 2. Lorenzo Melgarejo (Spartak Moscow) right footed shot from the centre of the box to the bottom right corner.
Sofiane Hanni (Spartak Moscow) hits the left post with a right footed shot from outside the box. Assisted by ZÃ© LuÃ­s.
Offside, Spartak Moscow. Georgi Dzhikiya tries a through ball, but ZÃ© LuÃ­s is caught offside.
Fernando (Spartak Moscow) is shown the yellow card.
Offside, Villarreal. Alfonso Pedraza tries a through ball, but Gerard Moreno is caught offside.
Goal!  Spartak Moscow 2, Villarreal 2. ZÃ© LuÃ­s (Spartak Moscow) header from very close range to the bottom right corner. Assisted by Aleksandr Tashaev with a cross.
Gerard Moreno (Villarreal) is shown the yellow card.
Georgi Dzhikiya (Spartak Moscow) is shown the yellow card for a bad foul.
Foul by Georgi Dzhikiya (Spartak Moscow).
Gerard Moreno (Villarreal) wins a free kick in the attacking half.
Substitution, Spartak Moscow. Aleksandr Tashaev replaces Alexander Lomovitskiy.
Attempt missed. Alexander Lomovitskiy (Spartak Moscow) right footed shot from the centre of the box is close, but misses to the left. Assisted by Fernando.
Attempt missed. Fernando (Spartak Moscow) right footed shot from outside the box misses to the left.
ZÃ© LuÃ­s (Spartak Moscow) wins a free kick in the defensive half.
Foul by Ramiro Funes Mori (Villarreal).
Offside, Spartak Moscow. Fernando tries a through ball, but Lorenzo Melgarejo is caught offside.
Substitution, Villarreal. Miguel LayÃºn replaces Karl Toko Ekambi.
Roman Zobnin (Spartak Moscow) wins a free kick in the defensive half.
Foul by Gerard Moreno (Villarreal).
Sofiane Hanni (Spartak Moscow) hits the left post with a right footed shot from outside the box. Assisted by Lorenzo Melgarejo.
Offside, Villarreal. Pablo Fornals tries a through ball, but Karl Toko Ekambi is caught offside.
Georgi Dzhikiya (Spartak Moscow) wins a free kick in the defensive half.
Foul by Gerard Moreno (Villarreal).
Substitution, Villarreal. Santiago Cazorla replaces Samuel Chukwueze.
Attempt blocked. Samuel Chukwueze (Villarreal) left footed shot from the right side of the box is blocked. Assisted by Pablo Fornals.