::Plymouth Argyle striker Matt Lecointe signs new contract::
@1401456104
url: https://www.bbc.com/sport/football/27632462
Plymouth Argyle striker Matt Lecointe has signed a new one-year deal.
The 19-year-old missed all of last season after suffering a knee ligament injury during a pre-season friendly.
"It's all looking really positive for next season," said the former England youth international. 
"It's nice to know that the manager has got confidence in you to give you another one-year deal, so I'm hoping I can repay him and score some goals," added Lecointe.
A product of Argyle's youth system, he made his debut as a 16-year-old in the club's 1-0 League Cup loss to Millwall.
He went on to make 30 appearances for Plymouth, scoring two goals, before being hurt against Truro City last summer.
"Everything feels fine. There have been no setbacks. I just hope that continues.
"My aim is just to try to play as much football for Plymouth Argyle; to score as many goals as I can. But my first target is just to get through pre-season, stay fit. After that, I'll reassess things."
He joins 18-year-old youth team graduates Aaron Bentley and River Allen in committing himself to Home Park.
The pair, who first joined the Pilgrims as part of their Centre of Excellence, have signed their first professional deals.
They were the only youth-team players 
They join  and  in signing new deals to stay at Home Park for the upcoming season, alongside new signings  and 