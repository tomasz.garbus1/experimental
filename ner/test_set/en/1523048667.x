::Cardiff City 0-1 Wolves::
@1523048667
url: https://www.bbc.com/sport/football/43592741
Cardiff City missed two stoppage-time penalties as they lost to Championship leaders Wolves in a dramatic finale.
At a packed Cardiff City Stadium, Ruben Neves scored a brilliant second-half free-kick from 25 yards.
After being bundled over in the box, Cardiff's Gary Madine saw his low penalty saved by John Ruddy.
Moments later, Junior Hoilett blasted a spot-kick against the bar as Wolves extended their lead at the top of the table to nine points.
They need only five points from the final five games of the season to seal a return to the Premier League.
The stupefying finish was a stark contrast to what had otherwise been something of a slow burner - an absorbing but unspectacular encounter between two very different but well-matched teams.
It was also a demoralising end to the game for Cardiff, who were twice given hope of rescuing a point when referee Mike Dean pointed to the spot - only for their missed penalties to consign them to a first defeat in 14 games.
Their despair was in startling contrast to the jubilation of their opponents, whose coaches and substitutes rushed on to the pitch at the full-time whistle, while their travelling supporters celebrated with riotous gusto in the stands.
The Wolves celebrations seemed to anger Cardiff manager Neil Warnock, who refused to shake hands with counterpart Nuno Espirito Santo at full-time and said after the game that he "did not want to see" the Portuguese.
While Wolves need only two wins to secure promotion, Cardiff remain five points clear of third-placed Fulham in a battle for the second automatic promotion place which promises to go down to the wire.
Fulham visit Sheffield Wednesday on Saturday and, with the Londoners on an 18-game unbeaten run, Cardiff will be looking over their shoulders.
Despite their advantage over Fulham, Warnock has always said the play-offs are Cardiff's target, although this is more than likely a little mischievous from the wily veteran of seven promotions.
Cardiff faced Wolves in the knowledge they needed only a point to be guaranteed at least a top-six spot but, like their opponents here, they had their eyes on a bigger and better prize.
With a record league crowd of 29,317 gathered under the lights at Cardiff City Stadium, there was a frisson of anticipation for this meeting between the Championship's two leading sides.
It was an intriguing clash of styles, with the patient, technically accomplished visitors confronted by an abrasive, functional Cardiff side, built very much in the image of their manager.
The hosts looked to fuel the fervent atmosphere with their direct approach, Sol Bamba heading one of their many dangerous set-pieces wide.
Wolves, by contrast, sought a more measured approach, dominating possession and slowing the tempo as they built their attacks gradually, with Neves and Diogo Jota both testing home goalkeeper Neil Etheridge with powerful efforts.
This was the style which had kept Santo's men at the top of the Championship since October, and it paid dividends once again against their closest rivals for the title.
After Leo Bonatini rounded Etheridge and struck the post, Neves sent the travelling fans wild with a brilliant curling free-kick from 25 yards.
The real drama, however, came in added time.
Wolves captain Conor Coady had his head in his hands after Dean adjudged him to have fouled Madine in the area, although he was given a reprieve when Ruddy - diving low to his left - saved the striker's poorly struck spot-kick.
Astonishingly, another penalty followed moments later when Aron Gunnarsson was fouled.
But just as Wolves thought victory had been snatched away from them, Hoilett smashed his effort against the bar to spark scenes of unadulterated jubilation among the visitors.
Cardiff City manager Neil Warnock: "It was a cracking game, a good advertisement. A great crowd and a fabulous, world-class goal for Wolves. Two good teams and a cracking atmosphere.
"I'm disappointed we haven't won, let alone get a point. It's one of those days when nothing seemed to go in.
"We just have to get on with it. We showed we're a decent team. 
"You're not going to get happy faces. They (Cardiff's players) are all disappointed. It shows how far we've come, playing against the so-called best team in the Championship and we more than held our own."
Wolves manager Nuno Espirito Santo: "Football is always innovating and it's a fantastic game. That's why we love it so much. It can give and take.
"We deserve it. If you look at the game, we had chances not to suffer in the end but that's why people voted for John Ruddy in the Championship team of the season and he gave us victory.
"I was not even thinking about the first penalty (when the second was awarded) but we should control the emotions better, which is my job."
Match ends, Cardiff City 0, Wolverhampton Wanderers 1.
Second Half ends, Cardiff City 0, Wolverhampton Wanderers 1.
Penalty missed! Still  Cardiff City 0, Wolverhampton Wanderers 1. David Junior Hoilett (Cardiff City) hits the bar with a right footed shot.
RÃºben Neves (Wolverhampton Wanderers) is shown the yellow card.
Penalty conceded by Ivan Cavaleiro (Wolverhampton Wanderers) after a foul in the penalty area.
Penalty Cardiff City. Aron Gunnarsson draws a foul in the penalty area.
Attempt saved. Sean Morrison (Cardiff City) header from very close range is saved in the top right corner. Assisted by Gary Madine with a headed pass.
Attempt blocked. Anthony Pilkington (Cardiff City) left footed shot from the centre of the box is blocked. Assisted by David Junior Hoilett with a headed pass.
Corner,  Cardiff City. Conceded by John Ruddy.
Penalty saved! Gary Madine (Cardiff City) fails to capitalise on this great opportunity,  right footed shot saved  in the bottom right corner.
Penalty conceded by Conor Coady (Wolverhampton Wanderers) after a foul in the penalty area.
Penalty Cardiff City. Anthony Pilkington draws a foul in the penalty area.
HÃ©lder Costa (Wolverhampton Wanderers) is shown the yellow card for dangerous play.
Dangerous play by HÃ©lder Costa (Wolverhampton Wanderers).
Neil Etheridge (Cardiff City) wins a free kick in the defensive half.
Attempt saved. Ivan Cavaleiro (Wolverhampton Wanderers) right footed shot from the right side of the box is saved in the bottom right corner. Assisted by HÃ©lder Costa.
Attempt saved. Ivan Cavaleiro (Wolverhampton Wanderers) right footed shot from outside the box is saved in the centre of the goal. Assisted by Willy Boly.
Attempt missed. HÃ©lder Costa (Wolverhampton Wanderers) left footed shot from the centre of the box is close, but misses to the left. Assisted by RÃºben Neves.
HÃ©lder Costa (Wolverhampton Wanderers) wins a free kick in the defensive half.
Foul by Sol Bamba (Cardiff City).
Attempt missed. Aron Gunnarsson (Cardiff City) right footed shot from outside the box is close, but misses the top left corner following a set piece situation.
Attempt missed. Craig Bryson (Cardiff City) left footed shot from outside the box misses to the right. Assisted by Sean Morrison with a headed pass following a set piece situation.
Substitution, Cardiff City. Anthony Pilkington replaces Kenneth Zohore.
Foul by Ivan Cavaleiro (Wolverhampton Wanderers).
David Junior Hoilett (Cardiff City) wins a free kick in the attacking half.
Attempt saved. Alfred N'Diaye (Wolverhampton Wanderers) left footed shot from the left side of the box is saved in the bottom left corner. Assisted by Ivan Cavaleiro.
Offside, Cardiff City. Sean Morrison tries a through ball, but Sol Bamba is caught offside.
Foul by HÃ©lder Costa (Wolverhampton Wanderers).
Craig Bryson (Cardiff City) wins a free kick in the attacking half.
Offside, Cardiff City. Aron Gunnarsson tries a through ball, but Sean Morrison is caught offside.
Substitution, Wolverhampton Wanderers. Alfred N'Diaye replaces Benik Afobe.
Corner,  Cardiff City. Conceded by Barry Douglas.
Romain Saiss (Wolverhampton Wanderers) is shown the yellow card for a bad foul.
Foul by Romain Saiss (Wolverhampton Wanderers).
Nathaniel Mendez-Laing (Cardiff City) wins a free kick on the left wing.
Attempt missed. Kenneth Zohore (Cardiff City) left footed shot from the left side of the box is high and wide to the left.
Romain Saiss (Wolverhampton Wanderers) wins a free kick in the defensive half.
Foul by Gary Madine (Cardiff City).
Substitution, Cardiff City. Gary Madine replaces Yanic Wildschut.
Attempt missed. RÃºben Neves (Wolverhampton Wanderers) right footed shot from outside the box is close, but misses to the right.