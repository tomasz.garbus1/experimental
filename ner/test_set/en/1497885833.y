::{player:Nathan Delfouneso}: {team:Blackpool} striker agrees new contract::
@1497885833
url: https://www.bbc.com/sport/football/40327621
{team:Blackpool} striker {player:Nathan Delfouneso} has signed a new one-year contract with the {league:League One} side, with the option of a further year.
The ex-{team:Aston Villa} trainee and {country:England} Under-21 international is in his fourth stint with {team:the Tangerines} having had two spells as well the 2014-15 season.
The 26-year-old scored six goals last term after returning in January.
"The club should never have been in {league:League Two} and we need to keep pushing," he told the club website.