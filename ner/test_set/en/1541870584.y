::{team:Reading} 2-2 {team:Ipswich}: {team:Reading}'s late header earns a draw::
@1541870584
url: https://www.bbc.com/sport/football/46081002
An 84th-minute header from {player:Yakou Meite} gave {team:Reading} a draw with {team:Ipswich}, after the visitors had led for the majority of the game.
{player:Gwion Edwards} scored inside five minutes for {team:Ipswich} with a right-footed shot that found the bottom left corner from outside the box.
It took the home side just two minutes to equalise with {player:Meite}'s overhead kick.
{team:The Tractor Boys} responded once more in the 11th minute, as a mix-up at the back for {team:Reading} gave {player:Freddie Sears} the chance to get through on goal and slot past keeper {player:Anssi Jaakkola}.
However, {player:Meite}'s late header from {player:Leandro Bacuna}'s pass secured a point for the hosts.
New {team:Ipswich} manager {manager:Paul Lambert} is still without a win in two matches in charge, but his side had control until the interval, with {player:Jaakkola} having to make smart saves to keep out two efforts from {player:Jordan Roberts}.
{team:Ipswich} were pegged back after the interval, with {team:Reading} showing much more intent going forward.
{player:Meite} looked to be through on goal, only to be flagged offside, then {player:Bartosz Bialkowski} made a point-blank save to deny {player:Mo Barrow}.
{team:Ipswich} sat back but almost extended their lead as {player:Jaakkola} had to move quickly to snuff out the danger from {player:Roberts} after an error by {player:John O'Shea}.
{team:Ipswich}, who had lost six of their previous eight {league:Championship} away games this season, were not able to withstand pressure from {player:Meite} late on.
The forward took his league tally to six goals in 15 games, which is more than he scored in 31 games for {team:Sochaux} in {league:Ligue 2} last season.
{team:Reading} manager {manager:Paul Clement}:
"Terrible performance from us in the first half, really bad, really disappointed, I was embarrassed actually, I can't get my head round why that was.
"I think we were fortunate to get in at the half, I don't think our defenders gave us the stability we needed.
"We deservedly got back into the game, we finished stronger than they did."
Ipswich manager {manager:Paul Lambert}:
"I thought we were outstanding, I thought we passed the ball great, we looked great, we controlled the game.
"I'm really, really proud of them, we're disappointed with a draw, but the performance was excellent, especially first half."
Match ends, {team:Reading} 2, {team:Ipswich Town} 2.
Second Half ends, {team:Reading} 2, {team:Ipswich Town} 2.
Attempt missed. {player:Sam Baldock} ({team:Reading}) right footed shot from the centre of the box is too high. Assisted by {player:Garath McCleary}.
Attempt blocked. {player:Garath McCleary} ({team:Reading}) right footed shot from the right side of the box is blocked. Assisted by {player:Marc McNulty}.
{player:Modou Barrow} ({team:Reading}) wins a free kick in the defensive half.
Foul by {player:Trevoh Chalobah} ({team:Ipswich Town}).
Attempt missed. {player:Sam Baldock} ({team:Reading}) right footed shot from the centre of the box is too high.
Offside, {team:Ipswich Town}. {player:Gwion Edwards} tries a through ball, but {player:Kayden Jackson} is caught offside.
{player:Jordan Spence} ({team:Ipswich Town}) is shown the yellow card for a bad foul.
{player:Modou Barrow} ({team:Reading}) wins a free kick on the left wing.
Foul by {player:Jordan Spence} ({team:Ipswich Town}).
Goal!  {team:Reading} 2, {team:Ipswich Town} 2. {player:Yakou Meite} ({team:Reading}) header from the centre of the box to the bottom right corner. Assisted by {player:Leandro Bacuna}.
Substitution, {team:Reading}. {player:Marc McNulty} replaces {player:Liam Kelly}.
Corner,  {team:Reading}. Conceded by {player:Jordan Spence}.
Substitution, {team:Ipswich Town}. {player:Danny Rowe} replaces {player:Freddie Sears}.
Foul by {player:Yakou Meite} ({team:Reading}).
{player:Cole Skuse} ({team:Ipswich Town}) wins a free kick in the defensive half.
Substitution, {team:Reading}. {player:John Swift} replaces {player:John O'Shea}.
Attempt missed. {player:Freddie Sears} ({team:Ipswich Town}) right footed shot from the right side of the box misses to the left. Assisted by {player:Kayden Jackson}.
Offside, {team:Ipswich Town}. {player:Luke Chambers} tries a through ball, but {player:Kayden Jackson} is caught offside.
Corner,  {team:Reading}. Conceded by {player:Freddie Sears}.
Delay over. They are ready to continue.
Delay in match {player:Cole Skuse} ({team:Ipswich Town}) because of an injury.
Foul by {player:Yakou Meite} ({team:Reading}).
{player:Matthew Pennington} ({team:Ipswich Town}) wins a free kick in the defensive half.
Corner,  {team:Ipswich Town}. Conceded by {player:John O'Shea}.
Attempt blocked. {player:Freddie Sears} ({team:Ipswich Town}) right footed shot from the left side of the box is blocked. Assisted by {player:Kayden Jackson}.
Attempt missed. {player:Leandro Bacuna} ({team:Reading}) right footed shot from outside the box misses to the left. Assisted by {player:Liam Kelly}.
Attempt blocked. {player:Yakou Meite} ({team:Reading}) left footed shot from the centre of the box is blocked. Assisted by {player:Modou Barrow}.
Substitution, {team:Ipswich Town}. {player:Kayden Jackson} replaces {player:Jordan Roberts}.
{player:Gwion Edwards} ({team:Ipswich Town}) wins a free kick in the defensive half.
Foul by {player:Leandro Bacuna} ({team:Reading}).
Attempt missed. {player:Garath McCleary} ({team:Reading}) left footed shot from outside the box is high and wide to the left following a corner.
Corner,  {team:Reading}. Conceded by {player:Jonas Knudsen}.
Attempt saved. {player:Modou Barrow} ({team:Reading}) left footed shot from the right side of the six yard box is saved in the bottom right corner. Assisted by {player:John O'Shea}.
Attempt blocked. {player:Yakou Meite} ({team:Reading}) header from the centre of the box is blocked.
Attempt missed. {player:Liam Moore} ({team:Reading}) header from the centre of the box is close, but misses to the left. Assisted by {player:Leandro Bacuna} with a cross following a corner.
Corner,  {team:Reading}. Conceded by {player:Freddie Sears}.
Foul by {player:Gwion Edwards} ({team:Ipswich Town}).
{player:Liam Kelly} ({team:Reading}) wins a free kick in the defensive half.