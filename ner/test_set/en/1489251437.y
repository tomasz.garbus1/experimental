::{team:Cardiff City} 1-1 {team:Birmingham City}::
@1489251437
url: https://www.bbc.com/sport/football/39162728
{player:Lukas Jutkiewicz}'s scrappy late equaliser earned {team:Birmingham City} a draw at {team:Cardiff City} to ease the pressure on beleaguered {player:Blues} boss {manager:Gianfranco Zola}. 
{player:Joe Ralls}' second-half penalty put the hosts ahead, after {player:Sean Morrison} had been fouled by {player:Ryan Shotton}.
{team:Birmingham} salvaged a point in the 89th minute as {player:Jutkiewicz} bundled in a rebound after his header was saved.
They stay 17th in the {league:Championship} table but now only six points clear of the relegation zone.
Despite {player:Jutkiewicz}'s late goal, {team:Bristol City}'s win at {team:Wigan} means {player:the Blues} look increasingly like a team who could get dragged into the battle to avoid the drop.
This match was a tale of two managerial appointments - each with wildly contrasting results.
{team:Birmingham} had won only twice in 17 games in all competitions since sacking {manager:Gary Rowett} in December, with {manager:Zola} overseeing a slump from seventh place to 17th in the {league:Championship} table.
Meanwhile, following {manager:Neil Warnock}'s arrival in October, {team:Cardiff} had climbed from second from bottom to 13th.
{team:The Bluebirds} had won four of their last six home games in the league - keeping a clean sheet in each victory - and they looked likely to improve that record against a Birmingham side low on confidence.
The visitors barely threatened, with one of their few forays into the opposition box ending with a yellow card for {player:Krystian Bielik} for diving.
{team:Cardiff} had to be patient and, after a forgettable first half, they were awarded a penalty when {player:Shotton} wrestled {player:Morrison} to the ground.
{player:Ralls} was less than convincing from the spot, but {player:Blues} keeper {player:Tomasz Kuszczak} could only palm his low effort into the roof of the net.
The home side were in control for the vast majority of the match, but let their guard slip late on as keeper {player:Alan McGregor} saved {player:Jutkiewicz}'s header, only for the ball to ricochet off the striker's shin and into the net.
{team:Cardiff City} manager {manager:Neil Warnock}: "I can't fault the effort. We put a lot of energy in the second half, but once again we had the weakness of conceding late on.
"If I'm honest I expected a bit more from {team:Birmingham} with all the money they've spent, but they were quite content to play for the draw.
"It was a game we should have won."
{team:Birmingham City} boss {manager:Gianfranco Zola}: "This was very encouraging because after such a poor performance there was a risk that the team could have had a bad reaction.
"But I didn't see that. I wanted to see the team fight and react to this bad moment, and I saw that. This is the thing which pleases me the most, and on personal terms it is very important as well.
"We need to be more consistent to get the momentum on our side because that is what has been lacking."
Match ends, {team:Cardiff City} 1, {team:Birmingham City} 1.
Second Half ends, {team:Cardiff City} 1, {team:Birmingham City} 1.
{player:Craig Gardner} ({team:Birmingham City}) is shown the yellow card.
Substitution, {team:Birmingham City}. {manager:Josh Cogley} replaces {player:Krystian Bielik} because of an injury.
Substitution, {team:Cardiff City}. {player:Kadeem Harris} replaces {player:Junior Hoilett}.
Delay over. They are ready to continue.
Delay in match {player:Krystian Bielik} ({team:Birmingham City}) because of an injury.
Attempt blocked. {player:Anthony Pilkington} ({team:Cardiff City}) right footed shot from outside the box is blocked. Assisted by {player:Junior Hoilett}.
Corner,  {team:Cardiff City}. Conceded by {player:Nsue}.
Goal!  {team:Cardiff City} 1, {team:Birmingham City} 1. {player:Lukas Jutkiewicz} ({team:Birmingham City}) with an attempt from very close range to the centre of the goal.
Attempt saved. {player:Lukas Jutkiewicz} ({team:Birmingham City}) header from very close range is saved in the centre of the goal. Assisted by {player:Che Adams} with a cross.
{player:Joe Ralls} ({team:Cardiff City}) is shown the yellow card for a bad foul.
Foul by {player:Joe Ralls} ({team:Cardiff City}).
{player:Kerim Frei} ({team:Birmingham City}) wins a free kick in the defensive half.
Attempt blocked. {player:Sean Morrison} ({team:Cardiff City}) header from the centre of the box is blocked. Assisted by {player:Peter Whittingham} with a cross.
{player:Kenneth Zohore} ({team:Cardiff City}) wins a free kick on the left wing.
Foul by {player:Nsue} ({team:Birmingham City}).
Attempt missed. {player:Lukas Jutkiewicz} ({team:Birmingham City}) header from the centre of the box is high and wide to the right. Assisted by {player:Maikel Kieftenbeld} with a cross.
Attempt missed. {player:Maikel Kieftenbeld} ({team:Birmingham City}) right footed shot from outside the box misses to the left.
Attempt saved. {player:Craig Gardner} ({team:Birmingham City}) right footed shot from outside the box is saved in the centre of the goal.
Foul by {player:Sol Bamba} ({team:Cardiff City}).
{player:Lukas Jutkiewicz} ({team:Birmingham City}) wins a free kick in the attacking half.
Corner,  {team:Birmingham City}. Conceded by {player:Aron Gunnarsson}.
Attempt blocked. {player:Che Adams} ({team:Birmingham City}) left footed shot from the left side of the box is blocked.
Offside, {team:Cardiff City}. {player:Aron Gunnarsson} tries a through ball, but {player:Sol Bamba} is caught offside.
Corner,  {team:Cardiff City}. Conceded by {player:Lukas Jutkiewicz}.
Corner,  {team:Cardiff City}. Conceded by {player:Tomasz Kuszczak}.
Attempt saved. {player:Kenneth Zohore} ({team:Cardiff City}) left footed shot from outside the box is saved in the bottom right corner. Assisted by {player:Peter Whittingham}.
{player:Jazz Richards} ({team:Cardiff City}) wins a free kick in the defensive half.
Foul by {player:Che Adams} ({team:Birmingham City}).
Foul by Joe Bennett ({team:Cardiff City}).
{player:Craig Gardner} ({team:Birmingham City}) wins a free kick in the defensive half.
Attempt blocked. {player:Aron Gunnarsson} ({team:Cardiff City}) right footed shot from outside the box is blocked. Assisted by {player:Joe Ralls}.
Attempt blocked. {player:Junior Hoilett} ({team:Cardiff City}) right footed shot from the centre of the box is blocked. Assisted by {player:Joe Ralls}.
Substitution, {team:Birmingham City}. {player:Kerim Frei} replaces {player:David Davis}.
Foul by {player:Lukas Jutkiewicz} ({team:Birmingham City}).
{player:Jazz Richards} ({team:Cardiff City}) wins a free kick in the defensive half.
Foul by {player:Che Adams} ({team:Birmingham City}).
{player:Aron Gunnarsson} ({team:Cardiff City}) wins a free kick in the defensive half.
Attempt saved. {player:Kenneth Zohore} ({team:Cardiff City}) right footed shot from outside the box is saved in the top right corner. Assisted by {player:Aron Gunnarsson}.