::Guernsey FC seal place in qualifying round with FA Cup win::
@1377977043
url: https://www.bbc.com/sport/football/23917135
Guernsey FC won their first ever FA Cup fixture beating Crawley Down Gatwick 3-1 to progress to the first qualifying round of the competition.
The Green Lions took an early lead with goals from Marc McGrath and man-of-the-match Matt Loaring.
The hosts, , got themselves back into the match with a penalty shortly before half-time. 
But Ross Allen sealed a famous win scoring a second-half penalty. 