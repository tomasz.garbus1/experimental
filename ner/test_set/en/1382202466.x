::Portsmouth 1-0 Bury::
@1382202466
url: https://www.bbc.com/sport/football/24503402
Bondz N'Gala's early header gave Portsmouth a narrow win over managerless Bury.
The giant centre-back rose highest to guide Ricky Holmes' corner into the net for his third goal of the season.
Bury, who sacked Kevin Blackwell on Monday, were the better side after the break, but Shaun Harrad's header was kept out by Phil Smith's fine save.
Danny Hylton and Anton Forrester both missed good chances for Bury before Jed Wallace hit the post for Portsmouth.
The defeat meant the Shakers, currently being led by caretaker boss Ronnie Jepson, are without a win in eight games and sit just above the relegation zone on goal difference.
Portsmouth manager Guy Whittingham told BBC Radio Solent:

                            
                        
"It's one of those victories that you don't want but you'll take. 
"I don't think there were many players who actually could say we had a good game on the ball, but we worked hard, we defended well and we've ultimately won the game.
"Teams often lift their game when a manager's gone, either to play for him or they're happy to see him go, and so either way there was going to be a reaction. 
"There was certainly a reaction from Bury; they were on top of us, they were lively, they were doing everything apart from scoring."
Bury caretaker manager Ronnie Jepson told BBC Radio Manchester:
"How we didn't get something out of the game, I'll never know.
"We were on the front foot from the off and you saw the sigh of relief from their staff afterwards. Guy Whittingham was almost a bit apologetic.
"This is a great place to come and they've got a good crowd behind them. Our boys showed fantastic spirit and it's just not dropping at this moment in time."
Match ends, Portsmouth 1, Bury 0.
Second Half ends, Portsmouth 1, Bury 0.
Attempt missed. Jordan Mustoe (Bury) right footed shot from outside the box is high and wide to the right.
Attempt missed. Danny Mayor (Bury) right footed shot from outside the box is close, but misses to the right.
Jed Wallace (Portsmouth) hits the right post with a right footed shot from the right side of the box.
Corner,  Bury. Conceded by Phil Smith.
Corner,  Bury. Conceded by Joe Devera.
Attempt missed. Shaun Beeley (Bury) right footed shot from outside the box is too high.
Sonny Bradley (Portsmouth) wins a free kick in the attacking half.
Foul by Anton Forrester (Bury).
Attempt missed. Anton Forrester (Bury) right footed shot from the right side of the box misses to the left.
Substitution, Portsmouth. Andy Barcham replaces Patrick Agyemang.
Substitution, Portsmouth. Gavin Mahon replaces Therry Racon.
Attempt missed. William Edjenguele (Bury) header from the centre of the box is too high.
Johannes Ertl (Portsmouth) wins a free kick in the defensive half.
Foul by Danny Hylton (Bury).
Substitution, Bury. Anton Forrester replaces Shaun Harrad.
Substitution, Bury. Danny Mayor replaces Tommy Miller.
Andrew Proctor (Bury) is shown the yellow card.
Johannes Ertl (Portsmouth) wins a free kick in the defensive half.
Foul by Andrew Proctor (Bury).
Therry Racon (Portsmouth) is shown the yellow card.
Therry Racon (Portsmouth) wins a free kick in the defensive half.
Foul by Nathan Cameron (Bury).
Corner,  Bury. Conceded by Sonny Bradley.
Substitution, Portsmouth. Ryan Bird replaces John Marquis.
Foul by Jed Wallace (Portsmouth).
Tommy Miller (Bury) wins a free kick on the left wing.
Therry Racon (Portsmouth) wins a free kick in the defensive half.
Foul by Nathan Cameron (Bury).
John Marquis (Portsmouth) is shown the yellow card.
Foul by John Marquis (Portsmouth).
Nathan Cameron (Bury) wins a free kick in the attacking half.
Corner,  Portsmouth. Conceded by William Edjenguele.
Attempt saved. Danny Hylton (Bury) right footed shot from the left side of the box is saved in the centre of the goal.
Patrick Agyemang (Portsmouth) wins a free kick in the defensive half.
Foul by Nathan Cameron (Bury).
Joe Devera (Portsmouth) wins a free kick in the defensive half.
Foul by Chris Sedgwick (Bury).
Yassin Moutaouakil (Portsmouth) wins a free kick in the defensive half.