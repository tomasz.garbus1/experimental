::Doncaster Rovers 7-0 Chorley: Alfie May scores four as Rover thrash non-league side::
@1542750106
url: https://www.bbc.com/sport/football/46189621

                            
                        
Five goals in the opening 36 minutes helped ensure Doncaster Rovers avoided an FA Cup first-round upset as they thrashed sixth-tier Chorley.
Alfie May scored a 29-minute hat-trick as the League One side had the tie sealed well before the interval - and the forward could have had a fourth just before the break but missed from close range.
Matty Blair and Herbie Kane - who hit an impressive equaliser in the first tie - scored in-between May's first and second goals as Rovers controlled the opening half.
Doncaster had Tom Anderson sent off for a second booking with 30 minutes left, but it was Rovers who continued to dominate.
John Marquis had a 73rd-minute penalty saved by Matt Unwin but May slotted in his fourth late on before Marquis made amends for his miss from 12 yards, scoring the seventh goal with an excellent curling effort.
Doncaster are away against fellow League One side Charlton Athletic in the second round.
Match ends, Doncaster Rovers 7, Chorley 0.
Second Half ends, Doncaster Rovers 7, Chorley 0.
Corner,  Chorley. Conceded by Matty Blair.
Attempt missed. Paul Taylor (Doncaster Rovers) right footed shot from the centre of the box misses to the left.
Attempt missed. Alfie May (Doncaster Rovers) header from the centre of the box is close, but misses to the left.
Corner,  Doncaster Rovers. Conceded by Stephen Jordan.
Goal!  Doncaster Rovers 7, Chorley 0. John Marquis (Doncaster Rovers) left footed shot from the right side of the box to the top left corner. Assisted by Matty Blair.
Goal!  Doncaster Rovers 6, Chorley 0. Alfie May (Doncaster Rovers) left footed shot from the centre of the box to the bottom right corner.
Foul by John Marquis (Doncaster Rovers).
Andrew Teague (Chorley) wins a free kick on the left wing.
Corner,  Doncaster Rovers. Conceded by Matthew Urwin.
Penalty saved! John Marquis (Doncaster Rovers) fails to capitalise on this great opportunity,  right footed shot saved  in the bottom right corner.
Penalty Doncaster Rovers. John Marquis draws a foul in the penalty area.
Penalty conceded by Matthew Urwin (Chorley) after a foul in the penalty area.
Substitution, Chorley. Reuben Noble-Lazarus replaces Josh Wilson.
Substitution, Doncaster Rovers. Ali Crawford replaces Herbie Kane.
Attempt missed. Paul Taylor (Doncaster Rovers) right footed shot from outside the box misses to the right.
Tommy Rowe (Doncaster Rovers) is shown the yellow card for a bad foul.
Attempt missed. Paul Taylor (Doncaster Rovers) right footed shot from outside the box is too high.
Attempt blocked. Andrew Teague (Chorley) right footed shot from outside the box is blocked.
Substitution, Chorley. Stephen Jordan replaces Scott Leather because of an injury.
Attempt missed. John Marquis (Doncaster Rovers) left footed shot from the left side of the box is too high.
Delay in match Scott Leather (Chorley) because of an injury.
Attempt blocked. Herbie Kane (Doncaster Rovers) right footed shot from long range on the left is blocked.
Alfie May (Doncaster Rovers) wins a free kick in the defensive half.
Foul by Scott Leather (Chorley).
Substitution, Chorley. Dale Whitham replaces Alex Newby.
Second yellow card to Tom Anderson (Doncaster Rovers) for a bad foul.
Foul by Tom Anderson (Doncaster Rovers).
Louis Almond (Chorley) wins a free kick on the right wing.
Attempt blocked. John Marquis (Doncaster Rovers) right footed shot from the centre of the box is blocked.
Attempt missed. Tommy Rowe (Doncaster Rovers) left footed shot from outside the box misses to the right.
Herbie Kane (Doncaster Rovers) wins a free kick in the defensive half.
Foul by Jake Cottrell (Chorley).
Tom Anderson (Doncaster Rovers) is shown the yellow card for a bad foul.
Foul by Tom Anderson (Doncaster Rovers).
Courtney Meppen-Walter (Chorley) wins a free kick in the defensive half.
Foul by Niall Mason (Doncaster Rovers).
Josh Wilson (Chorley) wins a free kick in the defensive half.
Attempt blocked. Andy Butler (Doncaster Rovers) right footed shot from the centre of the box is blocked.