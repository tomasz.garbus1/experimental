::Wrexham: On loan Daniel Bachmann out for three months::
@1412615286
url: https://www.bbc.com/sport/football/29509538
Wrexham keeper Daniel Bachmann says he is expected to be out for at least three months with a torn medial collateral ligament in his knee.
The Austrian Under-21 international, on loan from Stoke City, collided with the frame of the goal during the 2-1 defeat at Torquay.
Bachmann, 20, who signed for the Welsh club until the January transfer window confirmed the injury on Twitter.
"I'm going to work hard to be back playing as soon as possible," he said. 
Speaking before the extent of the injury was known, Wrexham manager Kevin Wilkin had hoped to be without Bachmann for "weeks" rather than months .
"It's disappointing for Dan but he'll bounce back," Wilkin said.
Bachmann's misfortune could provide an opportunity for understudy Andy Coughlin to win back his place in the starting line-up.
"I've no qualms about Andy being in there," said Wilkin.
"I know he'll do a good job because Cough's played a large part of last season.
"He's been itching to get his chance. He's fortunate enough to get it, albeit the circumstances he'll get it under are not ideal. 
"But Andy needs to get in there and show what it's all about now and keep that jersey."