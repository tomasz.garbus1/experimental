::Grzegorz Krychowiak odsunięty od składu PSG. Na treningu posprzeczał się z kolegą Piłka nożna - Sport.pl::
@2017-03-19 12:50
url: http://www.sport.pl/pilka/1,65084,21518521,grzegorz-krychowiak-odsuniety-od-skladu-psg-na-treningu-posprzeczal.html


Reprezentanta Polski zabrakło w kadrze meczowej paryżan na szlagier francuskiej ekstraklasy, w którym jego zespół zmierzy się z Olympique Lyon. Powodem absencji pomocnika ma być tylko i wyłącznie forma sportowa.

Sytuacji Krychowiaka nie poprawiło też zajście podczas poniedziałkowego treningu. Polak miał zetrzeć się ostro z Presnelem Kimpembe, co doprowadziło do ostrej kłótni pomiędzy graczami. Zawodników musieli odciągać od siebie koledzy z drużyny. - To pokazuje, jak zła atmosfera jest w klubie po porażce z Barceloną. Wszyscy są wyraźnie sfrustrowani - czytamy w "Le Parisien".

W sezonie 2016/2017 Krychowiak w PSG rozegrał tylko sześć spotkań w pełnym wymiarze czasowym. Polak przegrywa rywalizację o miejsce w składzie z Thiago Mottą, Blaisem Matuidim i Adrienem Rabiot, a plotki o jego odejściu przybierają z każdym miesiącem na sile. Usługami pomocnika zainteresowany jest Inter Mediolan, o czym informowała "La Gazzetta dello Sport".

Mecz PSG - Lyon w niedzielę o godz. 21. Paryżanie zajmują aktualnie 2. miejsce w ligowej tabeli i do prowadzącego Monaco tracą trzy punkty.








