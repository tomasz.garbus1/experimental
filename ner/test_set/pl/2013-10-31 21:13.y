::Właściciel {team:Zawiszy} chce adoptować piłkarza. Z radości po Legii [ZDJĘCIA] Piłka nożna - Sport.pl::
@2013-10-31 21:13


{manager:Radosław Osuch}, właściciel {team:Zawiszy}, wyściskał {player:Wahana Geworgiana} w korytarzu obok szatni i zawołał - Wan! Ja już piszę do urzędu. Chcę cię adoptować.

Żart wywołał wiele śmiechu. Z wygranej 3:1 cieszyło się 13 tys. kibiców, a wśród nich VIP-y: prezydenci Rafał Bruski i Jan Szopiński, były zastępca Sebastian Chmara, wiceprezes PZPN Eugeniusz Nowak.

Zaczęli oczywiście piłkarze - od razu po zakończeniu spotkania. Głównym bohaterem owacji był strzelec dwóch goli - {player:Geworgian}. Najbardziej zabawnie było, jak {player:Igor Lewczuk} zaczął naśladować, że ma kamerę w ręku i kręci sceny z {player:Geworgianem} - jak gwiazdą filmową.

Potem zawodnicy tradycyjnie już przybijali "piątki" z kibicami stojącymi wokół boiska. W szatni odśpiewali klubową pieśń zwycięstwa Umbaje belino.

Jesteś kibicem {team:Zawiszy}? Dołącz do nas na Facebooku Sport.pl Bydgoszcz






