::Piłkarze {team:Śląska}, {team:Zagłębia} i {team:Miedzi} powołani do reprezentacji {country:Polski} U-20 Piłka nożna - Sport.pl::
@2016-08-17 13:00


{player:Dankowski} w sezonie 2016/17 zaliczył dotychczas tylko dwa występy w barwach {team:Śląska}. Łącznie na boisku spędził 47 minut. Ostatni pojedynek wrocławian z {team:Arką Gdynia} rozpoczął w wyjściowym składzie. Zagrał słabo, sprokurował rzut karny i grę zakończył po 45 minutach.

Bez występów w nowych rozgrywkach jest jeszcze {player:Filip Jagiełło} z {team:Zagłębia Lubin}. Najwięcej spotkań w nowym sezonie zanotował {player:Rasak}. Pomocnik {team:Miedzi Legnica} zagrał już w czterech meczach, trzykrotnie wychodząc jako podstawowy zawodnik drużyny prowadzonej przez {manager:Ryszarda Tarasiewicza}.

Kadra trenera {manager:Macieja Stolarczyka} 1 września w Montreux zmierzy się z rówieśnikami z reprezentacji {country:Szwajcarii}. Z kolei 6 września biało-czerwoni zagrają w Świnoujściu z drużyną {country:Niemiec}. Zgrupowanie polskiego zespołu potrwa od 28 sierpnia do 7 września.

Kadra reprezentacji {country:Polski} U-20 na {league:Turniej Czterech Narodów}


{player:Jakub Bartosz} ({team:Wisła Kraków}), {player:Jan Bednarek} ({team:Lech Poznań}), {player:Kamil Dankowski} ({team:Śląsk Wrocław}), {player:Filip Jagiełło} ({team:Zagłębie Lubin}), {player:Robert Janicki} ({team:Hoffenheim}), {player:Mateusz Kuchta} ({team:Górnik Zabrze}), {player:Rafał Makowski} ({team:Pogoń Siedlce}), {player:Konrad Michalak}, {player:Mateusz Wieteska} (obaj {team:Legia Warszawa}), {player:Jakub Piotrowski} ({team:Pogoń Szczecin}), {player:Adrian Purzycki} ({team:Wigan Athletic}), {player:Damian Rasak} ({team:Miedź Legnica}), {player:Oktawian Skrzecz} ({team:Schalke 04}), {player:Paweł Stolarski} ({team:Lechia Gdańsk}), {player:Karol Świderski}, {player:Damian Węglarz} (obaj {team:Jagiellonia Białystok}), {player:Szymon Walczak} ({team:1. FC Köln}), {player:Mateusz Wdowiak} ({team:Cracovia}), {player:Bartosz Wolski} ({team:Latina Calcio}), {player:Oskar Zawada} ({team:VfL Wolfsburg})






