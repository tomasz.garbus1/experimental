::{player:Rzeźniczak} nie zatrzymał {team:Arsenalu}, {team:Milan} bez wpadki w {team:Pireusie} Piłka nożna - Sport.pl::
@2018-10-04 20:53

{player:Jakub Rzeźniczak} zagrał cały mecz w defensywie {team:Karabachu Agdam}, ale jego zespół nie sprawił sensacji w meczu drugiej kolejki {league:Ligi Europy} z {team:Arsenalem}. {team:Londyńczycy} wygrali 3:0. Pierwszego gola strzelili już w trzeciej minucie, dwa trafienia dołożyli po przerwie. To druga porażka drużyny {player:Rzeźniczaka} w tym sezonie {league:LE}.
REKLAMA




W {league:Lidze Europy} zagrali też {player:Jacek Góralski} i {player:Jakub Świerczok}. Obaj weszli po przerwie w {team:meczu Łudogorca Razgrad} z {team:FC Zurich} przy 0:0, ale ich zespół przegrał na wyjeździe ze Szwajcarami 0:1.{team:Anderlecht} przegrał 0:2 z {team:Dinamem Zagrzeb}, a cały mecz na ławce chorwackiej drużyny przesiedział {player:Damian Kądzior}. {player:Patryk Małecki} też nie podniósł się z ławki rezerwowych w meczu {team:Spartaka Trnava} z {team:Fenerbahce}, a jego klub przegrał na wyjeździe 0:2.{player:Igor Lewczuk} nie znalazł się w kadrze meczowej {team:Bordeaux} na mecz z FC. Klub Polaka przegrał 1:2 z Duńczykami.

{player:Tomasz Kędziora} grał od pierwszej minuty w barwach {team:Dynama Kijów} w meczu z {team:FK Jablonec}. Polak pechowo poślizgnął się w 81. minucie w polu karnym i pozostawił napastnika gospodarzy bez opieki. Rywal szansy nie zmarnował i doprowadził do remisu 2:2.W najciekawszym meczu {team:Milan} przegrywał 0:1 w {team:Pireusie} do 70. minuty, ale zwyciężył 3:1, strzelając trzy gole w dziewięć minut.{team:Chelsea} tylko 1:0 pokonała słabiutkie {team:MOL Vidi FC}. Zwycięską bramkę w 70. minucie strzelił {player:Alvaro Morata}. Z kolei {team:Lazio} kończyło mecz w dziewiątkę po dwóch czerwonych kartkach i przegrało 1:4 z {team:Eintrachtem Frankfurt}.{player:Arkadiusz Milik} okradziony. Żartobliwy wpis polskiego piłkarza

Wyniki meczów 2. kolejki {league:Ligi Europy}:{team:Anderlecht} - {team:Dinamo Zagrzeb} 0:2 (0:1){team:Bayer Leverkusen} - {team:AEK Larnaka} 4:2 (1:1){team:Bordeaux} - {team:FC Kopenhaga} 1:2 (0:1){team:FC Zurich} - {team:Łudogorec Razgrad} 1:0 (0:0)

{team:Fenerbahce} - {team:Spartak Trnava} 2:0 (0:0){team:Milan} - {team:Olympiakos Pireus} 3:1 (0:1){team:Karabach} - {team:Arsenal} 0:3 (0:1){team:Real Betis} - {team:Dudelange} 3:0 (0:0)

{team:Red Bull Salzburg} - {team:Celtic} 3:1 (0:1){team:Rosenborg} - {team:RB Lipsk} 1:3 (0:1){team:Worskla Połtawa} - {team:Sporting} 1:2 (1:0){team:Zenit} - {team:Slavia} 1:0 (0:0)

{team:FC Astana} - {team:Rennes} 2:0 (0:0){team:Apollon} - {team:Marsylia} 2:2 (0:0){team:BATE Borysów} - {team:PAOK Saloniki} 1:4 (0:3){team:Chelsea} - {team:MOL Vidi FC} 1:0 (0:0)

{team:Eintracht Frankfurt} - {team:Lazio} 4:1 (2:1){team:Krasnodar} - {team:Sevilla} 2:1 (0:1){team:Glasgow} - {team:Rapid} 3:1 (1:1){team:FK Jablonec} - {team:Dynamo Kijów} 2:2 (1:2)

{team:Malmoe} - {team:Besiktas} 2:0 (0:0){team:Sarpsborg} - {team:Genk} 3:1 (1:0){team:Spartak Moskwa} - {team:Villarreal} 3:3 (1:1){team:Standard Liege} - {team:Akhisarspor} 2:1 (2:1)

{player:Ludovic Obraniak} zakończył karierę





