::Niespodzianka w Bydgoszczy. Lech przegrywa z Zawiszą Piłka nożna - Sport.pl::
@2015-03-14 19:52
url: http://www.sport.pl/pilka/1,133416,17570790,Niespodzianka_w_Bydgoszczy__Lech_przegrywa_z_Zawisza.html


Zespół z Poznania nie będzie dobrze wspominał pierwszej połowy spotkania. Dotknął go pech i kara od arbitra. Już w początkowych minutach spotkania urazu doznał stoper Paulus Arajuuri. Fin grał jeszcze potem, ale ból był coraz silniejszy. W 24 min zastąpił go Węgier Tomas Kadar. 60 sekund później Lech doznał znacznie większej straty. Arnaud Djoum, który w środku pola zagrał za Łukasza Trałkę pauzującego za żółte kartki faulował Alvarinho. Belg urodzony w Kamerunie zobaczył drugą żółtą kartkę - i to w ciągu 4 minut. Wcześniej był ukarany za faul na Kamilu Drygasie. Djoum był jedynym nowynm graczem w jedenastce Lecha w porównaniu z poprzednim spotkaniem. Trener bydgoszczan Mariusz Rumak nie zmienił żadnego z piłkarzy, którzy wystąpili w zwycięskim meczu z Wisłą Kraków.

Zawisza grając z przewagą zawodnika od razu zdobył przewagę. Wcześniej Lech miał kilka niezłych akcji. W 18 min Zaur Sadajew mijał Andre Micale, ale dośrodkowanie z końcowej linii wyłapał Grzegorz Sandomierski. Niepokonany w tym roku bramkarz Zawiszy obronił minutę później strzał Szymona Pawłowskiego.

REKORD POBILI KIBICE I PIŁKARZE ZAWISZY

W 26 min Alvarinho miał dobrą okazję, ale pierwsze jego uderzenie zablokowali obrońcy, a drugie obronił Maciej Gostomski. Bramka dla gospodarzy padla po rzucie rożny. To był już 121 róg w sezonie i pierwszy gol dla zawiszan po tym stałym fragmencie gry. Po centrze Alvarinho, piłkę zgrał pod bramkę Andre Micael, a Josip Barisić głową z 3 m strzelił gola.

W 36 min mogło być 1:1. Marcin Kamiński miał znakomitą okazję, strzelał głową po centrze Gergo Lovrencicsa. Sandomierski znowu miał szczęście - odprowadzał tylko wzrokiem piłkę. Minęła o centymetry słupek.

Od razu w przerwie Rumak zmienił Drygasa. Pomocnik Zawiszy zobaczył bowiem żółtą kartkę, jedenastą w sezonie. Trener bał się czerwonej i wprowadził za niego Cornela Predescu.

Zadaniem bydgoszczan w drugiej połowie było szanowanie piłki i zwiększanie czasu jej posiadania - w pierwszej części meczu było 54:46 dla Zawiszy. Tempo spadło. Gospodarze częściej dzielili się piłką w środku pola niż pchali ją do przodu, pod bramkę Gostomskiego. Grą Zawiszy jak profesor kierował Iwan Majewski, Akurat w sobotę po raz pierwszy dostał powołanie do reprezentacji Białorusi - to efekt jego bardzo dobrej postawy w tym roku.

Lechici próbowali kontrować i szukali szansy w stałych fragmentach gry. Emocje przyszły w samej końcówce. Sandomierskiemu znowu dopisało szczęście, kiedy w 84 min Lovrencics uderzył z 20 metrów. Piłka odbiła się od poprzeczki. Po chwili bydgoszczanie mieli kontrę, ale strzał Gieorgiego Alawerdeszwilego wybił z bramki obrońca.

- Cieszymy się i myślimy już o Ruchu - mówi skrzydłowy bydgoszczan Sebastian Kamiński. Zawisza w pięciu meczach w tym roku zdobył 11 pkt. Po 19 spotkaniach rundy jesiennej miał ich 9.

SKORŻA I RUMAK O MECZU [WIDEO]






