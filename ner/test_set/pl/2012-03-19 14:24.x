::Gryf Wejherowo nie wygra walkowerem meczu z Legią Warszawa. PZPN tłumaczy dlaczego Piłka nożna - Sport.pl::
@2012-03-19 14:24
url: http://www.sport.pl/pilka/1,65045,11372264,Gryf_Wejherowo_nie_wygra_walkowerem_meczu_z_Legia.html


POLECAMY - POLSKIE STADIONY NA EURO 2012 NAJPIĘKNIEJSZE? ZOBACZ JAKIE CACKA POWSTAJĄ W EUROPIE [ZDJĘCIA]

Gryf, choć gra zaledwie w III lidze, jest rewelacją tej edycji PP. Wyeliminował już m.in. Koronę Kielce i Górnika Zabrze i w nagrodę w ćwierćfinale dostał dwumecz z liderem ekstraklasy Legią.

Pierwsze spotkanie w Wejherowie piłkarze ze stolicy wygrali łatwo 3:0, ale zaraz po meczu Gryf złożył protest do PZPN. W protokole z tego spotkania warszawska drużyna umieściła bowiem w wyjściowym składzie Jakuba Rzeźniczaka, który w ogóle nie pojawił się na boisku. Zamiast niego zagrał (do 67 minuty) Artur Jędrzejczyk, który wpisany był jako rezerwowy.

Prezes i piłkarze z Wejherowa zarzekali się, że nie chcą walkowera, ale protest złożyli i po cichu liczyli, że może zostanie rozpatrzony pozytywnie i we wtorek (godz. 20.30) w Warszawie to oni będą bronić trzybramkowej przewagi.

Sprawą zajął się w poniedziałek Wydział Gier PZPN. Uznał, że protest Gryfa jest bezzasadny i utrzymał wynik 3:0 dla Legii.

W komunikacje PZPN tak tłumaczy swoją decyzję: "Po analizie materiału zgromadzonego w trakcie postępowania, w szczególności sprawozdania sędziego, Wydział Gier PZPN uznał, że Artur Jędrzejczyk był uprawniony do gry w meczu 1/4 finału Pucharu Polski. Klub KP Legia Warszawa S.S.A. mógł dokonać zmiany zawodnika, który nie był zdolny do rozpoczęcia zawodów, pomimo że był wpisany jako zawodnik podstawowy i mógł zostać zastąpiony przez jednego z zawodników rezerwowych. Zgodnie z przepisami w takiej sytuacji zmniejszeniu ulega liczba zawodników rezerwowych, a podczas meczu klub może nadal dokonywać, określonej w regulaminie, liczby zmian zawodników".

Jesteś kibicem z Trójmiasta? - dołącz do nas na Fejsie i bądź na bieżąco! »

ZOBACZ KONIECZNIE! GORTAT, LIMUZYNY, MECZE NBA - CO ZA ZDJĘCIA SEKSOWNYCH CHEERLEADEREK Z GDYNI Z ICH WYJAZDU DO USA!






