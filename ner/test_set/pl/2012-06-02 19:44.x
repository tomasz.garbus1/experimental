::Stec: 'Idylla przed Euro 2012' Reprezentacja Polski::
@2012-06-02 19:44
url: http://www.sport.pl/pilka/1,65037,11851698,Stec___Idylla_przed_Euro_2012_.html


"Od 462 minut piłkarze Franciszka Smudy nie stracili gola, zupełnie niepostrzeżenie bijąc rekord absolutny reprezentacji Polski, i są jedynymi obok Portugalii (podejmuje wieczorem Turcję) finalistami mistrzostw Europy, którzy w tym roku kalendarzowym zdołali zachować czyste konto. Z czego oczywiście nie wolno wyciągać żadnych wniosków, zwłaszcza po towarzyskiej wprawce tak jednostronnej, że Wojtek Szczęsny o mało nie skamieniał z bezczynności.

Jeśli drobną statystykę bez znaczenia przywołuję, to dlatego, że wkomponowuje się w idylliczny nastrój, w jaki w przededniu Euro 2012 wpadają powoli wszyscy." - pisze Stec.

Euro 2012. Polański: ważne, że wszyscy zdrowi. Teraz wygrać pierwszy mecz!

"Trener Smuda ma pod sobą każdego, kogo naprawdę chciał, a to przecież w tym ostro kontaktowym sporcie nieczęste - wystarczy przypomnieć sobie poprzedni turniej, przed którym reprezentację okaleczył uraz Błaszczykowskiego.

Właśnie rzut oka w przeszłość uświadamia, jak zrobiło się sielankowo.

Tuż przed mundialem w 2002 roku mieliśmy świeżo wdrukowane w pamięć sparingowe 0:2 z Japonią i 1:2 z Rumunią (obie wpadki u siebie), przed następnym mistrzostwami świata - 1:2 z Kolumbią i 0:1 z Litwą (też u siebie), przed Euro 2008 - 0:3 ze Stanami Zjednoczonymi (znów u siebie). W ogóle powietrze przed każdą nowożytną imprezą było co najmniej podtruta. Każdy kolekcjoner w poprzedzających ją miesiącach dawał powody, by kibice zapomnieli o pomyślnej kampanii eliminacyjnej, bo przesłaniały ją coraz mocniej rzucające się w oczy przywary i mnożące się błędy Jerzego Engela, Pawła Janasa, Leo Beenhakkera" - zauważa dziennikarz.

Cały wpis Rafała Steca przeczytasz na blogu " A jednak się kręci".

Polska biało-czerwoni: Wygraj Xboxa!

Pierwszy mecz z Grecją już za sześć dni. Dyskutuj o reprezentacji Polski już teraz!






