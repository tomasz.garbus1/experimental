::{league:Ekstraklasa} w Sport.pl. {player:Adam Mójta} - 'Najlepsza lewa noga w lidze' Piłka nożna - Sport.pl::
@2016-03-08 10:22


{player:Adam Mójta} strzelił już cztery gole i zaliczył pięć asyst. Zdobył najwięcej bramek ze wszystkich lewych obrońców w {league:ekstraklasie}, a więcej asyst na swoim koncie ma tylko {player:Patrik Mraz}. {player:Jakub Wawrzyniak}, który ma realne szanse na wyjazd na {league:Euro 2016} we {country:Francji} w tych rozgrywkach nie strzelił żadnego gola i zaliczył tylko trzy asysty. Zawodnik {team:Podbeskidzia} swoimi statystykami bije go na głowę.

Dla większości kibiców w {country:Polsce} jest jednak zawodnikiem anonimowym. Nigdy nie grał w wielkich klubach, prawdziwą szansę w {league:ekstraklasie} dostał w wieku 28 lat, znany jest głównie z internetowej zabawy w "Wygraj Ligę". Przed ostatnią ligową kolejką lewego obrońcę {team:Podbeskidzia} kupiło 2029 graczy. Prawie co czwarty zawodnik ma {player:Mójtę} w swoim składzie. Wszystko dzięki jego ofensywnej grze i świetnie ułożonej lewej nodze, która dostarcza w "Wygraj Ligę" pokaźną liczbę punktów.

Skąd więc bierze się tak ofensywna gra 29-letniego obrońcy? - Całe życie grałem na lewej obronie, ale od początku ciągnęło mnie do przodu. Zawsze starałem się pomagać swoim drużynom w ofensywie. Nie wszystkim trenerom się to podobało, ale postawiłem na swoim i teraz zbieram tego efekty - mówi {player:Mójta} w rozmowie ze Sport.pl.

Trudne początki u {manager:Kubickiego}


Do Bielska-Białej przychodził ratować sytuację po nieudanym transferze {player:Roberta Mazana}. {team:Podbeskidzie} wydało na młodzieżowego reprezentanta {country:Słowacji} 150 tysięcy euro (najwięcej w historii klubu) i było przekonane, że lewą stronę obrony ma zabezpieczoną na lata. Jednak {player:Mazan} w {country:Polsce} kompletnie zawiódł, postanowiono więc dać szansę sprowadzonemu latem {player:Mójcie}.

Już w pierwszym meczu z {team:Zagłębiem Lubin} wywalczył miejsce w podstawowej jedenastce. Nie zagrał źle, a bielszczanie przywieźli z trudnego terenu jeden punkt. Kolejny mecz i wyjazd na Łazienkowską, gdzie {team:Podbeskidzie} do przerwy przegrywało z {team:Legią} 0:3. Cała drużyna z Bielska-Białej grała katastrofalnie, ale to właśnie {player:Mójta} został kozłem ofiarnym tej katastrofy. Trener {manager:Dariusz Kubicki} odsunął go od występów w ekstraklasie i lewy obrońca musiał grać w trzecioligowych rezerwach.

{manager:Podoliński} uwierzył


Na ekstraklasowe boiska wrócił w siódmej kolejce i z miejsca stał się jedną z ważniejszych postaci w {team:Podbeskidziu}. Imponował w ofensywie, a coraz większe zrozumienie z kolegami z defensywy zaowocowało mniejszą liczbą traconych bramek. Jego najlepszy czas w karierze zaczął się dokładnie w momencie przyjścia do Bielska-Białej nowego trenera. Nieprzypadkowo swojego pierwszego gola {player:Mójta} zdobył właśnie w ekstraklasowym debiucie {manager:Roberta Podolińskiego} w {team:Podbeskidziu}.

- Trener nie musiał mnie namawiać do akcji ofensywnych. Ma do mnie pełne zaufanie. Oczywiście nie mogę zapominać o grze obronnej, ale jak tylko pojawia się szansa, to mam jak najczęściej włączać się do ataku - mówi 29-latek.

Przypadek? Nie sądzę


- Cieszę się ze swojej skuteczności, bo wszystkie bramki, które zdobyłem w tym sezonie były po przemyślanych strzałach. W żadnej sytuacji nie chciałem dośrodkowywać. W meczu z {team:Górnikiem Łęczną} widziałem, że w polu karnym nie było żadnego kolegi i strzeliłem z ostrego kąta. Bramkarz pomógł i wpadło. Z {team:Jagiellonią} i {team:Górnikiem Zabrze} też chciałem strzelać - zaznacza {player:Mójta}.

Te słowa to odpowiedź na zarzuty kierowane w stronę lewego obrońcy {team:Podbeskidzia}. Część kibiców i ekspertów zarzucało {player:Mójcie}, że jego nieprawdopodobne uderzenia z lewej nogi nie były zamierzone. Tym bardziej, że jego bramki wpadały w kluczowych momentach i pieczętowały zwycięstwa {team:Podbeskidzia}. Kibicom nie mieściło się w głowie, że lewy obrońca w ostatniej minucie meczu rusza z całych sił do przodu, a nie skupia się na defensywie. Gra na czas i wybijanie piłki po autach? - To nie w moim stylu - zapewnia {player:Mójta}.

W {country:Czechach} nie miał za co żyć


O 29-letnim obrońcy długo mówiło się, że ekstraklasa nie jest dla niego. Próbował przebić się w {team:Koronie} i {team:Odrze Wodzisław} - bezskutecznie. Zniechęcony postanowił wyjechać do {country:Czech}, gdzie w barwach {team:Viktorii Żiżkov} zagrał w 23 meczach. To właśnie w {country:Czechach} poczuł, że jego kariera zaczyna zmierzać w dobrym kierunku, ale z gry w drugiej lidze nie był w stanie się utrzymać w Pradze.

- {country:Czechy} to świetne miejsce do życia. Żiżkov to dzielnica Pragi i do starego rynku szło się jedynie 10 minut. To był jeden z najlepszych okresów w mojej przygodzie z piłką. Zagrałem 23 spotkania i miałem 17 asyst. Zostałem wybrany do jedenastki ligi, ale nie mogłem zostać tam dłużej. Przez 7 miesięcy nie dostawałem pensji i brakowało mi środków do życia - wspomina.

{player:Mójta} w {country:Polsce} znów zderzył się ze ścianą. Na prawie trzy lata utknął w pierwszej lidze, a poważną szansę dostał dopiero w Bełchatowie. - Nie żałuję, że tak późno trafiłem do {league:ekstraklasy}. Kiedy zaczynałem swoją przygodę z piłką kluby w najwyższej lidze nie stawiały na młodych zawodników. Musiałem szukać swojej szansy w niższych ligach, może też po prostu byłem za słaby - zastanawia się {player:Mójta}.

Środowisko docenia


{manager:Czesław Michniewicz}, trener {team:Pogoni Szczecin} napisał niedawno na Twitterze, że {player:Adam Mójta} ma najbardziej wartościową lewą nogę w lidze. Patrząc na statystki piłkarza {team:Podbeskidzia}, trudno się z tym stwierdzeniem nie zgodzić. Słowa {manager:Michniewicza} nie są jednak odosobnione. {player:Jakub Rzeźniczak} zapytany o zawodnika miesiąca wskazał swojego kolegę z zespołu - {player:Michała Kucharczyka} i właśnie {player:Mójtę}.

@_Ekstraklasa_ dla mnie albo Kuchy albo {player:Adam Mójta}.— {player:Jakub Rzeźniczak} (@JakubRzezniczak) 4 marca 2016

Dobrą formę lewego obrońcy {team:Podbeskidzia} docenili również dziennikarze, a porównanie go do {player:Leightona Bainesa} z {team:Evertonu} nie mogło być przypadkowe.

{player:Adam Mójta} strzela z pozycji, z których inni boją się podawać #PODGKŁ— Krótka Piłka (@krotkapilkatv) 1 marca 2016

{team:Podbeskidzie} - {team:Górnik Łęczna} pobudza wyobraźnię. - {player:Adam Mójta} to taki polski {player:Leighton Baines} - rzucił przed momentem ktoś w redakcji :)— Dominik Piechota (@dominikpiechota) 1 marca 2016

Nie będzie narzekał na brak ofert


Kontrakt {player:Adama Mójty} z {team:Podbeskidziem} kończy się w czerwcu, ale umowa po rozegraniu odpowiedniej liczby meczów może zostać przedłużona o kolejny rok. To nie przeszkadza jednak innym klubom, które w lecie chciałyby sprowadzić lewego obrońcę {team:Podbeskidzia}.

- W ostatnim czasie zrobiło się głośno o mojej grze, ale muszę zachować spokój. Teraz skupiam się na walce dla {team:Podbeskidzia}. Do wszystkiego doszedłem ciężką pracą, nikt mi niczego za darmo nie dał. Wiem, że tylko dobrą grą mogę zrobić kolejny krok do przodu - kończy {player:Mójta}.

Obserwuj @dwardzich22






