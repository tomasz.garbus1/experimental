::Puchar Polski. Cracovia wygrała z GKS-em w Katowicach Piłka nożna - Sport.pl::
@2015-09-22 19:37
url: http://www.sport.pl/pilka/1,65045,18874876,puchar-polski-cracovia-wygrala-z-gks-em-w-katowicach.html


Ta wygrana ma być lekarstwem na ostatnie kłopoty Cracovii w ekstraklasie. Po czterech z rzędu meczach bez zwycięstwa krakowianie liczyli na przełamanie. Styl nie rzucił na kolana, ale najważniejsze, że wynik był inny niż ostatnio w lidze.

Trochę różnił się też skład, choć wystarczyło rzucić na niego okiem, by dojść do wniosku, że Cracovia wcale nie traktuje tych rozgrywek po macoszemu. Nie było rewolucji, tylko kosmetyczne zmiany. Przede wszystkim przemeblowano obronę. Brakowało Sretena Sretenovicia, który był w tym czasie w drodze powrotnej z Belgradu (kilka dni wcześniej narodziła mu się córka i w klubie dostał wolne), więc parę stoperów awaryjnie stworzyli Bartosz Rymaniak i Piotr Polczak. Dla drugiego z nich był to sentymentalny powrót - w wieku 18 lat zadebiutował w ekstraklasie jako piłkarz... GKS-u Katowice.

Nieoczekiwanie na boisku pojawił się też Paweł Jaroszyński - miał usiąść na ławce, ale w ostatniej chwili, na rozgrzewce, kontuzji kolana doznał Hubert Wołąkiewicz. Dla 21-letniego obrońcy to mogła być duża szansa, bo w tym sezonie grywał tylko w trzecioligowych rezerwach. Czasami bywał niepewny, ale pomagał z przodu i po meczu dostał pochwały od trenera.

Posklejana naprędce defensywa zrobiła swoje, ale na sumieniu ma kilka błędów. Mogło zacząć się od tragedii. Gdyby Adrian Frańczak nie uderzył wprost w Grzegorza Sandomierskiego, goście już na początku meczu straciliby bramkę. A w końcówce przez chwilę nerwowo było tylko dlatego, że Rymaniak nieodpowiedzialnie sfaulował rywala w polu karnym.

Wcześniej przez długi czas wszystko utrzymywało się w normie. Cracovia prowadziła grę, a dwie bramki zdobyła według doskonale znanego scenariusza. Mateusz Cetnarski wrzucił piłkę na głowę Miroslava Covili, który w powietrzu nie miał sobie równych. W pierwszej połowie Serb trafił do siatki, w drugiej jego strzał obronił Rafał Dobroliński, ale dobitka Erika Jendriszka to było już za dużo.

Cracovia zrobiła swoje, choć momentami przypominała drużynę znudzoną. W pierwszej połowie w ślamazarnym tempie rozgrywała piłkę, ale GKS nie robił wiele, by uprzykrzyć jej życie. Jeśli nawet krakowianie w bezsensowny sposób tracili piłkę (zdarzało się!), to zaraz ją odzyskiwali, bo rywalowi brakowało pomysłu.

Większe emocje niż niektóre akcje budziły poszukiwania złotego pierścionka zaręczynowego. "Dziękujemy znalazcy! Uczciwość ponad wszystko" - podsumował je spiker.

Do gry wrócił Deniss Rakels, ale pokazał tylko kilka niecelnych strzałów. Łotysz przez dwa lata grał w GKS-ie i przed meczem nie ukrywał, że nie może się doczekać powrotu. Tym bardziej że ostatnio zabrakło go nawet w 18-osobowej kadrze na mecz ligowy z Zagłębiem Lubin. W Katowicach mógł pokazać, że w sprawie jego formy nie ma powodów do obaw, ale kiedy już doszedł do znakomitej okazji, kopnął w siatkę zabezpieczającą, a wściekły trener Jacek Zieliński prawie wskoczył na murawę.

Denerwował się może dlatego, że dla jego piłkarzy awans był obowiązkiem. A kiedy Grzegorz Goncerz pewnie wykorzystał rzut karny, na trybunach zrobiło się głośno, ale gospodarze nie poszli za ciosem. Zamiast tego trzeciego gola zdobyła Cracovia. Wydawało się, że Boubacar Diabang nie ma prawa trafić do siatki z ostrego kąta, ale piłka odbiła się od nogi Adama Czerwińskiego i zaskoczyła bramkarza.

W ćwierćfinale Cracovia jeszcze w tym roku zagra z kolejnym pierwszoligowcem - Zagłębiem Sosnowiec.

GKS Katowice - Cracovia 1:3 (0:1)

Bramki: Covilo (19.), Jendriszek (70.), Czerwiński (85., sam.) - Goncerz (79., k.)

GKS: Dobroliński - Czerwiński Ż, Kamiński, Prażnovski, Pietrzak - Pielorz (76. Burkhardt), Leimonas - Bębenek, Iwan (68. Trochim), Frańczak - Goncerz Ż

Cracovia: Sandomierski - Deleu, Rymaniak, Polczak, Wołąkiewicz - Covilo, Dabrowski - Rakels (76. Diabang), Cetnarski, Kapustka (84. Wdowiak) - Jendriszek (90. Szewczyk)






