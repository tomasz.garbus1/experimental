::{manager:Czesław Michniewicz} odkrył karty! Są powołania na mecze z {country:Rosją} i {country:Serbią} Reprezentacja {country:Polski}::
@2019-10-01 11:43

Reprezentacja {country:Polski} U-21 wygrała dwa pierwsze mecze eliminacji {league:Euro U-21} w 2021 roku i jest liderem grupy E. Kadra {manager:Czesława Michniewicza} najpierw pokonała na wyjeździe {country:Łotwę} 1:0, a później w Białymstoku rozbiła {country:Estonię} 4:0. Teraz poprzeczka idzie w górę. Październikowymi rywalami biało-czerwonych będą Rosjanie i Serbowie. Tak prezentuje się kadra {manager:Michniewicza} na te spotkania:
REKLAMA




Piłkarze mają zarzuty do {player:Brzęczka}! O co? Odpowiedź w tym materiale wideo:

Bramkarze: {player:Kamil Grabara}, {player:Marcin Bułka}, {player:Karol Niemczycki} Obrońcy: {player:Maciej Ambrosiewicz}, {player:Karol Fila}, {player:Robert Gumny}, {player:Kamil Pestka}, {player:Jan Sobociński}, {player:Sebastian Walukiewicz}, {player:Jan Wiśniewski} Pomocnicy: {player:Mateusz Bogusz}, {player:Patryk Dziczek}, {player:Kamil Jóźwiak}, {player:Jakub Moder}, {player:Marcin Listkowski}, {player:Sylwester Lusiusz}, {player:Przemysław Płacheta}, {player:Daniel Ściślak}, {player:Kamil Wojtkowski} Napastnik: {player:Bartosz Bida}, {player:Patryk Klimala}, {player:Paweł Tomczyk}, {player:Patryk Szysz} Mecz z {country:Rosją} Polacy zagrają w Jekaterynburgu. Początek 11.10 o godz. 16. Spotkanie z {country:Serbią} zostanie rozegrane 15.10 o godz. 18  na stadionie Widzewa w Łodzi. Rywalami Polaków w grupie są reprezentacje: {country:Serbii}, {country:Rosji}, {country:Bułgarii}, {country:Łotwy} i {country:Estonii}. Do turnieju głównego bezpośrednio awansuje tylko zwycięzca grupy. Drugi zespół zagra w barażu. Turniej {league:Euro U-21} w 2021 roku zorganizują {country:Słowacja} i {country:Węgry}.Pobierz aplikację Football LIVE na AndroidaAplikacja Football LIVE Sport.pl





