::Dariusz Wdowczyk: Byliśmy tylko tłem dla Wisły Piłka nożna - Sport.pl::
@2014-05-03 23:04
url: http://www.sport.pl/pilka/1,128220,15897387,Dariusz_Wdowczyk__Bylismy_tylko_tlem_dla_Wisly.html


Pomeczowe opinie szkoleniowców.

Franciszek Smuda, trener Wisły:


Najważniejsze, że przełamaliśmy złą passę. Już wcześniej powtarzałem, że przy tylu brakach niełatwo nam zwyciężać. Ta wygrana dla wszystkich jest dodatkową motywacją przed kolejnym spotkaniem. Tomasz Frankowski kiedyś też miał taki okres, że nie trafiał, a później worek rozwiązał się i strzelał regularnie. Tak samo z jest z Pawłem Brożkiem [w sobotę zaliczył hat tricka - red.]. Ani chwili nie wahałem się, by po słabszych występach posadzić go na ławce, bo on... jest sam w linii napadu. Modliłem się jedynie do Boga, aby był zdrowy.

Dariusz Wdowczyk, trener Pogoni:


Kubeł zimnej wody na nasze głowy. Gratuluję trenerowi Smudzie, bo wygrał bardzo zasłużenie. Nic nam się nie układało. Wisła grała bardzo dobrze, byliśmy tłem dla rywala. To pierwszy taki przypadek w 32 kolejkach. To nie zdarzyło nam się w tym sezonie ani razu. Wisła była bardzo dobrze dysponowana, my bardzo słabo. Popełniliśmy masę błędów w defensywie. Być może nie popełniliśmy ich tyle w ciągu całego sezonu. Ale to nie tylko obrona, bo graliśmy słabo całą drużyną. Dla nas to bardzo bolesne, ale musimy przeanalizować fakty, wyciągnąć wnioski i przygotować się do następnego meczu.






