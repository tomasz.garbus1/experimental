::Typujemy skład Arki na inaugurację wiosny. Cz. 3: Atak [SONDAŻ] Piłka nożna - Sport.pl::
@2016-03-02 12:17
url: http://www.sport.pl/pilka/1,65044,19706857,typujemy-sklad-arki-na-inauguracje-wiosny-cz-3-atak-sondaz.html


To prawdopodobnie najtrudniejsza część naszego minicyklu. Czas na zastanowienie się i przeanalizowanie, kto zagra w Arce na pozycji numer dziesięć, a kto na "dziewiątce" w meczu inaugurującym rundę wiosenną I ligi w Katowicach. Kandydatów jest czterech. Ba, nawet pięciu, ale tych czterech poważnie bierzemy w kontekście wyjściowego składu.

Scenariusz nr 1 - wariant optymistyczny


Tak naprawdę chyba najmniej prawdopodobny do spełnienia. Bierzemy w nim pod uwagę, że w świetnej dyspozycji mentalnej i zdrowotnej jest najlepszy strzelec zespołu Paweł Abbott, na którego - mimo braku jakiekolwiek meczu od ponad miesiąca - od początku spotkania będzie chciał postawić trener Grzegorz Niciński. Sam zainteresowany grą napastnik jeszcze w ubiegłym tygodniu mówił, że nie wszystko jest jeszcze, jak należy, ale fizycznie czuje się znakomicie. Problemem może być jedynie codzienne ogranie, które i tak z istniejącym składem Arki Abbott ma znakomite.

Nawet jednak jeśli napastnik teoretycznie będzie gotowy do gry od pierwszej minuty - a wszystko na to wskazuje - to wydaje się, że na tak ryzykowny ruch Niciński się nie zdecyduje. Wiadomo, kusi. "Ubot" zagrałby w tym scenariuszu zapewne z Rafałem Siemaszko, z którym jest perfekcyjnie zgrany. Zresztą, mając nawet Gastona Sangoya i Mateusza Szwocha, a Abbotta w idealnym stanie - dziwnym ruchem byłoby zmienianie tego, co jesienią perfekcyjnie funkcjonowało, czyli Siemaszkę.

Poza tym Siemaszko to jeden z jaśniejszych punktów Arki zimowych przygotowań. Grzechem byłoby nie wystawić go od początku meczu (z Abbottem czy bez). Duet Abbott - Siemaszko w pełni dyspozycji zapewne namieszałby w obronie "Gieksy". Z racji zaś przede wszystkim statusu nowicjuszy w obecnej Arce, Sangoy i Szwoch mecz powinni zacząć na ławce rezerwowych.

Scenariusz nr 2 - wariant realistyczny


Z samych słów Abbotta można wywnioskować, że ten bardzo chce zagrać w Katowicach nawet i od początku, ale taka przyjemność zapewne go nie spotka. W wariancie realistycznym najlepszy strzelec Arki usiądzie na ławce rezerwowych i być może na placu pojawi się dopiero po przerwie. Kto zatem od zagra początku?

Najpewniejszym scenariuszem jest Rafał Siemaszko w roli najbardziej wysuniętego napastnika. Pozycję numer dziesięć okupi zaś Szwoch. Zresztą, w takiej konfiguracji Niciński grał w ostatnich sparingach, wliczając w to ten najbliżej ligi, a więc przeciwko Wiśle Płock.

Opcja z Gastonem Sangoyem w składzie tak czy inaczej nie wchodzi w grę. Jest zdecydowanie zbyt wcześnie. Argentyńczyk dołączył do zespołu jako ostatni i potrzebuje z pewnością więcej czasu na aklimatyzację. Ujrzymy go na ławce, jeśli w ogóle zdążą w Gdyni dopiąć formalności biurokratyczne związane z możliwością jego gry w Polsce.

W odwodzie jest jeszcze Grzegorz Tomasiewicz, który jako zmiennik wielokrotnie wydatnie pomagał żółto-niebieskim w pierwszej rundzie. Rzadko jednak wskakiwał do składu dzięki tak zwanej "coach decision". Raczej były to sytuacje wymuszone. Mimo to ma na swoim koncie gola i trzy asysty, lecz z tego drugiego jest bardziej rozliczany. A to trzeci wynik w zespole.

Scenariusz nr 3 - wariant pesymistyczny


Nie ma takiego. Forma Arki z jesieni, ta rosnąca z przygotowań zimowych oraz bogactwo w ataku wykluczają jakikolwiek wariant pesymistyczny. Nikogo z linii pomocy z konieczności nie trzeba będzie przesuwać do ataku. Rotacja pozostanie tylko między pozycjami dziewięć i dziesięć. A jak mówią sami piłkarzy - w tym układzie nie ma zbyt dużej różnicy.

Nasz typ: Mateusz Szwoch - Rafał Siemaszko.

Typujemy skład Arki na inaugurację wiosny. Cz. 1: Obrona

Typujemy skład Arki na inaugurację wiosny. Cz. 2: Pomoc






