::{player:Bartosz Kapustka} wrócił do gry i zapracował na piękne słowa trenera Piłka nożna - Sport.pl::
@2017-11-06 21:50

- Miał ciężki rok w {team:Leicester City}, był kontuzjowany i nie miał wielu okazji do gry, ale wiedziałem, że jest silny i może robić różnicę - ocenił {player:Bartosza Kapustkę} {manager:Christian Streich}, szkoleniowiec {team:Freiburga}.
REKLAMA




Polak nie miał łatwego początku w {country:Niemczech}. Nie grał regularnie, a trener {manager:Freiburga} publicznie przyznał, że ma problemy motoryczne. Wszystko zmieniło się w 11. kolejce {league:Bundesligi}. Polak wybiegł w pierwszym składzie {team:Freiburga} i dostał od "Kickera" najwyższą ocenę w zespole - 2,5.Zespół {player:Kapustki} przegrał z {team:Schalke} (0:1), ale Polak pokazał, że jest w stanie regularnie grać w pierwszym składzie niemieckiego zespołu. Potwierdził to {manager:Christian Streich}, szkoleniowiec {team:Freiburga}.- Świetnie było widzieć {player:Bartka} na boisku. Wcześniej nie grał zbyt wiele, a pokazał się z dobrej strony - zaznaczył niemiecki trener.{team:Freiburg} po 11 meczach {league:Bundesligi} znajduje się na miejscu barażowym. Drużyna {manager:Christiana Streicha} zdobyła osiem punktów i walczy o utrzymanie w {league:Bundeslidze}.





