::Kolejny Węgier w Kolejorzu? Lech chce kupić obrońcę Andrasa Dlusztusa Piłka nożna - Sport.pl::
@2012-12-28 10:13
url: http://www.sport.pl/pilka/1,70993,13113607,Kolejny_Wegier_w_Kolejorzu__Lech_chce_kupic_obronce.html


Zdaniem węgierskiego serwisu Rangado.hu, wysłannicy Lecha Poznań trzykrotnie oglądali tej jesieni mecze Lombard Papa np. w starciu z Videotonem Szekesfehervar i grę Andrasa Dlusztusa, po czym zdecydowali się podjąć negocjacje w sprawie pozyskania Węgra. Menedżer klubu Lombard Papa, Gabor Arki mówi: - Mogę potwierdzić, że Lech Poznań poważnie zainteresował się naszym graczem.

Odmówił jednak odpowiedzi na pytanie, czy to znaczy, że Andras Dlusztus odchodzi do Polski i stanie się tej zimy graczem Kolejorza. - Jeżeli jednak Polacy będą chcieli go kupić, podjęliśmy decyzję, iż nie będziemy stawiali przeszkód. Andras ma zgodę na zagraniczny transfer - dodał.

Węgrzy cytują nawet wypowiedzi samego piłkarza, który przyznaje, że z ochotą podejmie wyzwanie gry w klubie zagranicznym. - Mam nadzieję, że kluby się dogadają - mówi.

Rynek węgierski i sam klub Lombard Papa są Lechowi dobrze znane. Stąd pochodzi Gergo Lovrencsics, który gra w barwach Kolejorza. Z ligi węgierskiej, choć z innego klubu (TE Zalaegerszeg) poznaniacy kupili także swego byłego super strzelca Artjomsa Rudnevsa.

Andras Dlusztu przyznaje, że rozmawiał z Gergo Lovrencsicsem na temat Lecha i ten jest zadowolony z gry w polskiej lidze i tym klubie.

24-letni obrońca pochodzi z Szeged na południu Węgier. Mierzy 188 cm wzrostu. Zaczynał karierę w malutkim klubie Tisza Volan. Do Lombard Papa trafił z Soproni FC. Ma na swym koncie także okres gry w lidze brazylijskiej.

Tak gra węgierski obrońca:








