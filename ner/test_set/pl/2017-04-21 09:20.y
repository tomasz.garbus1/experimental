::Transfery. {team:Real Madryt} da 70 mln euro za {player:Davida de Geę}? Piłka nożna - Sport.pl::
@2017-04-21 09:20

Angielski dziennik twierdzi, że madrytczycy złożyli za {player:de Geę} ofertę opiewającą na 70 mln euro. Gdyby transakcja doszła do skutku, niemal dwukrotnie przebiłaby dotychczasowy rekord transferowy na pozycji bramkarza - w 2001 r. {team:Juventus} zapłacił za {player:Gianluigiego Buffona} 38 mln euro.
REKLAMA




"The Sun" twierdzi jednak, że {team:Real} ma opcję rezerwową. Jeżeli "{team:Czerwone Diabły}" odrzucą propozycję, triumfatorzy {league:Ligi Mistrzów} mogą spróować sięgnąć po golkipera {team:Chelsea}. Problem w tym, że {player:Thibaut Courtois} zapowiedział, że nie zamierza odchodzić z "{team:The Blues}".





