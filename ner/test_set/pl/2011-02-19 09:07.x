::Smolarek: Lech może się nas bać::
@2011-02-19 09:07
url: http://www.sport.pl/pilka/1,65050,9133834,Smolarek__Lech_moze_sie_nas_bac.html


Akcje z boisk piłkarskiej Ekstraklasy zobaczysz na Ekstraklasa.tv »

- Ciężko trenowałem, w sparingach strzelałem bramki, jestem zadowolony. Na pewno teraz będzie mi łatwiej, bo koledzy z zespołu już mnie poznali - mówi napastnik Polonii. Dla jego zespołu niedzielne spotkanie będzie pierwszym w tej rundzie meczem o stawkę. Lech ma już za sobą pierwszy mecz z Bragą w Lidze Europejskiej.

Smolarek liczy na to, że Theo Bos, nowy szkoleniowiec Czarnych Koszul, zaufa mu bardziej niż jego poprzednicy. - Mogę strzelić gola zarówno w pierwszej, jak i w ostatniej minucie. Nie zawsze miałem tą możliwość. W Dortmundzie czy w Racingu bywało, że zdobywałem bramki w ostatnich minutach, bo trenerzy mi ufali i dawali grać do końca - przypomina były reprezentant Polski.

Wychowanek Feyenoordu Rotterdam podpowiada także, na jakiej pozycji najbardziej lubi występować. - Najlepiej czuję się w "16", tam najłatwiej mi strzelać gole. Ale jak gram na szpicy, to potrzebuję dobrych podań. Poza tym rywale wiedzą, co potrafię, będą mnie kryli we dwóch - zastrzega polonista.

Napastnik Czarnych Koszul czeka już na rywalizację z Lechem Poznań w ćwierćfinale Pucharu Polski. - Liczy się nie tylko niedzielny mecz, bo trzeba zagrać dwa dobre spotkania. Zrobimy wszystko, by awansować dalej. Lech może się nas bać - zapowiada.

Smolarek, jak na najlepiej zarabiającego zawodnika ligi, jak dotąd prezentował się słabo. W rundzie jesiennej Ekstraklasy wystąpił w dwunastu spotkaniach, zdobywając w nich tylko trzy bramki. Taki dorobek nie satysfakcjonuje właściciela warszawskiego klubu Józefa Wojciechowskiego, który  publicznie krytykował piłkarza.

Lech - Polonia w pierwszym ćwierćfinale Pucharu Polski ».






