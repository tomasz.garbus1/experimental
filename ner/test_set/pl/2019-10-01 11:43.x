::Czesław Michniewicz odkrył karty! Są powołania na mecze z Rosją i Serbią Reprezentacja Polski::
@2019-10-01 11:43
url: http://www.sport.pl/pilka/7,65037,25255660,czeslaw-michniewicz-odkryl-karty-sa-powolania-na-mecze-z-rosja.html

Reprezentacja Polski U-21 wygrała dwa pierwsze mecze eliminacji Euro U-21 w 2021 roku i jest liderem grupy E. Kadra Czesława Michniewicza najpierw pokonała na wyjeździe Łotwę 1:0, a później w Białymstoku rozbiła Estonię 4:0. Teraz poprzeczka idzie w górę. Październikowymi rywalami biało-czerwonych będą Rosjanie i Serbowie. Tak prezentuje się kadra Michniewicza na te spotkania:
REKLAMA




Piłkarze mają zarzuty do Brzęczka! O co? Odpowiedź w tym materiale wideo:

Bramkarze: Kamil Grabara, Marcin Bułka, Karol NiemczyckiObrońcy: Maciej Ambrosiewicz, Karol Fila, Robert Gumny, Kamil Pestka, Jan Sobociński, Sebastian Walukiewicz, Jan WiśniewskiPomocnicy: Mateusz Bogusz, Patryk Dziczek, Kamil Jóźwiak, Jakub Moder, Marcin Listkowski, Sylwester Lusiusz, Przemysław Płacheta, Daniel Ściślak, Kamil WojtkowskiNapastnik: Bartosz Bida, Patryk Klimala, Paweł Tomczyk, Patryk Szysz Mecz z Rosją Polacy zagrają w Jekaterynburgu. Początek 11.10 o godz. 16. Spotkanie z Serbią zostanie rozegrane 15.10 o godz. 18  na stadionie Widzewa w Łodzi. Rywalami Polaków w grupie są reprezentacje: Serbii, Rosji, Bułgarii, Łotwy i Estonii. Do turnieju głównego bezpośrednio awansuje tylko zwycięzca grupy. Drugi zespół zagra w barażu. Turniej Euro U-21 w 2021 roku zorganizują Słowacja i Węgry.Pobierz aplikację Football LIVE na AndroidaAplikacja Football LIVE Sport.pl





