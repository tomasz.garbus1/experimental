::Były piłkarz klubów {league:Ekstraklasy} poszukiwany przez policję Piłka nożna - Sport.pl::
@2018-12-06 12:41

"Grożenie innej osobie popełnieniem przestępstwa na jej szkodę lub szkodę osoby najbliższej, jeżeli groźba wzbudza w zagrożonym uzasadnioną obawę, że będzie spełniona (groźba karalna)", to treść artykułu, na podstawie którego poszukiwany jest {player:Król}, były piłkarz klubów {league:Ekstraklasy}, który w czasach juniorskich trenował w drużynie rezerw {team:Realu Madryt} (razem z {player:Szymonem Matuszkiem} i {player:Kamilem Glikiem}).
REKLAMA




{player:Krzysztof Król} poszukiwany przez policję poszukiwani.policja.plJako ostatnie miejsce zameldowania {player:Króla}, policja podaje {country:Hiszpanię}. W piłkę zakończył on jednak grać w {country:Stanach Zjednoczonych}. Jego ostatnim klubem była bowiem drużyna {team:Chicago Eagles}. 31-letni {player:Król}, który przestał grać w piłkę w styczniu ubiegłego roku, w {league:Ekstraklasie} rozegrał 96 spotkań. W amerykańskiej {league:MLS}, w barwach {team:Montreal Impact} i {team:Chicago Fire}, wystąpił natomiast w 31 spotkaniach.





