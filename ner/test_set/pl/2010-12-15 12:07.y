::{league:Ekstraklasa}. {player:Peszko}: Jeżeli odejdę, to nie do {country:Włoch} Piłka nożna - Sport.pl::
@2010-12-15 12:07


"Fakt" donosi, że być może jeszcze przed świętami Bożego Narodzenia {player:Sławomir Peszko} podpisze kontrakt z nowym klubem. Piłkarz {team:Lecha Poznań} zapowiada, że jeżeli zdecyduje się odejść z zespołu mistrza {country:Polski}, to z pewnością nie skusi go żaden włoski klub. - Jeśli w ogóle zmienię barwy klubowe, to na pewno nie będzie to włoski klub - zapewnił skrzydłowy Kolejorza. Kilkanaście dni temu La Gazetta Dello Sport podała, że transferem {player:Peszki} zainteresowane są {team:Udinese} i {team:Brescia}.

- Mogę tylko powiedzieć, że coś się szykuje w temacie transferu {player:Sławka}. Jednak ze zrozumiałych względów nie mogę podać jeszcze nazw klubów. W przeciągu najbliższych dni powinna się wyjaśnić przyszłość piłkarza - powiedział menedżer piłkarza Eugeniusz Kamiński.

Do wyścigu o {player:Peszkę} - oprócz wyżej wymienionych klubów - stanęły również: {team:Vfl Wolfsburg} i {team:Panathinaikos Ateny}. Reprezentanta kraju widziałby w swojej ekipie Józef Wojciechowski. Cena za dynamicznego piłkarza nie jest wysoka. Prasa zapewnia, że {player:Peszko} ma wpisaną w kontrakcie sumę odstępnego, oscylującą w granicach 500 tysięcy uro.






