::Thiago Alcantara może wrócić do FC Barcelony Piłka nożna - Sport.pl::
@2017-10-16 16:35
url: http://www.sport.pl/pilka/7,65082,22521101,thiago-alcantara-moze-wrocic-do-fc-barcelony.html

Thiago Alcantara to wychowanek FC Barcelony i eksperci uznawali go nawet za potencjalnego następcę Iniesty i Xaviego. W 2013 roku Barcelona podjęła jednak decyzję o sprzedaży swojego zawodnika, a Alcantara trafił do Bayernu za 25 milionów euro.
REKLAMA




Hiszpański Don Balon pisze, że Barcelona rozważa ewentualne sprowadzenie Alcantary, gdyby nie wyszedł jej transfer Phillipe Coutinho z Liverpoolu. Alcantara ma za sobą 100 występów w barwach Barcelony. 26-latek strzelił 11 bramek i miał 20 asyst. Jego statystyki w Bayernie są nieco lepsze, bo piłkarz zagrał 129 meczów, strzelił 20 bramek i dołożył 23 asysty.Wyciągnięcie Alcantary z Bayernu również nie będzie łatwą sprawą, bo kontrakt zawodnika wygasa dopiero w 2021 roku.





