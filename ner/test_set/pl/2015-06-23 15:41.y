::{manager:Czesław Michniewicz} z {team:Pogoni}: Pierwszym kryterium doboru nowych piłkarzy była fryzura Piłka nożna - Sport.pl::
@2015-06-23 15:41


Zabrakło {player:Ricardo Nunesa} (uroczystość rodzinna), {player:Kamila Wojtkowskiego} (może przejść do {team:RB Lipsk}) i {player:Sebastiana Rudola} (był na zgrupowaniu kadry młodzieżowej). {player:Nunes} i {player:Rudol} w {team:Pogoni} pozostaną, przy {player:Wojtkowskim} trzeba postawić znak zapytania. Pojawiła się za to trójka nowych graczy: {player:Jakub Słowik} (bramkarz, ostatnio {team:Jagiellonia Białystok}), {player:Jakub Czerwiński} (obrońca, {team:Termalica Nieciecza}) i {player:Jarosław Fojut} (obrońca, {team:Dundee United}). Był też {player:Mateusz Lewandowski}, który wrócił z wypożyczenia do włoskiego drugoligowca.

- Przeważają łysi - zażartował trener {manager:Michniewicz}. - Jest {player:Kuba Słowik}, ale akurat on kończy rehabilitację i w pełny trening wejdzie dopiero podczas obozu we Wronkach [rozpocznie się w poniedziałek - red.].

Jakub Lisowski: Dobierał pan celowo takich obrońców, by straszyli i mylili przeciwników w meczach o punkty?


{manager:Czesław Michniewicz}: Tak. Pierwszym kryterium doboru była fryzura, czyli łysi, a drugim imię, czyli Jakub. A poważnie - patrzyliśmy na klasę piłkarza i wiek. {player:Jarek} ma 28 lat, ale wciąż przyszłość przed nim. Świetnie się prowadzi i uważam, że może mieć dobry wpływ na kolegów. Bije od niego pozytywna energia i niech tak pozostanie. {player:Kuba Czerwiński} grał 4 lata w {team:Termalice}, w końcu wywalczył awans do elity. Młody, a już ograny. I {player:Kuba Słowik} - młody, z perspektywami, jestem o niego spokojny.

{team:Pogoń} jeszcze szuka nowych zawodników?


- Przede wszystkim cieszę się, że ci nowi piłkarze już są z nami. Wymagało to wiele wysiłku, zaangażowania, ale działacze klubu stanęli na wysokości zadania. Nie ukrywam, że jeszcze pracujemy nad kolejnymi transferami, ale na efekty musimy jeszcze poczekać. Cieszy mnie, że wszyscy wrócili do treningów z dużą ochotą, stęsknieni za piłkami, palą się do gry. I nie było wpadek, że ktoś przybrał na wadze. Jedyny minus pierwszych zajęć to ta paskudna pogoda, chyba pod {player:Jarka Fojuta} zamówiona. Przed nami krótki okres przygotowawczy, ledwie 25 jednostek treningowych i mało sparingów. A musimy to wszystko poskładać i szukać optymalnego ustawienia na trudny początek ligi: {team:Lech}, {team:Śląsk}, {team:Lechia}...

Priorytet to defensywny pomocnik i napastnik? Czy tym pierwszym będzie {player:Iwan Majewski} z {team:Zawiszy Bydgoszcz}?


- Chcielibyśmy wzmocnić skład innym typem zawodnika niż był {player:Maks Rogalski}. Chodzi nam o rosłego, silnego piłkarza. Nie będzie łatwo, bo inne kluby też szukają. Jednego piłkarza jesteśmy blisko, ale nie zdradzę o kogo chodzi.

We Wronkach będzie już pełna kadra zespołu?


- Chciałbym, by tak było, ale to będzie bardzo trudne. Okienko transferowe otwiera się 1 lipca, a kluby różnie reagują, gdy ich zawodnik podpisuje umowę z innym zespołem. Są kluby, które trochę po złości trzymają piłkarzy do 30 czerwca, inne nie robią problemów. Nie ukrywam, że okienko trwa do 30 sierpnia i do końca będziemy bardzo czujni. Potrzebujemy jeszcze 2-3 klasowych wzmocnień, ale w granicach finansowych możliwości.

Na papierze {team:Pogoń} ma kim postraszyć w formacjach ofensywnych, jest rywalizacja. W obronie jeszcze nie.


- Przyjęliśmy taką strategię, by stworzyć fundamenty drużyny od bramkarza, stoperów, defensywnego zawodnika. Wszyscy wiemy, że odszedł od nas {player:Marcin Robak}, a takiego zawodnika nie da się szybko zastąpić "jeden do jednego". Wolny na rynku jest {player:Marco Paixao} [ostatnio {team:Śląsk Wrocław}], ale na razie naszego klubu nie stać na takiego zawodnika. Jesteśmy jeszcze na minusie na prawej obronie, bo jest tylko {player:Seba Rudol}. Może tam grać {player:Adaś Frączczak}, ale nie można mu ciągle zmieniać pozycji. Tu musimy poszukać rywala dla {player:Rudola}. Może {player:Kuba Czerwiński}? Fizycznie wygląda świetnie, ale z drugiej strony nie po to ściągaliśmy stopera, by teraz przesuwać go na bok. Jeśli zajdzie konieczność - może sprawdzimy takie rozwiązanie.

Planuje pan testowanie zawodników?


- Nie chcę nic wykluczać, ale wolałbym, by testy prowadzone były za pośrednictwem drugiej drużyny. Tam pokaże klasę, sprawdzimy go w pierwszym zespole. Różne mogą być sytuacje, oferty, a przykład {player:Nunesa} pokazuje, że można ciekawego zawodnika ściągnąć już w trakcie rozgrywek. Pamiętam również, że przed nami 21 spotkań ligowych. To ogromna dawka, a nie mam gwarancji, że ominą nas kontuzje, z czym mieliśmy problemy wiosną.

{player:Murayama} i {player:Małecki} wrócili do treningów na pełnych obrotach?


- Różnie. Z {player:Taku} wszystko OK, a ja na niego ogromnie liczę. Jak patrzę na jego statystyki z ubiegłego sezonu, to chciałbym powtórki. Tym bardziej że zabraknie nam {player:Robaka}, więc zdobytymi bramkami będziemy musieli się podzielić - rozpoczynając od {player:Jarka Fojuta} na {player:Łukaszu Zwolińskim} kończąc. Co do {player:Patryka}, to ma wielki zapał do gry, ale ponad pół roku nie grał. Musimy go spokojnie odbudowywać i wprowadzać na boisko.

Z kadry, która uczestniczyła w pierwszym treningu ktoś jeszcze może odejść?


- Raczej zostaną już wszyscy, bo chcemy mieć silny zespół rezerw. Być może pojawi się sytuacja, jak z {player:Dawidem Kortem}, który do nas wrócił, podpisał nową umowę i mógł walczyć o kadrę pierwszego zespołu. Nie miał żadnych gwarancji, więc skorzystał z propozycji {team:Bytovii Bytów}. Jego rozumowanie było bardzo logiczne. Mam nadzieję, że będzie się rozwijał. Dodam, że wielu młodych chce do nas przyjść i być może byśmy kogoś podmienili, ale na razie takich sytuacji nie ma w planach.

Ustalony został już ostatni sparingpartner?


- Tak, polecimy do Kaiserslautern na zaproszenie Niemców. Wyjazd autokarem nie wchodził w grę, bo za dużo byśmy stracili czasu i sił. To ledwie tydzień przed ligą, więc musimy być ostrożni.

Rozmawiał Jakub Lisowski




W sobotę {team:Pogoń} rozegra pierwszy sparing. Pojedzie do Lęborka na mecz z tamtejszą {team:Pogonią} dwoma składami, by wszyscy pograli i przeprowadzili trening.






