::Finały Australian Open, Milik wraca do Serie A, Lewandowski goni Aubameyanga [ROZKŁAD WEEKENDU] Piłka nożna - Sport.pl::
@2017-01-27 19:00
url: http://www.sport.pl/pilka/1,65085,21301845,finaly-australian-open-milik-wraca-do-serie-a-lewandowski.html


Sobota:


Godz. 9.30 Venus Williams - Serena Williams


W finale kobiecej rywalizacji zmierzą się 35-letnia Serena Williams, a po drugiej stronie kortu stanie jej starsza o rok siostra Venus. Pierwsza walczy teraz o rekordowy w liczonej od 1968 roku Open Erze 23. triumf w Wielkim Szlemie w grze pojedynczej. Druga z Amerykanek ma w dorobku siedem tytułów. Ich sobotni pojedynek zapisze się w historii - będzie to wielkoszlemowy finał z udziałem najstarszych singlistek w historii.

Godz. 14.45 Resovia - Skra


Mecz drugiej z trzecią drużyną w tabeli. W pierwszej rundzie rzeszowianie wygrali na wyjeździe 3:1, bo dobrze grali blokiem i popełniali mniej błędów niż rywale. Skra ma za sobą dobry okres w PlusLidze, zwyciężyła w ostatnich dziewięciu ligowych potyczkach. Resovia uległa w poprzedniej kolejce ekipie z Gdańska po pięciosetowym boju.

Godz. 15.30 Werder - Bayern


Zwycięski w sześciu ostatnich kolejkach Bayern zmierzy się z Werderem, z którym wygrał 12 ostatnich spotkań ligowych. Jeśli odniesie 13. zwycięstwo, ustanowi rekord Bundesligi - żaden zespół nie miał nigdy tak długiej serii wygranych meczów z jednym rywalem. Robert Lewandowski walczy o pierwsze miejsce w klasyfikacji strzelców. Na razie ma 14 bramek, o dwie mniej od prowadzącego Gabończyka Pierre'a-Emericka Aubameyanga, który powrócił już do składu Borussii Dortmund z Pucharu Narodów Afryki.

Godz. 16.05 PŚ w skokach - konkurs drużynowy w Willingen


Sobotni konkurs będzie trzecim drużynowym w sezonie 2016/17. Pierwszy - 3 grudnia w niemieckim Klingenthal - wygrali Polacy przed Niemcami i Austrią. Był to pierwszy triumf biało-czerwonych w historii. W drugim na Wielkiej Krokwi w Zakopanem triumfowali Niemcy przed Polską i Słowenią.

Poza tym dzieje się:


10:00 Biatlon: Mistrzostwa Europy w Dusznikach-Zdroju - bieg na dochodzenie mężczyzn

10.15 Narciarstwo alpejskie: PŚ w Cortinie d'Ampezzo - zjazd kobiet

11.45 Australian Open - mecz finałowy gry podwójnej mężczyzn

12.00 Narciarstwo alpejskie: PŚ w Garmisch-Partenkirchen - zjazd mężczyzn

12.55 Villarreal - Granada (transmisja w Eleven Sports)

13.00 Biatlon: Mistrzostwa Europy w Dusznikach-Zdroju - bieg na dochodzenie kobiet

13.25 Liverpool - Wolverhamtpon (transmisja w Eleven)

15.55 Chelsea - Brentford (transmisja w Eleven Sports)

16.10 Deportivo Alaves - Atletico Madryt (transmisja w Eleven)

16.55 Lyon - Lille (transmisja w Eleven Extra)





17.55 Legia Warszawa - Zenit Sankt Petersburg

17.55 Lazio - Chievo (transmisja w Elven Sports)

18.25 Southampton - Arsenal (transmisja w Eleven)

19.55 Rennes - Nantes (transmisja w Eleven Sports)

20.40 Inter - Pescara (transmisja w Eleven)

20.40 Leganes - Celta Vigo (transmisja w Eleven Extra)

Niedziela:


Godz. 9.30 Roger Federer - Rafael Nadal


W decydującym pojedynku u mężczyzn kibice obejrzą konfrontację 35-letniego Szwajcara Rogera Federera z młodszym o pięć lat Hiszpanem Rafaelem Nadalem. Pierwszy może pochwalić się 17 - rekordowymi w męskich singlu - zwycięstwami w Wielkim Szlemie, drugi - 14. Nadal z Federerem do tej pory grał 34 razy, wygrał 23 pojedynki. Gdy obaj spotykali się w finałach, Nadal ma bilans 14-7.

Godz. 12 Betis - FC Barcelona (transmisja w Eleven)


Broniąca tytułu FC Barcelona zagra na stadionie rywala z Betisem. Katalończycy od 1 listopada przegrali tylko jeden mecz - z Athletic Bilbao 1:2 w pierwszym spotkaniu 1/8 finału Pucharu Hiszpanii. Później jednak odrobili stratę i awansowali do kolejnej rundy, a po czwartkowym zwycięstwie nad Realem Sociedad 5:2 dostali się także do półfinału. W LaLiga

Godz. 15.05 PŚ w skokach. Konkurs indywidualny w Willingen


W niedzielę na skoczni w niemieckim Willingen lider klasyfikacji generalnej Pucharu Świata w skokach narciarskich Kamil Stoch stanie przed szansą odniesienia piątego zwycięstwa z rzędu i 21. w karierze. Od 2014 roku dwukrotny mistrz olimpijski z Soczi trzy razy startował w Willingen w konkursach indywidualnych i... trzy razy je wygrał.

Godz. 20.45 Napoli - Palermo (transmisja w Eleven)


Arkadiusz Milik po raz pierwszy od fatalnej kontuzji kolana znalazł się w kadrze meczowej Napoli. Czy w meczu z przedostatnim w Serie A Paleromo zagra, nie wiadomo. W wyjściowej jedenastce powinien się za to znaleźć Piotr Zieliński. Możemy też liczyć na to, że w defensywie Palermo od pierwszych minut zagra Thiago Cionek.

Godz. 20.45 Real Madryt - Real Sociedad (transmisja w Eleven Sports)


Po niespodziewanym odpadnięciu w ćwierćfinale piłkarskiego Pucharu Hiszpanii, lider tamtejszej ekstraklasy Real Madryt postara się szybko zażegnać kryzys. W 20. kolejce jego rywalem będzie Real Sociedad, który nie jest w tym sezonie łatwym przeciwnikiem. Jest piąty w tabeli i ma 35 punktów, podobnie jak czwarte Atletico Madryt. To oznacza, że wciąż liczy się w walce o awans do Ligi Mistrzów. Do lidera zespół z San Sebastian traci osiem "oczek".

Godz. 21 PSG - Monaco (transmisja w Eleven Extra)


Po 21. kolejkach Monaco było liderem z przewagą dwóch punktów nad Niceą. PSG zajmowało trzecia lokatę ze stratą trzech punktów. Mecz ma ogromne znaczenie w kontekście walki o mistrzostwo Francji. W pierwszy meczu lepsi byli gracze z Monako, którzy zwyciężyli aż 3:1. W niedzielnym spotkaniu nie zagra Grzegorz Krychowiak (PSG), który jest kontuzjowany. Nie wiadomo też, czy wystąpi Kamil Glik (Monaco), który narzekał na problemy mięśniowe.




Poza tym dzieje się:


10.00 Narciarstwo alpejskie: PŚ w Garmisch-Partenkirchen - slalom gigant mężczyzn

10.00 Biatlon: Mistrzostwa Europy w Dusznikach-Zdroju - pojedyncza sztafeta mieszana

12.25 Torino - Atalanta (transmisja w Eleven Sports)

13.00 Milwall - Watford (transmisja w Eleven Extra)

13.00 Biatlon: Mistrzostwa Europy w Dusznikach-Zdroju - sztafeta mieszana

13.00 Biegi narciarskie: PŚ w Falun - bieg na 30 km stylem klasycznym mężczyzn

14.00 Biegi narciarskie: PŚ w Falun - bieg na 15 km stylem klasycznym kobiet

14.55 Sassuolo - Juventus (transmisja w Eleven Extra)

14.55 Sampdoria - AS Roma (transmisja w Eleven Sports)

16.10 Espanyol -Sevilla (transmisja w Eleven)

16.55 Manchester United - Wigan (transmisja w Eleven Sports)

17.20 Piłka ręczna mężczyzn: Mistrzostwa Świata we Francji - mecz finałowy

18 Anderlecht - Standard (transmisja w Eleven Extra)

18.25 Athletic Bilbao - Sporting Gijon (transmisja w Eleven)

22.00 Hokej: NHL Mecz Gwiazd



Wielkie gwiazdy zmienią klub w ostatniej chwili? Koniec okna transferowego coraz bliżej!






