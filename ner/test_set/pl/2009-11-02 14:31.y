::Urlopowany. {manager:Marcin Brosz} nie poprowadzi już {team:Podbeskidzia} Piłka nożna - Sport.pl::
@2009-11-02 14:31


{team:Podbeskidzie}, które przez dwa kolejne sezony było blisko awansu, tej jesieni spisuje się fatalnie. {team:Górale} przegrali trzy kolejne mecze na własnym stadionie. Po 16 meczach bielszczanie zajmują trzynaste miejsce, mają ledwie punkt przewagi nad strefą spadkową.

Kim jest nowy trener? {manager:Tomasz Świderski}, nauczyciel wychowania fizycznego w jednej z bielskich podstawówek, od kilku lat prowadzi młodzieżowe grupy {team:Podbeskidzia}. - Ma 36 lat, jest równolatkiem trenera {manager:Brosza}. Zdolny, perspektywiczny, na dobre i na złe związany z naszym klubem - mówi Jarosław Zięba, członek Zarządu {team:Podbeskidzia}.

{manager:Świderski} jest trenerem tymczasowym, poprowadzi zespół w trzech ostatnich meczach tego roku. W połowie listopada zbiera się walne zgromadzenie członków klubu, które zdecyduje o dalszych losach {manager:Brosza} i jego następcy.

{manager:Świderski} w roli pierwszoligowego trenera zadebiutuje już w środę. W derbach {team:Podbeskidzie} zmierzy się z lokalnym rywalem {team:GKS-em Katowice}.





