::Trener Jan Urban o transferze Artjomsa Rudnevsa: Pomarzyć zawsze można Piłka nożna - Sport.pl::
@2015-11-20 07:40
url: http://www.sport.pl/pilka/1,65039,19217573,trener-jan-urban-o-transferze-artjomsa-rudnevsa-pomarzyc-zawsze.html


Artjoms Rudnevs po raz kolejny widziany był w Poznaniu. Tym razem pojawiły się już jednak informacje na temat tego, że Lech Poznań negocjuje z Hamburger SV kwestię jego przejścia, zapewne na wypożyczenie od 1 stycznia. Sęk w tym, że były znakomity napastnik Lecha zarabia w Hamburgu 1,5 mln euro. Kolejorza absolutnie nie stać na taki wydatek, nawet gdyby podzieliłby zarobki piłkarza z hamburczykami. Żeby Artjoms Rudnevs mógł trafić ponownie na Bułgarską, o czym marzą kibice, musiałby ograniczyć żądania finansowe.

Trener Jan Urban zapytany o to, czy chciałby aby łotewski snajper trafił do Lecha, odrzekł: - Tak. Jasne, że tak. Przydałby się nam, co? Nie jest to możliwe, ale pomarzyć zawsze można.

Artjoms Rudnevs był bohaterem Lecha zwłaszcza jesienią 2010 roku, gdy strzelił w pucharach cztery gole Juventusowi Turyn - aż trzy na wyjeździe. Odszedł do HSV w czerwcu 2012 roku. Hamburski klub zapłacił za niego 3,5 mln euro. Dzisiaj Rudnevs nie ma miejsca w składzie niemieckiego zespołu.






