::Krzysztof Piątek z Milanem gra o Ligę Mistrzów. Czy będzie niedziela cudów w Serie A? Piłka nożna - Sport.pl::
@2019-05-25 10:37
url: http://www.sport.pl/pilka/7,65083,24825418,krzysztof-piatek-z-milanem-gra-o-lige-mistrzow-czy-bedzie-niedziela.html

Mistrzem Juventus, wicemistrzem Napoli, pewne spadku Frosinone i Chievo - to wiadomo przed ostatnią, 38. kolejką Serie A. Na sobotę zaplanowano dwa z 10 meczów. Oba nieistotne dla końcowej tabeli (Frosinone - Chievo i Bologna - Napoli).
REKLAMA






W niedzielę najciekawiej powinno być w walce o trzecie i czwarte miejsce. Te pozycje dają prawo gry w Lidze Mistrzów. W walce o nie są cztery drużyny.Sytuacja w tabeli wygląda tak:3. Atalanta Bergamo 66 pkt4. Inter Mediolan 665. AC Milan 656. AS Roma 63Wydaje się, że najbliżej gry w Champions League jest Atalanta, która podejmie Sassuolo, a więc 10. zespół tabeli, pewny już utrzymania i mający zbyt dużą stratę punktową, by być jeszcze w grze o europejskie puchary.

Wszystko w swoich rękach mają też gracze Interu. Ale oni mogą mieć trudniej, bo na San Siro przyjedzie dobrze dysponowane w ostatnich kolejkach Empoli (trzy zwycięstwa z rzędu), które potrzebuje punktów, by utrzymać się w Serie A. Przed ostatnią kolejką drużyna, której bramki strzeże Bartłomiej Drągowski ma 38 oczek i jest 17. w tabeli. Ostatnie spadkowe miejsce, czyli 18., zajmuje Geona, która ma 37 punktów. Były klub Piątka w niedzielę zagra na wyjeździe z Fiorentiną, która ma 40 punktów, jest 15. i również nie może być pewna pozostania w Serie A.Końcówka tabeli Serie A wygląda tak:15. Fiorentina 40 pkt16. Udinese 4017. Empoli 3818. Genoa 3719. Frosinone 2420. Chievo 16Drągowski i jego drużyna są nadzieją dla Piątka i jego zespołu. Milan zmierzy się na wyjeździe ze SPAL. A ta ekipa ma 42 punkty i jest już pewna gry w ekstraklasie w następnym sezonie.

Również Roma zagra na koniec z drużyną, która nie walczy już ani o puchary, ani o uniknięcie spadku. Ale jest mało prawdopodobne, że ewentualne zwycięstwo nad Parmą da Romie awans do "czwórki". Rzymianie muszą po prostu liczyć na wpadki rywali.





