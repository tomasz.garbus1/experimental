::{league:Liga grecka}. Samobójcza bramka {player:Marka Saganowskiego} Piłka nożna - Sport.pl::
@2010-03-07 19:07


To goście pierwsi mogli objąć prowadzenie, ale po pół godzinie gry {player:Tatos} trafił w słupek.

W 82 min. rzut wolny wykonywał {player:Geladaris}. Piłkę do własnej bramki skierował {player:Saganowski}. Było 1:0 dla gospodarzy.

W pierwszej minucie doliczonego czasu gry z bliskiej odległości gola strzelił jednak {player:Sfakianakis} i uratował dla gości remis.

Cały mecz w zespole {team:Atromitosu} rozegrał także {player:Marcin Baszczyński}

Drużyna Polaków zajmuje obecnie siódme miejsce w ligowej tabeli z 32 punktami na koncie.

Gol {player:Żurawskiego} z karnego »





