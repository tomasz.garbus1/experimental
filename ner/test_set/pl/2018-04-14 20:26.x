::Bundesliga. Wysoka wygrana Bayernu, gol Lewandowskiego Piłka nożna - Sport.pl::
@2018-04-14 20:26
url: http://www.sport.pl/pilka/7,65081,23271270,bundesliga-wysoka-wygrana-bayernu-gol-lewandowskiego.html

Sobotnie spotkanie źle zaczęło się dla gospodarzy, bo już w 9. minucie gola dla Borussii strzelił Josip Drmić.
REKLAMA




Na odpowiedź mistrzów Niemiec czekaliśmy do 37. minuty, kiedy bramkę na 1:1 zdobył Sandro Wagner. Cztery minuty później 30-latek po raz drugi wpisał się na listę strzelców i dał prowadzenie Bayernowi.W 51. minucie na 3:1 podwyższył Thiago Alcantara, a w 68. minucie swoją bramkę zdobył też David Alaba. W 82. minucie wynik spotkania na 5:1 ustalił Lewandowski, który na boisku pojawił się w 69. minucie zmieniając Wagnera. Dla Polaka, który prowadzi w klasyfikacji strzelców Bundesligi, było to 27. trafienie w lidze.Bayern, który kolejne mistrzostwo Niemiec przypieczętował przed tygodniem, po 30 kolejkach ma na swoim koncie 75 punktów i aż o 23 "oczka" wyprzedza Schalke 04.





