::Smuda: Koniec kompromisów z klubami! Reprezentacja Polski::
@2011-01-31 09:21
url: http://www.sport.pl/pilka/1,65037,9028277,Smuda__Koniec_kompromisow_z_klubami_.html


- Skoro trener Theo Bos chce wygrać ligę i zdobyć Puchar Polski, to ja mu nie będę tego planu utrudniał. Na pierwszą część zgrupowania zabieram tylko Jodłowca i Sobiecha, na drugą Mierzejewskiego i Sadloka - mówi w wywiadzie dla poniedziałkowego "Przeglądu Sportowego" Smuda.

- Żadnej wyrozumiałości już nie będzie! Ostatni raz zgodziłem się na taki kompromis - ostrzega jednak chwilę później selekcjoner. - Już nigdy nie wezmę pod uwagę, że holenderski trener już za kilka miesięcy chce zdobył mistrzostwo i Puchar Polski! Bo każdy klub o coś walczy.

Franciszkowi Smudzie brakuje jednak konsekwencji. - Elastyczny to ja mogę być w przypadku Lecha, który zaraz będzie grał ważne mecze w Lidze Europy, a to chluba i interes dla całej polskiej piłki - mówi w tej samej rozmowie.

Były trener Lecha nie przejmuje się także tym, iż na pierwszej części zgrupowania będzie miał do dyspozycji tylko 17 piłkarzy. - W tej liczbie jest siódemka, a to dla mnie szczęśliwa cyfra. Tak jak dla Bogusława Cupiała. Gdy pracowałem w Wiśle, właściciel często mi to powtarzał i wreszcie ja też uwierzyłem w szczęśliwą magię siódemki - na poważnie deklaruje Smuda.

Selekcjoner wyjaśnia także po raz kolejny status Artura Boruca. - Boruc u mnie nie zagra. Oświadczam, że w tej chwili mam bez niego czterech mocnych kandydatów do kadry na Euro 2012 - mówi Smuda. Oprócz Fabiańskiego, Szczęsnego i Tytonia ma to jakoby być jeszcze Tomasz Kuszczak.

"Wyp***j", czyli "sympatyczne" stosunki między trenerem i dyrektorem sportowym Legii Warszawa »






