::Primera Division. Na Camp Nou fiesta Piłka nożna - Sport.pl::
@2015-02-12 11:49
url: http://www.sport.pl/pilka/1,65082,17395684,Primera_Division__Na_Camp_Nou_fiesta.html


Dyskutuj z autorem na jego blogu

Były piłkarz Barcelony i były trener Villarreal Victor Munoz uważa, że jeszcze nigdy drużyna z Camp Nou nie miała tak ogromnego potencjału w ataku. Stwierdzenie obarczone ogromnym stopniem ryzyka, przecież w ostatnich latach partnerami Leo Messiego byli Ronaldinho, Thierry Henry, Samuel Eto'o, David Villa, żeby nie wymienić Zlatana Ibrahimovicia, dla którego pobyt w Katalonii był klapą totalną. Rzecz jasna trio Messi - Suarez - Neymar daje Luisowi Enrique wielkie możliwości, ale tak naprawdę wszystko, co powiemy dziś, może ulec natychmiastowej weryfikacji.

Zaledwie 4 stycznia, czyli nie tak dawno, Camp Nou trząsł się w posadach. Po porażce z Realem Sociedad prasa hiszpańska donosiła, że między Messim a Luisem Enrique jest topór nie do zakopania. Trener miał wylatywać z pracy, czterokrotny laureat Złotej Piłki szukał swojej przyszłości w Premier League. Zanosiło się na rewolucję, która po sześciu tygodniach zmieniła się w idyllę. - Nie mam wątpliwości: jedna porażka i "fiesta" wróci - przestrzega sam siebie Luis Enrique. "Fiesta", czyli histeria i poczucie, że drużyna zmierza donikąd.

Wczorajszy mecz z Villarreal mógł ostatecznie rozstrzygnąć sprawę awansu do finału Copa del Rey. Zwłaszcza gdyby dobrze grający Ter Stegen nie popełnił błędu przy wyrównującej bramce. Ale i w końcówce meczu instynkt zmylił wygrywającą dwoma golami Barcelonę. Neymar poprosił Messiego, by mógł strzelać karnego, Leo wspaniałomyślnie pozwolił i bohaterem wieczoru został bramkarz gości Sergio Asenjo. Jak widać, atmosfera pełnej zgody i zrozumienia nie musi wcale służyć drużynie. Zamiast wygrać 4:1, Barca zostawiła rywala z opcjami na rewanż. Tylko raz w historii rozgrywek po zwycięstwie 3:1 na Camp Nou Katalończycy odpadli z rozgrywek. Było to w 1960 roku przeciw Athletic Bilbao, które w rewanżu ograło Barcę 3:0. W pozostałych przypadkach ten wynik zawsze wystarczał.

Katalończycy wygrali 10. kolejny mecz, ale kibiców z Camp Nou w ekstazę to nie wprowadza - 57 tys. widzów to bardzo zły wynik, na liście najgorszych frekwencji w tym sezonie trzeci od końca. Tylko na meczach z Huescą i Elche było mniej widzów, wszystko w Pucharze Króla, co może w jakimś stopniu pokazuje, że fani z Katalonii nie są najbardziej spragnieni tego akurat trofeum.

Prasa w Barcelonie donosi, że Messi i Neymar zdobyli w tym roku więcej bramek niż cały Real Madryt. Takie słodkie statystyki do niczego nieprzydatne. Tak samo jak fakt, że Gerard Pique zalicza właśnie sezon z największą liczbą goli (5). W czasie trwania rozgrywek liczy się bramki, po nich trofea. Niecierpliwość dziennikarzy i kibiców sprawia, że grzebią się w danych mniej istotnych. Tak naprawdę liczyć się będzie to, czy Barca Luisa Enrique znajdzie rozwiązanie swoich problemów na dłuższą metę. Na razie prognozy są obiecujące. Drużyna odzyskała radość z gry i sposoby na zaskakiwanie rywala. Średnia goli wzrosła znacząco - Barca nie musi już zazdrościć nikomu.

W jeszcze wyższym i bardziej uzasadnionym stanie euforii był tak niedawno Real Madryt, opromieniony czterema trofeami w 2014 roku i passą 22 kolejnych zwycięstw. Dziś na Santiago Bernabeu biją na alarm, a tamtejsi dziennikarze zapewniają, że pozycja Carlo Ancelottiego nigdy nie była tak niepewna. Dlatego ma rację Luis Enrique, nie chcąc wygłaszać triumfalnych sądów. Wie, że jego drużyna w końcu przegra, i co wtedy? Za chwilę Barcelona zmierzy się z Manchesterem City w 1/8 finału Ligi Mistrzów. Niby rywal to mistrz Anglii, ale na Camp Nou nikt nie dopuszcza do siebie czarnego scenariusza. Przecież z City poradziła sobie nawet drużyna "Taty" Martino, która zakończyła sezon na tarczy - bez trofeum. To była najgorsza Barca od 2008 roku. Luis Enrique przybył na Camp Nou, by ją z tego kryzysu wyciągnąć.

Gdyby na skutek jakiegoś nieszczęśliwego zbiegu okoliczności nowemu trenerowi noga się jednak powinęła, tych 10 zwycięstw, które świętuje się dziś, pójdzie w głębokie zapomnienie. Luis Enrique nie będzie mógł ich użyć nawet jako skromnego alibi. Dlatego musi zachować czujność. Serie zwycięstw niedające trofeów to w wielkich klubach fakt bez znaczenia?

Alaba i Weiser w stringach... oraz inni piłkarze w bieliźnie [ZDJĘCIA]






