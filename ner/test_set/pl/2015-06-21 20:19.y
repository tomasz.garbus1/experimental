::{league:Liga egipska}. Zawodnik zniósł kontuzjowanego rywala z boiska [WIDEO]  Piłka nożna - Sport.pl::
@2015-06-21 20:19


Nie był to bynajmniej akt samarytański. {player:Saad Samir} chciał jak najszybciej wznowić grę, gdyż zegar pokazywał już 89. minutę, ale w meczu był remis 1:1. Dla drugiego w tabeli {team:Al Ahly} oznaczało to stratę punktów i utratę szansy na zmniejszenie strat do prowadzącego {team:Zamaleka}.
{player:Samir} wziął na ręce zwijającego się z bólu rywala i wyniósł za linię boczną. Obejrzał za to żółtą kartkę. Sędzia doliczył potem jeszcze kilka minut, w 98 minucie czerwoną kartkę zobaczył jeden z graczy gości, ale wynik nie uległ zmianie. W tabeli prowadzi {team:Zamalek} z przewagą dziewięciu punktów nad {team:Al Ahly}.







