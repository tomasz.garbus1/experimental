::Brak wyróżnień dla piłkarzy {team:Pogoni}, dwójka nominowanych z {team:Floty} Piłka nożna - Sport.pl::
@2014-12-23 15:40


Canal Plus, który transmituje rozgrywki polskiej ekstraklasy wybierał tzw. największy plus jesieni. Kibice w głosowaniu wskazali na {player:Semira Stilicia} z {team:Wisły Kraków}. Jednym z jego kontrkandydatów był {player:Adam Frączczak} z {team:Pogoni Szczecin}, ale mimo uzyskania świetnego w sumie wyniku (prawie 12 procent głosów) zajął 4. miejsce. Wyprzedził m.in. {player:Grzegorza Bonina} (krótko był piłkarzem {team:Pogoni}, obecnie gra w {team:Górniku Łęczna}), {player:Sebastiana Milę} ({team:Śląsk}) czy {player:Kamila Wilczka} ({team:Piast}).

Drugie miejsce zajął {player:Bartłomiej Drągowski} z {team:Jagiellonii Białystok}, a trzecie {player:Szymon Pawłowski} z {team:Lecha Poznań}.

Także mecze z udziałem {team:Pogoni} nie zostały szczególnie wyróżnione wśród najciekawszych widowisk jesieni. W konkursie na mecz roku wygrało spotkanie {team:Legii} ze {team:Śląskiem} (4:3), a mecz {team:Wisła Kraków} - {team:Pogoń Szczecin} (3 maja, 5:0) zajął 9. pozycję w głosowaniu.

Za to serwis {league:Ekstraklasa}.net wybierał jedenastkę {league:I ligi}. Do grona wyróżnionych trafiła dwójka piłkarzy {team:Floty Świnoujście}, która długo była w czubie tabeli, ale końcówkę miała już słabszą. Na wyróżnienie zapracowali {player:Sebastian Kamiński} i {player:Rafał Grzelak}.

- {player:Kamiński} to odkrycie jesieni. Wiosną w ekipie ze {team:Świnoujścia} był jeszcze rezerwowym, lecz w przerwie letniej wywalczył pewny plac. Dobra szybkość i dośrodkowanie. Ciekawe, jak ten zawodnik się rozwinie. {player:Grzelak} po nieudanej przygodzie w gdyńskiej Arce odnalazł swoje miejsce w {team:Świnoujściu}. Rozgrywał, asystował. Dobrze sprawdzał się zarówno w środku pola, jak i na lewej flance - czytamy w uzasadnieniu portalu.






