::{league:Liga Mistrzów}. {player:Lewandowski} nie dał wymówek {team:Borussii} Piłka nożna - Sport.pl::
@2014-02-25 20:52




Zresztą przed meczem przygotowano wymówki, mimo że to {team:Zenit} nie znajduje się w rytmie meczowym, a po losowaniu powszechnie sądzono, że to do {team:Borussii} los się uśmiechnął. Problem polegał jednak na tym, że sam zespół z Dortmundu dał kibicom i ekspertom powody do zwątpienia. Nowy rok przyniósł nieznaczne wzmocnienia, nawet powrót do zwycięskiej serii, której trwałość jednak najlepiej pokazał sobotni mecz z {team:Hamburgerem}. Znów, jak w listopadzie, wydawało się, że mogą pojawić się spore pęknięcia na dortmundzkim pomniku {manager:Kloppa}.

Choćby ze względu na chorobę {player:Lewandowskiego}, kontuzje {player:Bendera} i {player:Hummelsa} to ostrożniejsze podejście byłoby nawet wskazane. Przecież w rozmowach przedmeczowych poruszano kwestię pogody w Petersburgu jako ewentualnej przeszkody w dobrej grze. Dodatkowo skupienie się bardziej na {league:Bundeslidze} mogłoby lepiej wyjść {team:Borussii}, która latem po prostu będzie potrzebowała gwarancji gry w następnej edycji {manager:Ligi Mistrzów}, by przyciągnąć następcę dla polskiego napastnika, ale też wzmocnić defensywę. Czy w Petersburgu {manager:Jurgen Klopp} z trenerskiego fanatyka nie powinien stać się pragmatykiem?

To byłoby jednak za łatwe i wbrew zasadom, które tak długo wpajał swoim piłkarzom {manager:Jurgen Klopp}. Czy to półfinał europejskich pucharów z faworyzowanym {team:Realem Madryt}, czy prestiżowy mecz na starcie sezonu z "nowym" {team:Bayernem} {manager:Guardioli} - niemiecki szkoleniowiec nie kalkuluje. Stąd wyjazd do Petersburga posłużył mu nie tyle do potwierdzenia jakości, lecz przypomnienia charakteru {team:Borussii}. To może tylko pierwszy mecz fazy pucharowej, może wyjazd tylko do {country:Rosji}, ale takimi zwycięstwami buduje się mentalną podstawę sukcesu. Czy jest on wciąż możliwy?

Najlepiej {team:Borussia} odpowiedziała na boisku. Nie było kunktatorstwa, ale agresja, otwarte starcia, a w drugiej połowie nawet wymiana ciosów - zwycięska. Przecież słabszą czy w niewielkim stopniu rozbitą drużynę te gole {team:Zenitu} - czy może bardziej decyzje sędziowskie? - by zatrzymały, zniechęciły, sparaliżowały. {team:Borussia} jednak nie dała sobie czasu na przemyślenie sytuacji czy reorganizację defensywy - {player:Piszczek} dalej szarżował po prawym skrzydle, ofensywna trójka wciąż dynamicznie wymieniała pozycje, a Lewandowski rozbijał rywali.

Oczywiście możliwe, że {manager:Luciano Spaletti} zlekceważył {team:Borussię}. Przed meczem na łamach rosyjskiego portalu "Sport Express" rozłożono niemiecką drużynę na czynniki pierwsze, za pomocą grafik i statystyk ukazując jej siłę. Opracowanie było autorstwa analityka Tereka Grozny, który przy jednoczesnym zaznaczeniu, że {team:Zenitowi} trafił się bardzo niewygodny rywal, wskazał aspekty, których Rosjanie powinni się trzymać, by pokonać {team:Borussię}. Miał przede wszystkim zachować zimną krew w obliczu jakiegokolwiek agresywnego pressingu przeciwnika. Jednak tej odwagi wystarczyło na ledwie trzy, cztery minuty. To obrońcy {team:Borussii} wygrywali wszystkie istotne starcia, zaliczyli więcej odbiorów.

- To drużyna, która rośnie dzięki własnej interpretacji futbolu totalnego, nawet jeśli indywidualnie ich potencjał techniczny nie jest najwyższy - pisał dla "La Gazzetta dello Sport" słynny trener {manager:Arigo Sacchi}. W futbolu także czasem mówi się, że zespół jest tak silny, jak jego najsłabsze ogniwo. A po najgorszym meczu w sezonie w Petersburgu {team:Borussia} stała się odzwierciedleniem {player:Roberta Lewandowskiego}. Polski napastnik w pierwszej połowie doskakiwał do {player:Lombaertsa} i {player:Neto}, częściej faulując, niż odbierając piłkę czysto, ale też budując kapitał, który wykorzystał po przerwie - gdy rywal wręcz bał się podchodzić, zostawiał miejsce, pozwalał na kluczowe dla akcji podania i decydujące o wyniku strzały. Jeszcze kogoś dziwi podwyżka zarobków przed jego ostatnim sezonem?

Tak jak {team:Borussia} bywała w ostatnich latach współczesną i autorską wersją "futbolu totalnego", tak salony {league:Ligi Mistrzów} wyzwalają w {player:Lewandowskim} postać "napastnika kompletnego". Dziś nie wyglądał jak piłkarz, którego {manager:Guardiola} dopiero dostanie do piłkarskiej obróbki w doborowym towarzystwie, ale taki, który potrafi dźwigać na swoich barkach grę całego zespołu. Tak jak jego odejście odbierano jako najgorszą wiadomość dla {team:Borussii}, tak najlepszą będą słowa {player:Lewandowskiego} o chęci realizacji celów, misji w jego ostatnim półroczu w Dortmundzie. W obliczu takich popisów można śmiało założyć, że nie pozostaną one tylko pustą zapowiedzią rzuconą na facebookową tablicę oficjalnego profilu.






