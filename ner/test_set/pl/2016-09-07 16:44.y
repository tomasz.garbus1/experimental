::{league:Premier League}. {player:Zlatan Ibrahimović} wysłał prezent {player:Claudio Bravo} przed derbami Manchesteru Piłka nożna - Sport.pl::
@2016-09-07 16:44

Obie drużyny wygrały swoje pierwsze trzy mecze. Liderem są "{team:The Citizens}", którzy mają bilans bramkowy 9-3. {team:United} jest trzecie z bilansem 6-1 (druga jest {team:Chelsea}). Najlepszymi strzelcami ligi są napastnicy obu klubów z Manchesteru - {player:Sergio Aguero} i {player:Ibrahimović} właśnie (po 3 gole). Ale {player:Aguero} nie zagra, bo jest zawieszony za uderzenie łokciem rywala. {player:Ibrahimović} skupił się za to na ostrzeżeniu {player:Bravo}, który został nowym bramkarzem {team:Manchesteru City}.

Na Instagramie zamieścił wideo, na którym pakował specjalny prezent dla Chilijczyka. {player:Ibrahimović} dał taki podpis: - Witaj w Manchesterze! Tu trochę sprzętu treningowego, przyda ci się. Do zobaczenia w sobotę!

Film zamieszczony przez użytkownika IAmZlatan (@iamzlatanibrahimovic) 7 Wrz, 2016 o 1:02 PDT

Derby Manchesteru w sobotę o 13.30. To może być "najdroższy" mecz w historii piłki nożnej. Więcej tutaj.

El. {league:MŚ 2018}. {country:Kazachstan} - {country:Polska}. Wypadek przy pracy? "{manager:Nawałka} pewnie dokręci śrubę na kolejnym zgrupowaniu"

Ranking najbardziej przepłaconych transferów letniego okienka. Jest jeden Polak!








