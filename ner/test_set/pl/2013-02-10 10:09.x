::Premier League. Angielskie media: Dobre mecze Boruca i Szczęsnego Piłka nożna - Sport.pl::
@2013-02-10 10:09
url: http://www.sport.pl/pilka/1,65080,13377034,Premier_League__Angielskie_media__Dobre_mecze_Boruca.html


Śledź występy polskich bramkarzy na żywo w swoim telefonie. Ściągnij aplikację Sport.pl Live

Zdecydowanie mniej pracy miał Boruc. Jego Southampton wygrał z mistrzami Anglii Manchesterem City 3:1. Polak przy bramce nie zawinił. W pierwszej połowie nie musiał w ogóle interweniować, a w drugiej popisał się przy obronie strzału Aguero. "Goście cały czas się bronili i liczyli na kontry. Po jednej z nich Artur Boruc spisał się bez zarzutu, gdy sparował uderzenie Sergio Aguero" - napisał Sky. Występ Polaka ocenił na 6 w dziesięciostopniowej skali.

"Miał niespodziewanie mało do roboty w pierwszej połowie. Przy golu strzelonym przez Dżeko był minimalnie źle ustawiony. Później utrzymał dwubramkowe prowadzenie swojego zespołu, gdy zatrzymał strzał Aguero" - można przeczytać na goal.com. Ten angielski portal dał Polakowi trzy gwiazdki w pięciostopniowej skali.

Arsenal Szczęsnego wygrał na wyjeździe z Sunderlandem 1:0. Bramkarz Kanonierów zebrał jeszcze lepsze recenzje niż Boruc. Sky uznało jedną z interwencji Polaka za najlepszą z tego meczu: "Obaj bramkarze spisywali się dobrze, ale to Szczęsny miał ważniejszą interwencję. W akrobatyczny sposób wybronił strzał głową Fletchera". Szczęsny został oceniony na 8. Lepszą notę dostał tylko Cazorla - 9.

Na goal.com Szczęsny dostał 3,5 gwiazdki. Tu z kolei najlepszy był Jack Wilshere z czterema gwiazdkami. "Dał bardzo mało w tym meczu. Jego defensywa spisywała się bez zarzutu. Wyłapywał dośrodkowania i uratował punkt w końcówce, gdy świetnie obronił strzał Fletchera" - można przeczytać o grze Polaka.






