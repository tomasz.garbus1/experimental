::Remis, który niepokoi kibiców Piłka nożna - Sport.pl::
@2010-05-17 18:00
url: http://www.sport.pl/pilka/1,128220,7895354,Remis__ktory_niepokoi_kibicow.html


Pogoń i Warta tej wiosny skompletowały ledwie po 10 punktów. Oba zespoły łączyło jeszcze coś - bierność obrońców przy stałych fragmentach gry. Podopieczni Marka Czerniawskiego w ostatnich 3 spotkaniach stracili przez gapiostwo w kryciu w sumie 6 bramek (!), a podopieczni Piotra Mandrysz zawalili gole z ŁKS i Górnikiem.

W sobotę rzuty wolne znów dały o sobie znać. Goście nie potrafili upilnować Krzysztofa Hrymowicza, chociaż doskonale wiedzieli, jak zachowuje się pod bramką przeciwnika. To była 50. minuta meczu. O pierwszej części lepiej nie wspominać, bo mocno wiało nudą. Jakby to przewidzieli fani "granatowo-bordowych", którzy na stadionie Floriana Krygiera stawili się w liczbie około 2 tys.

- Wiemy, że pokłosiem tego była nasza przegrana z Górnikiem Zabrze - przyznaje Mandrysz. - Poza tym pogoda była nieciekawa, a stadion mamy taki, że kibic przychodząc na mecz patrzy wcześniej w niebo.

Jednemu kibicowi faktycznie udzieliła się nuda i głupota. W pierwszej połowie zaczął świecić zielonym laserem po oczach poznańskich zawodników.

- Przy stałych fragmentach gry nasz bramkarz został kilka razy oślepiony, podobnie jak obrońcy - opisuje Marek Czerniawski. - Przykro mi, ale wiem, że zdarzają się tacy ludzie.

Sobotni pojedynek, który dla Pogoni był o ligową "pietruszkę", wprawdzie nie był poligonem doświadczalnym, ale trener dokonał pewnych zmian w wyjściowej jedenastce. Zdjął ostatnio fatalnie grającego środkowego obrońcę Omara Jaruna, w jego miejsce przesunął uniwersalnego Macieja Mysiaka. Piotra Petasza, który będzie pauzował w finale PP za kartki, zastąpił na lewej flance z powodzeniem Przemysław Pietruszka (ostatnio w środku pola). Do ważniejszych posunięć zaliczamy także grę w podstawowym składzie Dariusza Zawadzkiego po ponad miesięcznej przerwie. Zagrał na nietypowej dla siebie pozycji - tuż za napastnikiem. Trener zdjął go po 45. min.

- Próbowałem różnych wariantów pod kątem meczu pucharowego, a niektórym piłkarzom chciałem dać odpocząć. Pierwsza połowa zmusiła mnie jednak do korekt w składzie - podkreśla Mandrysz.

I tak po przerwie z ławki weszli Olgierd Moskalewicz i Marcin Bojarski, później Maksymilian Rogalski. I dobrze, bo gra się poprawiła. Jednak po strzelonej bramce szczecinianie jakby oddali inicjatywę gościom, którzy od 81. min grali w osłabieniu (w ostatnich minutach w "dziewiątkę" przez kontuzję).

- Trochę mnie zaskoczyło, że Pogoń zadowoliła się jedną bramką. Po meczu żartowałem, że czerwona kartka nam pomogła, bo wpłynęła na desperację w zespole i w osłabieniu wyrównaliśmy - cieszył się Czerniawski.

Pogoń zalicza trzeci mecz bez zwycięstwa. "Testowe" spotkanie nie napawało optymizmem przed finałem w Bydgoszczy (22 maja).

Pogoń Szczecin - Warta Poznań 1:1 (0:0)

Bramki. Pogoń: Hrymowicz (50. po dośrodkowaniu z rzutu wolnego Pietruszki). Warta: Magdziarz (85. asysta ze środka pola Iwanickiego).

Pogoń: Janukiewicz - Nowak, Mysiak, Hrymowicz, Woźniak, Wólkiewicz (76. Rogalski), Koman (46. Bojarski), Zawadzki (46. Moskalewicz Ż), R. Mandrysz, Pietruszka, Lebedyński.

Warta: Radliński - Ignasiński, Wichtowski Ż, Strugarek, Otuszewski, Bekas (75. Miklosik), Jankowski ŻŻCZ, Magdziarz, Seweryn Ż, Iwanicki, Kaźmierowski.

Sędziował Marcin Słupiński (Łódź)

Widzów : 2000

Oceny portowców: Janukiewicz 4, Nowak 2, Mysiak 2, Hrymowicz 3, Woźniak 3, Wólkiewicz 2, Rogalski 3, Koman 1, Bojarski 3, Zawadzki 1, Moskalewicz 2, Mandrysz 3, Pietruszka 4, Lebedyński 2.






