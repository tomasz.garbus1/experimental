::Transfery. {player:Drogba} prosi FIFA o zgodę na wypożyczenie Piłka nożna - Sport.pl::
@2012-11-21 13:42


O otrzymaniu wniosku FIFA poinformowała w środę. Federacja rozważa, czy udzielić {player:Drogbie} zgody na odstępstwo o przepisów. Sezon w chińskiej ekstraklasie zakończył się w listopadzie. Obowiązujące zasady zabraniają 34-latkowi zmiany klubu do 1 stycznia.

Zdarzało się już, że międzynarodowa federacja zezwalała piłkarzom na ominięcie zasad. Tak było w przypadku piłkarzy z {league:Major League Soccer} - {player:Davida Beckhama}, {player:Thierry'ego Henry'ego} i {player:Landona Donovana}

{player:Drogba} w 2012 roku podpisał dwuletni kontrakt z klubem z Szanghaju. Po wygraniu {league:Ligi Mistrzów} odszedł z {team:Chelsea}. Możliwe, że wróci do Londynu, gdzie jest legendą. {team:Chelsea} walczy o awans do fazy pucharowej {league:LM}. W ostatnim meczu musi wygrać z duńskim {team:Nordsjaelland} i liczyć na przegraną {team:Juventusu Turyn} w drugim meczu grupy E. Nawet jeśli Iworyjczyk trafi na {stadium:Stamford Bridge} przed kluczowym spotkaniem, nie będzie mógł w nim wystąpić. Do rozgrywek składy zgłasza się przed ich rozpoczęciem.

Zmiany w {team:Chelsea}. {manager:Di Matteo} nie jest już trenerem londyńczyków »






