::Primera Division. Barca tworzy dream team Piłka nożna - Sport.pl::
@2010-05-19 23:22
url: http://www.sport.pl/pilka/1,65082,7908946,Primera_Division__Barca_tworzy_dream_team.html


Sport.pl na Facebooku! Sprawdź nas »

O odejściu Villi mówiło się od miesięcy. Valencia ma 500 mln euro długu, brakuje jej pieniędzy na dokończenie budowy stadionu. 29-letniego napastnika, który przez ostatnie pięć lat w 212 meczach strzelił 129 goli, chciały sprowadzić najlepsze zespoły Premier League, Real Madryt i Barcelona. Wygrali Katalończycy, w środę król strzelców Euro 2008 podpisał czteroletnią umowę, będzie zarabiał 7 mln euro rocznie.

Villa w Barcy! Fabregas za chwilę. Ibra będzie musiał odejść?
Teraz władze Barcy zabrały się do ściągnięcia Fabregasa. 23-letni pomocnik powiedział już trenerowi Arsene'owi Wengerowi, że chce odejść. Arsenal milczy, ale wiadomo, że łatwo piłkarza nie puści. Hiszpan to lider, wokół którego francuski szkoleniowiec budował przez ostatnie lata drużynę.

Od kilku miesięcy hiszpańska prasa, powołując się na przyjaciół Fabregasa, twierdziła jednak, że piłkarz chce wrócić do Katalonii. W juniorach Barcy Cesc grał w zespole z Leo Messim, odszedł, gdy miał 16 lat, bo nie widział szans na regularne występy w pierwszej drużynie.

W Arsenalu został gwiazdą Premier League, ale zabrakło mu sukcesów drużynowych. Jego kolekcja trofeów ogranicza się do zdobytego pięć lat temu Pucharu Anglii. - Świetnie, że chce do nas wrócić, ale teraz ruch należy do Arsenalu. Cesc jest dla nich ważnym piłkarzem i musimy to uszanować - powiedział dyrektor sportowy Barcy Txiki Begiristain.

Katalończycy proponują za Fabregasa 35 mln euro, Arsenal chce 10 mln więcej. Barca może też zapłacić i wysłać do Londynu Yaye Toure, który twierdzi, że Pep Guardiola zbyt często sadza go na ławce rezerwowych.

Nie wiadomo jeszcze, jak trener Barcy pomieści tyle gwiazd w jedenastce. Z klubu prawdopodobnie odejdzie Thierry Henry, który rozmawia z New York Red Bulls.

O transferze Villi poinformował Jaume Ferrer, kandydat popierany przez prezesa Joana Laportę w czerwcowych wyborach. Laporta startować nie może, bo rządzi już dwie kadencje. Inni kandydaci twierdzą, że zakupy Villi i Fabregasa mają pomóc w zwycięstwie Ferrerowi. Według ostatnich sondaży faworytem jest Sandro Rosell, były członek zarządu klubu odpowiedzialny za sprowadzenie do zespołu Ronaldinho, wróg obecnego prezesa. Głosować chce na niego 55 proc. kibiców, na Ferrera - tylko 10 proc.

Sevilla z Pucharem Króla »






