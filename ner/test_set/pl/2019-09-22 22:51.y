::{team:Real Madryt} pokonał lidera. Niedoceniana gwiazda bohaterem meczu Piłka nożna - Sport.pl::
@2019-09-22 22:51

{manager:Zidane} planował rewolucję w {team:Realu}, ale niewiele z tego wyszło. Co teraz zrobi?
REKLAMA






Pozycja Zinedine&aposa {manager:Zidane}&aposa w {team:Realu Madryt} nie jest pewna, choć "{team:Królewscy}" w pierwszych czterech meczach zdobyli 8 punktów. Kibice coraz głośniej domagają się zmiany szkoleniowca, a media wymieniają wśród kandydatów do objęcia {team:Realu} m.in. {manager:Jose Mourinho}. Kolejne mecze są więc okazją do odbudowy pozycji francuskiego trenera.Niewielu sądziło, że {team:Real Madryt} stać na korzystny wynik w meczu przeciwko {team:Sevilli}. Pierwsze 15 minut potwierdzało ich obawy. Potem jednak "{team:Królewscy}" zaczęli grać tak, jak na nich przystało. Do przerwy utrzymywał się bezbramkowy remis, choć swoje okazje mieli {player:Carvajal} i {player:Hazard}. Fenomenalnie bronił jednak {player:Vaclik}.{player:Benzema} bohaterem {team:Realu} Po przerwie długo Real nie potrafił realnie zagrozić bramce {team:Sevilli}. To jednak zmieniło się w 64. minucie, gdy {team:Karim Benzema} wykorzystał perfekcyjne dośrodkowanie {player:Carvajala}. Gospodarze walczyli jak mogli o doprowadzenie do wyrównania, w 87. minucie piłka znalazła się nawet w bramce {player:Courtois}, ale {player:Chicharito} znalazł się na pozycji spalonej.Dzięki tej wygranej {team:Real Madryt} przesunął się na pozycję lidera {league:La Ligi}, mając tyle samo punktów co {team:Athletic Bilbao}, który pokonał w niedzielę {team:Alaves} 2:0. {team:Sevilla} spadła na 5. pozycję, mając punkt mniej.





