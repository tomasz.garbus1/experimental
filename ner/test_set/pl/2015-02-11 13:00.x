::Wychowanek Stomilu zagra w Arce. Ciężko na to pracował Piłka nożna - Sport.pl::
@2015-02-11 13:00
url: http://www.sport.pl/pilka/1,65044,17390319,Wychowanek_Stomilu_zagra_w_Arce__Ciezko_na_to_pracowal.html


Już kilka lat temu mówiło się o Karłowiczu jako obiecującym piłkarzu młodego pokolenia. Swoją karierę rozpoczynał w Stomilu Olsztyn, którego barwy reprezentował przez pięć sezonów. W 2009 roku jego talent dostrzegł Władysław Żmuda, ówczesny szkoleniowiec reprezentacji Polski do lat 17.

Jeszcze w czerwcu tego samego roku, po występie Karłowicza w biało-czerwonych barwach, na testy zaprosiła go Legia Warszawa. Wtedy jednak na przeszkodzie stanęła kontuzja kręgosłupa, która uniemożliwiła olsztynianinowi pokazanie pełni swoich umiejętności.

- Pierwsze kroki w roli piłkarza stawiał pod wodzą Janusza Czerniewicza - mówi Adam Łopatko, drugi trener pierwszoligowego Stomilu. - Od zawsze jego największym atutem była lewa noga. Co więcej, jest silny fizycznie i jak na swój wiek [21 lat - red.] zaawansowany pod względem technicznym.

Piotr Karłowicz trafił następnie do trzecioligowej Mrągowii Mrągowo (2010-2011) i właśnie tam grając, zwrócił na siebie uwagę skautów Wisły Kraków. Wystarczy wspomnieć, że w rundzie wiosennej zdobył 11 bramek w 13 meczach. O transferze do 13-krotnego mistrza Polski ostatecznie przesądziły dwudniowe testy w stolicy Małopolski.

Kariera młodego olsztynianina nie była jednak usłana różami. - Nie wszystko wyglądało tak, jak to sobie wyobrażałem i jakbym tego sobie życzył. Podpisywałem wówczas kontrakt z zespołem mistrza kraju, który aspirował do gry w Lidze Mistrzów - opowiada Piotr Karłowicz, teraz zawodnik Arki Gdynia. - Miałem możliwość trenowania ze znakomitymi piłkarzami. Dla chłopaka z Olsztyna, który pół roku wcześniej grał jeszcze w Mrągowie, to było coś wyjątkowego.

Mierzący 188 cm wzrostu zawodnik nie dostał szansy na debiut w seniorskiej drużynie Białej Gwiazdy. Co gorsza, również w rezerwach nie wiodło mu się najlepiej. W Młodej Ekstraklasie w trykocie Wisły rozegrał 50 meczów i strzelił sześć goli.

Aż w końcu, w sezonie 2013/2014 przeszedł do pierwszoligowych Wigier Suwałki, gdzie wystąpił w 29 spotkaniach, czterokrotnie trafiając do bramki przeciwnika. W rundzie jesiennej tego sezonu zagrał czterokrotnie, ale ani razu nie wpisał się na listę strzelców.

- Kiedy Piotrek występował w Suwałkach, zdobywał dużo ważnych bramek - podkreśla Łopatko. - Transfer do Arki jest dla niego bardzo ważny. Jeśli w tym klubie mu się powiedzie, to jego kariera bardzo się rozwinie.

Olsztynianin z drużyną z Trójmiasta ćwiczy od początku przygotowań do rundy rewanżowej. Miał też okazję wystąpić w sparingach ze Stomilem i Olimpią Grudziądz. - Moje przyjście tutaj jest sportowym awansem. Niewielu wróżyło mi, po rozstaniu się z własnej inicjatywy z Wigrami, tak dobrego klubu. Ciężką pracą na testach pokazałem jednak, że potrafię grać w piłkę i dostałem kontrakt. Jest to klub z tradycjami i nie ma co porównywać go do beniaminka z Suwałk - tłumaczy wychowanek Stomilu.

W żółto-niebieskich barwach na pozycji napastnika Karłowicz będzie rywalizował o miejsce w składzie z doświadczonymi Pawłem Abbottem i Marcusem da Silvą, a także z młodymi Michałem Szubertem (23lata) oraz Maciejem Wardzińskim (20 lat). - Przychodząc do Arki, widziałem, jak dużo jest piłkarzy występujących na mojej pozycji, ale nie boję się rywalizacji. Mogę trenować i walczyć o miejsce w jedenastce nawet z najlepszymi. W klubie jesteśmy kolegami, ale na zajęciach każdy ze wszystkich sił walczy o swoje - opowiada Piotr Karłowicz.

Przypomnijmy, że to nie jedyny olsztyński akcent w Arce Gdynia. W pierwszoligowej ekipie występują także Grzegorz Lech, Marcin Warcholak czy Michał Renusz. - Łatwiej było wejść do szatni, w której są znajome twarze. Z Grześkiem Lechem znam się dość długo. Jak wchodziłem do Stomilu, mając 15-16 lat, to był on taką małą, żywą legendą. Michał też miał swój epizod w drużynie, ale znamy się krócej - mówi Karłowicz.

Nowy nabytek gdynian cały czas śledzi też losy olsztyńskiego klubu. I nie ukrywa, że jest zaskoczony kłopotami finansowymi OKS-u. - Trudno się nie interesować klubem, który ma się w sercu. Jest mi żal chłopaków, którzy trenują w trudnych warunkach i oddają serce tej drużynie - mówi napastnik Arki. - Zrobili fantastyczne piąte miejsce w lidze, a tu nagle nie mają sponsora [9 stycznia ze współpracy wycofała się Galeria Warmińska - red.] i nie starcza im pieniędzy na treningi. Taka sytuacja jest nie do pomyślenia w tych czasach. Myślałem, że w końcu ten klub będzie funkcjonował normalnie.

I dodaje: - Sam pamiętam takie przypadki, że jak nie wiadomo o co chodzi, to chodzi o pieniądze. Dlatego życzę chłopakom przede wszystkim zdrowia i wierzę, że wszystko się poukłada.

Więcej o piłkarzach Stomilu czytaj na olsztyn.sport.pl.






