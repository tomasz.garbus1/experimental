::Serie A. 'Hańba i tchórzostwo' we Włoszech po wydarzeniach w Genui Piłka nożna - Sport.pl::
@2012-04-23 16:39
url: http://www.sport.pl/pilka/1,65083,11598310,Serie_A___Hanba_i_tchorzostwo__we_Wloszech_po_wydarzeniach.html


Tak się kibicuje! Trybuna Kibica otwarta dla wszystkich

"Hańba i tchórzostwo" - to tytuł z La Repubbliki, która pisze, że "stadiony we Włoszech są miejscami, gdzie prawo nie ma wstępu, a silniejszy jest w stanie odrzeć każdego z jego godności". W drugiej połowie meczu, który Genoa przegrywała już 0:4 na boisko poleciały race i świece dymne. Sędzia przerwał spotkanie. Kibole weszli na barierę oddzielającą trybuny od boiska i kazali piłkarzom zdjąć koszulki, wcześniej krzycząc, że nie są ich warci.

La Stampa pisze o zamroczonym kapitanie Genoi Marco Rossim, który "zmienił się z kapitana w chłopca z pralni". Na zdjęciach i nagraniach widać Rossiego, który pozbierał koszulki kolegów. Koszulki nie zdjął napastnik Giuseppe Sculli, który poszedł negocjować z kibolami.

Zobacz całe zajście na Z czuba.tv »

- Miałem wrażenie, że śnię - powiedział były piłkarz Milanu, Demertio Albertini, który jest obecnie wiceprezesem włoskiej federacji. - Wystarczy porównać nas do lig zagranicznych, by zorientować się z jakimi problemami musimy się zmagać - dodał. - To musi się skończyć bardzo szybko. Przejście nad tym do porządku dziennego robi zbyt wiele złego włoskiemu futbolowi -grzmi Carlo Ancelotti, były trener m.in. Juventusu i Milanu, teraz pracujący w Paris Saint Germain.

Incydenty związane z kibolami zdarzają się we Włoszech często. W 2004 r. derby Rzymu między Romą a Lazio zostały przerwane przez kiboli, którzy wtargnęli na boisko. Z przywódcami negocjował wtedy kapitan Romy Francesco Totti. Dwa lata temu na tym samym stadionie w Genui kibole przerwali mecz eliminacji Euro 2012 między Włochami a Serbią. Włosi przegrali starania o organizację Euro w 2012 i w 2016 r. m.in. przez złą sławę swoich kiboli i zły stan przestarzałych stadionów.

Dla klubu z Genui to tylko kolejny z kłopotów. Drużyna, która w tym sezonie mierzyła w puchary, jest 17. w tabeli Serie A. Na pięć kolejek przed końcem sezonu ma zaledwie punkt przewagi nad strefą spadkową. Prezes klubu Enrico Preziosi zwolnił po meczu trenera Alessandro Malesianiego, który został już zwolniony w grudniu, a przed miesiącem znów zatrudniony. W czasie całego zajścia Preziosi stał na murawie ale nie reagował, gdy jego piłkarze ściagąli koszulki.

Szef włoskiej federacji Mauro Beretta wezwał policję do zaostrzenia postępowania wobec kiboli. Minister spraw wewnętrznych Anna Maria Cancellieri powiedziała, że osoby odpowiedzialne za wydarzenia w Genui zostały już zidentyfikowane.

Kibole Genoi » przerwali mecz ze Sieną






