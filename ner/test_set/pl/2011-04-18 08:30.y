::{league:Premier League}. {manager:Dalglish} do {manager:Wengera}: Był karny, odp... się! Piłka nożna - Sport.pl::
@2011-04-18 08:30


{team:Arsenal} - {team:Liverpool}. {manager:Dalglish} wyzywa {manager:Wengera} [WIDEO]

Bramki z meczu {team:Arsenal} - {team:Liverpool} [WIDEO]

Mecz {team:Arsenalu} z {team:Liverpoolem} został przedłużony, gdyż siedem minut trwało udzielanie pomocy {player:Jamiemu Carragherowi}, który w 56. minucie meczu zderzył się z kolegą z zespołu i stracił przytomność. Sędzia doliczył ten czas do regulaminowych 90 minut.

W rezultacie w 98. minucie {player:Spearing} przewrócił w polu karnym {player:Fabregasa}, a jedenastkę na gola precyzyjnym strzałem zamienił {player:Van Persie}. Sędzia najwyraźniej nie chciał kończyć meczu w takim momencie, i pozwolił {team:Liverpoolowi} na rozegranie jeszcze jednej akcji. Po rzucie wolnym {player:Suareza} piłka wylądowała pod nogami {player:Lucasa}, na którego wpadł {player:Eboue}.

Ostatecznie sędzia podyktował jedenastkę dla {team:Liverpoolu}, którą mocnym uderzeniem wykorzystał {player:Dirk Kuyt}. {player:Wojciech Szczęsny} był bez szans. Zaraz po wyrównującej bramce zakończył mecz. Na zegarze była już wtedy 102. minuta.

{manager:Dalglishowi} puszczają nerwy


Ze stratą punktów nie mógł się pogodzić {manager:Aresene Wenger}. Tuż po końcowym gwizdku rozłożył szeroko ręce i ruszył w kierunku linii bocznej manifestując swoją dezaprobatę.

Reakcja Francuza doprowadziła do szału trenera gości {manager:Kenny'ego Dalglisha}. Szkoleniowiec {team:Liverpoolu} stanął na drodze {manager:Wengera} i zaczął na niego krzyczeć. - To był rzut karny, odpieprz się! - słychać wyraźnie na zapisie wideo. {manager:Wenger} macha ręką i zawraca. - To jest rzut karny! Odpierdol się! - rzuca mu jeszcze na odchodne {manager:Dalglish}.

Niesamowite emocje w meczu {team:Arsenal} - {team:Liverpool} »






