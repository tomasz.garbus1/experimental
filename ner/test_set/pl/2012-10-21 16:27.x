::W Stróżach lepszy Kolejarz. Co się dzieje z Zawiszą? Piłka nożna - Sport.pl::
@2012-10-21 16:27
url: http://www.sport.pl/pilka/1,133416,12712848,W_Strozach_lepszy_Kolejarz__Co_sie_dzieje_z_Zawisza_.html




Jesteś kibicem i jesteś z Bydgoszczy? Musisz zostać fanem Facebooka Sport.pl Bydgoszcz »

Choć to dopiero druga porażka Zawiszy w obecnym sezonie, to powinna ona jednak poważnie zaniepokoić działaczy, szkoleniowców i samych piłkarzy. Po świetnym początku sezonu bydgoski zespół ostatnio ciuła punkty, gra podopiecznych Jurija Szatałowa pozostawia wiele do życzenia. W Stróżach szkoleniowiec po raz kolejny zdecydował się na zmiany w wyjściowym składzie. Tym razem wpuścił nawet dwóch napastników. Efekt jednak był mierny, bo do nieskutecznego Pawła Abbotta dołączył równie nieskuteczny Tomasz Chałas. Jeśli do tego dodamy kolejne błędy obrońców, to wychodzi obraz zespołu, który przeżywa kryzys. Bydgoszczanie nadal znajdują się w czołówce tabeli, ale w niedzielę stracili pozycję wicelidera. W dodatku teraz czekają nas mecze z bardzo silnymi drużynami. Zawisza musi się obudzić, bo w przeciwnym razie wiosną o awans będzie niezwykle trudno.



Kolejarz - Zawisza 2:1 (1:1)

Bramki: 0:1 Geworgian (29.), 1:1 Niane (44. karny), 2:1 Gajtkowski (47.)

Kolejarz: Forenc - Pietrzak (46. Stefanik), Markowski, Szufryn, Walęciak - Wolański, Niane, Bocian (67. Arłukowicz), Giesa, Nitkiewicz - Gajtkowski (78. Pieczara).

Zawisza: Kaczmarek - Stefańczyk, Kopacz, Skrzyński, Jankowski - Geworgian, Zawistowski, Hermes, Błąd (56. Masłowski) - Chałas (63. Leśniewski), Abbott (76. Ostalczyk).



Inne wyniki 13. kolejki: Flota Świnoujście - Cracovia 1:2, Arka Gdynia - Olimpia Grudziądz 0:1, GKS Katowice - Okocimski Brzesko 1:2, Stomil Olsztyn - Miedź Legnica 1:2, Dolcan Ząbki - GKS Tychy 0:1, Warta Poznań - Polonia Bytom 2:1, ŁKS Łódź - Sandecja Nowy Sącz 3:2, Bogdanka Łęczna - Termalica Bruk-Bet Nieciecza 1:0



Tabela I ligi





1. Flota133426-6


2. Cracovia122620-12


3. Zawisza132526-9


4. Termalica122419-11


5. Miedź132218-13


6. Olimpia132117-13


7. GKS T.132010-9


8. Bogdanka131915-16


9. Arka131813-9


10. Warta131821-19


11. Kolejarz131516-19


12. Sandecja131514-28


13. GKS K.131416-19


14. Dolcan131316-19


15. Stomil131114-19


16. Okocimski131112-21


17. ŁKS131015-30


18. Polonia1349-25







