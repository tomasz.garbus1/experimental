::{league:Premier League}. Angielskie media: Dobre mecze {player:Boruca} i {player:Szczęsnego} Piłka nożna - Sport.pl::
@2013-02-10 10:09


Śledź występy polskich bramkarzy na żywo w swoim telefonie. Ściągnij aplikację Sport.pl Live

Zdecydowanie mniej pracy miał {player:Boruc}. Jego {team:Southampton} wygrał z mistrzami {country:Anglii} {team:Manchesterem City} 3:1. Polak przy bramce nie zawinił. W pierwszej połowie nie musiał w ogóle interweniować, a w drugiej popisał się przy obronie strzału {player:Aguero}. "Goście cały czas się bronili i liczyli na kontry. Po jednej z nich {player:Artur Boruc} spisał się bez zarzutu, gdy sparował uderzenie {player:Sergio Aguero}" - napisał Sky. Występ Polaka ocenił na 6 w dziesięciostopniowej skali.

"Miał niespodziewanie mało do roboty w pierwszej połowie. Przy golu strzelonym przez {player:Dżeko} był minimalnie źle ustawiony. Później utrzymał dwubramkowe prowadzenie swojego zespołu, gdy zatrzymał strzał {player:Aguero}" - można przeczytać na goal.com. Ten angielski portal dał Polakowi trzy gwiazdki w pięciostopniowej skali.

{team:Arsenal} {player:Szczęsnego} wygrał na wyjeździe z {team:Sunderlandem} 1:0. Bramkarz {team:Kanonierów} zebrał jeszcze lepsze recenzje niż {player:Boruc}. Sky uznało jedną z interwencji Polaka za najlepszą z tego meczu: "Obaj bramkarze spisywali się dobrze, ale to {player:Szczęsny} miał ważniejszą interwencję. W akrobatyczny sposób wybronił strzał głową {player:Fletchera}". {player:Szczęsny} został oceniony na 8. Lepszą notę dostał tylko {player:Cazorla} - 9.

Na goal.com {player:Szczęsny} dostał 3,5 gwiazdki. Tu z kolei najlepszy był {player:Jack Wilshere} z czterema gwiazdkami. "Dał bardzo mało w tym meczu. Jego defensywa spisywała się bez zarzutu. Wyłapywał dośrodkowania i uratował punkt w końcówce, gdy świetnie obronił strzał {player:Fletchera}" - można przeczytać o grze Polaka.






