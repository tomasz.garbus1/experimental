::Eliminacje MŚ 2018. Robert Lewandowski: biało-czerwona maszyna Nawałki Reprezentacja Polski::
@2016-11-12 10:23
url: http://www.sport.pl/pilka/1,65037,20967802,eliminacje-ms-2018-robert-lewandowski-bialo-czerwona-maszyna.html


Obserwuj @LukaszJachimiak

Gol w Astanie, hat-trick z Danią, ratująca nas bramka wbita Armenii w 95. minucie, teraz dwie petardy w Bukareszcie - po czterech kolejkach eliminacji mundialu "Lewy" z siedmioma trafieniami jest ich najlepszym strzelcem. Licząc z eliminacjami poprzedniego wielkiego turnieju, Euro 2016, kapitan naszej kadry ma serię dziewięciu spotkań z chociaż jednym zdobytym golem. Strzelił w nich aż 16 bramek.

Król strzelców kwalifikacji do ME 2016 w sumie zanotował w nich 13 trafień. Razem z siedmioma golami z trwających eliminacji MŚ Lewandowski ma 20 bramek w meczach o stawkę. Do tego dodajmy gola strzelonego Portugalii w ćwierćfinale Euro 2016 i mamy błyskawiczną odpowiedź na pytanie, czy aby lider Polski nie trafia głównie wtedy, kiedy gra ona towarzysko. Asysty "Lewy" też zalicza praktycznie tylko w meczach o coś. Ze wszystkich sześciu pięć zanotował w walce o wyjazd Polski na francuskie Euro, jedną w towarzyskim meczu z Islandią.

Lewandowski "robi" ponad bramkę na mecz. Ronaldo i Messi nie


Liczby Lewandowskiego są rewelacyjne i świetnie obrazują, jaką maszyną stał się, grając w reprezentacyjnych barwach. Jego średnia gola na mecz w trzech latach gry pod wodzą Nawałki wynosi 0,89. Po zsumowaniu goli (24) i asyst (sześć) łatwo policzyć, że kapitan ma średnio w spotkaniu udział przy 1,11 bramki dla drużyny.

Cristiano Ronaldo w swoich ostatnich 27 meczach dla Portugalii zdobył 22 gole i zaliczył pięć asyst. Lionel Messi w swych ostatnich 27 występach w barwach Argentyny zdobył 16 bramek i dołożył 10 asyst. Liczby imponujące, ale dwaj najwięksi współcześni piłkarze świata ustępują Lewndowskiemu, bo pierwszy z wymienionych miał udział przy 27, a drugi przy 26 bramkach swojej drużyny. A gdyby ktoś chciał umniejszać dorobek Polaka, twierdząc, że błyszczał m.in. przeciw Gibraltarowi (w dwóch meczach sześć goli i trzy asysty), to musi wiedzieć, że Ronaldo swoje statystyki miał okazję poprawiać grając m.in. z Andorą i Wyspami Owczymi, a Messi z Hongkongiem czy Panamą.

Jeszcze większa różnica między Polakiem a Portugalczykiem i Argentyńczykiem jest w meczach o punkty wielkich imprez oraz eliminacji do nich "Lewy" ma w ostatnich trzech latach bilans 19 takich występów, 21 goli i pięciu asyst. Ronaldo w tym czasie rozegrał 19 spotkań o stawkę, zdobywając w nich 18 bramek i notując cztery asysty. Liczby Messiego to 23 gry, 12 goli i osiem asyst.

Neymar też przegrywa


Z Lewandowskim równać może się tylko Neymar. On w swoich ostatnich 27 meczach w reprezentacji Brazylii zdobył 20 bramek i zanotował 11 asyst, a w 14 meczach o stawkę, jakie rozegrał w trzech ostatnich latach, trafił do bramki dziewięć razy i ośmiokrotnie otworzył do niej drogę któremuś z kolegów.

Krótkie podsumowanie dla każdego, komu zakręciło się w głowie: Lewandowski miał udział przy 1,37 bramki Polski w meczu o stawkę, uśredniając jego występy w takich spotkaniach w trzech ostatnich latach. Średnia Neymara to 1,21, Ronaldo - 1,16, a Messiego - 0,87.

El. MŚ. Rumunia - Polska 0:3. Lewandowski komentuje incydent z racą: Przez parę minut nie za bardzo wiedziałem, co się dzieje

Race w Rumunii? UEFA zamyka stadion Legii [MEMY PO POLSCE]






