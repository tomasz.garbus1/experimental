::Urlopowany. Marcin Brosz nie poprowadzi już Podbeskidzia Piłka nożna - Sport.pl::
@2009-11-02 14:31
url: http://www.sport.pl/pilka/1,64946,7212826,Urlopowany__Marcin_Brosz_nie_poprowadzi_juz_Podbeskidzia.html


Podbeskidzie, które przez dwa kolejne sezony było blisko awansu, tej jesieni spisuje się fatalnie. Górale przegrali trzy kolejne mecze na własnym stadionie. Po 16 meczach bielszczanie zajmują trzynaste miejsce, mają ledwie punkt przewagi nad strefą spadkową.

Kim jest nowy trener? Tomasz Świderski, nauczyciel wychowania fizycznego w jednej z bielskich podstawówek, od kilku lat prowadzi młodzieżowe grupy Podbeskidzia. - Ma 36 lat, jest równolatkiem trenera Brosza. Zdolny, perspektywiczny, na dobre i na złe związany z naszym klubem - mówi Jarosław Zięba, członek Zarządu Podbeskidzia.

Świderski jest trenerem tymczasowym, poprowadzi zespół w trzech ostatnich meczach tego roku. W połowie listopada zbiera się walne zgromadzenie członków klubu, które zdecyduje o dalszych losach Brosza i jego następcy.

Świderski w roli pierwszoligowego trenera zadebiutuje już w środę. W derbach Podbeskidzie zmierzy się z lokalnym rywalem GKS-em Katowice.





