::{manager:Smuda}: Koniec kompromisów z klubami! {team:Reprezentacja Polski}::
@2011-01-31 09:21


- Skoro trener {manager:Theo Bos} chce wygrać ligę i zdobyć {league:Puchar Polski}, to ja mu nie będę tego planu utrudniał. Na pierwszą część zgrupowania zabieram tylko {player:Jodłowca} i {player:Sobiecha}, na drugą {player:Mierzejewskiego} i {player:Sadloka} - mówi w wywiadzie dla poniedziałkowego "Przeglądu Sportowego" {manager:Smuda}.

- Żadnej wyrozumiałości już nie będzie! Ostatni raz zgodziłem się na taki kompromis - ostrzega jednak chwilę później selekcjoner. - Już nigdy nie wezmę pod uwagę, że holenderski trener już za kilka miesięcy chce zdobył mistrzostwo i {league:Puchar Polski}! Bo każdy klub o coś walczy.

{manager:Franciszkowi Smudzie} brakuje jednak konsekwencji. - Elastyczny to ja mogę być w przypadku {team:Lecha}, który zaraz będzie grał ważne mecze w {league:Lidze Europy}, a to chluba i interes dla całej polskiej piłki - mówi w tej samej rozmowie.

Były trener {team:Lecha} nie przejmuje się także tym, iż na pierwszej części zgrupowania będzie miał do dyspozycji tylko 17 piłkarzy. - W tej liczbie jest siódemka, a to dla mnie szczęśliwa cyfra. Tak jak dla Bogusława Cupiała. Gdy pracowałem w {team:Wiśle}, właściciel często mi to powtarzał i wreszcie ja też uwierzyłem w szczęśliwą magię siódemki - na poważnie deklaruje {manager:Smuda}.

Selekcjoner wyjaśnia także po raz kolejny status {player:Artura Boruca}. - {player:Boruc} u mnie nie zagra. Oświadczam, że w tej chwili mam bez niego czterech mocnych kandydatów do kadry na {league:Euro 2012} - mówi {manager:Smuda}. Oprócz {player:Fabiańskiego}, {player:Szczęsnego} i {player:Tytonia} ma to jakoby być jeszcze {player:Tomasz Kuszczak}.

"Wyp***j", czyli "sympatyczne" stosunki między trenerem i dyrektorem sportowym {team:Legii Warszawa} »






