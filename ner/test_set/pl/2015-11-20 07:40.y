::Trener {manager:Jan Urban} o transferze {player:Artjomsa Rudnevsa}: Pomarzyć zawsze można Piłka nożna - Sport.pl::
@2015-11-20 07:40

{player:Artjoms Rudnevs} po raz kolejny widziany był w Poznaniu. Tym razem pojawiły się już jednak informacje na temat tego, że {team:Lech Poznań} negocjuje z {team:Hamburger SV} kwestię jego przejścia, zapewne na wypożyczenie od 1 stycznia. Sęk w tym, że były znakomity napastnik {team:Lecha} zarabia w {team:Hamburgu} 1,5 mln euro. {team:Kolejorza} absolutnie nie stać na taki wydatek, nawet gdyby podzieliłby zarobki piłkarza z hamburczykami. Żeby {player:Artjoms Rudnevs} mógł trafić ponownie na {stadium:Bułgarską}, o czym marzą kibice, musiałby ograniczyć żądania finansowe.

Trener {manager:Jan Urban} zapytany o to, czy chciałby aby łotewski snajper trafił do {team:Lecha}, odrzekł: - Tak. Jasne, że tak. Przydałby się nam, co? Nie jest to możliwe, ale pomarzyć zawsze można.

{player:Artjoms Rudnevs} był bohaterem {team:Lecha} zwłaszcza jesienią 2010 roku, gdy strzelił w pucharach cztery gole {team:Juventusowi Turyn} - aż trzy na wyjeździe. Odszedł do {team:HSV} w czerwcu 2012 roku. Hamburski klub zapłacił za niego 3,5 mln euro. Dzisiaj {player:Rudnevs} nie ma miejsca w składzie niemieckiego zespołu.






