::BATE podbija Ligę Mistrzów Piłka nożna - Sport.pl::
@2008-10-01 12:54
url: http://www.sport.pl/pilka/1,65085,5755696,BATE_podbija_Lige_Mistrzow.html


Juventus w Lidze Mistrzów: Marynarze w histerii i stracone punkty

Dla BATE mecz z Włochami był ósmym w tej edycji LM. Białorusini zaczynali walkę o fazę grupową, gdy Alessandro del Piero i Pavel Nedved wylegiwali się na karaibskich plażach. W I rundzie BATE pokonało islandzkie Valur (2:0 i 1:0), w drugiej Anderlecht Bruksela (2:1 i 2:2), a w trzeciej Lewski Sofia (1:0 i 0:0). Białorusini są zespołem z najmniejszym współczynnikiem (1,760) UEFA w fazie grupowej LM. Poprzednim była Artmedia Petrżałka ze współczynnikiem niemal trzykrotnie większym (4,850).

Słowacy, gdy grali w fazie grupowej LM mieli budżet ok. 1,5 mln euro. Tyle samo ma dziś BATE. Większy ma nawet Odra Wodzisław. Albo raczej miała, bo po sukcesach Białorusini chcą go powiększyć do 4 mln euro.

Trenerem drużyny, która we wtorek po 23 minutach prowadziła z Juventusem jest 31-letni Wiktor Gonczarneko. - Jestem demokratą, który stosuje dyktaroskie metody - mówi o swoim stylu pracy szkoleniowiec.

Zespół opiera się na wychowankach, średnia wieku pierwszej jedenastki wynosi mniej niż 24 lata. Do tego sezonu klub utrzymywał się głównie z transferów młodych piłkarzy. W Borysowie kariery zaczynali tacy piłkarze jak Aleksander Hleb (latem przeniósł się z Arsenalu do Barcelony) czy Witalij Kutuzow (Milan, Sporting Lizbona, Parma).

Zobacz jak wielki Juventus został zatrzymany przez BATE Borysów - Z Czuba.tv »

LM: Saganowski i Boruc przegrywają






