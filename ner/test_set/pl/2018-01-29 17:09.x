::Transfery. ESPN: Zlatan Ibrahimović trafi do MLS Piłka nożna - Sport.pl::
@2018-01-29 17:09
url: http://www.sport.pl/pilka/7,64946,22958036,transfery-espn-zlatan-ibrahimovic-bliski-odejscia-do-mls.html

Wszystko wskazuje, że latem Szwed odejdzie z Manchesteru United i na zasadzie wolnego transferu przeniesie się do Los Angeles Galaxy. Ibrahimović występuje w Anglii od zeszłego sezonu. Dla "Czerwonych Diabłów" rozegrał łącznie 53 spotkania, strzelił 29 goli i zanotował 10 asyst. Problem w tym, że dobre statystyki zawdzięcza sezonowi 2016/17. W obecnym sezonie idzie mu dużo gorzej.
REKLAMA




Do gry wrócił w listopadzie, po tym jak w kwietniu 2017 zerwał więzadła w kolanie. Od tej pory wystąpił w 7 meczach, ale strzelił tylko jednego gola. W pierwszym składzie United gra Romelu Lukaku, który latem trafił do drużyny. W dodatku zimą Manchester ściągnął także Alexisa Sancheza z Arsenalu.- Zlatan mówił mi ostatnio, że chce wrócić do wysokiej formy i pomóc drużynie w drugiej części sezonu. Jednak jeśli zamierza odejść, to nie będę mu w tym przeszkadzał - powiedział Jose Mourinho.Według informacji ESPN 36-letni napastnik uzgodnił już warunki kontraktu z LA Galaxy. W przeszłości zawodnikiem tego klubu byli m.in: Robbie Kean i Steven Gerrard.





