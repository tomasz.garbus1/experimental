::Transfery. Real Madryt da 70 mln euro za Davida de Geę? Piłka nożna - Sport.pl::
@2017-04-21 09:20
url: http://www.sport.pl/pilka/7,96012,21667585,transfery-real-madryt-da-70-mln-euro-za-davida-de-gee.html

Angielski dziennik twierdzi, że madrytczycy złożyli za de Geę ofertę opiewającą na 70 mln euro. Gdyby transakcja doszła do skutku, niemal dwukrotnie przebiłaby dotychczasowy rekord transferowy na pozycji bramkarza - w 2001 r. Juventus zapłacił za Gianluigiego Buffona 38 mln euro.
REKLAMA




"The Sun" twierdzi jednak, że Real ma opcję rezerwową. Jeżeli "Czerwone Diabły" odrzucą propozycję, triumfatorzy Ligi Mistrzów mogą spróować sięgnąć po golkipera Chelsea. Problem w tym, że Thibaut Courtois zapowiedział, że nie zamierza odchodzić z "The Blues".





