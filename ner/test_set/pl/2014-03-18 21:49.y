::{league:Liga Mistrzów}. {player:Jese} poważnie kontuzjowany Piłka nożna - Sport.pl::
@2014-03-18 21:49




{player:Jese} ucierpiał po agresywnym wejściu {player:Kolasinicia} w drugiej minucie meczu. 21-letni gracz {team:Realu} długo nie podnosił się z murawy, pomocy udzielali mu lekarze. Z boiska zniesiono go na noszach, dostał gromkie brawa od publiczności. Do szatni zszedł prezydent klubu {manager:Florentino Perez}, by zapytać o zdrowie piłkarza.

Na razie wieści nie są optymistyczne, ale nie wiadomo też dokładnie, na ile poważny jest uraz prawego kolana. Jak twierdzi "AS", {player:Jese} został przewieziony już do jednej z klinik, gdzie ma przejść badanie rezonansem magnetycznym, które określi, co się stało. Jeśli okaże się, że zerwał więzadła w kolanie, czeka go kilka miesięcy leczenia.

{player:Jese} został zastąpiony przez {player:Garetha Bale}. W 21. minucie Walijczyk asystował przy golu {player:Ronaldo}. W 31. gola na 1:1 strzelił {player:Hoogland}. Po przerwie {team:Real} zdobył dwa gole w minutę. W 74. minucie bramkę strzelił {player:Ronaldo}, minutę później trafił {player:Morata}.

Udowodnij, że Z czuba się nie zna na piłce. Zostań naszym ekspertem »






