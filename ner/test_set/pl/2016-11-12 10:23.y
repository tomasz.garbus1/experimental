::Eliminacje {league:MŚ 2018}. {player:Robert Lewandowski}: biało-czerwona maszyna {manager:Nawałki} Reprezentacja {country:Polski}::
@2016-11-12 10:23

Obserwuj @LukaszJachimiak

Gol w Astanie, hat-trick z {country:Danią}, ratująca nas bramka wbita {country:Armenii} w 95. minucie, teraz dwie petardy w Bukareszcie - po czterech kolejkach eliminacji mundialu "{player:Lewy}" z siedmioma trafieniami jest ich najlepszym strzelcem. Licząc z eliminacjami poprzedniego wielkiego turnieju, {league:Euro 2016}, kapitan naszej kadry ma serię dziewięciu spotkań z chociaż jednym zdobytym golem. Strzelił w nich aż 16 bramek.

Król strzelców kwalifikacji do {league:ME 2016} w sumie zanotował w nich 13 trafień. Razem z siedmioma golami z trwających eliminacji {league:MŚ} {player:Lewandowski} ma 20 bramek w meczach o stawkę. Do tego dodajmy gola strzelonego {country:Portugalii} w ćwierćfinale {league:Euro 2016} i mamy błyskawiczną odpowiedź na pytanie, czy aby lider {country:Polski} nie trafia głównie wtedy, kiedy gra ona towarzysko. Asysty "{player:Lewy}" też zalicza praktycznie tylko w meczach o coś. Ze wszystkich sześciu pięć zanotował w walce o wyjazd {country:Polski} na francuskie {league:Euro}, jedną w towarzyskim meczu z {country:Islandią}.

{player:Lewandowski} "robi" ponad bramkę na mecz. {player:Ronaldo} i {player:Messi} nie


Liczby {player:Lewandowskiego} są rewelacyjne i świetnie obrazują, jaką maszyną stał się, grając w reprezentacyjnych barwach. Jego średnia gola na mecz w trzech latach gry pod wodzą {manager:Nawałki} wynosi 0,89. Po zsumowaniu goli (24) i asyst (sześć) łatwo policzyć, że kapitan ma średnio w spotkaniu udział przy 1,11 bramki dla drużyny.

{player:Cristiano Ronaldo} w swoich ostatnich 27 meczach dla {country:Portugalii} zdobył 22 gole i zaliczył pięć asyst. {player:Lionel Messi} w swych ostatnich 27 występach w barwach {country:Argentyny} zdobył 16 bramek i dołożył 10 asyst. Liczby imponujące, ale dwaj najwięksi współcześni piłkarze świata ustępują {player:Lewandowskiemu}, bo pierwszy z wymienionych miał udział przy 27, a drugi przy 26 bramkach swojej drużyny. A gdyby ktoś chciał umniejszać dorobek Polaka, twierdząc, że błyszczał m.in. przeciw {country:Gibraltarowi} (w dwóch meczach sześć goli i trzy asysty), to musi wiedzieć, że {player:Ronaldo} swoje statystyki miał okazję poprawiać grając m.in. z {country:Andorą} i {country:Wyspami Owczymi}, a {player:Messi} z {country:Hongkongiem} czy {country:Panamą}.

Jeszcze większa różnica między Polakiem a Portugalczykiem i Argentyńczykiem jest w meczach o punkty wielkich imprez oraz eliminacji do nich "{team:Lewy}" ma w ostatnich trzech latach bilans 19 takich występów, 21 goli i pięciu asyst. {player:Ronaldo} w tym czasie rozegrał 19 spotkań o stawkę, zdobywając w nich 18 bramek i notując cztery asysty. Liczby {player:Messiego} to 23 gry, 12 goli i osiem asyst.

{player:Neymar} też przegrywa


Z {player:Lewandowskim} równać może się tylko {player:Neymar}. On w swoich ostatnich 27 meczach w reprezentacji {country:Brazylii} zdobył 20 bramek i zanotował 11 asyst, a w 14 meczach o stawkę, jakie rozegrał w trzech ostatnich latach, trafił do bramki dziewięć razy i ośmiokrotnie otworzył do niej drogę któremuś z kolegów.

Krótkie podsumowanie dla każdego, komu zakręciło się w głowie: {player:Lewandowski} miał udział przy 1,37 bramki {country:Polski} w meczu o stawkę, uśredniając jego występy w takich spotkaniach w trzech ostatnich latach. Średnia {player:Neymara} to 1,21, {player:Ronaldo} - 1,16, a {player:Messiego} - 0,87.

El. {league:MŚ}. {country:Rumunia} - {country:Polska} 0:3. {player:Lewandowski} komentuje incydent z racą: Przez parę minut nie za bardzo wiedziałem, co się dzieje

Race w {country:Rumunii}? UEFA zamyka stadion {team:Legii} [MEMY PO POLSCE]






