::{team:Portowcy} wysoko, coraz wyżej! Piłka nożna - Sport.pl::
@2009-03-22 21:00


Co musi cieszyć - forma {team:portowców} idzie w górę. Ozdobą sobotniego meczu były bramki {player:Tomasza Parzego} i {player:Piotra Petasza}.

Potężny strzał w okienko z 30 metrów ({player:Petasz}) to było trafienie godne {league:Ligi Mistrzów}, akcja {player:Ferdinanda Chifona} zakończona pięknym golem {player:Parzego} kandydowałaby do nagród w polskiej {league:ekstraklasie}. Dwie bardzo efektowne bramki potwierdziły przewagę {team:portowców} w spotkaniu z {team:Czarnymi}. {team:Szczecinianie} grali skutecznie i z polotem, znaczniej lepiej niż na inaugurację z {team:MKS Kluczbork}. To również "zasługa" przeciwników, którzy nie przestraszyli się faworytów i na {stadium:Twardowskiego} zaprezentowali otwarty, dość ofensywny futbol.

- Gdybyśmy wykorzystali sytuację z pierwszych minut, mogło być ciekawiej - komentował trener {manager:Janusz Kubot}, zapewne mając w pamięci akcję {player:Pascala Ekwueme}.

Błyskotliwy prawoskrzydłowy przejął źle wybitą piłkę, kiwnął obrońców w polu karnym... i z 7-8 m trafił w boczną siatkę. {team:Pogoń} odpowiedziała strzałami {player:Damiana Sieniawskiego}, {player:Petasza} i {player:Parzego} (zablokowany, obroniony, niecelny), lecz ekipa z Żagania nadal nie zamierzała pilnować wyniku 0:0. Grający na szpicy {player:Marcin Jankowski} (jesienią rezerwowy {team:Pogoni}), wspierany przez pomocników, kilkakrotnie zmusił do błędu szczecińską defensywę. Sposobem na ujarzmienie przyjezdnych okazał się stały fragment - dobrze bity korner, główka {player:Piotra Komana} i 1:0. Od tego momentu było łatwiej - "{player:Parzol}" znakomicie rozdzielał piłki, skrzydłowi uciekali kryjącym, {player:Paweł Skrzypek} i {player:Marcin Woźniak} podłączali się do ataków. Dominacja {team:granatowo-bordowych} stawała się coraz wyraźniejsza. Gol do szatni właściwie rozstrzygnął losy meczu. Po przerwie {team:pogoniarze} kontrolowali sytuację, a po wspaniałym trafieniu {player:Petasza} grali już rozluźnieni. Trener {manager:Piotr Mandrysz} z ławki wybrał dwóch młodzieżowców, natomiast ostatnie 10 min zarezerwował na debiut {player:Michała Szczyrby} (ofensywny pomocnik sprowadzony z {team:Piasta Gliwice} długo pauzował w okresie przygotowawczym). Idealny moment - wysokie prowadzenie, fajna atmosfera na trybunach. {team:Portowcy} w efektownym stylu wywalczyli pozycję wicelidera.

- Przeciwnik postawił wysoko poprzeczkę, spotkanie kosztowało nas dużo zdrowia, ale ułożyło się dobrze. Wykorzystane stałe fragmenty, skuteczna akcja tuż przed przerwą, przekonujące zwycięstwo! To bardzo cieszy - przyznał {manager:Mandrysz}.

Jedyne, co może martwić, to dłuższa absencja najskuteczniejszego napastnika {team:Pogoni} - {player:Marka Kowala}. Zawodnik jest po zabiegu artroskopii (wiązadła krzyżowe) i w tym sezonie raczej nie zagra. W sobotę atak tworzyli {player:Damian Sieniawski} i {player:Mikołaj Lebedyński}. Zdaniem szkoleniowca, "wypadli obiecująco".

{team:Pogoń Szczecin} - {team:Czarni Żagań} 3:0 (2:0)

Bramki: {player:Koman} (22., asysta {player:Petasz}), {player:Parzy} (45., asysta {player:Chifon}), {player:Petasz} (61.).

{team:Pogoń}: {player:Binkowski} - {player:Skrzypek}, {player:Nowak}, {player:Dymek}, {player:Woźniak}, {player:Chifon} (72. {player:Rydzak}), {player:Koman}, {player:Parzy} (80. {player:Szczyrba}), {player:Petasz}, {player:Sieniawski} (68. {player:Wólkiewicz}), {player:Lebedyński}.

Widzów: 4 500



{team:Kotwica} nadal liderem...


... ale przewaga {team:kołobrzeżan} nad {team:Pogonią} stopniała do punktu. Zespół {manager:Tomasza Arteniuka} bezbramkowo zremisował wyjazdowe spotkanie z {team:Miedzią Legnica}, walczącą o opuszczenie strefy spadkowej. W pierwszej połowie "{team:Kotwa}" miała nieznaczną przewagę, jednak nie udało się jej udokumentować zdobyciem bramki ({player:Krzysztof Rusinek}, {player:Filip Marciniak}). W drugiej części meczu twardo grający gospodarze skutecznie bronili remisu.

{team:Miedź Legnica} - {team:Kotwica Kołobrzeg} 0:0. {team:Kotwica}: {player:Sobański} - {player:Małkowski Ż} (68. {player:Żdanow}), {player:Stankiewicz Ż}, {player:Grocholski}, {player:Kempa}, {player:Pietroń}, {player:Sawczuk}, {player:Rawa}, {player:Stróż}, {player:Marciniak} (78. {player:Misztal}), {player:Rusinek}

{team:Chemik} tylko zremisował


Policzanie na inaugurację wiosny - z {team:Lechią Zielona Góra} - mierzyli w zwycięstwo, więc rozpoczęli z animuszem, ale po 20 min gra się wyrównała. Składnych akcji było jak na lekarstwo. Goście wykorzystali jedną i przed przerwą objęli prowadzenie. Drużyna {manager:Marka Czerniawskiego} długo nie mogła sforsować defensywy rywali. Atak pozycyjny kończył się daleko od bramki, kontry nie wychodziły. Gdy wreszcie {player:Marek Sajewicz} doprowadził do remisu, gospodarze odzyskali wiarę w zwycięstwo, narzucili żywsze tempo, wywalczyli kilka rzutów wolnych i rożnych, ale drugiego gola nie zdobyli.

{team:Chemik Police} - {team:Lechia Zielona Góra} 1:1 (0:1). Bramki - {team:Chemik}: {player:Sajewicz} (72.); {team:Lechia}: {player:Żebrowski} (36.). {team:Chemik}: {player:Wiśniewski} - {player:Krzyżanowski}, {player:Bieniek}, {player:Michał Szałek}, {player:Brzeziński}, {player:Jóźwiak} (60. {player:Pachulski}), {player:Jarymowicz}, {player:Mateusz Szałek}, {player:Dutkiewicz} (76. {player:Baranowski}), {player:Sajewicz}, {player:Ciołek} (55. {player:Erlich}).

Pozostałe wyniki 21. kolejki: {team:GKS} - {team:Zawisza} 0:0, {team:Raków} - {team:Victoria} 1:0, {team:Polonia} - {team:Unia} 1:2, {team:Elana} - {team:Zagłębie} 0:1, {team:Jarota} - {team:MKS} 4:1.



1. {team:Kotwica Kołobrzeg}214236-18


2. {team:Pogoń Szczecin}214132-22


3. {team:Gawin Królewska Wola}194039-18


4. {team:MKS Kluczbork}213736-24


5. {team:Raków Częstochowa}213527-18


6. {team:Unia Janikowo}213422-16


7. {team:Zagłębie Sosnowiec}213127-19


8. {team:Elana Toruń}213126-25


9. {team:Nielba Wągrowiec}202937-30


10. {team:Zawisza Bydgoszcz}212727-28


11. {team:Jarota Jarocin}212728-23


12. {team:Czarni Żagań}212420-35


13. {team:GKS Tychy}212417-21


14. {team:Lechia Zielona Góra}212328-41


15. {team:Miedź Legnica}212322-33


16. {team:Chemik Police}201922-37


17. {team:Victoria Koronowo}21149-30


18. {team:Polonia Słubice}211419-36









