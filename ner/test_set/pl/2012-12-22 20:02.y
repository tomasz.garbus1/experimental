::{league:Primera Division}. {team:Barcelona} pokonała {team:Real Valladolid}. Emocjonująca końcówka Piłka nożna - Sport.pl::
@2012-12-22 20:02


Z Czuba.tv: Zobacz gole z sobotnich meczów {league:Primera Division} »

{team:Barcelona} w pełni kontrolowała przebieg spotkania, a pierwszą bramkę udało się jej strzelić jeszcze przed przerwą. {player:Messi} wpadł w pole karne, "dzióbnął" piłkę do oskrzydlającego akcję {player:Alby}, a ten zagrał wzdłuż bramki. {player:Xavi} musiał tylko dołożyć nogę.

W międzyczasie piłkarze gości dwukrotnie trafiali w słupek. Najpierw {player:Messi} z rzutu wolnego, a następnie {player:Pedro} po strzale głową. {player:Messi} błyskawicznie, bo kilkadziesiąt sekund później, zrehabilitował się za kolejną niewykorzystaną sytuację. {player:Messi} założył "siatkę" {player:Sereno} i strzelił lewą nogą w kierunku dalszego słupka. Piłka odbiła się właśnie od niego i zatrzepotała w siatce bramki {player:Hernandeza}.

Gospodarze nie gościli właściwie pod bramką {player:Valdesa}, ale w 89. minucie zdołali trafić do siatki. Błąd obrony {team:Barcelony} wykorzystał {player:Javi Guerra}, który uderzył głową w światło bramki, a następnie skutecznie dobił piłkę.

{team:Barcelona} podwyższyła wynik w ostatniej akcji meczu. {player:Tello}, który pojawił się na placu gry kilkanaście sekund wcześniej, znalazł się oko w oko z {player:Hernandezem} i umieścił piłkę w bramce.

{team:Real Valladolid} nie wygrał z {team:Barceloną} od 2002 roku. Bilans bramek z sześciu ostatnich spotkań ligowych jest niekorzystny dla drużyny z {team:Valladolid}, piłkarze {team:Realu} strzelili tylko 2 razy, a dali sobie wbić aż 21 bramek.

Relacje na żywo możesz śledzić także na smartfonie!


Wszystkie relacje Sport.pl możesz czytać, także jeśli nie jesteś akurat przy komputerze. Nieważne, czy ścinasz choinkę, robisz zakupy, sprzątasz czy gotujesz - możesz śledzić relacje za pomocą smartfona lub innego urządzenia mobilnego. Zachęcamy do ściągnięcia aplikacji Sport.pl Live na Androida lub korzystania z mobilnej wersji naszego serwisu - m.sport.pl.






