::Primera Division. Ancelotti krytykuje dalekie sparingi reprezentacji Hiszpanii Piłka nożna - Sport.pl::
@2013-11-22 16:43
url: http://www.sport.pl/pilka/1,65084,14999879,Primera_Division__Ancelotti_krytykuje_dalekie_sparingi.html


Iker Casillas, Alvaro Arbeloa i Sergio Ramos z Realu w ostatnim tygodniu przemierzyli wiele tysięcy kilometrów, by rozegrać mecze towarzyskie. W zeszłym tygodniu grali sparing z Gwineą Równikową, we wtorek udali się do RPA, gdzie niespodziewanie przegrali z gospodarzami 0:1. Trener "Królewskich" obawia się o to, jak te podróże wpłyną na jego zawodników. Wprost mówi, że rozgrywanie takich meczów to błąd.

- Mamy ogromne szczęście, że do marca nie ma meczów towarzyskich. Ciężko o tym mówić, ale decyzja hiszpańskiej federacji jest zła - stwierdził na konferencji prasowej przed sobotnim meczem z Almerią.

- Piłkarze spali trzy noce w samolocie, a to dla nich nie jest dobre. Bramkarz Barcelony (Valedes) doznał kontuzji, nas to na szczęście ominęło, ale lepsza organizacja mogłaby pomóc w uniknięciu podobnych urazów - dodał Ancelotti.

Po 13 kolejkach Primera Division Real z 31 punktami zajmuje trzecie miejsce. Przed "Królewskimi" są Atletico (34 pkt) oraz Barcelona (37 pkt).

Relacje z najważniejszych zawodów w aplikacji Sport.pl Live na iOS, na Androida i Windows Phone






