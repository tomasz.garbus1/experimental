::{player:Rafał Siemaszko} wraca do Arki Piłka nożna - Sport.pl::
@2015-06-24 15:49


{team:Arka} awansuje do ekstraklasy? Podyskutuj na Facebooku >>

{player:Siemaszko} jest wychowankiem {team:Orkanu Rumia}. Długo występował w niższych ligach, aż wreszcie w sezonie 2010/11 został wypożyczony do {team:Arki}. Gdynianie występowali wówczas w ekstraklasie. Siemaszko zagrał w dwunastu meczach, ale bramki nie zdobył.

Po zakończeniu sezonu ówczesny szkoleniowiec {team:Arki} {manager:Petr Nemec} uznał, że {player:Siemaszko} nie będzie mu już potrzebny, i ten wrócił do {team:Orkana}. Na trzecioligowym szczeblu zawodnik przebywał jednak tylko przez jeden sezon i w czerwcu trafił do {team:Gryfa Wejherowo}, gdzie miał okazję występować przez dwa lata.

W poprzednim sezonie {player:Siemaszko} reprezentował barwy pierwszoligowej {team:Chojniczanki Chojnice}. Zagrał 31 meczów, w których zdobył sześć bramek.

28-latek jest trzecim wzmocnieniem żółto-niebieskich bieżącego lata. Wcześniej do klubu znad morza trafili bowiem {player:Tadeusz Socha} i {player:Alan Fialho}.

Obserwuj trojmiasto.sport.pl na Twitterze - @3miasto_sportpl >>

PO ZWYCIĘSTWACH JESTEM "NITEK", PO PORAŻKACH NICIŃSKI - SZCZERA ROZMOWA Z TRENEREM ARKI GDYNIA






