::Ekstraklasa. Peszko: Jeżeli odejdę, to nie do Włoch Piłka nożna - Sport.pl::
@2010-12-15 12:07
url: http://www.sport.pl/pilka/1,70993,8820781,Ekstraklasa__Peszko__Jezeli_odejde__to_nie_do_Wloch.html


"Fakt" donosi, że być może jeszcze przed świętami Bożego Narodzenia Sławomir Peszko podpisze kontrakt z nowym klubem. Piłkarz Lecha Poznań zapowiada, że jeżeli zdecyduje się odejść z zespołu mistrza Polski, to z pewnością nie skusi go żaden włoski klub. - Jeśli w ogóle zmienię barwy klubowe, to na pewno nie będzie to włoski klub - zapewnił skrzydłowy Kolejorza. Kilkanaście dni temu La Gazetta Dello Sport podała, że transferem Peszki zainteresowane są Udinese i Brescia.

- Mogę tylko powiedzieć, że coś się szykuje w temacie transferu Sławka. Jednak ze zrozumiałych względów nie mogę podać jeszcze nazw klubów. W przeciągu najbliższych dni powinna się wyjaśnić przyszłość piłkarza - powiedział menedżer piłkarza Eugeniusz Kamiński.

Do wyścigu o Peszkę - oprócz wyżej wymienionych klubów - stanęły również: Vfl Wolfsburg i Panathinaikos Ateny. Reprezentanta kraju widziałby w swojej ekipie Józef Wojciechowski. Cena za dynamicznego piłkarza nie jest wysoka. Prasa zapewnia, że Peszko ma wpisaną w kontrakcie sumę odstępnego, oscylującą w granicach 500 tysięcy uro.






