::{team:BATE} podbija {league:Ligę Mistrzów} Piłka nożna - Sport.pl::
@2008-10-01 12:54

{team:Juventus} w {league:Lidze Mistrzów}: {team:Marynarze} w histerii i stracone punkty

Dla {team:BATE} mecz z {team:Włochami} był ósmym w tej edycji {league:LM}. {team:Białorusini} zaczynali walkę o fazę grupową, gdy {player:Alessandro del Piero} i {player:Pavel Nedved} wylegiwali się na karaibskich plażach. W I rundzie {team:BATE} pokonało islandzkie {team:Valur} (2:0 i 1:0), w drugiej {team:Anderlecht Bruksela} (2:1 i 2:2), a w trzeciej {team:Lewski Sofia} (1:0 i 0:0). {team:Białorusini} są zespołem z najmniejszym współczynnikiem (1,760) UEFA w fazie grupowej {league:LM}. Poprzednim była {team:Artmedia Petrżałka} ze współczynnikiem niemal trzykrotnie większym (4,850).

{team:Słowacy}, gdy grali w fazie grupowej {league:LM} mieli budżet ok. 1,5 mln euro. Tyle samo ma dziś {team:BATE}. Większy ma nawet {team:Odra Wodzisław}. Albo raczej miała, bo po sukcesach {team:Białorusini} chcą go powiększyć do 4 mln euro.

Trenerem drużyny, która we wtorek po 23 minutach prowadziła z {team:Juventusem} jest 31-letni {manager:Wiktor Gonczarneko}. - Jestem demokratą, który stosuje dyktaroskie metody - mówi o swoim stylu pracy szkoleniowiec.

Zespół opiera się na wychowankach, średnia wieku pierwszej jedenastki wynosi mniej niż 24 lata. Do tego sezonu klub utrzymywał się głównie z transferów młodych piłkarzy. W {team:Borysowie} kariery zaczynali tacy piłkarze jak {player:Aleksander Hleb} (latem przeniósł się z {team:Arsenalu} do {team:Barcelony}) czy {player:Witalij Kutuzow} ({team:Milan}, {team:Sporting Lizbona}, {team:Parma}).

Zobacz jak wielki {team:Juventus} został zatrzymany przez {team:BATE Borysów} - Z Czuba.tv »

{league:LM}: {player:Saganowski} i {player:Boruc} przegrywają






