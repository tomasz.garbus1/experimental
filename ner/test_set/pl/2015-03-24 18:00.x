::Wywalczył dwa karne i trafił do jedenastki kolejki Piłka nożna - Sport.pl::
@2015-03-24 18:00
url: http://www.sport.pl/pilka/1,70992,17650140,Wywalczyl_dwa_karne_i_trafil_do_jedenastki_kolejki.html


Brazylijskiemu skrzydłowemu wystarczyło 45 minut w meczu z Piastem Gliwice, aby zostać wyróżnionym za swój występ w 25. kolejce. 27-latek zaliczył bardzo dobrą drugą połowę, był aktywny, a przede wszystkim wywalczył dwa rzuty karne (pierwszy zmarnowany przez Olivera Kapo, drugi wykorzystany przez Jacka Kiełba).

Dla Luisa Carlosa to druga nominacja wiosną. Poprzednio pomocnik Korony trafił do jedenastki kolejki ze występ z Podbeskidziem Bielsko-Biała. Wcześniej trzykrotnie w jedenastce kolejki znalazł się Bośniak Vlastimir Jovanović, dwukrotnie Kamil Sylwestrzak i Jacek Kiełb oraz po razie Piotr Malarczyk i Radek Dejmek.






