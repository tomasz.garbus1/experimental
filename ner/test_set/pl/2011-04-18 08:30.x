::Premier League. Dalglish do Wengera: Był karny, odp... się! Piłka nożna - Sport.pl::
@2011-04-18 08:30
url: http://www.sport.pl/pilka/1,65080,9453479,Premier_League__Dalglish_do_Wengera__Byl_karny__odp___.html


Arsenal - Liverpool. Dalglish wyzywa Wengera [WIDEO]

Bramki z meczu Arsenal - Liverpool [WIDEO]

Mecz Arsenalu z Liverpoolem został przedłużony, gdyż siedem minut trwało udzielanie pomocy Jamiemu Carragherowi, który w 56. minucie meczu zderzył się z kolegą z zespołu i stracił przytomność. Sędzia doliczył ten czas do regulaminowych 90 minut.

W rezultacie w 98. minucie Spearing przewrócił w polu karnym Fabregasa, a jedenastkę na gola precyzyjnym strzałem zamienił Van Persie. Sędzia najwyraźniej nie chciał kończyć meczu w takim momencie, i pozwolił Liverpoolowi na rozegranie jeszcze jednej akcji. Po rzucie wolnym Suareza piłka wylądowała pod nogami Lucasa, na którego wpadł Eboue.

Ostatecznie sędzia podyktował jedenastkę dla Liverpoolu, którą mocnym uderzeniem wykorzystał Dirk Kuyt. Wojciech Szczęsny był bez szans. Zaraz po wyrównującej bramce zakończył mecz. Na zegarze była już wtedy 102. minuta.

Dalglishowi puszczają nerwy


Ze stratą punktów nie mógł się pogodzić Aresene Wenger. Tuż po końcowym gwizdku rozłożył szeroko ręce i ruszył w kierunku linii bocznej manifestując swoją dezaprobatę.

Reakcja Francuza doprowadziła do szału trenera gości Kenny'ego Dalglisha. Szkoleniowiec Liverpoolu stanął na drodze Wengera i zaczął na niego krzyczeć. - To był rzut karny, odpieprz się! - słychać wyraźnie na zapisie wideo. Wenger macha ręką i zawraca. - To jest rzut karny! Odpierdol się! - rzuca mu jeszcze na odchodne Dalglish.

Niesamowite emocje w meczu Arsenal - Liverpool »






