::Finisz I ligi. Które miejsce zajmie Arka? Piłka nożna - Sport.pl::
@2015-05-22 17:07
url: http://www.sport.pl/pilka/1,65044,17963681,Finisz_I_ligi__Ktore_miejsce_zajmie_Arka_.html


Dwa miesiące temu, kiedy Arka odprawiała z kwitkiem GKS Tychy (4:0) oraz Stomil Olsztyn (4:1) wydawało się, że 4. miejsce jest w zasięgu żółto-niebieskich. Po 21. kolejce gdynianie byli na 6. miejscu z dorobkiem 32 punktów. Do czwartego Stomilu tracili jeden punkt. Jednak w kolejnych 10 meczach podopieczni Grzegorza Nicińskiego wygrali jedno spotkanie, sześć zremisowali i trzy przegrali. Ten bilans sprawił, że na trzy kolejki przed końcem sezonu Arce bliżej do środka tabeli niż do miejsca tuż za pierwszą trójką walczącą o awans do ekstraklasy. Gdynianie są na 9. miejscu ze stratą sześciu oczek do olsztynian (4. miejsce) i pięciu do Olimpii Grudziądz (5. miejsce) oraz Chojniczanki Chojnice (6. miejsce). Bardziej realny scenariusz zakłada walkę Arki o 7. lokatę (Dolcan ma 43 punkty) lub nawet o pozycje 8-10. Wprawdzie celem żółto-niebieskich jest budowa zespołu na kolejny sezon, ale miejsce w dolnych rejonach tabeli świadczyć będzie o średniej jakości drużyny Nicińskiego.

"Nitek" przejmował Arkę w trudnej sytuacji, wydobył ją z dołu tabeli i skończył jesień na 7. miejscu. Solidnie przepracował zimę, pouzupełniał luki, ale mimo to Arka wiosną ma duże wahania formy. Potrafi zagrać słabo - jak z Wigrami Suwałki (0:1) czy Bytovią Bytów (0:1), ale też jak równy z równym - a nawet momentami przeważa - rywalizuje z Wisłą Płock (0:0) czy w ostatnim meczu z Zagłębiem Lubin (0:1).

- Najbardziej jestem rozczarowany wynikiem i sędzią. Jestem zdenerwowany, bo karnego nie było. Nie wiem, czy mogę nazwać to pechem, po strzale da Silvy piłka wpadła do bramki, ale po drodze musnął ją jeszcze Nalepa, gol nie został uznany, a później Zagłębie dostaje karnego. Nie zasłużyliśmy na taki wynik. Mieliśmy swoje sytuacje. Po prostu zabrakło kropki "nad i". Zagłębie ma doświadczonych piłkarzy, można było odczuć, że gramy z zawodnikami z ekstraklasy. Widać, że grają o awans - mówił po meczu pomocnik Arki Paweł Wojowski.

Arka ma jeszcze szansę poprawić swoją pozycję. Choć czy tutaj tak naprawdę o miejsce chodzi, czy bardziej o kontynuowanie myśli szkoleniowej i rozwój zespołu? Na to pytanie wkrótce poznamy odpowiedź. Znane są za to inne fakty - w najbliższym spotkaniu Arkę czeka pojedynek z wciąż niepewnym swego Chrobrym Głogów. Potem gdynianie pożegnają się z własną publicznością meczem z Olimpią Grudziądz, a na koniec pojadą do Katowic.

Na Chrobrego wraca już do składu pauzujący za żółte kartki z Zagłębiem Antoni Łukasiewicz. Jest także Grzegorz Tomasiewicz, który przebywał na zgrupowaniu reprezentacji Polsku U-19. Zabraknie za to kontuzjowanych Pawła Abbotta, Michała Rzuchowskiego i Michała Renusza.






