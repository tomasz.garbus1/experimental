::Premier League. Koniec skandalu z Terrym, nie będzie odwołania. Piłka nożna - Sport.pl::
@2012-10-18 15:05
url: http://www.sport.pl/pilka/1,65080,12696399,Premier_League__Koniec_skandalu_z_Terrym__nie_bedzie.html


Czy uważasz, że kara dla Terry'ego jest zbyt surowa? Podyskutuj o tym na Facebooku! »

Kapitan Chelsea, który będzie musiał opuścić cztery najbliższe mecze "The Blues" (w tym dwa mecze z wiceliderem Premier League, Manchesterem United). Terry mimo długich bojów z angielską federacją ostatecznie zdecydował się przeprosić za swoje postępowanie. - Chciałbym przeprosić wszystkich za to, jak zachowałem się w trakcie tamtego spotkania - przyznał. Decyzja Angielskiej Federacji dotyczy incydentu z meczu przeciwko QPR, który miał miejsce 23 października zeszłego roku.

Specjalne oświadczenie w tej sprawie wydał także klub. - John Terry podjął właściwą decyzję godząc się z werdyktem FA. Chelsea jako klub także wyraża żal z powodu języka użytego przez Terry'ego i wyraża nadzieje, że takie zachowania, które są niegodne zawodnika naszego klubu, więcej nie będą się powtarzać - czytamy w oświadczeniu.

31-latek oprócz zawieszenia będzie musiał jeszcze zapłacić 220 tysięcy funtów kary. - Jestem rozczarowany tym, że otrzymałem taką karę, ale akceptuje ją i zdaje sobie sprawę, że było to nieakceptowalne. W dalszym ciągu będę starał się robić wszystko, by dbać o dobre imię klubu - twierdzi Terry.

W związku z całą aferą obrońca podjął decyzję o zakończeniu kariery reprezentacyjnej.

Czy kara dla Terry'ego jest zbyt surowa? Zapraszamy do dyskusji w komentarzach!







