::Liga Mistrzów. Edwin van der Sar: Manuel Neuer to klasa światowa Piłka nożna - Sport.pl::
@2011-04-26 23:45
url: http://www.sport.pl/pilka/1,65041,9498955,Liga_Mistrzow__Edwin_van_der_Sar__Manuel_Neuer_to.html


Zobacz gole na Zczuba.tv »

- Wiedzieliśmy, że to nie będzie łatwe. Ale dwubramkowe zwycięstwo to dla nas doskonały wynik. Manuel Neuer zagrał fantastyczny mecz. To naprawdę bramkarz światowej klasy - powiedział po meczu van der Sar.

Nieco zawiedziony był po spotkaniu napastnik Manchesteru Wayne Rooney. - Mogliśmy strzelić więcej goli. Ale zagraliśmy bardzo dobrze. Musimy bardzo profesjonalnie podejść do kolejnego meczu. Nie wolno lekceważyć Schalke. Pojechali do Mediolanu i ograli Inter 5:2 - przypomina Rooney.

- Czułem, ze jeśli będziemy stwarzali okazje, to gole w końcu padną - powiedział Ryan Giggs, 37-letni skrzydłowy MU, który jest najstarszym strzelcem gola w historii Ligi Mistrzów. - Do przerwy mogliśmy prowadzić czterema, albo pięcioma golami. Przed meczem wynik 2:0 wzięlibyśmy w ciemno, ale teraz jestem takim wynikiem trochę rozczarowany - powiedział Walijczyk.

Przeczytaj o meczu Schalke - Manchester »






