::Primera Division. Barcelona pokonała Real Valladolid. Emocjonująca końcówka Piłka nożna - Sport.pl::
@2012-12-22 20:02
url: http://www.sport.pl/pilka/1,123132,13098853,Primera_Division__Barcelona_pokonala_Real_Valladolid_.html


Z Czuba.tv: Zobacz gole z sobotnich meczów Primera Division »

Barcelona w pełni kontrolowała przebieg spotkania, a pierwszą bramkę udało się jej strzelić jeszcze przed przerwą. Messi wpadł w pole karne, "dzióbnął" piłkę do oskrzydlającego akcję Alby, a ten zagrał wzdłuż bramki. Xavi musiał tylko dołożyć nogę.

W międzyczasie piłkarze gości dwukrotnie trafiali w słupek. Najpierw Messi z rzutu wolnego, a następnie Pedro po strzale głową. Messi błyskawicznie, bo kilkadziesiąt sekund później, zrehabilitował się za kolejną niewykorzystaną sytuację. Messi założył "siatkę" Sereno i strzelił lewą nogą w kierunku dalszego słupka. Piłka odbiła się właśnie od niego i zatrzepotała w siatce bramki Hernandeza.

Gospodarze nie gościli właściwie pod bramką Valdesa, ale w 89. minucie zdołali trafić do siatki. Błąd obrony Barcelony wykorzystał Javi Guerra, który uderzył głową w światło bramki, a następnie skutecznie dobił piłkę.

Barcelona podwyższyła wynik w ostatniej akcji meczu. Tello, który pojawił się na placu gry kilkanaście sekund wcześniej, znalazł się oko w oko z Hernandezem i umieścił piłkę w bramce.

Real Valladolid nie wygrał z Barceloną od 2002 roku. Bilans bramek z sześciu ostatnich spotkań ligowych jest niekorzystny dla drużyny z Valladolid, piłkarze Realu strzelili tylko 2 razy, a dali sobie wbić aż 21 bramek.

Relacje na żywo możesz śledzić także na smartfonie!


Wszystkie relacje Sport.pl możesz czytać, także jeśli nie jesteś akurat przy komputerze. Nieważne, czy ścinasz choinkę, robisz zakupy, sprzątasz czy gotujesz - możesz śledzić relacje za pomocą smartfona lub innego urządzenia mobilnego. Zachęcamy do ściągnięcia aplikacji Sport.pl Live na Androida lub korzystania z mobilnej wersji naszego serwisu - m.sport.pl.






