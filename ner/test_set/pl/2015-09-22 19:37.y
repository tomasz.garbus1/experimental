::{league:Puchar Polski}. {team:Cracovia} wygrała z {team:GKS-em} w Katowicach Piłka nożna - Sport.pl::
@2015-09-22 19:37


Ta wygrana ma być lekarstwem na ostatnie kłopoty {team:Cracovii} w ekstraklasie. Po czterech z rzędu meczach bez zwycięstwa krakowianie liczyli na przełamanie. Styl nie rzucił na kolana, ale najważniejsze, że wynik był inny niż ostatnio w lidze.

Trochę różnił się też skład, choć wystarczyło rzucić na niego okiem, by dojść do wniosku, że {team:Cracovia} wcale nie traktuje tych rozgrywek po macoszemu. Nie było rewolucji, tylko kosmetyczne zmiany. Przede wszystkim przemeblowano obronę. Brakowało {player:Sretena Sretenovicia}, który był w tym czasie w drodze powrotnej z Belgradu (kilka dni wcześniej narodziła mu się córka i w klubie dostał wolne), więc parę stoperów awaryjnie stworzyli {player:Bartosz Rymaniak} i {player:Piotr Polczak}. Dla drugiego z nich był to sentymentalny powrót - w wieku 18 lat zadebiutował w ekstraklasie jako piłkarz... {team:GKS-u Katowice}.

Nieoczekiwanie na boisku pojawił się też {player:Paweł Jaroszyński} - miał usiąść na ławce, ale w ostatniej chwili, na rozgrzewce, kontuzji kolana doznał {player:Hubert Wołąkiewicz}. Dla 21-letniego obrońcy to mogła być duża szansa, bo w tym sezonie grywał tylko w trzecioligowych rezerwach. Czasami bywał niepewny, ale pomagał z przodu i po meczu dostał pochwały od trenera.

Posklejana naprędce defensywa zrobiła swoje, ale na sumieniu ma kilka błędów. Mogło zacząć się od tragedii. Gdyby {player:Adrian Frańczak} nie uderzył wprost w {player:Grzegorza Sandomierskiego}, goście już na początku meczu straciliby bramkę. A w końcówce przez chwilę nerwowo było tylko dlatego, że {player:Rymaniak} nieodpowiedzialnie sfaulował rywala w polu karnym.

Wcześniej przez długi czas wszystko utrzymywało się w normie. {team:Cracovia} prowadziła grę, a dwie bramki zdobyła według doskonale znanego scenariusza. {player:Mateusz Cetnarski} wrzucił piłkę na głowę {player:Miroslava Covili}, który w powietrzu nie miał sobie równych. W pierwszej połowie {player:Serb} trafił do siatki, w drugiej jego strzał obronił {player:Rafał Dobroliński}, ale dobitka {player:Erika Jendriszka} to było już za dużo.

{team:Cracovia} zrobiła swoje, choć momentami przypominała drużynę znudzoną. W pierwszej połowie w ślamazarnym tempie rozgrywała piłkę, ale {team:GKS} nie robił wiele, by uprzykrzyć jej życie. Jeśli nawet krakowianie w bezsensowny sposób tracili piłkę (zdarzało się!), to zaraz ją odzyskiwali, bo rywalowi brakowało pomysłu.

Większe emocje niż niektóre akcje budziły poszukiwania złotego pierścionka zaręczynowego. "Dziękujemy znalazcy! Uczciwość ponad wszystko" - podsumował je spiker.

Do gry wrócił {player:Deniss Rakels}, ale pokazał tylko kilka niecelnych strzałów. Łotysz przez dwa lata grał w {team:GKS-ie} i przed meczem nie ukrywał, że nie może się doczekać powrotu. Tym bardziej że ostatnio zabrakło go nawet w 18-osobowej kadrze na mecz ligowy z {team:Zagłębiem Lubin}. W Katowicach mógł pokazać, że w sprawie jego formy nie ma powodów do obaw, ale kiedy już doszedł do znakomitej okazji, kopnął w siatkę zabezpieczającą, a wściekły trener {manager:Jacek Zieliński} prawie wskoczył na murawę.

Denerwował się może dlatego, że dla jego piłkarzy awans był obowiązkiem. A kiedy {player:Grzegorz Goncerz} pewnie wykorzystał rzut karny, na trybunach zrobiło się głośno, ale gospodarze nie poszli za ciosem. Zamiast tego trzeciego gola zdobyła {team:Cracovia}. Wydawało się, że {player:Boubacar Diabang} nie ma prawa trafić do siatki z ostrego kąta, ale piłka odbiła się od nogi {player:Adama Czerwińskiego} i zaskoczyła bramkarza.

W ćwierćfinale {team:Cracovia} jeszcze w tym roku zagra z kolejnym pierwszoligowcem - {team:Zagłębiem Sosnowiec}.

{team:GKS Katowice} - {team:Cracovia} 1:3 (0:1)

Bramki: {player:Covilo} (19.), {player:Jendriszek} (70.), {player:Czerwiński} (85., sam.) - {player:Goncerz} (79., k.)

{team:GKS}: {player:Dobroliński} - {player:Czerwiński} Ż, {player:Kamiński}, {player:Prażnovski}, {player:Pietrzak} - {player:Pielorz} (76. {player:Burkhardt}), {player:Leimonas} - {player:Bębenek}, {player:Iwan} (68. {player:Trochim}), {player:Frańczak} - {player:Goncerz} Ż

{team:Cracovia}: {player:Sandomierski} - {player:Deleu}, {player:Rymaniak}, {player:Polczak}, {player:Wołąkiewicz} - {player:Covilo}, {player:Dabrowski} - {player:Rakels} (76. {player:Diabang}), {player:Cetnarski}, {player:Kapustka} (84. {player:Wdowiak}) - {player:Jendriszek} (90. {player:Szewczyk})






