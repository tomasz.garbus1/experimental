::Real Madryt ma problem ze swoją gwiazdą. "Zidane nie jest sprawiedliwy" Piłka nożna - Sport.pl::
@2019-08-26 19:43
url: http://www.sport.pl/pilka/7,65082,25124690,real-madryt-ma-problem-ze-swoja-gwiazda-zidane-nie-jest-sprawiedliwy.html

Stracony sezon Realu. "Zidane nie ma czarodziejskiej różdżki"
REKLAMA






Real Madryt niespodziewanie zremisował u siebie w meczu 2. kolejki hiszpańskiej La Liga z Realem Valladolid. Gdy w 82. minucie bramkę strzelił Karim Benzema, wydawało się, że "Królewscy" zdobędą komplet punktów. Sześć minut później wyrównał jednak Sergio Guardiola, a zespół Zinedine&aposa Zidane&aposa nie zdołał już odpowiedzieć i obydwa zespoły podzieliły się punktami.W 57. minucie na boisko wszedł Vinicius Junior. Piłkarz, który miał stać się gwiazdą Realu Madryt w nadchodzących rozgrywkach. W meczu przeciwko Realowi Valladolid został przesunięty z lewej strony na prawą, co znacznie wpłynęło na jego grę. "Marca" zwraca uwagę, że w tym sezonie prezentuje się znacznie gorzej niż w połowie poprzednich rozgrywek. Jednym z powodów takiego stanu rzeczy jest transfer Edena Hazarda, który zmusił Brazylijczyka do zmiany pozycji. Vinicius dodatkowo był zaskoczony tym, że nie wyszedł w podstawowym składzie.Real Madryt wiąże wielkie nadzieje z Viniciusem, ale ten ma problemyDziennikarze "Marki" podkreślają, że w Brazylijczyku pokładano ogromne nadzieje, jednak Zidane nie dał mu wystarczająco dużo czasu do zyskania pewności siebie. Wszyscy zachwycali się jego umiejętnościami, dopóki nie doznał kontuzji w spotkaniu przeciwko Ajaksowi Amsterdam w Lidze Mistrzów. Był jednym z nielicznych piłkarzy, który mógł zagrozić Barcelonie w "El Clasico" na początku tego roku, po którym otrzymał pierwsze powołanie do reprezentacji Brazylii. Do tych czasów mu jednak daleko. "Marca" dodaje, że "Zidane nie jest do końca sprawiedliwy wobec Viniciusa", a piłkarz czuje, że każdy jego błąd lub słabszy występ sprawi, że jego sytuacja pogorszy się jeszcze bardziej.





