::Wojciechowski walczy o start w Londynie. Ćwiczy nogę w basenie Piłka nożna - Sport.pl::
@2012-06-20 12:56
url: http://www.sport.pl/pilka/1,133416,11975788,Wojciechowski_walczy_o_start_w_Londynie__Cwiczy_noge.html


Jesteś kibicem z Bydgoszczy? Dołącz do nas na Facebooku Sport.pl Bydgoszcz »

Polska nadzieja na medal w Londynie musi walczyć z kontuzją mięśnia dwugłowego. Zaczął się wyścig z czasem. Tyczkarze rozpoczynają skakanie na olimpiadzie dopiero 8 sierpnia, ale Wojciechowski nie ma jeszcze nawet minimum kwalfikującego do reprezentacji na igrzyska

Musi skoczyć 5,72. Jest pewne, że nie zdąży w oficjalnym terminie mijającym w przyszłym tygodniu zdobyć wymaganej na Londyn wysokości.

Jego trener Włodzimierz Michalski i zawodnik mają nadzieję, że PKOl zmieni im termin uzyskania kwalifikacji. Proszą o przedłużenie do 28 lipca.

Jak się dowiedzieliśmy, PKOl chce zamknąć listę naszych reprezentantów na igrzyska do 10 lipca

Czy zgodzicie się na prośbę mistrza świata? - zapytaliśmy prezesa Polskiego Komitetu Olimpijskiego Andrzeja Kraśnickiego

- Paweł Wojciechowski był i jest jedną z naszych nadziei na medal na igrzyskach. Wniosek o przedłużenie terminu kwalifikacji zgłasza Polski Związek Lekkiej Atletyki Podejmiemy rozsądną i rozważną decyzję. Mamy jeszcze wszyscy trochę czasu. Niczego teraz nie wykluczam - odpowiada prezes PKOl.

Cały ten sezon jest dla 23-letniego sportowca niezwykle pechowy. Na początku grudnia, kiedy startowały przygotowania do igrzysk naprężona wyskoczyła mu z dłoni i uderzyła podczas treningu w twarz. Miał złamaną kość jarzmową, konieczna była operacja. Musiał przerwać zimowe treningi. Powrót do formy z poprzedniego roku, kiedy wygrał mistrzostwa świata i poprawił rekord Polski, okazał się bardzo trudny.. Najlepiej poszło mu w Bydgoszczy, cztery dni przed nieszczęsnym startem w Oslo. Wojciechowski pokonał poprzeczkę na wysokości 5,62.

Mistrz świata bardzo mocno przeżywa swoją dramatyczną sytuację. - Paweł nie należy do cierpliwych osób - mówi trener Michalski.

Wojciechowski jest w Spale i tam będzie przechodził rehabilitację kontuzjowanej nogi. - Nie przerywamy treningów, na ile będą one teraz oczywiście możliwe. Wszystkie zajęcia z obciążeniem nogi Paweł będzie wykonywać w basenie - wyjaśnia Michalski.

HITY GRUPOWEJ FAZY EURO. PIĘKNE GOLE I SUPER KIBICE






