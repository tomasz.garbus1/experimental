::Typujemy skład {team:Arki} na inaugurację wiosny. Cz. 3: Atak [SONDAŻ] Piłka nożna - Sport.pl::
@2016-03-02 12:17


To prawdopodobnie najtrudniejsza część naszego minicyklu. Czas na zastanowienie się i przeanalizowanie, kto zagra w {team:Arce} na pozycji numer dziesięć, a kto na "dziewiątce" w meczu inaugurującym rundę wiosenną {league:I ligi} w Katowicach. Kandydatów jest czterech. Ba, nawet pięciu, ale tych czterech poważnie bierzemy w kontekście wyjściowego składu.

Scenariusz nr 1 - wariant optymistyczny


Tak naprawdę chyba najmniej prawdopodobny do spełnienia. Bierzemy w nim pod uwagę, że w świetnej dyspozycji mentalnej i zdrowotnej jest najlepszy strzelec zespołu {player:Paweł Abbott}, na którego - mimo braku jakiekolwiek meczu od ponad miesiąca - od początku spotkania będzie chciał postawić trener {manager:Grzegorz Niciński}. Sam zainteresowany grą napastnik jeszcze w ubiegłym tygodniu mówił, że nie wszystko jest jeszcze, jak należy, ale fizycznie czuje się znakomicie. Problemem może być jedynie codzienne ogranie, które i tak z istniejącym składem {team:Arki} {player:Abbott} ma znakomite.

Nawet jednak jeśli napastnik teoretycznie będzie gotowy do gry od pierwszej minuty - a wszystko na to wskazuje - to wydaje się, że na tak ryzykowny ruch {manager:Niciński} się nie zdecyduje. Wiadomo, kusi. "Ubot" zagrałby w tym scenariuszu zapewne z {player:Rafałem Siemaszko}, z którym jest perfekcyjnie zgrany. Zresztą, mając nawet {player:Gastona Sangoya} i {player:Mateusza Szwocha}, a {player:Abbotta} w idealnym stanie - dziwnym ruchem byłoby zmienianie tego, co jesienią perfekcyjnie funkcjonowało, czyli {player:Siemaszkę}.

Poza tym {player:Siemaszko} to jeden z jaśniejszych punktów {team:Arki} zimowych przygotowań. Grzechem byłoby nie wystawić go od początku meczu (z Abbottem czy bez). Duet {player:Abbott} - {player:Siemaszko} w pełni dyspozycji zapewne namieszałby w obronie "Gieksy". Z racji zaś przede wszystkim statusu nowicjuszy w obecnej {team:Arce}, {player:Sangoy} i {player:Szwoch} mecz powinni zacząć na ławce rezerwowych.

Scenariusz nr 2 - wariant realistyczny


Z samych słów {player:Abbotta} można wywnioskować, że ten bardzo chce zagrać w Katowicach nawet i od początku, ale taka przyjemność zapewne go nie spotka. W wariancie realistycznym najlepszy strzelec {team:Arki} usiądzie na ławce rezerwowych i być może na placu pojawi się dopiero po przerwie. Kto zatem od zagra początku?

Najpewniejszym scenariuszem jest {player:Rafał Siemaszko} w roli najbardziej wysuniętego napastnika. Pozycję numer dziesięć okupi zaś {player:Szwoch}. Zresztą, w takiej konfiguracji {manager:Niciński} grał w ostatnich sparingach, wliczając w to ten najbliżej ligi, a więc przeciwko {team:Wiśle Płock}.

Opcja z {player:Gastonem Sangoyem} w składzie tak czy inaczej nie wchodzi w grę. Jest zdecydowanie zbyt wcześnie. {player:Argentyńczyk} dołączył do zespołu jako ostatni i potrzebuje z pewnością więcej czasu na aklimatyzację. Ujrzymy go na ławce, jeśli w ogóle zdążą w Gdyni dopiąć formalności biurokratyczne związane z możliwością jego gry w Polsce.

W odwodzie jest jeszcze {player:Grzegorz Tomasiewicz}, który jako zmiennik wielokrotnie wydatnie pomagał żółto-niebieskim w pierwszej rundzie. Rzadko jednak wskakiwał do składu dzięki tak zwanej "coach decision". Raczej były to sytuacje wymuszone. Mimo to ma na swoim koncie gola i trzy asysty, lecz z tego drugiego jest bardziej rozliczany. A to trzeci wynik w zespole.

Scenariusz nr 3 - wariant pesymistyczny


Nie ma takiego. Forma {team:Arki} z jesieni, ta rosnąca z przygotowań zimowych oraz bogactwo w ataku wykluczają jakikolwiek wariant pesymistyczny. Nikogo z linii pomocy z konieczności nie trzeba będzie przesuwać do ataku. Rotacja pozostanie tylko między pozycjami dziewięć i dziesięć. A jak mówią sami piłkarzy - w tym układzie nie ma zbyt dużej różnicy.

Nasz typ: {player:Mateusz Szwoch} - {player:Rafał Siemaszko}.

Typujemy skład {team:Arki} na inaugurację wiosny. Cz. 1: Obrona

Typujemy skład {team:Arki} na inaugurację wiosny. Cz. 2: Pomoc






