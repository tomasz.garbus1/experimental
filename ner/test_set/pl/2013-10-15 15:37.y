::Wilkowicz przed meczem {country:Anglia} - {country:Polska}: Ciszej nad tym {stadium:Wembley} Reprezentacja Polski::
@2013-10-15 15:37


Relacje z najważniejszych zawodów w aplikacji Sport.pl Live na iOS, na Androida i Windows Phone

Polski samograj


Kto się lubi bać, niech się boi, ale akurat tegoroczne {stadium:Wembley} raczej masuje ego, niż przeraża. Żadna polska reprezentacja nie była tam od lat witana z taką atencją i tyloma pochwałami. Od bohaterów 1973 roku, z którymi, jak napisał jeden z angielskich dziennikarzy, pogrzebać szanse remisem nie było wstyd, bo niecały rok później mogli i powinni wygrać mundial, po dzisiejszą drużynę, w której rywale widzą albo siłę uśpioną, albo skłóconą, tak czy owak serio się boją, że ta siła, jeśli wreszcie będzie dobrze wykorzystana, może ich zepchnąć do baraży. I wykluczyć tego wcale nie można, zwłaszcza że wystarczyłby do tego remis. A polscy piłkarze grają dziś na {stadium:Wembley} mecz z gatunku takich, jakie lubią najbardziej. Samograj. Po pierwsze, na wielkiej scenie, po drugie, już o nic. Z wielkim rywalem, przeciw któremu motywować ich nie trzeba, jeśli już, to raczej studzić zapał. Z rywalem, który musi wygrać, więc zostawi więcej przestrzeni pod własną bramką. Czego niby Polacy mają się bać, co mogą stracić, zwłaszcza po tylu ciosach przyjętych w eliminacjach? Przegrają dzisiaj? Trudno. Zremisują? To będzie jak zwycięstwo. Zwyciężą? Zostawią wspomnienia na lata.

Tylko i aż sparing


I dobrze, niech remisują, wygrywają, niech tym meczem ładnie żegnają nieładne eliminacje, podnoszą się na duchu czy czego jeszcze chcą. Tylko niech nam nikt nie wmawia, że ten mecz miałby zmienić zdanie o tym, co robili w eliminacjach, czy o pracy {manager:Waldemara Fornalika}. Możemy każdej polskiej kadrze obiecać wiarę do ostatniej sekundy, póki jeszcze matematyka daje jej szansę, tak jak to było przed meczem z {country:Ukrainą} w Charkowie. Bo to nie żadne pompowanie balonu (to nowe słowa wytrychy w naszym sporcie, jak nie wiesz, co powiedzieć, mów o pompowaniu balonu), tylko esencja sportu. Ale skoro już szanse są pogrzebane, to nie dajmy sobie wmówić, że drużyna, której głównym problemem jest to, że odzwyczaiła się od wygrywania w meczach o stawkę, może odkupić winy w sparingu. Supersparingu, sparingu roku, ale jednak dla niej tylko sparingu.

Sposób {manager:Janas}


Oczywiście, mydlenie sobie oczu ewentualnym sukcesem na Wembley zawsze będzie lepsze niż nie tak dawne mydlenie oczu gdańskim remisem - co tam remisem, bliskością niedoszłej wygranej! - w sparingu z rezerwowymi Niemcami, za {manager:Franciszka Smudy}. Ale tylko trochę lepsze. Z ostatnich wywiadów {manager:Waldemara Fornalika} można wysnuć wniosek, że trener wierzy, iż zadanie miał podobne do {manager:Smudy}: spokojnie budować, żeby w określonym miejscu i czasie, za rok czy półtora, drużyna była już gotowa do naprawdę dobrej gry. I że jest co doceniać, bo widać poprawę, jest powtarzalność, itd. Ale sensem eliminacji nie jest to, że widać poprawę, tylko widać punkty. Jeśli trener narzeka na brak czasu, niech spojrzy na {manager:Mychajło Fomienkę}, który zrobił na ławce trenerskiej {country:Ukrainy} lotną zmianę podczas eliminacji. Jeśli narzeka na nieprzychylne nastawienie mediów, niech sobie przypomni czasy {manager:Pawła Janasa}. Wyśmiewany {manager:Janas} nie mówił o poprawie i postępach, bo kadra to wzlatywała, to podupadała. Ale doleciała do mundialu 2006, bo łupiła równych sobie albo słabszych rywali bez litości. Tylko tyle i aż tyle. Doleciała do mistrzostw, choć z {country:Anglią} przegrała wtedy i u siebie, i na wyjeździe. I komu to przeszkadzało?

Zobacz też: Eliminacje MŚ. "Polska to najwygodniejszy przeciwnik dla {country:Anglii}, ale mecze o honor nam wychodzą"





