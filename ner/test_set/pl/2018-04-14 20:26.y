::{league:Bundesliga}. Wysoka wygrana {team:Bayernu}, gol {player:Lewandowskiego} Piłka nożna - Sport.pl::
@2018-04-14 20:26

Sobotnie spotkanie źle zaczęło się dla gospodarzy, bo już w 9. minucie gola dla {team:Borussii} strzelił {player:Josip Drmić}.
REKLAMA




Na odpowiedź mistrzów {country:Niemiec} czekaliśmy do 37. minuty, kiedy bramkę na 1:1 zdobył {player:Sandro Wagner}. Cztery minuty później 30-latek po raz drugi wpisał się na listę strzelców i dał prowadzenie {team:Bayernowi}.W 51. minucie na 3:1 podwyższył {player:Thiago Alcantara}, a w 68. minucie swoją bramkę zdobył też {player:David Alaba}. W 82. minucie wynik spotkania na 5:1 ustalił {player:Lewandowski}, który na boisku pojawił się w 69. minucie zmieniając {player:Wagnera}. Dla Polaka, który prowadzi w klasyfikacji strzelców {league:Bundesligi}, było to 27. trafienie w lidze.{team:Bayern}, który kolejne mistrzostwo {country:Niemiec} przypieczętował przed tygodniem, po 30 kolejkach ma na swoim koncie 75 punktów i aż o 23 "oczka" wyprzedza {team:Schalke 04}.





