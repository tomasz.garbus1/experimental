::{league:Primera Division}. {team:Barca} tworzy dream team Piłka nożna - Sport.pl::
@2010-05-19 23:22


Sport.pl na Facebooku! Sprawdź nas »

O odejściu {player:Villi} mówiło się od miesięcy. {team:Valencia} ma 500 mln euro długu, brakuje jej pieniędzy na dokończenie budowy stadionu. 29-letniego napastnika, który przez ostatnie pięć lat w 212 meczach strzelił 129 goli, chciały sprowadzić najlepsze zespoły {league:Premier League}, {team:Real Madryt} i {team:Barcelona}. Wygrali {team:Katalończycy}, w środę król strzelców {league:Euro 2008} podpisał czteroletnią umowę, będzie zarabiał 7 mln euro rocznie.

{player:Villa} w {team:Barcy}! {player:Fabregas} za chwilę. {player:Ibra} będzie musiał odejść?
Teraz władze {team:Barcy} zabrały się do ściągnięcia {player:Fabregasa}. 23-letni pomocnik powiedział już trenerowi {manager:Arsene'owi Wengerowi}, że chce odejść. {team:Arsenal} milczy, ale wiadomo, że łatwo piłkarza nie puści. Hiszpan to lider, wokół którego francuski szkoleniowiec budował przez ostatnie lata drużynę.

Od kilku miesięcy hiszpańska prasa, powołując się na przyjaciół {player:Fabregasa}, twierdziła jednak, że piłkarz chce wrócić do Katalonii. W juniorach {team:Barcy} {player:Cesc} grał w zespole z {player:Leo Messim}, odszedł, gdy miał 16 lat, bo nie widział szans na regularne występy w pierwszej drużynie.

W {team:Arsenalu} został gwiazdą {league:Premier League}, ale zabrakło mu sukcesów drużynowych. Jego kolekcja trofeów ogranicza się do zdobytego pięć lat temu {league:Pucharu Anglii}. - Świetnie, że chce do nas wrócić, ale teraz ruch należy do {team:Arsenalu}. {player:Cesc} jest dla nich ważnym piłkarzem i musimy to uszanować - powiedział dyrektor sportowy {team:Barcy} Txiki Begiristain.

{team:Katalończycy} proponują za {player:Fabregasa} 35 mln euro, {team:Arsenal} chce 10 mln więcej. {team:Barca} może też zapłacić i wysłać do {team:Londynu} {player:Yaye Toure}, który twierdzi, że {manager:Pep Guardiola} zbyt często sadza go na ławce rezerwowych.

Nie wiadomo jeszcze, jak trener {team:Barcy} pomieści tyle gwiazd w jedenastce. Z klubu prawdopodobnie odejdzie {player:Thierry Henry}, który rozmawia z {team:New York Red Bulls}.

O transferze {player:Villi} poinformował Jaume Ferrer, kandydat popierany przez prezesa Joana Laportę w czerwcowych wyborach. Laporta startować nie może, bo rządzi już dwie kadencje. Inni kandydaci twierdzą, że zakupy {player:Villi} i {player:Fabregasa} mają pomóc w zwycięstwie Ferrerowi. Według ostatnich sondaży faworytem jest Sandro Rosell, były członek zarządu klubu odpowiedzialny za sprowadzenie do zespołu {player:Ronaldinho}, wróg obecnego prezesa. Głosować chce na niego 55 proc. kibiców, na Ferrera - tylko 10 proc.

{team:Sevilla} z {league:Pucharem Króla} »



