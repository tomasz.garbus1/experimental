::Ajax dostał wyjątkową pomoc przed półfinałem Ligi Mistrzów. Wszystkie kluby Eredivisie wyraziły zgodę Piłka nożna - Sport.pl::
@2019-04-19 10:42
url: http://www.sport.pl/pilka/7,65043,24682603,ajax-dostal-wyjatkowa-pomoc-przed-polfinalem-ligi-mistrzow.html



Ajax po wtorkowym zwycięstwie 2:1 nad Juventusem, awansował do półfinału Ligi Mistrzów, gdzie zmierzy się z Tottenhamem. Pierwszy mecz zaplanowano na 30 kwietnia, co sprawiło, że pojawił się problem. Dwa dni wcześniej, w niedzielę 28 kwietnia, miała zostać rozegrana 33. kolejka ligi holenderskiej. Zgodnie z harmonogramem, wszystkie zespoły miały zagrać tego dnia o tej samej porze.
REKLAMA




Władze Eredivisie poszły jednak na rękę Ajaksowi, w związku z czym cała kolejka została przeniesiona na środę, 15 maja. Drużyna z Amsterdamu będzie wtedy już po dwumeczu z Tottenhamem, a w przypadku awansu do finału, ten zostanie rozegrany dopiero 1 czerwca.- Sądzimy, że to dobra propozycja, na podstawie zgody wszystkich drużyn, aby wszyscy rozegrali 33. i 34. kolejkę w tym samym czasie. Doceniamy fakt, że wszystkie kluby chciały współpracować w tej kwestii - powiedział Eric Gudde, dyrektor techniczny holenderskiej federacji.Ajax ma bardzo napięty grafik, ze względu na grę aż na trzech frontach. Obecny lider Eredivisie walczy o tytuł mistrza Holandii, zagra w półfinale Ligi Mistrzów, a dodatkowo 5 maja zmierzy się z Willem II w finale krajowego pucharu.





