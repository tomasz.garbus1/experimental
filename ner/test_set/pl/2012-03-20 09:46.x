::Członek zarządu PZPN: Lato musi ustąpić. Myśli, że jesteśmy naiwni jak przedszkolaki? Piłka nożna - Sport.pl::
@2012-03-20 09:46
url: http://www.sport.pl/pilka/1,95642,11376305,Czlonek_zarzadu_PZPN__Lato_musi_ustapic__Mysli__ze.html


- Lato powinien odejść za całokształt swoich działań. Nagrania to kropla, która przepełniła czarę - przekonuje w  rozmowie z "Super Expressem" Zbigniew Lach. Nagrania, o których wspomina, zostały przekazane prokuraturze przez Grzegorza Kulikowskiego, który przyczynił się w znacznym stopniu do odsunięcia ze stanowiska sekretarza generalnego PZPN Zdzisława Kręciny.

- Szefów PZPN mogą czekać duże kłopoty. W połowie lutego zeznawałem w prokuraturze i powiedziałem wszystko, co wiem, i pokazałem wszystko, co mam. A jest tego sporo. Jak choćby nagrania, na których Zdzisław Kręcina i Grzegorz Lato przyjmują ode mnie pieniądze - mówił w marcu Grzegorz Kulikowski.

'Taśmy prawdy'. Grzegorz Lato chce 1-3 bańki od kontraktu.

Wniosek o odwołanie prezesa


Zdaniem Lacha, na posiedzeniu zarządu 29 marca, Jacek Masiota ma złożyć wniosek o zwołanie nadzwyczajnego zjazdu wyborczego, podczas którego doszłoby do odwołania obecnego prezesa i wyboru nowego.

Członek zarządu PZPN nie wierzy także w wersję Grzegorza Laty, który twierdzi, że po przyjęciu koperty od Kulikowskiego, nie otworzył jej, lecz przekazał do sejfu PZPN. - Śmiech pusty człowieka ogarnia, kiedy słyszy, że nic nikomu nie mówiąc pół roku trzymał w sejfie kopertę i nie wiedział co w niej jest. Co on myśli, że my jesteśmy naiwni jak przedszkolaki i uwierzymy w te bajki?! - złości się Lach.

Oświadczenie przekazane mediom


W środę 14 marca Grzegorz Lato wydał do mediów oficjalne oświadczenie w tej sprawie. "W związku z artykułem "Dałem Lacie łapówkę" opublikowanym w Super Expressie 14.03.2012 r., oświadczam, że nie jest prawdą, że przyjąłem od Grzegorza Kulikowskiego jakąkolwiek korzyść majątkową. 25 stycznia 2012 roku złożyłem do warszawskiej prokuratury zawiadomienie o podejrzeniu przestępstwa korupcji bądź spowodowania przeciwko mojej osobie postępowania karnego" - możemy w nim przeczytać.

Grzegorz Lato zapowiedział także złożenie pozwu cywilnego o naruszenie dóbr osobistych.

"Perquis? Francuski śmieć", "Lato? To prostak" - kontrowersyjne wypowiedzi Tomaszewskiego






