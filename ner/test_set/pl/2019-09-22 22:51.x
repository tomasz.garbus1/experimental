::Real Madryt pokonał lidera. Niedoceniana gwiazda bohaterem meczu Piłka nożna - Sport.pl::
@2019-09-22 22:51
url: http://www.sport.pl/pilka/7,65082,25223791,real-madryt-pokonal-lidera-niedoceniana-gwiazda-bohaterem-meczu.html

Zidane planował rewolucję w Realu, ale niewiele z tego wyszło. Co teraz zrobi?
REKLAMA






Pozycja Zinedine&aposa Zidane&aposa w Realu Madryt nie jest pewna, choć "Królewscy" w pierwszych czterech meczach zdobyli 8 punktów. Kibice coraz głośniej domagają się zmiany szkoleniowca, a media wymieniają wśród kandydatów do objęcia Realu m.in. Jose Mourinho. Kolejne mecze są więc okazją do odbudowy pozycji francuskiego trenera.Niewielu sądziło, że Real Madryt stać na korzystny wynik w meczu przeciwko Sevilli. Pierwsze 15 minut potwierdzało ich obawy. Potem jednak "Królewscy" zaczęli grać tak, jak na nich przystało. Do przerwy utrzymywał się bezbramkowy remis, choć swoje okazje mieli Carvajal i Hazard. Fenomenalnie bronił jednak Vaclik.Benzema bohaterem RealuPo przerwie długo Real nie potrafił realnie zagrozić bramce Sevilli. To jednak zmieniło się w 64. minucie, gdy Karim Benzema wykorzystał perfekcyjne dośrodkowanie Carvajala. Gospodarze walczyli jak mogli o doprowadzenie do wyrównania, w 87. minucie piłka znalazła się nawet w bramce Courtois, ale Chicharito znalazł się na pozycji spalonej.Dzięki tej wygranej Real Madryt przesunął się na pozycję lidera La Ligi, mając tyle samo punktów co Athletic Bilbao, który pokonał w niedzielę Alaves 2:0. Sevilla spadła na 5. pozycję, mając punkt mniej.





