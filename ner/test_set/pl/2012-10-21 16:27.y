::W Stróżach lepszy {team:Kolejarz}. Co się dzieje z {team:Zawiszą}? Piłka nożna - Sport.pl::
@2012-10-21 16:27




Jesteś kibicem i jesteś z Bydgoszczy? Musisz zostać fanem Facebooka Sport.pl Bydgoszcz »

Choć to dopiero druga porażka {team:Zawiszy} w obecnym sezonie, to powinna ona jednak poważnie zaniepokoić działaczy, szkoleniowców i samych piłkarzy. Po świetnym początku sezonu bydgoski zespół ostatnio ciuła punkty, gra podopiecznych {manager:Jurija Szatałowa} pozostawia wiele do życzenia. W Stróżach szkoleniowiec po raz kolejny zdecydował się na zmiany w wyjściowym składzie. Tym razem wpuścił nawet dwóch napastników. Efekt jednak był mierny, bo do nieskutecznego {player:Pawła Abbotta} dołączył równie nieskuteczny {player:Tomasz Chałas}. Jeśli do tego dodamy kolejne błędy obrońców, to wychodzi obraz zespołu, który przeżywa kryzys. Bydgoszczanie nadal znajdują się w czołówce tabeli, ale w niedzielę stracili pozycję wicelidera. W dodatku teraz czekają nas mecze z bardzo silnymi drużynami. {team:Zawisza} musi się obudzić, bo w przeciwnym razie wiosną o awans będzie niezwykle trudno.



{team:Kolejarz} - {team:Zawisza} 2:1 (1:1)

Bramki: 0:1 {player:Geworgian} (29.), 1:1 {player:Niane} (44. karny), 2:1 {player:Gajtkowski} (47.)

{team:Kolejarz}: {player:Forenc} - {player:Pietrzak} (46. {player:Stefanik}), {player:Markowski}, {player:Szufryn}, {player:Walęciak} - {player:Wolański}, {player:Niane}, {player:Bocian} (67. {player:Arłukowicz}), {player:Giesa}, {player:Nitkiewicz} - {player:Gajtkowski} (78. {player:Pieczara}).

{team:Zawisza}: {player:Kaczmarek} - {player:Stefańczyk}, {player:Kopacz}, {player:Skrzyński}, {player:Jankowski} - {player:Geworgian}, {player:Zawistowski}, {player:Hermes}, {player:Błąd} (56. {player:Masłowski}) - {player:Chałas} (63. {player:Leśniewski}), {player:Abbott} (76. {player:Ostalczyk}).



Inne wyniki 13. kolejki: {team:Flota Świnoujście} - {team:Cracovia} 1:2, {team:Arka Gdynia} - {team:Olimpia Grudziądz} 0:1, {team:GKS Katowice} - {team:Okocimski Brzesko} 1:2, {team:Stomil Olsztyn} - {team:Miedź Legnica} 1:2, {team:Dolcan Ząbki} - {team:GKS Tychy} 0:1, {team:Warta Poznań} - {team:Polonia Bytom} 2:1, {team:ŁKS Łódź} - {team:Sandecja Nowy Sącz} 3:2, {team:Bogdanka Łęczna} - {team:Termalica Bruk-Bet Nieciecza} 1:0



Tabela I ligi





1. {team:Flota}133426-6


2. {team:Cracovia}122620-12


3. {team:Zawisza}132526-9


4. {team:Termalica}122419-11


5. {team:Miedź}132218-13


6. {team:Olimpia}132117-13


7. {team:GKS T.}132010-9


8. {team:Bogdanka}131915-16


9. {team:Arka}131813-9


10. {team:Warta}131821-19


11. {team:Kolejarz}131516-19


12. {team:Sandecja}131514-28


13. {team:GKS K.}131416-19


14. {team:Dolcan}131316-19


15. {team:Stomil}131114-19


16. {team:Okocimski}131112-21


17. {team:ŁKS}131015-30


18. {team:Polonia}1349-25







