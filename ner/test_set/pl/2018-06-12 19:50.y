::{country:Polska} - {country:Litwa} 4:0. {player:Robert Lewandowski} z dwoma golami, trafili też {player:Dawid Kownacki} i {player:Jakub Błaszczykowski} Reprezentacja {country:Polski}::
@2018-06-12 19:50

{country:Polska} zaczęła spotkanie w ofensywnym ustawieniu. {manager:Adam Nawałka} postawił aż na trzech nominalnych napastników: {player:Dawida Kownackiego}, {player:Roberta Lewandowskiego} i {player:Arkadiusza Milika}.
REKLAMA




Początek nie był jednak najlepszy w wykonaniu Polaków. {player:Darvydas Sernas} strzelił gola dla {country:Litwy}, ale był na spalonym - polska obrona założyła dobrą pułapkę ofsajdową. Polacy przyspieszyli po kilkunastu minutach. Pierwszą groźną akcję przeprowadzili w 17. minucie. {player:Jacek Góralski} dobrze zagrał do {player:Bartosza Bereszyńskiego}, ale jego podanie do {player:Arkadiusza Milika} zostało przechwycone przez litewskiego obrońcę.Dwa ciosy {player:Lewandowskiego} Dwie minuty później Polacy już prowadzili po pięknej zespołowej akcji. {player:Dawid Kownacki} dośrodkował w pole karne, {player:Maciej Rybus} zgrał piłkę do {player:Roberta Lewandowskiego}, który z zimną krwią umieścił piłkę w bramce.

{player:Lewandowski} drugi cios wymierzył w 33. minucie. Z rzutu wolnego uderzył pod poprzeczkę, piłka odbiła się za linią bramkową i wypadła z powrotem w boisko. Arbiter po konsultacji z sędzią VAR uznał gola.Jeszcze przed przerwą dwie dobre okazje miał {player:Kownacki}. Najpierw z bliskiej odległości trafił w bramkarza po świetnej akcji {player:Bereszyńskiego}, a po chwili minimalnie chybił uderzeniem z ponad 30 metrów. W 43. minucie świetnie po strzale {player:Darvydasa Sernasa} interweniował {player:Łukasz Fabiański}.Bramkarz {team:Swansea} nie wyszedł na drugą połowę, został zmieniony przez {player:Wojciecha Szczęsnego}. Na boisko weszli również {player:Łukasz Piszczek} i {player:Łukasz Teodorczyk}, a nieco później {player:Michał Pazdan}.Groźny wślizg - {player:Darvydas Sernas}

Groźnie wyglądająca sytuacja miała miejsce w 53. minucie. {player:Darvydas Sernas} wyciął {player:Bartosza Bereszyńskiego} "równo z trawą", ale za swój wślizg dostał jedynie żółtą kartkę. Kilka chwil później litewski napastnik zagroził bramce Polaków, ale nie zdołał pokonać {player:Szczęsnego}.Polacy cieszyli się z prowadzenia 3:0 przez kilka chwil w 67. minucie. {player:Maciej Rybus} dośrodkował z rzutu rożnego, {player:Thiago Cionek} zgrał piłkę głową, {player:a Grzegorz Krychowiak} trafił piętą do siatki. Jednak tuż przed strzałem zagrał ręką, przez co sędzia nie uznał gola.Pierwszy gol {player:Kownackiego} Cztery minuty później było już 3:0. {player:Łukasz Teodorczyk} znakomicie podał do {player:Bereszyńskiego}, który wyłożył piłkę {player:Kownackiemu} jak na tacy - był to pierwszy gol w jego drugim występie w dorosłej kadrze.

{player:Błaszczykowski} wykorzystał karnegoW 82. minucie {country:Polska} dostała rzut karny. Obrońca {country:Litwy} zagrał piłkę ręką przy dośrodkowaniu {player:Jakuba Błaszczykowskiego}, co sędzia zauważył po weryfikacji wideo. Skrzydłowy reprezentacji {country:Polski} zamienił "jedenastkę" na gola! Był to jego pierwszy karny od feralnej sytuacji z meczu z {country:Portugalią} w ćwierćfinale {league:Euro 2016} (1:1, 3:5 w karnych, {player:Błaszczykowski} jako jedyny z Polaków nie trafił). {player:Błaszczykowski} pojawił się na boisku w 70. minucie przy ogromnej euforii kibiców na {stadium:Stadionie Narodowym}.W 88. minucie w niezłej sytuacji w bramkę nie trafił {player:Arkadiusz Milik}, który uderzył tuż obok słupka. Na kilkanaście sekund przed końcem kibice wstali z miejsc i zaczęli skandować "dziękujemy!", po czym sędzia zakończył spotkanie.{country:Polska} rozegra pierwszy mecz na {league:MŚ 2018} 19 czerwca z {country:Senegalem}. Kolejne spotkania z {country:Kolumbią} (24 czerwca) i {country:Japonią} (28 czerwca).

Zobacz podstawowy skład polski na mecz {country:Polska} - {country:Litwa}





