::Niespodzianka w Bydgoszczy. {team:Lech} przegrywa z {team:Zawiszą} Piłka nożna - Sport.pl::
@2015-03-14 19:52


Zespół z Poznania nie będzie dobrze wspominał pierwszej połowy spotkania. Dotknął go pech i kara od arbitra. Już w początkowych minutach spotkania urazu doznał stoper {player:Paulus Arajuuri}. Fin grał jeszcze potem, ale ból był coraz silniejszy. W 24 min zastąpił go Węgier {player:Tomas Kadar}. 60 sekund później {team:Lech} doznał znacznie większej straty. {player:Arnaud Djoum}, który w środku pola zagrał za {player:Łukasza Trałkę} pauzującego za żółte kartki faulował {player:Alvarinho}. Belg urodzony w {country:Kamerunie} zobaczył drugą żółtą kartkę - i to w ciągu 4 minut. Wcześniej był ukarany za faul na {player:Kamilu Drygasie}. {player:Djoum} był jedynym nowynm graczem w jedenastce {team:Lecha} w porównaniu z poprzednim spotkaniem. Trener bydgoszczan {manager:Mariusz Rumak} nie zmienił żadnego z piłkarzy, którzy wystąpili w zwycięskim meczu z {team:Wisłą Kraków}.

Zawisza grając z przewagą zawodnika od razu zdobył przewagę. Wcześniej {team:Lech} miał kilka niezłych akcji. W 18 min {player:Zaur Sadajew} mijał {player:Andre Micale}, ale dośrodkowanie z końcowej linii wyłapał {player:Grzegorz Sandomierski}. Niepokonany w tym roku bramkarz Zawiszy obronił minutę później strzał {player:Szymona Pawłowskiego}.

REKORD POBILI KIBICE I PIŁKARZE {team:ZAWISZY}

W 26 min {player:Alvarinho} miał dobrą okazję, ale pierwsze jego uderzenie zablokowali obrońcy, a drugie obronił {player:Maciej Gostomski}. Bramka dla gospodarzy padla po rzucie rożny. To był już 121 róg w sezonie i pierwszy gol dla zawiszan po tym stałym fragmencie gry. Po centrze {player:Alvarinho}, piłkę zgrał pod bramkę {player:Andre Micael}, a {player:Josip Barisić} głową z 3 m strzelił gola.

W 36 min mogło być 1:1. {player:Marcin Kamiński} miał znakomitą okazję, strzelał głową po centrze {player:Gergo Lovrencicsa}. {player:Sandomierski} znowu miał szczęście - odprowadzał tylko wzrokiem piłkę. Minęła o centymetry słupek.

Od razu w przerwie {manager:Rumak} zmienił {player:Drygasa}. Pomocnik {team:Zawiszy} zobaczył bowiem żółtą kartkę, jedenastą w sezonie. Trener bał się czerwonej i wprowadził za niego {player:Cornela Predescu}.

Zadaniem bydgoszczan w drugiej połowie było szanowanie piłki i zwiększanie czasu jej posiadania - w pierwszej części meczu było 54:46 dla {team:Zawiszy}. Tempo spadło. Gospodarze częściej dzielili się piłką w środku pola niż pchali ją do przodu, pod bramkę {player:Gostomskiego}. Grą {team:Zawiszy} jak profesor kierował {player:Iwan Majewski}, Akurat w sobotę po raz pierwszy dostał powołanie do reprezentacji {country:Białorusi} - to efekt jego bardzo dobrej postawy w tym roku.

{team:Lechici} próbowali kontrować i szukali szansy w stałych fragmentach gry. Emocje przyszły w samej końcówce. {player:Sandomierskiemu} znowu dopisało szczęście, kiedy w 84 min {player:Lovrencics} uderzył z 20 metrów. Piłka odbiła się od poprzeczki. Po chwili bydgoszczanie mieli kontrę, ale strzał {player:Gieorgiego Alawerdeszwilego} wybił z bramki obrońca.

- Cieszymy się i myślimy już o {team:Ruchu} - mówi skrzydłowy bydgoszczan {player:Sebastian Kamiński}. Zawisza w pięciu meczach w tym roku zdobył 11 pkt. Po 19 spotkaniach rundy jesiennej miał ich 9.

SKORŻA I RUMAK O MECZU [WIDEO]






