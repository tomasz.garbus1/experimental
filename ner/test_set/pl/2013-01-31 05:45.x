::Kasper Hämäläinen będzie na zgrupowaniu z Lechem Poznań czy z reprezentacją kraju? Piłka nożna - Sport.pl::
@2013-01-31 05:45
url: http://www.sport.pl/pilka/1,70993,13323502,Kasper_Hamalainen_bedzie_na_zgrupowaniu_z_Lechem_Poznan.html


Po podpisaniu umowy z Lechem Kasper Hämäläinen opuścił Poznań. Wrócił do Szwecji i ma dołączyć do drużyny Mariusza Rumaka w piątek, przed zaplanowanym na ten weekend wylotem na zgrupowanie w hiszpańskiej Esteponie. Nie wiadomo jednak, czy nowy pomocnik będzie z Lechem przez całe zgrupowanie. Dostał bowiem powołanie na towarzyski mecz z Izraelem i być może będzie najpierw musiał spełnić obowiązki reprezentanta kraju, a dopiero potem stawi się w Hiszpanii. To powołanie przyszło wtedy, gdy Kasper Hämäläinen był jeszcze zawodnikiem Djurgaardens Sztokholm. "Kolejorz", planując transfer, wiedział o tym, że Fin ma obowiązki reprezentacyjne.






