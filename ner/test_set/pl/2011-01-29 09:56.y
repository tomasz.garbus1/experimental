::{league:LE}. {team:Braga} szpieguje {team:Lecha} w {country:Hiszpanii} Piłka nożna - Sport.pl::
@2011-01-29 09:56


Zarówno wygrany w środę 4:0 sparingowy mecz {team:Lecha} z {team:Lokomtiwem Płowdiw}, jak i czwartkowy trening miał pilnych obserwatorów. Na wzgórzu obok boiska stał zaparkowany w krzakach samochód z portugalskimi tablicami rejestracyjnymi. Wewnątrz dwóch mężczyzn uważnie przyglądało się zajęciom, a jeden z nich kamerą rejestrował, to co działo się na treningu.

Obserwatorzy zagadnięci przez przedstawiciela poznańskiego klubu, odpowiedzieli że są dziennikarzami. Nie potrafili jednak wytłumaczyć, dlaczego filmują z ukrycia. {team:Lechici} nie mają wątpliwości, że to wysłannicy {team:Bragi}. Jest to o tyle dziwne, że przedstawiciele {team:Lecha} oglądali mecze Portugalczyków oficjalnie na stadionie.

Wszystko o {team:Lechu Poznań} w specjalnym serwisie Sport.pl »






