::Smuda: Przegraliśmy przez brak skuteczności, wynik krzywdzący Reprezentacja Polski::
@2010-09-07 23:01
url: http://www.sport.pl/pilka/1,65037,8348113,Smuda__Przegralismy_przez_brak_skutecznosci__wynik.html


Franciszek Smuda:


- Jestem zadowolony z tempa gry i szybkich akcji. Widać progres, coraz częściej budujemy akcje podaniami z pierwszej piłki. Zawodzi skuteczność, przez nią przegraliśmy 1:2. Ten wynik jest dla nas krzywdzący, bo zasłużyliśmy na zwycięstwo. Niektórzy napastnicy potrzebują dziesięciu sytuacji podbramkowych, by strzelić gola, inni dwóch. Nie winię jednak Ireneusza Jelenia i Roberta Lewandowskiego. W klubach udowadniają, że potrafią zdobywać bramki.

Przy pierwszym golu dla Australii (okiwany Michał Żewłakow, po pierwszej akcji Australijczyków - red.) zabrakło asekuracji, nad tym też pracujemy na treningach.

Błaszczykowski: w drewnianym kościele cegła nam spadła na głowę

Jakub Błaszczykowski:


Mieliśmy przewagę, szczególnie po przerwie, ale znowu zaczynamy wracać do naszego ulubionego tematu, czyli nieskuteczności. Szkoda, że nie wyrównaliśmy. Martwi, że rywalom wystarczą dwie okazje, żeby strzelili dwa gole. Sami sobie stwarzamy kłopoty. Mówi się, że biednemu, to i w drewnianym kościele cegła na głowę spadnie. Nam, w dwóch ostatnich meczach, spadły trzy. Z Ukrainą straciliśmy gola w doliczonym czasie, teraz po jedynych akcjach gości. Jeśli chodzi o moją precyzję w polu karnym, to zawsze może być lepiej. Podawałem mocno, wzdłuż linii, niestety piłka odbijała się od rywali nie w tę stronę, co trzeba. Coraz bardziej zaczynamy potrzebować zwycięstwa, piąte spotkanie bez wygranej doskwiera.

Wiem, że trener Borussii Dortmund Juergen Klopp nie oglądał spotkania z Ukrainą, nie wiem czy widział mecz z Australią. Chyba powinien zainteresować się polską kadrą, bo gra w niej trzech jego zawodników. Do poprawy są też nasze stałe fragmenty.

Robert Lewandowski:


Nie wiem, jak to możliwe, że w ten sposób tracimy gole. Za łatwo. Byliśmy lepsi, ale co z tego, skoro przegraliśmy. Ostatnio karnego wykonywałem podczas zimowego zgrupowania kadry w Tajlandii. Wtedy trafiłem. Teraz strzeliłem źle, za lekko. Zobaczyłem, że żaden z kolegów nie kwapi się do jego wykonania, a że czułem się na siłach, więc kopnąłem. Jak się okazało - źle.

Jeśli chodzi o grę, było przyzwoicie. Atakowaliśmy, byliśmy przy piłce, mieliśmy okazje. Tyle, że jeszcze nam brakuje jeśli chodzi o wykańczanie akcji. W końcu musimy zacząć wygrywać, na razie zaczęliśmy strzelać gole.

Michał Żewłakow:


Można się cieszyć ze stylu, ale nie chodzi o piękno tylko o zwycięstwa. One dodają pewności nam piłkarzom, radości - kibicom, oraz motywacji do pracy na przyszłość. Martwi nieskuteczność w ataku i, niestety, obronie.

Byłem zamieszany w obie sytuacje. Przy pierwszej, nim piłka wpadła, były ze dwa rykoszety. Przy karnym napastnik mądrze oszukał sędziego. Nie uważam, że go faulowałem. Był blisko mnie, zaczepił swoją nogą o moją i arbiter gwizdnął.

Po komentarzach w szatni wiem, że powoli zaczyna nas przytłaczać brak wygranej popartej niezłą grą. To będzie światełko w tunelu, na razie są przebłyski, brakuje ciągłości i wygranej.

99 występ w kadrze to moment ogromnej satysfakcji. Być może dostanę powołanie na mecze październikowe i wtedy przekroczę magiczną barierę?






