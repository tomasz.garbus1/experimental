::Portowcy wysoko, coraz wyżej! Piłka nożna - Sport.pl::
@2009-03-22 21:00
url: http://www.sport.pl/pilka/1,128220,6411893,Portowcy_wysoko__coraz_wyzej_.html


Co musi cieszyć - forma portowców idzie w górę. Ozdobą sobotniego meczu były bramki Tomasza Parzego i Piotra Petasza.

Potężny strzał w okienko z 30 metrów (Petasz) to było trafienie godne Ligi Mistrzów, akcja Ferdinanda Chifona zakończona pięknym golem Parzego kandydowałaby do nagród w polskiej ekstraklasie. Dwie bardzo efektowne bramki potwierdziły przewagę portowców w spotkaniu z Czarnymi. Szczecinianie grali skutecznie i z polotem, znaczniej lepiej niż na inaugurację z MKS Kluczbork. To również "zasługa" przeciwników, którzy nie przestraszyli się faworytów i na Twardowskiego zaprezentowali otwarty, dość ofensywny futbol.

- Gdybyśmy wykorzystali sytuację z pierwszych minut, mogło być ciekawiej - komentował trener Janusz Kubot, zapewne mając w pamięci akcję Pascala Ekwueme.

Błyskotliwy prawoskrzydłowy przejął źle wybitą piłkę, kiwnął obrońców w polu karnym... i z 7-8 m trafił w boczną siatkę. Pogoń odpowiedziała strzałami Damiana Sieniawskiego, Petasza i Parzego (zablokowany, obroniony, niecelny), lecz ekipa z Żagania nadal nie zamierzała pilnować wyniku 0:0. Grający na szpicy Marcin Jankowski (jesienią rezerwowy Pogoni), wspierany przez pomocników, kilkakrotnie zmusił do błędu szczecińską defensywę. Sposobem na ujarzmienie przyjezdnych okazał się stały fragment - dobrze bity korner, główka Piotra Komana i 1:0. Od tego momentu było łatwiej - "Parzol" znakomicie rozdzielał piłki, skrzydłowi uciekali kryjącym, Paweł Skrzypek i Marcin Woźniak podłączali się do ataków. Dominacja granatowo-bordowych stawała się coraz wyraźniejsza. Gol do szatni właściwie rozstrzygnął losy meczu. Po przerwie pogoniarze kontrolowali sytuację, a po wspaniałym trafieniu Petasza grali już rozluźnieni. Trener Piotr Mandrysz z ławki wybrał dwóch młodzieżowców, natomiast ostatnie 10 min zarezerwował na debiut Michała Szczyrby (ofensywny pomocnik sprowadzony z Piasta Gliwice długo pauzował w okresie przygotowawczym). Idealny moment - wysokie prowadzenie, fajna atmosfera na trybunach. Portowcy w efektownym stylu wywalczyli pozycję wicelidera.

- Przeciwnik postawił wysoko poprzeczkę, spotkanie kosztowało nas dużo zdrowia, ale ułożyło się dobrze. Wykorzystane stałe fragmenty, skuteczna akcja tuż przed przerwą, przekonujące zwycięstwo! To bardzo cieszy - przyznał Mandrysz.

Jedyne, co może martwić, to dłuższa absencja najskuteczniejszego napastnika Pogoni - Marka Kowala. Zawodnik jest po zabiegu artroskopii (wiązadła krzyżowe) i w tym sezonie raczej nie zagra. W sobotę atak tworzyli Damian Sieniawski i Mikołaj Lebedyński. Zdaniem szkoleniowca, "wypadli obiecująco".

Pogoń Szczecin - Czarni Żagań 3:0 (2:0)

Bramki: Koman (22., asysta Petasz), Parzy (45., asysta Chifon), Petasz (61.).

Pogoń: Binkowski - Skrzypek, Nowak, Dymek, Woźniak, Chifon (72. Rydzak), Koman, Parzy (80. Szczyrba), Petasz, Sieniawski (68. Wólkiewicz), Lebedyński.

Widzów: 4 500



Kotwica nadal liderem...


... ale przewaga kołobrzeżan nad Pogonią stopniała do punktu. Zespół Tomasza Arteniuka bezbramkowo zremisował wyjazdowe spotkanie z Miedzią Legnica, walczącą o opuszczenie strefy spadkowej. W pierwszej połowie "Kotwa" miała nieznaczną przewagę, jednak nie udało się jej udokumentować zdobyciem bramki (Krzysztof Rusinek, Filip Marciniak). W drugiej części meczu twardo grający gospodarze skutecznie bronili remisu.

Miedź Legnica - Kotwica Kołobrzeg 0:0. Kotwica: Sobański - Małkowski Ż (68. Żdanow), Stankiewicz Ż, Grocholski, Kempa, Pietroń, Sawczuk, Rawa, Stróż, Marciniak (78. Misztal), Rusinek

Chemik tylko zremisował


Policzanie na inaugurację wiosny - z Lechią Zielona Góra - mierzyli w zwycięstwo, więc rozpoczęli z animuszem, ale po 20 min gra się wyrównała. Składnych akcji było jak na lekarstwo. Goście wykorzystali jedną i przed przerwą objęli prowadzenie. Drużyna Marka Czerniawskiego długo nie mogła sforsować defensywy rywali. Atak pozycyjny kończył się daleko od bramki, kontry nie wychodziły. Gdy wreszcie Marek Sajewicz doprowadził do remisu, gospodarze odzyskali wiarę w zwycięstwo, narzucili żywsze tempo, wywalczyli kilka rzutów wolnych i rożnych, ale drugiego gola nie zdobyli.

Chemik Police - Lechia Zielona Góra 1:1 (0:1). Bramki - Chemik: Sajewicz (72.); Lechia: Żebrowski (36.). Chemik: Wiśniewski - Krzyżanowski, Bieniek, Michał Szałek, Brzeziński, Jóźwiak (60. Pachulski), Jarymowicz, Mateusz Szałek, Dutkiewicz (76. Baranowski), Sajewicz, Ciołek (55. Erlich).

Pozostałe wyniki 21. kolejki: GKS - Zawisza 0:0, Raków - Victoria 1:0, Polonia - Unia 1:2, Elana - Zagłębie 0:1, Jarota - MKS 4:1.



1. Kotwica Kołobrzeg214236-18


2. Pogoń Szczecin214132-22


3. Gawin Królewska Wola194039-18


4. MKS Kluczbork213736-24


5. Raków Częstochowa213527-18


6. Unia Janikowo213422-16


7. Zagłębie Sosnowiec213127-19


8. Elana Toruń213126-25


9. Nielba Wągrowiec202937-30


10. Zawisza Bydgoszcz212727-28


11. Jarota Jarocin212728-23


12. Czarni Żagań212420-35


13. GKS Tychy212417-21


14. Lechia Zielona Góra212328-41


15. Miedź Legnica212322-33


16. Chemik Police201922-37


17. Victoria Koronowo21149-30


18. Polonia Słubice211419-36









