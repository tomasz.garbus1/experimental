::Wywalczył dwa karne i trafił do jedenastki kolejki Piłka nożna - Sport.pl::
@2015-03-24 18:00


Brazylijskiemu skrzydłowemu wystarczyło 45 minut w meczu z {team:Piastem Gliwice}, aby zostać wyróżnionym za swój występ w 25. kolejce. 27-latek zaliczył bardzo dobrą drugą połowę, był aktywny, a przede wszystkim wywalczył dwa rzuty karne (pierwszy zmarnowany przez {player:Olivera Kapo}, drugi wykorzystany przez {player:Jacka Kiełba}).

Dla {player:Luisa Carlosa} to druga nominacja wiosną. Poprzednio pomocnik {team:Korony} trafił do jedenastki kolejki ze występ z {team:Podbeskidziem Bielsko-Biała}. Wcześniej trzykrotnie w jedenastce kolejki znalazł się Bośniak {player:Vlastimir Jovanović}, dwukrotnie {player:Kamil Sylwestrzak} i {player:Jacek Kiełb} oraz po razie {player:Piotr Malarczyk} i {player:Radek Dejmek}.






