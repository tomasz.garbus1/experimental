::Puchar Anglii. Szczęsny: Każdy wiedział, że Henry strzeli Piłka nożna - Sport.pl::
@2012-01-10 13:38
url: http://www.sport.pl/pilka/1,65080,10939528,Puchar_Anglii__Szczesny__Kazdy_wiedzial__ze_Henry.html


Sport.pl w mocno nieoficjalnej wersji... Polub nas na Facebooku »

Zobacz bramkę Henry'ego na Z czuba.tv »

"Czemu wszyscy tak się podniecają tym, że Thierry zdobył zwycięskiego gola?! Każdy wiedział, że to się stanie, tak czy inaczej" - napisał tuż po zakończeniu meczu na swoim twitterowym koncie reprezentant Polski.

Chwilę później dodał jednak wpis, z którego wcale nie wynika, iż ma takie chłodne podejście do tematu. "byłem tam wtedy, kiedy Thierry Henry zdobył zwycięskiego gola przeciwko Leeds w swoim powrocie do AFC (Arsenal Football Club - mariw)" - cieszył się Szczęsny.

Thierry Henry, najlepszy strzelec w historii Arsenalu, powrócił do klubu po czteroipółletniej przerwie. Kanonierzy wypożyczyli go na dwa miesiące z amerykańskiego New York Red Bulls - w lidze MLS trwa przerwa w rozgrywkach. W ponownym debiucie wszedł na boisko w 68. minucie meczu trzeciej rundy Pucharu Anglii przeciwko Leeds i zdobył jedynego gola meczu, dzięki któremu jego drużyna awansowała dalej.

Media w Anglii zachwycone ponownym debiutem Thierry'ego Henry'ego »

Hity zimowego okienka. Oni zmienią kluby?






