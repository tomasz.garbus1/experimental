::Były piłkarz reprezentacji Dariusz W. skazany w sprawie korupcji Piłka nożna - Sport.pl::
@2009-09-16 09:53
url: http://www.sport.pl/pilka/1,95642,7043867,Byly_pilkarz_reprezentacji_Dariusz_W__skazany_w_sprawie.html


Co będzie dalej z Dariuszem W.?

W. poddał się dobrowolnie karze. Był oskarżony o organizowanie w sezonie 2003/2004 wśród piłkarzy zrzutki pieniędzy, z których później opłacali sędziów pomagających Kolporterowi Koronie w awansie do II ligi. W trakcie śledztwa prowadzonego w ubiegłym roku przez wrocławską prokuraturę obaj przyznali się do winy i zaproponowali dobrowolne poddanie się karze.

Sąd przychylił się do wniosków prokuratury, która domagała się trzech lat w zawieszeniu na pięć lat oraz grzywny w wysokości 100 tysięcy złotych dla Dariusza W. i dwóch i pół roku więzienia w zawieszeniu na trzy lata oraz 30 tys. zł grzywny dla jego ówczesnego asystenta Andrzeja W.

Aktem oskarżenia w tej sprawie objęte są 43 osoby - piłkarze, trenerzy, działacze, sędziowie, obserwatorzy PZPN-u. 28 z nich zdecydowało dobrowolnie poddać się karze. W głównej sprawie na ławie oskarżonych zasiądzie m.in. Jerzy E. junior, syn byłego selekcjonera kadry narodowej, obecnie wiceprezes PZPN-u.

Dariusz W. rozegrał w reprezentacji 53 spotkania. W karierze zawodniczej  reprezentował barwy Gwardii i Legii Warszawa. W sezonie 1989/1990 przeniósł się do Szkocji, gdzie bronił barw Celticu Glasgow. Kilka lat później grał w angielskim Reading.
Jako trener zdobył mistrzostwo Polski z Polonią Warszawa w 2000 roku. Wyczyn ten powtórzył sześć lat później z innym stołecznym klubem - Legią.

O aferze korupcyjnej przeczytasz  tutaj





