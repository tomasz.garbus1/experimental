::{league:Primera Division}. {team:Saragossa} na celowniku {player:Messiego} Piłka nożna - Sport.pl::
@2012-04-07 08:46


Zabawy z piłką - czytaj blog autorki »

Odległość z {team:Barcelony} do {team:Saragossy} wynosi 300 kilometrów, trasę pokonuję się w blisko trzy godziny, zdecydowanie więcej  dzieli oba zespoły w ligowej tabeli.  {team:Blaugrana} jest druga, a {team:Saragossa} trzecia, ale od końca, do drużyny z Katalonii traci 44 punkty.

Jeszcze jakiś czas {team:Saragossa} była głównym kandydatem do spadku, Tymczasem zawodnicy {manager:Manolo Jiméneza} wydostali się z dna tabeli i choć wciąż znajdują się na miejscach zagrożonych spadkiem, to do siedemnastego {team:Villarrealu} brakuje im już tylko czterech punktów. Patrząc na znakomitą formę zespołu w ostatnich tygodniach, awans w górę tabeli wydaje się możliwy.

W ostatnich sześciu spotkaniach zespół z Aragonii odniósł cztery zwycięstwa, pokonując {team:Atletico}, {team:Valencię}, {team:Sporting} w trzech kolejnych meczach, a przegrywając zaledwie raz.  - Jeśli zagramy dobrze, możemy {team:Barcelonę} przestraszyć. Przy wsparciu kibiców możemy wygrać. - zapowiada {player:Ivan Obradović}, obrońca gospodarzy. A jego kolega z drużyny {player:Franco Zuculini} dodaje - Zwycięstwo z {team:Barceloną} nie jest niemożliwe.

{team:Barcelona} na stadion {league:La Romareda} jedzie jako faworyt. Choć na wyjeździe w tym sezonie nieraz zawodziła, po feralnej przegranej z {team:Osasuną} 3:2, kroczy od zwycięstwa do zwycięstwa. Jednak zawodnicy {manager:Pepa Guardioli} doskonale zdają sobie sprawę, że drużyna z {team:Saragossy} nie ma nic do stracenia, walczy o życie i łatwo skóry nie sprzeda.


- To będzie bardzo ciężki mecz. - zapowiada {manager:Guardiola}. Kataloński trener nie będzie mógł w sobotnim spotkaniu skorzystać z usług byłego zawodnika {team:Saragossy} - {player:Gerarda Piqué}, który doznał kontuzji we wtorkowym spotkaniu z {team:Milanem}.

Z pewnością na spotkanie z Aragończykami ręce zaciera {player:Leo Messi}, który z drużyny z {team:Saragossy} uczynił jeden ze swoich ulubionych celów. W jedenastu spotkaniach, które rozegrał przeciwko niej, strzelił dziewięć goli.

Choć katalońska prasa, co rusz przekonuje, że zwycięstwo w lidze jeszcze jest możliwe, {manager:Guardiola} na przedmeczowej konferencji po raz kolejny podkreślił, że zdania nie zmienił, mistrzostwo {team:Barcelona} już straciła. Gdy ostatnio tak powiedział - {team:Real} stracił w lidze punkty.

Zabawy z piłką - czytaj blog autorki »






