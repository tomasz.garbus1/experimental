::{league:Bundesliga}. {player:Marcin Kamiński} opuści {team:VfB Stuttgart}? Piłka nożna - Sport.pl::
@2018-08-03 13:24

W lipcu 2016 roku 26-letni {player:Marcin Kamiński} opuścił {team:Lecha} i trafił do {team:Stuttgartu}. W minionym sezonie początkowo był ważnym ogniwem, ale wiosną jego rola w zespole była stale marginalizowana.
REKLAMA




Obecnie - według "Bilda" - jest dopiero piątym obrońcą w klubowej hierarchii. Wszystko wskazuje, że będzie musiał zmienić drużynę. Zainteresowany jego pozyskaniem jest {team:Hamburg}.{player:Kamiński} zostanie w {team:Stuttgarcie}, jeśli drużynę opuści {player:Benjamin Pavard}, który jest na liście życzeń {team:Bayernu Monachium}.{player:Kamiński} był w szerokiej kadrze podczas przygotowań do mundialu. Ostatecznie {player:Kamil Glik} się jednak wykurował, a stoper {team:Stuttgartu} opuścił zgrupowanie i nie pojechał do {country:Rosji}.***{player:Hildeberto} odszedł z {team:Legii Warszawa}

Transfery. {team:Manchester United} nie odpuszcza. {player:Robert Lewandowski} częścią dużej wymiany?{manager:Leszek Ojrzyński}: Jestem gotów na {team:Legię}





