::{player:Thiago Alcantara} może wrócić do {team:FC Barcelony} Piłka nożna - Sport.pl::
@2017-10-16 16:35

{player:Thiago Alcantara} to wychowanek {team:FC Barcelony} i eksperci uznawali go nawet za potencjalnego następcę {player:Iniesty} i {player:Xaviego}. W 2013 roku {team:Barcelona} podjęła jednak decyzję o sprzedaży swojego zawodnika, a {player:Alcantara} trafił do {team:Bayernu} za 25 milionów euro.
REKLAMA




Hiszpański Don Balon pisze, że {team:Barcelona} rozważa ewentualne sprowadzenie {player:Alcantary}, gdyby nie wyszedł jej transfer {player:Phillipe Coutinho} z {team:Liverpoolu}.{player: Alcantara} ma za sobą 100 występów w barwach {team:Barcelony}. 26-latek strzelił 11 bramek i miał 20 asyst. Jego statystyki w {team:Bayernie} są nieco lepsze, bo piłkarz zagrał 129 meczów, strzelił 20 bramek i dołożył 23 asysty.Wyciągnięcie {player:Alcantary} z {team:Bayernu} również nie będzie łatwą sprawą, bo kontrakt zawodnika wygasa dopiero w 2021 roku.





