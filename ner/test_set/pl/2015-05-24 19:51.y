::{player:Robert Lewandowski}: Nie dziwię się, że trener {manager:Klopp} się wzruszył Piłka nożna - Sport.pl::
@2015-05-24 19:51


Jakub Ciastoń: Skończył pan swój pierwszy sezon w {team:Bayernie Monachium}, nie wszystkie cele udało się zrealizować, ale chyba nie ma pan powodów, by nie być zadowolonym?


{player:Robert Lewandowski}: Jasne, że się cieszę, to mój pierwszy w karierze sezon w {team:Bayernie} i oceniam go pozytywnie. To był ciężki sezon i niewiele zabrakło, byśmy nie poprzestali na mistrzostwie {country:Niemiec}, ale zagrali też w finale {league:Pucharu Niemiec} i {league:Ligi Mistrzów}. Dziś i jutro świętujemy mistrzostwo. Ostatnie trzy ligowe mecze nam nie wyszły, ale zmobilizowaliśmy się, żeby na koniec fajnie pożegnać się z kibicami na własnym stadionie.

Jak pan postrzega swój sezon indywidualnie? Porównując lata w {team:Borussii} i ten pierwszy rok w {team:Bayernie}, nie widać wielkiej różnicy.


- Oceniam sezon bardzo pozytywnie. Było dużo ważnych strzelonych bramek. Cieszę się z tego, co udało nam się zrobić. Odpadliśmy w dwóch półfinałach, ale to się zdarza, taka jest piłka. Mieliśmy dużo problemów kadrowych, kontuzji w tych kluczowych momentach sezonu, możliwe, że w najmocniejszym składzie moglibyśmy powalczyć o więcej. Ale i tak walczyliśmy, staraliśmy się, daliśmy z siebie wszystko. W {team:Bayernie} czuję się bardzo dobrze, wszystko w klubie funkcjonuje tak jak należy, mamy wielu wspaniałych piłkarzy, na treningach czuć najwyższą jakość. W {team:Bayernie} czuję, że staję się lepszym piłkarzem. Wierzę, że kolejny sezon może być lepszy niż ten. I dla klubu, i dla mnie.

Długo trwał proces, gdy pan uczył się {team:Bayernu}, a {team:Bayern} uczył się pana?


- Wiadomo, że pierwsze miesiące to było wzajemne poznawanie się. Ale z każdym tygodniem, miesiącem czułem się lepiej i pokazywałem, że umiem strzelać bramki w różnych rozgrywkach, a do tego grać bardzo zespołowo.

Oblał pan piwem {player:Bastiana Schweinsteigera} na murawie po meczu, prawie wszyscy wracali do szatni skąpani w piwie, ale to chyba nie koniec świętowania....


- Z {player:Bastianem} mam zawsze świetny kontakt, to jeden z moich najlepszych kolegów. Widziałem, że stoi sam, pomyślałem, że może poczuł się osamotniony i sprezentowałem mu parę litrów piwa (śmiech). Tak jest, będziemy się bawić dziś wieczorem i pewnie całą noc [rozmawialiśmy w sobotę - j.c.], a potem w niedzielę czeka nas zabawa z kibicami w centrum miasta. Będą z nami rodziny, wszyscy bardzo cieszymy się z mistrzostwa. Przed nami dwa długie dni.

Widzieliśmy, że podczas fety całkiem dobrze pan tańczył...


- Mamy w drużynie kilku Brazylijczyków, oni lubią tańczyć i to się wszystkim czasem udziela (śmiech). To coś fajnego, oczywiście nie zawsze, ale czasem warto się powygłupiać, odprężyć.

Mało brakowało, by obronił pan koronę króla strzelców, do {player:Alexandra Meiera} stracił pan ostatecznie dwie bramki i zajął drugie miejsce ex aequo z {player:Arjenem Robbenem} z 17 trafieniami.


- Od początku zakładałem, że pierwszy sezon w {team:Bayernie} to nie jest czas na podbijanie swoich życiowych rekordów. Będą kolejne sezony, będą kolejne szanse. Jestem zadowolony z 17 bramek w lidze. Plus sześć w {league:Lidze Mistrzów}, plus kilka w innych rozgrywkach, jak na pierwszy sezon w klubie, to jest dobry wynik.

W czerwcu arcyważny mecz z {country:Gruzją} w eliminacjach {league:Euro 2016}. Myśli pan już trochę o tym spotkaniu?


- No właśnie, trochę świętowania, trochę odpoczynku, ale za chwilę znów trzeba mocno się skoncentrować. Na reprezentację trzeba zachować formę, świeżość, będziemy mieli na szczęście tydzień treningów przed spotkaniem, więc liczę, że wszyscy będziemy w najwyższej dyspozycji i optymalnie przygotujemy się do meczu.

Ciągle gra pan w masce, kiedy skutki kontuzji przestaną dokuczać, kiedy będzie mógł ją pan zdjąć?


- Kontrolę będę miał pewnie dopiero na kadrze, mam nadzieję, że z {country:Gruzją} zagram już bez maski, ale decyzja będzie należała do lekarzy.

Do reprezentacji wraca {player:Kuba Błaszczykowski}, czy to będzie duże wzmocnienie na {country:Gruzję}?


- Na pewno tak, liczmy na trzy punkty, każdy, kto przyjeżdża i jest optymalnie przygotowany, jest dla trenera wzmocnieniem. Najlepiej byłoby, gdyby pan trener miał kłopot bogactwa, bo do tej pory mieliśmy sporo kłopotów zdrowotnych w kadrze. Mam nadzieję, że wiele z nich pomyślnie się do tego czasu zakończy.

Piękne pożegnanie zgotowali fani {team:Borussii Dortmund} rozgrywającemu ostatni sezon {manager:Juergenowi Kloppowi}, który płakał jak dziecko, żegnając się z klubem. {team:Borussia} to rzeczywiście tak specjalne miejsce?


- Myślę, że dla każdego, kto spędził w klubie tyle lat, z takimi sukcesami i emocjami jak {manager:Juergen Klopp}, to nic dziwnego, że przy pożegnaniu emocje są wysokie i trudno się pożegnać. Ja też spędziłem w {team:Dortmundzie} cztery lata i to nie był łatwy moment, gdy odchodziłem, ale czasem tak bywa, że trzeba podjąć decyzję o zmianie. Jeśli chodzi o trenera {manager:Kloppa}, to jest to fantastyczny szkoleniowiec, który na pewno znajdzie za chwilę inną pracę i będzie mógł podjąć nowe wyzwania. Nie dziwię się, że się wzruszył. W {team:Borussii} na dobre zaczęła się jego przygoda z poważną piłką, zdobył pierwsze wielkie trofea, w jego przypadku skończyło się więc coś ważnego, ale jestem pewien, że dopisze do tej książki kolejne rozdziały.

Niemieccy dziennikarze zapytali {manager:Kloppa}, czy wyobrażałby sobie pracę w {team:Bayernie}. Odpowiedział krótko: "tak".


- Nigdy nie można mówić nigdy. Nikt nie wie, co się zdarzy za kilka lat, a trenerzy przecież pracują dłużej niż piłkarze. Wszystko jest możliwe.

Czy wiadomo, jak drużyna się zmieni w kolejnym sezonie? Trener {manager:Guardiola} zostaje, to chyba dobra wiadomość?


- Zupełnie nie wiem, czy będzie jakaś przebudowa i jaka. Trzeba poczekać na okienko transferowe. Na pewno bez względu, co się stanie, będziemy walczyć o te same cele, bo zawsze walczymy o najwyższe, to się nie zmieni.

O błyskotliwej karierze i pasjach {player:Roberta Lewandowskiego} przeczytaj w książce "Pogromca {team:Realu}" >>

Fala krytyki, gole i problem z {player:Robbenem}. To nie był łatwy sezon dla {player:Lewandowskiego}

PUMA Koszulka Borussia Dort...				 					Adidas Koszulka domowa Baye...				 					Spokey Piłka nożna 80640				    Sprawdź ceny »   Porównaj ceny »   Sprawdź ceny »      źródło: Okazje.info






