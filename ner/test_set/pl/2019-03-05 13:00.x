::Były piłkarz Lecha Poznań stanął przed sądem. Odpowie za pobicie oraz posiadanie broni i narkotyków Piłka nożna - Sport.pl::
@2019-03-05 13:00
url: http://www.sport.pl/pilka/7,65039,24518227,byly-pilkarz-lecha-poznan-stanal-przed-sadem-odpowie-za-pobicie.html



Nicki Bille Nielsen przed sądem. Długa lista przewinień byłego gracza LechaNicki Bille Nielsen ma za sobą traumatyczne doświadczenia. W grudniu, podczas imprezy świątecznej trzy osoby próbowały zamordować piłkarza. Nielsen został postrzelony i trafił do szpitala. Zaraz po jego opuszczeniu, Duńczyk stanął przed wymiarem sprawiedliwości.
REKLAMA




Wśród zarzutów, jakie prokuratura stawia byłemu graczowi Lecha Poznań są: nielegalne posiadanie broni i narkotyków oraz pobicie portiera przed jednym z nocnych klubów. Gdy Nielsen przebywał w szpitalu straszył pielęgniarkę pobiciem. Miał też zastraszyć rowerzystę przy użyciu pistoletu gazowego.Zawodnikowi może grozić kilka miesięcy pozbawienia wolności.Duński piłkarz od lat jest na bakier z prawem. Nielsen ma już za sobą odsiadkęPrzyzwyczailiśmy się już, że o Nielsenie głośniej jest nie ze względu na jego grę, ale z powodu pozaboiskowych wybryków. Po raz pierwszy do więzienia trafił w 2009 roku (na 40 dni), a w czerwcu 2018 r. skazano go na miesiąc więzienia za uprawianie seksu oralnego w miejscu publicznym.Napastnik grał w Lechu Poznań w latach 2016-2018. Nie wyróżnił się niczym szczególnym – w 32 ligowych meczach strzelił 4 gole. Po odejściu ze stolicy Wielkopolski Nielsen grał przez chwilę w greckim Panionios F.C. i duńskim Lyngby Boldklub. Obecnie pozostaje bez klubu.





