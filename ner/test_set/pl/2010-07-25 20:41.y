::Wygrana {team:Górnika} we Wronkach Piłka nożna - Sport.pl::
@2010-07-25 20:41


W ostatnim meczu na obozie we Wronkach {team:Górnik} pokonał pierwszoligową {team:Flotę Świnoujście}. Jedynego gola zdobył kapitan zabrzan {player:Adam Banaś}

{team:Górnik Zabrze} - {team:Flota Świnoujście} 1:0 (0:0)

Bramka: 1:0 {player:Banaś} (58., karny).

{team:Górnik}: {player:Nowak} (46. {player:Stachowiak}) - {player:Bemben} (64. {player:Danch}), {player:Banaś}, {player:Jop}, {player:Magiera} (83. {player:Marciniak}) - {player:Bonin} (70. {player:Bębenek}), {player:Przybylski} (56. {player:Balát}), {player:Kwiek} (75. {player:Leszczak}), {player:Sikorski} (87. {player:Kopacz}) - {player:Zahorski} (75. {player:Chałas}), {player:Świątek} (67. {player:Wodecki}).





