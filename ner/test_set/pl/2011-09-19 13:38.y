::{league:Liga Europy}. 'Napastnicy {team:Odense} muszą być bezwzględni' Piłka nożna - Sport.pl::
@2011-09-19 13:38


Poprzednie rozgrywki dzisiejsi rywale zakończyli dwie pozycje na strefą spadkową. W tym spisują się lepiej i zajmują siódme miejsce. - Są groźni zwłaszcza u siebie. Walczą o każdą piłkę, więc nie możemy odpuszczać. Zagrają bardzo agresywnie i musimy odpłacić się tym samym. Ja i pozostali napastnicy powinniśmy być bezwzględni pod bramką - przekonuje {player:Peter Utaka}, atakujący {team:Odense}.

Strzelec drugiej bramki w meczu z {team:Wisłą} uważa, że jego drużyna może być spokojna o zwycięstwo, jeśli zagra podobnie jak w meczu w Krakowie. - Pewność siebie czerpiemy ze zwycięstw z {team:Silkeborgiem} w lidze i z mistrzami {country:Polski} w {league:LE}. Czeka nas trudny mecz, ale jesteśmy w dobrej formie i to sprawia, że wszystko jest prostsze - uważa reprezentant {country:Nigerii}.

Po meczach z soboty i niedzieli {team:Odense} spadło na szóste miejsce. W ośmiu kolejkach zgromadziło 14 punktów. Wciąż deklaruje walkę o mistrzostwo, ale do pierwszego {team:FC Kopenhaga} traci 11 punktów.






