::Syn byłego prezesa Barcelony w Arsenalu Piłka nożna - Sport.pl::
@2011-09-23 12:38
url: http://www.sport.pl/pilka/1,65082,10341707,Syn_bylego_prezesa_Barcelony_w_Arsenalu.html


Joan Laporta był prezesem "Katalończyków" przez siedem lat, w lipcu 2010 r. zastąpił go Sandro Rossell. Za czasów Laporty rozpoczęła się wojna transferowa między Barceloną, a Arsenalem. - Tylko w tym sezonie zainwestowaliśmy w zespół juniorów 7 mln euro, ale mamy ogromny problem z Anglikami, którzy podbierają nam wychowanków - mówił w maju prezes Barcelony. - Wcześniej wykorzystywali ligę francuską, teraz celują w nas. Nie mają własnego systemu szkolenia, więc szukają ratunku gdzie indziej. I oferują 14- lub 15-latkom astronomiczne zarobki, których my ze względów etycznych nie możemy zaproponować - wyjaśnił Laporta.

Zaczęło się osiem lat temu, gdy Fábregas, jeden z najzdolniejszych uczniów barcelońskiej szkółki, wyjechał do Londynu. Później dołączył do niego Fran Mérida. Dziś w szkółce Arsenalu trzech piłkarzy, którzy futbolowego elementarza uczyli się w Katalonii. Sprowadzeni wiosną 16-letni Hector Bellerin i Jon Toral kosztowali 400 i 350 tys. euro. Temu ostatniemu Wenger miał obiecać debiut w pierwszej drużynie już w sezonie 2012/13. Hiszpanie szacują, że w Londynie obaj dostali nawet 35 razy większą pensję niż w Barcelonie. Także dlatego, że angielskie przepisy pozwalają na podpisywanie profesjonalnych kontraktów już z 16-latkami, a hiszpańskie - wyłącznie z dorosłymi.

Laporta sprowadził na Camp Nou piłkarzy Arsenalu Alaksandra Hleba i Thierry'ego Henry'ego. Starał się też o Fabregasa, ale wykupić 24-letniego rozgrywającego udało się dopiero Rossellowi.






