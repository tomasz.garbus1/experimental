::Rafał Siemaszko wraca do Arki Piłka nożna - Sport.pl::
@2015-06-24 15:49
url: http://www.sport.pl/pilka/1,65044,18242003,Rafal_Siemaszko_wraca_do_Arki.html


Arka awansuje do ekstraklasy? Podyskutuj na Facebooku >>

Siemaszko jest wychowankiem Orkanu Rumia. Długo występował w niższych ligach, aż wreszcie w sezonie 2010/11 został wypożyczony do Arki. Gdynianie występowali wówczas w ekstraklasie. Siemaszko zagrał w dwunastu meczach, ale bramki nie zdobył.

Po zakończeniu sezonu ówczesny szkoleniowiec Arki Petr Nemec uznał, że Siemaszko nie będzie mu już potrzebny, i ten wrócił do Orkana. Na trzecioligowym szczeblu zawodnik przebywał jednak tylko przez jeden sezon i w czerwcu trafił do Gryfa Wejherowo, gdzie miał okazję występować przez dwa lata.

W poprzednim sezonie Siemaszko reprezentował barwy pierwszoligowej Chojniczanki Chojnice. Zagrał 31 meczów, w których zdobył sześć bramek.

28-latek jest trzecim wzmocnieniem żółto-niebieskich bieżącego lata. Wcześniej do klubu znad morza trafili bowiem Tadeusz Socha i Alan Fialho.

Obserwuj trojmiasto.sport.pl na Twitterze - @3miasto_sportpl >>

PO ZWYCIĘSTWACH JESTEM "NITEK", PO PORAŻKACH NICIŃSKI - SZCZERA ROZMOWA Z TRENEREM ARKI GDYNIA






