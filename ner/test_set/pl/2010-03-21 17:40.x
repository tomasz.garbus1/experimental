::Mówi napastnik Pogoni Piłka nożna - Sport.pl::
@2010-03-21 17:40
url: http://www.sport.pl/pilka/1,128220,7685142,Mowi_napastnik_Pogoni.html


Mam się nie poddawać


Marcin Klatt na początku spotkania miał dwie doskonałe sytuacje. Nie wykorzystał ani jednej. Były napastnik Warty Poznań świetnie dochodzi do sytuacji, ale pod bramką zachowuje się jak junior. Coś go paraliżuje. Sobotni mecz nie był pierwszym, w którym mógł przesądzić o wyniku. Trener Mandrysz usprawiedliwia podopiecznego: - Marcin jest zblokowany i niestety potrzebuje gola, by się odblokować. Nie chciałbym byśmy go dołowali, bo to klasowy zawodnik i jeszcze wiele dobrego zrobi dla naszej drużyny.

Łukasz Kasprzyk: W każdym spotkaniu dochodzisz do sytuacji, ale nie potrafisz ich wykończyć. Dlaczego?


Marcin Klatt: Nie wiem. To był dopiero pierwszy mecz, w którym miałem tak dogodne sytuacje. To nie jest tak, że nie chcę zdobywać bramek. Bardzo chciałem, ale nie wyszło. Zwłaszcza w pierwszych minutach spotkania powinienem wykorzystać miejsce, w którym się znalazłem. Gdyby mi się udało, byłoby po meczu. Moja bramka ustawiłaby spotkanie i pewnie byśmy nie przegrali. Takie jest jednak życie napastnika, którego rozlicza się ze zdobywania goli.

Skończyło się pechową porażką?


- Według mnie - tak. Graliśmy naprawdę dobrze, ale nieskutecznie.

Co powiedział ci trener po spotkaniu?


- A co mógł mi powiedzieć? Żebym się nie załamywał. Przed nami kolejne spotkania: w środę Puchar Polski, a w weekend kolejny ligowy pojedynek.

Macie bardzo mało czasu, by pozbierać się po tym spotkaniu.


- Zgadza się, każda porażka boli. Nas tym bardziej, bo po raz trzeci nie zbieramy kompletu punktów. Musimy się podnieść - po to mamy środowy ćwierćfinał. Trzeba po prostu wygrać, a później gromadzić punkty w kolejnych spotkaniach.

MARCIN KLATT Z SANDECJĄ




Czasy gry45


Gole0


Asysty0


Faulował/faulowany0/1


Żółta/czerwona kartka0


Strzały celne/niecelne:



Strzały z pola karnego1/2


Strzały zza pola karnego0/1


Strzały prawą nogą3


Strzały lewą nogą1


Strzały głową0


Spalony0


Dryblingi udane/nieudane1/3


Podania celne/niecelne:



Podania do przodu3/4


Podania do tyłu4/0


Przechwyty2


Straty5







