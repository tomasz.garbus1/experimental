::Liga egipska. Zawodnik zniósł kontuzjowanego rywala z boiska [WIDEO]  Piłka nożna - Sport.pl::
@2015-06-21 20:19
url: http://www.sport.pl/pilka/1,65085,18182630,Liga_egipska__Zawodnik_zniosl_kontuzjowanego_rywala.html


Nie był to bynajmniej akt samarytański. Saad Samir chciał jak najszybciej wznowić grę, gdyż zegar pokazywał już 89. minutę, ale w meczu był remis 1:1. Dla drugiego w tabeli Al Ahly oznaczało to stratę punktów i utratę szansy na zmniejszenie strat do prowadzącego Zamaleka.
Samir wziął na ręce zwijającego się z bólu rywala i wyniósł za linię boczną. Obejrzał za to żółtą kartkę. Sędzia doliczył potem jeszcze kilka minut, w 98 minucie czerwoną kartkę zobaczył jeden z graczy gości, ale wynik nie uległ zmianie. W tabeli prowadzi Zamalek z przewagą dziewięciu punktów nad Al Ahly.







