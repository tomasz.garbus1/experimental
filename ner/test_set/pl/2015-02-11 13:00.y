::Wychowanek {team:Stomilu} zagra w {team:Arce}. Ciężko na to pracował Piłka nożna - Sport.pl::
@2015-02-11 13:00


Już kilka lat temu mówiło się o {player:Karłowiczu} jako obiecującym piłkarzu młodego pokolenia. Swoją karierę rozpoczynał w {team:Stomilu Olsztyn}, którego barwy reprezentował przez pięć sezonów. W 2009 roku jego talent dostrzegł {manager:Władysław Żmuda}, ówczesny szkoleniowiec reprezentacji {country:Polski} do lat 17.

Jeszcze w czerwcu tego samego roku, po występie {player:Karłowicza} w biało-czerwonych barwach, na testy zaprosiła go {team:Legia Warszawa}. Wtedy jednak na przeszkodzie stanęła kontuzja kręgosłupa, która uniemożliwiła olsztynianinowi pokazanie pełni swoich umiejętności.

- Pierwsze kroki w roli piłkarza stawiał pod wodzą {manager:Janusza Czerniewicza} - mówi {manager:Adam Łopatko}, drugi trener pierwszoligowego {team:Stomilu}. - Od zawsze jego największym atutem była lewa noga. Co więcej, jest silny fizycznie i jak na swój wiek [21 lat - red.] zaawansowany pod względem technicznym.

{player:Piotr Karłowicz} trafił następnie do trzecioligowej {team:Mrągowii Mrągowo} (2010-2011) i właśnie tam grając, zwrócił na siebie uwagę skautów {team:Wisły Kraków}. Wystarczy wspomnieć, że w rundzie wiosennej zdobył 11 bramek w 13 meczach. O transferze do 13-krotnego mistrza {country:Polski} ostatecznie przesądziły dwudniowe testy w stolicy Małopolski.

Kariera młodego olsztynianina nie była jednak usłana różami. - Nie wszystko wyglądało tak, jak to sobie wyobrażałem i jakbym tego sobie życzył. Podpisywałem wówczas kontrakt z zespołem mistrza kraju, który aspirował do gry w {league:Lidze Mistrzów} - opowiada Piotr Karłowicz, teraz zawodnik {team:Arki Gdynia}. - Miałem możliwość trenowania ze znakomitymi piłkarzami. Dla chłopaka z Olsztyna, który pół roku wcześniej grał jeszcze w {team:Mrągowie}, to było coś wyjątkowego.

Mierzący 188 cm wzrostu zawodnik nie dostał szansy na debiut w seniorskiej drużynie {team:Białej Gwiazdy}. Co gorsza, również w rezerwach nie wiodło mu się najlepiej. W {league:Młodej Ekstraklasie} w trykocie {team:Wisły} rozegrał 50 meczów i strzelił sześć goli.

Aż w końcu, w sezonie 2013/2014 przeszedł do pierwszoligowych {player:Wigier Suwałki}, gdzie wystąpił w 29 spotkaniach, czterokrotnie trafiając do bramki przeciwnika. W rundzie jesiennej tego sezonu zagrał czterokrotnie, ale ani razu nie wpisał się na listę strzelców.

- Kiedy {player:Piotrek} występował w {team:Suwałkach}, zdobywał dużo ważnych bramek - podkreśla {manager:Łopatko}. - Transfer do {team:Arki} jest dla niego bardzo ważny. Jeśli w tym klubie mu się powiedzie, to jego kariera bardzo się rozwinie.

Olsztynianin z drużyną z Trójmiasta ćwiczy od początku przygotowań do rundy rewanżowej. Miał też okazję wystąpić w sparingach ze {team:Stomilem} i {team:Olimpią Grudziądz}. - Moje przyjście tutaj jest sportowym awansem. Niewielu wróżyło mi, po rozstaniu się z własnej inicjatywy z {team:Wigrami}, tak dobrego klubu. Ciężką pracą na testach pokazałem jednak, że potrafię grać w piłkę i dostałem kontrakt. Jest to klub z tradycjami i nie ma co porównywać go do beniaminka z Suwałk - tłumaczy wychowanek {team:Stomilu}.

W żółto-niebieskich barwach na pozycji napastnika {player:Karłowicz} będzie rywalizował o miejsce w składzie z doświadczonymi {player:Pawłem Abbottem} i {player:Marcusem da Silvą}, a także z młodymi {player:Michałem Szubertem} (23lata) oraz {player:Maciejem Wardzińskim} (20 lat). - Przychodząc do {team:Arki}, widziałem, jak dużo jest piłkarzy występujących na mojej pozycji, ale nie boję się rywalizacji. Mogę trenować i walczyć o miejsce w jedenastce nawet z najlepszymi. W klubie jesteśmy kolegami, ale na zajęciach każdy ze wszystkich sił walczy o swoje - opowiada {player:Piotr Karłowicz}.

Przypomnijmy, że to nie jedyny olsztyński akcent w {team:Arce Gdynia}. W pierwszoligowej ekipie występują także {player:Grzegorz Lech}, {player:Marcin Warcholak} czy {player:Michał Renusz}. - Łatwiej było wejść do szatni, w której są znajome twarze. Z {player:Grześkiem Lechem} znam się dość długo. Jak wchodziłem do {team:Stomilu}, mając 15-16 lat, to był on taką małą, żywą legendą. {player:Michał} też miał swój epizod w drużynie, ale znamy się krócej - mówi {player:Karłowicz}.

Nowy nabytek gdynian cały czas śledzi też losy olsztyńskiego klubu. I nie ukrywa, że jest zaskoczony kłopotami finansowymi {team:OKS}-u. - Trudno się nie interesować klubem, który ma się w sercu. Jest mi żal chłopaków, którzy trenują w trudnych warunkach i oddają serce tej drużynie - mówi napastnik {team:Arki}. - Zrobili fantastyczne piąte miejsce w lidze, a tu nagle nie mają sponsora [9 stycznia ze współpracy wycofała się Galeria Warmińska - red.] i nie starcza im pieniędzy na treningi. Taka sytuacja jest nie do pomyślenia w tych czasach. Myślałem, że w końcu ten klub będzie funkcjonował normalnie.

I dodaje: - Sam pamiętam takie przypadki, że jak nie wiadomo o co chodzi, to chodzi o pieniądze. Dlatego życzę chłopakom przede wszystkim zdrowia i wierzę, że wszystko się poukłada.

Więcej o piłkarzach Stomilu czytaj na olsztyn.sport.pl.






