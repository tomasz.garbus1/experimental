::Najlepszy strzelec Serie A trafi do Napoli? Piłka nożna - Sport.pl::
@2019-05-21 08:38
url: http://www.sport.pl/pilka/7,65083,24808068,najlepszy-strzelec-serie-a-trafi-do-napoli.html

Do końca rozgrywek Serie A pozostała kolejka, ale już wiadomo, że nikt nie przebije osiągnięć Fabio Quagliarelli. Napastnik Sampdorii Genua zdobył aż 26 bramek i zaliczył 8 asyst. I choć ma już 36-lat, to wydaje się, że spokojnie może pograć na wysokim poziomie przez kolejny sezon, a może nawet dwa. Dlaczego? Quagliarella nigdy nie był zawodnikiem, którego gra opierała się na szybkości i przygotowaniu fizycznym. Włoch raczej imponował dobrą techniką, kapitalnym strzałem i mądrymi decyzjami, które podejmuje na boisku. Niedawno został nawet przywrócony - po niemal dziesięciu latach przerwy - do reprezentacji i na dzień dobry odpłacił się golami.
REKLAMA






Quagliarela w trakcie kariery zwiedził wiele klubów. Oprócz Sampdorii występował w Torino, Juventusie, Udinese, Ascoli i Napoli. Dla tego ostatniego klubu rozegrał zaledwie 37 meczów (strzelił 11 goli i zanotował 6 asyst w sezonie 2009/10). Na więcej nie pozwoliły mu problemy osobiste. W tamtym okresie zadawał się z policjantem Raffaele Piccolo, który prywatnie udawał przyjaciela piłkarza, a tak naprawdę miał problemy psychiczne i próbował wrobić Quagliarellę w pedofilię i inny przestępstwa. Dopiero po kilku latach afera wyszła na jaw, ale śledztwo w sprawie Piccolo wciąż się nie zakończyło.- Musiałem zdjąć z siebie ten ciężar i z tego powodu opuściłem Neapol. Sytuacja przemieniła moje życie w koszmar. Nie życzę tego nikomu. Ja i moja rodzina nie mogliśmy wychodzić z domu. Trudno to opisać – opowiadał po latach napastnik.Kibice Napoli zareagowali błyskawicznie na jego słowa. Gdy Quagliarella wróciło do Neapolu jako piłkarz Torino, to miejscowi kibice wywiesili transparent z napisem: „W piekle, w którym tyle lat żyłeś, zachowałeś godność. Jesteś synem tego miasta, Fabio.". Dlatego pewnie dzisiaj wielu z nich będzie kibicowało temu transferowi. Ciekawe jak potencjalny transfer Włocha wpłynąłby na rolę Arkadiusza Milika, który również występuje na pozycji nr. "9". Potencjalne przyjście 36-latka może sprawić, że Polak będzie grał mniej, niż w obecnym sezonie.





