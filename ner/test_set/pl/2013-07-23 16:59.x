::Kolejny transfer Śląska. Obrońca z Izraela Piłka nożna - Sport.pl::
@2013-07-23 16:59
url: http://www.sport.pl/pilka/1,65042,14325316,Kolejny_transfer_Slaska__Obronca_z_Izraela.html


Gavish będzie wzmocnieniem Śląska? Czekamy na FB

Oded Gavish ma 24 lata i występuje na pozycji środkowego obrońcy. Pochodzi z Izraela, ale ma również paszport rumuński. Przez ostatnie trzy lata bronił barw Hapoelu Beer Szewa, w którym pełnił funkcję kapitana. Na koncie ma także występy w młodzieżowych reprezentacjach Izraela.

- Przez ostatnie dni zawodnik przechodził we Wrocławiu testy medyczne. Ich wyniki są pozytywne, dlatego podpisaliśmy z Odedem trzyletnią umowę. Cieszymy się, że tak dobry i perspektywiczny piłkarz zasilił naszą drużynę. Jestem przekonany, że będzie dla Śląska dużym wzmocnieniem - mówi prezes wrocławskiego klubu Piotr Waśniewski. Oded Gavish przechodzi do Śląska na zasadzie transferu definitywnego.

Piłkarzem Śląska w ciągu kilku dni zostanie też Flavio Paixao.

O Śląsku ćwierkamy też na Twitterze






