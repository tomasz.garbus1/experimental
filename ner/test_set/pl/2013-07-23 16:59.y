::Kolejny transfer {team:Śląska}. Obrońca z {country:Izraela} Piłka nożna - Sport.pl::
@2013-07-23 16:59


{player:Gavish} będzie wzmocnieniem {team:Śląska}? Czekamy na FB

{player:Oded Gavish} ma 24 lata i występuje na pozycji środkowego obrońcy. Pochodzi z {country:Izraela}, ale ma również paszport rumuński. Przez ostatnie trzy lata bronił barw {team:Hapoelu Beer Szewa}, w którym pełnił funkcję kapitana. Na koncie ma także występy w młodzieżowych reprezentacjach {country:Izraela}.

- Przez ostatnie dni zawodnik przechodził we Wrocławiu testy medyczne. Ich wyniki są pozytywne, dlatego podpisaliśmy z {player:Odedem} trzyletnią umowę. Cieszymy się, że tak dobry i perspektywiczny piłkarz zasilił naszą drużynę. Jestem przekonany, że będzie dla Śląska dużym wzmocnieniem - mówi prezes wrocławskiego klubu {manager:Piotr Waśniewski}. {player:Oded Gavish} przechodzi do Śląska na zasadzie transferu definitywnego.

Piłkarzem {team:Śląska} w ciągu kilku dni zostanie też {player:Flavio Paixao}.

O {team:Śląsku} ćwierkamy też na Twitterze






