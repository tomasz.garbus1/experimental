::W Serie A lepiej nie jest, ale ciekawiej Piłka nożna - Sport.pl::
@2014-08-29 19:09
url: http://www.sport.pl/pilka/1,65083,16554280,W_Serie_A_lepiej_nie_jest__ale_ciekawiej.html




Dla Juventusu czy Fiorentiny największym sukcesem w tym oknie transferowym jest zatrzymanie swoich gwiazd, Arturo Vidala i Juana Cuadrado. Ich odejście jest teraz mało prawdopodobne. AS Roma takiego szczęścia nie miała i oddała Bayernowi Monachium Mehdiego Benatię. Marokańczyk był kluczową postacią zespołu, który niespodziewanie wywalczył wicemistrzostwo Włoch. Ale Rzymianie, tak jak i Juventus, paradoksalnie zyskają na odpadnięciu Napoli. Dzięki temu oba kluby zarobią odpowiednio 10.7 i 8.7 milionów euro więcej za udział w Lidze Mistrzów (pieniądze z praw telewizyjnych). Choć z drugiej strony, w europejskich pucharach nadal grają wszystkie włoskie drużyny - to jedyne takie państwo w Europie.

Nisko, coraz niżej


W ostatnich pięciu latach przez eliminacje LM przebrnął tylko AC Milan. Udinese odpadało z Bragą i Arsenalem, natomiast Sampdoria z Werderem Bremą. To sytuacja tym bardziej bolesna, że od dwóch sezonów Włosi wystawiają nie cztery, a tylko trzy drużyny. Od sezonu 2010/11 przedstawiciele Serie A zagrali w zaledwie trzech ćwierćfinałach - Inter, Milan i Juventus odpadały znacznie ulegając swoim rywalom. To wszystko odbija się na rankingu krajowym UEFA (na jego podstawie rozdzielana jest liczba miejsc w pucharach). Serie A nie jest już czwartą najlepszą ligą. To miano przejęła portugalska Liga Sagres. Groźby dalszego spadku na razie nie ma, ale upadek jest ogromny. W 1999 roku Włosi byli na szczycie rankingu, wyprzedzali Hiszpanię. W 2006 roku byli na drugim miejscu, wyprzedzali angielską Premier League. Jeszcze w 2010 roku odepchnęli atak Niemców na trzecią pozycję, ale rok później już skapitulowali. Tendencja spadkowa jest oczywista.

W czołowej dwudziestce klubów jest tylko AC Milan. Ten sam, który w poprzednim sezonie zajął ósme miejsce i pogrążony jest w wiecznym chaosie sportowo-organizacyjnym. Włoski potentat, czyli Juventus, w poprzednim sezonie w pucharach zawiódł na całej linii. Z Ligi Mistrzów odpadł w fazie grupowej, w Lidze Europy nie potrafił dojść do finału rozgrywanego na własnym stadionie. Po kilku latach na salony powraca Roma, ale nie dość, że straciła Benatię, to jeszcze trafiła do jednej grupy z Bayernem Monachium i Manchesterem City. Losowana była z tego samego koszyka co np. szwedzkie Malmoe czy białoruskie BATE Borysów.

Kibice tracą zaufanie


To wszystko widzą kibice. "Corriere della Sera" donosi, że w Serie A przed tym sezonem sprzedano 50 tysięcy karnetów mniej niż rok temu. Przestarzałe stadiony zapełniają się w zaledwie 56,6 proc. W Niemczech to 96 proc., Anglii 95 proc., Francji 70 proc., a w Hiszpanii 68 proc. A ceny stale idą w górę, jakby na przekór - w Juventusie o 6,9 proc., w Fiorentinie 6 proc., w Milanie 5,5 proc., w Romie o 4,5 proc.

Milan sprzedał 16 tys. karnetów (ostatnio 23,5 tys.). Kibice na oficjalnym profilu na Facebooku pytają, czy mogą wypożyczyć karnet z opcją wykupu, żartując w ten sposób z polityki transferowej klubu. Polega ona na pozyskiwaniu jak najtańszych piłkarzy - Alex czy Jeremy Menez - lub wypożyczeniach. Przez to transferami rządzi przypadek, Milan kupuje piłkarzy bez głębszego pomyślunku, bierze tego, kto akurat jest dostępny. Najlepiej ilustruje to sytuacja z bramkarzami. Za darmo pozyskano Michaela Agazziego z Cagliari; miał być zmiennikiem Christiana Abbiatiego. Ale w sierpniu przyszedł niechciany w Realu Madryt Diego Lopeza. Efekt? Milan ma czterech bramkarzy, bo w klubie jest jeszcze młody Gabriel, który nie dostaje szans na grę. A prawego skrzydłowego, którego najbardziej chce Filippo Inzaghi, nadal nie ma. Adriano Galliani obecnie koncentruje się na zastąpieniu sprzedanego do Liverpoolu Mario Balotellego. "Rossoneri" chcą pozyskać Fernando Torresa.

W Neapolu i Turynie bez większych zmian


W Napoli sprzedaż biletów sezonowych spadła z 15 tys. do 6. Przed tym sezonem właściciel i prezydent Aurelio De Laurentiis nie zdecydował się na takie zakupy, jak w poprzednim sezonie (wydał ponad 110 mln euro na wzmocnienia, m.in. Gonzalo Higuaina, Driesa Mertensa czy Jose Callejona). Czekał aż Napoli wejdzie do Ligi Mistrzów? Zmiany były kosmetyczne, sprzedanego do Swansea Federico Fernandeza zastąpił Kalidou Koulibaly, a za Valona Behramiego przybył Jonathan de Guzman. Podopieczni Rafaela Beniteza w dwumeczu z Athletikiem zagrali jedną dobrą połowę. W dwumeczu przegrali "tylko" 2-4, gdyż wynik mógł być o wiele gorszy. Przy dwóch golach straconych w rewanżu fatalnie zachowała się obrona, a jedynym pomysłem na atak była długa piłka do przodu. Najbardziej zawodzi Marek Hamsik, ulubieniec tamtejszych fanów, który w niczym nie przypomina zawodnika, jakim był jeszcze dwa, trzy lata temu. W tej chwili zespół, który w poprzednim sezonie zdobył 12 punktów w grupie z Arsenalem, Borussią Dortmund i Olympique Marsylią, nie istnieje.

W Juventusie wyprzedano 28 tysięcy karnetów, nawet pomimo zamieszania związanego z odejściem Antonio Conte na samym początku okresu przygotowawczego. Zastąpił go nielubiany przez kibiców Massimiliano Allegri, lecz wydaje się, że dadzą mu jednak kredyt zaufania. Jego metody treningowe i elastyczność chwali chociażby Claudio Marchisio. "Stara Dama" nie będzie grała wyłącznie w ustawieniu 3-5-2, możliwa jest również formacja 4-3-3. Na lewej obronie miejsce zajmie sprowadzony z Manchesteru United Patrice Evra; poza tym do klubu przyszli Alvaro Morata (obecnie kontuzjowany), oraz wypożyczono Romulo (z Hellas Werony) i Roberto Pereyrę (Udinese). Ponowne zdobycie 102 punktów w lidze wydaje się niemożliwe, ale czwarte Scudetto z rzędu i awans do ćwierćfinału Ligi Mistrzów to główne cele Juventusu na ten sezon.

W Rzymie niespokojnie


Wzrost w sprzedaży biletów sezonowych zanotowała Roma (z 23 tys. do 26).To pokłosie świetnego poprzedniego sezonu i powrotu do Ligi Mistrzów. Teraz Rzymianie mogą się pokusić nawet o zdobycie mistrzostwa, jeśli wykorzystają zamieszanie w Juventusie. Tak już zrobili, gdy pozyskali Juana Iturbe. W swoich szeregach widziała go również "Stara Dama". 21-letni Argentyńczyk z Hellas Werony to jak na razie najdroższy transfer w tym oknie transferowym. Według niektórych doniesień, razem z bonusami może kosztować Romę aż 31 milionów euro! To jasny wyraz ambicji wicemistrza, w barwach którego występować będzie również Ashley Cole czy wypożyczony z Cagliari Davide Astori.

Po drugiej stronie Rzymu sytuacja tak różowa już nie jest. Tylko 7 tysięcy fanów Lazio zdecydowało się na wykupienie karnetu (ostatnio 23 tys.). Kibice od dawna są skonfliktowani z władzami. Dyrektorowi Igliemu Tare "fani" grozili śmiercią. Jego numer telefonu wyciekł do internetu po tym, jak Lazio miało pozyskać Astoriego (w ostatniej chwili pozyskała go Roma, czyli najwięksi i znienawidzeni rywale). Prezydent Claudio Lotito pisał do kibiców w liście otwartym, że przez lata był ich zakładnikiem, a oni działali wbrew interesowi klubu, ale jednocześnie prosi ich o to, by "nie opuszczali Lazio". Lotito pogróżki otrzymywał już w lutym, tuż po sprzedaży Hernanesa do Interu. Fanów przyciągnąć na stadion mogą jedynie wyniki. Nowy trener Stefano Pioli dostał do dyspozycji kilku ciekawych piłkarzy. Przede wszystkim Marco Parolo, Dusana Bastę czy brązowego medalistę mistrzostw świata, Holendra Stefana de Vrija.

Inter i Fiorentina marzą o podium


Liderem w sprzedaży karnetów jest prawdopodobnie Inter (oficjalnych danych nie klub nie podał, ale mówi się o 31 tysiącach). W Mediolanie nie ma już ani jednego zawodnika, który pamiętałby triumf w Lidze Mistrzów z 2010 roku. Na emeryturę przeszedł Javier Zanetti, do Argentyny wrócił Diego Milito, a Esteban Cambiasso wzmocnił Leicester City. Inter jednak w rozsądny sposób uzupełnił te luki; Nemanja Vidić już w sparingach wyrósł na przywódcę linii defensywnej a Gary "Pitbull" Medel ma za sobą udane mistrzostwa świata. Oprócz nich wypożyczono Dodo (AS Roma), Pablo Osvaldo (Southampton) oraz Yanna M'Vilę (Rubin Kazań). A wielkie nadzieje wiąże się z możliwym przełomem Mateo Kovacicia, 20-letniego Chorwata. Walter Mazzarri ma drużynę, która przy pomyślnych wiatrach może pokusić się o miejsce na podium.

O tym samym marzy Fiorentina. Ale tamtejsi fani wykupili trzy tysiące biletów sezonowych mniej niż w poprzednim sezonie (20 tys.). We Włoszech mówi się o "efekcie Cuadrado". Kolumbijczyka przez całe lato łączono a to z FC Barceloną, a to z Manchesterem United czy Bayernem Monachium. Wydaje się jednak, że zostanie we Florencji, co byłoby najważniejszym transferem Fiorentiny w tym oknie. Skład praktycznie się nie zmienił. Wypożyczono Marko Marina, o miejsce w bramce rywalizować będzie z Neto Ciprian Tatarusanu, a obronę wzmocnił Jose Maria Basanta. Trener Vincenzo Montella musi liczyć przede wszystkim na to, że tym razem jego dwie strzelby - Giuseppe Rossi i Mario Gomez - będą zdrowe przez większą część sezonu. Choć Rossi już doznał urazu i może nie zagrać w pierwszym meczu z AS Romą...

Powraca "Zemanlandia"


Ciekawe zmiany zaszły w Torino, gdzie kapitanem jest Kamil Glik. Odszedł król strzelców, Ciro Immobile (do Borussii Dortmund) i nieznana jest przyszłość reprezentanta Włoch Alessiego Cerciego. Ale Turyńczycy pozyskali Fabio Quagliarellę z Juventusu i wypożyczyli Omara El Kaddouriego z Napoli, Antonio Nocerino z Milanu czy Rubena Pereza z Atletico Madryt. Warto zwrócić uwagę na Cagliari, gdzie trenerem został znany z zamiłowania do ultraofensywnego futbolu Zdenek Zeman. Te mecze na pewno nie będą nudne i powinny obfitować w ogromną liczbę bramek. Jednocześnie to ostatni sezon Antonio Di Natale. Miał zakończyć karierę po poprzednim sezonie (hat trick w meczu, który miał być jego pożegnaniem!), ale tego nie zrobił. I w Pucharze Włoch z Ternaną zdobył cztery bramki. 36-latek w ostatnich sezonach zdobywał odpowiednio 17, 23, 23, 28 i 29 bramek, dwukrotnie zostając królem strzelców.

Niestety, spora część klubów nie wykorzystuje potencjału marketingowego. Aż siedem drużyn nie ma reklamy sponsora na koszulkach - są to Cesena, Fiorentina, Genoa, Lazio (od ośmiu lat, reklam nie chce prezydent Claudio Lotito pomimo paru ofert i nie najlepszej kondycji finansowej), Palermo, Roma i Sampdoria. W Premier League, Bundeslidze i Ligue 1 nie ma ani jednego takiego zespołu. W Hiszpanii reklam nie mają jedynie Levante i Valencia.

Niewiele wskazuje na to, by Serie A miała w najbliższym czasie powrócić do dawnych, lepszych czasów. Ale rozgrywki powinny być ciekawsze niż ostatnio - mistrzostwo Juventusu nie jest już rzeczą oczywistą, zagrażać Turyńczykom mają AS Roma oraz Napoli. A miejsce na podium chcą znaleźć również takie kluby jak Inter, Fiorentina, czy nawet AC Milan. A może ktoś pokusi się o wygranie Ligi Europy i w ten sposób awansuje do Ligi Mistrzów? Nudy nie uświadczymy, nawet jeśli włoski futbol nie potrafi znaleźć drogi powrotnej na szczyt.

Miniprzewodnik po występach polskich piłkarzy za granicą






