::Afera korupcyjna. Procesu {team:Korony} ciąg dalszy Piłka nożna - Sport.pl::
@2009-11-10 07:00


Prokuratura twierdzi, że w trzecioligowym sezonie 2003/04 prowadzony przez Dariusza W. kielecki zespół ustawił 22 spotkania {team:Korony} i najgroźniejszych rywali. Akt oskarżenia w tej sprawie obejmuje 43 osoby - trenerów, piłkarzy, działaczy, sędziów, obserwatorów. Przed sądem stanie tylko 15. Pozostali dobrowolnie poddali się karze, czyli przyznali się do winy i uzgodnili wyroki z prokuratorem. Ich sprawy w większości się już zakończyły. Karze poddało się m.in. kilkunastu piłkarzy, a także trenerzy Dariusz W. i Andrzej W. (byli reprezentanci {country:Polski}), którzy dostali wyroki w zawieszeniu, wysokie grzywny i kilkuletnie zakazy zajmowania stanowisk w zawodowym sporcie.

Początkowo korupcją z polecenia Dariusza W. zajmował się kierownik drużyny Paweł W., ale gdy po rundzie jesiennej Korona zajmowała 2. miejsce, za ustawianie meczów wzięli się sami trenerzy. W Warszawie spotkali się z Jerzym E. juniorem, synem byłego trenera reprezentacji {country:Polski}, który przyprowadził ze sobą Andrzeja B., byłego drugiego trenera {team:Wisły Kraków} rekomendowanego jako "specjalista od awansów". I poszło. Na łapówki zrzucali się piłkarze i trenerzy, pieniądze trafiały do B., a ten, wg prokuratury, opłacał sędziów, obserwatorów i rywali. Zwykle było to od 2 do 5 tys. zł, w sumie na korupcję przeznaczono co najmniej 44 tys. zł. Andrzej B. i Jerzy E. junior do winy się nie przyznają. B. będzie tu odpowiadał "tylko" za pomoc w kupieniu 15 meczów kielczan, ale czekają go kolejne procesy. Według śledczych ustawił jeszcze 89 spotkań, m.in. {team:Unii Janikowo} i {team:Znicza Pruszków}. Miał przyjąć ponad 300 tys. zł łapówek, wręczyć prawie 400 tys. B. jest jedynym w sprawie {team:Korony}, który siedzi w areszcie.

Zarzuty dla dwóch sędziów w aferze korupcyjnej »






