::{player:Edinson Cavani} w {team:PSG}. Kogo rozboli brzuch Piłka nożna - Sport.pl::
@2013-07-16 21:48


Startuje nowy sezon Wygraj Ligę! Kupisz {player:Saganowskiego} czy {player:Ślusarskiego}? »

- Chcemy zbudować jeden z najlepszych klubów w Europie, inwestycje są niezbędne - tłumaczył kilka miesięcy temu prezes mistrzów {country:Francji} {manager:Nasser Al-Khelaifi}. We wtorek sprawił sobie napastnika, który przez trzy sezony strzelił dla {team:Napoli} 104 gole i był jednym z najbardziej rozrywanych piłkarzy lata - starały się o niego {team:Chelsea}, {team:Manchester City} i {team:Real Madryt}. Wczoraj 26-letni Urugwajczyk przeszedł testy medyczne i podpisał pięcioletni kontrakt. Będzie zarabiał 10 mln euro rocznie.

Transfer {player:Cavaniego} jest piątym w historii i największym w lidze francuskiej. Poprzednie rekordy biło w maju {team:Monaco} napędzane przez majątek Dmitrija Rybołowlewa. Beniaminek {league:Ligue 1} sprowadził {player:Jamesa Rodrigueza} (45 mln) i {player:Radamela Falcao} (60 mln).

Katarczycy od przejęcia {team:PSG} w 2011 r. wydali już na transfery 320 mln euro, a to nie koniec, starają się także o wycenianego na 35 mln euro stopera {team:Romy} {player:Marquinhosa}. - Kiedy usłyszałem, że {player:Zlatan Ibrahimović} zarabia w Paryżu 14 mln euro, rozbolał mnie brzuch. W rok dostanie tyle, ile nasz {player:Javi Martinez} w cztery lata - narzekał kilka miesięcy temu prezes {team:Bayernu} {manager:Karl-Heinz Rummenigge}.

{manager:Rummenigge} to zwolennik finansowego fair play - pomysłu szefa UEFA Michela Platiniego, który miał odmienić europejski futbol. Kilka lat temu Francuz uznał, że trzeba przeciwdziałać "finansowemu dopingowi", czyli utrzymywaniu piłkarskich firm przez miliarderów ze skarbcami bez dna. Według FFP kluby, które występują w europejskich pucharach, nie mogą wydawać więcej, niż zarobią. UEFA skupia się na pieniądzach wydanych na transfery i pensje, na zarobkach z biletów, sprzedaży praw marketingowych i telewizyjnych, na umowach sponsorskich i działalności komercyjnej. Nie interesują jej koszty budowy stadionów, centrów treningowych i szkolenia młodzieży. W ostatnich sezonach UEFA tylko przyglądała się rachunkom klubów, za dwa lata ma już nie dopuszczać do pucharów tych, którzy w sezonach 2013/14 i 2014/15 przyniosą więcej niż 45 mln euro strat.

Nie ma wątpliwości, że {team:PSG}, które dostaje za prawa do transmisji telewizyjnych mniej niż najlepsze kluby angielskie, niemieckie, hiszpańskie i włoskie, miałoby gigantyczny problem, by zmieścić się w widełkach UEFA. Dlatego paryżanie szukają luk w systemie. W grudniu podpisali czteroletnią umowę z Qatar Tourism Authority wartą 700 mln euro. Sponsor nie zapłacił za miejsce na koszulkach, nie wykupił prawa do nazwy stadionu Parc des Princes. Życzy sobie tylko, żeby klub promował {country:Katar}. Qatar Tourism Authority podobnie jak właściciel {team:PSG} Qatar Investment Authority jest zarządzane przez katarski rząd.

Teoretycznie UEFA zabezpieczyła się przed takimi przypadkami - specjalna komisja ma oceniać, ile tak naprawdę powinny kosztować umowy sponsorskie. - {team:PSG} zna zasady i musi je respektować. Nie ma mowy o oszustwach - zapowiadał kilka miesięcy temu sekretarz generalny UEFA Gianni Infantino. Nie wiadomo tylko, w jaki sposób komisja udowodni QTA, że promocję kraju można zorganizować za mniejsze pieniądze.

Największe transfery






1. Cristiano RonaldoManchester UnitedReal942009


2. Zinedine ZidaneJuventusReal752001


3. Zlatan Ibrahimović*InterBarcelona692009


4. KakáMilanReal682009


5. Edinson CavaniNapoliPSG642013


6. Luis FigoBarcelonaReal622000


7. HulkPortoZenit602012


8. Radamel FalcaoAtléticoMonaco602013


9. Fernando TorresLiverpoolChelsea582011


10. NeymarSantosBarcelona572013


Kolejno: nazwisko, z, do, suma w milionach euro, rok transferu

*{team:Barcelona} zapłaciła za Szweda 49 mln euro oraz wysłała do Mediolanu {player:Samuela Eto'o}, którego wyceniała na 20 mln euro.






