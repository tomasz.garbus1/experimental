::{team:Real Madryt} ma problem ze swoją gwiazdą. "{manager:Zidane} nie jest sprawiedliwy" Piłka nożna - Sport.pl::
@2019-08-26 19:43

Stracony sezon Realu. "{manager:Zidane} nie ma czarodziejskiej różdżki"
REKLAMA






{team:Real Madryt} niespodziewanie zremisował u siebie w meczu 2. kolejki hiszpańskiej {league:La Liga} z {team:Realem Valladolid}. Gdy w 82. minucie bramkę strzelił {player:Karim Benzema}, wydawało się, że "{team:Królewscy}" zdobędą komplet punktów. Sześć minut później wyrównał jednak {player:Sergio Guardiola}, a zespół Zinedine&aposa {manager:Zidane}&aposa nie zdołał już odpowiedzieć i obydwa zespoły podzieliły się punktami.W 57. minucie na boisko wszedł {player:Vinicius Junior}. Piłkarz, który miał stać się gwiazdą {team:Realu Madryt} w nadchodzących rozgrywkach. W meczu przeciwko {team:Realowi Valladolid} został przesunięty z lewej strony na prawą, co znacznie wpłynęło na jego grę. "Marca" zwraca uwagę, że w tym sezonie prezentuje się znacznie gorzej niż w połowie poprzednich rozgrywek. Jednym z powodów takiego stanu rzeczy jest transfer {player:Edena Hazarda}, który zmusił Brazylijczyka do zmiany pozycji. {player:Vinicius} dodatkowo był zaskoczony tym, że nie wyszedł w podstawowym składzie.{team:Real Madryt} wiąże wielkie nadzieje z {player:Viniciusem}, ale ten ma problemyDziennikarze "Marki" podkreślają, że w Brazylijczyku pokładano ogromne nadzieje, jednak {manager:Zidane} nie dał mu wystarczająco dużo czasu do zyskania pewności siebie. Wszyscy zachwycali się jego umiejętnościami, dopóki nie doznał kontuzji w spotkaniu przeciwko {team:Ajaksowi Amsterdam} w {league:Lidze Mistrzów}. Był jednym z nielicznych piłkarzy, który mógł zagrozić {team:Barcelonie} w "El Clasico" na początku tego roku, po którym otrzymał pierwsze powołanie do reprezentacji Brazylii. Do tych czasów mu jednak daleko. "Marca" dodaje, że "{manager:Zidane} nie jest do końca sprawiedliwy wobec {player:Viniciusa}", a piłkarz czuje, że każdy jego błąd lub słabszy występ sprawi, że jego sytuacja pogorszy się jeszcze bardziej.





