::LE. Braga szpieguje Lecha w Hiszpanii Piłka nożna - Sport.pl::
@2011-01-29 09:56
url: http://www.sport.pl/pilka/1,65042,9023256,LE__Braga_szpieguje_Lecha_w_Hiszpanii.html


Zarówno wygrany w środę 4:0 sparingowy mecz Lecha z Lokomtiwem Płowdiw, jak i czwartkowy trening miał pilnych obserwatorów. Na wzgórzu obok boiska stał zaparkowany w krzakach samochód z portugalskimi tablicami rejestracyjnymi. Wewnątrz dwóch mężczyzn uważnie przyglądało się zajęciom, a jeden z nich kamerą rejestrował, to co działo się na treningu.

Obserwatorzy zagadnięci przez przedstawiciela poznańskiego klubu, odpowiedzieli że są dziennikarzami. Nie potrafili jednak wytłumaczyć, dlaczego filmują z ukrycia. Lechici nie mają wątpliwości, że to wysłannicy Bragi. Jest to o tyle dziwne, że przedstawiciele Lecha oglądali mecze Portugalczyków oficjalnie na stadionie.

Wszystko o Lechu Poznań w specjalnym serwisie Sport.pl »






