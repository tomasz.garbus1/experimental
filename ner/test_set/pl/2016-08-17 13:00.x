::Piłkarze Śląska, Zagłębia i Miedzi powołani do reprezentacji Polski U-20 Piłka nożna - Sport.pl::
@2016-08-17 13:00
url: http://www.sport.pl/pilka/1,65044,20558552,pilkarze-slaska-zaglebia-i-miedzi-powolani-do-reprezentacji.html


Dankowski w sezonie 2016/17 zaliczył dotychczas tylko dwa występy w barwach Śląska. Łącznie na boisku spędził 47 minut. Ostatni pojedynek wrocławian z Arką Gdynia rozpoczął w wyjściowym składzie. Zagrał słabo, sprokurował rzut karny i grę zakończył po 45 minutach.

Bez występów w nowych rozgrywkach jest jeszcze Filip Jagiełło z Zagłębia Lubin. Najwięcej spotkań w nowym sezonie zanotował Rasak. Pomocnik Miedzi Legnica zagrał już w czterech meczach, trzykrotnie wychodząc jako podstawowy zawodnik drużyny prowadzonej przez Ryszarda Tarasiewicza.

Kadra trenera Macieja Stolarczyka 1 września w Montreux zmierzy się z rówieśnikami z reprezentacji Szwajcarii. Z kolei 6 września biało-czerwoni zagrają w Świnoujściu z drużyną Niemiec. Zgrupowanie polskiego zespołu potrwa od 28 sierpnia do 7 września.

Kadra reprezentacji Polski U-20 na Turniej Czterech Narodów


Jakub Bartosz (Wisła Kraków), Jan Bednarek (Lech Poznań), Kamil Dankowski (Śląsk Wrocław), Filip Jagiełło (Zagłębie Lubin), Robert Janicki (Hoffenheim), Mateusz Kuchta (Górnik Zabrze), Rafał Makowski (Pogoń Siedlce), Konrad Michalak, Mateusz Wieteska (obaj Legia Warszawa), Jakub Piotrowski (Pogoń Szczecin), Adrian Purzycki (Wigan Athletic), Damian Rasak (Miedź Legnica), Oktawian Skrzecz (Schalke 04), Paweł Stolarski (Lechia Gdańsk), Karol Świderski, Damian Węglarz (obaj Jagiellonia Białystok), Szymon Walczak (1. FC Köln), Mateusz Wdowiak (Cracovia), Bartosz Wolski (Latina Calcio), Oskar Zawada (VfL Wolfsburg)






