::Finisz {league:I ligi}. Które miejsce zajmie {team:Arka}? Piłka nożna - Sport.pl::
@2015-05-22 17:07


Dwa miesiące temu, kiedy {team:Arka} odprawiała z kwitkiem {team:GKS Tychy} (4:0) oraz {team:Stomil Olsztyn} (4:1) wydawało się, że 4. miejsce jest w zasięgu żółto-niebieskich. Po 21. kolejce gdynianie byli na 6. miejscu z dorobkiem 32 punktów. Do czwartego {team:Stomilu} tracili jeden punkt. Jednak w kolejnych 10 meczach podopieczni {manager:Grzegorza Nicińskiego} wygrali jedno spotkanie, sześć zremisowali i trzy przegrali. Ten bilans sprawił, że na trzy kolejki przed końcem sezonu {team:Arce} bliżej do środka tabeli niż do miejsca tuż za pierwszą trójką walczącą o awans do ekstraklasy. {team:Gdynianie} są na 9. miejscu ze stratą sześciu oczek do olsztynian (4. miejsce) i pięciu do {team:Olimpii Grudziądz} (5. miejsce) oraz {team:Chojniczanki Chojnice} (6. miejsce). Bardziej realny scenariusz zakłada walkę {team:Arki} o 7. lokatę ({team:Dolcan} ma 43 punkty) lub nawet o pozycje 8-10. Wprawdzie celem żółto-niebieskich jest budowa zespołu na kolejny sezon, ale miejsce w dolnych rejonach tabeli świadczyć będzie o średniej jakości drużyny {manager:Nicińskiego}.

"{manager:Nitek}" przejmował {team:Arkę} w trudnej sytuacji, wydobył ją z dołu tabeli i skończył jesień na 7. miejscu. Solidnie przepracował zimę, pouzupełniał luki, ale mimo to {team:Arka} wiosną ma duże wahania formy. Potrafi zagrać słabo - jak z {team:Wigrami Suwałki} (0:1) czy {team:Bytovią Bytów} (0:1), ale też jak równy z równym - a nawet momentami przeważa - rywalizuje z {team:Wisłą Płock} (0:0) czy w ostatnim meczu z {team:Zagłębiem Lubin} (0:1).

- Najbardziej jestem rozczarowany wynikiem i sędzią. Jestem zdenerwowany, bo karnego nie było. Nie wiem, czy mogę nazwać to pechem, po strzale da {player:Silvy} piłka wpadła do bramki, ale po drodze musnął ją jeszcze {player:Nalepa}, gol nie został uznany, a później {team:Zagłębie} dostaje karnego. Nie zasłużyliśmy na taki wynik. Mieliśmy swoje sytuacje. Po prostu zabrakło kropki "nad i". {team:Zagłębie} ma doświadczonych piłkarzy, można było odczuć, że gramy z zawodnikami z ekstraklasy. Widać, że grają o awans - mówił po meczu pomocnik {team:Arki} {player:Paweł Wojowski}.

{team:Arka} ma jeszcze szansę poprawić swoją pozycję. Choć czy tutaj tak naprawdę o miejsce chodzi, czy bardziej o kontynuowanie myśli szkoleniowej i rozwój zespołu? Na to pytanie wkrótce poznamy odpowiedź. Znane są za to inne fakty - w najbliższym spotkaniu {team:Arkę} czeka pojedynek z wciąż niepewnym swego {team:Chrobrym Głogów}. Potem gdynianie pożegnają się z własną publicznością meczem z {team:Olimpią Grudziądz}, a na koniec pojadą do Katowic.

Na {league:Chrobrego} wraca już do składu pauzujący za żółte kartki z {team:Zagłębiem} {player:Antoni Łukasiewicz}. Jest także {player:Grzegorz Tomasiewicz}, który przebywał na zgrupowaniu reprezentacji Polsku U-19. Zabraknie za to kontuzjowanych {player:Pawła Abbotta}, {player:Michała Rzuchowskiego} i {player:Michała Renusza}.






