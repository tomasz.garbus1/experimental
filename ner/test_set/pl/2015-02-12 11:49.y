::{league:Primera Division}. Na {stadium:Camp Nou} fiesta Piłka nożna - Sport.pl::
@2015-02-12 11:49


Dyskutuj z autorem na jego blogu

Były piłkarz {team:Barcelony} i były trener {team:Villarreal} {manager:Victor Munoz} uważa, że jeszcze nigdy drużyna z {stadium:Camp Nou} nie miała tak ogromnego potencjału w ataku. Stwierdzenie obarczone ogromnym stopniem ryzyka, przecież w ostatnich latach partnerami {player:Leo Messiego} byli {player:Ronaldinho}, {player:Thierry Henry}, {player:Samuel Eto'o}, {player:David Villa}, żeby nie wymienić {player:Zlatana Ibrahimovicia}, dla którego pobyt w Katalonii był klapą totalną. Rzecz jasna trio {player:Messi} - {player:Suarez} - {player:Neymar} daje {manager:Luisowi Enrique} wielkie możliwości, ale tak naprawdę wszystko, co powiemy dziś, może ulec natychmiastowej weryfikacji.

Zaledwie 4 stycznia, czyli nie tak dawno, {stadium:Camp Nou} trząsł się w posadach. Po porażce z {team:Realem Sociedad} prasa hiszpańska donosiła, że między {player:Messim} a {manager:Luisem Enrique} jest topór nie do zakopania. Trener miał wylatywać z pracy, czterokrotny laureat Złotej Piłki szukał swojej przyszłości w {league:Premier League}. Zanosiło się na rewolucję, która po sześciu tygodniach zmieniła się w idyllę. - Nie mam wątpliwości: jedna porażka i "fiesta" wróci - przestrzega sam siebie {manager:Luis Enrique}. "Fiesta", czyli histeria i poczucie, że drużyna zmierza donikąd.

Wczorajszy mecz z {team:Villarreal} mógł ostatecznie rozstrzygnąć sprawę awansu do finału {league:Copa del Rey}. Zwłaszcza gdyby dobrze grający {player:Ter Stegen} nie popełnił błędu przy wyrównującej bramce. Ale i w końcówce meczu instynkt zmylił wygrywającą dwoma golami {team:Barcelonę}. {player:Neymar} poprosił {player:Messiego}, by mógł strzelać karnego, {player:Leo} wspaniałomyślnie pozwolił i bohaterem wieczoru został bramkarz gości {player:Sergio Asenjo}. Jak widać, atmosfera pełnej zgody i zrozumienia nie musi wcale służyć drużynie. Zamiast wygrać 4:1, {team:Barca} zostawiła rywala z opcjami na rewanż. Tylko raz w historii rozgrywek po zwycięstwie 3:1 na {stadium:Camp Nou} {team:Katalończycy} odpadli z rozgrywek. Było to w 1960 roku przeciw {team:Athletic Bilbao}, które w rewanżu ograło {team:Barcę} 3:0. W pozostałych przypadkach ten wynik zawsze wystarczał.

{team:Katalończycy} wygrali 10. kolejny mecz, ale kibiców z {league:Camp Nou} w ekstazę to nie wprowadza - 57 tys. widzów to bardzo zły wynik, na liście najgorszych frekwencji w tym sezonie trzeci od końca. Tylko na meczach z {team:Huescą} i {team:Elche} było mniej widzów, wszystko w {league:Pucharze Króla}, co może w jakimś stopniu pokazuje, że fani z Katalonii nie są najbardziej spragnieni tego akurat trofeum.

Prasa w {team:Barcelonie} donosi, że {player:Messi} i {player:Neymar} zdobyli w tym roku więcej bramek niż cały {team:Real Madryt}. Takie słodkie statystyki do niczego nieprzydatne. Tak samo jak fakt, że {player:Gerard Pique} zalicza właśnie sezon z największą liczbą goli (5). W czasie trwania rozgrywek liczy się bramki, po nich trofea. Niecierpliwość dziennikarzy i kibiców sprawia, że grzebią się w danych mniej istotnych. Tak naprawdę liczyć się będzie to, czy {team:Barca} {manager:Luisa Enrique} znajdzie rozwiązanie swoich problemów na dłuższą metę. Na razie prognozy są obiecujące. Drużyna odzyskała radość z gry i sposoby na zaskakiwanie rywala. Średnia goli wzrosła znacząco - {team:Barca} nie musi już zazdrościć nikomu.

W jeszcze wyższym i bardziej uzasadnionym stanie euforii był tak niedawno {team:Real Madryt}, opromieniony czterema trofeami w 2014 roku i passą 22 kolejnych zwycięstw. Dziś na {stadium:Santiago Bernabeu} biją na alarm, a tamtejsi dziennikarze zapewniają, że pozycja {manager:Carlo Ancelottiego} nigdy nie była tak niepewna. Dlatego ma rację {manager:Luis Enrique}, nie chcąc wygłaszać triumfalnych sądów. Wie, że jego drużyna w końcu przegra, i co wtedy? Za chwilę {team:Barcelona} zmierzy się z {team:Manchesterem City} w 1/8 finału {league:Ligi Mistrzów}. Niby rywal to mistrz {country:Anglii}, ale na {stadium:Camp Nou} nikt nie dopuszcza do siebie czarnego scenariusza. Przecież z {team:City} poradziła sobie nawet drużyna "Taty" Martino, która zakończyła sezon na tarczy - bez trofeum. To była najgorsza {team:Barca} od 2008 roku. {manager:Luis Enrique} przybył na {stadium:Camp Nou}, by ją z tego kryzysu wyciągnąć.

Gdyby na skutek jakiegoś nieszczęśliwego zbiegu okoliczności nowemu trenerowi noga się jednak powinęła, tych 10 zwycięstw, które świętuje się dziś, pójdzie w głębokie zapomnienie. {manager:Luis Enrique} nie będzie mógł ich użyć nawet jako skromnego alibi. Dlatego musi zachować czujność. Serie zwycięstw niedające trofeów to w wielkich klubach fakt bez znaczenia?

Alaba i Weiser w stringach... oraz inni piłkarze w bieliźnie [ZDJĘCIA]






