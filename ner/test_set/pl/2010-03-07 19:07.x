::Liga grecka. Samobójcza bramka Marka Saganowskiego Piłka nożna - Sport.pl::
@2010-03-07 19:07
url: http://www.sport.pl/pilka/1,73556,7635702,Liga_grecka__Samobojcza_bramka_Marka_Saganowskiego.html


To goście pierwsi mogli objąć prowadzenie, ale po pół godzinie gry Tatos trafił w słupek.

W 82 min. rzut wolny wykonywał Geladaris. Piłkę do własnej bramki skierował Saganowski. Było 1:0 dla gospodarzy.

W pierwszej minucie doliczonego czasu gry z bliskiej odległości gola strzelił jednak Sfakianakis i uratował dla gości remis.

Cały mecz w zespole Atromitosu rozegrał także Marcin Baszczyński

Drużyna Polaków zajmuje obecnie siódme miejsce w ligowej tabeli z 32 punktami na koncie.

Gol Żurawskiego z karnego »





