::Były piłkarz klubów Ekstraklasy poszukiwany przez policję Piłka nożna - Sport.pl::
@2018-12-06 12:41
url: http://www.sport.pl/pilka/7,64946,24248892,byly-pilkarz-klubow-ekstraklasy-poszukiwany-przez-policje.html

"Grożenie innej osobie popełnieniem przestępstwa na jej szkodę lub szkodę osoby najbliższej, jeżeli groźba wzbudza w zagrożonym uzasadnioną obawę, że będzie spełniona (groźba karalna)", to treść artykułu, na podstawie którego poszukiwany jest Król, były piłkarz klubów Ekstraklasy, który w czasach juniorskich trenował w drużynie rezerw Realu Madryt (razem z Szymonem Matuszkiem i Kamilem Glikiem).
REKLAMA




Krzysztof Król poszukiwany przez policję poszukiwani.policja.plJako ostatnie miejsce zameldowania Króla, policja podaje Hiszpanię. W piłkę zakończył on jednak grać w Stanach Zjednoczonych. Jego ostatnim klubem była bowiem drużyna Chicago Eagles. 31-letni Król, który przestał grać w piłkę w styczniu ubiegłego roku, w Ekstraklasie rozegrał 96 spotkań. W amerykańskiej MLS, w barwach Montreal Impact i Chicago Fire, wystąpił natomiast w 31 spotkaniach.





