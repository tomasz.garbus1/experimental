::LM. Real z Bayernem! Atletico z Chelsea! Piłka nożna - Sport.pl::
@2014-04-11 12:36
url: http://www.sport.pl/pilka/1,65041,15781871,LM__Real_z_Bayernem__Atletico_z_Chelsea_.html


Real i Bayern to główni faworycie do wygrania LM w tym sezonie. Los sprawił, że zmierzą się ze sobą już w półfinale. Trener Bayernu, a były szkoleniowiec Barcelony Pep Guardiola będzie kolejny raz miał okazje zmierzyć z Królewskimi.

Duże emocje wzbudza sprawa Thibaut Courtoisa w rywalizacji Atletico z Chelsea. Bramkarz jest wypożyczony do hiszpańskiego klubu z The Blues i w umowie między klubami istnieje zapis o tym, że zawodnik nie może grać przeciwko The Blues, chyba że Hiszpanie zapłacą za taki występ 3 miliony euro. O sprawie zrobiło się głośno w czwartek, w piątek UEFA wydała oświadczenie, że piłkarz będzie mógł zagrać bez względu na ustalenia międzyklubowe.

W Madrycie zdecydowanie odetchnęli. Jeszcze w czwartek byli pogodzeni ze stratą swojego kluczowego zawodnika. - Jeśli trafimy na Chelsea, Courtois nie zagra. Nie stać nas na to - mówił prezes Atletico Enrique Cerezo.

- Wypożyczenie było zaaranżowane przed sezonem. Courtois będzie mógł jednak zagrać przeciwko nam. Nie ma co do tego wątpliwości - powiedział dyrektor generalny Chelsea Ron Gourlay

Pary 1/2 finału Ligi Mistrzów:

Real Madryt - Bayern Monachium

Atletico Madryt - Chelsea Londyn

Na pierwszym miejscu gospodarze pierwszych meczów. Odbędą się one 22-23 kwietnia, rewanże 29-30 kwietnia. Wielki finał 24 maja w Lizbonie.

W ćwierćfinałach Bayern Monachium okazał się lepszy od Manchesteru United (1:1, 3:1), Atletico od Barcelony (1:1, 1:0), Chelsea od PSG (1:3, 2:0), a Real od Borussii (3:0, 0:2).

Infografika: Przemysław Piotrowski






