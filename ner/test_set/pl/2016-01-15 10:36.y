::Od piątku w {league:Serie B} sędziowie pokażą zieloną kartkę! Piłka nożna - Sport.pl::
@2016-01-15 10:36


- Zielona kartka ma być nagrodą, uhonorowaniem właściwego postępowania na boisku. Ma walor edukacyjny - podkreśla Andrea Abodi, prezydent {league:Serie B}. Nagroda ta ma mieć znaczenie symboliczne, nie przyniesie piłkarzom żadnych dodatkowych korzyści. Władze ligi są przekonane, że nowe narzędzie w rękach arbitrów pozytywnie wpłynie na jakość gry, oglądalność, ale też postawę młodzieży.

Dla przykładu, sędzia będzie mógł pokazać kartkę, kiedy zawodnik zrezygnuje z korzyści dla swojej drużyny i przyzna, że nie był faulowany w polu karnym.

Rozwiązanie to było testowane we Włoszech na szczeblu młodzieżowym. Inauguracja w seniorskiej piłce już piątek o 20:30 podczas meczu {team:Spezia Calcio} - {team:AS Bari}.

Złota Piłka. Zachodny o 4. miejscu {player:Lewandowskiego}: Polak staje się globalną marką








