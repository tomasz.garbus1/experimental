::{league:Primera Division}. {manager:Ancelotti} krytykuje dalekie sparingi reprezentacji {country:Hiszpanii} Piłka nożna - Sport.pl::
@2013-11-22 16:43


{player:Iker Casillas}, {player:Alvaro Arbeloa} i {player:Sergio Ramos} z {team:Realu} w ostatnim tygodniu przemierzyli wiele tysięcy kilometrów, by rozegrać mecze towarzyskie. W zeszłym tygodniu grali sparing z {country:Gwineą Równikową}, we wtorek udali się do {country:RPA}, gdzie niespodziewanie przegrali z gospodarzami 0:1. Trener "{team:Królewskich}" obawia się o to, jak te podróże wpłyną na jego zawodników. Wprost mówi, że rozgrywanie takich meczów to błąd.

- Mamy ogromne szczęście, że do marca nie ma meczów towarzyskich. Ciężko o tym mówić, ale decyzja hiszpańskiej federacji jest zła - stwierdził na konferencji prasowej przed sobotnim meczem z {country:Almerią}.

- Piłkarze spali trzy noce w samolocie, a to dla nich nie jest dobre. Bramkarz {team:Barcelony} ({player:Valedes}) doznał kontuzji, nas to na szczęście ominęło, ale lepsza organizacja mogłaby pomóc w uniknięciu podobnych urazów - dodał {manager:Ancelotti}.

Po 13 kolejkach {league:Primera Division} {team:Real} z 31 punktami zajmuje trzecie miejsce. Przed "{team:Królewskimi}" są {team:Atletico} (34 pkt) oraz {team:Barcelona} (37 pkt).

Relacje z najważniejszych zawodów w aplikacji Sport.pl Live na iOS, na Androida i Windows Phone






