::Po meczu z {team:Legią} kibolska Barbórka {team:Zagłębia Lubin} Piłka nożna - Sport.pl::
@2009-12-06 21:21


16. kolejka {league:Ekstraklasy} w pigułce »

Podczas piątkowego meczu na stadionie odpalono zakazane na stadionach race, ochrona długo i nieskutecznie walczyła o zdjęcie obraźliwego transparentu oddanego kibolom gospodarzy przez przyjezdnych. Do Lubina przyjechała bowiem kilkusetosobowa grupa z Warszawy, która ominęła przepisy {league:Ekstraklasy}, nie kupując biletów w macierzystym klubie.

{team:Zagłębie} się broni, warszawscy kibole triumfują na swoich internetowych stronach. - Kibolom chodzi o to, aby pokazać, że to od nich zależy, kto wejdzie na stadion i jakie transparenty się pojawią na trybunach - mówi Jarosław Ostrowski z zarządu {team:Legii}.

- Była Barbórka. Nasi kibice to głównie górnicy. Dla nich było ważne, aby dzień uczcić. Oni uważali, że race są OK - mówi prezes {team:Zagłębia} Jerzy Koziński. - O ile można znaleźć usprawiedliwienie dla rac, o tyle wywieszanie obraźliwych szmat jest niedopuszczalne i za pomocą monitoringu znajdziemy tych, którzy to zrobili.

Jakim cudem na stadion weszła wbrew przepisom wielka grupa z Warszawy? Przecież uchwała {league:Ekstraklasy} mówi jasno: "Organizator zawodów może przyjąć kibiców drużyny gości tylko i wyłącznie w grupach zorganizowanych zaakceptowanych przez klub wysyłający, wedle ściśle określonych zasad".

- Była koncepcja, aby ich nie wpuścić. Gdyby przyjechali autobusami, można byłoby uznać, że są zorganizowani. Ale przyjechali osobowymi samochodami, zaparkowali na parkingu, stanęli w kolejce po bilety. Szef bezpieczeństwa klubu nakazał zarejestrowanie ich w naszej bazie. Każdemu zrobiono zdjęcie i spisano PESEL, zeskanowano dowód osobisty. W sumie było 361 osób, więc trochę to trwało. Dopiero po tej procedurze sprzedano bilety - mówi Koziński.

Kibolskie strony {team:Legii} podają, że do Lubina pojechało 800 osób.

- Nie wierzę, że wszyscy kibice {team:Legii} przekazali Zagłębiu wszystkie dane wymagane przez ustawę oraz że klub zdążył wyłapać ukaranych zakazami stadionowymi. Jeśli ktoś miał wejść na trybuny, dostał się tam - twierdzi Ostrowski.

Na stronie legia.com.pl czytamy: "(...) klub, jak i kibice {team:Zagłębia} od kilku lat nie robią warszawianom problemów z wejściem na obiekt, niezależnie od aktualnego stanu wewnątrzlegijnej zawieruchy (...)".

Prezes Koziński spodziewa się kar ze strony {league:Ekstraklasy}, ale tylko za race. - Zdajemy sobie sprawę, że race były złamaniem przepisów, a ekstraklasa nie będzie zważać na święto górników - mówi prezes Koziński.

Syndrom {team:Legii}: z dobrymi wygrywa, ze słabymi traci punkty
{team:Arka} zapowiada walkę z bandytami


W czwartek Komisja Ligi ukarała {team:Arkę Gdynia} 50 tys. zł grzywny i zamknięciem stadionu na trzy mecze za rozróby i race. - To, co działo się na meczach z {team:Lechią} i ze {team:Śląskiem}, mogę określić tylko jednym słowem: bandytyzm - mówi Dariusz Guzikowski, dyrektor ds. bezpieczeństwa w klubie z Gdyni. - Dlatego zrobimy wszystko, aby złapać i ukarać jak najwięcej bandytów, którzy rozrabiali na tych meczach. A nawet nie jak najwięcej. Chcemy złapać wszystkich!

Klub zapowiada też, że wytoczy kibolom sprawy cywilne i będzie się domagał wysokich odszkodowań. {team:Arka} straci w sumie 200 tys. zł - 50 tys. to grzywna, a 150 tys. to straty z powodu rozgrywania trzech spotkań bez udziału publiczności.

- Użyjemy wszelkich metod, aby te pieniądze zapłacili bandyci. Mamy wyjątkowo bogaty materiał z tych spotkań. Zapis z monitoringu, kamer telewizyjnych, prywatne zdjęcia kibiców - dodaje Guzikowski.

Od derbów z {team:Lechią} minęło 12 dni. {team:Arka} nie ukarała żadnego kibola klubowym zakazem stadionowym.

Polscy piłkarze mogą mieć końskie zdrowie - twierdzi paul Robbins »






