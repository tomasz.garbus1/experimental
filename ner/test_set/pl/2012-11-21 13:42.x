::Transfery. Drogba prosi FIFA o zgodę na wypożyczenie Piłka nożna - Sport.pl::
@2012-11-21 13:42
url: http://www.sport.pl/pilka/1,96012,12897717,Transfery__Drogba_prosi_FIFA_o_zgode_na_wypozyczenie.html


O otrzymaniu wniosku FIFA poinformowała w środę. Federacja rozważa, czy udzielić Drogbie zgody na odstępstwo o przepisów. Sezon w chińskiej ekstraklasie zakończył się w listopadzie. Obowiązujące zasady zabraniają 34-latkowi zmiany klubu do 1 stycznia.

Zdarzało się już, że międzynarodowa federacja zezwalała piłkarzom na ominięcie zasad. Tak było w przypadku piłkarzy z Major League Soccer - Davida Beckhama, Thierry'ego Henry'ego i Landona Donovana

Drogba w 2012 roku podpisał dwuletni kontrakt z klubem z Szanghaju. Po wygraniu Ligi Mistrzów odszedł z Chelsea. Możliwe, że wróci do Londynu, gdzie jest legendą. Chelsea walczy o awans do fazy pucharowej LM. W ostatnim meczu musi wygrać z duńskim Nordsjaelland i liczyć na przegraną Juventusu Turyn w drugim meczu grupy E. Nawet jeśli Iworyjczyk trafi na Stamford Bridge przed kluczowym spotkaniem, nie będzie mógł w nim wystąpić. Do rozgrywek składy zgłasza się przed ich rozpoczęciem.

Zmiany w Chelsea. Di Matteo nie jest już trenerem londyńczyków »






