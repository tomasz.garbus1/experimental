::{player:Krzysztof Piątek} z {team:Milanem} gra o {league:Ligę Mistrzów}. Czy będzie niedziela cudów w {league:Serie A}? Piłka nożna - Sport.pl::
@2019-05-25 10:37

Mistrzem {team:Juventus}, wicemistrzem {team:Napoli}, pewne spadku {team:Frosinone} i {team:Chievo} - to wiadomo przed ostatnią, 38. kolejką {league:Serie A}. Na sobotę zaplanowano dwa z 10 meczów. Oba nieistotne dla końcowej tabeli (Frosinone - Chievo i Bologna - Napoli).
REKLAMA






W niedzielę najciekawiej powinno być w walce o trzecie i czwarte miejsce. Te pozycje dają prawo gry w {league:Lidze Mistrzów}. W walce o nie są cztery drużyny.Sytuacja w tabeli wygląda tak:3. {team:Atalanta Bergamo} 66 pkt4. {team:Inter Mediolan} 665. {team:AC Milan} 656. {team:AS Roma} 63Wydaje się, że najbliżej gry w {league:Champions League} jest {team:Atalanta}, która podejmie {team:Sassuolo}, a więc 10. zespół tabeli, pewny już utrzymania i mający zbyt dużą stratę punktową, by być jeszcze w grze o europejskie puchary.

Wszystko w swoich rękach mają też gracze {team:Interu}. Ale oni mogą mieć trudniej, bo na {stadium:San Siro} przyjedzie dobrze dysponowane w ostatnich kolejkach {team:Empoli} (trzy zwycięstwa z rzędu), które potrzebuje punktów, by utrzymać się w {league:Serie A}. Przed ostatnią kolejką drużyna, której bramki strzeże {player:Bartłomiej Drągowski} ma 38 oczek i jest 17. w tabeli. Ostatnie spadkowe miejsce, czyli 18., zajmuje {team:Geona}, która ma 37 punktów. Były klub {player:Piątka} w niedzielę zagra na wyjeździe z {team:Fiorentiną}, która ma 40 punktów, jest 15. i również nie może być pewna pozostania w {league:Serie A}.Końcówka tabeli {league:Serie A} wygląda tak:15. {team:Fiorentina} 40 pkt16. {team:Udinese} 4017. {team:Empoli} 3818. {team:Genoa} 3719. {team:Frosinone} 2420. {team:Chievo} 16{manager:Drągowski} i jego drużyna są nadzieją dla {player:Piątka} i jego zespołu. {team:Milan} zmierzy się na wyjeździe ze {team:SPAL}. A ta ekipa ma 42 punkty i jest już pewna gry w ekstraklasie w następnym sezonie.

Również {team:Roma} zagra na koniec z drużyną, która nie walczy już ani o puchary, ani o uniknięcie spadku. Ale jest mało prawdopodobne, że ewentualne zwycięstwo nad {team:Parmą} da {team:Romie} awans do "czwórki". Rzymianie muszą po prostu liczyć na wpadki rywali.





