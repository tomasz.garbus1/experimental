::Bartosz Kapustka wrócił do gry i zapracował na piękne słowa trenera Piłka nożna - Sport.pl::
@2017-11-06 21:50
url: http://www.sport.pl/pilka/7,65081,22612370,bartosz-kapustka-wrocil-do-gry-i-zapracowal-na-piekne-slowa.html

- Miał ciężki rok w Leicester City, był kontuzjowany i nie miał wielu okazji do gry, ale wiedziałem, że jest silny i może robić różnicę - ocenił Bartosza Kapustkę Christian Streich, szkoleniowiec Freiburga.
REKLAMA




Polak nie miał łatwego początku w Niemczech. Nie grał regularnie, a trener Freiburga publicznie przyznał, że ma problemy motoryczne. Wszystko zmieniło się w 11. kolejce Bundesligi. Polak wybiegł w pierwszym składzie Freiburga i dostał od "Kickera" najwyższą ocenę w zespole - 2,5.Zespół Kapustki przegrał z Schalke (0:1), ale Polak pokazał, że jest w stanie regularnie grać w pierwszym składzie niemieckiego zespołu. Potwierdził to Christian Streich, szkoleniowiec Freiburga.- Świetnie było widzieć Bartka na boisku. Wcześniej nie grał zbyt wiele, a pokazał się z dobrej strony - zaznaczył niemiecki trener.Freiburg po 11 meczach Bundesligi znajduje się na miejscu barażowym. Drużyna Christiana Streicha zdobyła osiem punktów i walczy o utrzymanie w Bundeslidze.





