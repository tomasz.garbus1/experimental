::Remis, który niepokoi kibiców Piłka nożna - Sport.pl::
@2010-05-17 18:00


{team:Pogoń} i {team:Warta} tej wiosny skompletowały ledwie po 10 punktów. Oba zespoły łączyło jeszcze coś - bierność obrońców przy stałych fragmentach gry. Podopieczni {manager:Marka Czerniawskiego} w ostatnich 3 spotkaniach stracili przez gapiostwo w kryciu w sumie 6 bramek (!), a podopieczni {manager:Piotra Mandrysz} zawalili gole z {team:ŁKS} i {team:Górnikiem}.

W sobotę rzuty wolne znów dały o sobie znać. Goście nie potrafili upilnować {player:Krzysztofa Hrymowicza}, chociaż doskonale wiedzieli, jak zachowuje się pod bramką przeciwnika. To była 50. minuta meczu. O pierwszej części lepiej nie wspominać, bo mocno wiało nudą. Jakby to przewidzieli fani "granatowo-bordowych", którzy na {stadium:stadionie Floriana Krygiera} stawili się w liczbie około 2 tys.

- Wiemy, że pokłosiem tego była nasza przegrana z {team:Górnikiem Zabrze} - przyznaje {manager:Mandrysz}. - Poza tym pogoda była nieciekawa, a stadion mamy taki, że kibic przychodząc na mecz patrzy wcześniej w niebo.

Jednemu kibicowi faktycznie udzieliła się nuda i głupota. W pierwszej połowie zaczął świecić zielonym laserem po oczach poznańskich zawodników.

- Przy stałych fragmentach gry nasz bramkarz został kilka razy oślepiony, podobnie jak obrońcy - opisuje {manager:Marek Czerniawski}. - Przykro mi, ale wiem, że zdarzają się tacy ludzie.

Sobotni pojedynek, który dla {team:Pogoni} był o ligową "pietruszkę", wprawdzie nie był poligonem doświadczalnym, ale trener dokonał pewnych zmian w wyjściowej jedenastce. Zdjął ostatnio fatalnie grającego środkowego obrońcę {player:Omara Jaruna}, w jego miejsce przesunął uniwersalnego {player:Macieja Mysiaka}. {player:Piotra Petasza}, który będzie pauzował w finale {league:PP} za kartki, zastąpił na lewej flance z powodzeniem {player:Przemysław Pietruszka} (ostatnio w środku pola). Do ważniejszych posunięć zaliczamy także grę w podstawowym składzie Dariusza Zawadzkiego po ponad miesięcznej przerwie. Zagrał na nietypowej dla siebie pozycji - tuż za napastnikiem. Trener zdjął go po 45. min.

- Próbowałem różnych wariantów pod kątem meczu pucharowego, a niektórym piłkarzom chciałem dać odpocząć. Pierwsza połowa zmusiła mnie jednak do korekt w składzie - podkreśla {manager:Mandrysz}.

I tak po przerwie z ławki weszli {player:Olgierd Moskalewicz} i {player:Marcin Bojarski}, później {player:Maksymilian Rogalski}. I dobrze, bo gra się poprawiła. Jednak po strzelonej bramce szczecinianie jakby oddali inicjatywę gościom, którzy od 81. min grali w osłabieniu (w ostatnich minutach w "dziewiątkę" przez kontuzję).

- Trochę mnie zaskoczyło, że {team:Pogoń} zadowoliła się jedną bramką. Po meczu żartowałem, że czerwona kartka nam pomogła, bo wpłynęła na desperację w zespole i w osłabieniu wyrównaliśmy - cieszył się {manager:Czerniawski}.

{team:Pogoń} zalicza trzeci mecz bez zwycięstwa. "Testowe" spotkanie nie napawało optymizmem przed finałem w Bydgoszczy (22 maja).

{team:Pogoń Szczecin} - {team:Warta Poznań} 1:1 (0:0)

Bramki. {team:Pogoń}: {player:Hrymowicz} (50. po dośrodkowaniu z rzutu wolnego {player:Pietruszki}). Warta: {player:Magdziarz} (85. asysta ze środka pola {player:Iwanickiego}).

{team:Pogoń}: {player:Janukiewicz} - {player:Nowak}, {player:Mysiak}, {player:Hrymowicz}, {player:Woźniak}, {player:Wólkiewicz} (76. {player:Rogalski}), {player:Koman} (46. {player:Bojarski}), {player:Zawadzki} (46. {player:Moskalewicz Ż}), {player:R. Mandrysz}, {player:Pietruszka}, {player:Lebedyński}.

{team:Warta}: {player:Radliński} - {player:Ignasiński}, {player:Wichtowski Ż}, {player:Strugarek}, {player:Otuszewski}, {player:Bekas} (75. {player:Miklosik}), {player:Jankowski ŻŻCZ}, {player:Magdziarz}, {player:Seweryn Ż}, {player:Iwanicki}, {player:Kaźmierowski}.

Sędziował Marcin Słupiński (Łódź)

Widzów : 2000

Oceny portowców: {player:Janukiewicz} 4, {player:Nowak} 2, {player:Mysiak} 2, {player:Hrymowicz} 3, {player:Woźniak} 3, {player:Wólkiewicz} 2, {player:Rogalski} 3, {player:Koman} 1, {player:Bojarski} 3, {player:Zawadzki} 1, {player:Moskalewicz} 2, {player:Mandrysz} 3, {player:Pietruszka} 4, {player:Lebedyński} 2.






