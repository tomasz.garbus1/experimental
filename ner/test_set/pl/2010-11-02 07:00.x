::Piłkarz bez brzucha jak garnek bez ucha Reprezentacja Polski::
@2010-11-02 07:00
url: http://www.sport.pl/pilka/1,65037,8597366,Pilkarz_bez_brzucha_jak_garnek_bez_ucha.html


Bogate społeczeństwa Zachodu stworzyły nie tylko cywilizację śmierci, ale jeszcze starają się stworzyć cywilizację wychudzenia, nikt już nie je po to, żeby zjeść, przeżuwamy wyłącznie z poczucia obowiązku. Albo poprawiamy perystaltykę jelit, albo polerujemy pamięć (koniecznie w jednej porcji z koncentracją), leki przyjmujemy na śniadanie, obiad i kolację. Aż dziwne, że sklepów spożywczych wciąż nie wchłonęły apteki, że kolejne tomy z dietami cud nie stają na półkach z poradnikami medycznymi. Odróżnić oba gatunki coraz trudniej.

Tym niżej pokłońmy się naszym piłkarzom, którzy bywają szczególnie narażeni na propagandę. Czy raczej - naszym ligowcom, bo importowani obcokrajowcy prędko nabywają biegłości w biesiadowaniu. Nawet jeśli ewidentnie sobie szkodzą. Czy Dickson Choto, leczący właśnie ścięgno Achillesa, nie urywałby sobie członków rzadziej, gdyby nie ćwiczył dorodnie otłuszczony?  Czy Takesure Chinyama kulałby miesiącami, gdyby również nie obrastał w dodatkowe fałdy?


Hołdy składam na razie wyłącznie bohaterom - byłym lub obecnym - Legii, bo warszawski futbol od lat puchnie od wykwintnych kulinariów. Tutaj mało kto obmacuje opakowania ze wszystkich stron, by zlizać z etykiety nienasycone kwasy omega-3, mało kto zawaha się przed wylizaniem michy z sosu o wysokim indeksie glikemicznym, nikt nie sprawdza, czy goloneczka zassana w nocy przed meczem zwolni jelita przed pierwszą połową czy dopiero w przerwie. Mecz nie doktorat z chemii, Maciej Iwański rozgrywał wizjonersko także wtedy, gdy trybuny wołały nań "Pączek". Nie tylko stołeczni piłkarze lubią zajechać po wieczornym ligowym hicie do McDonald'sa, choć dla budowania gwiazdorskiego wizerunku buły wypchniętej hamburgerem najobficiej zasłużył się akurat były legijny bramkarz, sławny Artur Boruc, który McDonald'sa reklamował. Oczywiście spoty kręcił razem z kilkuletnimi malcami, by chłopiec już za młodu wiedział, że ganiać za piłką, nie znaczy umartwiać się, a droga ku chwale wiedzie przez sieć właśnie tych jadłodajni, nawiasem mówiąc, przez koncern uparcie nazywanych restauracjami.

Sztukę życia opanowali nasi futboliści po wirtuozersku. Taki np. Maciej Rybus - inny młody półbóg z Łazienkowskiej - papierosami zaciągał się jeszcze jako nastoletni skrzydłowy. I znów - palił pomimo bezrefleksyjnej kampanii promującej niepalenie, pomimo antynikotynowego cenzurowania filmów, które za kilka lat, po kolejnym barbarzyńskim cyfrowym retuszu, będą nam wmawiać, że Humphrey Bogart nie dmuchał wrogom w twarz kłębami dymu, lecz strzelał gumą balonową. Zafałszować przeszłość, by ulepić na swoją modłę teraźniejszość - im bardziej znamy tę strategię bezwzględnego kolportowania kłamstw, chowaną za politycznie poprawnymi sloganami o inżynierii społecznej, z tym większym podziwem powinniśmy spoglądać na Rybusa i jego kumpli, którzy manipulować sobą nie pozwalają.

Janczyk, Choto, Chinyama, Iwański, Boruc, Rybus - wszyscy szkołę sportu przeszli lub przechodzą w Warszawie, ale okrajanie listy smakoszy życia do okolic stołecznych byłoby nieuczciwe. Opowiadał niedawno grający w mniejszym mieście ponadtrzydziestoletni piłkarz, jak to osłupiał, gdy na postoju w trakcie autokarowej podróży na mecz jego partnerzy z boiska urządzili sobie gromadną konsumpcję kebabów i czekoladowych groszków. On akurat został skażony na zepsutym Zachodzie - właśnie wrócił stamtąd u schyłku kariery - więc chciał interweniować, wyperswadować objuczanie kolan przed grą zbędnym balastem, zwłaszcza że drużyna regularnie wygrywała i obwołano ją rewelacją sezonu w ekstraklasie. Z interwencji zrezygnował, gdy dostrzegł, że kebaba konsumuje również trener.

W żywieniowej awangardzie tkwi cała liga jak długa i szeroka.


Opowieści o jej bohaterach zajeżdżających do przydrożnych barów zasługiwałyby na pękaty leksykon. Ach, te hot dogi na stacjach benzynowych, frytkowe kopczyki w McZestawach, chipsy podlane piwkiem, oczywiście małym... Ta przebiegłość kopaczy, którzy jednym zgrabnym szarpnięciem widelca przykrywają ziemniaki sałatą, bo przecież nie będzie fizjolog pluł im do talerza...

Inna sprawa, że kluby podobnych nadzorców wynajmują rzadko, a dietetyków to już nie zatrudniają wcale. Nie dają się zwariować, nie przejmują bezmyślnie kolejnego wytworu paranoicznej współczesności, która tak opływa w dostatek i rozleniwiła się intelektualnie, że suto płaci za radę, co i jaką chochlą nalać do gara. Piłkarze z lig bliższych Atlantykowi - tam ludzkość od dobrobytu nienormalnieje szybciej - coraz chętniej zatrudniają wręcz osobistych konsultantów od talerza! Czy i my popadniemy w obłęd? Czy Polakom najmłodszym, o bezbronnie chłonnych umysłach, świat również wyrwie tradycyjną patelnię, żeby obłowili się producenci aparatury do gotowania na parze?

Piłkarze, powtórzmy, są specjalnie narażeni na propagandę, która zdrową tkankę narodu przetapia w paradę narcyzów dopieszczających każdy centymetr sześcienny samych siebie. Młodym łatwo namieszać w głowach porównaniami ludzkiego organizmu do samochodu, który wymaga mądrej eksploatacji, jeśli chcemy używać go długo i na najwyższych obrotach. Albo jeszcze bardziej szatańskimi porównaniami do bolidu Formuły 1, który wymaga obsesyjnej dbałości o detal, by co wyścig wycisnąć zeń maksymalne osiągi. Albo antypolskimi porównaniami do gladiatorów z ligi angielskiej, którzy zdaniem zdrajców narodu mogą wychylić jeden browar więcej, bo ich wytrenowane ciała to zniosą, a nasze niewytrenowane nie zniosą.



Uczestniczyłem w burzliwej, wytrawnie zakrapianej dyskusji o futbolu, podczas której usłyszałem - oszołom zeskrobywał panierkę z kotleta, klasyczna ofiara nowoczesności - że skoro niejaki Peszko kończy zgrupowanie reprezentacji pod wpływem i o świcie, bo nawet nie zdaje sobie sprawy, że to mu zaszkodzi na dłużej niż dwie godziny po przebudzeniu, to on, mój dyskutant, nie dziwi się, że Lech Poznań w czwartek remisuje z Juventusem, a w weekend broni się przed spadkiem z polskiej ligi. Grać w tzw. ekstraklasie, europejskich pucharach i zarazem w kadrze narodowej, w przerwach trenować, a po nocach wciągać etanol z dwutlenkiem węgla? Kto by to wytrzymał? Nie ma wyjścia, musisz co drugi mecz odbębnić byle jak, sprinterskie zrywy tylko symulując.

Ot, perora typowego kulinarnego faszysty, autorytarne zapędy organizowania cudzego życia obsmażającego dobrymi intencjami. On by nawet drinki mieszał piłkarzom osobiście - tak się oburzył chemicznym składem ulubionego napoju polskiej ligi, czyli kultowej whisky z colą.

Próby wychowawcze podejmuje się w wielu miejscach, usłyszałem od fizjoterapeuty jednej z juniorskich reprezentacji Polski, jak próbował truć kadrowiczom, czego wkładać do ust nie powinni. I pewnie wyprałby im - teoretycznie najzdolniejszym w swoim pokoleniu - mózgi, gdyby nie musieli wracać do rozrzucanych po całym kraju klubów. Tam podają kartę dań pełnowartościową, tam kierowca w trasie zawsze przystanie, żeby chłopaki wyskoczyły na kebaba albo parówkę. A potem, kiedy już awansują do kadry seniorskiej, zagrożenie całkiem zniknie. Trener Franciszek Smuda - zaprzysięgły tradycjonalista i wróg postępu - skazał Peszkę po spożyciu na reprezentacyjną banicję dożywotnio. Amnestię obiecał już miesiąc później.

Podyskutuj o felietonie na blogu Rafała Steca






