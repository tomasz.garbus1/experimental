::Po Śląsku: '{player:Jova}' lubi grillować, {player:Kapo} dobra rada, '{player:Ryba}' z tlenem [OCENY WYBORCZEJ] Piłka nożna - Sport.pl::
@2015-03-16 13:00


Tradycyjnie w skali 1-10. Oceny posłużą nam do wyboru piłkarza wiosny i całego sezonu 2014/15.

Oceny za {team:Śląsk}


{player:Vytautas Cerniauskas} (90) - 2


My też jesteśmy cierpliwi i wyrozumiali, ale co za dużo to nie zdrowo...

{player:Paweł Golański} (90) - 6


Wprawdzie tym razem bez asysty, ale zagrał najlepszy mecz na wiosnę. Harował na całej długości boiska.

{player:Piotr Malarczyk} (90, Ż) - 6


Podtrzymujemy opinię trenera {player:Tarasiewicza}, że to jeden z najlepiej grających głową obrońców w całej lidze.

{player:Radek Dejmek} (90) - 5


Gdyby zachował się lepiej przy {player:Pichu}, to {player:Cerniauskas} mógłby sobie wybiegać nawet na 30 metr.

{player:Kamil Sylwestrzak} (90, asysta) - 5


Piękna asysta i cała akcja przy drugim golu {player:Kiełba}, ale gra w defensywie do poprawy. Bo momentami strasznie objeżdżany.

{player:Jacek Kiełb} (90, 2 gole) - 7


Takiego "Rybę" chciałoby się oglądać co tydzień. Choć w końcówce baterie trochę mu siadły i przydałby się zmiana.

{player:Aleksandrs Fertovs} (90) - 6


Obdarzony zaufaniem po słabej grze w Bielsku pokazał, że możliwości naprawdę ma duże. Spokój, opanowanie, a kilka odbiorów, że palce lizać.

{player:Vlastimir Jovanović} (90) - 7


W takich meczach - jak to mawia trener {manager:Tarasiewicz} - "na grillu" czuje się najlepiej. Zaczyna być prawdziwym liderem drużyny.

{player:Luis Carlos} (90) - 6


Wymiana piłki z {player:Kapo}, ogranie {player:Celebana} i dogranie w "szesnastkę" powinny pojawić się w spocie reklamowym na przyszły sezon ekstraklasy.

{player:Olivier Kapo} (90) - 6


Można do niego zagrywać w ciemno, bo wiadomo, że zawsze coś pozytywnego wymyśli. A już na pewno nie straci piłki.

{player:Rafael Porcellis} (90) - 5


Piłka szuka go w polu karnym. Pierwszą połowę powinien zakończyć z golem.

{player:Nabila Aankour} (38) - 6


No, takiego Nabila to rozumiemy. "Grillował" od początku.






