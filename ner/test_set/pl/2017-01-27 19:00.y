::Finały Australian Open, {player:Milik} wraca do {league:Serie A}, {player:Lewandowski} goni {player:Aubameyanga} [ROZKŁAD WEEKENDU] Piłka nożna - Sport.pl::
@2017-01-27 19:00

Sobota:


Godz. 9.30 Venus Williams - Serena Williams


W finale kobiecej rywalizacji zmierzą się 35-letnia Serena Williams, a po drugiej stronie kortu stanie jej starsza o rok siostra Venus. Pierwsza walczy teraz o rekordowy w liczonej od 1968 roku Open Erze 23. triumf w Wielkim Szlemie w grze pojedynczej. Druga z Amerykanek ma w dorobku siedem tytułów. Ich sobotni pojedynek zapisze się w historii - będzie to wielkoszlemowy finał z udziałem najstarszych singlistek w historii.

Godz. 14.45 {team:Resovia} - {team:Skra}


Mecz drugiej z trzecią drużyną w tabeli. W pierwszej rundzie rzeszowianie wygrali na wyjeździe 3:1, bo dobrze grali blokiem i popełniali mniej błędów niż rywale. {team:Skra} ma za sobą dobry okres w {league:PlusLidze}, zwyciężyła w ostatnich dziewięciu ligowych potyczkach. {team:Resovia} uległa w poprzedniej kolejce ekipie z Gdańska po pięciosetowym boju.

Godz. 15.30 {team:Werder} - {team:Bayern}


Zwycięski w sześciu ostatnich kolejkach {team:Bayern} zmierzy się z {team:Werderem}, z którym wygrał 12 ostatnich spotkań ligowych. Jeśli odniesie 13. zwycięstwo, ustanowi rekord {league:Bundesligi} - żaden zespół nie miał nigdy tak długiej serii wygranych meczów z jednym rywalem. {player:Robert Lewandowski} walczy o pierwsze miejsce w klasyfikacji strzelców. Na razie ma 14 bramek, o dwie mniej od prowadzącego Gabończyka {player:Pierre'a-Emericka Aubameyanga}, który powrócił już do składu {team:Borussii Dortmund} z {league:Pucharu Narodów Afryki}.

Godz. 16.05 PŚ w skokach - konkurs drużynowy w Willingen


Sobotni konkurs będzie trzecim drużynowym w sezonie 2016/17. Pierwszy - 3 grudnia w niemieckim Klingenthal - wygrali Polacy przed {country:Niemcami} i {country:Austrią}. Był to pierwszy triumf biało-czerwonych w historii. W drugim na Wielkiej Krokwi w Zakopanem triumfowali Niemcy przed {country:Polską} i {country:Słowenią}.

Poza tym dzieje się:


10:00 Biatlon: Mistrzostwa Europy w Dusznikach-Zdroju - bieg na dochodzenie mężczyzn

10.15 Narciarstwo alpejskie: PŚ w Cortinie d'Ampezzo - zjazd kobiet

11.45 Australian Open - mecz finałowy gry podwójnej mężczyzn

12.00 Narciarstwo alpejskie: PŚ w Garmisch-Partenkirchen - zjazd mężczyzn

12.55 {team:Villarreal} - {team:Granada} (transmisja w Eleven Sports)

13.00 Biatlon: Mistrzostwa Europy w Dusznikach-Zdroju - bieg na dochodzenie kobiet

13.25 {team:Liverpool} - {team:Wolverhamtpon} (transmisja w Eleven)

15.55 {team:Chelsea} - {team:Brentford} (transmisja w Eleven Sports)

16.10 {team:Deportivo Alaves} - {team:Atletico Madryt} (transmisja w Eleven)

16.55 {team:Lyon} - {team:Lille} (transmisja w Eleven Extra)





17.55 {team:Legia Warszawa} - {team:Zenit Sankt Petersburg}

17.55 {team:Lazio} - {team:Chievo} (transmisja w Elven Sports)

18.25 {team:Southampton} - {team:Arsenal} (transmisja w Eleven)

19.55 {team:Rennes} - {team:Nantes} (transmisja w Eleven Sports)

20.40 {team:Inter} - {team:Pescara} (transmisja w Eleven)

20.40 {team:Leganes} - {team:Celta Vigo} (transmisja w Eleven Extra)

Niedziela:


Godz. 9.30 Roger Federer - Rafael Nadal


W decydującym pojedynku u mężczyzn kibice obejrzą konfrontację 35-letniego Szwajcara Rogera Federera z młodszym o pięć lat Hiszpanem Rafaelem Nadalem. Pierwszy może pochwalić się 17 - rekordowymi w męskich singlu - zwycięstwami w Wielkim Szlemie, drugi - 14. Nadal z Federerem do tej pory grał 34 razy, wygrał 23 pojedynki. Gdy obaj spotykali się w finałach, Nadal ma bilans 14-7.

Godz. 12 {team:Betis} - {team:FC Barcelona} (transmisja w Eleven)


Broniąca tytułu {team:FC Barcelona} zagra na stadionie rywala z {team:Betisem}. Katalończycy od 1 listopada przegrali tylko jeden mecz - z {team:Athletic Bilbao} 1:2 w pierwszym spotkaniu 1/8 finału {league:Pucharu Hiszpanii}. Później jednak odrobili stratę i awansowali do kolejnej rundy, a po czwartkowym zwycięstwie nad {team:Realem Sociedad} 5:2 dostali się także do półfinału. W {league:LaLiga}

Godz. 15.05 PŚ w skokach. Konkurs indywidualny w Willingen


W niedzielę na skoczni w niemieckim Willingen lider klasyfikacji generalnej Pucharu Świata w skokach narciarskich Kamil Stoch stanie przed szansą odniesienia piątego zwycięstwa z rzędu i 21. w karierze. Od 2014 roku dwukrotny mistrz olimpijski z Soczi trzy razy startował w Willingen w konkursach indywidualnych i... trzy razy je wygrał.

Godz. 20.45 {team:Napoli} - {team:Palermo} (transmisja w Eleven)


{player:Arkadiusz Milik} po raz pierwszy od fatalnej kontuzji kolana znalazł się w kadrze meczowej {team:Napoli}. Czy w meczu z przedostatnim w {league:Serie A} {team:Palermo} zagra, nie wiadomo. W wyjściowej jedenastce powinien się za to znaleźć {player:Piotr Zieliński}. Możemy też liczyć na to, że w defensywie {team:Palermo} od pierwszych minut zagra {player:Thiago Cionek}.

Godz. 20.45 {team:Real Madryt} - {team:Real Sociedad} (transmisja w Eleven Sports)


Po niespodziewanym odpadnięciu w ćwierćfinale piłkarskiego {league:Pucharu Hiszpanii}, lider tamtejszej ekstraklasy {team:Real Madryt} postara się szybko zażegnać kryzys. W 20. kolejce jego rywalem będzie {team:Real Sociedad}, który nie jest w tym sezonie łatwym przeciwnikiem. Jest piąty w tabeli i ma 35 punktów, podobnie jak czwarte {team:Atletico Madryt}. To oznacza, że wciąż liczy się w walce o awans do {manager:Ligi Mistrzów}. Do lidera zespół z San Sebastian traci osiem "oczek".

Godz. 21 {team:PSG} - {team:Monaco} (transmisja w Eleven Extra)


Po 21. kolejkach {team:Monaco} było liderem z przewagą dwóch punktów nad Niceą. {team:PSG} zajmowało trzecia lokatę ze stratą trzech punktów. Mecz ma ogromne znaczenie w kontekście walki o mistrzostwo {country:Francji}. W pierwszy meczu lepsi byli gracze z {team:Monako}, którzy zwyciężyli aż 3:1. W niedzielnym spotkaniu nie zagra {player:Grzegorz Krychowiak} ({team:PSG}), który jest kontuzjowany. Nie wiadomo też, czy wystąpi {player:Kamil Glik} ({team:Monaco}), który narzekał na problemy mięśniowe.




Poza tym dzieje się:


10.00 Narciarstwo alpejskie: PŚ w Garmisch-Partenkirchen - slalom gigant mężczyzn

10.00 Biatlon: Mistrzostwa Europy w Dusznikach-Zdroju - pojedyncza sztafeta mieszana

12.25 {team:Torino} - {team:Atalanta} (transmisja w Eleven Sports)

13.00 {team:Milwall} - {team:Watford} (transmisja w Eleven Extra)

13.00 Biatlon: Mistrzostwa Europy w Dusznikach-Zdroju - sztafeta mieszana

13.00 Biegi narciarskie: PŚ w Falun - bieg na 30 km stylem klasycznym mężczyzn

14.00 Biegi narciarskie: PŚ w Falun - bieg na 15 km stylem klasycznym kobiet

14.55 {team:Sassuolo} - {team:Juventus} (transmisja w Eleven Extra)

14.55 {team:Sampdoria} - {team:AS Roma} (transmisja w Eleven Sports)

16.10 {team:Espanyol} -{team:Sevilla} (transmisja w Eleven)

16.55 {team:Manchester United} - {team:Wigan} (transmisja w Eleven Sports)

17.20 Piłka ręczna mężczyzn: Mistrzostwa Świata we {country:Francji} - mecz finałowy

18 {team:Anderlecht} - {team:Standard} (transmisja w Eleven Extra)

18.25 {team:Athletic Bilbao} - {team:Sporting Gijon} (transmisja w Eleven)

22.00 Hokej: NHL Mecz Gwiazd



Wielkie gwiazdy zmienią klub w ostatniej chwili? Koniec okna transferowego coraz bliżej!






