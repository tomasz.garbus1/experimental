::Transfery. ESPN: {player:Zlatan Ibrahimović} trafi do {league:MLS} Piłka nożna - Sport.pl::
@2018-01-29 17:09

Wszystko wskazuje, że latem Szwed odejdzie z {team:Manchesteru United} i na zasadzie wolnego transferu przeniesie się do {team:Los Angeles Galaxy}. {player:Ibrahimović} występuje w {country:Anglii} od zeszłego sezonu. Dla "{team:Czerwonych Diabłów}" rozegrał łącznie 53 spotkania, strzelił 29 goli i zanotował 10 asyst. Problem w tym, że dobre statystyki zawdzięcza sezonowi 2016/17. W obecnym sezonie idzie mu dużo gorzej.
REKLAMA




Do gry wrócił w listopadzie, po tym jak w kwietniu 2017 zerwał więzadła w kolanie. Od tej pory wystąpił w 7 meczach, ale strzelił tylko jednego gola. W pierwszym składzie {team:United} gra {player:Romelu Lukaku}, który latem trafił do drużyny. W dodatku zimą {team:Manchester} ściągnął także {player:Alexisa Sancheza} z {team:Arsenalu}.- {player:Zlatan} mówił mi ostatnio, że chce wrócić do wysokiej formy i pomóc drużynie w drugiej części sezonu. Jednak jeśli zamierza odejść, to nie będę mu w tym przeszkadzał - powiedział {manager:Jose Mourinho}.Według informacji ESPN 36-letni napastnik uzgodnił już warunki kontraktu z {team:LA Galaxy}. W przeszłości zawodnikiem tego klubu byli m.in: {player:Robbie Kean} i {player:Steven Gerrard}.





