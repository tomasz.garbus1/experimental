::{team:Ajax} dostał wyjątkową pomoc przed półfinałem {league:Ligi Mistrzów}. Wszystkie kluby {league:Eredivisie} wyraziły zgodę Piłka nożna - Sport.pl::
@2019-04-19 10:42


{team:Ajax} po wtorkowym zwycięstwie 2:1 nad {team:Juventusem}, awansował do półfinału {league:Ligi Mistrzów}, gdzie zmierzy się z {team:Tottenhamem}. Pierwszy mecz zaplanowano na 30 kwietnia, co sprawiło, że pojawił się problem. Dwa dni wcześniej, w niedzielę 28 kwietnia, miała zostać rozegrana 33. kolejka {league:ligi holenderskiej}. Zgodnie z harmonogramem, wszystkie zespoły miały zagrać tego dnia o tej samej porze.
REKLAMA




Władze {league:Eredivisie} poszły jednak na rękę {team:Ajaksowi}, w związku z czym cała kolejka została przeniesiona na środę, 15 maja. Drużyna z Amsterdamu będzie wtedy już po dwumeczu z {team:Tottenhamem}, a w przypadku awansu do finału, ten zostanie rozegrany dopiero 1 czerwca.- Sądzimy, że to dobra propozycja, na podstawie zgody wszystkich drużyn, aby wszyscy rozegrali 33. i 34. kolejkę w tym samym czasie. Doceniamy fakt, że wszystkie kluby chciały współpracować w tej kwestii - powiedział Eric Gudde, dyrektor techniczny holenderskiej federacji.{team:Ajax} ma bardzo napięty grafik, ze względu na grę aż na trzech frontach. Obecny lider {league:Eredivisie} walczy o tytuł mistrza {country:Holandii}, zagra w półfinale {league:Ligi Mistrzów}, a dodatkowo 5 maja zmierzy się z {team:Willem II} w finale krajowego pucharu.





