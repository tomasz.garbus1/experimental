::{player:Sandomierski}: Pogrzebaliście mnie. Odżyłem i znowu jestem w formie Piłka nożna - Sport.pl::
@2014-12-11 16:20


- Wiele osób pogrzebało mnie za życia. Dochodziły mnie przecież dziwne głosy na mój temat. Postanowiłem natomiast skupić się na pracy. Po tym, jak usiadłem na ławce, miałem więcej czasu nie tyle na przemyślenia, ale na tym, żeby skupić się na robocie. To było dla mnie najważniejsze, ale nie żeby coś komuś udowadniać i za wszelką cenę pokazać, że skreślanie mnie było błędem. Nie miałem zamiaru "na złość mamie odmrażać sobie uszy". Zależało mi tylko na tym, żebym mógł wrócić do poziomu, który prezentowałem. Nie starałem się biczować, ale dochodzić do siebie - powiedział {player:Sandomierski}, który przed przerwą w występach miał najsłabsze statystyki skuteczności obronionych strzałów. Co drugie celne uderzenie wpadało do siatki. Teraz jest zdecydowanie lepiej.

- Zamieniłbym kilka moich udanych interwencji na zwycięstwo, bo punktów potrzebujemy jak powietrza. Moje statystyki mogłyby zostać takie, jak były. Zamiast tego wziąłbym punkty w lidze - mówi bramkarz {team:Zawiszy}.

- Wcześniej zawsze mi czegoś brakowało, innej decyzji, ustawienia, czy wyjścia z bramki. Wpadały gole, które można było wybronić przy większej pewności siebie i wyższej formie. Pewnie nie dałoby się zapobiec wszystkim, ale kilku z pewnością. Sam dochodziłem do takich wniosków patrząc później na nagrania wideo - kończy {player:Sandomierski}.

Jesteś kibicem z Bydgoszczy? Dyskutuj i dołącz do nas na Facebooku Sport.pl Bydgoszcz






