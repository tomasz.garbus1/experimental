::Brak wyróżnień dla piłkarzy Pogoni, dwójka nominowanych z Floty Piłka nożna - Sport.pl::
@2014-12-23 15:40
url: http://www.sport.pl/pilka/1,128220,17176638,Brak_wyroznien_dla_pilkarzy_Pogoni__dwojka_nominowanych.html


Canal Plus, który transmituje rozgrywki polskiej ekstraklasy wybierał tzw. największy plus jesieni. Kibice w głosowaniu wskazali na Semira Stilicia z Wisły Kraków. Jednym z jego kontrkandydatów był Adam Frączczak z Pogoni Szczecin, ale mimo uzyskania świetnego w sumie wyniku (prawie 12 procent głosów) zajął 4. miejsce. Wyprzedził m.in. Grzegorza Bonina (krótko był piłkarzem Pogoni, obecnie gra w Górniku Łęczna), Sebastiana Milę (Śląsk) czy Kamila Wilczka (Piast).

Drugie miejsce zajął Bartłomiej Drągowski z Jagiellonii Białystok, a trzecie Szymon Pawłowski z Lecha Poznań.

Także mecze z udziałem Pogoni nie zostały szczególnie wyróżnione wśród najciekawszych widowisk jesieni. W konkursie na mecz roku wygrało spotkanie Legii ze Śląskiem (4:3), a mecz Wisła Kraków - Pogoń Szczecin (3 maja, 5:0) zajął 9. pozycję w głosowaniu.

Za to serwis Ekstraklasa.net wybierał jedenastkę I ligi. Do grona wyróżnionych trafiła dwójka piłkarzy Floty Świnoujście, która długo była w czubie tabeli, ale końcówkę miała już słabszą. Na wyróżnienie zapracowali Sebastian Kamiński i Rafał Grzelak.

- Kamiński to odkrycie jesieni. Wiosną w ekipie ze Świnoujścia był jeszcze rezerwowym, lecz w przerwie letniej wywalczył pewny plac. Dobra szybkość i dośrodkowanie. Ciekawe, jak ten zawodnik się rozwinie. Grzelak po nieudanej przygodzie w gdyńskiej Arce odnalazł swoje miejsce w Świnoujściu. Rozgrywał, asystował. Dobrze sprawdzał się zarówno w środku pola, jak i na lewej flance - czytamy w uzasadnieniu portalu.






