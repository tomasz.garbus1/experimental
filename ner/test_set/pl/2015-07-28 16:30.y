::{team:Korona} wypożyczy skrzydłowego do {team:Radomiaka}? Piłka nożna - Sport.pl::
@2015-07-28 16:30


22-letni skrzydłowy od lutego jest zawodnikiem {team:Korony} (trafił z {team:Pilicy Białobrzegi}). W ekstraklasie jednak jeszcze nie zadebiutował. Wiosną zagrał w 10 meczach drużyny rezerw, w których strzelił jedną bramkę. W dwóch pierwszych meczach nowego sezonu trener {manager:Marcin Brosz} nie włączył {player:Matulki} do osiemnastki.








