::Ekstraklasa. Sześć meczów kary dla Cionka Piłka nożna - Sport.pl::
@2011-05-12 18:05
url: http://www.sport.pl/pilka/1,82313,9587850,Ekstraklasa__Szesc_meczow_kary_dla_Cionka.html


Zobacz brutalny atak Cionka - wideo »

Piłkarz wicelidera Ekstraklasy będzie musiał pauzować przez sześć najbliższych spotkań oraz zapłacić karę w wysokości 10 tysięcy złotych.

Sześć meczów pauzy to niespotykana dotąd wysokość kary za przewinienie na boisku. Dla przykładu, ukarany niedawno za uderzenie Manuela Arboledy Euzebiusz Smolarek odpoczywał "tylko" trzy mecze.

Do końca sezonu 2010/11 pozostały cztery kolejki spotkań. Thiago Cionek będzie zatem wyłączony również z dwóch pierwszych meczów nowego sezonu.






