::Sytuacja zdrowotna w Pogoni jest dobra Piłka nożna - Sport.pl::
@2013-07-02 10:26
url: http://www.sport.pl/pilka/1,128220,14205887,Sytuacja_zdrowotna_w_Pogoni_jest_dobra.html


Portowcy mają już za sobą dwa tygodnie przygotowań i 4 sparingi. Wydaje się, że najtrudniejszy okres przygotowań za nimi. Nad siłą pracowali w Szczecinie. Od niedzieli są na obozie w Gniewinie i tam, według zapewnień trenera Dariusza Wdowczyka, mają się koncentrować na zajęciach czysto piłkarskich - technika, taktyka, skuteczność.

Na obóz nie pojechał brazylijski obrońca Hernani, który leczy w Szczecinie naderwany mięsień czworogłowy. Uraz wyłączy z gry Hernaniego na 2-4 tygodnie. Inauguracja sezonu (19 lipca pogoniarze zagrają w Lubinie) raczej bez niego.

Pozostali piłkarze - zdrowi.

Po pierwszych (w poprzednią środę) sparingach kontuzję kostki w stawie skokowym zgłosił Tomasz Chałas, ale...

- Od poniedziałku trenuje na pełnych obciążeniach - mówi Dariusz Dalke, fizjoterapeuta Pogoni.

We wtorkowym spotkaniu sparingowym z Lechią Gdańsk może zabraknąć Radosława Wiśniewskiego, który pauzował w sobotnich meczach ze Światowidem Łobez i Jagiellonią Białystok. Skrzydłowy narzekał na tzw. przywodziciela.

- Nie jest to jakaś poważna sprawa, ale trzeba zachować ostrożność. Za dzień lub dwa Radek będzie normalnie trenował - stwierdza Dalke.






