::Rzeźniczak nie zatrzymał Arsenalu, Milan bez wpadki w Pireusie Piłka nożna - Sport.pl::
@2018-10-04 20:53
url: http://www.sport.pl/pilka/7,65042,24004681,liga-europy-rzezniczak-nie-zatrzymal-arsenalu-milan-bez-wpadki.html

Jakub Rzeźniczak zagrał cały mecz w defensywie Karabachu Agdam, ale jego zespół nie sprawił sensacji w meczu drugiej kolejki Ligi Europy z Arsenalem. Londyńczycy wygrali 3:0. Pierwszego gola strzelili już w trzeciej minucie, dwa trafienia dołożyli po przerwie. To druga porażka drużyny Rzeźniczaka w tym sezonie LE.
REKLAMA




W Lidze Europy zagrali też Jacek Góralski i Jakub Świerczok. Obaj weszli po przerwie w meczu Łudogorca Razgrad z FC Zurich przy 0:0, ale ich zespół przegrał na wyjeździe ze Szwajcarami 0:1.Anderlecht przegrał 0:2 z Dinamem Zagrzeb, a cały mecz na ławce chorwackiej drużyny przesiedział Damian Kądzior. Patryk Małecki też nie podniósł się z ławki rezerwowych w meczu Spartaka Trnava z Fenerbahce, a jego klub przegrał na wyjeździe 0:2.Igor Lewczuk nie znalazł się w kadrze meczowej Bordeaux na mecz z FC. Klub Polaka przegrał 1:2 z Duńczykami.

Tomasz Kędziora grał od pierwszej minuty w barwach Dynama Kijów w meczu z FK Jablonec. Polak pechowo poślizgnął się w 81. minucie w polu karnym i pozostawił napastnika gospodarzy bez opieki. Rywal szansy nie zmarnował i doprowadził do remisu 2:2.W najciekawszym meczu Milan przegrywał 0:1 w Pireusie do 70. minuty, ale zwyciężył 3:1, strzelając trzy gole w dziewięć minut.Chelsea tylko 1:0 pokonała słabiutkie MOL Vidi FC. Zwycięską bramkę w 70. minucie strzelił Alvaro Morata. Z kolei Lazio kończyło mecz w dziewiątkę po dwóch czerwonych kartkach i przegrało 1:4 z Eintrachtem Frankfurt.Arkadiusz Milik okradziony. Żartobliwy wpis polskiego piłkarza

Wyniki meczów 2. kolejki Ligi Europy:Anderlecht - Dinamo Zagrzeb 0:2 (0:1)Bayer Leverkusen - AEK Larnaka 4:2 (1:1)Bordeaux - FC Kopenhaga 1:2 (0:1)FC Zurich - Łudogorec Razgrad 1:0 (0:0)

Fenerbahce - Spartak Trnava 2:0 (0:0)Milan - Olympiakos Pireus 3:1 (0:1)Karabach - Arsenal 0:3 (0:1)Real Betis - Dudelange 3:0 (0:0)

Red Bull Salzburg - Celtic 3:1 (0:1)Rosenborg - RB Lipsk 1:3 (0:1)Worskla Połtawa - Sporting 1:2 (1:0)Zenit - Slavia 1:0 (0:0)

FC Astana - Rennes 2:0 (0:0)Apollon - Marsylia 2:2 (0:0)BATE Borysów - PAOK Saloniki 1:4 (0:3)Chelsea - MOL Vidi FC 1:0 (0:0)

Eintracht Frankfurt - Lazio 4:1 (2:1)Krasnodar - Sevilla 2:1 (0:1)Glasgow - Rapid 3:1 (1:1)FK Jablonec - Dynamo Kijów 2:2 (1:2)

Malmoe - Besiktas 2:0 (0:0)Sarpsborg - Genk 3:1 (1:0)Spartak Moskwa - Villarreal 3:3 (1:1)Standard Liege - Akhisarspor 2:1 (2:1)

Ludovic Obraniak zakończył karierę





