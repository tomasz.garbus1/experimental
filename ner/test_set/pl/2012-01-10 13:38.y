::{league:Puchar Anglii}. {player:Szczęsny}: Każdy wiedział, że {player:Henry} strzeli Piłka nożna - Sport.pl::
@2012-01-10 13:38


Sport.pl w mocno nieoficjalnej wersji... Polub nas na Facebooku »

Zobacz bramkę {player:Henry'ego} na Z czuba.tv »

"Czemu wszyscy tak się podniecają tym, że {player:Thierry} zdobył zwycięskiego gola?! Każdy wiedział, że to się stanie, tak czy inaczej" - napisał tuż po zakończeniu meczu na swoim twitterowym koncie reprezentant {country:Polski}.

Chwilę później dodał jednak wpis, z którego wcale nie wynika, iż ma takie chłodne podejście do tematu. "byłem tam wtedy, kiedy {player:Thierry Henry} zdobył zwycięskiego gola przeciwko {team:Leeds} w swoim powrocie do {team:AFC} ({team:Arsenal Football Club} - mariw)" - cieszył się {player:Szczęsny}.

{player:Thierry Henry}, najlepszy strzelec w historii {team:Arsenalu}, powrócił do klubu po czteroipółletniej przerwie. {team:Kanonierzy} wypożyczyli go na dwa miesiące z amerykańskiego {team:New York Red Bulls} - w lidze {league:MLS} trwa przerwa w rozgrywkach. W ponownym debiucie wszedł na boisko w 68. minucie meczu trzeciej rundy {league:Pucharu Anglii} przeciwko {team:Leeds} i zdobył jedynego gola meczu, dzięki któremu jego drużyna awansowała dalej.

Media w {country:Anglii} zachwycone ponownym debiutem {player:Thierry'ego Henry'ego} »

Hity zimowego okienka. Oni zmienią kluby?






