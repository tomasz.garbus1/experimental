::Primera Division. Saragossa na celowniku Messiego Piłka nożna - Sport.pl::
@2012-04-07 08:46
url: http://www.sport.pl/pilka/1,123132,11500328,Primera_Division__Saragossa_na_celowniku_Messiego.html


Zabawy z piłką - czytaj blog autorki »

Odległość z Barcelony do Saragossy wynosi 300 kilometrów, trasę pokonuję się w blisko trzy godziny, zdecydowanie więcej  dzieli oba zespoły w ligowej tabeli.  Blaugrana jest druga, a Saragossa trzecia, ale od końca, do drużyny z Katalonii traci 44 punkty.

Jeszcze jakiś czas Saragossa była głównym kandydatem do spadku, Tymczasem zawodnicy Manolo Jiméneza wydostali się z dna tabeli i choć wciąż znajdują się na miejscach zagrożonych spadkiem, to do siedemnastego Villarrealu brakuje im już tylko czterech punktów. Patrząc na znakomitą formę zespołu w ostatnich tygodniach, awans w górę tabeli wydaje się możliwy.

W ostatnich sześciu spotkaniach zespół z Aragonii odniósł cztery zwycięstwa, pokonując Atletico, Valencię, Sporting w trzech kolejnych meczach, a przegrywając zaledwie raz.  - Jeśli zagramy dobrze, możemy Barcelonę przestraszyć. Przy wsparciu kibiców możemy wygrać. - zapowiada Ivan Obradović, obrońca gospodarzy. A jego kolega z drużyny Franco Zuculini dodaje - Zwycięstwo z Barceloną nie jest niemożliwe.

Barcelona na stadion La Romareda jedzie jako faworyt. Choć na wyjeździe w tym sezonie nieraz zawodziła, po feralnej przegranej z Osasuną 3:2, kroczy od zwycięstwa do zwycięstwa. Jednak zawodnicy Pepa Guardioli doskonale zdają sobie sprawę, że drużyna z Saragossy nie ma nic do stracenia, walczy o życie i łatwo skóry nie sprzeda.


- To będzie bardzo ciężki mecz. - zapowiada Guardiola. Kataloński trener nie będzie mógł w sobotnim spotkaniu skorzystać z usług byłego zawodnika Saragossy - Gerarda Piqué, który doznał kontuzji we wtorkowym spotkaniu z Milanem.

Z pewnością na spotkanie z Aragończykami ręce zaciera Leo Messi, który z drużyny z Saragossy uczynił jeden ze swoich ulubionych celów. W jedenastu spotkaniach, które rozegrał przeciwko niej, strzelił dziewięć goli.

Choć katalońska prasa, co rusz przekonuje, że zwycięstwo w lidze jeszcze jest możliwe, Guardiola na przedmeczowej konferencji po raz kolejny podkreślił, że zdania nie zmienił, mistrzostwo Barcelona już straciła. Gdy ostatnio tak powiedział - Real stracił w lidze punkty.

Zabawy z piłką - czytaj blog autorki »






