::Liga Mistrzów. Jese poważnie kontuzjowany Piłka nożna - Sport.pl::
@2014-03-18 21:49
url: http://www.sport.pl/pilka/1,65041,15645676,Liga_Mistrzow__Jese_powaznie_kontuzjowany.html




Jese ucierpiał po agresywnym wejściu Kolasinicia w drugiej minucie meczu. 21-letni gracz Realu długo nie podnosił się z murawy, pomocy udzielali mu lekarze. Z boiska zniesiono go na noszach, dostał gromkie brawa od publiczności. Do szatni zszedł prezydent klubu Florentino Perez, by zapytać o zdrowie piłkarza.

Na razie wieści nie są optymistyczne, ale nie wiadomo też dokładnie, na ile poważny jest uraz prawego kolana. Jak twierdzi "AS", Jese został przewieziony już do jednej z klinik, gdzie ma przejść badanie rezonansem magnetycznym, które określi, co się stało. Jeśli okaże się, że zerwał więzadła w kolanie, czeka go kilka miesięcy leczenia.

Jese został zastąpiony przez Garetha Bale. W 21. minucie Walijczyk asystował przy golu Ronaldo. W 31. gola na 1:1 strzelił Hoogland. Po przerwie Real zdobył dwa gole w minutę. W 74. minucie bramkę strzelił Ronaldo, minutę później trafił Morata.

Udowodnij, że Z czuba się nie zna na piłce. Zostań naszym ekspertem »






