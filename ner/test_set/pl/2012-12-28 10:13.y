::Kolejny Węgier w {team:Kolejorzu}? {team:Lech} chce kupić obrońcę {player:Andrasa Dlusztusa} Piłka nożna - Sport.pl::
@2012-12-28 10:13


Zdaniem węgierskiego serwisu Rangado.hu, wysłannicy {team:Lecha Poznań} trzykrotnie oglądali tej jesieni mecze {team:Lombard Papa} np. w starciu z {team:Videotonem Szekesfehervar} i grę {player:Andrasa Dlusztusa}, po czym zdecydowali się podjąć negocjacje w sprawie pozyskania Węgra. Menedżer klubu {team:Lombard Papa}, {manager:Gabor Arki} mówi: - Mogę potwierdzić, że {team:Lech Poznań} poważnie zainteresował się naszym graczem.

Odmówił jednak odpowiedzi na pytanie, czy to znaczy, że {player:Andras Dlusztus} odchodzi do {country:Polski} i stanie się tej zimy graczem {team:Kolejorza}. - Jeżeli jednak Polacy będą chcieli go kupić, podjęliśmy decyzję, iż nie będziemy stawiali przeszkód. {player:Andras} ma zgodę na zagraniczny transfer - dodał.

Węgrzy cytują nawet wypowiedzi samego piłkarza, który przyznaje, że z ochotą podejmie wyzwanie gry w klubie zagranicznym. - Mam nadzieję, że kluby się dogadają - mówi.

Rynek węgierski i sam klub {team:Lombard Papa} są {team:Lechowi} dobrze znane. Stąd pochodzi {player:Gergo Lovrencsics}, który gra w barwach {team:Kolejorza}. Z ligi węgierskiej, choć z innego klubu ({team:TE Zalaegerszeg}) poznaniacy kupili także swego byłego super strzelca {player:Artjomsa Rudnevsa}.

{player:Andras Dlusztu} przyznaje, że rozmawiał z {player:Gergo Lovrencsicsem} na temat {team:Lecha} i ten jest zadowolony z gry w polskiej lidze i tym klubie.

24-letni obrońca pochodzi z Szeged na południu Węgier. Mierzy 188 cm wzrostu. Zaczynał karierę w malutkim klubie {team:Tisza Volan}. Do {player:Lombard Papa} trafił z {team:Soproni FC}. Ma na swym koncie także okres gry w lidze brazylijskiej.

Tak gra węgierski obrońca:








