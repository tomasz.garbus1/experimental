::Liga Europy. 'Napastnicy Odense muszą być bezwzględni' Piłka nożna - Sport.pl::
@2011-09-19 13:38
url: http://www.sport.pl/pilka/1,65042,10314679,Liga_Europy___Napastnicy_Odense_musza_byc_bezwzgledni_.html


Poprzednie rozgrywki dzisiejsi rywale zakończyli dwie pozycje na strefą spadkową. W tym spisują się lepiej i zajmują siódme miejsce. - Są groźni zwłaszcza u siebie. Walczą o każdą piłkę, więc nie możemy odpuszczać. Zagrają bardzo agresywnie i musimy odpłacić się tym samym. Ja i pozostali napastnicy powinniśmy być bezwzględni pod bramką - przekonuje Peter Utaka, atakujący Odense.

Strzelec drugiej bramki w meczu z Wisłą uważa, że jego drużyna może być spokojna o zwycięstwo, jeśli zagra podobnie jak w meczu w Krakowie. - Pewność siebie czerpiemy ze zwycięstw z Silkeborgiem w lidze i z mistrzami Polski w LE. Czeka nas trudny mecz, ale jesteśmy w dobrej formie i to sprawia, że wszystko jest prostsze - uważa reprezentant Nigerii.

Po meczach z soboty i niedzieli Odense spadło na szóste miejsce. W ośmiu kolejkach zgromadziło 14 punktów. Wciąż deklaruje walkę o mistrzostwo, ale do pierwszego FC Kopenhaga traci 11 punktów.






