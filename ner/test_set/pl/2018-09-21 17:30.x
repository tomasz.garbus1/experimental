::Florentino Perez zdradził kulisy odejścia Cristiano Ronaldo Piłka nożna - Sport.pl::
@2018-09-21 17:30
url: http://www.sport.pl/pilka/7,65082,23952463,florentino-perez-zdradzil-kulisy-odejscia-cristiano-ronaldo.html

W ostatnią niedzielę odbyło się zgromadzenie socios Realu Madryt. Florentino Perez był zmuszony do odpowiedzi na wiele trudnych pytań, w tym te o Cristiano Ronaldo. Odpowiedź na nie hiszpańskie media ujawniły w piątek.
REKLAMA




Podczas tego spotkania, prezes Realu Madryt ujawnił, dlaczego Królewscy zgodzili się obniżyć klauzulę rozwiązania Cristiano Ronaldo i dopuścić do późniejszej sprzedaży do Juventusu.- Klauzule stawione przez Real Madryt są odstraszające. My chcieliśmy jednak dojść do porozumienia. Wartość Ronaldo zaczęła spadać i nikt nie dałby więcej niż 100 milionów euro - mówił Perez.- Wierzymy, że to co się stało jest najlepsze dla obu stron. Ronaldo został też najdrożej sprzedanym piłkarzem w tym roku. Przed Madrytem nikt nie wydawał więcej niż 100 milionów euro. Trzeba też pamiętać, że Ronaldo chciał odejść ze względu na swoje sprawy osobiste - wyliczył Perez. Latem Cristiano Ronaldo trafił do Juventusu za około 117 milionów euro.

Piątek odniósł się do plotek związanych z BarcelonąBilety na reprezentację droższe niż na Irlandię Ondrej Duda: Po porażkach Legii czułem wstyd





