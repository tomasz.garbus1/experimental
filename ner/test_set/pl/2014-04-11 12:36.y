::{league:LM}. {team:Real} z {team:Bayernem}! {team:Atletico} z {team:Chelsea}! Piłka nożna - Sport.pl::
@2014-04-11 12:36


{team:Real} i {team:Bayern} to główni faworycie do wygrania {league:LM} w tym sezonie. Los sprawił, że zmierzą się ze sobą już w półfinale. Trener {team:Bayernu}, a były szkoleniowiec {team:Barcelony} {manager:Pep Guardiola} będzie kolejny raz miał okazje zmierzyć z {team:Królewskimi}.

Duże emocje wzbudza sprawa {player:Thibaut Courtoisa} w rywalizacji {team:Atletico} z {team:Chelsea}. Bramkarz jest wypożyczony do hiszpańskiego klubu z {team:The Blues} i w umowie między klubami istnieje zapis o tym, że zawodnik nie może grać przeciwko {team:The Blues}, chyba że Hiszpanie zapłacą za taki występ 3 miliony euro. O sprawie zrobiło się głośno w czwartek, w piątek UEFA wydała oświadczenie, że piłkarz będzie mógł zagrać bez względu na ustalenia międzyklubowe.

W Madrycie zdecydowanie odetchnęli. Jeszcze w czwartek byli pogodzeni ze stratą swojego kluczowego zawodnika. - Jeśli trafimy na {team:Chelsea}, {player:Courtois} nie zagra. Nie stać nas na to - mówił prezes {team:Atletico} {manager:Enrique Cerezo}.

- Wypożyczenie było zaaranżowane przed sezonem. {player:Courtois} będzie mógł jednak zagrać przeciwko nam. Nie ma co do tego wątpliwości - powiedział dyrektor generalny {team:Chelsea} Ron Gourlay

Pary 1/2 finału {league:Ligi Mistrzów}:

{team:Real Madryt} - {team:Bayern Monachium}

{team:Atletico Madryt} - {team:Chelsea Londyn}

Na pierwszym miejscu gospodarze pierwszych meczów. Odbędą się one 22-23 kwietnia, rewanże 29-30 kwietnia. Wielki finał 24 maja w Lizbonie.

W ćwierćfinałach {team:Bayern Monachium} okazał się lepszy od {team:Manchesteru United} (1:1, 3:1), {team:Atletico} od {team:Barcelony} (1:1, 1:0), {team:Chelsea} od {team:PSG} (1:3, 2:0), a {team:Real} od {team:Borussii} (3:0, 0:2).

Infografika: Przemysław Piotrowski






