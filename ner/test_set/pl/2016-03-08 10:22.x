::Ekstraklasa w Sport.pl. Adam Mójta - 'Najlepsza lewa noga w lidze' Piłka nożna - Sport.pl::
@2016-03-08 10:22
url: http://www.sport.pl/pilka/1,65039,19733927,ekstraklasa-w-sport-pl-adam-mojta-najlepsza-lewa-noga-w.html


Adam Mójta strzelił już cztery gole i zaliczył pięć asyst. Zdobył najwięcej bramek ze wszystkich lewych obrońców w ekstraklasie, a więcej asyst na swoim koncie ma tylko Patrik Mraz. Jakub Wawrzyniak, który ma realne szanse na wyjazd na Euro 2016 we Francji w tych rozgrywkach nie strzelił żadnego gola i zaliczył tylko trzy asysty. Zawodnik Podbeskidzia swoimi statystykami bije go na głowę.

Dla większości kibiców w Polsce jest jednak zawodnikiem anonimowym. Nigdy nie grał w wielkich klubach, prawdziwą szansę w ekstraklasie dostał w wieku 28 lat, znany jest głównie z internetowej zabawy w "Wygraj Ligę". Przed ostatnią ligową kolejką lewego obrońcę Podbeskidzia kupiło 2029 graczy. Prawie co czwarty zawodnik ma Mójtę w swoim składzie. Wszystko dzięki jego ofensywnej grze i świetnie ułożonej lewej nodze, która dostarcza w "Wygraj Ligę" pokaźną liczbę punktów.

Skąd więc bierze się tak ofensywna gra 29-letniego obrońcy? - Całe życie grałem na lewej obronie, ale od początku ciągnęło mnie do przodu. Zawsze starałem się pomagać swoim drużynom w ofensywie. Nie wszystkim trenerom się to podobało, ale postawiłem na swoim i teraz zbieram tego efekty - mówi Mójta w rozmowie ze Sport.pl.

Trudne początki u Kubickiego


Do Bielska-Białej przychodził ratować sytuację po nieudanym transferze Roberta Mazana. Podbeskidzie wydało na młodzieżowego reprezentanta Słowacji 150 tysięcy euro (najwięcej w historii klubu) i było przekonane, że lewą stronę obrony ma zabezpieczoną na lata. Jednak Mazan w Polsce kompletnie zawiódł, postanowiono więc dać szansę sprowadzonemu latem Mójcie.

Już w pierwszym meczu z Zagłębiem Lubin wywalczył miejsce w podstawowej jedenastce. Nie zagrał źle, a bielszczanie przywieźli z trudnego terenu jeden punkt. Kolejny mecz i wyjazd na Łazienkowską, gdzie Podbeskidzie do przerwy przegrywało z Legią 0:3. Cała drużyna z Bielska-Białej grała katastrofalnie, ale to właśnie Mójta został kozłem ofiarnym tej katastrofy. Trener Dariusz Kubicki odsunął go od występów w ekstraklasie i lewy obrońca musiał grać w trzecioligowych rezerwach.

Podoliński uwierzył


Na ekstraklasowe boiska wrócił w siódmej kolejce i z miejsca stał się jedną z ważniejszych postaci w Podbeskidziu. Imponował w ofensywie, a coraz większe zrozumienie z kolegami z defensywy zaowocowało mniejszą liczbą traconych bramek. Jego najlepszy czas w karierze zaczął się dokładnie w momencie przyjścia do Bielska-Białej nowego trenera. Nieprzypadkowo swojego pierwszego gola Mójta zdobył właśnie w ekstraklasowym debiucie Roberta Podolińskiego w Podbeskidziu.

- Trener nie musiał mnie namawiać do akcji ofensywnych. Ma do mnie pełne zaufanie. Oczywiście nie mogę zapominać o grze obronnej, ale jak tylko pojawia się szansa, to mam jak najczęściej włączać się do ataku - mówi 29-latek.

Przypadek? Nie sądzę


- Cieszę się ze swojej skuteczności, bo wszystkie bramki, które zdobyłem w tym sezonie były po przemyślanych strzałach. W żadnej sytuacji nie chciałem dośrodkowywać. W meczu z Górnikiem Łęczną widziałem, że w polu karnym nie było żadnego kolegi i strzeliłem z ostrego kąta. Bramkarz pomógł i wpadło. Z Jagiellonią i Górnikiem Zabrze też chciałem strzelać - zaznacza Mójta.

Te słowa to odpowiedź na zarzuty kierowane w stronę lewego obrońcy Podbeskidzia. Część kibiców i ekspertów zarzucało Mójcie, że jego nieprawdopodobne uderzenia z lewej nogi nie były zamierzone. Tym bardziej, że jego bramki wpadały w kluczowych momentach i pieczętowały zwycięstwa Podbeskidzia. Kibicom nie mieściło się w głowie, że lewy obrońca w ostatniej minucie meczu rusza z całych sił do przodu, a nie skupia się na defensywie. Gra na czas i wybijanie piłki po autach? - To nie w moim stylu - zapewnia Mójta.

W Czechach nie miał za co żyć


O 29-letnim obrońcy długo mówiło się, że ekstraklasa nie jest dla niego. Próbował przebić się w Koronie i Odrze Wodzisław - bezskutecznie. Zniechęcony postanowił wyjechać do Czech, gdzie w barwach Viktorii Żiżkov zagrał w 23 meczach. To właśnie w Czechach poczuł, że jego kariera zaczyna zmierzać w dobrym kierunku, ale z gry w drugiej lidze nie był w stanie się utrzymać w Pradze.

- Czechy to świetne miejsce do życia. Żiżkov to dzielnica Pragi i do starego rynku szło się jedynie 10 minut. To był jeden z najlepszych okresów w mojej przygodzie z piłką. Zagrałem 23 spotkania i miałem 17 asyst. Zostałem wybrany do jedenastki ligi, ale nie mogłem zostać tam dłużej. Przez 7 miesięcy nie dostawałem pensji i brakowało mi środków do życia - wspomina.

Mójta w Polsce znów zderzył się ze ścianą. Na prawie trzy lata utknął w pierwszej lidze, a poważną szansę dostał dopiero w Bełchatowie. - Nie żałuję, że tak późno trafiłem do ekstraklasy. Kiedy zaczynałem swoją przygodę z piłką kluby w najwyższej lidze nie stawiały na młodych zawodników. Musiałem szukać swojej szansy w niższych ligach, może też po prostu byłem za słaby - zastanawia się Mójta.

Środowisko docenia


Czesław Michniewicz, trener Pogoni Szczecin napisał niedawno na Twitterze, że Adam Mójta ma najbardziej wartościową lewą nogę w lidze. Patrząc na statystki piłkarza Podbeskidzia, trudno się z tym stwierdzeniem nie zgodzić. Słowa Michniewicza nie są jednak odosobnione. Jakub Rzeźniczak zapytany o zawodnika miesiąca wskazał swojego kolegę z zespołu - Michała Kucharczyka i właśnie Mójtę.

@_Ekstraklasa_ dla mnie albo Kuchy albo Adam Mójta.— Jakub Rzeźniczak (@JakubRzezniczak) 4 marca 2016

Dobrą formę lewego obrońcy Podbeskidzia docenili również dziennikarze, a porównanie go do Leightona Bainesa z Evertonu nie mogło być przypadkowe.

Adam Mójta strzela z pozycji, z których inni boją się podawać #PODGKŁ— Krótka Piłka (@krotkapilkatv) 1 marca 2016

Podbeskidzie - Górnik Łęczna pobudza wyobraźnię. - Adam Mójta to taki polski Leighton Baines - rzucił przed momentem ktoś w redakcji :)— Dominik Piechota (@dominikpiechota) 1 marca 2016

Nie będzie narzekał na brak ofert


Kontrakt Adama Mójty z Podbeskidziem kończy się w czerwcu, ale umowa po rozegraniu odpowiedniej liczby meczów może zostać przedłużona o kolejny rok. To nie przeszkadza jednak innym klubom, które w lecie chciałyby sprowadzić lewego obrońcę Podbeskidzia.

- W ostatnim czasie zrobiło się głośno o mojej grze, ale muszę zachować spokój. Teraz skupiam się na walce dla Podbeskidzia. Do wszystkiego doszedłem ciężką pracą, nikt mi niczego za darmo nie dał. Wiem, że tylko dobrą grą mogę zrobić kolejny krok do przodu - kończy Mójta.

Obserwuj @dwardzich22






