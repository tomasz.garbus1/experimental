::Polska - Litwa 4:0. Robert Lewandowski z dwoma golami, trafili też Dawid Kownacki i Jakub Błaszczykowski Reprezentacja Polski::
@2018-06-12 19:50
url: http://www.sport.pl/pilka/7,65037,23530874,polska-litwa-4-0-robert-lewandowski-z-dwoma-golami-trafili.html

Polska zaczęła spotkanie w ofensywnym ustawieniu. Adam Nawałka postawił aż na trzech nominalnych napastników: Dawida Kownackiego, Roberta Lewandowskiego i Arkadiusza Milika.
REKLAMA




Początek nie był jednak najlepszy w wykonaniu Polaków. Darvydas Sernas strzelił gola dla Litwy, ale był na spalonym - polska obrona założyła dobrą pułapkę ofsajdową. Polacy przyspieszyli po kilkunastu minutach. Pierwszą groźną akcję przeprowadzili w 17. minucie. Jacek Góralski dobrze zagrał do Bartosza Bereszyńskiego, ale jego podanie do Arkadiusza Milika zostało przechwycone przez litewskiego obrońcę.Dwa ciosy LewandowskiegoDwie minuty później Polacy już prowadzili po pięknej zespołowej akcji. Dawid Kownacki dośrodkował w pole karne, Maciej Rybus zgrał piłkę do Roberta Lewandowskiego, który z zimną krwią umieścił piłkę w bramce.

Lewandowski drugi cios wymierzył w 33. minucie. Z rzutu wolnego uderzył pod poprzeczkę, piłka odbiła się za linią bramkową i wypadła z powrotem w boisko. Arbiter po konsultacji z sędzią VAR uznał gola.Jeszcze przed przerwą dwie dobre okazje miał Kownacki. Najpierw z bliskiej odległości trafił w bramkarza po świetnej akcji Bereszyńskiego, a po chwili minimalnie chybił uderzeniem z ponad 30 metrów. W 43. minucie świetnie po strzale Darvydasa Sernasa interweniował Łukasz Fabiański.Bramkarz Swansea nie wyszedł na drugą połowę, został zmieniony przez Wojciecha Szczęsnego. Na boisko weszli również Łukasz Piszczek i Łukasz Teodorczyk, a nieco później Michał Pazdan.Groźny wślizg - Darvydas Sernas

Groźnie wyglądająca sytuacja miała miejsce w 53. minucie. Darvydas Sernas wyciął Bartosza Bereszyńskiego "równo z trawą", ale za swój wślizg dostał jedynie żółtą kartkę. Kilka chwil później litewski napastnik zagroził bramce Polaków, ale nie zdołał pokonać Szczęsnego.Polacy cieszyli się z prowadzenia 3:0 przez kilka chwil w 67. minucie. Maciej Rybus dośrodkował z rzutu rożnego, Thiago Cionek zgrał piłkę głową, a Grzegorz Krychowiak trafił piętą do siatki. Jednak tuż przed strzałem zagrał ręką, przez co sędzia nie uznał gola.Pierwszy gol KownackiegoCztery minuty później było już 3:0. Łukasz Teodorczyk znakomicie podał do Bereszyńskiego, który wyłożył piłkę Kownackiemu jak na tacy - był to pierwszy gol w jego drugim występie w dorosłej kadrze.

Błaszczykowski wykorzystał karnegoW 82. minucie Polska dostała rzut karny. Obrońca Litwy zagrał piłkę ręką przy dośrodkowaniu Jakuba Błaszczykowskiego, co sędzia zauważył po weryfikacji wideo. Skrzydłowy reprezentacji Polski zamienił "jedenastkę" na gola! Był to jego pierwszy karny od feralnej sytuacji z meczu z Portugalią w ćwierćfinale Euro 2016 (1:1, 3:5 w karnych, Błaszczykowski jako jedyny z Polaków nie trafił). Błaszczykowski pojawił się na boisku w 70. minucie przy ogromnej euforii kibiców na Stadionie Narodowym.W 88. minucie w niezłej sytuacji w bramkę nie trafił Arkadiusz Milik, który uderzył tuż obok słupka. Na kilkanaście sekund przed końcem kibice wstali z miejsc i zaczęli skandować "dziękujemy!", po czym sędzia zakończył spotkanie.Polska rozegra pierwszy mecz na MŚ 2018 19 czerwca z Senegalem. Kolejne spotkania z Kolumbią (24 czerwca) i Japonią (28 czerwca).

Zobacz podstawowy skład polski na mecz Polska - Litwa





