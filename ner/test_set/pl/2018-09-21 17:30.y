::{manager:Florentino Perez} zdradził kulisy odejścia {player:Cristiano Ronaldo} Piłka nożna - Sport.pl::
@2018-09-21 17:30

W ostatnią niedzielę odbyło się zgromadzenie socios {team:Realu Madryt}. {manager:Florentino Perez} był zmuszony do odpowiedzi na wiele trudnych pytań, w tym te o {player:Cristiano Ronaldo}. Odpowiedź na nie hiszpańskie media ujawniły w piątek.
REKLAMA




Podczas tego spotkania, prezes {team:Realu Madryt} ujawnił, dlaczego {team:Królewscy} zgodzili się obniżyć klauzulę rozwiązania {player:Cristiano Ronaldo} i dopuścić do późniejszej sprzedaży do {team:Juventusu}.- Klauzule stawione przez {team:Real Madryt} są odstraszające. My chcieliśmy jednak dojść do porozumienia. Wartość {player:Ronaldo} zaczęła spadać i nikt nie dałby więcej niż 100 milionów euro - mówił {manager:Perez}.- Wierzymy, że to co się stało jest najlepsze dla obu stron. {player:Ronaldo} został też najdrożej sprzedanym piłkarzem w tym roku. Przed {team:Madrytem} nikt nie wydawał więcej niż 100 milionów euro. Trzeba też pamiętać, że {player:Ronaldo} chciał odejść ze względu na swoje sprawy osobiste - wyliczył {manager:Perez}. Latem {player:Cristiano Ronaldo} trafił do {team:Juventusu} za około 117 milionów euro.

{player:Piątek} odniósł się do plotek związanych z {team:Barceloną} Bilety na reprezentację droższe niż na {country:Irlandię Ondrej} {player:Duda}: Po porażkach {team:Legii} czułem wstyd





