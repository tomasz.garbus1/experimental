::Sytuacja zdrowotna w {team:Pogoni} jest dobra Piłka nożna - Sport.pl::
@2013-07-02 10:26


{team:Portowcy} mają już za sobą dwa tygodnie przygotowań i 4 sparingi. Wydaje się, że najtrudniejszy okres przygotowań za nimi. Nad siłą pracowali w Szczecinie. Od niedzieli są na obozie w Gniewinie i tam, według zapewnień trenera {manager:Dariusza Wdowczyka}, mają się koncentrować na zajęciach czysto piłkarskich - technika, taktyka, skuteczność.

Na obóz nie pojechał brazylijski obrońca {player:Hernani}, który leczy w Szczecinie naderwany mięsień czworogłowy. Uraz wyłączy z gry {player:Hernaniego} na 2-4 tygodnie. Inauguracja sezonu (19 lipca pogoniarze zagrają w Lubinie) raczej bez niego.

Pozostali piłkarze - zdrowi.

Po pierwszych (w poprzednią środę) sparingach kontuzję kostki w stawie skokowym zgłosił {player:Tomasz Chałas}, ale...

- Od poniedziałku trenuje na pełnych obciążeniach - mówi Dariusz Dalke, fizjoterapeuta {team:Pogoni}.

We wtorkowym spotkaniu sparingowym z {team:Lechią Gdańsk} może zabraknąć {player:Radosława Wiśniewskiego}, który pauzował w sobotnich meczach ze {team:Światowidem Łobez} i {team:Jagiellonią Białystok}. Skrzydłowy narzekał na tzw. przywodziciela.

- Nie jest to jakaś poważna sprawa, ale trzeba zachować ostrożność. Za dzień lub dwa {player:Radek} będzie normalnie trenował - stwierdza Dalke.






