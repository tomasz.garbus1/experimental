::{player:Smolarek}: {team:Lech} może się nas bać::
@2011-02-19 09:07


Akcje z boisk piłkarskiej {league:Ekstraklasy} zobaczysz na {league:Ekstraklasa}.tv »

- Ciężko trenowałem, w sparingach strzelałem bramki, jestem zadowolony. Na pewno teraz będzie mi łatwiej, bo koledzy z zespołu już mnie poznali - mówi napastnik {team:Polonii}. Dla jego zespołu niedzielne spotkanie będzie pierwszym w tej rundzie meczem o stawkę. Lech ma już za sobą pierwszy mecz z {team:Bragą} w {league:Lidze Europejskiej}.

{player:Smolarek} liczy na to, że {team:Theo Bos}, nowy szkoleniowiec {team:Czarnych Koszul}, zaufa mu bardziej niż jego poprzednicy. - Mogę strzelić gola zarówno w pierwszej, jak i w ostatniej minucie. Nie zawsze miałem tą możliwość. W {team:Dortmundzie} czy w {team:Racingu} bywało, że zdobywałem bramki w ostatnich minutach, bo trenerzy mi ufali i dawali grać do końca - przypomina były reprezentant {country:Polski}.

Wychowanek {team:Feyenoordu Rotterdam} podpowiada także, na jakiej pozycji najbardziej lubi występować. - Najlepiej czuję się w "16", tam najłatwiej mi strzelać gole. Ale jak gram na szpicy, to potrzebuję dobrych podań. Poza tym rywale wiedzą, co potrafię, będą mnie kryli we dwóch - zastrzega polonista.

Napastnik {team:Czarnych Koszul} czeka już na rywalizację z {team:Lechem Poznań} w ćwierćfinale {league:Pucharu Polski}. - Liczy się nie tylko niedzielny mecz, bo trzeba zagrać dwa dobre spotkania. Zrobimy wszystko, by awansować dalej. {team:Lech} może się nas bać - zapowiada.

{player:Smolarek}, jak na najlepiej zarabiającego zawodnika ligi, jak dotąd prezentował się słabo. W rundzie jesiennej {league:Ekstraklasy} wystąpił w dwunastu spotkaniach, zdobywając w nich tylko trzy bramki. Taki dorobek nie satysfakcjonuje właściciela warszawskiego klubu Józefa Wojciechowskiego, który  publicznie krytykował piłkarza.

{team:Lech} - {team:Polonia} w pierwszym ćwierćfinale {league:Pucharu Polski} ».






