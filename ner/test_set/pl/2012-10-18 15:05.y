::{league:Premier League}. Koniec skandalu z {player:Terrym}, nie będzie odwołania. Piłka nożna - Sport.pl::
@2012-10-18 15:05


Czy uważasz, że kara dla Terry'ego jest zbyt surowa? Podyskutuj o tym na Facebooku! »

Kapitan {team:Chelsea}, który będzie musiał opuścić cztery najbliższe mecze "{team:The Blues}" (w tym dwa mecze z wiceliderem {league:Premier League}, {team:Manchesterem United}). {player:Terry} mimo długich bojów z angielską federacją ostatecznie zdecydował się przeprosić za swoje postępowanie. - Chciałbym przeprosić wszystkich za to, jak zachowałem się w trakcie tamtego spotkania - przyznał. Decyzja Angielskiej Federacji dotyczy incydentu z meczu przeciwko {team:QPR}, który miał miejsce 23 października zeszłego roku.

Specjalne oświadczenie w tej sprawie wydał także klub. - {player:John Terry} podjął właściwą decyzję godząc się z werdyktem FA. {team:Chelsea} jako klub także wyraża żal z powodu języka użytego przez {player:Terry'ego} i wyraża nadzieje, że takie zachowania, które są niegodne zawodnika naszego klubu, więcej nie będą się powtarzać - czytamy w oświadczeniu.

31-latek oprócz zawieszenia będzie musiał jeszcze zapłacić 220 tysięcy funtów kary. - Jestem rozczarowany tym, że otrzymałem taką karę, ale akceptuje ją i zdaje sobie sprawę, że było to nieakceptowalne. W dalszym ciągu będę starał się robić wszystko, by dbać o dobre imię klubu - twierdzi {player:Terry}.

W związku z całą aferą obrońca podjął decyzję o zakończeniu kariery reprezentacyjnej.

Czy kara dla {player:Terry'ego} jest zbyt surowa? Zapraszamy do dyskusji w komentarzach!







