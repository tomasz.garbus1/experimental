::El. {league:MŚ 2018}. {country:Kazachstan} - {country:Polska} 2:2. Czas odbudować Reprezentacja {country:Polski}::
@2016-09-04 20:35


Zastanawialiśmy się, co zobaczymy po tej reprezentacji w eliminacjach do {league:mistrzostw świata}. Bo piłkarze pozmieniali kluby na lepsze, dostali podwyżkę i stali się narodowymi ulubieńcami. I przez czterdzieści pięć minut meczu w Astanie wszystko układało się niemal idealnie. Pełnej dominacji Polaków nie były w stanie przykryć nawet dwie sytuacje Kazachów, bo przecież znacznie bliżej było kolejnych goli Polaków.

I naprawdę wszystko, co polskiemu kibicowi powinno zostać w głowie po tym spotkaniu, to dobra akcja {player:Łukasza Piszczka}, {player:Roberta Lewandowskiego} i {player:Jakuba Błaszczykowskiego}, którą golem zakończył {player:Bartosz Kapustka}. Albo to, jak na luzie i na najwyższej skali trudności biało-czerwoni rozgrywali nad głowami rywali, choć dwukrotnie tych koronkowych akcji nie potrafił skończyć {player:Arkadiusz Milik}.

A zostaną zbędne przepychanki z Kazachami, którzy nawet przegrywając dwoma bramkami starali się sprowokować Polaków. Udało im się, gdy zareagował {player:Lewandowski} i łokciem zaatakował twarz agresywnie pilnującego go rywala, a później mocniej niż powinien przytrzymywał w chwycie kolejnego. {player:Kamil Glik}, który nogą odepchnął przeciwnika, gdy ten zdaniem obrońcy zbyt długo leżał na murawie.

Nerwowość liderów udzieliła się innym - {player:Maciej Rybus} i {player:Bartosz Salamon} zamiast na interwencjach skupili się na apelowaniu do sędziego o faule i spalone. Pokazał to pierwszy kwadrans drugiej połowy - ciosy przeciwnika spadły na twarz boksera, bo ten opuścił gardę. Poczuł się tak pewnie, zaczął się może popisywać i ambitny rywal odpowiedział. A przecież miał już leżeć na deskach.

"Wyjechać, wygrać, wrócić" - taki w skrócie był plan na {country:Kazachstan}. Nawet reprezentanci cieszyli się, że na początku eliminacji będą mogli odbębnić podróż do Astany, do której tydzień przygotowywali się w spokoju. padło słów o ambicjach, celach i odnajdywaniu się w roli faworytów. To już wiemy, że faworyci od tej łatki się odbili, że trzeba więcej. Do sprowadzenia na ziemię wystarczył kwadrans dobrej gry bardzo przeciętnej reprezentacji i 27 fauli. Ale nie oczekujmy, że teraz rywale będą inaczej postępować z gwiazdami {team:Bayernu}, {team:Napoli}, {team:PSG} i {team:Leicester City}.

Prztyczek na początku przyda się, ale w tej wyrównanej grupie eliminacyjnej awans rozstrzygnąć się może głównie pewnością w wygrywaniu u siebie, a pierwsze miejsce już na pewno punktami zdobytymi na trudnych wyjazdach. Tym bardziej pozostaje żal, że trzy "oczka" z Astany były już pakowane do walizki ze sprzętem, jednym okiem patrzyliśmy na inne wyniki. Tak zamiast odlotu jest myśl, że trzeba będzie zrealizować trudniejszą misję wygrania w Bukareszcie lub Kopenhadze.

Uczucie jest dziwne, bo wcześniej taka zapaść była mało charakterystyczna dla drużyny {manager:Adama Nawałki}. Może jedynie w meczu ze {country:Szkocją} w Glasgow (2:2), gdy długi fragment dobrej gry przesłoniły dwa gole rywali i pozycję w grupie trzeba było ratować w ostatnich sekundach meczu. Ale wtedy nie było tak wielu momentów w których Polacy wydawali się po prostu wytrąceni z równowagi. Rozkojarzeni, szukający rewanżu we wślizgu lub szarpnięciu, a nie zagraniu. Kiedy już zdali sobie sprawę, że trzeba w piłkę po prostu grać, to było już za późno. A wtedy i tak wkurzony {player:Glik} uderzył z czterdziestu metrów na wiwat. Po co?

To pytanie zdominuje dyskusję po tym spotkaniu. I tak samo w kwestiach o indywidualnych błędach obrońców, i zachowaniu polskich piłkarzy. Rosnące ciśnienie stonuje {manager:Adam Nawałka}, bo kto obserwuje działania selekcjonera ten wie, że nie ma selekcjonera, który częściej mówiłby o aspekcie mentalnym. On nie będzie rozliczał, ale podniesie w tej kwestii wymagania na kolejnych zgrupowaniach. Może będzie mniej śmiechu, mniej luzu - on wie, jak przykręcić śrubę. Może nie będzie nawet potrzebował tego zrobić, bo piłkarze także mają swoją świadomość i to na światowym poziomie.

Może najważniejsze na koniec - ta drużyna już zasłużyła sobie na zaufanie i jeden remis tego nie powinien zmienić. Oni sami będą wiedzieli, że coś nie zadziałało, coś zawiodło, ale po ostatnich dwóch latach wypada wierzyć, że - tak jak to zwykle jest u {manager:Nawałki} - piłkarze odbudują.

{player:Milik} utrzymał formę z {league:Euro}! {player:Pazdan} zablokował sam siebie [MEMY]






