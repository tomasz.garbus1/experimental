::{player:Kasper Hämäläinen} będzie na zgrupowaniu z {team:Lechem Poznań} czy z reprezentacją kraju? Piłka nożna - Sport.pl::
@2013-01-31 05:45


Po podpisaniu umowy z {team:Lechem} {player:Kasper Hämäläinen} opuścił Poznań. Wrócił do {country:Szwecji} i ma dołączyć do drużyny {manager:Mariusza Rumaka} w piątek, przed zaplanowanym na ten weekend wylotem na zgrupowanie w hiszpańskiej Esteponie. Nie wiadomo jednak, czy nowy pomocnik będzie z {team:Lechem} przez całe zgrupowanie. Dostał bowiem powołanie na towarzyski mecz z {country:Izraelem} i być może będzie najpierw musiał spełnić obowiązki reprezentanta kraju, a dopiero potem stawi się w {country:Hiszpanii}. To powołanie przyszło wtedy, gdy {player:Kasper Hämäläinen} był jeszcze zawodnikiem {team:Djurgaardens Sztokholm}. "{team:Kolejorz}", planując transfer, wiedział o tym, że Fin ma obowiązki reprezentacyjne.






