::Syn byłego prezesa {team:Barcelony} w {team:Arsenalu} Piłka nożna - Sport.pl::
@2011-09-23 12:38


{manager:Joan Laporta} był prezesem "{team:Katalończyków}" przez siedem lat, w lipcu 2010 r. zastąpił go {manager:Sandro Rossell}. Za czasów {manager:Laporty} rozpoczęła się wojna transferowa między {team:Barceloną}, a {team:Arsenalem}. - Tylko w tym sezonie zainwestowaliśmy w zespół juniorów 7 mln euro, ale mamy ogromny problem z Anglikami, którzy podbierają nam wychowanków - mówił w maju prezes {team:Barcelony}. - Wcześniej wykorzystywali ligę francuską, teraz celują w nas. Nie mają własnego systemu szkolenia, więc szukają ratunku gdzie indziej. I oferują 14- lub 15-latkom astronomiczne zarobki, których my ze względów etycznych nie możemy zaproponować - wyjaśnił {manager:Laporta}.

Zaczęło się osiem lat temu, gdy {player:Fábregas}, jeden z najzdolniejszych uczniów barcelońskiej szkółki, wyjechał do Londynu. Później dołączył do niego {player:Fran Mérida}. Dziś w szkółce {team:Arsenalu} trzech piłkarzy, którzy futbolowego elementarza uczyli się w Katalonii. Sprowadzeni wiosną 16-letni {player:Hector Bellerin} i {player:Jon Toral} kosztowali 400 i 350 tys. euro. Temu ostatniemu {manager:Wenger} miał obiecać debiut w pierwszej drużynie już w sezonie 2012/13. Hiszpanie szacują, że w Londynie obaj dostali nawet 35 razy większą pensję niż w {team:Barcelonie}. Także dlatego, że angielskie przepisy pozwalają na podpisywanie profesjonalnych kontraktów już z 16-latkami, a hiszpańskie - wyłącznie z dorosłymi.

{manager:Laporta} sprowadził na {stadium:Camp Nou} piłkarzy {team:Arsenalu} {player:Alaksandra Hleba} i {player:Thierry'ego Henry'ego}. Starał się też o {player:Fabregasa}, ale wykupić 24-letniego rozgrywającego udało się dopiero {manager:Rossellowi}.






