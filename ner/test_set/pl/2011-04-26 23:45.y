::{league:Liga Mistrzów}. {player:Edwin van der Sar}: {player:Manuel Neuer} to klasa światowa Piłka nożna - Sport.pl::
@2011-04-26 23:45


Zobacz gole na Zczuba.tv »

- Wiedzieliśmy, że to nie będzie łatwe. Ale dwubramkowe zwycięstwo to dla nas doskonały wynik. {player:Manuel Neuer} zagrał fantastyczny mecz. To naprawdę bramkarz światowej klasy - powiedział po meczu {player:van der Sar}.

Nieco zawiedziony był po spotkaniu napastnik {team:Manchesteru} {player:Wayne Rooney}. - Mogliśmy strzelić więcej goli. Ale zagraliśmy bardzo dobrze. Musimy bardzo profesjonalnie podejść do kolejnego meczu. Nie wolno lekceważyć {team:Schalke}. Pojechali do Mediolanu i ograli {team:Inter} 5:2 - przypomina {player:Rooney}.

- Czułem, ze jeśli będziemy stwarzali okazje, to gole w końcu padną - powiedział {player:Ryan Giggs}, 37-letni skrzydłowy {team:MU}, który jest najstarszym strzelcem gola w historii {league:Ligi Mistrzów}. - Do przerwy mogliśmy prowadzić czterema, albo pięcioma golami. Przed meczem wynik 2:0 wzięlibyśmy w ciemno, ale teraz jestem takim wynikiem trochę rozczarowany - powiedział {player:Walijczyk}.

Przeczytaj o meczu {team:Schalke} - {team:Manchester} »






