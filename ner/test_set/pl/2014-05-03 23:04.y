::{manager:Dariusz Wdowczyk}: Byliśmy tylko tłem dla {team:Wisły} Piłka nożna - Sport.pl::
@2014-05-03 23:04


Pomeczowe opinie szkoleniowców.

{manager:Franciszek Smuda}, trener {team:Wisły}:


Najważniejsze, że przełamaliśmy złą passę. Już wcześniej powtarzałem, że przy tylu brakach niełatwo nam zwyciężać. Ta wygrana dla wszystkich jest dodatkową motywacją przed kolejnym spotkaniem. {player:Tomasz Frankowski} kiedyś też miał taki okres, że nie trafiał, a później worek rozwiązał się i strzelał regularnie. Tak samo z jest z {player:Pawłem Brożkiem} [w sobotę zaliczył hat tricka - red.]. Ani chwili nie wahałem się, by po słabszych występach posadzić go na ławce, bo on... jest sam w linii napadu. Modliłem się jedynie do Boga, aby był zdrowy.

{manager:Dariusz Wdowczyk}, trener {team:Pogoni}:


Kubeł zimnej wody na nasze głowy. Gratuluję trenerowi {manager:Smudzie}, bo wygrał bardzo zasłużenie. Nic nam się nie układało. {team:Wisła} grała bardzo dobrze, byliśmy tłem dla rywala. To pierwszy taki przypadek w 32 kolejkach. To nie zdarzyło nam się w tym sezonie ani razu. {team:Wisła} była bardzo dobrze dysponowana, my bardzo słabo. Popełniliśmy masę błędów w defensywie. Być może nie popełniliśmy ich tyle w ciągu całego sezonu. Ale to nie tylko obrona, bo graliśmy słabo całą drużyną. Dla nas to bardzo bolesne, ale musimy przeanalizować fakty, wyciągnąć wnioski i przygotować się do następnego meczu.






