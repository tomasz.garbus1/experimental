"""
Produces prompts for all articles in provided directory and produces a histogram
of cardinalities of entity sets.
"""
import argparse
import os

import matplotlib.pyplot as plt
from tqdm import tqdm

from common.utils import load_all_articles
from ner.highlighter import Annotator

input_dir = None
dataset_name = None
MAX_SIZE = 100


def parse_args():
    global input_dir, dataset_name
    parser = argparse.ArgumentParser(prog='Plot prompt set cardinalities')
    parser.add_argument('-i', action='store', required=True,
                        type=str)
    parser.add_argument('-n', action='store', required=True,
                        type=str)
    args = parser.parse_args()
    input_dir = args.i
    dataset_name = args.n


def get_lengths() -> list:
    articles = load_all_articles(input_dir)
    annotator = Annotator(mode='default', language='en')
    print('Annotating articles')
    for article in tqdm(articles):
        annotator.add_art(article)
    lengths = []
    for _, entities in annotator.data:
        lengths.append(len(entities))
    cache_lengths(lengths)
    return lengths


def cache_lengths(lengths):
    with open('ner/cache/entity_set_sizes_%s.txt' % dataset_name, 'w+') as file:
        file.write(','.join(map(str, lengths)))


def load_lengths_from_cache():
    fname = 'ner/cache/entity_set_sizes_%s.txt' % dataset_name
    if not os.path.isfile(fname):
        return None
    with open(fname, 'r') as file:
        return list(map(int, file.read().strip().split(',')))


def main():
    lengths = load_lengths_from_cache() or get_lengths()
    plt.hist(lengths, bins=MAX_SIZE + 1, range=(0, MAX_SIZE))
    plt.show()


if __name__ == '__main__':
    parse_args()
    main()
