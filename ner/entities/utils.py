import re

from trans import trans


def cleanup_name(name: str) -> str:
    """
    Cleans up a person's or team's name from unnecessary characters like
    parentheses and whitespace.
    """
    name = name.strip()
    name = re.sub('\([^\(\)]*\)', '', name)
    name = re.sub('\[[^\[\]]*\]', '', name)
    for _ in range(5):
        name = re.sub('\n', '', name)
        name = re.sub('\r', '', name)
        name = name.strip()
        name = re.sub('  ', ' ', name)
        name = re.sub('  ', ' ', name)
        name = re.sub('  ', ' ', name)
        name = re.sub('  ', ' ', name)
        name = re.sub('  ', ' ', name)
    name = trans(name)
    return name