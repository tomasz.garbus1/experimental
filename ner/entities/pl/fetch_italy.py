"""
Scrapes Polish Wikipedia for Italian clubs and players.
"""

from ner.entities.pl.utils import get_country_from_wiki

START_LINK = 'https://pl.wikipedia.org/wiki/Serie_A'
SHORT_NAME = 'italy'


if __name__ == '__main__':
    get_country_from_wiki(START_LINK, SHORT_NAME)
