"""
Scrapes Polish Wikipedia for PD football clubs but uses Polish declension.
"""
from ner.entities.pl.utils import get_country_from_wiki

START_LINK = 'https://pl.wikipedia.org/wiki/Primera_Divisi%C3%B3n'
SHORT_NAME = 'primiera_division'


if __name__ == '__main__':
    get_country_from_wiki(START_LINK, SHORT_NAME)
