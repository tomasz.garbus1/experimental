import re
from typing import Optional

import requests
from bs4 import BeautifulSoup

from ner.entities.pl.declension import declense_name
from ner.entities.utils import cleanup_name


def is_wiki_site_about_club(soup: BeautifulSoup) -> bool:
    """
    Determines whether a Wikipedia site provided in |soup| is about a football
    club."
    """
    return True


def team_name_variants(name: str) -> str:
    """
    Returns team name variants separated by ';'.
    """
    variants = [name]

    # Copied from English version.
    if '&' in name:
        variants.append(re.sub('&', 'and', name))
    if name.endswith(' United') or name.endswith(' United F.C.'):
        variants.append('United')
        variants.append(name[:name.find(' United')])
        variants.append(re.sub('United', 'Utd', name))
        variants.append(re.sub('United F.C.', 'Utd', name))
    if name.endswith(' City') or name.endswith(' City F.C.'):
        variants.append('City')
        variants.append(name[:name.find(' City')])
    if name.endswith(' Town') or name.endswith(' Town F.C.'):
        variants.append('Town')
        variants.append(name[:name.find(' Town')])
    if name.endswith(' Athletic') or name.endswith(' Athletic F.C.'):
        variants.append('Athletic')
        variants.append(name[:name.find(' Athletic')])
    if name.endswith(' F.C.'):
        variants.append(name[:name.find(' F.C.')])
    if name.endswith(' A.F.C.'):
        variants.append(name[:name.find(' A.F.C.')])
    if 'Rovers' in name:
        variants.append('Rovers')
        variants.append('the Rovers')
        variants.append('The Rovers')
    if 'Rangers' in name:
        variants.append('Rangers')
        variants.append('the Rangers')
        variants.append('The Rangers')
    variants = list(filter(lambda s: s != '', variants))

    if name.endswith('x'):
        variants.append(name[:-1] + 'ks')
    if 'x ' in name:
        variants.append(re.sub(r'x$', 'ks ', name))
    if 'Real ' in name:
        variants.append('Real')
    if 'Atletico ' in name:
        variants.append('Atletico')
    if name.split(' ')[0].isalnum() and len(name.split(' ')[0]) >= 4:
        variants.append(name.split(' ')[0])
    variants = list(filter(lambda s: s != '', variants))

    variants = list(filter(is_team_name_variant_ok, variants))
    return declense_all_variants(variants)


def get_infobox_field_with_link(soup: BeautifulSoup, name: str) -> (
        Optional[str], Optional[str]):
    """
    Gets the content of an infobox field denoted by |name|. If it contains a
    link, it fetches also the URL, otherwise the link is None.
    """
    if not soup.find('table', class_='infobox vcard'):
        return None, None
    for tr in soup.find('table', class_='infobox vcard').find_all('tr'):
        th = tr.find('th')
        if th is not None and th.text.strip() == name:
            td = tr.find('td')
            if td.find('a'):
                a = td.find('a')
                href = a['href']
                if href.startswith('/wiki'):
                    href = 'https://en.wikipedia.org' + href
            else:
                href = None
            return td.text.strip(), href
    return None, None


def get_infobox_field(soup: BeautifulSoup, name: str) -> Optional[str]:
    """
    Gets the content of an infobox field denoted by |name|.
    """
    return get_infobox_field_with_link(soup, name)[0]


def nickname_variants(name: str) -> str:
    """
    Team's nickname variants.
    """
    return ';'.join(declense_name(name))


def clean_up_stadium_name(name: str) -> str:
    if ',' in name:
        name = name.split(',')[0]
    name = cleanup_name(name)
    return name


def declense_all_variants(variants: list) -> str:
    variants = list(filter(lambda s: s != '', variants))

    declensed_variants = []

    for variant in variants:
        for dec in declense_name(variant):
            declensed_variants.append(dec)
    return ';'.join(declensed_variants)


def stadium_name_variants(name: str) -> str:
    """
    Produces a list of stadium name variants, separated by a semicolon.
    """
    name = clean_up_stadium_name(name)
    variants = [name]
    if name.endswith(' Stadium'):
        variants.append(name[:name.find(' Stadium')])
    if name.endswith(' Arena'):
        variants.append(name[:name.find(' Arena')])
    if name.endswith(' Park'):
        variants.append(name[:name.find(' Park')])
    variants = list(filter(lambda s: s != '', variants))

    variants = list(filter(is_stadium_name_variant_ok, variants))
    return declense_all_variants(variants)


def is_stadium_name_variant_ok(name: str) -> bool:
    blacklist = ['Sport']
    if name not in blacklist:
        return True
    return False


def is_team_name_variant_ok(name: str) -> bool:
    blacklist = ['Sport', 'Kim', 'Bundesliga niemiecka w pilce noznej',
                 'Bundesliga', 'Europa', 'Europy']
    if name not in blacklist:
        return True
    return False


def is_person_name_variant_ok(name: str) -> bool:
    blacklist = ['Premier', 'Canal', 'Ale', 'Polak']
    if name not in blacklist:
        return True
    return False


def person_name_variants(name: str) -> str:
    """
    Produces a list of person's name variants, separated by a semicolon.
    """
    variants = [name]

    if len(name.split(' ')) == 2:
        first_name, last_name = name.split(' ')
        variants.append('%s [%s]' % (first_name, last_name))

    if 'Jakub ' in name:
        variants.append(re.sub('Jakub', 'Kuba', name))

    # Add last name to variants if it's long enough.
    if len(name.split(' ')) > 1:
        last_name = name.split(' ')[-1]
        if len(last_name) >= 5:
            variants.append(last_name)
            variants.append(name[0] + '. ' + last_name)
            variants.append(name[0] + ' ' + last_name)

    variants = list(filter(is_person_name_variant_ok, variants))
    return declense_all_variants(variants)


def get_persons_nationality(link: str) -> Optional[str]:
    """
    Given a link to an article about a person (player, coach...), tries to
    fetch their nationality (precisely name of the country they were born in).
    """
    try:
        soup = BeautifulSoup(requests.get(link).text, 'html.parser')
        for br in soup.find_all("br"):
            br.replace_with("\n")
    except requests.exceptions.MissingSchema:
        return None
    field = get_infobox_field(soup, 'Place of birth')
    if field is None:
        return None
    return field.split(',')[-1].strip()


def natio_to_variants(natio: str) -> str:
    return ''


def get_club_from_wiki_page(link: str, team_names: set, player_names: set,
                            stadiums: set, managers: set):
    """
    Scrapes a football club's Wikipedia page and appends team name and players
    to global sets.
    """
    try:
        soup = BeautifulSoup(requests.get(link).text, 'html.parser')
        for br in soup.find_all("br"):
            br.replace_with("\n")
    except requests.exceptions.MissingSchema:
        return

    if not is_wiki_site_about_club(soup):
        return

    # Fetch team name.
    team_name = soup.find('h1', class_='firstHeading').text
    team_name = cleanup_name(team_name)
    team_name_short = team_name
    team_name = team_name_variants(team_name)

    # Add team's nicknames to name variants.
    nicknames_node = soup.find('td', class_='nickname')
    short_names_text = get_infobox_field(soup, 'Short name') or ''
    nicknames_text = nicknames_node.text if nicknames_node else ''
    for nickname in re.split('[,\n/]|\sor\s', nicknames_text + ',' +
                                              short_names_text):
        team_name = team_name + ';' + nickname_variants(
            cleanup_name(nickname))

    # Fetch ground.
    stadium = get_infobox_field(soup, 'Ground')
    if stadium:
        stadiums.add(stadium_name_variants(stadium))

    # Fetch manager.
    manager, manager_link = get_infobox_field_with_link(soup, 'Head coach')
    if (manager, manager_link) == (None, None):
        manager, manager_link = get_infobox_field_with_link(soup, 'Manager')
    if manager:
        def add_manager(man: str, man_link: Optional[str]):
            man = cleanup_name(man)
            manager_variants = person_name_variants(man)
            # Not sure if helpful but creates some additional team name variants
            # based on manager's name.
            # last_name = man.split(' ')[-1]
            # if len(last_name) >= 5:
            #     team_name = team_name + ';' + last_name + '\'s side'
            # Tries to add some more variants for manager based on their nationality.
            if man_link:
                natio = get_persons_nationality(man_link)
                manager_variants += natio_to_variants(natio)
            managers.add(manager_variants)
        if '&' in manager:
            for manager in manager.split('&'):
                add_manager(manager, None)
        else:
            add_manager(manager, manager_link)
    team_names.add(team_name)

    # List players.
    player_rows = soup.find_all('tr', class_='vcard agent')
    players = []
    for player in player_rows:
        name = player.find_all('td')[-1].text.strip()
        name = cleanup_name(name)
        name = person_name_variants(name)
        players.append(name)
        player_names.add(name + ',' + team_name_short + ',')
    if players:
        print(team_name, players)


def get_country_from_wiki(start_link: str, short_name: str):
    team_names = set()
    player_names = set()
    stadiums = set()
    managers = set()

    main_art = requests.get(start_link).text
    soup = BeautifulSoup(main_art, 'html.parser')
    for mwhd in soup.find_all('span', class_='mw-headline'):
        print(mwhd.text)
    for tab in soup.find_all('table', class_='wikitable'):
        for tr in tab.find_all('tr'):
            td = tr.find('td')
            try:
                a = td.find('a')
                href = a['href']
                if href.startswith('/wiki'):
                    href = 'https://en.wikipedia.org' + href
            except (AttributeError, TypeError):
                continue
            get_club_from_wiki_page(href, team_names, player_names,
                                    stadiums, managers)

    with open('ner/entities/pl/team/%s.csv' % short_name, 'w+') as file:
        file.write('\n'.join(team_names))
    with open('ner/entities/pl/player/%s.csv' % short_name, 'w+') as file:
        file.write('\n'.join(player_names))
    with open('ner/entities/pl/stadium/%s.csv' % short_name, 'w+') as file:
        file.write('\n'.join(stadiums))
    with open('ner/entities/pl/manager/%s.csv' % short_name, 'w+') as file:
        file.write('\n'.join(managers))
