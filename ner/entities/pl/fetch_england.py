"""
Scrapes Wikipedia for English football clubs but uses Polish declension.
"""
from ner.entities.pl.utils import get_country_from_wiki

START_LINK = 'https://en.wikipedia.org/wiki/List_of_football_clubs_in_England'
SHORT_NAME = 'eng'


if __name__ == '__main__':
    get_country_from_wiki(START_LINK, SHORT_NAME)
