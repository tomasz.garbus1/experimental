"""
Declenses all leagues in ner/entities/en/league/____.csv and writes them to
analogous file in 'pl' directory.
"""
from ner.entities.pl.declension import declense_name


def run(fname: str):
    leagues = set()
    with open('ner/entities/en/league/%s.csv' % fname, 'r') as file:
        lines = file.read().split('\n')
        file.close()
    for league in lines:
        variants = ';'.join(declense_name(league))
        leagues.add(variants)
    with open('ner/entities/pl/league/%s_en.csv' % fname, 'w+') as file:
        file.write('\n'.join(leagues))


def run2(src: str, dest: str):
    leagues = set()
    with open(src, 'r') as file:
        lines = file.read().split('\n')
        file.close()
    for league in lines:
        variants = ';'.join(declense_name(league))
        leagues.add(variants)
    with open(dest, 'w+') as file:
        file.write('\n'.join(leagues))


if __name__ == '__main__':
    run('rapid')
    run('manual')
    run2('ner/entities/pl/league/manual_undeclensed.csv',
         'ner/entities/pl/league/manual_declensed.csv')
