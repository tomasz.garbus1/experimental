import os
from tqdm import tqdm
import random
from trans import trans
from ner.highlighter import Highlighter


DATA_DIR = '../data/sportpl'
CAPITALS ='QWERTYUIOPASDFGHJKLZXCVBNM'


if __name__ == '__main__':
    hl = Highlighter(language='pl')
    dirs = os.listdir(DATA_DIR)
    random.shuffle(dirs)

    words = {}

    for dir in tqdm(dirs):
        if not os.path.isdir(os.path.join(DATA_DIR, dir)):
            continue
        for fname in os.listdir(os.path.join(DATA_DIR, dir))[:1]:
            fpath = os.path.join(DATA_DIR, dir, fname)
            with open(fpath, 'r') as file:
                content = file.read()
            content = trans(content)
            for word in content.split(' '):
                if word != '' and word[0].isalpha() and word[0] in CAPITALS:
                    if word not in hl.trie:
                        if word not in words:
                            words[word] = 0
                        words[word] += 1

    most_common = []
    for k, v in words.items():
        most_common.append((v, k))
    most_common.sort(reverse=True)
    with open('most_common.tmp', 'w+') as file:
        for elem in most_common:
            file.write(str(elem[0]) + ',' + elem[1] + '\n')

