"""
Lists all team and player from footballsquads.co.uk.
Prerequsite:
Execute the following command in this directory
wget -r http://www.footballsquads.co.uk/ www.footballsquads.co.uk
"""
import os
import re

from bs4 import BeautifulSoup
from tqdm import tqdm
from trans import trans

from ner.entities.pl.utils import team_name_variants, person_name_variants, natio_to_variants
from ner.entities.utils import cleanup_name


DOMAIN = 'www.footballsquads.co.uk'
MAIN_DIR = os.path.join('ner', DOMAIN)
NATIO_MAIN_DIR = os.path.join('ner', DOMAIN, 'national')

team_names = set()
player_names = set()
managers = set()


def process_file(path: str, season: str):
    try:
        with open(path, 'r') as file:
            content = trans(file.read())
    except UnicodeDecodeError:
        with open(path, 'rb') as file:
            bytes_content = file.read()
            content = trans(bytes_content.decode('iso-8859-1'))
    soup = BeautifulSoup(content, 'html.parser')

    # EXTRACT TEAM
    title_node = soup.find('title')
    if title_node is None:
        return
    team_name = ' - '.join(title_node.text.split(' - ')[1:-1])
    team_name = trans(team_name)
    team_name = cleanup_name(team_name)
    team_names.add(team_name_variants(team_name))

    # EXTRACT MANAGER
    div_main_node = soup.find('div', {'id': 'main'})
    if div_main_node is None:
        return
    if len(div_main_node.find_all('h3')) < 2:
        return
    dmn_txt = div_main_node.find_all('h3')[0].text
    if dmn_txt.find('Manager:') == -1:
        dmn_txt = div_main_node.find_all('h3')[1].text
    if dmn_txt.find('Manager:') != -1:
        manager = dmn_txt[dmn_txt.find('Manager:') + len('Manager:'):]

        def add_manager(man: str):
            natio = man[-4:-1]
            man = re.sub('\[[A-Z][A-Z][A-Z]\]', '', man)
            man = cleanup_name(man)
            man = person_name_variants(man)
            man += natio_to_variants(natio)
            if man != '' and len(man) >= 9:
                managers.add(man + ',' + team_name)

        for manager in manager.split('&'):
            add_manager(manager)

    # EXTRACT PLAYERS
    for player_row in soup.find_all('tr'):
        tds = player_row.find_all('td')
        if len(tds) < 9:
            continue
        name = tds[1].text.strip()
        natio = tds[2].text.strip()
        name = cleanup_name(name)
        name = person_name_variants(name)
        name += natio_to_variants(natio)
        if name != '' and len(name) >= 9:
            player_names.add(name + ',' + team_name + ',' + season)


def run_from_dir(dir: str, not_natio=True):
    for country in tqdm(os.listdir(dir)):
        path = os.path.join(dir, country)
        if not os.path.isdir(path) or country == 'images':
            continue
        for season in os.listdir(path):
            is_latest_season = (season in ['2019', '2019-2020'])
            path = os.path.join(dir, country, season)
            if not os.path.isdir(path):
                continue
            if not_natio:
                for league in os.listdir(path):
                    path = os.path.join(dir, country, season, league)
                    if not os.path.isdir(path):
                        continue
                    for team in os.listdir(path):
                        path = os.path.join(dir, country, season, league, team)
                        process_file(path, season)
            else:
                for team in os.listdir(path):
                    path = os.path.join(dir, country, season, team)
                    process_file(path, season[-4:])


if __name__ == '__main__':
    run_from_dir(NATIO_MAIN_DIR, False)
    run_from_dir(MAIN_DIR)
    print(len(team_names), len(player_names))
    with open('ner/entities/pl/team/footballsquads.csv', 'w+') as file:
        file.write('\n'.join(team_names))
    with open('ner/entities/pl/player/footballsquads.csv', 'w+') as file:
        file.write('\n'.join(player_names))
    with open('ner/entities/pl/manager/footballsquads.csv', 'w+') as file:
        file.write('\n'.join(managers))
