"""
Scrapes Polish Wikipedia for Ligue 1 clubs and players.
"""
from ner.entities.pl.utils import get_country_from_wiki

START_LINK = 'https://pl.wikipedia.org/wiki/Ligue_1'
SHORT_NAME = 'france'


if __name__ == '__main__':
    get_country_from_wiki(START_LINK, SHORT_NAME)
