from bs4 import BeautifulSoup
import requests
from ner.entities.pl.declension import declense_name
from ner.entities.utils import cleanup_name


if __name__ == '__main__':
    countries = set()
    link = 'https://pl.wikipedia.org/wiki/Pa%C5%84stwa_%C5%9Bwiata'
    soup = BeautifulSoup(requests.get(link).text, 'html.parser')
    for table in soup.find_all('table',
                               {'class': ['wikitable', 'sortable']}):
        for tr in table.find('tbody').find_all('tr'):
            try:
                td = tr.find_all('td')[1]
                country = td.text
                country = cleanup_name(country)
                countries.add(';'.join(declense_name(country)))
            except IndexError:
                continue
    with open('ner/entities/pl/country/wikipedia.csv', 'w+') as file:
        file.write('\n'.join(countries))