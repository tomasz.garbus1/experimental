"""
Scrapes Polish Wikipedia for Bundesliga clubs and players.
"""
from ner.entities.pl.utils import get_country_from_wiki

START_LINK = 'https://pl.wikipedia.org/wiki/Bundesliga_niemiecka_w_pi%C5%82ce_no%C5%BCnej'
SHORT_NAME = 'bundesliga'


if __name__ == '__main__':
    get_country_from_wiki(START_LINK, SHORT_NAME)
