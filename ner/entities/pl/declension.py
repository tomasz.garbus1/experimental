from typing import List
import re
import itertools

CONSONANTS = 'qwrtpsdfghjklzxcvbnm'
VOWELS = 'eyuioa'


def declense_word(word: str) -> List[str]:
    """
    A very simple and erroneous heuristic for declension of Polish nouns.
    Returns a list of variants.
    """
    # words that should not be declensed
    DECLENSION_BLACKLIST = [
        'The', 'the', 'FC', 'F.C', 'AFC', 'A.F.C.', 'C.F.', 'CF', 'of', 'and',
        'Town', 'City', 'United', '&', 'of', 'La', 'el', 'van', 'bin'
    ]
    if word in DECLENSION_BLACKLIST:
        return [word]
    variants = []
    if word.endswith('iga'):
        variants.append(re.sub(r'iga$', 'idze', word))
        variants.append(re.sub(r'iga$', 'igi', word))
        variants.append(re.sub(r'iga$', 'ige', word))
    if word.endswith('ski'):
        variants.append(re.sub(r'ski$', 'skiego', word))
        variants.append(re.sub(r'ski$', 'skiemu', word))
        variants.append(re.sub(r'ski$', 'skim', word))
    if word.endswith('cki'):
        variants.append(re.sub(r'cki$', 'ckiego', word))
        variants.append(re.sub(r'cki$', 'ckiemu', word))
        variants.append(re.sub(r'cki$', 'ckim', word))
    if word.endswith('y'):
        variants.append(re.sub(r'y$', r'ego', word))
        variants.append(re.sub(r'y$', r'emu', word))
        variants.append(re.sub(r'y$', r'ym', word))
    if word.endswith('wel'):
        variants.append(re.sub(r'wel$', 'wla', word))
        variants.append(re.sub(r'wel$', 'wlowi', word))
    if word.endswith('ia'):
        variants.append(re.sub(r'ia$', 'ii', word))
        variants.append(re.sub(r'ia$', 'ie', word))
    if word.endswith('ek'):
        variants.append(re.sub(r'ek$', 'ku', word))
        variants.append(re.sub(r'ek$', 'ka', word))
        variants.append(re.sub(r'ek$', 'kiem', word))
        variants.append(re.sub(r'ek$', 'kowi', word))
    if word.endswith('a'):
        variants.append(re.sub(r'a$', 'i', word))
        variants.append(re.sub(r'a$', 'ie', word))
        variants.append(re.sub(r'a$', 'y', word))
    if word.endswith('ko'):
        variants.append(re.sub(r'ko$', 'ki', word))
        variants.append(re.sub(r'ko$', 'ce', word))
    if word.endswith('ka'):
        variants.append(re.sub(r'ka$', 'ki', word))
        variants.append(re.sub(r'ka$', 'ce', word))
        variants.append(re.sub(r'ka$', 'kiej', word))
    if word.endswith('lli'):
        variants.append(re.sub(r'lli$', 'llim', word))
        variants.append(re.sub(r'lli$', 'lliego', word))
        variants.append(re.sub(r'lli$', 'lliemu', word))
    if word.endswith('n'):
        variants.append(re.sub(r'n$', 'nie', word))
    if word.endswith('t'):
        variants.append(re.sub(r't$', 'cie', word))
    if word.endswith('en'):
        variants.append(re.sub(r'en$', 'nu', word))
        variants.append(re.sub(r'en$', 'nowi', word))
        variants.append(re.sub(r'en$', 'nem', word))
    if word.endswith('es'):
        variants.append(re.sub(r'es$', 'su', word))
        variants.append(re.sub(r'es$', 'sa', word))
        variants.append(re.sub(r'es$', 'sem', word))
    if word.endswith('ec'):
        variants.append(re.sub(r'ec$', 'cu', word))
        variants.append(re.sub(r'ec$', 'ca', word))
        variants.append(re.sub(r'ec$', 'cowi', word))
        variants.append(re.sub(r'ec$', 'cem', word))
    if word != '' and word[-1] in CONSONANTS:
        variants.append(word + 'owi')
        variants.append(word + 'em')
        variants.append(word + 'a')
        variants.append(word + 'u')
        variants.append(word + 'e')
    if word.endswith('r'):
        variants.append(re.sub(r'r$', 'rze', word))
    if word.endswith('ja'):
        variants.append('i')
    if word != '' and word[-1] in VOWELS:
        variants.append(word + '\'owi')
        variants.append(word + '\'a')
    if word == 'Pogon':
        variants.append('Pogoni')
        variants.append('Pogonia')
    if word == 'Zaglebie':
        variants.append('Zaglebia')
        variants.append('Zaglebiem')
        variants.append('Zaglebiu')

    variants = [word] + list(set(variants))
    return variants


def declense_name(name: str) -> List[str]:
    """
    Creates a list of name declensions.
    """
    parts = name.split(' ')
    if len(parts) >= 4:
        # To reduce the output volume a bit.
        return [name]
    parts_declensed = list(map(declense_word, parts))
    variants = []
    for variant in itertools.product(*parts_declensed):
        variants.append(' '.join(variant))

    variants = set(variants)
    variants.remove(name)
    variants = [name] + list(variants)

    return variants
