"""
Scrapes Polish Wikipedia for Ekstraklasa clubs and players.
"""
from ner.entities.pl.utils import get_country_from_wiki

START_LINK = 'https://pl.wikipedia.org/wiki/Ekstraklasa_w_pi%C5%82ce_no%C5%BCnej_(2018/2019)'
SHORT_NAME = 'ekstraklasa'


if __name__ == '__main__':
    get_country_from_wiki(START_LINK, SHORT_NAME)
