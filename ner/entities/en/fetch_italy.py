"""
Scrapes Wikipedia for Italian clubs and players.
"""

from ner.entities.en.utils import get_country_from_wiki

START_LINK = 'https://en.wikipedia.org/wiki/2019%E2%80%9320_Serie_A#League_table'
SHORT_NAME = 'italy'


if __name__ == '__main__':
    get_country_from_wiki(START_LINK, SHORT_NAME)
