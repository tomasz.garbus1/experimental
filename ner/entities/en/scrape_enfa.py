import os
import time

import requests
from bs4 import BeautifulSoup
from tqdm import tqdm

from ner.entities.en.utils import person_name_variants, cleanup_name

players = set()
managers = set()

CLUBS = {
    'AFC Bournemouth': 5,
    'Arsenal': 10,
    'Aston Villa': 12,
    'Brighton and Hove Albion': 25,
    'Burnley': 28,
    'Chelsea': 39,
    'Crystal Palace': 49,
    'Everton': 56,
    'Leicester City': 75,
    'Liverpool': 78,
    'Manchester City': 83,
    'Manchester United': 84,
    'Newcastle Unted': 95,
    'Norwich City': 101,
    'Sheffield United': 122,
    'Southampton': 127,
    'Tottenham Hotspur': 139,
    'Watford': 142,
    'West Ham United': 144,
    'Wolverhamption Wanderers': 148,
}

SEASONS = [2010, 2012, 2013, 2015, 2016, 2017, 2018]

if __name__ == '__main__':
    assert os.path.isdir('ner/entities')
    os.makedirs('ner/cache/enfa_raw', exist_ok=True)
    for season in SEASONS:
        for club, club_no in tqdm(CLUBS.items()):
            for div in [3, 4]:
                suf = 'team=%d&season=%d&div=%d' % (club_no, season, div)
                fname = 'ner/cache/enfa_raw/' + suf
                if os.path.isfile(fname):
                    pass
                else:
                    addr = 'http://enfa.co.uk/team.php?' + suf
                    content = requests.get(addr).text
                    with open(fname, 'w+') as file:
                        file.write(content)
                    time.sleep(1)

                with open(fname, 'r+') as file:
                    content = file.read()
                soup = BeautifulSoup(content, 'html.parser')
                if div == 3:
                    for a in soup.find('div', attrs={'id': '7'}).find_all('a'):
                        name = a.text
                        if ',' in name:
                            name = name.split(',')
                            name = name[1] + ' ' + name[0]
                        name = cleanup_name(name)
                        name = person_name_variants(name)
                        players.add(name + ',' + club + ',' + str(season))
                if div == 4:
                    for a in soup.find('div', attrs={'id': '8'}).find_all('a'):
                        name = a.text
                        if ',' in name:
                            name = name.split(',')
                            name = name[1] + ' ' + name[0]
                        name = cleanup_name(name)
                        name = person_name_variants(name)
                        managers.add(name + ',' + club)
    with open('ner/entities/player/enfa.csv', 'w+') as file:
        file.write('\n'.join(players))
    with open('ner/entities/manager/enfa.csv', 'w+') as file:
        file.write('\n'.join(managers))
