"""
Scrapes Wikipedia for Scottish clubs and players.
"""

from ner.entities.en.utils import get_country_from_wiki

START_LINK = 'https://en.wikipedia.org/wiki/List_of_Scottish_Professional_Football_League_clubs'
SHORT_NAME = 'scot'


if __name__ == '__main__':
    get_country_from_wiki(START_LINK, SHORT_NAME)

