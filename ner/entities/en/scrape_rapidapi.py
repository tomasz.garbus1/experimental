import os
import json
import requests
from tqdm import tqdm

from ner.entities.en.utils import stadium_name_variants, person_name_variants, is_person_name_variant_ok, \
    team_name_variants
from ner.entities.utils import cleanup_name


HEADERS = {
        'x-rapidapi-host': "api-football-v1.p.rapidapi.com",
        'x-rapidapi-key': "yENsMFb7G2mshjMhpploV5dQUhuKp1Rreu4jsnQNRL6kObFkqc"
        }


class RapidScraper:
    def __init__(self):
        self.leagues = {}
        self.teams = {}
        self.players = {}
        self.coachs = {}
        self.stadiums = set()

    def run(self):
        self.fetch_leagues()
        self.fetch_teams()
        self.fetch_players()
        self.fetch_coachs()

    def fetch_leagues(self):
        print('Fetching leagues')
        fname = 'ner/cache/rapid/leagues.json'
        if os.path.isfile(fname):
            pass
        else:
            url = "https://api-football-v1.p.rapidapi.com/v2/leagues"
            response = requests.request("GET", url, headers=HEADERS).json()
            with open(fname, 'w+') as file:
                json.dump(response, file)
        with open(fname, 'r+') as file:
            response = json.load(file)
        for league in response['api']['leagues']:
            self.leagues[league['league_id']] = league

    def fetch_teams(self):
        print('Fetching teams')
        for league_id in tqdm(self.leagues):
            fname = 'ner/cache/rapid/teams_%d.json' % league_id
            if os.path.isfile(fname):
                pass
            else:
                url = ("https://api-football-v1.p.rapidapi.com/v2/teams/league/%d" % league_id)
                response = requests.request("GET", url, headers=HEADERS).json()
                with open(fname, 'w+') as file:
                    json.dump(response, file)
            with open(fname, 'r+') as file:
                response = json.load(file)
            teams = response['api']['teams']
            for team in teams:
                self.teams[team['team_id']] = team
                if team['venue_name'] is not None:
                    self.stadiums.add(team['venue_name'])

    def fetch_players(self):
        print('Fetching players')
        for team_id in tqdm(self.teams):
            for season in [2017, 2018, 2019]:
                fname = 'ner/cache/rapid/players_%d_%d.json' % (team_id, season)
                if os.path.isfile(fname):
                    pass
                else:
                    url = "https://api-football-v1.p.rapidapi.com/v2/players/team/%d/%d" % (team_id, season)
                    response = requests.request("GET", url, headers=HEADERS).json()
                    with open(fname, 'w+') as file:
                        json.dump(response, file)
                with open(fname, 'r+') as file:
                    response = json.load(file)
                players = response['api']['players']
                for player in players:
                    self.players[player['player_id']] = player

    def _process_persons(self, persons):
        persons = map(lambda p: (p['name'], p), persons)
        persons = map(lambda p: (cleanup_name(p[0]), p[1]), persons)
        persons = filter(lambda p: p[0] != '', persons)
        persons = filter(lambda p: ';' not in p[0], persons)
        persons = filter(lambda p: ',' not in p[0], persons)
        persons = filter(lambda p: len(p[0]) >= 6, persons)
        persons = filter(lambda p: is_person_name_variant_ok(p[0]), persons)
        persons = map(lambda p: (person_name_variants(p[0]), p[1]), persons)
        persons = list(persons)
        names = list(map(lambda p: p[0], persons))
        for i, p in enumerate(persons):
            if p[1]['nationality'] == 'Scotland':
                names[i] += ';the Scot;The Scot'
        names = map(lambda p: p + ',,', names)
        names = list(set(names))
        names = sorted(names, key=len)
        return names

    def fetch_coachs(self):
        print('Fetching coachs')
        for team_id in tqdm(self.teams):
            fname = 'ner/cache/rapid/coachs_%d.json' % team_id
            if os.path.isfile(fname):
                pass
            else:
                url = "https://api-football-v1.p.rapidapi.com/v2/coachs/team/%d" % team_id
                response = requests.request("GET", url, headers=HEADERS).json()
                with open(fname, 'w+') as file:
                    json.dump(response, file)
            with open(fname, 'r+') as file:
                response = json.load(file)
            coachs = response['api']['coachs']
            for coach in coachs:
                self.coachs[coach['id']] = coach

    def save_entities(self):
        with open('ner/entities/en/league/rapid.csv', 'w+') as file:
            file.write('\n'.join(map(lambda l: l['name'],
                                     self.leagues.values())))
        with open('ner/entities/en/team/rapid.csv', 'w+') as file:
            non_countries = filter(lambda t: t['name'] != t['country'],
                                   self.teams.values())
            team_names = map(lambda t: t['name'], non_countries)
            team_names = map(team_name_variants, team_names)
            file.write('\n'.join(team_names))
        with open('ner/entities/en/stadium/rapid.csv', 'w+') as file:
            stadiums = map(stadium_name_variants, self.stadiums)
            file.write('\n'.join(stadiums))
        with open('ner/entities/en/player/rapid.csv', 'w+') as file:
            for p in self.players.values():
                p['name'] = p['player_name']
            players = self._process_persons(self.players.values())
            file.write('\n'.join(players))
        with open('ner/entities/en/manager/rapid.csv', 'w+') as file:
            managers = self._process_persons(self.coachs.values())
            file.write('\n'.join(managers))


if __name__ == '__main__':
    assert os.path.isdir('ner/cache')
    os.makedirs('ner/cache/rapid', exist_ok=True)
    scraper = RapidScraper()
    scraper.run()
    scraper.save_entities()
    print(scraper.teams)
    print(scraper.stadiums)
    print(scraper.players)
    print(scraper.coachs)
    print(len(scraper.teams))
    print(len(scraper.stadiums))
    print(len(scraper.players))
    print(len(scraper.coachs))
