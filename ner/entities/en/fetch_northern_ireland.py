"""
Scrapes Wikipedia for Nothern Irish clubs and players.
"""

from ner.entities.en.utils import get_country_from_wiki

START_LINK = 'https://en.wikipedia.org/wiki/List_of_association_football_clubs_in_Northern_Ireland'
SHORT_NAME = 'nor_irl'


if __name__ == '__main__':
    get_country_from_wiki(START_LINK, SHORT_NAME)

