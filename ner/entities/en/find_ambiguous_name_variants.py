import os

from ner.highlighter import CATEGORIES


def load_category(cat_name: str, d: dict):
    for fname in os.listdir('ner/entities/%s' % cat_name):
        with open('ner/entities/%s/%s' % (cat_name, fname), 'r+') as file:
            content = file.read()
            for line in content.split('\n'):
                name_variants = line.split(',')[0].split(';')
                for var in name_variants:
                    if var not in d:
                        d[var] = set()
                    d[var].add(cat_name)


if __name__ == '__main__':
    d = dict()
    for cat in CATEGORIES:
        load_category(cat, d)
    amb = dict()
    for k in d:
        if len(d[k]) > 1:
            amb[k] = d[k]
    print(len(amb))
    for k in amb:
        if 'team' in amb[k]:
            print(k, amb[k])
