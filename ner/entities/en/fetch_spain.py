"""
Scrapes Wikipedia for Spanish clubs and players.
"""

from ner.entities.en.utils import get_country_from_wiki

START_LINK = 'https://en.wikipedia.org/wiki/La_Liga#Clubs'
SHORT_NAME = 'spain'


if __name__ == '__main__':
    get_country_from_wiki(START_LINK, SHORT_NAME)

