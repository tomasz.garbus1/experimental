import os
import pickle
import re
from collections import defaultdict
from typing import Optional

from tqdm import tqdm

from ner.highlighter import CATEGORIES, Annotator


def entity_set_to_prompt(itemset: set) -> str:
    cat_sets = defaultdict(list)
    for elem in itemset:
        cat_sets[elem[1]].append(elem[0])
    cats = ['league', 'stadium', 'manager', 'player', 'team', 'country']
    out = ''
    for cat in cats:
        out += cat + ':' + ','.join(cat_sets[cat]) + '\n'
    return out


def to_spacy_format(y: str):
    """
    Transforms a text from the manually labelled training set into a
    SpaCy-format article with marked entities.
    :param y: A manually labelled article.
    :return: A pair <original article, entities dictionary>.
    """
    cats = '(' + '|'.join(map(lambda cat: '{%s[^{}]*}' % cat, CATEGORIES)) + ')'
    restored = ''
    entities = []
    y_chunks = re.split(cats, y)
    for chunk in y_chunks:
        matched_cat = False
        for cat in CATEGORIES:
            if chunk.startswith('{' + cat + ':') and chunk.endswith('}'):
                matched_cat = True
                val = chunk[len(cat) + 2:-1]
                entities.append((len(restored),
                                 len(restored) + len(val),
                                 cat))
                restored += val
        if not matched_cat:
            restored += chunk
    return restored, {'entities': entities}


def prec_rec_f1_score(p: set, y: set, cat: Optional[str] = None) -> (
        float, float, float):
    """
    Measures the accuracy of set |p| with regard to |y| and returns 3 metrics:
    * precision
    * recall
    * F1-score
    :param p: predictions
    :param y: ground truths
    :param cat: if not None, only this category is taken into account
    :return: (prec, rec, f1)
    """
    if cat is not None:
        p = set(filter(lambda v: v[2] == cat, p))
        y = set(filter(lambda v: v[2] == cat, y))
    if len(p) == 0:
        return 0., 0., 0.
    if len(y) == 0:
        # There were no ground truths so it makes no sense to evaluate the
        # predictions.
        return None, None, None
    prec = len(p.intersection(y)) / len(p)
    rec = len(p.intersection(y)) / len(y)
    if prec + rec == 0:
        return 0., 0., 0.
    f1 = 2 * prec * rec / (prec + rec)
    return prec, rec, f1


def extract_entity_sets(articles, language='en'):
    """
    Given an Iterable of articles, extracts the sets of entities in each
    article.
    :param articles: List of languages to extract entities from
    :param language: Language of the |articles| -- 'pl' or 'en'.
    :return: A list of sets of tuples (resolved name, category).
    """
    annotator = Annotator(mode='default', language=language)
    for art in tqdm(articles):
        annotator.add_art(art)
    entities = []
    for art, ents in annotator.data:
        entities.append(ents)
    return entities


def cache_entity_sets(entities, fname):
    """
    Pickles the entitiy sets on disk.
    """
    with open(fname, 'wb') as fp:
        pickle.dump(entities, fp)


def load_cached_entity_sets(fname):
    """
    Loads pickled entity sets from disk.
    """
    if not os.path.isfile(fname):
        return None
    with open(fname, 'rb') as fp:
        entities = pickle.load(fp)
    return entities
