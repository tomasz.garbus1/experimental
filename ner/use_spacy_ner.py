import os
import pickle
import random
from pathlib import Path
from typing import List

import matplotlib.pyplot as plt
import numpy as np
import spacy
from spacy.util import minibatch, compounding
from tqdm import tqdm
from trans import trans

from ner.highlighter import Annotator, CATEGORIES
from ner.utils import to_spacy_format

MODEL_DIR = 'ner/cache/spacy_model'


def label_dataset() -> List:
    if os.path.isfile('ner/cache/spacy_train_data'):
        with open('ner/cache/spacy_train_data', 'rb') as f:
            ret = pickle.load(f)
        return ret
    annotator = Annotator(mode='spacy')
    print('Labeling dataset')
    for dir in tqdm(os.listdir('../data/bbcsport')):
        if not os.path.isdir(os.path.join('../data/bbcsport', dir)):
            continue
        for fname in os.listdir(os.path.join('../data/bbcsport', dir)):
            fpath = os.path.join('../data/bbcsport', dir, fname)
            with open(fpath, 'r') as file:
                content = file.read()
            content = trans(content)
            annotator.add_art(content)
            outdir = os.path.join('../data/bbcsport_pl', dir)
            os.makedirs(outdir, exist_ok=True)
            outpath = os.path.join(outdir, fname)
            with open(outpath, 'w+') as file:
                file.write(content)
    os.makedirs('ner/cache', exist_ok=True)
    with open('ner/cache/spacy_train_data', 'wb+') as f:
        pickle.dump(annotator.data, f)
    return annotator.data


def train(data):
    global MODEL_DIR
    model = "en_core_web_sm"
    n_iter = 1
    nlp = spacy.load(model)
    if "ner" not in nlp.pipe_names:
        ner = nlp.create_pipe("ner")
        nlp.add_pipe(ner, last=True)
    else:
        ner = nlp.get_pipe("ner")
    for cat in CATEGORIES:
        ner.add_label(cat)

    # get names of other pipes to disable them during training
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "ner"]
    with nlp.disable_pipes(*other_pipes):  # only train NER
        # reset and initialize the weights randomly – but only if we're
        # training a new model
        if model is None:
            nlp.begin_training()
        ner_losses = []
        for itn in range(n_iter):
            random.shuffle(data)
            sumbatchlens = 0
            losses = {}
            # batch up the examples using spaCy's minibatch
            batches = list(minibatch(data, size=compounding(4.0, 32.0, 1.001)))
            for i, batch in tqdm(list(enumerate(batches))):
                texts, annotations = zip(*batch)
                nlp.update(
                    texts,  # batch of texts
                    annotations,  # batch of annotations
                    drop=0.5,  # dropout - make it harder to memorise data
                    losses=losses,
                )
                sumbatchlens += len(batch)
                ner_losses.append(losses['ner'] / sumbatchlens)
                if i % 100 == 0:
                    print("Losses", losses['ner'] / sumbatchlens)
            print("Losses", losses)
        plt.plot(ner_losses)
        plt.show()

    # test the trained model
    # for text, _ in data:
    #     doc = nlp(text)
        # print("Entities", [(ent.text, ent.label_) for ent in doc.ents])
        # print("Tokens", [(t.text, t.ent_type_, t.ent_iob) for t in doc])

    # save model to output directory
    if MODEL_DIR is not None:
        MODEL_DIR = Path(MODEL_DIR)
        if not MODEL_DIR.exists():
            MODEL_DIR.mkdir()
        nlp.to_disk(MODEL_DIR)
        print("Saved model to", MODEL_DIR)


def get_test_dataset():
    test_data = []
    path = 'ner/test_set/en'
    for f in os.listdir(path):
        if f.endswith('.y'):
            with open(os.path.join(path, f)) as file:
                content = file.read()
            test_data.append(to_spacy_format(content))
    return test_data


def evaluate(data):
    # test the saved model
    print("Loading from", MODEL_DIR)
    nlp2 = spacy.load(MODEL_DIR)
    precision = []
    recall = []
    fmeas = []
    for text, true_entities in data:
        true_entities = true_entities['entities']
        doc = nlp2(text)
        p = set([(ent.text, ent.label_) for ent in doc.ents])
        y = set([(text[s:b], c) for (s, b, c) in true_entities])
        precision.append(len(p.intersection(y)) / len(p))
        recall.append(len(p.intersection(y)) / len(y))
        fmeas.append(2 * (precision[-1] * recall[-1]) /
                     (precision[-1] + recall[-1]))
    print("prec: %f, recall: %f, f1: %f" % (
        np.mean(precision), np.mean(recall), np.mean(fmeas)))


if __name__ == '__main__':
    # train_data = label_dataset()
    # train(train_data)
    test_data = get_test_dataset()
    print(test_data)
    evaluate(test_data)
