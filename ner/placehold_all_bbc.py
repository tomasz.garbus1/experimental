"""
A script which iterates all BBC articles, replaces entity names with
ID'd placeholders (so 'Manchester United' and 'United' will both be '{team:0}'
and 'Arsenal' and 'Gunners' will be '{team:1}' and saves them to another
directory.
"""
import os

from tqdm import tqdm
from trans import trans

from ner.highlighter import Highlighter


def placehold_all_bbc(hl: Highlighter):
    for dir in tqdm(os.listdir('../data/bbcsport')):
        if not os.path.isdir(os.path.join('../data/bbcsport', dir)):
            continue
        for fname in os.listdir(os.path.join('../data/bbcsport', dir)):
            fpath = os.path.join('../data/bbcsport', dir, fname)
            with open(fpath, 'r') as file:
                content = file.read()
            content = trans(content)
            content = hl.placehold_with_id(content)
            outdir = os.path.join('../data/bbcsport_pl', dir)
            os.makedirs(outdir, exist_ok=True)
            outpath = os.path.join(outdir, fname)
            with open(outpath, 'w+') as file:
                file.write(content)


if __name__ == '__main__':
    hl = Highlighter()
    placehold_all_bbc(hl)
    pass
