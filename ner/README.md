# Named Entity Recognition
This experiment is dedicated to:
* identifying entities occurring in texts using a bunch of heuristics and entity
  datasets from different sources
* producing a new corpus with marked entities
* text generation "conscious" of the occurring entities 

Consider the screenshot below:

![](highlight.png)

## Metrics and results
Highlighter class is evaluated on the basis of detected entities in 100 test 
articles (100 in English, 100 in Polish). F1 score is computed from the set
of entities occurring in a text and the set of entities detected by Highlighter.
A single entity is defined in SpaCy NER format, i.e. as a tuple
`(beg, end, cat)` - range in text and category of the entity. 

### English
```
category: all
prec: 0.759632, recall: 0.828691, f1: 0.785271 (#entities: 4586)
category: league
prec: 0.671901, recall: 0.669751, f1: 0.667461 (#entities: 285)
category: stadium
prec: 0.273674, recall: 0.332386, f1: 0.286012 (#entities: 73)
category: manager
prec: 0.538499, recall: 0.463081, f1: 0.479853 (#entities: 431)
category: player
prec: 0.663199, recall: 0.841397, f1: 0.720612 (#entities: 1612)
category: team
prec: 0.838725, recall: 0.912286, f1: 0.857498 (#entities: 1820)
category: country
prec: 0.418267, recall: 0.396359, f1: 0.400916 (#entities: 365)
```

### Polish
```
category: all
prec: 0.725941, recall: 0.609127, f1: 0.652036 (#entities: 2858)
category: league
prec: 0.438552, recall: 0.327396, f1: 0.362545 (#entities: 205)
category: stadium
prec: 0.060383, recall: 0.060383, f1: 0.060383 (#entities: 20)
category: manager
prec: 0.330658, recall: 0.253418, f1: 0.275342 (#entities: 200)
category: player
prec: 0.610734, recall: 0.595375, f1: 0.579130 (#entities: 1081)
category: team
prec: 0.713225, recall: 0.638572, f1: 0.649267 (#entities: 1181)
category: country
prec: 0.416745, recall: 0.354278, f1: 0.372267 (#entities: 171)
```

## `entities` files format
* Entity datasets are stored in CSV format.
* Any list field is separated with semi-colons.
* __TODO__: Distinguishing between "any occurrence" name variants and
 "subsequent occurrence" name variants is important. For instance, *"Manchester
 United"* may occur in text at any time, but just *"United"* will occur only
 provided that the full name has occurred before
### Countries
| Column 1 |
| -------- |
| Country name variants |
### Leagues
| Column 1 |
| -------- |
| League name variants | 
### Managers
| Column 1 | Column 2 |
| -------- | -------- |
| Any occurrence name variants | Club name | 
### Players
| Column 1 | Column 2 | Column 3 |
| -------- | -------- | -------- |
| Any occurrence name variants | Club name | Season |
### Stadiums
| Column 1 |
| -------- |
| Stadium name variants |
### Teams
| Column 1 |
| -------- | 
| Any occurrence name variants |

## Experiments

### #2 - replace all names with identified placeholders
TODO: Samples (take the trained model from my google cloud console machine
`tensorflow-2-vm`, file `run1.zip`)

### #1 - replace all names with placeholders, train gpt2-simple

```
======== SAMPLE 1 ========
ised} had a great game, which ended up taking him the top of the Premiership with a win over the {player}s, but could only get to grips with the {player}s' lack of possession. 
Newcastle are still in a tough place heading into the final week, but are still in the relegation zone. But they are still one of six {player}s in six {player}s.
{team} manager Roy {player} told BBC Sport:
"I'm disappointed with their defensive half being reduced - they're still going to keep scoring goals. 
"We've got to find a way to win this league and we've got to get ourselves over the line more often. 
"But if you lose a game or get into a situation where you're losing your mind, then that was not a positive outcome."
{team} forward {player} has told BBC Sport the team performed well under {player} on Saturday.
"The players performed well throughout the game, but I think we've got to improve a bit," he said of the performance.
"You have to be better than the players and to get better than them, that's what we've worked with over the last five days.
"It's a tough game to get into, but we didn't lose too many players to get in when Saturday was played.
"There's a lot more in the game - I've got to look at it against any team that is not playing well.
"We've conceded lots of goals but we haven't done too much today. I thought Newcastle gave us a little bit too many chances.
"I don't think we've had any big changes at the back and I think in the opening game we'd need a big kick and we're a good team."<|endoftext|>
```
```
@14242729081
url: https://www.bbc.com/sport/football/36986890
Tottenham defender {team} made 25 changes to their squad for Saturday's {league} tie with {team}.
With {player}'s opener, the former {team} man turned in an effort that went straight to the keeper.
Spurs were awarded a first yellow card for a foul on {player}, but {team}'s {player} headed in an effort to give the visitors an injury-time lead.
{player}, who was given a red card, was replaced by {player} with eight minutes left.
{team} had just one shot on the hour as {player} scored in injury time after {player}'s curling pass had missed to create a danger.
The former Spurs striker, who scored twice in the home win, then got a second penalty after a penalty which was saved by {player}.
Spurs keeper {player} fired a fierce free-kick into the bottom corner before half-time that had him looking almost unstoppable in the closing stages.
The loss of {player} meant {team} manager {manager} was confident he would still make a positive impression in a win against an attacking {team} defence.
{manager}'s side are currently seventh in the Capital {player}ship table, eight
```