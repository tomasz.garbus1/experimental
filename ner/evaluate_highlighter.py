"""
Evaluates the Highlighter class against the manually labelled test set (only
English BBC articles).
"""
import argparse
import os

import numpy as np

from ner.highlighter import Annotator, CATEGORIES
from ner.utils import to_spacy_format, prec_rec_f1_score

LANGUAGE = 'en'


def get_test_dataset():
    test_data = []
    path = 'ner/test_set/%s' % LANGUAGE
    for f in os.listdir(path):
        if f.endswith('.y'):
            with open(os.path.join(path, f)) as file:
                content = file.read()
            test_data.append(to_spacy_format(content))
    return test_data


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='Evaluate highlighter')
    parser.add_argument('--pl', action='store_true', required=False,
                        help='input directory')
    parser.add_argument('--table', action='store_true',
                        help='whether to output in latex table rows format')
    args = parser.parse_args()
    if args.pl:
        LANGUAGE = 'pl'

    arts = []
    ground_truths = []
    preds = []
    annotator = Annotator(mode='spacy', language=LANGUAGE)
    for x, y in get_test_dataset():
        arts.append(x)
        ground_truths.append(set(y['entities']))
        preds.append(set(annotator.add_art(x)))
    for cat in [None] + CATEGORIES:
        precision = []
        recall = []
        fmeas = []
        number_of_entities = 0
        for p, y in zip(preds, ground_truths):
            number_of_entities += len(
                list(filter(lambda e: cat is None or e[2] == cat, y)))
            prec, rec, f1 = prec_rec_f1_score(p, y, cat=cat)
            if (prec, rec, f1) == (None, None, None):
                continue
            precision.append(prec)
            recall.append(rec)
            fmeas.append(f1)
        if args.table:
            print('%s & %d & %f & %f & %f\\\\' % (
                cat, number_of_entities, np.mean(fmeas), np.mean(precision),
                np.mean(recall)))
            print('\\hline')
        else:
            print("category: " + str(cat))
            print("prec: %f, recall: %f, f1: %f (#entities: %d)" % (
                np.mean(precision), np.mean(recall), np.mean(fmeas),
                number_of_entities))
