league:Premier League
stadium:Emirates Stadium
manager:Arsene Wenger
player:Folarin Balogun,Calum Chambers,Hector Bellerin,Nathan Baxter,Pedro
team:Arsenal F.C.,Chelsea F.C.
country:Netherlands
::Arsenal 5-1 Chelsea::
@1354958467
url: https://www.bbc.com/sport/football/20521846
Arsenal continued their superb start to the Premier League season with a comfortable win over Chelsea at Emirates Stadium.
Folarin Balogun scored the only goal in the first half, heading in from Calum Chambers' cross to give the hosts the lead.
Hector Bellerin doubled the lead before Nathan Baxter headed in from a Pedro cross.
The away side started the second half as they did in the first. N'Golo Kante's drilled cross found Bruno, who headed home from six yards.
It kept the visitors in contention but Arsene Wenger's team were not to be denied their first win in eight matches.
At the Emirates, the hosts had the better of the first half but, after Kante's early opener, Chelsea were much more industrious in the second period.
N'Golo Kante has now scored in the first four games of the Premier League season.
Arsenal manager Arsene Wenger:
"I think we played well, but I think we just didn't have enough quality. We played well in the first half but we didn't have a bit of quality.
"In the second half we had a lot of quality but we didn't have enough quality to score first goal.
"But I think we played well, we responded to the first goal.
"We had some chances and then we lost in the end. We had a lot of possession and created a lot of chances but we lost the game.
"In the end we had too much possession for the first goal."
Chelsea manager Andre Villas-Boas:
"The first half was a very, very good, very good performance, we played well.
"In the second half we were more comfortable, we scored a lot of goals