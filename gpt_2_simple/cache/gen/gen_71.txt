league:World Cup
stadium:Allianz Arena
manager:Adam Nawalka
player:Robert Lewandowski
team:Borussia Dortmund
country:Poland,Ireland,Germany
::World Cup: Poland coach Adam Nawalka expects nation's support::
@1423443328
url: https://www.bbc.com/sport/football/31035009
Poland coach Adam Nawalka believes his team can get their nation behind them in the World Cup qualifiers against the Republic of Ireland and Germany.
The Poles, top of Group H on goal difference ahead of the Republic match, 2-1 win over the Republic in Warsaw on Tuesday.
"We know the people and we know what it means," Nawalka told BBC Sport. "We are ready for the game."
The victory was the first time Poland have won a competitive match in the World Cup as they beat the Republic 3-1 on Sunday.
Fellow former international Robert Lewandowski netted twice in the victory, and added to the scoreline by scoring.
"We had to work hard," said Nawalka. "We had to prepare, and we had to be prepared mentally."
Coach Nawalka's side started brightly, but were caught by the quick passing of the Republic of Ireland, who were playing in their first game in a major tournament as they were denied a win in their first competitive match at the Allianz Arena.
It was a result that saw the Republic of Ireland take the lead before the game in Warsaw.
"They were very good in the first half, but we dominated in the second half and in the last 20 minutes we were very good," he said.
"It is a disappointing result for us. But that's the way it goes. We have to do better."