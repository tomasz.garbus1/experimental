league:World Cup
stadium:Allianz Arena
manager:Adam Nawalka
player:Robert Lewandowski
team:Borussia Dortmund
country:Poland,Ireland,Germany,England,South Africa
::World Cup qualifiers: Poland 1-2 Germany::
@1346464375
url: https://www.bbc.com/sport/football/19290919
Werder Bremen forward Robert Lewandowski's hat-trick helped Germany win the first World Cup qualifier against Poland in their World Cup qualifier against Ireland.
Lewandowski struck twice as the German side won the first leg of the last-16 tie 1-0 at the Allianz Arena.
Heidi Sjogren's header had cancelled out the opener for the Polish side.
Lewandowski's first came from a corner and he then scored his second from a tight angle.
The Poland striker was magnificent from the spot.
Heidi Sjogren's header had cancelled out Lewandowski's opener.
Lewandowski then made it 2-0 with a composed finish from a tight angle.
Lewandowski completed the rout from a tight angle.
The result was a major relief for Lewandowski, who had scored just once in nine appearances for Borussia Dortmund this season.
In the first leg, Lewandowski did not make a single first-team appearance for the Bundesliga champions.
In the second leg he featured alongside top scorer Thomas Muller, who also played without Lewandowski.
Dortmund have now won all their World Cup qualifiers in the past 13 seasons, but they have not won a competitive match at the tournament since the 1998 tournament in South Africa.
They were grateful to playmaker Lewandowski, who also had a hand in the opener and the second goal.
Heidi Sjogren's header was the closest they came to scoring in the second half, although Lewandowski hit the bar with a free-kick from the edge of the box.
Lewandowski's second goal, his first in this competition since 2013, was a superb finish from close range.
Dortmund's Robert Lewandowski celebrates scoring