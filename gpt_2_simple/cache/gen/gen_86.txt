league:World Cup
stadium:Allianz Arena
manager:Adam Nawalka
player:Robert Lewandowski
team:Borussia Dortmund
country:Poland,Ireland,Germany
::World Cup qualifiers: Poland 2-0 Germany::
@1430667131
url: https://www.bbc.com/sport/football/32538659
Robert Lewandowski's goals helped Poland to victory in the first World Cup qualifier against Germany. 
The former Borussia Dortmund forward scored a superb solo goal from close range to give Poland a 2-0 win at the Allianz Arena.
Lewandowski's first was a great first touch to the ball and he then controlled the ball on his chest before shooting past goalkeeper Niklas Hleb.
The second goal was Lewandowski's fourth in four World Cup qualifiers, his second of the campaign.
Only Germany, on the other hand, lost 4-0 away to Northern Ireland in their previous World Cup qualifier.
Adam Nawalka's Republic of Ireland, who are top of Group E, are top of the group on six points, two points over third-placed Germany.
Poland, who have not previously played at a World Cup finals, now have a possible advantage on Germany, who play their opening match against Northern Ireland on Tuesday (19:45 BST).
The match in Berlin will be broadcast live by the BBC World Service (BBC World).
Match ends, Poland 2, Germany 2.
Second Half ends, Poland 2, Germany 2.
Goal!  Poland 2, Germany 2. Robert Lewandowski (Poland) right footed shot from the centre of the box to the bottom right corner. Assisted by Szymon with a cross.
Goal!  Poland 1, Germany 2. Lewandowski (Poland) header from very close range to the bottom left corner. Assisted by Szymon with a cross.
Attempt blocked. Szymon (Poland) right footed shot from outside the box is blocked. Assisted by Lewandowski.
Attempt missed. Lewandowski (Poland) right footed shot from