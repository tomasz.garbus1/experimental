<|startoftext|>::{team:0} boss {manager:0} says {team:1} are 'under great pressure'::
@1389122523
url: https://www.bbc.com/sport/football/25291022
{team:0} manager {manager:0} says he is "under great pressure" after Saturday's 3-1 home defeat by {team:1}.
{team:2} failed to register a shot on target in 11 games, their worst start since 2008.
"I'm under tremendous pressure," said {manager:0}. "It's got to be the right team for the next three games."
{team:2} are four points off second-bottom {team:3} with nine games remaining.
{team:0} have won their last four matches at home and are two points away from the play-off places.
"I'm under great pressure," added {manager:0}. "The players have been terrific and I'm trying to make sure we're not going to slip up again.
"It's hard to put into words. I'm trying to make sure we don't allow that to happen."
{team:2} started with 10 men after {player:0} was sent off following a challenge in the second minute of added time.
{team:0} head coach {manager:0} added: "It was a tough game for