<|startoftext|>::{player:0}: {team:0} sign {team:1} striker on loan::
@1437904030
url: https://www.bbc.com/sport/football/33809960
{team:0} have signed striker {player:0} on loan from {team:1} until the end of the season.
The {country:0} international, 24, has scored three goals in eight games for {team:2} this season, including three headers, in the {league:0} {league:1} play-offs.
"I think my team-mates will be delighted about me and the way I'm doing things is fantastic," he said.
"I've got to put my best team out. I've got to concentrate on my game now and hopefully I can score a goal."
{team:0} have been without a striker for the past four games, with {player:1} the only player at the club who has scored a goal in a full {league:0} game.
{player:0} joined {team:1} in 2013 and scored 15 goals in a loan spell which also included a double in the {league:0} play-offs.
{team:0} have won just once in their past nine matches, and {player:0} is pleased to have a chance to play a part.
"It's a