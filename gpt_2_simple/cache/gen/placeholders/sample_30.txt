<|startoftext|>::{team:0} 1-1 {team:1}::
@1368135720
url: https://www.bbc.com/sport/football/21961529
{team:1} earned a late equaliser as they fought back to draw with {team:0}.
{team:2} took the lead when {player:0} headed in {player:1}'s cross after {player:2}'s well-worked free-kick.
{team:0} equalised when {player:3} poked home from close range.
{team:1}'s {player:4} could have won it but he headed wide.
{team:0} manager {player:5} told BBC Radio {team:3}:

                            
                         
"It's a point away from the game, really, but we had to come back.
"It's been a long time since we've had a game like that.
"It's been a long time since we've had to take the three points away from home.
"We've been a little bit unlucky and I'm disappointed with the result, but they