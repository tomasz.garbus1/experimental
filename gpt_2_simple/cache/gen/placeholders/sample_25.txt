<|startoftext|>::{player:0}: {team:0} sign former {team:1} striker::
@1413583389
url: https://www.bbc.com/sport/football/29079413
{team:0} have signed former {team:1} striker {player:0} on a two-year contract.
The former {team:1} forward, 30, made over 100 appearances for {team:2} from 2006 and won four {country:0} caps.
"I've been waiting for this opportunity," he told the club website.
"I knew I had to be here. I've had talks with {team:0} and I'm excited to be here."
{player:0} scored 17 goals in 48 appearances for {team:3} before joining {team:0} last summer.
"I've only been here for a few weeks, so it has been wonky," he told BBC {country:0}. "I've been looking over the past few weeks. I felt that the manager [{manager:0}] and {player:1} [assistant manager] had to come to me and have a chat and they said they were in for me."
{player:0} has scored five goals in 27 games for {team:1} since his 18th birthday.
And he is the second player to join {team:0} this