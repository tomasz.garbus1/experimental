<|startoftext|>::{team:0} 4-2 {team:1}::
@1393557247
url: https://www.bbc.com/sport/football/26165089
{player:0} scored twice as {team:0} came from behind to beat {team:1} and complete a three-match winless run.
{player:1} gave {team:1} the lead in the first minute, heading in from {player:2}'s free-kick.
{player:0} doubled the lead in the second minute with a free-kick into the top corner.
{player:3} put {team:1} ahead just before half-time, but {player:0} equalised shortly after the restart with a low shot.
The {country:0} international added a third in the final minute after a cross from {player:4}, before {player:5}'s early goal ensured victory.
{team:1} manager {manager:0} would have been disappointed to lose the game at {stadium:0}, which left him with just five points from his previous three games.
However, the late win was all but confirmed by {player:0}'s late strike.
{team:1} had only won once in their previous five games, their win over {team:2} in December 2012, their last appearance at {st