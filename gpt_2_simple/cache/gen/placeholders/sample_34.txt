<|startoftext|>::{team:0} 2-1 {team:1}::
@1471233696
url: https://www.bbc.com/sport/football/3709697
{player:0} grabbed a late winner as {team:0} beat {team:1} to go top of {league:0}.
The hosts had the better of the opening half, with {team:2} goalkeeper {player:1} saving a {player:2} penalty.
{team:1} equalised when {player:3}'s low shot was headed home by {player:0}.
{team:0} had {player:4} sent off for bringing down {player:5} in the box, but {player:0}'s timely winner ensured that {team:1}'s troubles were ended.
{team:0} have now won just once in their last five league games - once against {team:3} in the {league:0} fourth round.
{team:1}, who had only one clean sheet in their previous nine, went into the game with a 3-0 win over {team:4} and a 2-0 victory over {team:5} on Boxing Day.
But {team:0} dominated possession and territory and had a number of chances at the start of the second period.
{player:6} had a good chance to