<|startoftext|>::{player:0}: {team:0} sign {team:1} striker on loan::
@1410794038
url: https://www.bbc.com/sport/football/29351097
{team:0} have signed striker {player:0} from {team:1} on loan until the end of the season.
The 25-year-old former {team:2} and {team:3} forward, who helped {team:4} win the {league:0} in 2015-16, has been on loan at {team:5} since leaving {team:6} in the summer.
He scored eight goals in 37 games last season to help {team:4} avoid relegation.
{player:0} has made more than 500 appearances for {team:1}, scoring 179 goals, and has represented the {country:0} at Under-17, Under-20 and Under-21 level.