<|startoftext|>::{team:0} 1-2 {team:1}, {team:2} win 2::
@1395252966
url: https://www.bbc.com/sport/football/26598683
{player:0} scored two late goals as {team:1} moved to within two points of the play-offs.
The home side took the lead when {player:1} fired past {player:2} after a cross from {player:3} was met by {player:0}.
{team:2} equalised when {player:0} converted {player:4}'s cross before {player:5} headed in {player:6}'s cross to make it 2-2.
{team:1}'s win takes them to 10th in the table, four points behind the bottom three.
{team:2} are down to one point in 10 games.
{team:0} boss Steve Kean:
"I thought it was a fair result. I felt it was a fair game. The second half was a bit of a penalty shoot-out.
"I think we played some good stuff. We were on the back foot for a couple of weeks. We knew our task was to try and get back to winning ways.
"I think we had a couple of chances to score but I thought we were pretty solid for the second half