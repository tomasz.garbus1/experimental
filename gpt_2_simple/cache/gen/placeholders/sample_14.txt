<|startoftext|>::{player:0}: {team:0} sign {team:1} defender::
@1499500848
url: https://www.bbc.com/sport/football/40309629
{team:0} have completed the signing of {team:1} defender {player:0} for an undisclosed fee.
The 25-year-old, who has signed a two-year contract with {team:2}, has agreed a three-year deal.
"I have had a chat with {player:0} and we agreed on terms," said {team:0} boss {manager:0}.
"I am delighted to bring him to {team:0}," added {manager:0}.
{player:0} was at {team:1} when {team:0} were relegated to the {league:0} last season.
"I've watched him many times and I'm delighted to have him here. He's a good player, a good player who can bring a lot of energy to the team."
{team:0}'s other summer signing, {player:1}, has signed a two-year deal with {team:2} to join on a two-year deal.
The former {team:3} and {team:4} defender was on loan at {team:1} from {team:5} in 2008-09 and has made 121