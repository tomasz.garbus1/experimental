<|startoftext|>::{team:0} 0-1 {team:1}::
@1435979300
url: https://www.bbc.com/sport/football/32539759
Two goals from {player:0} helped {team:1} beat {team:0} and secure a win for the first time in their {league:0} career.
{player:0} scored twice, first lobbing {team:0} defender {player:1} before slotting home from close range.
{team:2}'s {player:2} opened the scoring after just 11 minutes before the former {team:3} striker scored for {team:0}.
{player:0} then added his second in stoppage time as {team:2} held on to claim a victory.
The home side had the better of the early exchanges, but {team:0} were ruthless in the final stages and were rewarded for their effort with a deserved victory.
{team:4} manager {manager:0} told BBC Radio {team:0}:

                          
                       
"We knew we had to do better than that. We were struggling. We were two goals down