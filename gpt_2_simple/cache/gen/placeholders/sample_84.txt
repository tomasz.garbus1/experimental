<|startoftext|>::{team:0} 0-1 {team:1}::
@1397010111
url: https://www.bbc.com/sport/football/26961056
{team:1} held on to beat {team:0} and keep alive their hopes of a play-off push.
{team:2} were ahead inside the first minute when {player:0} produced a superb fingertip save to deny {player:1} after he fouled {player:2}.
They were level two minutes later when {player:3}'s low cross was palmed away by {player:4} inside the box.
{team:1} held on to enjoy a spell of dominance through {player:5}'s close-range finish.
But {team:0} equalised five minutes before the interval when {player:6}'s cross from the left was headed in by {player:0} for his sixth goal of the season.
{team:0} were frustrated by the visitors, who had their best spell after the break as {player:7} hit the bar with an effort from the right, while {player:8} had a goal disallowed for offside.
{team:1} striker {player:9} hit the post in the second half as {team:0} pushed for a penalty.
{team:1} had a