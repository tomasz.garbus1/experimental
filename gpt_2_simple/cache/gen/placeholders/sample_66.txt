<|startoftext|>::{team:0}'s {player:0} signs new three-year contract::
@1388152399
url: https://www.bbc.com/sport/football/25176594
{team:0}'s {player:0} has signed a new three-year contract with the {league:0} club.
The 26-year-old, who has played in the {league:1} for {team:1}, has signed a three-year deal at the {stadium:0}.
The former {team:2} youth-team graduate has been a free agent since leaving {team:1} in June 2013.
"It's been a long time coming and it has been a bit of a whirlwind," he said.
He told BBC Radio {team:0}: "I came to {team:1} at the start of last season and it's a massive club. I have got to take some things with me but I think I have the best squad here.
"It's a great club and I love it here."
{player:0} added: "I want to play over there and I can play here for a long time, it's just up to finishing it off and getting my head round it.
"The last two games have been difficult for me, I just need to put it right. I am looking forward to it and hopefully I