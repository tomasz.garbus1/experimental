<|startoftext|>Yeovil Town == Gateshead
Jamie Reid gave a battling first-half display as Gateshead earned a draw at Poolside.
The away side took the lead when Jordan Jones headed home a corner.
But the visitors levelled in the second half when Jamie Hamilton's header went in off a post.
The home side were denied a penalty when Ben Davies went down under a challenge from Lewis Young.
Match ends, Yeovil Town 1, Gateshead 1.
Second Half ends, Yeovil Town 1, Gateshead 1.
Attempt blocked. Jamie Reid (Gateshead) right footed shot from the right side of the box is blocked.
Attempt missed. Ben Davies (Gateshead) right footed shot from the right side of the box misses to the right.
Goal!  Yeovil Town 1, Gateshead 1. Jamie Reid (Gateshead) right footed shot from the centre of the box to the bottom right corner.
Attempt saved. Lewis Young (Gateshead) right footed shot from outside the box is saved.
Corner,  Gateshead.
Corner,  Gateshead.
Attempt saved. Ben Davies (Gateshead) right footed shot from outside the box is saved.
Substitution, Yeovil Town. Liam Shephard replaces James Hayter.
Substitution, Yeovil Town. Jamie Reid replaces James Hayter.
Attempt missed