<|startoftext|>Blackburn Rovers == Norwich City
Norwich City missed a chance to move into the top six with a defeat at Championship strugglers Blackburn Rovers.
Keaton Wintle headed in Conor Sammon's cross to give Norwich an early lead.
The Canaries, who looked sure to claim their first away win of the season, went behind when Alex Pritchard's header from a corner was turned into his own net by Darren Ambrose.
And the Canaries were level when Graham Dorrans' low shot from the left side of the area beat keeper Angus Gunn.
Blackburn's best effort came from substitute Chris Taylor, but he could not keep his effort down.
Norwich, who were without captain Russell Martin because of a broken ankle, led when Graham Dorrans' shot from the left side of the area was deflected past goalkeeper Angus Gunn.
The home side's lead lasted just three minutes, though.
Wintle headed home from a corner, but the striker was penalised for handball.

                               
                           
Norwich, who have won just once in six games, did not win their first shot on target, with Taylor's effort blocked by a sliding