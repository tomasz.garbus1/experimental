<|startoftext|>Harrogate Town > Dover Athletic
Harrogate moved two points clear at the top of the National League with a resounding win over Dover.
The home side took the lead through Craig Ross's penalty after the visitors had failed to clear a free-kick.
The visitors, who had goalkeeper Chris Robertson to thank for keeping them in the game, had a great chance to draw level but was denied by a superb reaction save from Ben Heneghan.
The visitors' best effort fell to Michael Folivi and he made a fine save from Michael Green, before Harrogate took the lead after 20 minutes.
Ross's shot was parried and at the other end Ross had a free-kick cleared off the line by Jordan Burrow.
The visitors attempted a late equaliser but goalkeeper Liam Davis got down well to keep out Lee Molyneux's close-range header.
Match report supplied by the Press Association.
Match ends, Harrogate Town 1, Dover Athletic 0.
Second Half ends, Harrogate Town 1, Dover Athletic 0.
Substitution, Dover Athletic. Alex Woodyard replaces Tom King.
Substitution, Dover Athletic. Brandon Goodship replaces Mark Yeates.
Substitution, Harrogate Town. Jack Muldoon replaces Craig Ross.
Substitution, Dover Athletic. Cameron Salkeld replaces Craig McAllister.
Substitution, Dover Athletic. Joe Leesley replaces Lee