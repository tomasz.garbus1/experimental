<|startoftext|>Walsall == Derby County
Substitute Marcus Tavernier's late penalty earned Walsall a draw against Derby County.
The former Leicester loanee was brought down by Chris Martin in the box by him and converted the penalty into his own net.
The Rams had goalkeeper Jon McLaughlin to thank after a smart save from substitute David Nugent.
But Walsall kept going for a goalless draw.
Walsall had a first-half penalty appeal turned down when Ben Purkiss went down in the box under a challenge from David Nugent.
And the Saddlers, who had won their previous two games, had a glorious opportunity to win it in the 73rd minute when Jordan Hugill's header was flicked on by Josh Murphy and went in.
But there was to be a twist in the tie when a challenge by Martin on Nugent saw the Rams winger fall to the ground and his opponent converted the penalty.
The win saw Walsall climb to 20th in the Championship table, while Derby, who dropped to the bottom four, are now without a win in five games.
Walsall manager Dean Smith: "I was happy with the result. I wouldn't want to be on the pitch with someone who has given everything for the team but I'm not going to be that.
"I'm happier with the result than the performance. The players should be proud of what they did.
"We deserved it because we