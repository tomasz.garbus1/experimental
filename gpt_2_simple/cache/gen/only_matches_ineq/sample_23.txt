<|startoftext|>Aston Villa > West Ham United
Aston Villa came from behind to beat West Ham and end a run of six Premier League games without a win. 
Villa took the lead in the second minute when Christian Benteke's shot from the edge of the box was parried by goalkeeper Adrian, allowing the Hammers a foothold.
Benteke's shot came back off the far post and the Belgium striker fired in from 20 yards.
The home side's pressure continued after half an hour, with Marko Arnautovic heading over from eight yards out.
But the U's gradually gained a foothold over the half-hour mark, and Benteke's shot was headed over the bar by Adrian.
The Czech Republic international needed a penalty to earn Villa a first Premier League point this season. 
Carlos Tevez was adjudged to be offside when Arnautovic headed in a Villa corner, before Tevez himself felled Villa's leading scorer, but referee Lee Mason waved play on.
The visitors, who had won their previous five games, became the first team to lose to Villa since 1964.
"Spurs should have been two or three up at half-time but wasted a great chance. Villa are the only team in the Premier League to have managed to score a goal in this season's campaign. It was a real killer for the home fans," said manager Mauricio Pochettino.
"I am very proud of my players