<|startoftext|>Norwich City > Leeds United
Norwich City's Stuart Dallas hit a late winner as Leeds United's play-off push was dented.
The Tractor Boys, who have won just once in nine games, led through Daryl Murphy's own goal.
But Dallas struck from the spot after Graham Dorrans was brought down by goalkeeper Alex McCarthy.
Leeds's best chance fell to Marius Zaliukas, who turned a low effort from the left against the bar.
Dallas, the deadline-day signing from Rochdale, thought he had grabbed the winner but it was ruled out for offside.
Leeds are now without a win in three attempts and cannot even claim a point in the Championship this season.
And they remain in the relegation zone, nine points from safety.
"It's a very tough time for us," said the striker. "We have a great crowd here, a great atmosphere. But we have to keep believing that we can achieve this.
"We have to keep believing, keep believing. It's not over yet. We have five games in a row now, and we are in a great position.
"We can still keep going and keep dreaming. We are in a rocket at the moment, but there is still a chance for us."
Norwich City head coach Daniel Farke told BBC Radio Norfolk: "I'm very proud of the players, the staff and the supporters.
"I'm very proud