<|startoftext|>Burton Albion == Ipswich Town
Jon Stead's first goal for Burton Albion landed on the roof of the net for the first time since February as the Brewers held on for a goalless draw against Ipswich.
Burton, who won promotion to League One, are now without a victory in eight games.
The visitors had the best of the early chances but the hosts failed to capitalise.
Ipswich were denied a penalty for a Stephen Whittingham handball in the area, while Stead was denied by goalkeeper Bartosz Bialkowski.
Adam Armstrong went closest for the visitors but his header went over the bar before Stead finished from close range.
Burton manager Gary Rowett told BBC Radio Derby:
"It's a great point and we deserved to win.
"I thought we were the better team in the first half but we didn't take our chances in the second half.
"I felt we deserved to win but we didn't, they had a penalty and then the ref can't give it. He gives it and then he can't give it again."
Match ends, Burton Albion 0, Ipswich Town 0.
Second Half ends, Burton Albion 0, Ipswich Town 0.
Foul by Callum Butcher (Ipswich Town).
Adam Armstrong (Burton Albion) wins a free kick in the defensive half.
Foul by Laurent Depoitre (Ipswich Town).
Adam Armstrong (