<|startoftext|>Chelsea > Swansea City
Tottenham's hopes of avoiding relegation to the Premier League were dented as Frank Lampard's late strike secured a draw for Chelsea at Stamford Bridge.
Spurs were 2-0 up in the first half thanks to Lampard's first Blues goal, before Angel Rangel's header put Swansea level from 35 yards.
John Terry curled in a superb free-kick before Oscar felled Lampard and the Spain international drove the ball home from six yards.

                           
                        
Swansea's best chance fell to Marvin Emnes, but his header was saved by Petr Cech.
Chelsea remain in the Premier League's bottom three but they are now one point above Swansea, who have an inferior goal difference.
Swansea's other two points have come in the past four days, but they remain nine points adrift of safety.
Cech saved from Set-pieces - and Lampard's clever flick took him to 16 goals for the season - before Lampard's late finish.
The England international has now won his second goal in as many games, and his 20th of the season.
"I am really delighted," said Cech. "I am really pleased with the result and the