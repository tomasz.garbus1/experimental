<|startoftext|>Southend < AFC Wimbledon
AFC Wimbledon's victory over Southend means they stay top of the Championship table and are 10 points clear of a play-off place.
The visitors dominated in the first half, with Mark Little and Jonathan Meades orchestrating the Dons' attack.
The visitors' best chance came when Liam Trotter headed over from eight yards.
Southend pulled a goal back when Ryan Leonard headed in from a corner.
But Wimbledon took the lead when Little's header from Tyrone Barnett's cross was turned into his own net by Tom Soares.
The victory moves Wimbledon up to 13th place, while Southend are six points from safety.
Wimbledon manager Neal Ardley told BBC London 94.9:
"We were quite clever in the first half, we got the goal and the goal came from a set piece.
"We've had a good go and we've got a good result.
"The goal was absolutely fantastic. I came out to the stadium and saw my boys playing and I wanted to get a couple of players on the pitch and we've got that goal.
"That's the biggest win in my time at this football club and I'm delighted with the players' reaction, they've shown they can do the job."
Match ends, Southend United 1, AFC Wimbledon 2.
Second Half ends, Southend United 1, AFC