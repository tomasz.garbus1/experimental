<|startoftext|>Norwich City > Bristol City
Norwich City moved up to second in the Championship with a comfortable win over Bristol City.
The Canaries took a lead when Wes Hoolahan headed in Jonny Howson's cross.
The forward was then given an excellent opportunity to double the lead but his header was headed over by Joe Bryan.
Bobby Reid and Cameron Jerome went close for the visitors before the break.
Cammy Smith headed over and Jerome was denied by a fine save from Adam Federici.
The result means the Canaries have now won four of their last six games in all competitions in all competitions. They are now one point clear of third-placed Ipswich Town, who lost 3-0 in their last game, while Bristol City, who suffered their fourth consecutive league defeat, stay in the relegation zone.

                            
                        
Norwich manager Daniel Farke: "The first goal was a decisive one. It is a fantastic goal for the manager. It is a fantastic feeling.
"We have played much better and been undone by bad defending from the players. It was a really tough game and it was a really good way to end the week.
"We played with a lot of energy and