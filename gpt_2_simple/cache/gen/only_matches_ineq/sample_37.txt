<|startoftext|>Middlesbrough > Norwich City
Middlesbrough eased their relegation fears with a win over Norwich that saw them leapfrog their hosts into third place in the Championship.
Middlesbrough held on for the win despite a late scare from Norwich keeper Angus Gunn, who was sent off for a second booking.
Caretaker boss Aitor Karanka, who was watching from the touchline, made five changes from Saturday's draw at Watford, with the lone change being that of midfielder Rudy Gestede.
Gestede had a shot blocked on the line moments earlier, and could have scored when he headed a low left-footed shot against the bar.
However, the visitors were pegged back when a cross from Jonny Howson was headed against the bar by Nathan Baker and Lewis Grabban headed the rebound against the post.
The home side then doubled their lead when Gestede's shot was headed down by Baker for Grabban to turn a low shot into the net.
Norwich's best chance fell to Cameron Jerome, whose shot was blocked on the line by Gestede.
The Canaries started brightly but could not find a way through and were punished for missing a number of chances.
Gestede's shot from 25 yards hit the post, before Stevenson's close-range header was saved by Gestede.
But their hopes of a comeback were dented when Baker turned Jerome's inswinging corner into his own net.