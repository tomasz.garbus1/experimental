<|startoftext|>Bolton Wanderers == Sheffield United
Sheffield United were denied a first win in four league games as Bolton moved into the Championship's top six with a draw at Molineux.
The Blades, who slipped to the bottom of the table, had the better of the first half but David Wheater and Jonathan Spector both went close.
Neil Lennon's men looked in control but Bolton's top-scorer Tyler Blackett was sent off for a tackle on Matthew Lund before the break.
Chris Porter then had a close-range shot saved by David Stockdale, before the Blades took the lead through Wheater's powerful shot.
Lionel Messi's effort was tipped over the bar by Stockdale before the Blades took the lead.
A penalty was awarded for a push by goalkeeper Stockdale on Simon Church.
However, the Blades were level seven minutes later when James Henry's shot was pushed over by Stockdale.
Neil Lennon's men then had a late penalty appeal turned down.
Bolton manager Neil Lennon:
"I think we did well to get the result.
"I thought we had a great start and could have been 2-0 ahead but I thought after that we had a fantastic comeback.
"We deserved to win and I thought we deserved a very lucky goal. We've had a bit of luck with the goalkeeper in the past but I don't think that's going to be the case this time."
Sheffield United