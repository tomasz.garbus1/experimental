<|startoftext|>Bolton Wanderers < Nottingham Forest
Chris Cohen's stunning volley put Nottingham Forest ahead and led to a nervy finish for Bolton.
The hosts had the ball in the net twice in the first three minutes but Jakal Lowe's header cleared a crossbar and then struck the woodwork.
The hosts celebrated wildly as Bolton's fans left the pitch in glee. 
The former Charlton manager watched from the stand as Bolton's hopes of a late equaliser were dashed.
The hosts' task was made easier when Lowe cleverly diverted a right-wing cross into the net from a corner.
And when Bolton's top scorer Lukas Jutkiewicz headed over from six yards, the hosts' lead was doubled.
The hosts, who had only been beaten by Norwich in their previous two games, were unable to make their numerical advantage count and Bolton, who are now unbeaten in 10, moved level with their third win of the campaign.
The visitors went ahead when Lowe broke clear from the middle before firing a low shot into the left corner.
The home fans' reaction was wild, with former Forest midfielder Marc Wilson and his wife joining in the celebrations.
Bolton's first meaningful effort came when Jutkiewicz headed over from close range from Jutkiewicz's header from a corner.
And just when Bolton were on top, Bolton made it 2-0 when Jutkiewicz's deflected shot came back off the Bolton wall