<|startoftext|>AFC Wimbledon == Leicester City
AFC Wimbledon's hopes of a top-four finish were dented by a draw with Leicester, who remain 10 points above the relegation zone.
Jamie Vardy had a goal disallowed for offside before Vardy fired a shot past Kasper Schmeichel from a tight angle and in.
But the Foxes were level soon afterwards when Paul Konchesky's weak clearance was flicked on by Andrew Frampton and Jamie Vardy's header was headed home by Kasper Schmeichel.
Vardy went close with a volley and the visitors deservedly had a penalty appeal turned down for a foul by Schmeichel.
Leicester's reward was an opening goal, which Konchesky blasted over the bar.
The Foxes thought they had won it when Marc Albrighton raced clear and fired wide from a tight angle but Wimbledon were not done yet.

                          
                       
James Maddison went close with an effort before Frampton's poor clearance found the net.
But the game was not over, with Vardy's header looking to have won it for the Foxes.
And it was Frampton's turn to make his mark on the game when he