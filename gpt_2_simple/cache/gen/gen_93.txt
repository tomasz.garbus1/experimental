league:Premier League
stadium:Emirates Stadium
manager:Arsene Wenger
player:Folarin Balogun,Calum Chambers,Hector Bellerin,Nathan Baxter,Pedro
team:Arsenal F.C.,Chelsea F.C.
country:Honduras
::Arsenal boss Arsene Wenger says Arsene Wenger will get results::
@1305521498
url: https://www.bbc.com/sport/football/13793087
Arsenal manager Arsene Wenger says he will get results after his side beat Huddersfield 2-0 at Emirates Stadium.
Folarin Balogun put the visitors ahead with a low finish from inside the box before Calum Chambers equalised from a corner.
Wenger added: "It was a difficult game to win against a lot of quality attackers."
Arsenal's Hector Bellerin hit the bar for the visitors before Chambers made it 2-0 with a 20-yard shot.
"It was a good performance," said Wenger. "I am very proud of the players. We did a good job."
The result leaves Arsene Wenger's side five points adrift of the top four in the Premier League.
Nathan Baxter twice went close for the visitors, who are now six points adrift of the top five.
The hosts have now won only one of their last 12 matches and Wenger admitted afterwards he would be disappointed if his side did not win all of their remaining games.
"We have earned a lot of points in the last few games. But we have to do better now," he said.
"We have to be more clinical in front of goal."
Arsenal have now won just one of their last 11 top-flight games but Wenger claimed his side might have been as good as beaten.
"If you want to score goals then you need to score goals," he said.
"We played well in the first half but we conceded too many. I think we conceded too many too many. 
"I don't think there is one way we can improve and keep winning until the end of the season."