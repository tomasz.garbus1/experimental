league:Premier League
stadium:Emirates Stadium
manager:Arsene Wenger
player:Folarin Balogun,Calum Chambers,Hector Bellerin,Nathan Baxter,Pedro
team:Arsenal F.C.,Chelsea F.C.
country:Belgium
::Arsenal 1-1 Chelsea::
@1414878880
url: https://www.bbc.com/sport/football/29855481
Chelsea's Europa League exit at the Emirates Stadium was a big wake-up, says Arsenal manager Arsene Wenger.
The visitors made a bright start and goalkeeper Calum Chambers saved Hector Bellerin's header from Nathan Baxter's cross.
But after a couple of minutes, the visitors went ahead when Bellerin slotted home a low finish from 10 yards.
It was a deserved victory for Chelsea, who have now gone from a European defeat to a Premier League victory.
Wenger, who has won just one of his last 10 games at Arsenal, said: "Chelsea are a good side with a good standard.
"We have to have the patience and the belief to win the game.
"We played well, we played well, we played well. I am very pleased with the result."
Chelsea have now won only twice in the Europa League in four seasons, winning five of them and losing one.
But the Frenchman was pleased with the result and praised his players for their effort in the opening period.
"We worked hard and created chances," he said.
"We played with great intensity and quality and we were very happy with that. I think we deserved to win the game.
"It was a bit of a surprise how difficult it was to defend. It was a difficult game.
"The first half was really difficult, they had lots of chances and they had to defend very well for their chances to score.
"But the second half was not so good but I am very happy with the result."
Hector Bellerin was the main threat for Chelsea in the opening exchanges, his header from Nathan Baxter's cross going just wide of the far post.
But in the second half, the Belgian was on target again, this