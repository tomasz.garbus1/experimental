league:World Cup
stadium:Allianz Arena
manager:Adam Nawalka
player:Robert Lewandowski
team:Borussia Dortmund
country:Poland,Ireland,Germany
::World Cup qualifier: Poland's Lewandowski wanted by Borussia Dortmund::
@1508454974
url: https://www.bbc.com/sport/football/41586734
Poland coach Adam Nawalka has confirmed that his side's Robert Lewandowski is one of the players he would like to sign for his national side, but is refusing to discuss the potential transfer.
The 20-year-old striker, who has played for his country in the European Championship qualifiers against the Republic of Ireland and Germany, scored a hat-trick as the Republic of Ireland thrashed the Republic of Ireland 5-0 in the 2012 World Cup qualifier in Poland.
"It's not a big deal for me because it's not something I'm interested in," he said. "I don't want to talk about it."
Lewandowski is keen to play in the World Cup qualifiers and has been linked with a move to Germany.
However, the former Republic of Ireland international has not agreed a new contract with the Republic of Ireland.
"I really don't know what's going to happen," he said. "I'll sign a contract and see if anything changes."
Lewandowski, who has scored in his past two appearances for the Republic of Ireland, said it was "not really up to me" whether he could play in the World Cup qualifier or not.
"It's something that's not really in my hands," he said. "It's not a big deal for me.
"I'm just doing my best to do as well as possible.
"I've tried to play in the Euros but I kept my discipline and worked hard towards it."
Lewandowski was one of nine players to score in those two games.
"It's really difficult for me to play in the Euros with the intensity and intensity of the games," he added.