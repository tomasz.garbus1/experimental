league:
stadium:
manager:Petr Ulicny
player:
team:Sigma Olomouc
country:Czech Republic
::Sigma Olomouc sign Czech Republic forward Aleksandar Moča::
@1414661951
url: https://www.bbc.com/sport/football/29874390
Czech striker Aleksandar Moča has joined Sigma Olomouc on a three-year contract after leaving Czech side SZL.
Moča, 24, is signed by head coach Petr Ulicny after he had previously played for his country.
He signed a three-year contract with SZL in June 2011.
Moča becomes Ulicny's first signing after he was released by SZL in 2010.
Moča scored 28 goals in 51 league appearances for SZL last season, netting six goals after his transfer to SZL.
He started his career with Alania II, for whom he spent three years.
Moča joined SZL in 2012, scoring six goals in 40 appearances before leaving to join Alania II last season.
Moča started his career with Alania II, for whom he played for three years, before moving to CZL in 2012.
Moča scored 14 goals in 51 league appearances for SZL last season, netting five goals in his last 13 games.
Moča played for Czech sides UstA_cs and Alania IV before joining SZL.
He scored 19 goals in 68 league appearances for SZL in 2012-13.
Mo