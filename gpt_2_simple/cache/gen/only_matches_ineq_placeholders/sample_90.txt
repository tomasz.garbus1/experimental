<|startoftext|>{team:0} > {team:1}: {player:0} scores as {team:2} stay fourth

                             
                          
{team:0}'s {player:0} hit the winner as {team:1} dropped to fourth place in the {league:0} table.
{player:1} volleyed {team:0} in front from close range after {player:2}'s free-kick was saved.
But {team:3}'s {player:3}'s header gave {team:4} a third win in five games.
{player:0} converted {player:4}'s cross to secure a dramatic win for {team:4}.
The victory means {team:0} remain two points clear of the bottom three with five games remaining.
{team:2}, who were just above the relegation zone at the same point last season, are now six points clear of the bottom four with one game left to play.
{team:0} manager {manager:0}: "I thought it was a poor performance from the team. We thought it was going to be easy to get into the game