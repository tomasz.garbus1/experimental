<|startoftext|>{team:0} > {team:1}
{team:2}'s {league:0} play-off hopes took a blow after they were beaten by {team:1} at {stadium:0}.
{player:0} opened the scoring for the hosts from the penalty spot after {player:1} was fouled by {team:3} keeper {player:2}.
{player:3} missed a glorious chance to equalise for {team:1}, who were lucky not to concede a penalty when {player:4} was sent off for a second caution.
{player:5} hit the bar for {team:1} before {player:0} sealed victory after {player:6}'s shot was cleared off the line.
The result leaves {team:1} just outside the relegation zone in 19th place on goal difference, one point above the bottom three, with the home side now just outside the bottom two on goal difference.
{team:2}'s winless run is now six games in a row and they are now four points from safety in 16th place.
{team:1} manager {manager:0} told BBC Radio {team:4}:      

                             
     