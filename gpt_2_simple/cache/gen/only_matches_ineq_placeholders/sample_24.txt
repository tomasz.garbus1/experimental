<|startoftext|>{team:0} > {team:1}
{player:0} scored twice as {team:0} thumped {team:1} to move above their opponents into second place in {league:0}.
{player:1}'s first goal for the club came in the 17th minute and, after going down under a challenge from {player:2}, the {country:0} striker smashed home from 18 yards out.
{player:0} added his second, turning in a {player:3} cross from close range, before {player:4} converted a penalty to seal the win.
{team:0} remain fourth in the table, but have now won their last four league matches, level on points with {team:2}.
{team:1} are now back to third place, two points behind {team:0} and just three above the bottom three, having now lost their last two games.
{team:0} manager {manager:0}:
"We deserved to win the game and that's all three points.
"We thoroughly deserved to win. We took it very seriously, we came here to win and we've gone and done it.
"Our resilience was excellent and we could have been more clinical at the end, but it's not been easy."
{team:1} manager {manager:1}:
"We've lost five in six, but I'm