<|startoftext|>{team:0} > {team:1}
Two goals in three minutes helped {team:0} beat {team:1} in {league:0} at {stadium:0}.
{player:0} gave the hosts the lead in the first half with {player:1}'s deflected shot making it 2-1 before the break.
{team:1}'s {player:2} equalised in the second half but {player:3}'s header restored the hosts' advantage.
{player:4} headed in {team:2}'s third before {player:5} completed the scoring in the dying stages.
The result means {team:0} remain fifth in the {league:0}, four points behind {team:3}, with {team:1} 15th, just three points above the relegation zone.
{team:2} stay in the bottom three, having won just one of their last eight league games.
{team:0} manager {player:6} told BBC Radio {team:4}:

                                
                            
"I thought we deserved it, I thought the