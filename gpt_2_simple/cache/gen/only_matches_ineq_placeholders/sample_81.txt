<|startoftext|>{team:0} == {team:1}
{player:0} scored a dramatic last-minute leveller for {team:0} as his {team:1} side held on to claim a draw at {stadium:0}.
{team:2} goalkeeper {player:1} came close to denying {team:3} debutant {player:2} from close range early on.
But {team:1} were in front when {player:3} fired home from a tight angle after excellent work by {player:4}.
{team:1} levelled when {player:5}'s shot was cleared off the line by {team:2} centre-half {player:6}.
{team:1} remain third in the table, but {team:2} are now just a point outside the automatic promotion places.
{team:0} manager {manager:0} told BBC Radio {team:4}: 
"I thought we deserved it. We were the better side. We were the better side for the first half until the second goal.
"It's the first time I've said this season that I've been honest with the players and what we expected.
"And it was a really good performance and I thought we deserved the draw.
"We've come here and been able to win a lot of games and we're two points clear of third-placed {team