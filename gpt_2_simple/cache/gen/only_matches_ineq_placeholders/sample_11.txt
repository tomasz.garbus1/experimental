<|startoftext|>{team:0} > {team:1}
Right-back {player:0} scored a hat-trick as {team:0} thrashed {team:1} to reach the {league:0} last four.
The visitors went ahead when {player:1} converted {player:2}'s cross at the near post.
{player:0} then made it 2-0 before the break with a 10-yard free-kick.
{player:3} hit the bar for {team:1} before {player:0} scored a hat-trick from the edge of the area and {player:4} headed one for the home side.
{team:2} are now three points above the relegation zone and three ahead of bottom side {team:3}, who have two games in hand over them.
In-form {team:0} host {team:4} on Tuesday, while {team:1} are now four points off safety with just two games left to play.
{team:0} manager {manager:0} told BBC Radio {team:5}:  

                             
                         
"It was a