<|startoftext|>{team:0} == {team:1}
{team:1} goalkeeper {player:0} made two vital saves to earn his side a point on his return to the {stadium:0} against {team:0} in the {league:0}.
{player:0} saved from {player:1}'s header and then twice denied {player:2} in a clear-cut opening period.
{team:1} then had {player:3} and {player:4} sent off in the second half for two bookings in the space of a minute.
But {team:0} earned the draw their play deserved with a {player:5} equaliser.
{team:2} jumped out to a 3-0 lead in the opening 10 minutes thanks to {player:3}'s penalty and {player:6}'s curling free-kick. 
{team:1} were soon level as {player:1}'s header from a corner set up {player:3} to score from close range after {player:7}'s weak attempt at a back-heel.
{player:4} and {player:8} both had efforts blocked for the visitors before the break, but {team:0} began to take the game to {team:1} in the second period. 
{team:3}'s {player: