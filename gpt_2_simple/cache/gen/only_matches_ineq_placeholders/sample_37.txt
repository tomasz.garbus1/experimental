<|startoftext|>{team:0} > {team:1}
{team:0} moved three points clear of the drop zone after a rousing win over {team:1}.
{player:0}'s first goal in {team:2}'s top-flight since January gave them the lead before the late dismissal of {player:1} for a second booking.
{team:3} substitute {player:2} levelled but {player:3}'s header won it.
Match ends, {team:0} 3, {team:1} 1.
Second Half ends, {team:0} 3, {team:1} 1.
Attempt missed. {player:4} ({team:1}) right footed shot from outside the box is close, but misses to the left.
Substitution, {team:0}. {player:5} replaces {player:3}.
Offside, {team:0}. {player:6} tries a through ball, but {player:7} is caught offside.
Corner,  {team:0}. Conceded by {player:8}.
Attempt saved. {player:9} ({team:1}) right footed shot from the centre of the box is saved in the bottom right corner. Assisted by {player:10}.
Corner,  {team:1}. Conceded by {player:11