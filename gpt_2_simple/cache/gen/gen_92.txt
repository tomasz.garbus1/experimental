league:Champions League
stadium:
manager:Jacek Magiera
player:
team:Legia Warszawa,Borussia Monchengladbach
country:Poland,Spain,Italy,Mexico,Switzerland
::Kare Ince: Legia Warszawa sign Borussia Monchengladbach striker::
@1434772131
url: https://www.bbc.com/sport/football/32400842
Champions League finalists Legia Warszawa have signed Borussia Monchengladbach striker Marek Suchy on a two-year deal.
Suchy, 23, scored the winner in Poland's 2-1 win over Spain in the previous round.
He is the 24-year-old's second signing for Legia after the arrival of midfielder Marko Jankovic from the Polish champions.
Jacek Magiera's side face Italy in the first leg of their quarter-final against Switzerland on 19 March.
They will be based in Poland's FK Klattnica in the capital Warsaw.
The former Chelsea youth player scored just six goals in 11 games for German side Hannover last season.
He has also played for Monchengladbach in the Champions League in Italy and Mexico.