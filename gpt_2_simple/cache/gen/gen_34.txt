league:
stadium:
manager:Petr Ulicny
player:
team:Sigma Olomouc
country:Czech Republic
::Sigma Olomouc boss Petr Ulicny says club will not get loan::
@1434774789
url: https://www.bbc.com/sport/football/33056418
Sigma Olomouc boss Petr Ulicny says his squad will not get a loan from the Czech Republic.
The team are currently second in their domestic league, a point behind leaders Dinamo Zagreb.
"We will not be getting a loan from the Czech Republic," Ulicny told the club's website.
"We have done a lot of work, but these things are not easy for any team."
He added: "We can only work hard to help the team."
Ulicny's side have been one point off the top of the table in the last three games but they are without a win in their last six games, conceding four goals.
"We have been playing well and it's not easy to keep the same level of concentration and intensity," said Ulicny. "It is a difficult situation.
"We cannot do much to make the points count."
The team's current record is seven wins and four draws from their last six games.