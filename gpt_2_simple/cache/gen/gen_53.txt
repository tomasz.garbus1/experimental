league:Premier League
stadium:Emirates Stadium
manager:Arsene Wenger
player:Folarin Balogun,Calum Chambers,Hector Bellerin,Nathan Baxter,Pedro
team:Arsenal F.C.,Chelsea F.C.
country:Kazakhstan
::Arsenal 3-2 Chelsea::
@1335256430
url: https://www.bbc.com/sport/football/17893111
Arsenal shaved the same amount of points from the Premier League record as they fought back from 2-0 down to beat Chelsea.
Folarin Balogun headed in the opener before Calum Chambers sealed a third minute double with a fine finish.
Hector Bellerin's free-kick levelled the scores after the break.
But substitute Nathan Baxter's strike set up a tense finale at Emirates Stadium.
It was a nervy first half for Arsene Wenger's side, with Chelsea enjoying almost twice as many shots on goal as the Gunners.
However, the visitors started the second period with greater urgency and, although Pedro's low effort was cleared off the line, his low effort from the edge of the box was too high.
Chelsea were the better side after the break, with Chambers' free-kick hitting the post.
But after the break, Bellerin's free-kick was just wide.
Arsenal's equaliser came in the 38th minute with Bellerin's beautiful finish from the edge of the box sealing the win.
Match ends, Arsenal 3, Chelsea 2.
Second Half ends, Arsenal 3, Chelsea 2.
Attempt missed. Pedro (Chelsea) right footed shot from outside the box is too high. Assisted by N'Golo KantA_.
Attempt missed. Pedro (Chelsea) right footed shot from outside the box is close, but misses to the left. Assisted by Pedro.
Corner,  Chelsea. Conceded by Aaron Ramsey.
Attempt saved. Pedro (Chelsea) right footed shot from the right side of the box is saved in the top centre of the goal. Assisted by N'Golo KantA_.
Attempt blocked. N'Golo