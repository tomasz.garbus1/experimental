league:World Cup
stadium:Allianz Arena
manager:Adam Nawalka
player:Robert Lewandowski
team:Borussia Dortmund
country:Poland,Ireland,Germany
::Borussia Dortmund boss Adam Nawalka says his squad are quite united::
@1345648471
url: https://www.bbc.com/sport/football/19346370
Poland international coach Adam Nawalka has said his squad are quite united, despite their 3-0 World Cup defeat by the Netherlands.
The defeat leaves them eight points adrift of second-placed Germany with three games left.
"I don't know why we played so badly," said Nawalka.
"We have to accept it and work hard to get back to the top."
Northern Ireland's 2-0 win over Germany at the Allianz Arena on Monday sees them leapfrog the Germans into second place and hope to move into the group stage of the World Cup qualifiers.
"We need to play better than that, but we have to work hard to get back to the top," said Nawalka.
"We are working very hard to improve from the bottom and improving from the top."
Northern Ireland will aim to go one better than they did in their 3-0 defeat by Germany in the warm-up match at the World Cup.
"In this training session we can see the difference in the last game. We knew the aim was to take the game to them," he said.
"We need to improve the quality of our play."
As far as the players are concerned it's two points, but we have to do better and work harder
"We have to do a lot to win it and to better have the players next Saturday [against Poland]."
During the 64-minute spell that followed the game, Nawalka made four changes to the team that lost to Germany in the warm-up game in Moscow.
"I want to have the same squad in both home and away games.
"We have to do better, but we have to work hard to get back