league:
stadium:
manager:Petr Ulicny
player:
team:Sigma Olomouc
country:Czech Republic,Hungary,Ukraine,Romania
::Czech Republic 1-1 Hungary::
@1489191862
url: https://www.bbc.com/sport/football/39376973
Sigma Olomouc's first European game of the season ended in defeat as Hungary came from behind to defeat the Czech Republic.
The visitors opened the scoring when goalkeeper Petr Ulicny saved Michal Lagorek's penalty after the break.
Ulicny made a string of saves to keep the match goalless, including one from the spot.
Romania, who won 2-0 in Ukraine to reach the group stages, could have added more goals but for a superb goalkeeper.
The Czechs are top of Group D on goal difference.
Czech Republic manager Petr Ulic:
"Hungary deserved the win, but we worked hard to create a lot of chances.
"We had all the chances to score but we missed several chances, especially in the first half.
"We had a lot of possession and we did not take advantage of their back-pass but we made it difficult for them to score."
Hungary coach Ion Bezjajic:
"We have to try to do something positive in the second half. If we did that, it would be a very difficult game.
"But we showed our spirit and fighting spirit and the players showed that."
Match ends, Hungary 1, Czech Republic 1.
Second Half ends, Hungary 1, Czech Republic 1.
Corner,  Hungary. Conceded by Michal Lagorek.
Attempt blocked. Leif Kjell (Hungary) left footed shot from the left side of the box is blocked.
Substitution, Hungary. Leif Kjell replaces Michal Tauluz.
Attempt blocked. Leif Kjell (Hungary) right footed shot from outside