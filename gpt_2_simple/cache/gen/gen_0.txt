league:World Cup
stadium:Allianz Arena
manager:Adam Nawalka
player:Robert Lewandowski
team:Borussia Dortmund
country:Poland,Ireland,Germany,Ukraine,Italy,Netherlands,Belgium,Switzerland,Senegal,Nigeria,South Africa,South Africa,Cameroon,Bosnia and Herzegovina,Brazil,Brazilian
::World Cup qualifiers: Poland 2-0 Germany::
@1457418673
url: https://www.bbc.com/sport/football/35949551
Germany's World Cup qualifying campaign began as a tale of two legs as they lost 2-0 at home to Poland in Warsaw.
The game was played in the Eastern region, where Poland have won five of their last six matches.
But it was in Poland that the home side held a strong start with goals from Robert Lewandowski and Mats Hummels.
But the Polish defence was often poor, with Lewandowski's late strike a particularly impressive moment.
The winning goal came after 71 minutes when Lewandowski's low shot found the bottom right corner of Hummels' far post.
Poland's defeat means they have yet to pick up a win in their last six matches.
The defeat was particularly disappointing as, in the five previous matches, the Poles had won all three of them.
The meeting of the two sides will be played at the Allianz Arena in Warsaw on Wednesday, 16 December, and is the first of three Group B fixtures in the process.
The first four games are scheduled to be played in the capital or the surrounding area.
The other three will be played in the capital or the area.
In Group B, Croatia, Italy, Germany and the Netherlands, who are the other three sides in contention for a place in the finals, are the other four teams to make it into the play-offs.
Group A
Group B
Group C
Group D
Group E
Group F
Group G