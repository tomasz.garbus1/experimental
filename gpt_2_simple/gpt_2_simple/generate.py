import gpt_2_simple as gpt2
import os


model_name = "124M"

sess = gpt2.start_tf_sess()
gpt2.load_gpt2(sess)
while True:
    print("Provide prefix:")
    inp = input()
    gpt2.generate(sess, prefix=inp, truncate='<|endoftext|>')
