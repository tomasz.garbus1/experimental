import gpt_2_simple as gpt2
import os


model_name = "124M"
if not os.path.isdir('/models/%s' % model_name):
    gpt2.download_gpt2(model_name=model_name)   # model is saved into current directory under /models/124M/

sess = gpt2.start_tf_sess()
gpt2.finetune(sess,
              '../data/bbcsport_gpt2',
              model_name=model_name,
              steps=16000)   # steps is max number of training steps

gpt2.generate(sess)
