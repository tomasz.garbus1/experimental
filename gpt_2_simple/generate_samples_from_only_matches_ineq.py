"""
Generates samples from the model finetuned on the data/bbcsport_only_matches_ineq.py
"""
from gpt_2_simple.gpt_2_simple.gpt_2 import *


def main():
    out_dir = 'gpt_2_simple/cache/gen/only_matches_ineq'
    num_samples = 100 - len(os.listdir(out_dir))
    model_name = "355M"
    os.makedirs(out_dir, exist_ok=True)
    checkpoint_dir = '../gpt-2-simple/checkpoints_only_matches_ineq'
    sess = start_tf_sess()
    load_gpt2(sess, checkpoint_dir=checkpoint_dir)
    for _ in tqdm(range(num_samples)):
        gen = generate(sess, length=300, prefix='<|startoftext|>',
                       truncate='<|endoftext|>', checkpoint_dir=checkpoint_dir, return_as_list=True)[0]

        fname = 'sample_%d.txt' % len(os.listdir(out_dir))
        with open(os.path.join(out_dir, fname), 'w+') as fp:
            fp.write(str(gen))


if __name__ == '__main__':
    main()
