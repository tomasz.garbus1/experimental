import argparse
import json
import re
from collections import defaultdict

import matplotlib.pyplot as plt


def longest_cohesive_prefix(art):
    votes = defaultdict(int)
    for vote in art['votes']:
        votes[vote['num_lines']] += (1 if vote['real'] else -1)
    # print(votes.keys())
    for i in range(10, 0, -1):
        if votes[i] > 0:
            return i
    # for i in range(11):
    #     if votes[i] < 0:
    #         return i
    return i + 1


def main():
    parser = argparse.ArgumentParser(prog='Text Quiz results plotter')
    parser.add_argument('-i', type=str, action='store', required=True)
    args = parser.parse_args()
    with open(args.i, 'r') as fp:
        content = json.load(fp)
    for a in content:
        a['votes_gen'] = 0
        a['votes_real'] = 0
        for vote in a['votes']:
            if vote['real']:
                a['votes_real'] += 1
            else:
                a['votes_gen'] += 1
    print('Total votes: %d' % (sum(map(lambda a: a['votes_gen'] + a['votes_real'],
                                       content))))
    print('Total votes real: %d' % (sum(map(lambda a: a['votes_real'],
                                            content))))
    print('Total votes gen: %d' % (sum(map(lambda a: a['votes_gen'],
                                           content))))
    print('Max votes: %d' % (max(map(lambda a: a['votes_gen'] + a['votes_real'],
                                     content))))
    print('Min votes: %d' % (min(map(lambda a: a['votes_gen'] + a['votes_real'],
                                     content))))
    print('Unvoted: %d' % (len(list(filter(lambda a: a['votes_gen'] + a['votes_real'] == 0,
                                           content)))))
    # plt.hist(list(map(lambda a: a['votes_gen'] + a['votes_real'], content)))
    # plt.show()
    only_real = list(filter(lambda a: a['real'], content))
    only_gen = list(filter(lambda a: not a['real'], content))

    r_real = len(list(filter(lambda a: a['votes_real'] > a['votes_gen'], only_real)))
    r_draw = len(list(filter(lambda a: a['votes_real'] == a['votes_gen'], only_real)))
    r_gen = len(list(filter(lambda a: a['votes_real'] < a['votes_gen'], only_real)))
    print(r_real, r_draw, r_gen)
    g_real = len(list(filter(lambda a: a['votes_real'] > a['votes_gen'], only_gen)))
    g_draw = len(list(filter(lambda a: a['votes_real'] == a['votes_gen'], only_gen)))
    g_gen = len(list(filter(lambda a: a['votes_real'] < a['votes_gen'], only_gen)))
    print(g_real, g_draw, g_gen)

    plt.bar((0, 1), (r_real, g_real), label='votes_real > votes_gen')
    plt.bar((0, 1), (r_draw, g_draw), bottom=(r_real, g_real), label='votes_real = votes_gen')
    plt.bar((0, 1), (r_gen, g_gen), bottom=(r_real + r_draw, g_real + g_draw), label='votes_real < votes_gen')
    plt.xticks((0, 1), ('Real articles', 'Generated articles'))
    plt.legend()
    plt.show()

    num_votes = [0 for _ in range(11)]
    for a in content:
        for vote in a['votes']:
            num_votes[vote['num_lines']] += 1
    print(num_votes)

    # nlvals = [2, 3, 4, 5, 6, 7, 8, 9, 10]
    # g_real = []
    # g_draw = []
    # g_gen = []
    # for num_lines in nlvals:
    #     for a in content:
    #         a['votes_gen'] = 0
    #         a['votes_real'] = 0
    #         for vote in a['votes']:
    #             if vote['num_lines'] != num_lines:
    #                 continue
    #             if vote['real']:
    #                 a['votes_real'] += 1
    #             else:
    #                 a['votes_gen'] += 1
    #     only_gen = list(filter(lambda a: not a['real'], content))
    #     g_real.append(len(list(filter(lambda a: a['votes_real'] > a['votes_gen'], only_gen))))
    #     g_draw.append(len(list(filter(lambda a: a['votes_real'] == a['votes_gen'], only_gen))))
    #     g_gen.append(len(list(filter(lambda a: a['votes_real'] < a['votes_gen'], only_gen))))
    #
    # plt.bar(nlvals, g_real, label='votes_real > votes_gen')
    # plt.bar(nlvals, g_draw, bottom=g_real, label='votes_real = votes_gen')
    # plt.bar(nlvals, g_gen, bottom=list(map(lambda a: a[0] + a[1], zip(g_real, g_draw))), label='votes_real < votes_gen')
    # plt.xticks(nlvals, list(map(str, nlvals)))
    # plt.xlabel('number of lines displayed')
    # plt.legend()
    # plt.show()

    those_that_fooled = []
    for art in content:
        if not art['real'] and art['votes_real'] > art['votes_gen']:
            those_that_fooled.append({
                'title': art['title'],
                'text': re.sub('\n', '\\\\\\\\ \n', art['content']),
                'votes_real': art['votes_real'],
                'votes_gen': art['votes_gen']
            })
    those_that_fooled.sort(key=lambda a: (a['votes_real'] / (a['votes_real']+a['votes_gen'])),
                           reverse=True)
    for a in those_that_fooled:
        print(a['title'])
        print(a['text'])
        print('\n\n\n\n')
    #
    # good_prefs = []
    # for a in content:
    #     if a['real']:
    #         continue
    #     good_prefs.append(longest_cohesive_prefix(a))
    # plt.hist(good_prefs, bins=9)
    # plt.show()
    #
    # votes_real = [0 for _ in range(11)]
    # votes_gen = [0 for _ in range(11)]
    # for a in content:
    #     if a['real']:
    #         continue
    #     for vote in a['votes']:
    #         if vote['real']:
    #             votes_real[vote['num_lines']] += 1
    #         else:
    #             votes_gen[vote['num_lines']] += 1
    # for i in range(2, 11):
    #     print(i, votes_real[i] / (votes_real[i] + votes_gen[i]))


if __name__ == '__main__':
    main()
