from collections import defaultdict


def main():
    with open('gpt_2_simple/cache/gen/only_matches_ineq_placeholders/results.txt', 'r') as fp:
        content = fp.read()
    num_no = 0
    num_ok = 0
    cohesive_prefixes = []
    cp_dict = defaultdict(int)
    for line in content.splitlines():
        if not line:
            continue
        num, verdict = line.split(':')
        if verdict == 'NO':
            num_no += 1
        elif verdict == 'OK':
            num_ok += 1
        else:
            assert verdict.startswith('OK')
            cohesive_prefixes.append(int(verdict.split(',')[1]))
            cp_dict[cohesive_prefixes[-1]] += 1
    assert num_ok + num_no + len(cohesive_prefixes) == 100
    print('NO: %d' % num_no)
    print('OK: %d' % num_ok)
    for i in range(15):
        print('OK,%d: %d' % (i, cp_dict[i]))


if __name__ == '__main__':
    main()
