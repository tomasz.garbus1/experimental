"""
Populates samples from gpt_2_simple/cache/gen/placeholders with random values.
"""
import os
import random
import re
from collections import defaultdict

import numpy as np
from tqdm import tqdm

from common.utils import list_all_files
from ner.highlighter import CATEGORIES

IN_DIR = 'gpt_2_simple/cache/gen/only_matches_ineq_placeholders'
OUT_DIR = 'gpt_2_simple/cache/gen/placeholders_filled'
# Used for geometric distribution:
P = 0.3
VALUES = defaultdict(list)


def fill(text: str) -> str:
    for cat in CATEGORIES:
        for d in list(range(20)):
            pattern = '{%s:%d}' % (cat, d)
            if pattern in text:
                name = random.choice(VALUES[cat])
                variants = name.split(';')
                variants = list(filter(lambda v: '[' not in v, variants))
                text = re.sub(pattern, variants[0].strip(), text, count=1)
                while pattern in text:
                    text = re.sub(pattern,
                                  variants[min(len(variants)-1,
                                               np.random.geometric(P))].strip(),
                                  text,
                                  count=1)
    return text


def init():
    global VALUES
    print("initializing")
    for cat in CATEGORIES:
        for f in tqdm(os.listdir('ner/entities/en/%s' % cat)):
            if f != 'eng.csv' and cat not in ['country', 'league']:
                continue
            fname = os.path.join('ner/entities/en/%s' % cat, f)
            with open(fname, 'r') as fp:
                for line in fp.readlines():
                    if not line:
                        continue
                    name = line.split(',')[0]
                    variants = name.split(';')
                    variants = list(filter(lambda v: '[' not in v, variants))
                    if variants:
                        VALUES[cat].append(name)
    print("done")


def main():
    os.makedirs(OUT_DIR, exist_ok=True)
    for f in tqdm(list_all_files(IN_DIR)):
        with open(os.path.join(IN_DIR, f), 'r') as fp:
            text = fp.read()
        text = fill(text)
        with open(os.path.join(OUT_DIR, f), 'w') as fp:
            fp.write(text)


if __name__ == '__main__':
    init()
    main()
