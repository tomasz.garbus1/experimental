"""
Generates samples from the model finetuned on the data/bbcsport_pl.
"""
from gpt_2_simple.gpt_2_simple.gpt_2 import *
import argparse


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--mxsamples', type=int, default=100, required=False)
    args = parser.parse_args()
    return args.mxsamples

def main():
    mxsamples = parse_args()
    out_dir = 'gpt_2_simple/cache/gen/placeholders'
    os.makedirs(out_dir, exist_ok=True)
    num_samples = min(mxsamples, 100 - len(os.listdir(out_dir)))
    model_name = "355M"
    checkpoint_dir = '../gpt-2-simple/checkpoints_pl'
    sess = start_tf_sess()
    load_gpt2(sess, checkpoint_dir=checkpoint_dir)
    for _ in tqdm(range(num_samples)):
        gen = generate(sess, length=300, prefix='<|startoftext|>',
                       truncate='<|endoftext|>', checkpoint_dir=checkpoint_dir, return_as_list=True)[0]

        fname = 'sample_%d.txt' % len(os.listdir(out_dir))
        with open(os.path.join(out_dir, fname), 'w+') as fp:
            fp.write(str(gen))


if __name__ == '__main__':
    main()
