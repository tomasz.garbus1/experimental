import os
import re

from regex import regex
from tqdm import tqdm
from trans import trans

IN_DIR = '../data/bbcsport_pl'
OUT_DIR = '../data/bbcsport_only_matches_ineq_pl'


def title_to_ineq_or_none(title: str):
    if regex.match('[a-zA-Z{}:\d\s]*\s\d-\d\s[a-zA-Z{}:\d\s]*$', title):
        for i in range(10):
            for j in range(10):
                if '%d-%d' % (i, j) in title:
                    rep = '=='
                    if i > j:
                        rep = '>'
                    if i < j:
                        rep = '<'
                    title = re.sub('%d-%d' % (i, j), rep, title)
                    return title
    return None


def main():
    c = 0
    for d in tqdm(os.listdir(IN_DIR)):
        if not os.path.isdir(os.path.join(IN_DIR, d)):
            continue
        dir = os.path.join(IN_DIR, d)
        for f in os.listdir(dir):
            with open(os.path.join(dir, f), 'r') as fp:
                content = fp.read()
            content = trans(content)
            title = content.splitlines()[0]
            title = title[2:-2]
            title = title_to_ineq_or_none(title)

            if title is not None:
                c += 1
                content = title + '\n' + '\n'.join(content.splitlines()[3:])
                # print(content)

                os.makedirs(os.path.join(OUT_DIR, d), exist_ok=True)
                out_path = os.path.join(OUT_DIR, d, f)

                with open(out_path, 'w+') as op:
                    op.write(content)


if __name__ == '__main__':
    main()
    pass
