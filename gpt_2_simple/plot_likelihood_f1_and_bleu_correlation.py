"""
Plots the correlation between prompt likelihood and F1 and BLEU scores of
generated samples.
"""
import os
from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np

from common.data_helper import DataHelper
from common.utils import load_tokenizer
from seq_gan.discriminators.bleu_discriminator import BLEUDiscriminator


def plot_f1():
    values = []
    max_size = 0
    with open('gpt_2_simple/cache/likelihood_experiments_results.txt', 'r') as fp:
        lines = fp.read().split('\n')
        for line in lines:
            if line == '':
                continue
            likelihood, size, f1 = list(map(lambda s: s.split(':')[1], line.split(',')))
            likelihood = float(likelihood)
            size = int(size)
            f1 = float(f1)
            values.append((likelihood, size, f1))
            max_size = max(size, max_size)
    values.sort()
    lhoods = list(map(lambda a: a[0], values))
    sizes = list(map(lambda a: a[1] / max_size, values))
    f1s = list(map(lambda a: a[2], values))
    plt.plot(lhoods, sizes, label='itemset size / max itemset size')
    plt.plot(lhoods, f1s, label='F1')
    plt.plot(lhoods, [np.mean(f1s[max(0, i - 15):i + 15]) for i in range(len(f1s))], label='F1 smoothed')
    plt.legend()
    plt.show()

    s_to_f1 = defaultdict(list)
    for _, s, f1 in values:
        if s <= 60:
            s_to_f1[s].append(f1)
    values = list(map(lambda a: (a[0], np.mean(a[1])), s_to_f1.items()))
    values.sort()

    x = range(1, 61)
    y = [np.mean(s_to_f1[i]) for i in range(1, 61)]
    yerr = [[np.mean(s_to_f1[i]) - np.min(s_to_f1[i]) for i in range(1, 61)],
            [np.max(s_to_f1[i]) - np.mean(s_to_f1[i]) for i in range(1, 61)]]
    plt.errorbar(x=x,
                 y=y,
                 yerr=yerr,
                 ecolor='r',
                 capsize=5,
                 label='F1')

    sizes = list(map(lambda a: a[0], values))
    f1s = list(map(lambda a: a[1], values))
    plt.plot(sizes, f1s, '-')
    plt.legend()
    plt.show()


def main():
    plot_f1()


if __name__ == '__main__':
    main()
