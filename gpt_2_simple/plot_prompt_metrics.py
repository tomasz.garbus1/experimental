"""
Plots the correlation between nsamples and F1 score of generated samples.
"""
from collections import defaultdict
import matplotlib.pyplot as plt
import numpy as np


def main():
    with open('gpt_2_simple/cache/gen/metrics.txt', 'r') as fp:
        content = fp.read()
    lines = content.split('\n')
    lines = list(filter(lambda l: l != '', lines))

    metrics = [defaultdict(list) for _ in range(21)]
    for line in lines:
        nsamples, f1, prec, rec = list(map(lambda v: float(v.split(': ')[1]),
                                           line.split(',')))
        nsamples = int(nsamples)
        metrics[nsamples]['f1'].append(f1)
        metrics[nsamples]['precision'].append(prec)
        metrics[nsamples]['recall'].append(rec)

    print(metrics)

    def plot_metric(metric: str):
        plt.errorbar(x=range(1, 21),
                     y=[np.mean(metrics[i][metric]) for i in range(1, 21)],
                     yerr=[np.std(metrics[i][metric]) for i in range(1, 21)],
                     ecolor='r',
                     capsize=5,
                     label=metric)
        plt.legend()
        plt.show()

    plot_metric('f1')
    plot_metric('precision')
    plot_metric('recall')


if __name__ == '__main__':
    main()
