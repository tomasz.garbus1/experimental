"""
PromptLikelihood is a class which aims to estimate a likelihood of a given
entity set occurring in the dataset. Note that the predicted values are not
stricly probability and may occupy different range, may sum to different value
than 1 (over all entity sets), but the linear order of entity sets defined by
this likelihood function should to some extent resemble the one defined by
probability function.

The likelihood measure for given set E is defined as:
p(set size = |E|) * avg_{F \subset E, 1 <= |F| <= 3} p(F is a subset of all entities in article)
"""
import argparse
import itertools
import numpy as np
import sys
from collections import defaultdict
from tqdm import tqdm
import matplotlib.pyplot as plt
import os

if not sys.version.startswith('3.5'):
    from efficient_apriori import apriori

from common.utils import load_all_articles
import tensorflow as tf
if tf.__version__ != '2.0.0':
    from gpt_2_simple.gpt_2_simple.gpt_2 import *
from ner.highlighter import Annotator, CATEGORIES, Highlighter
from ner.utils import load_cached_entity_sets, extract_entity_sets, cache_entity_sets, prec_rec_f1_score, \
    entity_set_to_prompt

PROMPTS_DIR = 'gpt_2_simple/gen_prompts/likelihood_experiment'


class PromptLikelihood:
    def __init__(self, input_dir: str):
        """
        Initializes the instance from a directory of articles.
        """
        self.entities_cache_path = 'gpt_2_simple/cache/entities.pickle'
        self.itemsets_cache_path = 'gpt_2_simple/cache/itemsets.pickle'
        self.entities = load_cached_entity_sets(self.entities_cache_path)
        if not self.entities:
            self.articles = load_all_articles(input_dir)
            self.entities = extract_entity_sets(articles=self.articles)
            cache_entity_sets(self.entities, self.entities_cache_path)

        print('Preprocessing PromptLikelihood instance -- computing itemsets')
        itemsets = load_cached_entity_sets(self.itemsets_cache_path)
        if not itemsets:
            itemsets, _ = apriori(self.entities, min_support=0.01,
                                  min_confidence=0.05, verbosity=1, max_length=3)
            cache_entity_sets(itemsets, self.itemsets_cache_path)
        print(len(itemsets[1]))
        print(len(itemsets[2]))
        print(len(itemsets[3]))
        self.support = defaultdict(float)
        self.set_sizes_p = defaultdict(float)
        for entity_set in tqdm(self.entities):
            self.set_sizes_p[len(entity_set)] += 1./float(len(self.entities))
        for size in [1, 2, 3]:
            for itemset in itemsets[size]:
                self.support[itemset] += float(itemsets[size][itemset])/float(len(self.entities))

        for es in self.entities:
            for ent in es:
                self.support[(ent,)] = 1./float(len(self.entities))

        self.itemsets = [None, [], [], []]
        self.p = [None, [], [], []]
        for itemset, prob in self.support.items():
            self.itemsets[len(itemset)].append(itemset)
            self.p[len(itemset)].append(prob)

        for i in [1, 2, 3]:
            self.p[i] = np.array(self.p[i])
            self.p[i] *= 1. / np.sum(self.p[i])

    def predict(self, itemset) -> float:
        """
        Computes the likelihood of given |itemset|.
        """
        p_subset = []
        for size in [1, 2, 3]:
            tmp = []
            for comb in itertools.combinations(itemset, size):
                tmp.append(self.support[comb])
            if tmp != []:
                p_subset.append(np.mean(tmp))

        return float(np.mean(p_subset)) * self.set_sizes_p[len(itemset)] * 1000

    def generate_random(self, set_size=None) -> [set, float]:
        """
        Generates random set of entities from the likelihood distribution.
        :return: Set and its likelihood.
        """
        if set_size is None:
            set_sizes, p_size = tuple(zip(*self.set_sizes_p.items()))
            set_size = 0
            while set_size == 0:
                set_size = np.random.choice(set_sizes, p=p_size)

        itemset = set()
        while len(itemset) < set_size:
            num_elems_to_add = np.random.randint(1, 1 + min(3, set_size - len(itemset)))
            elems_to_add = self.itemsets[num_elems_to_add][np.random.choice(len(self.itemsets[num_elems_to_add]),
                                                                            p=self.p[num_elems_to_add])]
            itemset = itemset.union(elems_to_add)

        return itemset, self.predict(itemset)


def generate_prompts():
    # Generate samples with near-uniform distribution.
    pl = PromptLikelihood(input_dir='../data/bbcsport')
    nrandoms = 2500
    nsamples = 200
    eps = 15
    all = []
    for _ in tqdm(range(nrandoms + eps)):
        itemset, likelihood = pl.generate_random()
        all.append((likelihood, itemset))
    all.sort(key=lambda a: a[0], reverse=True)
    all = all[eps:]

    bsize_min = 0.
    bsize_max = 1.
    while True:
        bsize_mid = (bsize_min + bsize_max) / 2.
        chosen = []
        for i in range(len(all)):
            if chosen != []:
                print(all[i][0], chosen[-1][0], bsize_mid)
            if chosen == [] or chosen[-1][0] - all[i][0] >= bsize_mid or i == len(all) - 1:
                chosen.append(all[i])
        if len(chosen) == nsamples:
            break
        if len(chosen) < nsamples:
            bsize_max = bsize_mid
        else:
            bsize_min = bsize_mid
        print(len(chosen), bsize_mid)
    print(len(chosen))
    plt.plot(list(map(lambda a: a[0], all)), [0] * nrandoms, 'ro')
    plt.plot(list(map(lambda a: a[0], chosen)), [0] * nsamples, 'bo')
    plt.show()

    for likelihood, itemset in chosen:
        fname = 'sample_%f_%d' % (likelihood, len(itemset))
        with open(os.path.join(PROMPTS_DIR, fname), 'w+') as file:
            file.write(entity_set_to_prompt(itemset))


def generate_prompts_by_size():
    # Generate prompts with uniform distribution of sizes from 1 to 60.
    pl = PromptLikelihood(input_dir='../data/bbcsport')
    all = []
    for set_size in range(1, 61):
        for _ in range(5):
            itemset, likelihood = pl.generate_random(set_size=set_size)
            all.append((likelihood, itemset))

    for likelihood, itemset in all:
        fname = 'sample_%f%d_%d' % (likelihood, np.random.randint(1, 10000), len(itemset))
        with open(os.path.join(PROMPTS_DIR, fname), 'w+') as file:
            file.write(entity_set_to_prompt(itemset))


def load_prompt_from_file(fname: str, highlighter: Highlighter) -> (str, set):
    entities = set()
    with open(fname, 'r') as file:
        content = file.read()
    for line in content.split('\n'):
        if line == '':
            continue
        cat = line.split(':')[0]
        if len(line.split(':')) > 1:
            vals = line.split(':')[1].split(',')
        else:
            vals = []
        assert cat in CATEGORIES
        for val in vals:
            if val in highlighter.trie:
                for e in highlighter.trie[val]:
                    if e[1] == cat:
                        val = e[0]
                        break
            entities.add((val, cat))
    return content, entities


def run_experiment(checkpoint_dir: str, max_steps: int):
    nsamples = 6
    sess = start_tf_sess()
    load_gpt2(sess, checkpoint_dir=checkpoint_dir)
    annotator = Annotator('default')
    num_gen = 0
    for f in tqdm(os.listdir(PROMPTS_DIR)):
        if not f.startswith('sample_'):
            continue
        out_cache_path = os.path.join('gpt_2_simple/cache/gen', f)
        if os.path.exists(out_cache_path):
            continue
        inp, entities = load_prompt_from_file(os.path.join(PROMPTS_DIR, f),
                                              annotator)
        gen = generate(sess, length=300, nsamples=nsamples, prefix=inp,
                       truncate='<|endoftext|>', checkpoint_dir=checkpoint_dir,
                       return_as_list=True)
        variants = []
        # Select best one.
        for variant in gen:
            variant_entities = annotator.add_art(variant[len(inp):])
            prec, rec, f1 = prec_rec_f1_score(variant_entities, entities)
            variants.append((f1, prec, rec, variant))
        variants.sort(reverse=True)

        with open(out_cache_path, 'a+') as outf:
            outf.write(variants[0][3])

        _, likelihood, itemset_size = f.split('_')
        with open('gpt_2_simple/cache/likelihood_experiments_results.txt', 'a+') as fp:
            fp.write('likelihood:%s,itemset_size:%s,f1:%f\n' % (
                likelihood, itemset_size, variants[0][0]))

        num_gen += 1
        if num_gen > max_steps:
            return


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--mode', type=str, choices=['generate', 'run', 'generate_by_size'],
                        required=True)
    parser.add_argument('--checkpoint', action='store', type=str,
                        required=True, help='checkpoint path')
    parser.add_argument('--steps', action='store', type=int, required=False,
                        help='for how many steps should the experiment run'
                             '(to prevent memory overflow)', default=1000)
    args = parser.parse_args()
    if args.mode == 'generate':
        generate_prompts()
    elif args.mode == 'generate_by_size':
        generate_prompts_by_size()
    else:
        run_experiment(args.checkpoint, args.steps)

