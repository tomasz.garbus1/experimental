import os
import re
from collections import defaultdict
from typing import List, Optional, Tuple

import tensorflow as tf
import numpy as np
from tqdm import tqdm

FILTERS_DEFAULT = '!"#$%&()*+,-./:;<>?@[\\]^_`{|}~\t\n'  # unfiltered: '='


class DataHelper:
    def __init__(self,
                 tokenizer: tf.keras.preprocessing.text.Tokenizer,
                 sequence_length: Optional[int],
                 val_split: float = 0.01,
                 only_headlines: bool = False,
                 pad_sequences: bool = True):
        self.articles = []  #: List[str] = []
        self.urls = []  #: List[str] = []
        self.tokenizer = tokenizer
        self.sequence_length = sequence_length
        self.val_split = val_split
        self.pad_sequences = pad_sequences
        self.ngram_occurrences = None
        self.only_headlines = only_headlines

    def preprocess_article(self, art: str) -> Tuple[str, str]:
        """
        Preprocesses the article before feeding it to tokenizer.
        :param art: Raw article scraped by the Scraper™.
        :return: A pair (preprocessed article text, article url).
        """
        # Remove the lines containing URL and timestamp.
        art_lines = art.split('\n')
        art_url = art_lines[2]
        art_lines[0] = art_lines[0][2:-2]
        # Add a marker for the header end.
        art_lines = [art_lines[0], ' =headerend= '] + art_lines[3:]
        art_lines = list(filter(lambda line: line.strip() != '', art_lines))
        # Add a marker for the article end.
        art_lines += [' =articleend= ']

        if self.only_headlines:
            art = art_lines[0]
        else:
            art = '\n'.join(art_lines)

        # Add special markers for dots.
        art = re.sub('Piłka nożna - Sport\.pl', '', art)
        art = re.sub('\.', ' =dot= ', art)
        art = re.sub(',', ' =comma= ', art)

        return art, art_url

    @staticmethod
    def remove_markers(art: str) -> str:
        """ Translates the special tokens to human-readable form. """
        art = re.sub('=dot=', '. ', art)
        art = re.sub('=comma=', ', ', art)
        art = re.sub('=headerend=', '\n\n\n', art)
        art = re.sub(' \. ', '. ', art)
        art = re.sub(' , ', ', ', art)
        return art

    def initialize_input(self, input_dir: str):
        print("Loading articles")
        for date in tqdm(os.listdir(input_dir)):
            if not os.path.isdir(os.path.join(input_dir, date)):
                continue
            for fname in os.listdir(os.path.join(input_dir, date)):
                fullpath = os.path.join(input_dir, date, fname)
                with open(fullpath, 'r') as f:
                    art = f.read()
                art, art_url = self.preprocess_article(art)
                self.articles.append(art)
                self.urls.append(art_url)

    def fit_tokenizer_on_articles(self):
        print("Fitting the tokenizer")
        self.tokenizer.fit_on_texts(self.articles)
        print("Found %d distinct tokens." % len(self.tokenizer.word_counts))

    @staticmethod
    def one_hot(val, dim):
        ret = np.zeros((1, dim))
        ret[0, val] = 1
        return ret

    def article_to_seq(self, art: str, min_len=None) -> List[int]:
        seq = self.tokenizer.texts_to_sequences([art])[0]
        if self.sequence_length is not None and self.pad_sequences:
            seq = [0] * (self.sequence_length - 1) + seq
        else:
            seq = [0] * min_len + seq

        # Delete consecutive interpunction tokens.
        def is_interpunction_or_unk(token: int):
            return (self.tokenizer.sequences_to_texts([[token]])[0] in
                    ['=dot=', '=comma=', '=unk='])
        seq2 = []
        was_previous_interpunction = False
        for i in range(len(seq)):
            if is_interpunction_or_unk(seq[i]) and was_previous_interpunction:
                continue
            seq2.append(seq[i])
            was_previous_interpunction = is_interpunction_or_unk(seq[i])
        seq = seq2

        if min_len is not None and len(seq) < min_len:
            seq = [0] * (min_len - len(seq)) + seq

        return seq

    def _get_first_val_index(self) -> int:
        """
        Returns the index of the first article belonging to the validation set.
        """
        return int(len(self.articles) * (1. - self.val_split))

    def get_train_articles(self) -> List[str]:
        return self.articles[:self._get_first_val_index()]

    def get_val_articles(self) -> List[str]:
        return self.articles[self._get_first_val_index():]

    def get_train_urls(self) -> List[str]:
        return self.urls[:self._get_first_val_index()]

    def get_val_urls(self) -> List[str]:
        return self.urls[self._get_first_val_index():]

    def init_for_bleu(self):
        print("Initializing DataHelper for BLEU evaluation")
        self.ngram_occurrences = defaultdict(int)
        for n in range(1, 6):
            print("n = %d" % n)
            for art in tqdm(self.articles):
                occ = defaultdict(int)
                art_seq = self.tokenizer.texts_to_sequences([art])[0]
                for w in zip(*[art_seq[i:] for i in range(n)]):
                    occ[w] += 1
                for w in occ.keys():
                    self.ngram_occurrences[w] = max(self.ngram_occurrences[w],
                                                    occ[w])

    def evaluate_text_bleu(self, text: str, n: int) -> float:
        seq = self.tokenizer.texts_to_sequences([text])[0]
        numerator = 0.
        denominator = 0.
        occ = defaultdict(int)
        for w in zip(*[seq[i:] for i in range(n)]):
            occ[w] += 1
        for w in occ.keys():
            numerator += min(occ[w], self.ngram_occurrences[w])
            denominator += occ[w]
        if denominator == 0.:
            return 0.
        return numerator / denominator
