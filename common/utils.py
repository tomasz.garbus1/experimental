import os
import pickle
from random import shuffle
from typing import Optional, List

import numpy as np
import tensorflow as tf
from tqdm import tqdm


def save_tokenizer(tokenizer: tf.keras.preprocessing.text.Tokenizer,
                   fname: str) -> None:
    path = os.path.split(fname)[0]
    if path != '':
        os.makedirs(path, exist_ok=True)
    with open(fname, 'wb+') as file:
        pickle.dump(tokenizer, file)


def load_tokenizer(fname) -> Optional[tf.keras.preprocessing.text.Tokenizer]:
    if os.path.isfile(fname):
        with open(fname, 'rb+') as file:
            return pickle.load(file)
    else:
        return None


def load_all_articles(path: str) -> List[str]:
    """
    Loads all articles from the provided data directory and returns them as
    as list of strings in random order.
    """
    print("Loading articles from %s" % path)
    articles = []
    for d in tqdm(os.listdir(path)):
        if not os.path.isdir(os.path.join(path, d)):
            with open(os.path.join(path, d), 'r') as file:
                articles.append(file.read())
            continue
        for f in os.listdir(os.path.join(path, d)):
            with open(os.path.join(path, d, f), 'r') as file:
                articles.append(file.read())
    shuffle(articles)
    return articles


def smooth_arr(arr, r):
    """
    Smooths the array values by taking an average from given radius |r|. Useful
    for plots.
    """
    return [np.mean(arr[max(0, i-r):i+r]) for i in range(len(arr))]


def list_all_files(dir):
    ret = []
    for e in os.listdir(dir):
        if os.path.isfile(os.path.join(dir, e)):
            ret.append(e)
        elif os.path.isdir(os.path.join(dir, e)):
            ret += list(map(lambda f: os.path.join(e, f),
                            list_all_files(os.path.join(dir, e))))
    return ret
