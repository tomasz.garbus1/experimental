import numpy as np
import tensorflow as tf
from tqdm import tqdm

from common.data_helper import DataHelper


class DataProvider(tf.keras.utils.Sequence):
    def __init__(self, helper: DataHelper, batch_size, sparse=False,
                 full_sequence=False, validation=False):
        self.batch_size = batch_size
        print("Initializing data provider (validation=%s)" % validation)
        self.seqs = []
        self.samples = []
        self.sequence_length = helper.sequence_length
        self.num_words = helper.tokenizer.num_words
        self.sparse = sparse
        self.full_sequence = full_sequence
        articles = (helper.get_train_articles() if not validation else
                    helper.get_val_articles())
        for i, art in tqdm(list(enumerate(articles))):
            seq = helper.article_to_seq(art, min_len=self.sequence_length+2)
            self.seqs.append(seq)
            for offset in range(self.sequence_length, len(seq)):
                self.samples.append((i, offset))
        np.random.shuffle(self.samples)
        print("%d samples" % len(self.samples))

    def __getitem__(self, index):
        x = []
        y = []
        for i in range(index * self.batch_size, (index + 1) * self.batch_size):
            seq_idx, offset = self.samples[i]
            xseq = self.seqs[seq_idx][offset-self.sequence_length:offset]
            if self.full_sequence:
                yval = self.seqs[seq_idx][offset - self.sequence_length + 1:offset + 1]
            else:
                yval = self.seqs[seq_idx][offset]
            x.append(np.array([xseq]))
            y.append(yval)
        x = np.concatenate(x)
        if self.sparse:
            y = np.array(y)
        else:
            y = tf.keras.utils.to_categorical(y, self.num_words + 1)
        return x, y

    def __len__(self):
        return len(self.samples) // self.batch_size
