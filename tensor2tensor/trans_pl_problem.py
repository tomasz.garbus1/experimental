import os
import re

from tensor2tensor.data_generators import problem
from tensor2tensor.data_generators import text_problems
from tensor2tensor.utils import registry


def list_all_files(dir):
    ret = []
    for e in os.listdir(dir):
        if os.path.isfile(os.path.join(dir, e)):
            ret.append(e)
        elif os.path.isdir(os.path.join(dir, e)):
            ret += list(map(lambda f: os.path.join(e, f),
                            list_all_files(os.path.join(dir, e))))
    return ret


@registry.register_problem
class TransPlProblem(text_problems.Text2TextProblem):
    DATA_DIR = 'training_data'

    @property
    def max_subtoken_length(self):
        return 8

    @property
    def is_generate_per_split(self):
        return False

    def __init__(self, was_reversed=False, was_copy=False):
        super().__init__(was_reversed, was_copy)
        files = list_all_files(self.DATA_DIR)
        self.all_prompts = []
        self.all_targets = []
        for f in files:
            with open(os.path.join(self.DATA_DIR, f), 'r') as fp:
                content = fp.readlines()
            num_prompt_lines = 0
            for line in content:
                if line.startswith('::'):
                    break
                num_prompt_lines += 1
            prompt = '\n'.join(content[:num_prompt_lines])
            self.all_prompts.append(prompt)
            target = content[num_prompt_lines:]
            target = [target[0]] + target[3:]
            target = '\n'.join(target)
            self.all_targets.append(target)

    def generate_samples(self, data_dir, tmp_dir, dataset_split):
        for prompt, target in zip(self.all_prompts, self.all_targets):
            yield {
                'inputs': prompt,
                'targets': target,
            }
