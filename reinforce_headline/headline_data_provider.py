import numpy as np
import tensorflow as tf

from reinforce_headline.constants import SEQ_LEN, PSEQ_LEN
from reinforce_headline.headline_data_helper import HeadlineDataHelper


class HeadlineDataProvider(tf.keras.utils.Sequence):

    def __init__(self, helper: HeadlineDataHelper,
                 batch_size: int, validation=False):
        self.helper = helper
        self.batch_size = batch_size
        self.samples = []
        self.prompts = []
        print("Initializing HeadlineDataProvider (validation=%s)" % validation)
        self.samples, self.prompts = (helper.get_train_headlines() if not validation else helper.get_val_headlines())
        # for p in self.prompts:
        #     print(p, self.helper.tokenizer.texts_to_sequences([p])[0],
        #           self.helper.tokenizer.sequences_to_texts(self.helper.tokenizer.texts_to_sequences([p])))
        #     print("::::::::")

        inputs0 = self.helper.tokenizer.texts_to_sequences(self.prompts)
        inputs0 = tf.keras.preprocessing.sequence.pad_sequences(
            inputs0, maxlen=PSEQ_LEN, dtype='int32', padding='post',
            truncating='post', value=0
        )
        inputs1 = self.helper.tokenizer.texts_to_sequences(self.samples)
        inputs1 = tf.keras.preprocessing.sequence.pad_sequences(
            inputs1, maxlen=SEQ_LEN, dtype='int32', padding='post',
            truncating='post', value=0
        )
        self.inputs = [inputs0, inputs1]

    def __getitem__(self, index):
        x0 = np.array(
            self.inputs[0][index * self.batch_size:(index + 1) * self.batch_size])
        x1 = np.concatenate([
            np.zeros((self.batch_size, 1)),
            np.array(self.inputs[1][index * self.batch_size:(index + 1) * self.batch_size, :-1])
        ], axis=1)
        y = np.array(self.inputs[1][index * self.batch_size:(index + 1) * self.batch_size])
        return (x0, x1), y

    def __len__(self):
        return len(self.inputs[0]) // self.batch_size

    def get_prompts(self, num=None):
        if num is None:
            num = self.batch_size
        idx = np.random.choice(list(range(len(self.inputs[0]))), size=num)
        return self.inputs[0][idx]
