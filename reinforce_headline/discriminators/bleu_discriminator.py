"""
A simple Discriminator computing BLEU-n metric for provided samples, using the
training set as reference set for BLEU.

The Generator's reward is directly the BLEU score. The decision for provided
sample is:
 * real if BLEU = 1
 * fake if BLEU < 1

Obviously this is heavily overfitted solution BUT the reward appears to be more
valuable for the geneator.

Also, using BLEU discriminator, this should not be considered a GAN setting,
but a PG-BLEU implementation. Note that the Discriminator does not learn during
the adversarial training phase.
"""
import itertools
import os
import pickle
from collections import defaultdict
from typing import Tuple

import numpy as np
import tensorflow as tf
from tqdm import tqdm

from reinforce_headline.headline_data_helper import HeadlineDataHelper
from reinforce_headline.headline_data_provider import HeadlineDataProvider
from reinforce_headline.constants import SEQ_LEN
from reinforce_headline.discriminators.base_discriminator import Discriminator
from reinforce_headline.generators.base_generator import Generator


class BLEUDiscriminator(Discriminator):
    def __init__(self, dhelper: HeadlineDataHelper, n=3):
        """
        :param n: length of n-grams considered by BLEU metric
        """
        super(BLEUDiscriminator, self).__init__(dhelper)
        self.most_occurrences = defaultdict(int)
        self.n = n
        self.cache_path_pre = 'reinforce_headline/cache/bleu_discriminator_pre.pickle'
        # Note that this one's useless as the discriminator does not learn
        # adversarily.
        self.cache_path_adv = 'reinforce_headline/cache/bleu_discriminator_adv.pickle'

    def _unique_ngrams_ratio(self, seq: tf.Tensor, n: int = 1) -> float:
        """
        Computes the ratio of unique ngrams / all ngrams in a batch of
        sequences.
        :param seq: A tensor of shape (batch size, sequence length).
        :param n: n
        :return: A float in range [0..1].
        """
        ngrams = set()
        seqlen = seq.shape[1]
        for i in range(len(seq)):
            for j in range(seqlen - n + 1):
                ngrams.add(tuple(seq[i][j:j+n].numpy().tolist()))
        return len(ngrams)/(len(seq) * (seqlen - n + 1))

    def bleu(self, seq: tf.Tensor) -> tf.Tensor:
        bleus = []
        for x in seq:
            x = list(map(int, x))
            numerator = 0.
            denominator = 0.
            occ = defaultdict(int)
            for w in zip(*[x[i:] for i in range(self.n)]):
                occ[w] += 1
            for w in occ.keys():
                numerator += min(occ[w], self.most_occurrences[w])
                denominator += occ[w]
            cur_bleu = numerator / denominator
            bleus.append([cur_bleu])
        return tf.convert_to_tensor(bleus, dtype=tf.float32)

    def predict(self, prompts: tf.Tensor, seq: tf.Tensor) -> tf.Tensor:
        return self.bleu(seq) * tf.convert_to_tensor(
            1. - 1.666 * np.abs(.6 - self._unique_ngrams_ratio(seq)),
            dtype=tf.float32)

    def train_on_batch(self, x: tf.Tensor, y: tf.Tensor) -> Tuple[float, float]:
        bleus = self.predict(None, x)
        loss = []
        accs = []
        for i in range(len(bleus)):
            loss.append(abs(float(y[i]) - bleus[i]))
            if y[i] == 1:
                accs.append(1. if bleus[i] == 1. else 0.)
            else:
                accs.append(1. if bleus[i] < 1. else 0.)
        return float(np.mean(loss)), float(np.mean(accs))

    def pretrain(self):
        print("Pretraining BLEU discriminator")
        if os.path.exists(self.cache_path_pre):
            print("Loading weights (pre)")
            self.load_from_disk_pre()
            return
        data_provider = HeadlineDataProvider(self.dhelper, batch_size=1)
        for x, _ in tqdm(data_provider):
            occ = defaultdict(int)
            x = list(map(int, x[1][0]))
            assert len(x) == SEQ_LEN
            for w in zip(*[x[i:] for i in range(self.n)]):
                occ[w] += 1
            for w in occ.keys():
                self.most_occurrences[w] = max(self.most_occurrences[w],
                                               occ[w])
            # TODO: just re-generate the cached training sets with the new
            #       changes in DataHelper (no consecutive interpunction)
            # This is temporary workaround to stop rewarding the generator for
            # just sequences of dots and/or commas.
            int_tokens = self.dhelper.tokenizer.texts_to_sequences(
                ['=dot=', '=comma='])
            int_tokens = [int_tokens[0][0], int_tokens[1][0]]
            for w in list(itertools.product(*[int_tokens] * self.n)):
                self.most_occurrences[w] = 0

        del data_provider

        self.save_to_disk_pre()

    def load_from_disk_pre(self):
        with open(self.cache_path_pre, 'rb') as fp:
            self.n, self.most_occurrences = pickle.load(fp)

    def load_from_disk_adv(self):
        with open(self.cache_path_adv, 'rb') as fp:
            self.n, self.most_occurrences = pickle.load(fp)

    def save_to_disk_pre(self):
        with open(self.cache_path_pre, 'wb') as fp:
            pickle.dump((self.n, self.most_occurrences), fp)

    def save_to_disk_adv(self, iter_num):
        with open(self.cache_path_adv, 'wb') as fp:
            pickle.dump((self.n, self.most_occurrences), fp)
