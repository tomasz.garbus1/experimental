from typing import Tuple

import tensorflow as tf

from reinforce_headline.headline_data_helper import HeadlineDataHelper


class Discriminator:
    def __init__(self, dhelper: HeadlineDataHelper):
        self.dhelper = dhelper

    def predict(self, promtps: tf.Tensor, seq: tf.Tensor) -> tf.Tensor:
        raise NotImplementedError()

    def train_on_batch(self, x: tf.Tensor, y: tf.Tensor) -> Tuple[float, float]:
        """
        Returns two values - loss and accuracy.
        """
        raise NotImplementedError()

    def pretrain(self):
        raise NotImplementedError()

    def load_from_disk_pre(self):
        raise NotImplementedError()

    def load_from_disk_adv(self):
        raise NotImplementedError()

    def save_to_disk_pre(self):
        raise NotImplementedError()

    def save_to_disk_adv(self, iter_num):
        raise NotImplementedError()

    def trainable(self):
        """
        True iff the Discriminator can improve from adversarial training.
        """
        return False
