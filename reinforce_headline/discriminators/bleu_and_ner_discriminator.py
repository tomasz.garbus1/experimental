from typing import Tuple

import tensorflow as tf

from reinforce_headline.discriminators.base_discriminator import Discriminator
from reinforce_headline.discriminators.bleu_discriminator import BLEUDiscriminator
from reinforce_headline.discriminators.ner_discriminator import NERDiscriminator
from reinforce_headline.headline_data_helper import HeadlineDataHelper
from reinforce_headline.history import TrainingHistory


class BLEUandNERDiscriminator(Discriminator):
    def __init__(self, dhelper: HeadlineDataHelper,
                 bleu_discriminator: BLEUDiscriminator,
                 ner_discriminator: NERDiscriminator):
        super(BLEUandNERDiscriminator, self).__init__(dhelper)
        self.bleu_discriminator = bleu_discriminator
        self.ner_discriminator = ner_discriminator

    def predict(self, prompts: tf.Tensor, seq: tf.Tensor) -> tf.Tensor:
        pred1 = self.bleu_discriminator.predict(prompts, seq)
        pred2 = self.ner_discriminator.predict(prompts, seq)
        return (pred1 + pred2) / 2

    def bleu(self, prompts: tf.Tensor, seq: tf.Tensor) -> tf.Tensor:
        return self.bleu_discriminator.predict(prompts, seq)

    def fscore(self, prompts: tf.Tensor, seq: tf.Tensor) -> tf.Tensor:
        return self.ner_discriminator.predict(prompts, seq)

    def train_on_batch(self, x: tf.Tensor, y: tf.Tensor) -> Tuple[float, float]:
        pass

    def pretrain(self):
        self.bleu_discriminator.pretrain()
        self.ner_discriminator.pretrain()

    def load_from_disk_pre(self):
        self.bleu_discriminator.load_from_disk_pre()
        self.ner_discriminator.load_from_disk_pre()

    def load_from_disk_adv(self):
        self.load_from_disk_pre()

    def save_to_disk_pre(self):
        self.bleu_discriminator.save_to_disk_pre()
        self.ner_discriminator.save_to_disk_pre()

    def save_to_disk_adv(self, iter_num):
        self.save_to_disk_pre()
