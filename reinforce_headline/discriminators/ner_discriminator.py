import os
import pickle
from typing import Tuple

import tensorflow as tf

from ner.highlighter import Annotator
from ner.utils import prec_rec_f1_score
from reinforce_headline.discriminators.base_discriminator import Discriminator
from reinforce_headline.headline_data_helper import HeadlineDataHelper


class NERDiscriminator(Discriminator):
    def __init__(self, dhelper: HeadlineDataHelper):
        super(NERDiscriminator, self).__init__(dhelper)
        self.annotator = None
        self.cache_path_pre = 'reinforce_headline/cache/ner_discriminator_pre.pickle'
    
    def predict(self, prompts: tf.Tensor, seq: tf.Tensor) -> tf.Tensor:
        ground_truths = list(
            map(self.annotator.add_art,
                self.dhelper.tokenizer.sequences_to_texts(prompts))
        )
        texts = self.dhelper.tokenizer.sequences_to_texts(seq.numpy())
        found_ents = list(map(self.annotator.add_art, texts))
        f1s = list(map(lambda e: [prec_rec_f1_score(e[0], e[1])[2]],
                       zip(found_ents, ground_truths)))
        f1s = list(map(lambda a: [a[0] or 0.], f1s))
        return tf.convert_to_tensor(f1s)

    def train_on_batch(self, x: tf.Tensor, y: tf.Tensor) -> Tuple[float, float]:
        pass

    def pretrain(self):
        print("Pretraining NER discriminator")
        if os.path.isfile(self.cache_path_pre):
            print("Loading from disk")
            self.load_from_disk_pre()
        else:
            self.annotator = Annotator()
            self.save_to_disk_pre()

    def load_from_disk_pre(self):
        with open(self.cache_path_pre, 'rb') as fp:
            self.annotator = pickle.load(fp)

    def load_from_disk_adv(self):
        self.load_from_disk_pre()

    def save_to_disk_pre(self):
        with open(self.cache_path_pre, 'wb') as fp:
            pickle.dump(self.annotator, fp)

    def save_to_disk_adv(self, iter_num):
        self.save_to_disk_pre()
