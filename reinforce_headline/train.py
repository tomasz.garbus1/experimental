import argparse
import os
import tensorflow as tf

from common.utils import load_tokenizer, save_tokenizer
from ner.highlighter import Annotator
from reinforce_headline.constants import VOCAB_SIZE, LOWER_TOKENS, SEQ_LEN, EMBEDDING_DIM, LSTM_UNITS
from reinforce_headline.discriminators.bleu_and_ner_discriminator import BLEUandNERDiscriminator
from reinforce_headline.discriminators.bleu_discriminator import BLEUDiscriminator
from reinforce_headline.discriminators.ner_discriminator import NERDiscriminator
from reinforce_headline.generators.basic_lstm_sequence_output import BasicLSTMSequenceOutput
from reinforce_headline.headline_data_helper import HeadlineDataHelper
from reinforce_headline.history import TrainingHistory
from reinforce_headline.model import SeqGanModel

INPUT_DIR = None
LANGUAGE = 'en'


def parse_args():
    global LANGUAGE, INPUT_DIR
    parser = argparse.ArgumentParser()
    parser.add_argument('--pl', action='store_true')
    parser.add_argument('-i', action='store', type=str, required=True,
                        help='input directory')
    args = parser.parse_args()
    if args.pl:
        LANGUAGE = 'pl'
    INPUT_DIR = args.i


def main():
    parse_args()

    global INPUT_DIR
    os.makedirs('reinforce_headline/cache', exist_ok=True)
    tokenizer = load_tokenizer('reinforce_headline/cache/tokenizer')
    need_to_fit_tokenizer = (tokenizer is None)
    if tokenizer is None:
        tokenizer = tf.keras.preprocessing.text.Tokenizer(VOCAB_SIZE, "",
                                                          LOWER_TOKENS,
                                                          oov_token='=unk=')
    dhelper = HeadlineDataHelper(tokenizer, sequence_length=SEQ_LEN,
                                 val_split=0.01)
    dhelper.initialize_input(INPUT_DIR)
    if need_to_fit_tokenizer:
        dhelper.fit_tokenizer_on_articles()
    save_tokenizer(dhelper.tokenizer, 'reinforce_headline/cache/tokenizer')
    generator = BasicLSTMSequenceOutput(embedding_dim=EMBEDDING_DIM,
                                        state_size=LSTM_UNITS,
                                        dhelper=dhelper,
                                        start_token=tokenizer.texts_to_sequences(['=start='])[0][0])
    bleu_discriminator = BLEUDiscriminator(dhelper, n=3)
    ner_discriminator = NERDiscriminator(dhelper)
    discriminator = BLEUandNERDiscriminator(dhelper, bleu_discriminator, ner_discriminator)
    model = SeqGanModel(generator, discriminator, dhelper)
    model.train()


if __name__ == '__main__':
    main()
