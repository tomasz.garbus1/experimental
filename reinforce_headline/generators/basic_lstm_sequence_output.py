import os
from typing import List, Tuple, Optional

import numpy as np
import tensorflow as tf

from reinforce_headline.constants import *
from reinforce_headline.generators.base_generator import Generator
from reinforce_headline.generators.step_metrics import GeneratorStepMetrics
# Helper type for internal LSTM state.
from reinforce_headline.headline_data_helper import HeadlineDataHelper
from reinforce_headline.headline_data_provider import HeadlineDataProvider

LSTMState = Tuple[tf.Tensor, tf.Tensor]
# List of gradients corresponding to a single step output.
Gradients = List[tf.Tensor]


class BasicLSTMSequenceOutput(Generator):
    def __init__(self, embedding_dim: int, state_size: int,
                 dhelper: HeadlineDataHelper, start_token: int):
        super().__init__()
        self.embedding_dim = embedding_dim
        self.state_size = state_size
        self.dhelper = dhelper
        self.start_token = start_token

        # Learning rate
        self.eta = 0.001

        # Last step metrics.
        self.step_metrics = GeneratorStepMetrics.zeros()
        # self.step_metrics: GeneratorStepMetrics = GeneratorStepMetrics.zeros()

        # Builds Keras model.
        self.embedding = tf.keras.layers.Embedding(input_dim=VOCAB_SIZE + 1,
                                                   output_dim=embedding_dim)
        self.lstm_cell_read = tf.keras.layers.LSTMCell(units=state_size)
        self.lstm_read = tf.keras.layers.RNN(self.lstm_cell_read)
        self.lstm_cell_gen = tf.keras.layers.LSTMCell(units=state_size)
        self.lstm_gen = tf.keras.layers.RNN(self.lstm_cell_gen,
                                            return_sequences=True)
        self.dense = tf.keras.layers.Dense(units=VOCAB_SIZE + 1,
                                           activation='softmax')

        self.input_read = tf.keras.layers.Input(shape=(PSEQ_LEN,))
        self.input_gen = tf.keras.layers.Input(shape=(SEQ_LEN,))
        lstm_state = self.lstm_read(self.embedding(self.input_read))
        # lstm_state = [lstm_state, lstm_state]
        emb_inp_gen = self.embedding(self.input_gen)
        lstm_gen_in = tf.keras.layers.concatenate(
            [
                emb_inp_gen,
                tf.tile(tf.reshape(lstm_state,
                                   [-1, 1, LSTM_UNITS]), [1, SEQ_LEN, 1])
            ],
            axis=2
        )
        lstm_gen_out = self.lstm_gen(lstm_gen_in)
        output = self.dense(lstm_gen_out)
        self.keras_model = tf.keras.Model(
            inputs=(self.input_read, self.input_gen), outputs=output)

        # Sets up the optimizer.
        self.grad_sources = self.keras_model.trainable_weights

        self.opt = tf.optimizers.Adam(learning_rate=0.0005)

        self.keras_model.compile(optimizer=self.opt,
                                 loss='sparse_categorical_crossentropy',
                                 metrics=['acc'])

        self.pre_weights_path = 'reinforce_headline/cache/generator_keras_model_pre.h5'
        self.adv_weights_path = 'reinforce_headline/cache/generator_keras_model_adv.h5'

    def _update_step_metrics(self, output: tf.Tensor,
                             h_state: tf.Tensor):
        """ Updates the GeneratorStepMetrics for a single timestep. """
        var = tf.keras.backend.var(output, axis=0)
        var = tf.reduce_mean(var)
        self.step_metrics.variance += var / SEQ_LEN
        h_state_var = tf.keras.backend.var(h_state, axis=0)
        h_state_var = tf.reduce_mean(h_state_var)
        self.step_metrics.h_state_var += h_state_var / SEQ_LEN

    def _generate_timestep(self, context_vec: tf.Tensor,\
                           input_tokens: tf.Tensor, state: LSTMState)\
            -> Tuple[tf.Tensor, LSTMState, tf.Tensor]:
        """
        Performs a single timestep of token generation. If used inside a
        TapeGradient context, the tape.gradient(...) method should work fine.
        Supports batches / multiple rollouts.

        :param context_vec: Context vector.
        :param input_tokens: Input tokens at the current timestep. A typical use
                             would be either a matrix of zeros or tokens
                             generated at previous timestep.
        :param state: Hidden LSTM state.
        :return: A pair (next tokens, new state, probability distributions).
        """
        is3d = len(tf.shape(input_tokens)) == 2
        if is3d:
            assert tf.shape(input_tokens).numpy().tolist() == [BATCH_SIZE_ADV,
                                                               NUM_ROLLOUTS]
            input_tokens = tf.reshape(input_tokens,
                                      (BATCH_SIZE_ADV * NUM_ROLLOUTS,))
            orig_state_shape = tf.shape(state[0])
            flat_state_shape = (BATCH_SIZE_ADV * NUM_ROLLOUTS, self.state_size)
            state = (tf.reshape(state[0], flat_state_shape),
                     tf.reshape(state[1], flat_state_shape))

        num_parallel = tf.shape(input_tokens)[0]
        emb = self.embedding(input_tokens)
        # print(emb.shape, context_vec.shape)
        cell_inp = tf.concat([emb, context_vec], 1)
        lstm_out, state = self.lstm_cell_gen(inputs=cell_inp, states=state)
        output = self.dense(lstm_out)
        np_output = output.numpy()
        next_tokens = [np.random.choice(
            a=list(range(VOCAB_SIZE + 1)),
            p=np_output[r])
            for r in range(num_parallel)]
        next_tokens = tf.convert_to_tensor(np.array(next_tokens),
                                           dtype=tf.int32)
        if is3d:
            state = (tf.reshape(state[0], orig_state_shape),
                     tf.reshape(state[1], orig_state_shape))
            next_tokens = tf.reshape(next_tokens,
                                     (BATCH_SIZE_ADV, NUM_ROLLOUTS))

        self._update_step_metrics(output, lstm_out)
        return next_tokens, state, output

    def rollout_policy(self, prompts: tf.Tensor, prefixes: tf.Tensor) -> tf.Tensor:
        assert len(prefixes) == BATCH_SIZE_ADV

        init_len = tf.shape(prefixes)[1]
        # Construct a 3-D `seq` Tensor. Dimensions are (batch size, #rollouts,
        # sequence length).
        seq = np.zeros([BATCH_SIZE_ADV, NUM_ROLLOUTS, SEQ_LEN], dtype=np.int32)
        for i in range(BATCH_SIZE_ADV):
            for j in range(NUM_ROLLOUTS):
                seq[i, j, -init_len:] = prefixes[i].numpy()
        seq = tf.convert_to_tensor(seq)

        # Run through the generated prefixes to set the internal state.
        state = (tf.zeros((BATCH_SIZE_ADV, NUM_ROLLOUTS, self.state_size)),
                 tf.zeros((BATCH_SIZE_ADV, NUM_ROLLOUTS, self.state_size)))
        # TODO: use tf.repeat instead once migrated to TF2.1
        prompts_rep = np.repeat(prompts, NUM_ROLLOUTS, axis=0)
        prompts = tf.convert_to_tensor(prompts_rep)
        context_vec = self.lstm_read(self.embedding(prompts))
        # state = (state, state)
        orig_state_shape = tf.shape(state[0])
        flat_state_shape = (BATCH_SIZE_ADV * NUM_ROLLOUTS, self.state_size)
        state = (tf.reshape(state[0], flat_state_shape),
                 tf.reshape(state[1], flat_state_shape))
        for i in range(init_len-1):
            emb = self.embedding(tf.reshape(seq[:, :, i],
                                            (BATCH_SIZE_ADV * NUM_ROLLOUTS,)))
            cell_in = tf.concat([emb, context_vec], 1)
            _, state = self.lstm_cell_gen(inputs=cell_in, states=state)
        state = (tf.reshape(state[0], orig_state_shape),
                 tf.reshape(state[1], orig_state_shape))
        cur_tokens = seq[:, :, init_len-1]

        # Now rollout the policies.
        for i in range(init_len-1, SEQ_LEN):
            cur_tokens, state, output = self._generate_timestep(context_vec, cur_tokens, state)
            cur_tokens_r = tf.reshape(cur_tokens,
                                      [BATCH_SIZE_ADV, NUM_ROLLOUTS, 1])
            seq = tf.concat([seq[:, :, 1:], cur_tokens_r], axis=2)
        return seq

    def generate_sequences(self, num, prompts) -> Tuple[tf.Tensor, List[List[Gradients]]]:
        """
        Generates |num| sequences and returns gradients of logarithms of
        probability distributions at each step. List of gradients is returned
        per each (sample in batch, timestep) pair.
        :param num: Number of sequences to generate.
        :return: A tuple (sequences, ∇_Θ log(G_Θ(y_t | Y_{1:t-1}))) at each
                 per each sequence, at each step.
        """
        with tf.GradientTape(persistent=True) as tape:
            assert prompts.shape[0] == num, ("%d, %d" % (prompts.shape, num))
            seq = tf.convert_to_tensor(np.tile(
                self.start_token, (num, SEQ_LEN)), dtype=np.int32)

            log_grads = [[] for _ in range(num)]
            state = tf.zeros((1, self.state_size)), tf.zeros((1, self.state_size))

            context_vec = self.lstm_read(self.embedding(prompts))

            # Use the provided start tokens to set the internal state.
            cur_tokens = seq[:, -1]

            self.step_metrics = GeneratorStepMetrics(
                variance=tf.convert_to_tensor(0.),
                h_state_var=tf.convert_to_tensor(0.))

            for i in range(SEQ_LEN):

                cur_tokens, state, output = self._generate_timestep(context_vec,
                    cur_tokens, state)
                output_log = - tf.math.log(output + tf.keras.backend.epsilon())
                output_log *= tf.keras.utils.to_categorical(cur_tokens,
                                                            VOCAB_SIZE + 1)
                for j in range(num):
                    step_grads = tape.gradient(output_log[j], self.grad_sources)
                    log_grads[j].append(step_grads)

                seq = tf.concat([seq[:, 1:], tf.reshape(cur_tokens, (num, 1))],
                                axis=1)
        return seq, log_grads

    def apply_gradients(self, grads: List[tf.Tensor]):
        # Apply learning rate. Note that there is a separate learning rate set
        # in the ADAM optimizer.
        # TODO: Make the learning rate setting more clear.
        grads = list(map(lambda grad: tf.multiply(grad, self.eta), grads))

        # Apply gradients
        self.opt.apply_gradients(zip(grads, self.grad_sources))

        # for debugging
        norm = tf.reduce_mean(list(map(lambda g: tf.norm(g, ord=2), grads)))
        print("Gradients norm: ", norm)

    def load_from_disk_pre(self):
        print("Loading weights (pre)")
        self.keras_model.load_weights(self.pre_weights_path)

    def load_from_disk_adv(self):
        print("Loading weights (adv)")
        self.keras_model.load_weights(self.adv_weights_path)

    def train_mle(self, n_epoch=20):
        # Initializes data generators.
        data_source = HeadlineDataProvider(self.dhelper,
                                           batch_size=BATCH_SIZE_PRE,
                                           validation=False)
        val_data_source = HeadlineDataProvider(self.dhelper,
                                               batch_size=BATCH_SIZE_PRE,
                                               validation=True)
        callbacks = [
            tf.keras.callbacks.LambdaCallback(
                on_epoch_end=(lambda e, l:
                              self.keras_model.save_weights(
                                  self.pre_weights_path)))
        ]
        self.keras_model.fit_generator(data_source,
                                       validation_data=val_data_source,
                                       epochs=n_epoch,
                                       callbacks=callbacks)

    def load_or_pretrain(self, n_epoch=5):
        print("Pretraining generator")
        if os.path.exists(self.adv_weights_path):
            self.load_from_disk_adv()
        elif os.path.exists(self.pre_weights_path):
            self.load_from_disk_pre()
        else:
            self.train_mle(n_epoch)
            self.keras_model.save_weights(self.pre_weights_path)

    def generate_readable_sequence(self, dhelper: HeadlineDataHelper, prompts):
        seq = self.generate_sequences(1, prompts)[0].numpy()
        text = dhelper.tokenizer.sequences_to_texts(seq)[0]
        return text

    def get_step_metrics(self) -> GeneratorStepMetrics:
        return self.step_metrics

    def save_to_disk_adv(self, iter_num):
        self.keras_model.save_weights(self.adv_weights_path)
