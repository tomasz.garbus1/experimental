"""
Using notation from: https://arxiv.org/pdf/1609.05473.pdf
"""

from typing import Tuple, List

import tensorflow as tf

from seq_gan.generators.step_metrics import GeneratorStepMetrics


class Generator:
    def __init__(self):
        pass

    def generate_sequences(self, num, prompts) -> Tuple[tf.Tensor, List[List[tf.Tensor]]]:
        """
        :param num: Number of sequences to generate.
        :return: A pair:
                 * sequence
                 * ∇_Θ log(G_Θ(y_t | Y_{1:t-1})) per each step of the sequence
        """
        raise NotImplementedError()

    def apply_gradients(self, grads: List[tf.Tensor]):
        raise NotImplementedError()

    def rollout_policy(self, prompts: tf.Tensor, prefixes: tf.Tensor) -> tf.Tensor:
        """
        Performs |NUM_ROLLOUTS| rollouts of the learned policy for each of the
        |prefixes|.

        :param prefixes: A batch of prefixes of the generated sequence.
        :return: A 3-D tensor of shape (batch size, number of rollouts, sequence
                 length).
        """
        raise NotImplementedError()

    def train_mle(self, n_epoch=5):
        raise NotImplemented()

    def load_or_pretrain(self, n_epoch=5):
        raise NotImplementedError()

    def load_from_disk_pre(self):
        raise NotImplementedError()

    def load_from_disk_adv(self):
        raise NotImplementedError()

    def get_step_metrics(self) -> GeneratorStepMetrics:
        raise NotImplementedError()

    def save_to_disk_adv(self, iter_num):
        raise NotImplementedError()
