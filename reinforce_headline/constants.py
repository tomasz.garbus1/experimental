from common.data_helper import FILTERS_DEFAULT


VOCAB_SIZE = 15000
FILTERS = FILTERS_DEFAULT
LOWER_TOKENS = False

SEQ_LEN = 12
PSEQ_LEN = 30
EMBEDDING_DIM = 64
LSTM_UNITS = 128
# Pretraining batch size
BATCH_SIZE_PRE = 32
# Adversarial training batch size
BATCH_SIZE_ADV = 4
NUM_ROLLOUTS = 4
