from typing import Optional

import numpy as np
import tensorflow as tf
from tqdm import tqdm

from reinforce_headline.constants import *
from reinforce_headline.discriminators.base_discriminator import Discriminator
from reinforce_headline.discriminators.bleu_and_ner_discriminator import BLEUandNERDiscriminator
from reinforce_headline.discriminators.bleu_discriminator import BLEUDiscriminator
from reinforce_headline.generators.base_generator import Generator
from reinforce_headline.headline_data_helper import HeadlineDataHelper
from reinforce_headline.headline_data_provider import HeadlineDataProvider
from reinforce_headline.history import TrainingHistory


class SeqGanModel:
    def __init__(self,
                 generator: Generator,
                 discriminator: Discriminator,
                 dhelper: HeadlineDataHelper):
        """
        :param generator: Generator G_Θ
        :param discriminator: Discriminator D_Φ
        """
        self.generator = generator
        self.discriminator = discriminator
        self.dhelper = dhelper
        self.history = TrainingHistory()
        # A queue of generated sequences to help train the discriminator. They
        # are collected when training generator.
        self._generated_seqs = []
        self.data_provider = None

    def pretrain_generator(self, n_epoch=20):
        self.generator.load_or_pretrain(n_epoch)

    def train_generator_mle(self, n_epoch=1):
        self.generator.train_mle(n_epoch)

    def pretrain_discriminator(self):
        self.discriminator.pretrain()

    def _unique_ngrams_ratio(self, seq: tf.Tensor, n: int) -> float:
        """
        Computes the ratio of unique ngrams / all ngrams in a batch of
        sequences.
        :param seq: A tensor of shape (batch size, sequence length).
        :param n: n
        :return: A float in range [0..1].
        """
        ngrams = set()
        for i in range(BATCH_SIZE_ADV):
            for j in range(SEQ_LEN - n + 1):
                ngrams.add(tuple(seq[i][j:j+n].numpy().tolist()))
        return len(ngrams)/(BATCH_SIZE_ADV * (SEQ_LEN - n + 1))

    def _collect_metrics(self, prompts, seq, r, grads):
        """
        Stores current step metrics in the History object.

        """
        self.history.rewards.append(tf.reduce_mean(r))
        self.history.reward_stds.append(tf.math.reduce_std(r))
        self.history.grad_norms.append(
            tf.reduce_mean(list(map(lambda g: tf.norm(g, ord=2), grads))))
        # Compute variance stats.
        self.history.variances.append(
            self.generator.get_step_metrics().get_variance())
        self.history.h_state_vars.append(
            self.generator.get_step_metrics().get_h_state_var())
        # Count distinct tokens ratio.
        self.history.unique_words.append(self._unique_ngrams_ratio(seq, 1))
        # Compute unique bigrams ratio.
        self.history.unique_bigrams.append(self._unique_ngrams_ratio(seq, 2))

        # Append BLEU to history if relevant.
        if type(self.discriminator) is BLEUDiscriminator:
            self.history.bleu.append(tf.reduce_mean(self.discriminator.bleu(seq)))
        if type(self.discriminator) is BLEUandNERDiscriminator:
            self.history.bleu.append(tf.reduce_mean(self.discriminator.bleu(prompts, seq)))
            self.history.fscore.append(tf.reduce_mean(self.discriminator.fscore(prompts, seq)))

    def q_function(self, prompts, seq: tf.Tensor, ts, final_step: bool):
        """
        Q function as defined in SeqGAN paper.
        :param seq: A batch of sequences - a 2D tensor.
        :param ts: timestep number.
        :param final_step: True if this is the final step and no MC rollouts
                           are to be performed
        :return: A single float Tensor with shape (BATCH_SIZE_ADV,).
        """
        if final_step:
            self._generated_seqs.append(seq)
            r = self.discriminator.predict(prompts, seq)
            return r
        rollouts = self.generator.rollout_policy(prompts, seq)
        rewards = []
        for i in range(BATCH_SIZE_ADV):
            r = tf.reduce_mean(self.discriminator.predict(prompts, rollouts[i]))
            rewards.append(r)
        rewards = tf.convert_to_tensor(rewards)
        return rewards

    def fit_generator_step(self):
        prompts = self.data_provider.get_prompts(num=BATCH_SIZE_ADV)
        seq, log_grads = self.generator.generate_sequences(BATCH_SIZE_ADV,
                                                           prompts)
        self._generated_seqs.append(seq)
        assert len(log_grads) == BATCH_SIZE_ADV
        for i in range(BATCH_SIZE_ADV):
            assert len(log_grads[i]) == SEQ_LEN

        res_grads = [tf.zeros(shape=tf.shape(grad)) for grad in log_grads[0][0]]
        for ts in tqdm(range(SEQ_LEN)):
            # Estimate the reward.
            r = self.q_function(prompts, seq[:, :ts + 1], ts, ts == SEQ_LEN - 1)

            self.history.intermediate_rewards[ts].append(tf.reduce_mean(r))
            step_grads = [
                list(map(lambda grad: tf.multiply(grad, r[i]),
                         log_grads[i][ts]))
                for i in range(BATCH_SIZE_ADV)
            ]
            for i in range(BATCH_SIZE_ADV):
                res_grads = list(map(lambda p: tf.add(p[0], p[1]),
                                     zip(res_grads, step_grads[i])))
        self._collect_metrics(prompts, seq, r, res_grads)
        self.generator.apply_gradients(res_grads)

    def fit_discriminator_step(self, iters=10):
        # TODO: This is ugly
        x = self._generated_seqs
        y = []
        for seq in self._generated_seqs:
            for _ in range(len(seq)):
                y.append(0)
        for i in range(len(y)):
            r_seq = self.data_provider[
                np.random.randint(0, len(self.data_provider))][0]
            x.append(r_seq)
            y.append(1)

        print("Fitting discriminator on %d samples" % len(x))
        order = list(range(len(x)))
        np.random.shuffle(order)
        x = np.concatenate(x)[order]
        y = np.array(y)[order]
        losses, accs = [], []
        for _ in tqdm(range(iters)):
            for i in range((len(y) + BATCH_SIZE_PRE - 1) // BATCH_SIZE_PRE):
                beg = i * BATCH_SIZE_PRE
                end = (i+1) * BATCH_SIZE_PRE
                loss, acc = self.discriminator.train_on_batch(
                    tf.convert_to_tensor(x[beg:end]), tf.convert_to_tensor(y[beg:end]))
                losses.append(loss)
                accs.append(acc)
        # Update discriminator metrics.
        self.history.discriminator_losses.append(np.mean(losses))
        self.history.discriminator_acc.append(np.mean(accs))
        # Clear the generates seqs queue.
        self._generated_seqs = []

    def demo_generator(self):
        prompts = self.data_provider.get_prompts(1)
        prompt = self.dhelper.tokenizer.sequences_to_texts(prompts)[0]
        return '[%s] %s' % (prompt,
                            self.generator.generate_readable_sequence(
                                self.dhelper, prompts))

    def train(self):
        self.pretrain_generator()
        self.data_provider = HeadlineDataProvider(self.dhelper,
                                                  batch_size=1)
        print(self.demo_generator())
        self.pretrain_discriminator()

        for _ in range(100000):
            self.fit_generator_step()
            if self.discriminator.trainable():
                self.fit_discriminator_step(1)
            else:
                self._generated_seqs = []

            print("Step %d Last reward %f" %
                  (self.history.timestep, float(self.history.rewards[-1])))
            self.history.timestep += 1

            # TODO: this is implementation-specific.
            self.history.genseqs.append(self.demo_generator())
            print(self.history.genseqs[-1])

            if self.history.timestep > 0 and self.history.timestep % 100 == 99\
                    or self.history.timestep == 9:
                self.history.plot_stats()

                # Cache trained generator and discriminator.
                self.generator.save_to_disk_adv(self.history.timestep)
                self.discriminator.save_to_disk_adv(self.history.timestep)

                # Log all metrics and generated sequences.
                self.history.log()
