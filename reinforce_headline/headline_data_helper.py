import os
import pickle
import re
from typing import Optional, List
import tensorflow as tf
from tqdm import tqdm

from common.utils import load_all_articles
from ner.highlighter import Annotator
from ner.utils import entity_set_to_prompt


class HeadlineDataHelper:
    annotator = None

    def __init__(self,
                 tokenizer: tf.keras.preprocessing.text.Tokenizer,
                 sequence_length: int,
                 val_split: float = 0.01,
                 ):
        self.headlines = []
        self.tokenizer = tokenizer
        self.val_split = val_split
        self.samples = []
        self.prompts = []

        if HeadlineDataHelper.annotator is None:
            ann_cache = 'reinforce_headline/cache/annotator.pickle'
            if os.path.isfile(ann_cache):
                with open(ann_cache, 'rb') as fp:
                    HeadlineDataHelper.annotator = pickle.load(fp)
            else:
                HeadlineDataHelper.annotator = Annotator()
                with open(ann_cache, 'wb') as fp:
                    pickle.dump(HeadlineDataHelper.annotator, fp)

    def fit_tokenizer_on_articles(self):
        print("Fitting the tokenizer")
        self.tokenizer.fit_on_texts(self.headlines + self.prompts)
        print("Found %d distinct tokens." % len(self.tokenizer.word_counts))

    def initialize_input(self, input_dir: str):
        print("Loading articles to HeadlineDataHelper")
        all_articles = load_all_articles(input_dir)
        headlines = list(map(lambda a: a.splitlines()[0][2:-2], all_articles))
        self.headlines = headlines

        for art in tqdm(headlines):
            headline = art.splitlines()[0]
            sample = '=start= ' + headline
            self.samples.append(sample)
            entities = self.annotator.add_art(sample)
            prompt = ' , '.join(list(map(lambda e: e[0], entities)))
            # prompt = entity_set_to_prompt(entities)
            # prompt = re.sub('\n', ' \n ', prompt)
            # prompt = re.sub(':', ' : ', prompt)
            # prompt = re.sub(',', ' , ', prompt)
            self.prompts.append(prompt)

    def _get_first_val_index(self) -> int:
        """
        Returns the index of the first headline belonging to the validation set.
        """
        return int(len(self.headlines) * (1. - self.val_split))

    def get_train_headlines(self) -> (List[str], List[str]):
        return self.headlines[:self._get_first_val_index()], self.prompts[:self._get_first_val_index()]

    def get_val_headlines(self) -> (List[str], List[str]):
        return self.headlines[self._get_first_val_index():], self.prompts[self._get_first_val_index():]
