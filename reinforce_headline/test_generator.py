from random import choice

import numpy as np

from common.utils import load_tokenizer
from reinforce_headline.constants import SEQ_LEN, EMBEDDING_DIM, LSTM_UNITS
from reinforce_headline.generators.basic_lstm_sequence_output import BasicLSTMSequenceOutput
from reinforce_headline.headline_data_helper import HeadlineDataHelper
from reinforce_headline.headline_data_provider import HeadlineDataProvider

INPUT_DIR = None
LANGUAGE = 'en'


def main():
    tokenizer = load_tokenizer('reinforce_headline/cache/tokenizer')
    dhelper = HeadlineDataHelper(tokenizer, sequence_length=SEQ_LEN,
                                 val_split=0.01)
    dhelper.initialize_input('../data/bbcsport')
    provider = HeadlineDataProvider(dhelper, batch_size=1)
    generator = BasicLSTMSequenceOutput(embedding_dim=EMBEDDING_DIM,
                                        state_size=LSTM_UNITS,
                                        dhelper=dhelper,
                                        start_token=tokenizer.texts_to_sequences(['=start='])[0][0])
    generator.load_or_pretrain()
    generator.train_mle(n_epoch=1)
    for _ in range(10):
        prompt = np.array([choice(provider.inputs[0])])
        print(dhelper.tokenizer.sequences_to_texts(prompt))
        print(generator.generate_readable_sequence(dhelper, prompt))


if __name__ == '__main__':
    main()
