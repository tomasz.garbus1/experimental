import argparse

import tensorflow as tf
from keras.callbacks import LambdaCallback

from common.data_helper import DataHelper
from common.data_provider import DataProvider
from common.utils import save_tokenizer, load_tokenizer
from lstm.constants import *
from lstm.generate_samples import generate
from lstm.model import build_model

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='LSTM experiment script')
    parser.add_argument('-i', action='store', type=str, required=True,
                        help='input directory')
    args = parser.parse_args()
    input_dir = args.i

    tokenizer = load_tokenizer('lstm/cache/tokenizer')
    tokenizer_ready = (tokenizer is not None)
    if not tokenizer_ready:
        tokenizer = tf.keras.preprocessing.text.Tokenizer(VOCAB_SIZE, FILTERS,
                                                          LOWER_TOKENS)
    dhelper = DataHelper(tokenizer, sequence_length=SEQ_LEN)
    dhelper.initialize_input(input_dir)
    if not tokenizer_ready:
        dhelper.fit_tokenizer_on_articles()
        save_tokenizer(dhelper.tokenizer, 'lstm/cache/tokenizer')
    generator = DataProvider(dhelper, batch_size=512)
    val_generator = DataProvider(dhelper, 128, validation=True)
    del dhelper

    model = build_model()
    print(model.summary())
    model.fit_generator(generator, validation_data=val_generator, callbacks=[
        LambdaCallback(on_epoch_end=(lambda e, l: model.save('lstm/cache/lstm')))
    ], epochs=10)
    model.save('lstm/cache/lstm')
    generate(model, tokenizer)
