import keras

from lstm.constants import *


def build_model():
    print("Building LSTM model")
    model = keras.models.Sequential([
        keras.layers.Embedding(input_dim=VOCAB_SIZE + 1,
                               output_dim=EMBEDDING_DIM, input_length=SEQ_LEN),
        keras.layers.LSTM(units=LSTM_UNITS),
        keras.layers.Dense(VOCAB_SIZE + 1, activation='softmax')
    ])
    model.compile(optimizer=keras.optimizers.Adam(),
                  loss='categorical_crossentropy', metrics=['acc'])
    return model


def load_model():
    return keras.models.load_model('lstm/cache/lstm')
