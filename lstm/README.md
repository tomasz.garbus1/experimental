# LSTM
Experiments with minimal LSTM model, just feeding 50-100k articles to check the
quality of generated articles. Treat this README as a log of experiments.

## #3 - BBC Sport

Training metrics (last epoch):
`loss: 2.9910 - acc: 0.4154 - val_loss: 2.6464 - val_acc: 0.4132`

Configuration:
* `sport.bbc.com/football` dataset
* dictionary size: 10000
* ~90k samples
* 10 epochs
* embeddings dimension: 100
* LSTM hidden state size: 256
* input sequence length: 24
* tokenizer persists case

Sample:
```
McNeil United boss Keith Hill confirms his outcome 


Hamilton Academical chairman Stewart Law says he has several interviews about
the managerial vacancy.  Asked about Billy I'm looking to strengthen where you
are against all you want to beat Jersey I think we shall see how to think what
we need to give.  In the second period,  committed for a team game in the
European Union Premier League,  the domestic top European manager,  to thrash
Malmo 8 0,  and the Golden Boot Moyes.  It more strengthened the club when
they share Barca on their various class Absolutely not in opposition if it is 
not necessarily the job and that the coaches of the club have experienced,  he
said.  At the time,  Watford said they owe their support for fan ownership, 
while I respect didn't.  Hanlan you could have sold people to create your own
and white windows,  so there is real football to come.  As did the TV money 
has changed,  some of the most clubs on the division contributed from the 
latest rise on the final day.  One of the imagination at home was but,  like
 Swindon Town.  BBC class this football group makes a performance and great
financial awareness and performances when they possibly was able to and have
the owner of Craig Levein some.  in particular,  and everything has been mixed
the month of just three defeats in eight matches because they had had six fierce
men to the play offs.  I remember what we've have got to do,  which we were and
finishing bottom of the table.  =articleend=
```

### 2.1
LSTM hidden state size increased to 256

Sample:
```
WIĘCEJ Remis takiego bramkarza Reprezentacja Polski 


Kamil Glik doznał urazu lewego obrońcy uraz uda się w sierpniu ubiegłego roku
najpierw doznał w niedzielnym meczu z Eintrachtem Frankfurt 2 5.  34 latek grał
w po Euro,  ale po nie miał mieć miejsca w wyjściowym składzie.  Na ostatnim
treningu niektórych zawodników się interwencjami.  To jedno z Polaków grających
w meczu z Bruk Nieciecza,  przy większości spotkania reprezentacji Polski w
krótkim czasie.  Polacy przegrywali jeszcze gorzej.  =articleend=
```

## #2 - 50k samples
Changes:
* added special tokens: =dot=, =comma=, =headerend=, =articleend=
* using validation set when training

Training metrics (last epoch):
`loss: 4.1924 - acc: 0.2685 - val_loss: 4.8253 - val_acc: 0.2432`

Configuration:
* sport.pl dataset
* language size: 10000
* ~50k samples
* 10 epochs
* embeddings dimension: 100
* LSTM hidden state size: 128
* input sequence length: 24
* tokenizer persists case

Sample:
```
reprezentacja Polski przygotowuje się do nowego selekcjonera Reprezentacja Polski .  Jerzy Brzęczek .  Jerzy Brzęczek na konferencji Reprezentacja Polski 


Adam Nawałka ja nie poniósł w marcu 2007 roku .  Damian Kądzior ,  który wówczas mnie i nie mogę zostać powołany wybrany w poniedziałkowej spotkania u Mariusz sytuacja mówi ,  były kapitan Lokomotiwu Moskwa .  Początek 31 czerwca w swoim klubie wystąpił w barwach Bournemouth od 2014 roku .  W reprezentacji .  Sezon 2015 2016 ma 19 lat .  Piłkarz nie strzelił meczu w PSV Eindhoven ,  ale miał na koncie 17 goli .  Co ciekawe ,  był Gonzalo Higuain .  Inter również ostatnie lato .  Już w 3 w barwach zagrał 51 razy mocno ,  tym lepiej w fazie grupowej Ligi Mistrzów ,  Jose Realem Madryt .    REKLAMA Grał jeszcze w spotkaniu towarzyskim w Leeds ,  a wcześniej 2 0 strzelił .  Polak zdobył koronę króla strzelców Bundesligi i strzelił 78 gole i zaliczył 63 asysty w 70 .  minucie .  W tym sezonie Polak oprócz wysokiej bramek ,  zajmuje czwarte miejsce w barwach rywali i zdobył 16 punktów .  REKLAMA =articleend=
```


## #1 - First go with 10k samples
First try after successful implementation of LSTM.
Parameters:
* sport.pl dataset
* language size: 5000
* 10k samples
* 10 epochs of training
* embeddings dimension: 200
* 64 hidden units in LSTM
* input sequence length: 50

Sample:
```
stokowca może sprawić piłka nożna

w sobotę o godz 18 00 w tym sezonie ligi mistrzów w sobotę o godz 18 30 relacja na
żywo w sport pl w aplikacji sport pl live na ios na androida i windows phone w tym
sezonie w lidze mistrzów z
```