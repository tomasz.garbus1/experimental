VOCAB_SIZE = 10000
FILTERS = '!"#$%&()*+,-./:;<>?@[\\]^_`{|}~\t\n'  # unfiltered: '='
SEQ_LEN = 24
EMBEDDING_DIM = 100
LSTM_UNITS = 384
LOWER_TOKENS = False
