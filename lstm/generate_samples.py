import argparse
import os
from typing import Optional

import keras
import numpy as np
import tensorflow as tf
from tqdm import tqdm

from common.data_helper import DataHelper
from common.utils import load_tokenizer
from lstm.constants import *


def generate(model: keras.models.Model,
             tokenizer: tf.keras.preprocessing.text.Tokenizer,
             output_dir: Optional[str] = None):
    seq = [0] * SEQ_LEN
    seqgen = []
    while True:
        output = model.predict(np.array([seq]))
        next_token = np.random.choice(list(range(VOCAB_SIZE + 1)), p=output[0])
        seq = seq[1:] + [next_token]
        seqgen = seqgen + [next_token]
        text = tokenizer.sequences_to_texts([seqgen])[0]
        if text.endswith('=articleend='):
            out = DataHelper.remove_markers(text)
            if output_dir:
                no = len(os.listdir(output_dir))
                with open(os.path.join(output_dir, 'lstm_gen_%d.txt' % no),
                          'w+') as file:
                    file.write(out)
            else:
                print(out)
            break


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='LSTM generate sample script')
    parser.add_argument('-i', action='store', type=str, required=True,
                        help='input directory')
    parser.add_argument('-n', action='store', type=int, required=False,
                        default=1, help='number of samples')
    parser.add_argument('-o', action='store', type=str, required=False,
                        help='output directory')
    args = parser.parse_args()
    input_dir = args.i
    num_samples = args.n
    output_dir = args.o

    model: keras.models.Model = keras.models.load_model('lstm/cache/lstm')
    tokenizer = load_tokenizer('lstm/cache/tokenizer')
    if output_dir:
        os.makedirs(output_dir, exist_ok=True)
        for i in tqdm(range(num_samples)):
            generate(model, tokenizer, output_dir)
    else:
        for i in range(num_samples):
            generate(model, tokenizer)
