::Funding boost for Scotland women::
@1382693846
url: https://www.bbc.com/sport/football/24668309
Manager Anna Signeul believes a new funding agreement for the women's game is a "game-changer" for Scotland.
A Scottish Government initiative will provide £200,000 to cover the qualifying period for the 2015 World Cup and allow players to work less.
"This is going to have a major impact on the day-to-day lives of these players," Signeul told BBC Scotland.
"They are training 15 to 20 hours a week and that is hard to sustain on top of 40 hours work."
Scotland have never reached a major tournament, having missed out on the last two European Championship finals at the play-off stage.
The majority of Scotland players currently fit their football commitments around full-time work or study and the new financial support will allow for more recovery time as well as more strength and conditioning sessions.
"To reduce the work load and be able to rest a little bit more and maybe get some more training time, it's going to make a huge difference," Signeul told BBC Scotland.

                            
                        
"Some of the players had already made the decision to work part-time but not everyone can afford to do that."
Midfielder Kim Little is one of the few Scotland players to earn a wage from playing and she also welcomes the new injection of money.
"I'm very lucky to be at Arsenal and to be able to focus mainly on my football," she said. "It's definitely helped me a lot.
"For Scotland to move to the next level, this is definitely the way to go.
"You can see the difference funding has made to the England team. They are a very good side now - in the top four in Europe.
"In terms of fitness and technique, giving the girls more time to train will improve our national team - without a doubt."
Scotland are seeded second in their World Cup qualifying group, behind Sweden, and have opened with high-scoring wins against the Faroe Islands and Bosnia.
The Scots host Northern Ireland on Saturday and travel to Poland next week.
Meanwhile, Signeul is on a shortlist of 10 candidates for Fifa's Women's Coach of the Year award, with the world governing body describing Scotland as "a quiet success story".