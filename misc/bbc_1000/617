::Euro 2016: Scotland face Republic of Ireland & Gibraltar in qualifiers::
@1393162317
url: https://www.bbc.com/sport/football/26207723
Scotland and the Republic of Ireland have been drawn together in one of the toughest groups in qualifying for the 2016 European Championship.
Their group also includes Germany, Poland, Georgia and Gibraltar.
England face Switzerland, Slovenia, Estonia, Lithuania and San Marino while Wales have drawn Bosnia-Hercegovina, Belgium, Israel, Cyprus and Andorra.

Northern Ireland are in Group F with Greece, Hungary, Romania, Finland and the Faroe Islands.
Euro 2016 is being hosted by France, who qualify automatically, and will be made up of 24 nations, an increase from 16 in the current format.
The top two teams in each of the nine groups will progress to the tournament finals, along with the best third-placed finisher. The remaining eight third-placed teams go into the play-offs in November 2015.
The increased number of teams at the competition will have given the the home nations added belief that they could qualify, but Scotland face a stern challenge.
"Every tie, there's something in it," said Scotland manager Gordon Strachan.
"Some groups are mundane, but we are in an exciting one. It's a terrific, terrific draw. We're excited, but it will be competitive. 
"We've got a hard group, you never know how it will fluctuate over a couple of years, but if we can keep our players fit, we have a chance. 

                            
                        
"We want to keep our momentum going, but this will be a really good level we will play at. If we play at the level we can play, we can definitely qualify."
Scotland's last major tournament appearance came at the 1998 World Cup in France.
The Republic played at Euro 2012 when they qualified for the tournament via the play-offs.
"It's a difficult group but an exciting one, nevertheless," said Republic boss Martin O'Neill.
"Germany are the outstanding team in the group but there's plenty to fight for. Let's be positive and let's go for it.
"With the exception of Germany, it looks like a group where lots of teams will be able to take points off each other and I think it will be tight right until the end."
"To reach France, especially out of this group, would be the utopia. It would be amazing but we have a lot of fighting to do between now and then."

                            
                        
England manager Roy Hodgson will be confident of his side's qualifying chances and he is familiar with Switzerland, having led the country to the 1994 World Cup. 
"I'm very satisfied [with the group], particularly for the fans," said Hodgson. "They've had some quite difficult trips in recent years and this time we go to places that are a little easier to get to and very pleasant to visit.
"Switzerland always brings a smile to my face because I had four fantastic years with them."
England have never played Lithuania and Hodgson added: "I went to Vilnius [the capital] with Fulham, so it's not completely unknown to me. 
"It's a nice place to visit and Lithuania, like many countries in that region, are a good footballing team."
Wales have never played at a European Championship and their last appearance at a major tournament was the 1958 World Cup. 
"I'm happy," said Wales manager Chris Coleman. "The biggest concern is ourselves and the amount of times we can field our best team.
"Bosnia and Belgium are two strong teams, there are no mugs in the group and there will be no easy games. 
"We are not going into it targeting third, we have to win as many games as we can."
Northern Ireland have not qualified for a major tournament since the 1986 World Cup and they have never participated in a European Championship.
"Automatic qualification is the aim but having third place to aim at also helps," said Northern Ireland manager Michael O'Neill. "You could possibly get third place with 14 points, which I think is achievable.
"It's important for us to get points on the board and get momentum."
Gibraltar, taking part for the first time after being accepted as a Uefa member last year, were initially drawn to play against Spain but were moved to Group D to keep the two teams apart and avoid exacerbating political tensions.

                            
                        
Armenia and Azerbaijan were also kept apart for a similar reason after also being drawn in the same group.
Current European champions Spain have been pitted against Ukraine, Slovakia, Belarus, FYR Macedonia and Luxembourg.
The qualification period runs from 7 September 2014 until 13 October 2015. The play-offs will be held over two legs in November, with the draw for the main tournament being held on 12 December 2015. It all gets under way in France on 10 June 2016.
There is a change to the Euro 2016 fixture schedule, with qualifying matches spread over six days rather than being played on just Fridays and Tuesdays.
France will also take part in qualifying, the first hosts to do so after a rule change, although no qualifying points will rest on their fixtures in Group I.
They will play in the finals whatever the outcome of their games.
England begin with a trip to Switzerland on 8 September, while Scotland travel to Germany, Wales visit Andorra, Northern Ireland go to Hungary and the Republic of Ireland face Georgia away.
In the final round of matches, England travel to Lithuania on 12 October 2015, Scotland are away against Gibraltar, the Republic of Ireland visit Poland, Northern Ireland go to Finland and Wales host Andorra.
Group A: Netherlands, Czech Republic, Turkey, Latvia, Iceland, Kazakhstan.
Group B: Bosnia-Hercegovina, Belgium, Israel, Wales, Cyprus, Andorra.
Group C: Spain, Ukraine, Slovakia, Belarus, FYR Macedonia, Luxembourg.
Group D: Germany, Republic of Ireland, Poland, Scotland, Georgia, Gibraltar.
Group E: England, Switzerland, Slovenia, Estonia, Lithuania, San Marino.
Group F: Greece, Hungary, Romania, Finland, Northern Ireland, Faroe Islands.
Group G: Russia, Sweden, Austria, Montenegro, Moldova, Liechtenstein.
Group H: Italy, Croatia, Norway, Bulgaria, Azerbaijan, Malta.
Group I: Portugal, Denmark, Serbia, Armenia, Albania.