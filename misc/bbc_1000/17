::Aberdeen: Derek McInnes remains unfazed by Kris Boyd snub::
@1403976197
url: https://www.bbc.com/sport/football/28072559
Derek McInnes says Aberdeen will not be panicked into new signings ahead of Thursday's Europa League qualifier against Daugava Riga.
The Dons spoke to Kris Boyd about a move to Pittodrie but the striker opted to sign a one-year deal with Rangers.
Manager McInnes, however, says the club will be patient in their search for an alternative front-man.
"It's no secret we're still trying to bring one or two in, we're a bit short up front," he told BBC Radio Scotland.
"We've let three players move on - Scott Vernon, Josh Magennis and Calvin Zola.
"Replacements are needed there so we'll certainly try to get one or two in if we can, ideally for the European game. But, for us it's a long season, it's more important that we get the right one in.
"We could easily jump at one or two things at the minute but we're conscious it's still early in the season - most of the English clubs aren't even back yet and it's important that we try and hold our nerve, be a bit patient to try to get the right one we feel can make a difference.
"We spoke to Kris, I spoke to him and his agent. And, although we never made an offer of such, we did say 'if you feel Aberdeen's the right place for you and you want to come we'll try and do a deal', but obviously he's signed now for Rangers, we move on. 
There was a real enthusiasm and energy about the club last year that's important to manifest and drive forward again
"We're trying to bring players to the club that can improve us, help us challenge on all fronts domestically and in Europe."
The Dons have been back in pre-season training for three weeks, and defeated Arbroath 7-0 in their final warm-up game before Thursday's European clash.
McInnes is well aware the last time Aberdeen faced Latvian opposition in Europe it did not end well, with Skonto Riga knocking Willie Miller's side out on away goals in 1994.
"We feel quite well-versed on them now [Daugava Riga]," he said. "Since the draw's been made we've had plenty of information. 
"When we were playing Brechin the other night they were playing Skonto Riga and Paul Sheerin went over and done a match report for us.
"We've managed to get every game they've played this season and we've concentrated more on the last half a dozen games; the last two and three in particular.
"We feel we know their players enough after watching them, certainly know the system they've been consistently playing. I think we can be as well prepared as we can be going into it.
"It shows there are very few teams in European competition that you can get away with playing poor against and expect to come through. We've just got to try and concentrate on our performance and make sure it is of a high enough level to cause them problems. 
"The biggest thing they have over us is match fitness and game sharpness that we are trying to get.
"There was a real enthusiasm and energy about the club last year that's important to manifest and drive forward again."