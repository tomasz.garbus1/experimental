::Tommy Gemmell: Former Celtic defender and 'Lisbon Lion' dies aged 73::
@1488444679
url: https://www.bbc.com/sport/football/39139454
Celtic have paid tribute to Lisbon Lion Tommy Gemmell, who has died aged 73 following a long illness.
Former Scotland defender Gemmell scored in the 2-1 victory against Inter Milan in 1967 when Celtic became the first British club to win the European Cup.
"Our thoughts are with Mary and Tommy's family and friends at this very difficult time," Celtic said.
Club chief executive Peter Lawwell expressed sadness at the loss of "a true Celtic giant". 
Gemmell also scored in the 1970 European Cup final, which Celtic lost 2-1 to Feyenoord. He spent 10 years at Celtic, between 1961 and 1971, making 418 appearances and scoring 63 goals. 
The right-footed left-back also won 18 Scotland caps, making his debut against England in April 1966 and playing in the famous 3-2 victory over the world champions at Wembley the following year. 
"Everyone at Celtic is deeply saddened by the loss of Tommy, a true Celtic giant and a man who gave the club so many years of his life in an illustrious football career," said Lawwell.
"Tommy was a Celtic great, one of football's greats and I know he will be so sadly missed by everyone who knew him.
"He was a man of huge stature in the game and someone who made such an important mark on Celtic football club.
Listen: Jim Craig explains what Gemmell was like as a player
"In this particular year [the 50th anniversary of the Lisbon Lions' European cup win] it is so very sad to lose such an important figure.  While we mourn his loss, I am sure all our supporters will also celebrate the life and the wonderful achievements of the great Tommy Gemmell."
Fellow Lisbon Lion Bertie Auld says his late team-mate viewed himself as an entertainer.
"Tommy actually thought he was [the actor] Danny Kaye," Auld told BBC Scotland. "He looked like him, but he believed he was.
"And he was, in every degree, because he was an entertainer. 
"He was the best left-back in the world at that time - without fear of contradiction." 
Former Celtic player Murdo MacLeod said it was "very sad news" and described Gemmell as "one of the greats". 
MacLeod, who also had a spell as assistant boss at Celtic, told BBC Scotland: "I know he had been struggling over the last few months.  Just really sad news.  
"He's obviously been one of the greats at Celtic Park to be part of the European Cup-winning side.  
"A top player, one of the first defenders getting forward all the time. [It's] just so sad. We heard Billy McNeill's news [about suffering from dementia] over the last few days and now this.  It's just very sad."
Asked how Gemmell would be remembered, MacLeod said: "Scoring a goal in the European Cup final. To be part of that was just fantastic. The Celtic Lisbon Lions - anywhere they went over the years everybody knew who they were. 
"And for Tommy Gemmell to score a goal in that [1967] European final was just wonderful." 
After retiring as a player with Dundee in 1977, Gemmell managed the club for three years, and also had two spells in charge of Albion Rovers.  
SPFL chief executive Neil Doncaster said: "Tommy is one of the most significant figures in Scottish football history having scored in two European Cup finals, including the famous 1967 victory over Inter Milan in Lisbon. 
"Today's news is particularly poignant with this year being the 50th anniversary of Celtic's achievement in becoming the first British club to win that special trophy."  