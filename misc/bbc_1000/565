::Swansea City 1-0 Burnley::
@1518284333
url: https://www.bbc.com/sport/football/42927828
Swansea City edged further away from the Premier League relegation zone as Ki Sung-yueng's late strike gave them a significant win over Burnley.
A meeting between two of the division's four lowest-scoring teams was predictably light on clear-cut chances, with the game's first shot on target not materialising until Johann Berg Gudmundsson's second-half effort was saved by Lukasz Fabianski.
Burnley looked the likeliest victors for much of the game but the introduction of Swansea's club-record signing Andre Ayew inspired a stirring finish from the hosts.
After his brother Jordan Ayew was denied by the visitors' keeper Nick Pope, the Swans continued to press for the winner, which Ki supplied with a powerful low drive from the edge of the penalty area.

                            
                        
Victory moves Swansea up to 15th in the table - two points clear of the bottom three - and extends their unbeaten run to nine matches.
Burnley, meanwhile, are still without a win in 2018 but remain seventh in the table.
Swansea have been revitalised by the arrival of manager Carlos Carvalhal, who has lost only one of his 11 games since taking charge in December.
During that period the team have played with greater fluency and purpose, while Carvalhal has lifted spirits on and off the field with his eccentric turns of phrase and spontaneous acts of kindness, such as handing out Portuguese custard tarts to journalists in his media conference before this fixture.
Even one of Carvalhal's quirky analogies, however, would have struggled to illuminate this goalless first half.
Swansea had managed only 19 goals in their preceding 26 league fixtures and they struggled to trouble a Burnley defence which had conceded just 23, the fourth best record in the division.
Forward Andre Ayew came on in the second half for his first appearance since rejoining the Swans from West Ham for a club-record £18m, possibly rising to £20m.
His introduction and that of striker Tammy Abraham gave Swansea the presence in Burnley's penalty area they had previously been lacking, and prompted a sustained spell of pressure from the hosts.
Abraham's flick-on set up Jordan Ayew for an instinctive close-range effort which was saved by Pope, and that chance in particular appeared to spur the home side on to push for the winning goal. 
It arrived after some fine work from right-back Kyle Naughton, who wriggled into a promising position on the outskirts of the Burnley box before passing to Ki, whose rasping low drive whistled into the bottom corner. 
Although Burnley have defied the odds to sit in seventh, they have endured a trying start to 2018.
Sean Dyche's men had failed to win their past nine games in all competitions, albeit with a creditable draw against Manchester City in their latest outing.
The Clarets made an assertive start at a rain-sodden Liberty Stadium but, for all their energy and industry, their attacks were limited to long-range efforts such as Sam Vokes' which went wide.
The visitors shaded the early parts of the second half as well, with Fabianski tipping Berg Gudmundsson's half-volley over the bar and Ben Mee heading wide from a free-kick.
But when Swansea increased the tempo in the closing stages, Burnley had no answer. 
They remain seventh - still exceeding expectations - but a 10th game without victory will be a cause for concern for Dyche and his players.
Swansea boss Carlos Carvalhal, speaking to BBC Sport: "I am very happy with my players, very proud of them, they are doing fantastic work. They follow 100% the strategy we create, very professional. 
"We blocked them, we started creating problems and as you know during the game I was not happy because we wanted to win, we felt we really wanted to win which is why as a manager I can start to risk and put more players in attack.
"We put in Andre Ayew, Tammy Abraham; we had three attackers in the middle. We put all the meat in the barbecue, all the meat inside the grill, because we wish a lot to win the game. I am very happy because that created a big impact."
Burnley manager Sean Dyche, speaking to BBC Sport: "It was a tight game - there was nothing in it really. There was action in both boxes but we were not nearly as good as we have been of late. We were a bit huff and puff today, and it feels like the injuries are beginning to take their toll.
"Their goal was a bit of a stuffy one. We didn't clear our lines at the corner and it had a few deflections. The pitch is pretty tricky here, and it wasn't great for either side. 

                            
                        
"But the reality is that a team that eight games ago had nowhere near this kind of endeavour, with the crowd not behind them like they were today, suddenly they have those things. When a new manager comes in, sometimes that is the way it goes."
Swansea travel to Championship side Sheffield Wednesday in the fifth round of the FA Cup on Saturday (12:30 GMT), before a trip to Brighton on 24 February in their next league game.
Burnley are out of the FA Cup and their next match is not until 24 February when they host Southampton.
More to follow.
Match ends, Swansea City 1, Burnley 0.
Second Half ends, Swansea City 1, Burnley 0.
AndrÃ© Ayew (Swansea City) wins a free kick on the right wing.
Foul by Ben Mee (Burnley).
AndrÃ© Ayew (Swansea City) wins a free kick on the right wing.
Foul by Charlie Taylor (Burnley).
AndrÃ© Ayew (Swansea City) is shown the yellow card for a bad foul.
Foul by AndrÃ© Ayew (Swansea City).
Charlie Taylor (Burnley) wins a free kick in the defensive half.
Substitution, Swansea City. Andy King replaces Jordan Ayew.
Corner,  Swansea City. Conceded by Ben Mee.
Attempt blocked. Tammy Abraham (Swansea City) right footed shot from the centre of the box is blocked. Assisted by Ki Sung-yueng.
Attempt saved. Jordan Ayew (Swansea City) right footed shot from the right side of the six yard box is saved in the centre of the goal. Assisted by AndrÃ© Ayew.
Substitution, Burnley. Georges-KÃ©vin Nkoudou replaces Johann Berg Gudmundsson.
Attempt blocked. Nahki Wells (Burnley) left footed shot from outside the box is blocked.
Jordan Ayew (Swansea City) wins a free kick on the right wing.
Foul by Scott Arfield (Burnley).
Goal!  Swansea City 1, Burnley 0. Ki Sung-yueng (Swansea City) right footed shot from outside the box to the bottom left corner. Assisted by Kyle Naughton.
Substitution, Burnley. Nahki Wells replaces Sam Vokes.
Attempt blocked. Kyle Naughton (Swansea City) right footed shot from outside the box is blocked.
Attempt blocked. Jordan Ayew (Swansea City) right footed shot from outside the box is blocked. Assisted by Ki Sung-yueng.
Attempt saved. Jordan Ayew (Swansea City) left footed shot from the centre of the box is saved in the centre of the goal. Assisted by Tammy Abraham with a headed pass.
Attempt blocked. Tammy Abraham (Swansea City) right footed shot from the right side of the box is blocked. Assisted by Jordan Ayew.
Tammy Abraham (Swansea City) wins a free kick in the defensive half.
Foul by Kevin Long (Burnley).
Substitution, Swansea City. Tammy Abraham replaces Nathan Dyer.
Delay over. They are ready to continue.
Substitution, Burnley. Scott Arfield replaces Aaron Lennon.
Delay in match Sam Clucas (Swansea City) because of an injury.
Attempt blocked. Johann Berg Gudmundsson (Burnley) left footed shot from outside the box is blocked.
Foul by Mike van der Hoorn (Swansea City).
Ashley Barnes (Burnley) wins a free kick in the attacking half.
Attempt missed. Ben Mee (Burnley) header from the centre of the box misses to the right. Assisted by Johann Berg Gudmundsson with a cross following a set piece situation.
Foul by Nathan Dyer (Swansea City).
Charlie Taylor (Burnley) wins a free kick on the left wing.
Attempt missed. Sam Vokes (Burnley) right footed shot from outside the box misses to the left.
Attempt missed. Tom Carroll (Swansea City) left footed shot from outside the box is high and wide to the left.
Offside, Swansea City. Nathan Dyer tries a through ball, but Tom Carroll is caught offside.
Attempt blocked. Ki Sung-yueng (Swansea City) right footed shot from the right side of the box is blocked. Assisted by Tom Carroll.
Corner,  Swansea City. Conceded by Charlie Taylor.