::Sir Alex Ferguson going nowhere soon says Gary Neville::
@1315123888
url: https://www.bbc.com/sport/football/14774972
Gary Neville has said "there's no point" trying to predict when his former Manchester United boss Sir Alex Ferguson will quit the club.
Ferguson, who turns 70 in December, shelved retirement plans in 2002, but intrigue over his future has remained.
"There's no point in thinking about succession planning because he will stay as long as he wants," Neville told BBC 5 live's Sportsweek programme. 
"I don't see this manager quitting. He is just the absolute best."
Neville, who clocked up 400 league appearances and more than 600 in total at United under Ferguson between 1992 and 2011, added: "He has health on his side and he looks the same to me as he did 10 to 15 years ago.
"How can you think about succession when you have a man that shows no sign of giving up, no sign of wanting to quit, no sign of not wanting to be the manager anymore?
"For all Manchester United fans, they hope he stays forever."
Ferguson has a remarkable record at the club, winning 12 Premier League titles, two Champions League trophies, five FA Cups, four League Cups and 10 Community Shields since he took over in 1986.
Eyebrows were raised over the summer when the 69-year-old replaced the departing experience of Neville, Edwin van der Sar, Paul Scholes, Wes Brown and John O'Shea with the youthful trio of Ashley Young, Phil Jones and David de Gea.
Are Pep Guardiola and Jose Mourinho better than Alex Ferguson for Manchester United? I don't think so
However a low-key opening day victory over West Brom was followed by a three-goal thumping of Tottenham and an eight-goal demolition of Arsenal to suggest Ferguson has lost none of his touch.
But when Red Devils boss does decide to call time on his career, Neville believes the club's board will have difficulty finding someone to take over the reigns.
"The best managers out there at the moment are Pep Guardiola and Jose Mourinho but they are at other clubs - Real Madrid and Barcelona," said Neville.
"Will they let them go? I don't think so. Are they better than Alex Ferguson for Manchester United? I don't think so."
The Sportsweek podcast is available to download.