::Manchester United defender Jonny Evans feared for Old Trafford future::
@1315470508
url: https://www.bbc.com/sport/football/14834425
Manchester United defender Jonny Evans has admitted he feared for his future at Old Trafford after his poor form cost him his place last season.
The 23-year-old has played in every game of the new campaign so far but feels he still has a point to prove.
Evans said: "If I had another season like that I am sure the manager wouldn't want me around.
"I wasn't playing consistently to a good enough standard. I went away and thought, 'Maybe I have another year'."
United's regular central defensive pairing of Rio Ferdinand and Nemanja Vidic have been absent through injury since the opening day of the season, allowing Evans to forge a partnership with new £18m signing Phil Jones.
Fellow defenders Wes Brown and John O'Shea both left United for Sunderland in the summer but Evans was confident he would not follow them out of the club.
The Northern Ireland international added: "The manager never said anything to me and I never went to see him.
"I was pretty confident the manager wouldn't get rid of me. 
"I have always thought he has a bit of faith in me. Maybe you do have the odd doubt but I thought it would have been unfair if I had left, considering the two seasons I had before.
"Last year wasn't a total disaster. There were times when I did feel good and others where I felt I needed that level of consistency every week. I have gone back to basics and worked hard.
"I was on holiday when I heard John and Wes were in talks with Sunderland and would be leaving.
"It did make me think there was room but, no matter who the manager gets rid of, or signs, there are so many players who can step into so many different positions it is crazy.
"If you are not in his plans he will move you on. If you are here you have a great chance of being involved."