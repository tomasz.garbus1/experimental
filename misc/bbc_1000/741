::AC Milan 2-0 Celtic::
@1379538455
url: https://www.bbc.com/sport/football/24044286
AC Milan scored twice in the last eight minutes to undo a spirited Celtic performance in their Group H Champions League opener in Italy.
The visitors came close with a Georgios Samaras shot and an Anthony Stokes free-kick, which struck the crossbar.
In a stroke of misfortune for Celtic, Cristian Zapata's strike was deflected into his own net by Emilio Izaguirre.

                            
                        
And Sulley Muntari knocked in a loose ball after Mario Balotelli's curling free-kick was palmed by Fraser Forster.
The result is Celtic's 20th loss in 22 away group outings but those sorry figures do not reflect what was an impressive display from Neil Lennon's men, which will provide hope for coming games against Barcelona and Ajax once the pain of defeat has subsided.
Led by the industry and composure of captain Scott Brown, Celtic matched the seven-time European Cup winners for most of the game and had chances to take the lead at the San Siro.
From an indirect free-kick, Charlie Mulgrew's shot from 10 yards out was charged down by a cluster of defenders racing from the goal-line.
Samaras hammered a shot narrowly wide, Brown was denied a clear run of goal by a brilliant Nigel de Jong tackle and Stokes found the junction of post and crossbar with a lovely free-kick just a few minutes before the deadlock was broken.
Defenders Efe Ambrose and Virgil van Dijk were as imposing as the snow-capped mountains that border the city, doing a wonderful job of protecting Forster, although the goalkeeper made his own contribution.
He was fortunate that a ferocious 18-yard volley from Balotelli flew straight at him in the opening minutes as the hosts started strongly.
And Forster needed to look sharp to push away a glancing header from Alessandro Matri before Antonio Nocerino fizzed a shot over.
But Balotelli and Matri did not link up well in attack for AC Milan, with the former Manchester City forward a peripheral figure for long spells.
Indeed, Balotelli spent most of the first 45 minutes shrugging his shoulders on the left flank, apparently unhappy that a couple of robust challenges on him had not yielded fouls.
The Italy striker did, however, come very close to opening the scoring in the final moments of the first half when his deflected strike looked destined for the bottom corner before Forster produced a marvellous diving stop.
Samaras scored in all three of Celtic's away games in last year's successful group campaign and the Greek forward thought he had done it again after the interval when his slaloming run culminated with a fizzing shot that swerved inches wide.
Soon after, he powered into the six-yard box to connect with a header only for his effort to be blocked by a defender.
But that brief flurry was to be the best Samaras could offer as he failed to impose himself on the left wing, while Adam Matthews was also wasteful from promising positions on the opposite flank.
There was a huge let-off for Celtic when Muntari somehow missed with a close-range header.
And the visitors nearly took advantage when a slick counter-attack involving Stokes and Kris Commons released Brown into the penalty box but the skipper delayed his shot a fraction, allowing De Jong to make a terrific sliding challenge.
Celtic, who earned nine corners, came closer still when Stokes' free-kick clipped the Milan crossbar.
The Italians laboured for periods, with the home support voicing their frustration, but the stadium exploded in joyous relief as the hearts of travelling fans sunk when Zapata's raking strike clipped Izaguirre and left Forster wrong-footed.
Much was made of the home side's long injury list but there were still several big names on display in red and black and Balotelli made amends for his disappointing display with a curling free-kick that had Forster clawing the ball out and Muntari was on hand to convert from close range.