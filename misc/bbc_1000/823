::Where do Portsmouth go now?::
@1327439736
url: https://www.bbc.com/sport/football/16710949
"This is as bad as it can get for Portsmouth without the club ceasing to exist."
Those are the words of Guy Thomas, a leading insolvency lawyer and football finance expert.
It sums up the grave situation that Portsmouth now find themselves in after being  issued with a winding-up petition by HM Revenue and Customs over unpaid tax on Tuesday.
BBC Sport talks to Thomas, who analyses the implications of Pompey's latest financial disaster.
What is a winding-up petition?
At the hearing the court will decide if the company needs to be wound up. If so, the company comes to an end and a liquidator gets appointed
A winding-up petition is when you have a creditor of a company, in this case the football club. They have a liquidated debt and the tax authority will have a liquidated debt and that is an undisputed debt. All it has to be is over £750 and this is far in excess of that.
They put in a petition to the High Court. Once that issue is then served, the company then has seven working days to respond. After that time, the petition gets advertised. When it is advertised that is the point where it becomes public knowledge. 
This is when the banks get involved and start freezing bank accounts for their own protection and the protection of the creditors. Then there will be a hearing. 
At the hearing, the court will decide if the company needs to be wound up. If so, the company comes to end and a liquidator gets appointed. It stops trading and the liquidator gets appointed to assign its assets to the creditors and then decides on the validity of the claims.
What are the implications for Portsmouth?
They usually demand they get paid in full or the company and in this case the football club get wound up
From this winding-up petition, there are only a few possible outcomes. The first is that with a secured creditor, with a debenture registered at Companies House, that debenture holder can appoint an administrator. If the administrator is appointed, then the winding-up petition is suspended. The administrator then takes control, much like before when Andrew Andronikou from Hacker Young got appointed last time. 
The other option is that, if an administrator does not get appointed, the company gets wound up. The only other thing that could happen is if HMRC get paid. It is HMRC policy that, once they have issued a winding-up petition, they want to be paid in full before the hearing. As far as they are concerned, they will negotiate but, once they have made the decision to issue a winding-up petition, the time for negotiation has really passed.
They usually demand they get paid in full or the company and in this case the football club get wound up.
How does this affect Pompey's search for a new owner?
It would be very rare for a buyer to come in and buy a company facing a winding-up petition without making sure the winding-up petition is being dealt with
It affects the timetable. It means that everything needs to happen quickly. If Portpin Ltd are going to appoint an administrator they have to do so soon as possible. 
Assuming the administrator gets appointed, much as Hacker Young did last time, they have to trade the business and find a buyer. It does not change things from where they were before. The winding-up got issued. They were looking for a buyer. They are still looking for a buyer, whereas before CSI were looking to sell their assets, now the administrator is looking to sell it, if one is appointed. It would be very rare for a buyer to come in and buy a company facing a winding-up petition without making sure the winding-up petition is being dealt with.
Are Portsmouth close to going into administration?
From here on in, Portsmouth either go into administration, or they get sold, or they get wound up.
Is this worse than before?
They have been in some pretty bad situations over the past couple of years. Especially when they first got placed into administration and the CVA got challenged by HMRC, when they were looking for a buyer. When all these things were happening, it threatened the existence of the club. This is as bad as it can get for the club without the club ceasing to exist.
This is as bad as it can get for the club without the club ceasing to exist
Why does this keep happening to Pompey?
They are obviously in trouble financially. They've never quite got out of the hole that they were in. They have not been helped by a revolving door of new owners over the past few years. Their problems have never really gone away. There are a lot of people owed a lot of money by the club even after the Company Voluntary Arrangement exit from the last administration. It is as bad as it was before.
Why would anyone buy Portsmouth now?
They are a performing Championship club. They have form of playing in the top flight before, have a very loyal fan base and that means cash income for the owners. There are still the parachute payments coming back down from the Premier League, from which they were relegated only in 2010. There are still good reasons to be in the football business as well as bad ones. It's like any business. There is potential there and, as long as someone sees that potential, they will remain interested. 
I'm sure there are people interested in buying Portsmouth. It is just a question of whether they can do it quickly enough.
Could Pompey cease to exist?
If the secured creditor doesn't step in - that's Portpin Ltd - before the hearing then, when the liquidation hammer comes down, it ceases to trade
Yes. As a trading, playing club when the winding-up petition is heard. If they don't have an answer for that they could cease to exist. If the secured creditor doesn't step in - that's Portpin Ltd - before the hearing then, when the liquidation hammer comes down, it ceases to trade. You can't have a trading liquidated company.
Will they have to sell their best players?
I can see that situation happening but, only in the frame work of a wider solution. Yes, it will get them part of the way but it won't completely clear away all the issues they are facing. There needs to be a plan in place, new investment and old debt got rid of before this can be a healthy and successful club. Just selling the players on its own is not a magic wand that can fix all of this. Portpin Ltd still have a big say in all of this They are the ones also looking for a buyer effectively.