::Partick Thistle 0-1 Dundee: Alan Archibald vows to get results::
@1442086881
url: https://www.bbc.com/sport/football/34235906
Partick Thistle manager Alan Archibald insists he and his players will deal with the pressure of getting the Jags off bottom spot in the Premiership.
Thistle are winless in seven league games and Dundee took all three points at Firhill thanks to Greg Stewart's second-half winner.
"We're in a poor place now but we've got to pick the boys up," Archibald told BBC Scotland.
"We've got to get results and we think we've got enough to go and do it."
Kris Doolan has scored Thistle's only two goals so far in the 2015-16 season and the Jags have hit the net the least times out of all 42 senior Scottish clubs.
Against Dundee, Doolan hit the crossbar and Ryan Stevenson hit the post and bar as the ball refused to go in for the Maryhill men.
Robbie Muirhead, on loan from Dundee United, made an impressive debut, albeit missing a good opportunity from close range in the opening minutes.
"We're not proud of the record we've got - only scoring two goals in seven games," said Archibald.
"It's just not happening for us. We had a very good spell in the first half but the longer games go on and you don't take your chances, I think it seeps in a wee bit.
"We've been here before, a couple of years ago when we struggled to score and teams picked us off and won 1-0. That's what happened today.
"All our good play came from Robbie and Stevo linking up and supplying Kris Doolan - it was very good to watch. We created enough chances today to go and win two games. We've just got to keep on going."
Dundee manager Paul Hartley, whose side jump to fifth in the Premiership, praised match-winner Stewart's eye for goal.
The 25-year-old is up to four for the season after reaching 15 in the previous campaign.
"He's getting better and better," Hartley told BBC Scotland.
"He always scores those type of goals when he comes in off the right-hand side and he passed it into the net.
"It's something we've been working on and stressing; pass it into the net and not always putting your foot through it. But he should have scored another one and I was disappointed because you want to be ruthless."
Hartley was disappointed with the Dark Blues' "scrappy" first-half showing but applauded his team's ability to make a better fist of things after the break.
James McPake had to be replaced with a slight hip knock although the defender is likely to be fit to face Celtic next weekend.
"It's been a really solid start for us," added Hartley.
"At the start of the season you want to try and get away from the bottom as soon as possible and build up momentum.
"I've got a terrific group of players and we face a tough challenge next week, but we'll go to Celtic and hopefully be positive. When you go to Celtic Park you've got to be brave and disciplined."