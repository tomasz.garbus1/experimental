::Jose Mourinho: Chelsea boss says FFP sanctions should change::
@1411206707
url: https://www.bbc.com/sport/football/29292339
Chelsea manager Jose Mourinho says clubs who breach Financial Fair Play rules should be penalised with sporting sanctions, rather than fines.
Premier League champions Manchester City, who Chelsea meet on Sunday, were fined £49m - £32m of which is suspended - in May over FFP.
"Are the fines fair? I don't think so. What is fair is to remove points and titles," Mourinho, 51, said.
"If you win titles and are then penalised economically, you continue."
City, who posted combined losses of £148.1m in 2012 and 2013, also had their Champions League squad for this season capped at 21 players, rather than the maximum 25.
In the summer transfer window, they were permitted to spend £49m, as well as the money they received from selling players and their wage bill for this season must not exceed that of last term.
Mourinho, though, highlighted the Champions League sanction as ineffective.
"If you have to take one or two players off your list for the Champions League and instead of going with 24 go with 22, no problem," said the Portuguese.
"But say you start the next championship with six points deducted, which means that you don't play in the Champions League, but in the Europa League - that is more serious."
City's spending came with the help of a takeover by the Abu Dhabi United group in 2008, much like Chelsea benefitted from Roman Abramovich's arrival in 2003.
But Mourinho insists that the Blues will adhere to the FFP regulations.
"The teams that benefit from this are the most powerful economically, with more history and more followers," said the former Porto, Inter Milan and Real Madrid boss.
"Right now, with FFP, we have become a team like any other, managing our resources in a rational manner."