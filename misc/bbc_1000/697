::Tottenham will keep best players - chairman Daniel Levy::
@1306076469
url: https://www.bbc.com/sport/football/13493194
Tottenham chairman Daniel Levy has told fans that the club are determined to keep their best players this summer.
With Spurs failing to repeat their Champions League qualification, there has been speculation about the likes of Gareth Bale and Luka Modric.
But Levy said: "We have spent hundreds of millions of pounds [on] our squad and on creating a settled team.
"We have no reason to sell and every intention of retaining our key players. We will not entertain any approaches."
Bale's two incredible performances against Inter Milan in the Champions League alerted a number of Europe's biggest clubs to the 21-year-old Wales midfielder's potential.
Also in midfield, Croatian Modric has enjoyed his finest season at White Hart Lane to date, picking up the supporters' player of the year award after a series of impressive displays.
Manchester City, Barcelona and Manchester United have all been linked with big-money moves for the pair.
However, Levy admitted in an open letter to supporters that there will be plenty of ins and outs at White Hart Lane this summer.
Boss Harry Redknapp is keen to move on at least six members of the first-team squad to make room for new signings and for Kyle Walker and Kyle Naughton, who will return from loan spells at Aston Villa and Leicester respectively.
Levy said: "We currently have one of the largest squads in the Premier League and, given the 25-man squad rule, it is no longer practical to retain players who are unlikely to qualify within that limit.
"We shall, therefore, look to reduce the number of these players during the coming transfer window in order to operate both effectively and efficiently."