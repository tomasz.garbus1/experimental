::Sir Alex Ferguson wins Manager of Year award::
@1369120717
url: https://www.bbc.com/sport/football/22604396
Sir Alex Ferguson was named the League Managers' Association Manager of the Year after guiding Manchester United to a 20th title.
Cardiff City's Malky Mackay was the top Championship manager, while Yeovil Town's Gary Johnson was honoured as League One Manager of the Year.
Gillingham's Martin Allen won the League Two manager's award.
"In a way I am glad it is all over now because it has been hard work," said the retiring Ferguson.
Manchester United drew 5-5 with West Brom in Ferguson's 1,500th and final game in charge of United.
The 71-year-old, who won 38 trophies in a 26-year reign at Old Trafford, will be succeeded by Everton's David Moyes, who takes over on 1 July.
"It has been overwhelming," said Ferguson. "The club has been fantastic and congratulations to West Brom with the way they handled yesterday [Sunday].
"I think [West Brom manager] Steve Clarke should get an award because anyone who scores five against United deserves it."
LMA chairman Howard Wilkinson said: "There is no doubt that the LMA Annual Awards are perceived as the most accurate measure of a manager's ability because there are no better judges of your performance than your peers.
"Finding words to adequately describe the monumental levels of achievement and the indelible legacy Sir Alex Ferguson leaves in the game is nigh on impossible. This evening's award adds yet another record to that list, making him the only person to secure the much-coveted LMA Manager of the Year Award for a fourth time. 
"David Moyes, his choice of successor to the Old Trafford throne, has won it three times and I know that Sir Alex will hope and believe that David goes on to equal and surpass the new record."
Mackay's Cardiff secured promotion to the Premier League in April and went on to win the Championship by eight points from Hull. 
He said: "I am just very proud of the whole club - a lot of people in the last two years have put hard work into building this."
Bradford City manager Phil Parkinson was honoured with an LMA Special Merit Award after his side reached the final of the Carling Cup and won promotion from League Two via the play-offs this season.
Former Chelsea boss Roberto Di Matteo also collected a Merit Award in recognition of the Blues' Champions League final victory over Bayern Munich in 2012. 
Di Matteo was sacked after Chelsea were knocked out at the group stage of this season's competition.
England boss Roy Hodgson was elected into the LMA's 1,000 club for managers who had been in charge of 1,000 games after the body agreed to take overseas and international matches into account.
Ex-Southampton and Northern Ireland boss Lawrie McMenemy was also elected into the club.
Hodgson, who has coached in eight different countries, said the job of a manager was becoming tougher.
"You have to be very lucky to survive the ups and downs and that's getting harder and harder," he said.
"I have been lucky and done it in some quite easy places compared to what Sir Alex Ferguson has done at Manchester United and Aberdeen, that's for sure."