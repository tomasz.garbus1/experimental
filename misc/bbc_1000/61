::Keane would be a complex fit for Celtic managerial role::
@1401476365
url: https://www.bbc.com/sport/football/27639400
Conflict is a feature of Roy Keane's career, and it even precedes his potential arrival in Glasgow.   
There are already dissenting voices among Celtic supporters, although the club remains adamant other candidates are being considered to succeed Neil Lennon as their manager.
Chief executive Peter Lawwell put the number between "five and 10", but while others remain aware that an opportunity might arise for them at Celtic Park, it seems that only Keane has discussed the situation with the club's majority shareholder Dermot Desmond.
That alone is significant, because Desmond's opinion tends to hold sway. 
Fans are not unanimous in their backing of Keane, but that reflects the absence of an outstanding candidate for the role, as much as concerns about the Irishman's managerial pedigree.
David Moyes is the strongest figure in terms of experience and achievement. 
His body of work is impressive enough to encourage some perspective on his failure at Manchester United. He will be still be troubled by that turn of events, and Celtic could offer a form of rehabilitation.
Even so, the possibility of Keane taking charge has become public knowledge and it is inconceivable that confirmation would have been provided in such a manner without the prior knowledge of Desmond. 
He is a close friend of Keane and Martin O'Neill, and it was the latter who revealed that an "informal chat" had taken place between the 42-year-old and Desmond.
Celtic's majority shareholder would not want the club to look snubbed if Keane turns them down, nor would he want his friend to look as though he has been discarded in the pursuit of a different candidate. 
The machinations are intriguing, not least because they will continue to be significant.
The complexity of Keane would be a challenge to Celtic. 
There will be a flurry of interest if he is appointed, but from out-with the Celtic support much of that will be voyeurism, shaped by a curiosity about Keane's often explosive nature.
His spell in charge of Sunderland was initially successful, with relegation to League One avoided then promotion gained to the Premier League. The lingering impact, though, was a wearying sense of flux. It eventually turned to antagonism.
There was a constant turnover of players and a harsh disciplinary regime that saw some left behind if they were late for the team bus. 
Desmond may view potential volatility as a worthwhile risk when Keane might impose a blunt but elitist attitude on the club and redraw its horizons
That approach will usually finally crack into fault lines if there is never any leeway granted. Some players celebrated his departure, while the team needed to be lifted out of the lower reaches of the Premier League.
It was instructive, for instance, to read Tony Cascarino's reflections on Keane in an interview in last weekend's Irish Independent on Sunday.
The two former international team-mates are not close friends, so bias is inevitable, but Cascarino dwelled upon Keane's manner with team-mates and his managers.
"Roy has a lot of qualities, but one area where he has always struggled is dealing with people," Cascarino said. "Roy challenged people all of the time."
Nobody at Celtic will recoil from standards being re-set by an individual who thrived at the very top level of the game. Confrontation will be a less desirable trait when a method of working is already established, though.
Strategy and recruitment are collaborative decisions currently made between the head coach, Lawwell and the head of football development John Park. If there were disagreements with Keane, Lawwell would not be the sole presence with the authority of Desmond's support behind him.
Relationships can be managed, of course, and there was not always unanimity when Lennon was in charge. 
Desmond may view potential volatility as a worthwhile risk when Keane might impose a blunt but elitist attitude on the club and redraw its horizons.
It will also be welcome from a business perspective to generate a fresh surge of interest in the club. 
Competition will not draw large crowds to Celtic Park because the domestic scene is not currently a challenge.
Three Champions League qualifiers need to be negotiated for another season in the group stages, but Keane's mere presence would enhance Celtic's profile and status. 
All the same, the appointment would be accompanied by inherent risks.
Celtic's model, if it is to continue to be adhered to, is based upon sourcing and developing potential. Coaching skills are critical, because individual flaws will need to be ironed out and the means provided for the team to perform at the contrasting levels of Scottish and European football.
At Sunderland, and to a lesser extent Ipswich, Keane relied on signing batches of players. Those kinds of splurges are not currently tolerated at Celtic, although it ought to be recognised that Keane is now older and wiser for his experiences, and the constraints of the role might focus him to adopt a more considered approach.
Desmond's instincts should not be dismissed. Few Celtic fans clamoured for Gordon Strachan to be appointed, but he led the club to three consecutive titles and some memorable European occasions. 
Lennon, too, was a risk, but he developed into an effective and successful manager. O'Neill's appointment was also championed by Desmond.
If there is a legitimate concern, it would be that the Irish businessman's friendship with Keane could skew his judgement. 
There is little competitive challenge next season, though, and it is absurd to suggest that the tensions of managing one half of the Old Firm would prove troubling to a figure who not only captained Manchester United and the Republic of Ireland, but charged through every boundary and confrontation with a relentless and aggressive self-assurance.
Keane is not a perfect fit for the role, but the attractions are obvious. The spikiness, after all, is accompanied by a career at the highest levels, a shrewd wit and a fierce ambition.
A short spell at Celtic may even revive his own managerial reputation whilst ensuring that the club retains a high profile, even though the Scottish Premiership offers an essentially uncompetitive title race and Champions League qualification is difficult.
There could be no slacking under Keane, nor for those who would operate above him. 
There would also be an obligation to maintain a focus on the club. Fascination alone would be guaranteed.