::Chelsea 2-2 Man Utd: Jose Mourinho involved in bust-up after Ross Barkley saves Blues::
@1540046415
url: https://www.bbc.com/sport/football/45847416
Manchester United manager Jose Mourinho was involved in a furious touchline bust-up after Ross Barkley's 96th-minute equaliser rescued Chelsea's unbeaten Premier League start at Stamford Bridge.
Mourinho's side looked on course for an outstanding win over his former club after two goals from Anthony Martial overturned the first-half lead given to the hosts by Antonio Rudiger's 21st-minute header.
He bowed his head in disappointment as Barkley scored in the dying seconds after a goalmouth scramble, but then reacted angrily as Chelsea technical assistant Marco Ianni celebrated in front of him before then pumping his fists in Mourinho's direction again.
The Portuguese jumped from his seat and was then held back by a mixture of stewards and Manchester United staff as he attempted to get down the tunnel at Ianni.
It was a dramatic conclusion as Mourinho saw victory snatched away after he had replied to the taunts of Chelsea fans by holding up three fingers to remind them of the three Premier League titles he won while in charge at Stamford Bridge.
The Blues looked in control when Rudiger took advantage of Paul Pogba's poor marking to head home Willian's corner, but United responded superbly after the break and Martial's double - a smart, swivelling finish and a composed, low effort - put the visitors on the brink of three points until that frantic finale.
Mourinho kept his emotions in check for most of the afternoon as returned again to the club where enjoyed success still under pressure at Manchester United - but then it all boiled over in that super-charged finish.
He allowed himself a celebratory fist-pump when Martial drove in United's second goal. It was low-key and would have gone pretty much unnoticed had it not been caught on the television cameras.
It was then, as the clock ticked down, that Mourinho's body language became animated and his emotions bubbled to the surface, concluding in that chaotic confrontation by the tunnel entrance as he felt Chelsea technical staff member Ianni overdid the celebrations of Barkley's late leveller.
The manager's temperature was rising even before then as United protected the lead that could have given them a crucial win, with the added delight for Mourinho of achieving it at Stamford Bridge, where he won those three titles. No matter how calm he tried to remain, you could see how much this meant to him.
When goalscorer Martial chased back towards the halfway line to close down a Chelsea player, Mourinho was jumping out of his seat in the technical area - first to urge the France forward on to greater efforts to win the ball, then with an extravagant fist-pump, applause and even an advance onto the pitch to slap him on the back for completing his task.
Mourinho's understandable anguish as Barkley celebrated his equaliser was not helped by what he clearly saw as gloating from the Chelsea backroom member, and he was only persuaded to let the matter rest after being calmed down by stewards and his staff.
The Portuguese decided to have the last word with that three-fingered gesture towards Chelsea fans - but victory would have been a much sweeter response.

                            
                        
Martial has often cut a discontended figure at Manchester United this season - and his relationship with Mourinho has been the subject of much debate.
The 22-year-old France forward has been marginalised on occasions this season, but has now proved his worth and quality in successive games.
Martial's snapshot finish played its part in the comeback from two goals down to beat Newcastle United before the international break, and on Saturday at Stamford Bridge he was the spearhead for United's revival after Chelsea controlled the first half.
He equalised with a finish of superb technique on the turn, then coolly completed good work by Juan Mata and Marcus Rashford by steering a low shot into the bottom corner.
Martial was also willing to do the dirty work and was rewarded with a warm embrace and words from his manager that brought a smile to his face when he was substituted.
United's hierarchy clearly see Martial as a key element of their future and hope he will commit to a long-term contract. Mourinho will certainly be grateful for his contribution in the past two games.
Chelsea's wild celebrations after Barkley scrambled home their equaliser demonstrated the importance of keeping their unbeaten league start going against one of their close rivals.
Maurizio Sarri's side had been the victim of Daniel Sturridge's late equaliser when Liverpool took a point here - but were grateful for something similar of their own this time, with Barkley on hand to smash home after David Luiz's header hit a post and David de Gea saved brilliantly from Rudiger.
Chelsea boss Sarri will surely be concerned about how his side lost the control they enjoyed in the first half, when they were untroubled by United, but he must be delighted they were still battling and pushing so deep into six minutes of stoppage time to rescue what could be an important point.
The Blues have shown they are willing to scrap as well as turn on the style - and it was enough to ensure they are yet to suffer a league loss this season.
Chelsea boss Maurizio Sarri to BBC Sport: "We have played very well in the first hour but then we have played the match of United - a physical match, and United are better than us in a physical match. I am disappointed with the last 30 minutes. We could win but at the end one point is enough.
"We were not organised for the long ball, we usually play with short passes so we were not organised for this in the last half hour. This is not our football. I prefer to play our football for 90 minutes.
"I didn't see what happened but I have spoken to Jose Mourinho. I understood that we were on the wrong side of the situation. I have spoken to a member of my staff and have dealt with this immediately."

                            
                        
Manchester United manager Jose Mourinho to BBC Sport: "A fantastic match, a very undeserved result for us but that is football. We were the best team, even in the first half when we were losing. 
"We were in control, tactically - the result is really unfair for us. We conceded from two set-pieces, but that is a way to score goals and you have to be able to defend against that.
"Anthony Martial is improving, he is doing different things than before. He is a more complete player than before - but he needs to improve. He has a huge talent and he wants it, which is a good thing. The team as a team was really good."
Tell us about your reaction at the end...
"It is not my reaction, it is Sarri's assistant. He was very impolite but Sarri took care of the situation. They have both apologised to me. I accept. For me, the story is over.
"Don't do what everyone does and say: 'It's Mourinho who does things.' I don't know his name, I don't need to know. Everything is fine."
Chelsea host Bate Borisov in the Europa League on Thursday (kick-off 20:00 BST), before returning to Premier League action at Burnley on Sunday, 28 October (13:30 GMT).
Manchester United welcome Juventus to Old Trafford in the Champions League on Tuesday (20:00 BST), and then host Everton in the league next Sunday (16:00 GMT).
Match ends, Chelsea 2, Manchester United 2.
Second Half ends, Chelsea 2, Manchester United 2.
Goal!  Chelsea 2, Manchester United 2. Ross Barkley (Chelsea) right footed shot from very close range to the bottom right corner.
Attempt saved. Antonio RÃ¼diger (Chelsea) header from very close range is saved in the centre of the goal.
David Luiz (Chelsea) hits the right post with a header from the centre of the box. Assisted by CÃ©sar Azpilicueta with a cross.
Andreas Pereira (Manchester United) is shown the yellow card.
Marcos Alonso (Chelsea) wins a free kick in the attacking half.
Foul by Andreas Pereira (Manchester United).
Attempt missed. Eden Hazard (Chelsea) left footed shot from the left side of the box is high and wide to the left. Assisted by Jorginho.
Alexis SÃ¡nchez (Manchester United) is shown the yellow card for a bad foul.
Jorginho (Chelsea) wins a free kick in the defensive half.
Foul by Alexis SÃ¡nchez (Manchester United).
Attempt missed. CÃ©sar Azpilicueta (Chelsea) right footed shot from the right side of the box is high and wide to the right. Assisted by N'Golo KantÃ©.
Olivier Giroud (Chelsea) wins a free kick in the defensive half.
Foul by Victor LindelÃ¶f (Manchester United).
Foul by Ross Barkley (Chelsea).
Nemanja Matic (Manchester United) wins a free kick in the defensive half.
Substitution, Manchester United. Alexis SÃ¡nchez replaces Marcus Rashford because of an injury.
Foul by David Luiz (Chelsea).
Paul Pogba (Manchester United) wins a free kick in the defensive half.
Corner,  Chelsea. Conceded by Luke Shaw.
Substitution, Manchester United. Andreas Pereira replaces Anthony Martial.
Delay over. They are ready to continue.
Delay in match Marcus Rashford (Manchester United) because of an injury.
Attempt blocked. Eden Hazard (Chelsea) right footed shot from outside the box is blocked. Assisted by Olivier Giroud.
Attempt saved. Ander Herrera (Manchester United) left footed shot from the left side of the box is saved in the bottom right corner. Assisted by Romelu Lukaku with a headed pass.
Substitution, Chelsea. Olivier Giroud replaces Ãlvaro Morata.
Attempt missed. Marcos Alonso (Chelsea) left footed shot from the left side of the box misses to the left. Assisted by Eden Hazard following a set piece situation.
Ãlvaro Morata (Chelsea) wins a free kick in the attacking half.
Foul by Paul Pogba (Manchester United).
Attempt blocked. Ross Barkley (Chelsea) right footed shot from outside the box is blocked. Assisted by Ãlvaro Morata.
Attempt blocked. Antonio RÃ¼diger (Chelsea) right footed shot from outside the box is blocked. Assisted by Eden Hazard.
Substitution, Manchester United. Ander Herrera replaces Juan Mata.
Substitution, Chelsea. Pedro replaces Willian.
Goal!  Chelsea 1, Manchester United 2. Anthony Martial (Manchester United) right footed shot from the centre of the box to the bottom right corner. Assisted by Marcus Rashford.
Marcos Alonso (Chelsea) wins a free kick on the left wing.
Foul by Marcus Rashford (Manchester United).
Attempt saved. N'Golo KantÃ© (Chelsea) right footed shot from outside the box is saved in the bottom left corner. Assisted by Willian.
Substitution, Chelsea. Ross Barkley replaces Mateo Kovacic.
Juan Mata (Manchester United) is shown the yellow card for a bad foul.