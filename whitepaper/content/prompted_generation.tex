\label{chapter:prompted_generation}
\section{Related work}
\subsection*{CTRL}
\cite{ctrl} recall the intuitive factorization of the probability distribution
approximated by a language model\footnote{Original equations from \cite{ctrl}}:
\begin{equation}
    p(x) = \underset{i=1}{\overset{n}{\prod}} p(x_i|x_{<i})
\end{equation}
and extend it with a control code:
\begin{equation}
    p(x|c) = \underset{i=1}{\overset{n}{\prod}} p(x_i|x_{<i}, c)
\end{equation}
Similarily, they modify the negative log-likelihood cost function:
\begin{equation}
    \mathcal{L}(D) = - \underset{k=1}{\overset{|D|}{\sum}} \log p_{\theta}(x_i^k|x_{<i}^k)
\end{equation}
where $\theta$ is the parametrization of language model and
$D = \{ x^1, ..., x^{|D|} \}$ is the dataset, to account for the control code:
\begin{equation}
    \mathcal{L}(D) = - \underset{k=1}{\overset{|D|}{\sum}} \log p_{\theta}(x_i^k|x_{<i}^k, c^k)
\end{equation}
On top of this intuition, that a language model can model a probability
distribution conditioned not only on previous tokens but also on some control
code, authors build \textsc{CTRL} -- a 1.63 billion-parameter transformer
architecture, trained on 140GB of text data, with control codes prepended to
each text. Control codes include:
\begin{table}[H]
    \centering
    \caption{An overview of control codes fed to the \textsc{CTRL} model.}
    \tiny{
        \begin{tabular}{|c|c|c|}
            \hline
            \textbf{Control code category} & \textbf{Description} & \textbf{Examples} \\
            \hline
            Style by domain & Specify overall style by pointing to a source domain & \texttt{Wikipedia}, \texttt{Books}, \texttt{Reviews} \\
            \hline
            More complex codes & Detailed specification of output text & \texttt{Reviews Rating: 1.0} \\
            \hline
            Trigerring specific task & Related to specific tasks such as QA or MT & \texttt{Questions Q: What is the capital of India? A:}\\
            \hline
            Zero-shot code mixing & Mixing of cross-domain control codes that do not occur in training data & Mix of politics subreddit with a French prompt \\
            \hline
        \end{tabular}
    }
\end{table}
\textsc{CTRL} achieves phenomenal results from a human perspective. Numerous
samples presented in the paper are coherent over multiple lines, compatible with
provided parametrization.

One could hypothesise that when it comes to controlled text generation, no generation-time
supervision is required and that a traditional likelihood-estimation approach can be successfully
generalized to prompts and control codes. \textsc{CTRL} is a strong point in case.

\subsection*{Plug and Play Language Models (PPLM)}
\cite{pplm} propose an alternative to costly domain-specific fine-tuning of a pretrained GPT-2 model.
Instead, they attach to the output of the Transformer a discriminator (authors experiment with bag-of-words
and linear classifiers), which steers the generation. Having defined a history matrix $H_t$ as key-value
pairs from the past: $H_t = [(K_t^{(1)}, V_t^{(1)}), ..., (K_t^{(l)}, V_t^{(l)})]$, where the upper index
denotes the layer, and the lower index -- timestep, authors summarize the transformer architecture as\footnote{
    Original equation from \cite{pplm}.
}:
\begin{equation}
    o_{t+1}, H_{t+1} = \text{LM}(x_t, H_t)
\end{equation}
where $o_{t+1}$ is a logit vector of vocabulary size, from which a single token is sampled with probability
$softmax(Wo_{t+1})$ and $W$ is a linear transformation.

At each generation step $t$, the history $H_t$ is shifted towards a combination of two gradients:
\begin{itemize}
    \item firstly, history $H_t$ is nudged toward higher output of the attached discriminator.
    \item secondly, to prevent the generator from sneaky adversarial practices of sacrificing text quality for
          the sake of fooling the discriminator, $H_t$ is updated toward higher LL of unmodified LM $p(x)$.
\end{itemize}

Authors construct bag-of-words models for seven topics: \textsc{Science}, \textsc{Military}, \textsc{Legal},
\textsc{Computers}, \textsc{Space}, \textsc{Politics}, \textsc{Religion}. They have also built linear classifiers
of text sentiment, distinguishes between two classes, \textsc{Positive} and \textsc{Negative}.

While the PPLM has performed similarily good as CTRL, despite having much less trainable parameters\footnote{
    The pretrained GPT-2 model is not updated in PPLM.
}, it was also tested on very general control codes (sentiment, general topic etc.). It is difficult to predict how
well it would perform under a more fine-grained discriminator.

\section{Fine-tuning GPT-2}
With a set of procedures for annotating an article with the occurring
samples (vide chapter \ref{chapter:ner}), it was possible to enhance the training
set with relevant prompts.
\subsection{Prompt format}\label{subsection:prompt_format}
All prompts (except section \ref{section:prompt_human_evaluation}) followed the BNF specification
below:
\begin{verbatim}
    <category> ::= "player" | "stadium" | "manager" | "league" | "country" | "team"
    <category_entities> ::= "" | <entity> "," <category_entities>
    <category_line> :== <category> ":" <category_entities>
    <prompt> ::= "" | <category_line> "\n" <prompt>
\end{verbatim}
In practice, best results were achieved if several other assumptions were made:
\begin{itemize}
    \item categories were listed in the same order in all prompts
    \item empty categories were also listed
    \item each category was listed exactly once (as opposed to separate line for
          each entity)
\end{itemize}
\subsection{Generation procedure}
What distinguishes prompted generation implemented in a traditional MLE framework
from an adversarial or RL setting at the high level is an assumption that with enough
data fed into the model, there is no need for a judge (of course here by judge we mean
a function from sequences to real numbers rather than a human linguist)
which explicitly evaluates the compliance of output sequence with the expectation
(prompt) during training.
Such judge can however be useful to select the best sample to be presented to the
end user. Following this observation, we want the judge to assist the generation
process by selecting the best path every $n$ tokens rather than choosing a single
end result.
A generation procedure emerges from this intuition:
\begin{algorithm}[H]
    \caption{Generate sample}\label{algo:generate_sample}
    \small{
        \hspace*{\algorithmicindent} \textbf{Input:} $prompt, tokens\_at\_step, num\_steps, nsamples$
    \begin{algorithmic}[1]
        \STATE{$entities \gets \texttt{extract\_entities}(prompt)$}
        \STATE{$text \gets prompt$}
        \FOR{$\_$ in $\texttt{range}(num\_steps)$}
            \STATE{$variants \gets \texttt{gpt2.generate}(tokens\_at\_step, nsamples, \texttt{prefix=}text)$}
            \STATE{$variants.\texttt{sort}(\texttt{key=}\lambda v.\texttt{f\_score}(\texttt{extract\_entities}(v), entities), \texttt{reverse=}True)$}
            \STATE{$text \gets \texttt{concat}(text, variants[0])$}
        \ENDFOR
        \RETURN{$text$}
    \end{algorithmic}
    }
\end{algorithm}
Note that this procedure closely resembles a special case of beam search, where only one
path is explored at each step. Perhaps a beam search with higher beam width would yield
better results, but it was not experimented with (and would be more computationally expensive).

Values $tokens\_at\_step$ and $num\_steps$ were set to, respectively, 100 and 4. One can easily notice that
the parameter $nsamples$ is a trade-off between computation time and expected quality of produced samples.

\subsubsection*{Correlation between number of samples at each step and F-measure.}
The time required to generate a text increases linearly with the $nsamples$ parameter.
A correlation between this parameter and the quality of generated text is not so easy to
reason about. To measure it experimentally, generation script was executed for each
value of $nsamples$ from $1$ to $20$ against 6 manually written prompts. Then the 6 scores
were averaged for three metrics: precision, recall and their harmonic mean -- the F measure.

\begin{figure}[H]
    \centering
    \begin{subfigure}{0.32\textwidth}
        \includegraphics[scale=0.26]{content/figures/bbc_nsamples_f1_corel.png}
        \caption{F1 score}
    \end{subfigure}
    \begin{subfigure}{0.32\textwidth}
        \includegraphics[scale=0.26]{content/figures/bbc_nsamples_prec_corel.png}
        \caption{precision}
    \end{subfigure}
    \begin{subfigure}{0.32\textwidth}
        \includegraphics[scale=0.26]{content/figures/bbc_nsamples_recall_corel.png}
        \caption{recall}
    \end{subfigure}
    \caption{
        Correlation between number of samples at each step of generation and
        entity-wise quality of generated articles. $x$-axis -- number of samples,
        $y$-axis -- score in particular metric.
        The blue line is an averaged score, the red bars span from minimum to maximum
        obtained among the 6 prompts for a particular $nsamples$.
        }
\end{figure}

The results of this experiment suffer from high variance (which seems to be the only explanation
why the obtained metrics are not monotonic), but a clear observation is that 5 seems to be
a threshold value for $nsamples$, where the plot flattens.

\subsection*{Correlation between prompt likelihood and text quality}
\subsubsection*{Probability distribution similarity of entities in real and generated samples}
The approach of describing each text with an itemset of mentioned entities leads
us to treating the corpus $\mathcal{C}$ as a database of transactions $\mathcal{D}_\mathcal{C}$,
in which some entities are more likely to appear in a given context than others.
For instance, \textit{Chelsea F.C.} is more probable to occur in the same row of
$\mathcal{D}_\mathcal{C}$ as \textit{Manchester United} than \textit{AS Roma}.
It is intuitive to assume that the language model trained on this corpus will
also model the probability distribution of itemsets in $\mathcal{D}_\mathcal{C}$.

This assumption was put under test -- an unsmoothed probability distribution estimation
was computed from entities collected from 100 generated samples. The target probability
distribution was computed similarly from all real articles in the corpus. Cosine similarity
was chosen as the measure instead of Kullback-Leibler divergence, because the latter requires
the actual probability to have no non-zero values for all elements where the
target probability has non-zero values\footnote{
    It is of course possible to smooth the probability distribution, but the scale of resulting
    KL divergence value is then dependent on the level of smoothing. In particular, as the
    probability $p(e)$ for some element $e$ goes to $0$, the KL divergence goes to $+\infty$.
}. The estimated similarity is presented below at Table \ref{fig:cosine_similarity}.
\begin{table}[H]
    \caption{Similarity between entity probability distributions in generated samples and real corpus.}
    \centering
    \begin{tabular}{|c|c|}
        \hline
        \textbf{Generative model} & \textbf{Cosine similarity}\\
        \hline
        GPT-2 & 0.655706 \\
        \hline
        LSTM & 0.652653 \\
        \hline
    \end{tabular}
    \label{fig:cosine_similarity}
\end{table}


\subsubsection*{Prompt likelihood measure and experiments}
During prompted generation, a user of such language model may want to draw prompts
from a different probability distribution than the one from which $\mathcal{D}_\mathcal{C}$
is sampled (denoted as $\mathcal{P}_{\mathcal{C}}$).
It's not easy to predict how well will the language model perform prompted with
an itemset $S$ such that $\mathcal{P}_{\mathcal{C}}(S)$ is very small. An
\textit{exposure bias} is expected to occur. Experiments were made to research
the correlation between the prompt likelihood and text quality.

The latter was evaluated using F1 score, to measure how compliant the texts are with provided prompts.

A prompt likelihood is, unfortunately, not so clearly defined. An obvious
probability measure would be defined as $p(S) = \frac{\#_S(\mathcal{D})}{|\mathcal{D}|}$,
i.e. fraction of rows of $\mathcal{D}$ which are exactly $S$. This definition is
not meaningful enough for itemsets which never occur in $\mathcal{D}$ as they would
always be assigned a zero probability. An itemset not present in $\mathcal{D}$,
but differing from a real itemset by only one element intuitively seems more
"likely" than an itemset consisting of e.g. all hundreds of thousands of entities
in database. A new likelihood measure is introduced:
\begin{equation}
    \mathcal{PL}^\prime(S) = \texttt{avg}(\{ support(s)\ |\ s \subseteq S, |s| \leq 3 \})
\end{equation}
\noindent
where $support$ is defined as in {\sc Apriori} algorithm.
Such formulation is not enough as it does not consider the size of $S$. Luckily,
the probability distribution of itemset sizes is easy to compute (see \ref{fig:bbc_entities_sizes})
and sampled from. The likelihood measure is thus improved:
\begin{equation}
    \mathcal{PL}(S) = \mathcal{PL}^\prime(S) \cdot p(|S|) \cdot 10^3
\end{equation}
\noindent
Note that while the $10^3$ constant makes the value range of $\mathcal{PL}$ more
pleasant to a human reader, $\mathcal{PL}$ image is not the same as that of
a probability function. It is (at least for some database, not necessarily for
BBC Sport articles) possible that $\mathcal{PL}(S) > 1$ for some $S$.

In order to research the correlation between prompt likelihood and text quality
a large number of prompts of various likelihoods must be prepared. Instead of
building an algorithm for collecting an itemset with fixed likelihood,
a large number of itemsets was generated and most were filtered out to obtain
a near-uniform distribution.

\begin{figure}[H]
    \begin{subfigure}{.49\textwidth}
        \includegraphics[scale=0.6]{content/figures/likelihood_f1_correlation.png}
        \caption{
            Correlation between itemset likelihood measure and the F-measure.\\
            \\
        }
    \end{subfigure}
    \begin{subfigure}{.49\textwidth}
        \includegraphics[scale=0.6]{content/figures/itemset_size_f1_correlation.png}
        \caption{
            $x$-axis is the itemset size, $y$-axis is F-measure. Blue line
            shows the score averaged from 6 samples, red bars span the range
            from minimum and maximum scores among 6 samples.
        }
    \end{subfigure}
    \caption{Correlations between itemset properties and the F-measure.}
\end{figure}

No clear correlation was observed between the chosen $\mathcal{PL}$ metric and the
quality of generated text. In particular, the F1 measure is not increasing as the
$\mathcal{PL}$ increases, but peaks around $\mathcal{PL} = 0.06$. This result may
simply show that the designed likelihood metric $\mathcal{PL}$ is not representative
of what entity sets are really most probable.

On the other hand, there is a visible correlation between F1 score and itemset
size, as confirmed by a separate experiment reported on subfigure (b).

\subsection{EDGAR}
The EDGAR repository, described in chapter \ref{chapter:edgar} implements
scripts needed to execute a full pipeline of control-code-driven generation.
Prompt format from section \ref{subsection:prompt_format} is generalized to
\begin{verbatim}
    <category> ::= [a-zA-Z]
    <category_entities> ::= "" | <entity> "," <category_entities>
    <category_line> :== <category> ":" <category_entities>
    <prompt> ::= "" | <category_line> "\n" <prompt>
\end{verbatim}
\noindent
so users can provide their own configurations for the NER component. EDGAR uses
algorithm \ref{algo:generate_sample} to produce samples more compliant with
provided prompts than raw GPT-2 model would do. Users can control parameters
$tokens\_at\_step,\ num\_steps,\ nsamples$ via command-line or function arguments.
All experiments presented above in this chapter are reproducible with little
effort using EDGAR scripts on an arbitrary dataset.

\section{Human evaluation of text cohesion}\label{section:prompt_human_evaluation}
Human evaluation of prompted texts was conducted in a similar manner
as in section \ref{section:placeholders_human_evaluation}. In both experiments below
the prompted generation has performed significantly better than placeholder-driven
generation.

Both experiments below
have shown that prompted generation outperforms placeholder-driven generation under
human judgment.

\subsection*{Setting}
Again, a medium GPT-2 model with 36 layers and 355M trainable parameters
was finetuned on a subset of BBC Sport corpus with only match reports.
Article titles were once again normalized, i.e. exact score was replaced
with an (in)equality sign.

We prompt the text generation with match result -- one may point out that we
did the same in experiments described in subsection \ref{section:placeholders_human_evaluation}.
Note that in the experiments with template generation, only three prompts were
possible: \texttt{\{team:0\} < \{team:1\}}, \texttt{\{team:0\} > \{team:1\}},
\texttt{\{team:0\} == \{team:1\}}, reducing it to a 3-class generation task.
Here, $3 \cdot |\text{TEAMS}|^2$ prompts are possible.

\subsection{Experiment: Longest cohesive prefix}\label{subsection:prompts_longest_cohesive_prefix}
The methodology followed the same rules as the experiment described
in subsection \ref{subsection:placeholders_longest_cohesive_prefix}.

\noindent
Table \ref{table:longest_cohesive_prefix_prompt_results} presents the aggregated results.
\begin{table}[H]
    \centering
    \caption{Longest cohesive prefix experiment aggregated results.}
    \begin{tabular}{|c|c|c|c|c|c|c|}
        \hline
        \texttt{NO} & \texttt{OK} & \texttt{OK,1} & \texttt{OK,2} & \texttt{OK,3} & \texttt{OK,4} & \texttt{OK,5} \\
        9 & 40 & 3 & 3 & 5 & 6 & 9\\
        \hline
        \texttt{OK,6} & \texttt{OK,7} & \texttt{OK,8} & \texttt{OK,9} & \texttt{OK,10} & \texttt{OK,11} & \texttt{OK,12} \\
        7 & 9 & 1 & 3 & 2 & 2 & 1\\
        \hline
    \end{tabular}
    \label{table:longest_cohesive_prefix_prompt_results}
\end{table}

\subsection{Experiment: Online quiz}\label{subsection:prompt_online_quiz}
The online quiz from subsection \ref{subsection:placeholders_online_quiz}
was reused to evaluate samples generated from prompts.

Once again, the quiz was shared on several subreddits (\textit{r/machinelearning},
\textit{r/datascienceproject} and \textit{r/soccer}) as well as Facebook groups
for University of Warsaw students. Results were collected after about a week.

In total, 831 votes were collected, giving a bit over 4 votes per text sample on average.
Votes were very well balanced -- 415 votes said \textit{REAL}, 416 said \textit{GENERATED}.
Figure \ref{fig:general_audience_results} presents aggregated results.

\subsection{Experiment: Online quiz on football fans}
Comparison of results presented in sections \ref{subsection:prompt_online_quiz}
and \ref{subsection:placeholders_online_quiz} shows that samples generated from
prompts are more believable to be real. Both experiments were conducted on a
mixed audience, with varying degrees of domain knowledge, so most participants
had to base their judgement on spotting logical and grammatical errors, or
searching for common patterns or artifacts in fake samples.

To raise the bar for prompted generation, another online quiz experiment was
done solely on football fans from \textit{r/football} subreddit.

This time, the quiz drew much more attention. In total, 6258 votes were
collected, 2910 stating that a text is real and 3348 that a text is fake.
Maximum number of votes per single article was 50 and the minimum -- 19.
Unsurprisingly, that audience did much better at distinguishing real and
fake articles. Figure \ref{fig:r_football_results} presents aggregated results.

\begin{figure}[h]
    \begin{tabular}{c|c}
        \begin{subfigure}{.49\textwidth}
            \centering
            \includegraphics[scale=0.6]{content/figures/text_quiz_votes_results.png}
            \caption{
                General audience online quiz results.
                Among the human-written articles, 50 were correctly identified as real (i.e. more
                votes said \textit{REAL} than \textit{GENERATED}), 30 were falsely named generated
                and in case of remaining 20, there was a tie.
                35 of the generated samples have fooled majority of voters, 43 were correctly identified
                and for the remaining 22, there was a tie.
                }
            \label{fig:general_audience_results}
        \end{subfigure}
        &
        \begin{subfigure}{.49\textwidth}
            \centering
            \includegraphics[scale=0.6]{content/figures/text_quiz_votes_results_r_football.png}
            \caption{
                \textit{r/football} online quiz results.
                Among the human-written articles, 76 were correctly identified as real (i.e. more
                votes said \textit{REAL} than \textit{GENERATED}), 21 were falsely named generated
                and in case of remaining 3, there was a tie.
                12 of the generated samples have fooled majority of voters, 87 were correctly identified
                and for the remaining 1, there was a tie.
                }
            \label{fig:r_football_results}
        \end{subfigure}
    \end{tabular}
    \caption{Online quiz aggregated results.}
\end{figure}


\begin{figure}[H]
    \begin{tabular}{c|c}
        \begin{subfigure}{0.48\textwidth}
            \tiny{
            \textit{
                \noindent
                \textbf{Doncaster Rovers > Crawley Town}\\
                Doncaster moved into the League One play-off places with victory over Crawley.\\ 
                The home side came closest to opening the scoring in the first half when Dan Jones' header from a corner hit the crossbar.\\ 
                Doncaster had the ball in the net when Karleigh Osborne drilled in a shot from 25 yards, but John Marquis' save cleared the danger - before the goal came.\\ 
                Rovers' third win in four games moves them up to fourth, four points clear of the bottom three.\\ 
                The goal came when Martin Samuelsen's corner had not been dealt with by the Crawley defence and fired into the net.\\ 
                Rovers' equaliser came on the stroke of half time, as the ball was knocked down to Osborne and the England Under-21 winger slid the ball into the net from close range.\\ 
                Crawley's best chance came in the final minute, when Josh Yorwerth's header from a corner was cleared off the line by Marko Marosi.\\ 
                Match report supplied by the Press Association.\\ 
                Match ends, Doncaster Rovers 2, Crawley Town 0.\\ 
                Second Half ends, Doncaster Rovers 2, Crawley Town 0.\\ 
                Foul by Dean Furman (Doncaster Rovers).\\ 
                Josh Yorwerth (Crawley Town) wins a free kick in the defensive half.\\ 
                Substitution, Crawley Town. Dannie Bulman replaces Oliver
            }
            }
            \end{subfigure}
            &
            \begin{subfigure}{0.48\textwidth}
                \tiny{
                    \textit{
                        \textbf{Blackburn Rovers == Norwich City}\\
                        Norwich City missed a chance to move into the top six with a defeat at Championship strugglers Blackburn Rovers.\\ 
                        Keaton Wintle headed in Conor Sammon's cross to give Norwich an early lead.\\ 
                        The Canaries, who looked sure to claim their first away win of the season, went behind when Alex Pritchard's header from a corner was turned into his own net by Darren Ambrose.\\ 
                        And the Canaries were level when Graham Dorrans' low shot from the left side of the area beat keeper Angus Gunn.\\ 
                        Blackburn's best effort came from substitute Chris Taylor, but he could not keep his effort down.\\ 
                        Norwich, who were without captain Russell Martin because of a broken ankle, led when Graham Dorrans' shot from the left side of the area was deflected past goalkeeper Angus Gunn.\\ 
                        The home side's lead lasted just three minutes, though.\\ 
                        Wintle headed home from a corner, but the striker was penalised for handball.\\ 
                        \\ 
                                                    \\ 
                                                \\ 
                        Norwich, who have won just once in six games, did not win their first shot on target, with Taylor's effort blocked by a sliding
                    }
                }
            \end{subfigure}
    \end{tabular}
    \caption{
        2 of the 12 generated articles that have fooled quiz participants
        into thinking they're human-written. Two articles with highest
        $\frac{\text{votes\_real}}{\text{votes\_total}}$ ratio were chosen.}
\end{figure}

Additional notes regarding results:
\begin{itemize}
    \item Participants often commented that what tipped them off was a wrong
          relation between entities (eg. a player listed in wrong team). To some
          extent this may have been caused by the fact that the model was trained on
          data from over 10 seasons so the team squads are surely inconsistent
          in training data. However, not all such mistakes can be attributed to
          the multi-season nature of corpus. For instance, in one generated
          sample, fooballer Kamil Glik is listed as FC Barcelona player (which
          was never true in any season of his career).
    \item Some participants have noted that while the test was easy for them to pass
          with a high score (such as 8 or 9 out of 10), it was only because they
          were explicitly searching for artifacts and other properties of
          machine-generated text and would not necessarily detect a fake article
          if they had casually read it.
    \item One of the most significant difficulties for the model was to
          consistently report on number of goals scored and chronology of the
          goals. This is why the task was simplified and the model was prompted
          only with an inequality sign between two teams, and not an exact end
          result.
    \item Note that only a random number of first $n \in [2, 3, ..., 10]$
          sentences was displayed to a participant, so one should not draw a
          conclusion that the model is capable of outputting a realistic
          complete article.
\end{itemize}

Besides evaluating the model on a more difficult audience, another motivation
behind this experiment was to seek correlation between number of sentences
presented to the reader and the difficulty in spotting fake texts.
At each step of the quiz, a random number of first sentences was presented
to the user with the following distribution\footnote{
    The same distribution applied to both real and generated samples.
}:\\
\begin{figure}[H]
    \centering
    \begin{tabular}{c|c|c|c|c|c|c|c|c|c}
        \#sentences & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 \\
        \hline
        $p$ & 0.0625 & 0.0625 & 0.0625 & 0.0625 & 0.0625 & 0.0625 & 0.0625 & 0.0625 & 0.5 \\
    \end{tabular}
\end{figure}

Distribution of samples that fooled the voters and those that were correctly
classified as fake, for each number $n$ of first $n$ sentences displayed,
is reported at Figure \ref{fig:num_lines_votes_distributions} below.
\begin{figure}[H]
    \centering
    \includegraphics[scale=0.65]{content/figures/text_quiz_votes_results_num_lines.png}
    \caption{Correlation between number of lines displayed to quiz paricipant and votes distribution.}
    \label{fig:num_lines_votes_distributions}
\end{figure}


\section{Experiment: Training a Transformer on Polish corpus}
The experiments reported above in this and previous chapter were all conducted
on an English corpus. The collected Polish articles, however more numerous
and requiring more effort to annotate, were left unused.
That is because the GPT-2 model was pretrained exclusively on English corpus
and is thus only suitable for fine-tuning on that language.

Benefiting from the TensorFlow Research Cloud (TFRC) programme, which offers
free access for 30 days to a limited selection of Google Cloud's Tensor
Processing Units (TPUs), an attempt was made to train a Transformer on the
Polish sport articles.

In the experiment, Tensor2Tensor library \citep{tensor2tensor} was used.
Firstly, a problem was defined as a subclass of \texttt{Text2TextProblem},
where inputs where entity sets, and targets -- sport articles\footnote{
    It's worth noting that at first unconditional generation was attempted
    with \texttt{Text2SelfProblem}, but there are known issues in Tensor2Tensor
    library with handling text problems with no inputs.
}. The maximum subtoken length was set to 8, and the dataset split proportions
were left at default values (train:dev = 100:1). Almost all of the 0.5M scraped
Polish articles were used: in total 490609 texts from \texttt{interia.pl/sport},
\texttt{sport.pl/pilka} and \texttt{pilkanozna.pl}.

The model was then trained for 250,000 iterations (around a week)\footnote{
    For comparison, \cite{transformer} have trained their big models for
    300,000 steps.
} on a single
TPU v3-8. Results were as underwhelming as they were interesting -- an extreme
case of overfitting has occured where the model has "memorized" training
samples. Whatever the trained model was prompted with -- real or hand-written
prompt -- it would always output a sample that was to be found in the training
data.

It's very likely that the model has just failed to generalize due to too small
training set. For comparison, \cite{transformer_tips} have used 58M samples
to train an English-to-Czech translator, and \cite{transformer} have trained
English-French translator on 36M sentence pairs and English-German translator on
4.5 milion sentence pairs.
