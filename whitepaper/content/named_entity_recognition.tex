\label{chapter:ner}
In order to build a language model conditioned on a set of entities, one must
be capable of identifying and categorizing entities in real data.

There exist out-of-the box neural NER models (for instance spaCy's
EntityRecognizer\footnote{
    https://spacy.io/api/entityrecognizer
}), however applying them to a specific task requires defining custom entity
types and training or fine-tuning the model on an annotated training set. Since
all data in the experiments consists of scraped articles from various sources,
it does not come with any sort of annotations. Labelling a training set would be
a lengthy manual process\footnote{
    It would be interesting to see how many manually labelled articles are actually needed
    to finetune a NER model to obtain satisfying results.
}, therefore a more database-driven solution was implemented.

In the experiments, 6 entity categories were selected:
\begin{itemize}
    \item \texttt{player} -- currently active footballer, former players do not
          belong in this category.
    \item \texttt{team} -- a football club. It can be ambiguous in an
          article reporting on a match between two national teams whether e.g.
          \textit{Germany} should be interpreted as a team or a country.
    \item \texttt{manager} -- a club/national team manager/coach/assisting coach.
          Club owners, financial directors etc. do not belong in this category.
    \item \texttt{league} -- any sort of club or international competition; may
          refer to either a recurring competition (\textit{World Cup}) or a
          particular instance (\textit{Euro 2016}).
    \item \texttt{country}
    \item \texttt{stadium}
\end{itemize}
It's easy to observe how \texttt{player} and \texttt{manager} categories are
problematic -- in an article from 2005 Zinedine Zidane would be mentioned as
a player, in 2020 he would appear as a manager of Real Madrid. An ideal language
understanding system should condition the evaluation of provided text on its
date. Even more challenges arise if we expect the language understanding system
to capture relations between entities, such as \texttt{player}'s belonging to a
\texttt{team}. This and other relations (\texttt{team}-\texttt{stadium},
\texttt{team}-\texttt{league}, \texttt{manager}-\texttt{team})
are also temporally conditioned and should be evaluated as such.
Since this work focuses on conditional generation, using NER only as a tool to
build a training set, rather than building a comprehensive football knowledge base,
a simple approach is taken and entity recognition does not depend on
article's date.

\section{Entities database}

Entities of the 6 selected types were scraped from \texttt{footballsquads.co.uk},
RapidAPI\footnote{https://rapidapi.com/api-sports/api/api-football},
\texttt{enfa.co.uk}\footnote{the \texttt{enfa.co.uk} website contains much more data
than downloaded, only several teams were hand-picked not to breach the website's
terms of use}, English Wikipedia pages about
England\footnote{https://en.wikipedia.org/wiki/List\_of\_football\_clubs\_in\_England},
Italy\footnote{https://en.wikipedia.org/wiki/2019\%E2\%80\%9320\_Serie\_A},
Northern Ireland\footnote{https://en.wikipedia.org/wiki/List\_of\_association\_football\_clubs\_in\_Northern\_Ireland},
Scotland\footnote{https://en.wikipedia.org/wiki/List\_of\_Scottish\_Professional\_Football\_League\_clubs} and
Spain\footnote{https://en.wikipedia.org/wiki/La\_Liga} as well as Polish Wikipedia
pages about
Bundesliga\footnote{https://pl.wikipedia.org/wiki/Bundesliga\_niemiecka\_w\_pi\%C5\%82ce\_no\%C5\%BCnej},
Ekstraklasa\footnote{https://pl.wikipedia.org/wiki/Ekstraklasa\_w\_pi\%C5\%82ce\_no\%C5\%BCnej\_(2018/2019)},
Italy\footnote{https://pl.wikipedia.org/wiki/Serie\_A},
French Ligue 1\footnote{https://pl.wikipedia.org/wiki/Ligue\_1} and
Spanish La Liga\footnote{https://pl.wikipedia.org/wiki/Primera\_Divisi\%C3\%B3n}.
Numbers of entities fetched from each source are listed in Figure \ref{fig:entity_database}.

\begin{figure}[h]
    \begin{subfigure}{\textwidth}
        \centering
        \tiny{
            \begin{tabular}{|c|c|c|c|c|c|c|}
                \hline
                & \textbf{player} & \textbf{team} & \textbf{manager} & \textbf{league} & \textbf{country} & \textbf{stadium}\\
                \hline
                \textbf{footballsquads.co.uk} & 297535 & 1426 & 7688 & 0 & 0 & 0\\
                \hline
                \textbf{RapidAPI} & 52379 & 9703 & 5124 & 1319 & 0 & 6230 \\
                \hline
                \textbf{Wikipedia: England} & 7954 & 940 & 926 & 0 & 0 & 856 \\
                \hline
                \textbf{Wikipedia: Italy} & 1050 & 19 & 17 & 0 & 0 & 16 \\
                \hline
                \textbf{Wikipedia: Nor. Ireland} & 607 & 30 & 30 & 0 & 0 & 27\\
                \hline
                \textbf{Wikipedia: Scotland} & 1174 & 41 & 38 & 0 & 0 & 41 \\
                \hline
                \textbf{Wikipedia: Spain} & 747 & 20 & 20 & 0 & 0 & 19 \\
                \hline
                \textbf{enfa.co.uk} & 3555 & 0 & 114 & 0 & 0 & 0 \\
                \hline
                \textbf{manual/other} & 1 & 12 & 2 & 61 & 197 & 438\\
                \hline
            \end{tabular}
        }
        \caption{English entities}
    \end{subfigure}

    \begin{subfigure}{\textwidth}
        \centering
        \tiny{
            \begin{tabular}{|c|c|c|c|c|c|c|}
                \hline
                & \textbf{player} & \textbf{team} & \textbf{manager} & \textbf{league} & \textbf{country} & \textbf{stadium}\\
                \hline
                \textbf{footballsquads.co.uk} & 309980 & 1426 & 7654 & 0 & 0 & 0\\
                \hline
                \textbf{RapidAPI} & 52379 & 9703 & 5124 & 1319 & 0 & 6230 \\
                \hline
                \textbf{Wikipedia: Bundesliga} & 667 & 21 &  14 & 0 & 0 & 18 \\
                \hline
                \textbf{Wikipedia: Ekstraklasa} & 178 & 60 & 4 & 0 & 0 & 5 \\
                \hline
                \textbf{Wikipedia: England} & 8070 & 1041 & 1024 & 0 & 0 & 921 \\
                \hline
                \textbf{Wikipedia: France} & 1325 & 36 & 29 & 0 & 0 & 31 \\
                \hline
                \textbf{Wikipedia: Italy} & 1439 & 44 & 28 & 0 & 0 & 28 \\
                \hline
                \textbf{Wikipedia: La Liga} & 840 & 24 & 21 & 0 & 0 & 21 \\
                \hline
                \textbf{manual/other} & 1 & 2 & 0 & 12 & 198 & 1 \\
                \hline
            \end{tabular}
        }
        \caption{Polish entities}
    \end{subfigure}
    \centering
    \caption{Comparison of sources scraped for entities}
    \label{fig:entity_database}
\end{figure}

\subsection*{English variants and filters}
In order to increase the chances of detecting an entity in an article, multiple
variants of each entity were created. To avoid false matches, filters
were applied to remove variants with very high false positive count.

\begin{itemize}
    \item person (player or manager) name variants -- if a person's name consists of two
          words (e.g. \textit{Firstname Lastname}), a variant \textit{Firstname [Lastname]} 
          is added to database. If it consists of two or more words and the last
          word is at least 5 letters long, 3 variants are added: \textit{Lastname},
          \textit{F Lastname}, \textit{F. Lastname}, where \textit{F} is the first letter
          of first word. If the name starts with \textit{Sir}, a variant without
          this title is added. If the person's nationality is known and it is
          among several chosen countries, a variant such as \textit{the Englishman},
          \textit{the Scot}, \textit{the Spaniard} etc. is added.
    \item team name variants -- if the team name contains one of the words
          \textit{United}, \textit{City}, \textit{Town}, \textit{Athletic},
          \textit{Rangers}, \textit{Rovers}, then it's probable the team will
          be referenced by just this single word (usually in a subsequent
          mention in an article, e.g. \textit{City} will appear after
          \textit{Manchester City}). Similarily, the team may be mentioned by the
          remaining part of the name, e.g. \textit{Manchester} as a shorthand for
          \textit{Manchester United}. Adequate variants were added to the database.
          For teams which have \textit{\&} in their names, it was replaced with
          \textit{and} and vice versa. Suffixes such as \textit{F.C.} or \textit{A.F.C.}
          where added or removed in different variants.
          Many Wikipedia pages about foootball clubs have a \textit{nicknames}
          field in their infoboxes. Those nicknames were also scraped and added to
          database (sometimes several variants of nicknames were created).
          If the manager's name is known (for instance it was available at the
          Wikipedia's infobox), then a variant \textit{[manager\_name]'s side} is added
          (for instance a variant of \textit{Tottenham Hotspur} would be \textit{Jose Mourinho's side}).
    \item stadium name variants -- from each name, the following combinations were
          stored: \textit{[stadium\_name]}, \textit{[stadium\_name] Arena}, \textit{[stadium\_name] Stadium},
          \textit{[stadium\_name] Park}.
    \item person name filters -- some person name variants had to be filtered out,
          because they were matched too often in articles, in a context that did
          not refer to those persons. For instance, it's clear to see why footballer
          \textit{Tom Corner} was matched too often or players whose last name
          is the same as a country/month/day of the week. Some erroneous variants
          were a result of a Wikipedia page where player's names and nationalities
          columns were swapped in a table and had to be filtered out.
          \begin{figure}[H]
            \small{
            \texttt{['Sunday', 'England', 'Northern Ireland', 'Ireland', 'Portugal',
                 'N Ireland', 'N. Ireland', 'France', 'Denmark', 'Spain',
                 'Sweden', 'Ecuador', 'Netherlands', 'DR Congo', 'D. Congo',
                 'Congo', 'Mali', 'Croatia', 'Albania', 'Wales', 'London',
                 'Manager', 'Scotland', 'Nigeria', 'Jamaica', 'Ghana',
                 'Zimbabwe', 'August', 'October', '-', 'English', 'Corner',
                 'March', 'February', 'German', 'Madrid', 'French', 'Irish',
                 'until', 'Friday', 'Under']}
            }
            \centering
            \caption{The full list of filtered out player names.}
          \end{figure}
\end{itemize}

\subsection*{Polish variants and filters}
Recognizing Polish entity names was a much more difficult task due to declination and
shortage of sources to scrape (compared to vast choice of APIs with English names).
While many (foreign) names are the same in Polish articles (\textit{Manchester United}),
some may have different Polish variants (\textit{Juventus Turyn} -- not only is the
city name translated, but it's frequently appended to the club name, which is
not so common in English sources). Polish person names usually come in two
variants -- formal (\textit{Jakub}) and informal (\textit{Kuba})\footnote{
    There was an idea to create name variants by replacing formal names with informal ones,
    but unfortunately no good name database was found.
}.
\pagebreak
As for declination, there exist some good libraries, such as
pystempel\footnote{https://pypi.org/project/pystempel/} or
Morfeusz\footnote{http://morfeusz.sgjp.pl/}, however performance was an issue when
trying to process hundreds of thousands of names. Instead, some hand-picked heuristics
for matching and replacing word suffixes were used to create multiple variants of
each name. As one can notice after reading about the search algorithm below,
appending incorrect declinations to the database does not harm the accuracy --
it was more important to include the correct declination in the list of variants
than it was to not include an erroneous one. Some words were blacklisted from
declination, for example \textit{the}, \textit{of}, \textit{City}, \textit{United}.
Others were declensed using the patterns listed in Table \ref{fig:declination_suffixes}.
Some word suffix replacement patterns were collected by manual inspection of the
corpus, others were found in a book \textit{Nauka o języku dla polonistów}\citep{naukaojezyku}.
In a name consisting of multiple words, all words were declensed into several
variants and all combinations were added to database -- since this procedure
is exponential, names with 4 or more words were not declensed at all (but
their shorter variants were).

\begin{table}[H]
    \centering
    \caption{Declination heuristics -- word suffix replacement patterns.}
    \tiny{
        \begin{tabular}{|c|c|}
            \hline
            \textbf{suffix} & \textbf{replacements} \\
            \hline
            iga & idze, igi, ige \\
            \hline
            ski & skiego, skiemu, skim \\
            \hline
            cki & ckiego, ckiemu, ckim \\
            \hline
            y & ego, emu, ym \\
            \hline
            wel & wla, wlowi \\
            \hline
            ia & ii,ie \\
            \hline
            ek & ku, ka, kiem, kowi \\
            \hline
            a & i, ie, y \\
            \hline
            ko & ki, ce \\
            \hline
            ka & ki, ce, kiej \\
            \hline
            lli & llim, lliego, lliemu \\
            \hline
            n & nie \\
            \hline
            t & cie \\
            \hline
            en & nu, nowi, nem \\
            \hline
            es & su, sa, sem \\
            \hline
            ec & cu, ca, cowi, cem \\
            \hline
            ja & i \\
            \hline
            r & rze \\
            \hline
            (any consonant) & +owi, +em, +a, +u, +e \\
            \hline
            (any vowel) & +'owi, +'a \\
            \hline
            Pogon & Pogoni, Pogonia \\
            \hline
            Zaglebie & Zaglebia, Zaglebiem, Zaglebiu \\
            \hline
        \end{tabular}
    }
    \label{fig:declination_suffixes}
\end{table}

The variant creation and filtering rules were similar for Polish entities as
they were for English. Several additional filters were added, such as \textit{Canal}
(there is a popular TV provider \textit{Canal} in Poland) or \textit{Europa}
(which was very often wrongly matched with an \textit{Europa F.C.} club from
Gibraltar).

\section{Search algorithm}
There are hundreds of thousands of entities in the database, and millions of
different variants in total. A naive implementation of pattern matching is thus
too computationally expensive even for very short articles. Instead, a search
procedure aided by a trie tree was used. 

First, a trie tree is built from the database. The value stored in each leaf
is a list of base variants that the string can be resolved to. For instance,
\texttt{trie['the Blues'] = [('Chelsea F.C.', 'team'), ('Everton F.C', 'team'),
('Burnham F.C.', 'team'), ...]}.
A simplified version of the algorithm collecting entities mentioned in an
article is presented below:
\begin{algorithm}[H]
    \caption{Collect mentioned entities}\label{algo:highlighter_simmplified}
    \small{
    \begin{algorithmic}[1]
        \STATE{$mentions \gets \emptyset$}
        \STATE{$i \gets 0$}
        \WHILE{$i < |A|$}
            \STATE{$s \gets \epsilon$}
            \STATE{$j \gets i$}
            \WHILE{$A[i:j]$ is a path in $trie$}
                \IF{$A[i:j]$ ends in a leaf \AND $A[j+1]$ is not a letter}
                    \STATE{$s \gets A[i:j]$}
                \ENDIF
                \STATE{$j \gets j + 1$}
            \ENDWHILE
            \IF{$s \neq \epsilon$}
                \STATE{$value \gets \epsilon$}
                \STATE{$category \gets \epsilon$}
                \FOR{$v, c$ in $trie[s]$}
                    \STATE{$value \gets v$}
                    \STATE{$category \gets c$}
                    \IF{$(v, c)$\ in\ $mentions$}
                        \BREAK
                    \ENDIF
                \ENDFOR
                \STATE{$mentions \gets mentions \cup \{(value, category)\}$}
                \STATE{$i \gets i + |s|$}
            \ELSE
                \STATE{$i \gets i + 1$}
            \ENDIF
        \ENDWHILE
        \RETURN{$mentions$}
    \end{algorithmic}
    }
\end{algorithm}
The actual implementation is not position-agnostic and takes a callable as a
parameter allowing for different handling of entities -- building prompts for
generation, annotating a dataset, replacing names with placeholders etc. The
check in line 18 increases the quality of entity resolution; for instance if
the substring \textit{City} is matched and \textit{Norwhich City} was found
earlier, then \textit{City} will be resolved to \textit{Norwhich City} and not
\textit{Manchester City}.

\begin{figure}[h]
    \begin{subfigure}{.48\textwidth}
        \tiny{\texttt{::\textcolor{red}{Andy Halliday} extends \textcolor{green}{Middlesbrough} deal until 2015::\\
        @1363258836\\
        url: https://www.bbc.com/sport/football/21783091\\
        \textcolor{green}{Middlesbrough} utility player \textcolor{red}{Andy Halliday} has signed a two-year contract extension with the \textcolor{blue}{Championship} club.
        and the former \textcolor{green}{Livingston} player has agreed terms.
        Since arriving at the \textcolor{orange}{Riverside (Riverside Stadium)}, under \textcolor{cyan}{Gordon Strachan}'s management in August 2010, \textcolor{red}{Halliday (Andy Halliday)} has made 32 appearances.
        Although he arrived at \textcolor{green}{Middlesbrough} as a winger, \textcolor{red}{the Scot (Andy Halliday)} has played at full-back this season.}}
        \caption{English sample}
    \end{subfigure}
    \begin{subfigure}{.48\textwidth}
        \tiny{\texttt{
        ::\textcolor{cyan}{Zinedine Zidane} znalazl nastepce \textcolor{red}{Marcelo} w \textcolor{green}{Realu Madryt (Real Madryt)}. 40 milionow euro! Pilka nozna - Sport.pl::\\
        @2019-05-02 11:52\\
        url: [...] \\ %http://www.sport.pl/pilka/7,65082,24745532,zinedine-zidane-znalazl-nastepce-marcelo-w-realu-madryt-40.html
        Wedlug gazety \_\textcolor{red}{Marca (Lavinius Marca)}\_ \textcolor{cyan}{Zinedine Zidane}\_poprosil wladze klubu sciagniecie do klubu\_lewego obroncy \textcolor{green}{Olympique (Olympique Lillois) Lyon (Olympique Lyonnais)}, 23-letniego\_\textcolor{red}{Ferlanda Mendy (Ferland Mendy)}\&aposego.\_W ostatnich tygodniach w mediach pojawialy sie rozne nazwiska, jak: \textcolor{red}{Junior Firpo} (\textcolor{green}{Betis (Real Betis)}), \textcolor{red}{Alex Grimaldo} (\textcolor{green}{Benfica}) i \textcolor{red}{David Alaba} (\textcolor{green}{Bayern (Bayern Monachium)}), ale to \textcolor{red}{Mendy (Ferland Mendy)} najbardziej\_przekonuje trenera Krolewskich.
        REKLAMA}}
        \caption{Polish sample}
    \end{subfigure}
    \caption{
        A demo of the entity recognition heuristics -- example articles with
        entities of different categories highlighted with different colors.
        The values in parentheses are base variants that the mentions were resolved to.
    }
\end{figure}

\section{Evaluation}
A test set was prepared to evaluate the NER heuristics. 100 articles from
\url{http://www.sport.pl/pilka} and 100 articles from \url{https://www.bbc.co.uk/sport/football}
chosen by a random number generator were manually annotated with the 6 categories
of entities. Figure \ref{fig:ner_test_set} showcases two samples from the test set.
During evaluation, each annotated article was translated to a set of tuples $(start, end, category)$\footnote{
    This is the same format as spaCy NER component uses.
},
where $(start, end)$ is a character-level range in the article. The \texttt{Highlighter} class
encapsulating the entity recognition logic was asked to produce such tuples from
unlabelled article. The two resulting sets -- ground truths and predictions -- were
then compared and F-score, precision and recall metrics were computed. The end metrics were averaged over all articles.
Table \ref{fig:ner_results} shows the evaluation results on both test sets.
\begin{table}[H]
    \centering
    \caption{NER evaluation}
    \begin{tabular}{|c|c|c|c|c|}
        \hline
        \textbf{language} & \textbf{\#entities} & \textbf{F-score} & \textbf{precision} & \textbf{recall} \\
        \hline
        \textbf{en} & 4586 & 0.797617 & 0.782374 & 0.829196\\
        \hline
        \textbf{pl} & 2862 & 0.657026 & 0.731524 & 0.614048\\
        \hline
    \end{tabular}-
    \label{fig:ner_results}
\end{table}
There are certain limitations to this evaluation methodology. Entities cannot
overlap, so a phrase \textit{former England international}, referring to current
Manchester United's first coach (as of 15.03.2020) Michael Carrick, can be labelled
as \texttt{manager}, but \textit{England} can be labelled as \texttt{country}.
Moreover, the test set is not annotated with base variants of detected entities
(a base variant of \textit{former England international} here would be \textit{Michael Carrick}).
It is thus impossible to say whether the \texttt{Highlighter} class under test
resolves the entities correctly (modulo category).

\begin{figure}[h]
    \begin{subfigure}{.48\textwidth}
        \tiny{\texttt{
            ::\textcolor{green}{\{team:Cliftonville\}} sign ex-\textcolor{green}{\{team:Ballymena\}} goalkeeper \textcolor{red}{\{player:Ryan Brown\}}::\\
            @1307556749\\
            url: https://www.bbc.com/sport/football/13705050\\
            \textcolor{green}{\{team:Cliftonville\}} have signed former \textcolor{green}{\{team:Bangor\}}, \textcolor{green}{\{team:Larne\}} and \textcolor{green}{\{team:Ballymena United\}} goalkeeper \textcolor{red}{\{player:Ryan Brown\}}.
            \textcolor{red}{\{player:Brown\}} spent last season with \textcolor{green}{\{team:Ballymena United\}} and becomes manager \textcolor{cyan}{\{manager:Tommy Breslin\}}'s fifth signing of the summer.\\
            "I'm looking forward to getting started," said former junior international keeper \textcolor{red}{\{player:Brown\}}.
            "\textcolor{green}{\{team:Cliftonville\}} have shown great consistency over the last few years and have been challenging at the top end of the table."
            Other \textcolor{green}{\{team:Reds\}} signing in recent days includes former \textcolor{green}{\{team:Newry\}} and \textcolor{green}{\{team:Portadown\}} defender \textcolor{red}{\{player:John Convery\}} and ex-\textcolor{green}{\{team:Glentoran\}} midfielder \textcolor{red}{\{player:Peter Steele\}}.
        }}
        \caption{English sample}
    \end{subfigure}
    \begin{subfigure}{.48\textwidth}
        \tiny{\texttt{
            ::\textcolor{green}{\{team:BATE\}} podbija \textcolor{blue}{\{league:Ligę Mistrzów\}} Piłka nożna - Sport.pl::\\
            @2008-10-01 12:54\\
            \textcolor{green}{\{team:Juventus\}} w \textcolor{blue}{\{league:Lidze Mistrzów\}}: \textcolor{green}{\{team:Marynarze\}} w histerii i stracone punkty\\
            Dla \textcolor{green}{\{team:BATE\}} mecz z \textcolor{green}{\{team:Włochami\}} był ósmym w tej edycji \textcolor{blue}{\{league:LM\}}. \textcolor{green}{\{team:Białorusini\}} zaczynali walkę o fazę grupową, gdy \textcolor{red}{\{player:Alessandro del Piero\}} i \textcolor{red}{\{player:Pavel Nedved\}} wylegiwali się na karaibskich plażach. W I rundzie \textcolor{green}{\{team:BATE\}} pokonało islandzkie \textcolor{green}{\{team:Valur\}} (2:0 i 1:0), w drugiej \textcolor{green}{\{team:Anderlecht Bruksela\}} (2:1 i 2:2), a w trzeciej \textcolor{green}{\{team:Lewski Sofia\}} (1:0 i 0:0). \textcolor{green}{\{team:Białorusini\}} są zespołem z najmniejszym współczynnikiem (1,760) UEFA w fazie grupowej \textcolor{blue}{\{league:LM\}}. Poprzednim była \textcolor{green}{\{team:Artmedia Petrżałka\}} ze współczynnikiem niemal trzykrotnie większym (4,850).\\
            \textcolor{green}{\{team:Słowacy\}}, gdy grali w fazie grupowej \textcolor{blue}{\{league:LM\}} mieli budżet ok. 1,5 mln euro. Tyle samo ma dziś \textcolor{green}{\{team:BATE\}}. Większy ma nawet \textcolor{green}{\{team:Odra Wodzisław\}}. Albo raczej miała, bo po sukcesach \textcolor{green}{\{team:Białorusini\}} chcą go powiększyć do 4 mln euro.
        }}
        \caption{Polish sample}
    \end{subfigure}
    \caption{Samples from the NER test set}
    \label{fig:ner_test_set}
\end{figure}
\pagebreak
Readers are encouraged to reproduce the NER evaluation experiment in a demo
Colab notebook attached to EDGAR repository\footnote{
    \url{https://colab.research.google.com/drive/1PURy7p7jscwRxjLBnqNaKzxV6hyHPhJe?usp=sharing}
}. It is advised to do it after reading at least chapters
\ref{chapter:prompted_generation} and \ref{chapter:edgar}.
    Please note that the results obtained in the Colab notebook are a bit lower than
reported above. That is because, in the reported experiment, the piority of
categories was carefully selected -- if there exist two entities with the same
name (not necessarily same base name, but the sets of name variants overlap)
but different categories, the following prioritization of categories applied:
\begin{itemize}
    \item[1.] \textsc{Country}
    \item[2.] \textsc{Team}
    \item[3.] \textsc{Player}
    \item[4.] \textsc{Manager}
    \item[5.] \textsc{Stadium}
    \item[6.] \textsc{League}
\end{itemize}
\noindent
whereas the order of entities in the example \texttt{Highlighter} configuration
included in EDGAR repository is random (\textsc{League}, \textsc{Country},
\textsc{Stadium}, \textsc{Manager}, \textsc{Player}, \textsc{Team}). Readers are
encouraged, as an exercise, to experiment with the order of entities to improve
the final F-score.