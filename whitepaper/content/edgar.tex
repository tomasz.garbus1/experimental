\label{chapter:edgar}
From the experimental repository\footnote{
    \url{https://gitlab.com/tomasz.garbus1/experimental}

    This repository contains most of the work described here (except for the
    online quizes and Scraper tool).
}, a set of key functionalities was extracted to build an out-of-the-box
solution for prompted generation -- a tool named
EDGAR (\textbf{E}ntity \textbf{D}riven \textbf{G}PT-2-based \textbf{A}utomatic
w\textbf{R}iter) hosted at \url{https://github.com/tomaszgarbus/edgar}.

EDGAR consists of 3 main components:
\begin{itemize}
    \item clone of the \href{https://github.com/minimaxir/gpt-2-simple}{gpt-2-simple} repository
    \item NER module described in chapter \ref{chapter:ner}
    \item a set of scripts binding the two together
\end{itemize}
\noindent
There are also two demo Colab notebooks:
\begin{itemize}
    \item \href{https://colab.research.google.com/drive/1oB6UicgeQEIaGrf2JHD_8AwJXel4gjV0?usp=sharing}{E2E demo Colab notebook}
    demonstrating how to use EDGAR on E2E dataset \citep{e2e_dataset} to increase
    generated text compliance with control codes ($\sim 0.94$ F-score versus $\sim 0.85$
    obtained by raw GPT-2 model)
    \item \href{https://colab.research.google.com/drive/1PURy7p7jscwRxjLBnqNaKzxV6hyHPhJe?usp=sharing}{football demo Colab notebook},
    which uses the provided football config Json. In this notebook, EDGAR
    achieves $\sim 0.52$ F-score while raw GPT-2 scores only $\sim 0.25$ (although
    the model is highly underfitted for the sake of reasonable cells execution time).
\end{itemize}

\section{Usage}
\subsection*{highlight\_demo.py}
A demo script for the Highlighter class. User is expected to provide an input
path and a Json config file. The script outputs the samples from input path
in random order, highlighting the entities:
\begin{figure}[H]
    \centering
    \includegraphics[scale=0.65]{content/figures/highlight_demo.png}
    \caption{Output of the \texttt{highlight\_demo.py} script.}
\end{figure}
\begin{Verbatim}[fontsize=\small]
usage: highlight_demo.py [-h] -i I -c C

optional arguments:
    -h, --help  show this help message and exit
    -i I        input file or directory
    -c C        config json path
\end{Verbatim}
\subsection*{build\_prompts.py}
Creates an annotated dataset.
\begin{Verbatim}[fontsize=\small]
usage: build_prompts.py [-h] -i I -o O -c C [--limit LIMIT]

optional arguments:
    -h, --help     show this help message and exit
    -i I           Dataset input file directory.
    -o O           Prompted dataset output directory.
    -c C           Highlighter config file path.
    --limit LIMIT  Limit of output files. Use this parameter if you want the
                    script to run for a shorter time.
\end{Verbatim}
\subsection*{finetune.py}
Finetunes GPT-2 model on the annotated dataset.
\begin{Verbatim}[fontsize=\small]
usage: finetune.py [-h] -i I [--num_steps NUM_STEPS]
                   [--model_name {124M,355M,774M,1558M}] [--multi-gpu]

optional arguments:
    -h, --help            show this help message and exit
    -i I                  Input corpus directory.
    --num_steps NUM_STEPS
    --model_name {124M,355M,774M,1558M}
            Name of the finetuned model to use. Note that most
            likely you don't have enough computing power to use
            1558M or 774M.
    --multi-gpu           Whether or not to use multiple GPU cores. This may be
            useful if you have an Tesla K80.
\end{Verbatim}
\subsection*{generate.py}
Generates one or more samples from the provided control codes.
\begin{Verbatim}[fontsize=\small]
usage: Generate samples from finetuned model. [-h] -i I -c C -p P -o O
    [--steps STEPS]
    [--step_length STEP_LENGTH]
    [--samples_per_step SAMPLES_PER_STEP]
    [--multi-gpu] [--limit LIMIT]

optional arguments:
    -h, --help            show this help message and exit
    -i I                  Input directory you have used to train the model.
    -c C                  Highlighter config. Refer to ner/schema.py for the
    JSON schema definition.
    -p P                  Prompts source directory.
    -o O                  Output directory for samples.
    --steps STEPS         Number of generation steps.
    --step_length STEP_LENGTH
    Number of tokens generated at each step.
    --samples_per_step SAMPLES_PER_STEP
    Number of samples to select from at each step.
    --multi-gpu           Whether or not to use multiple GPU cores. This may be
    useful if you have an Tesla K80.
    --limit LIMIT         Maximum number of samples to generate.
\end{Verbatim}

\section{Example use: E2E dataset}
E2E dataset was selected for an EDGAR demo since it comes with manual annotations
and it's fairly easy to build configuration file for. This dataset is also a
good example that even if the data is already annotated, the NER module of
EDGAR can still be benefited from during generation.

An example use of EDGAR (on E2E dataset, following the Colab demo) looks as follows:
\subsection*{Build a configuration JSON}
EDGAR is configured by providing a JSON file following the schema:
\begin{Verbatim}[fontsize=\small]
    schema = {
    "type": "object",
    "required": ["categories"],
    "properties": {
        "categories": {
            "type": "array",
            "description": "Categories of entities",
            "items": {
                "type": "object",
                "required": ["name", "entities"],
                "properties": {
                    "name": {
                        "type": "string",
                        "description": "Category name."
                    },
                    "entities": {
                        "type": "array",
                        "description": "Entities belonging to the category.",
                        "items": {
                            "required": ["name", "variants"],
                            "name": {
                                "type": "str",
                                "description": "Base variant of the entity name."
                            },
                            "variants": {
                                "type": "array",
                                "description": "Other variants of the entity name.",
                                "items": {
                                    "type": "str"
                                }
                            }
                        }
                    }
                }
            }
        },
        "case_sensitive": {
            "type": "boolean",
            "description": "Case-sensitive entity name matching.",
            "default": True
        }
    }
}
\end{Verbatim}
An example configuration file is provided at \href{https://github.com/tomaszgarbus/edgar/blob/master/configs/football.json}{configs/football.json}.
The user must hand-pick a set of categories of entities that they want to
condition the generation on. On E2E dataset, this step is particularly simple,
because all texts are already annotated with a set of properties:
\texttt{area, familyFriendly, food, eatType, priceRange, name, near, customer rating}.

\subsection*{Optional: Evaluate the Highlighter}
\texttt{Highlighter} is a central class to the NER component. It executes the
algorithm \ref{algo:highlighter_simmplified} to detect entities in texts and
builds prompts. A helper function \texttt{evaluate} in
\href{https://github.com/tomaszgarbus/edgar/blob/master/ner/evaluate_highlighter.py}{evaluate\_highlighter.py}
prints to standard output three metrics for each category and for all categories
altogether: precision, recall and F-score.

\subsection*{Annotate the dataset}
Next we annotate the dataset by prepending a prompt to each text. An example
execution of the \texttt{build\_prompts} script:
\begin{figure}[H]
    \centering
    \small{
    \texttt{\$ python build\_prompts.py -i trainset\_nlr.txt -c configs/e2e\_v2.json -o train\_data --limit 1000}
    }
\end{figure}
\noindent
where:
\begin{itemize}
    \item \texttt{trainset\_nlr.txt} is the input path. The input path may either
          be a single file (then each line is a separate text) or a (potentially multi-level) directory
          (then each file is a separate text).
    \item \texttt{configs/e2e\_vs.json} is the config JSON produced for E2E dataset.
    \item \texttt{train\_data} is the output directory.
    \item \texttt{----limit 1000} is used to set the maximum number of output files.
          For a very large dataset it makes little sense to wait for a long time
          to get it annotated, if we do not plan on fine-tuning the model for
          that many steps.
\end{itemize}

\subsection*{Finetune GPT-2}
After building prompts we are ready to finetune the model:
\begin{figure}[H]
    \centering
    \small{
        \texttt{\$ python finetune.py -i train\_data --num\_steps 1000}
    }
\end{figure}

\subsection*{Generate samples}
Samples can be generated either by a call to the \texttt{generate} function from
\href{https://github.com/tomaszgarbus/edgar/blob/master/generate.py}{generate.py}
or by running a script:
\begin{figure}[H]
    \centering
    \small{\texttt{
        \$ python generate.py -i train\_data -o gen\_samples -c configs/e2e\_v2.json -p dev\_prompts --limit 10 --steps 5 --step\_length 6 --samples\_per\_step 5
    }}
\end{figure}
\noindent
The parameters in the above command are:
\begin{itemize}
    \item \texttt{-i train\_data} is the path to an input directory that the model
          was finetuned on. This value is used to retrieve the correct model checkpoint
          (as there may be potentially multiple cached models).
    \item \texttt{-o gen\_samples} is the output directory.
    \item \texttt{-c configs/e2e\_v2.json} is, again, the config json.
    \item \texttt{-p dev\_prompts} is the input directory with prompts. One sample
          will be generated for each file in \texttt{dev\_prompts} (unless \texttt{----limit} is provided).
    \item \texttt{----limit 10} sets the maximum number of samples to generate.
          If provided, prompts are selected randomly with uniform distribution.
    \item \texttt{----steps 5} sets the number of generation steps (vide algorithm \ref{algo:generate_sample})
    \item \texttt{----step\_length 6} sets then number of tokens to output in each generation step.
    \item \texttt{----samples\_per\_step 5} sets the number of variants generated in
          each step, from which the one with highest F-score is selected.
\end{itemize}