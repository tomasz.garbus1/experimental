\section{Related work}

More dated NLG systems are often based on hand-coded templates or grammars.
Facts are extracted from the knowledge base (\textit{what to say}) and a rule-based
text generator constructs a sentence (\textit{how to say it}).
\cite{kukich_83} describes an (at the time of publication) partially implemented
computer program that reports on stock market. \cite{yag} present a YAG
(Yet Another Generator) -- an efficient, general-purpose with expressive
formal representation language. \cite{reiter_2005} build a system that generates
weather forecasts from numerical data by careful engineering of rules and paying
attention to the choice of words; for instance, their study includes statistical
research on what is considered \textit{evening} or \textit{late evening} by
larger part of population.

\cite{howald_2013} use \textit{Boxer} computer program \citep{boxer} to build
Discourse Representation Structures \citep{drt}. They use domain general entity
tags such as \textsc{company}, \textsc{date}, \textsc{date} to transform
training sentences to templates. Finally, they use statistical methods to select
the best template for a given conceptual unit and fill in the gaps with entities
listed in the unit.

\cite{neuraltemplates} propose an approach to neural natural language generation
by using neural hidden semi-markov model (HSMM) to learn latent, discrete
templates. Their model learns to successfully extract templates from E2E Dataset
\citep{e2e_dataset} and to semantically split the sequence into multi-word
segments.

\section{Discussion}
\subsection*{Languages with declension}
English is particularly convenient for template-driven generation. Usually,
a sequence from the domain-specific corpus can be split into mutliple
interleaving segments: \textit{part of template}, \texttt{entity:type},
\textit{part of template}, \texttt{entity:type}, \textit{part of template}, ...
since the types of entities are often limited to
\begin{itemize}
    \item[a)] proper nouns
    \item[b)] nouns in general
    \item[c)] numerical data
    \item[d)] verbs from a limited dictionary, (e.g.
    \textit{increased}/\textit{decreased} token conditioned on the sign of a
    certain numerical value)
\end{itemize}
For the sake of providing concrete data to the user, all we need to know about
a template, is the set of entities we need to fill, together with their types.
The rest of tokens in the template is only important to the end user, but not
to a program providing data to the text generator.

Consider an example from the football domain:
\begin{figure}[H]
    \centering
    \textit{Cristiano Ronaldo's pass was intercepted by Michał Pazdan.}
\end{figure}
\noindent
In some knowledge-based system we could create a template:
\begin{figure}[H]
    \centering
    \textit{\{player:pass\_origin\}'s pass was intercepted by \{player:pass\_intercept\}.}
\end{figure}
\noindent
Consider the same sentence in Polish:
\begin{figure}[H]
    \centering
    \textit{Podanie Cristiano Ronaldo przejęte przez Michała Pazdana.}
\end{figure}
\noindent
Clearly, a template:
\begin{figure}[H]
    \centering
    \textit{Podanie \{player:pass\_origin\} zostało przejęte przez \{player:pass\_intercept\}.}
\end{figure}
\noindent
will not render a grammatically correct sentence when filled with entity names
in nominative case. On the other hand, the following template:
\begin{figure}[H]
    \centering
    \textit{\{player:pass\_origin\} podaje, jednak \{player:pass\_intercept\} przejął to podanie.}
\end{figure}
\noindent
conveys the same fact, yet expects entities in different case.

\subsection*{Enriching templates with synonyms}
A tempting techinque for enriching the set of hand-written templates may be
replacing some words with synonyms. \cite{charcnn} have used this approach to
data augmentation when training a CNN sentence classifier. Concretely, they have
used a Thesaurus from LibreOffice, which in turn was obtained from WordNet\footnote{
    The Thesaurus data is available at \url{https://raw.githubusercontent.com/LibreOffice/dictionaries/master/en/th_en_US_v2.dat}
}.
The number of words chosen to replace, as well as the index of
the chosen synonym (synonyms in WordNet are ordered by decreasing
relevance) were determined by a geometric distribution with
parameter $0.5$. In general, CharCNN variants trained on such augmented data have
obtained better results.

Note that applying this method to texts presented to the end user must be done
with much more caution and possibly hand-picked filters of words to avoid.

Consider a toy example, a fragment of a sequence from Stanford Sentiment Treebank\footnote{
    https://nlp.stanford.edu/sentiment/treebank.html
}:
\begin{figure}[H]
    \centering
    (...) goes on and on to the point of nausea.
\end{figure}
Let's review different parametrizations of the geometric distribution
determining the number of replaced words ($\rho$) and the index of selected
synonym ($\phi$). For each of four pairs of values $\rho$ and $\phi$, 5
samples were generated from Thesaurus:
\begin{figure}[H]
    \begin{tabular}{c|c}
        \begin{subfigure}{.48\textwidth}
            \small{
                (...) goes on and on to the location of disgust.\\
                (...) goes on and on to the convexity of disgust.\\
                (...) goes on and on to the factor of disgust.\\
                (...) goes on and on to the item of sickness.\\
                (...) goes on and on to the detail of disgust.
            }
            \caption{$\rho = 0.1, \phi = 0.1$}
        \end{subfigure} & 
        \begin{subfigure}{.48\textwidth}
            \small{
                (...) goes on and on to the component of sickness.\\
                (...) goes on and on to the constituent of symptom.\\
                (...) goes on and on to the component of sickness.\\
                (...) goes on and on to the component of sickness.\\
                (...) goes on and on to the component of sickness.
            }
            \caption{$\rho = 0.1, \phi = 0.9$}
        \end{subfigure} \\
        \hline
        \begin{subfigure}{.48\textwidth}
            \small{
                (...) goes on and on to the location of nausea.\\
                (...) goes on and on to the convexity of nausea.\\
                (...) goes on and on to the point of disgust.\\
                (...) goes on and on to the item of nausea.\\
                (...) goes on and on to the detail of nausea.
            }
            \caption{$\rho = 0.9, \phi = 0.1$}
        \end{subfigure} &
        \begin{subfigure}{.48\textwidth}
            \small{
                (...) goes on and on to the component of nausea.\\
                (...) goes on and on to the constituent of nausea.\\
                (...) goes on and on to the point of sickness.\\
                (...) goes on and on to the component of nausea.\\
                (...) goes on and on to the component of nausea.
            }
            \caption{$\rho = 0.9, \phi = 0.9$}
        \end{subfigure}
    \end{tabular}
    \centering
\end{figure}
Using such data augmentation technique may be useful when building a classifier,
especially if there are no pre-trained word embeddings. However, most of the
samples presented above cannot be presented to a human user with an expectation
that they will understand the message.

\section{Template generation with GPT-2}\label{subsection:template_generation}
A powerful language model such as GPT-2 may be a promising tool for generating
domain-specific templates, as an alternative for laborious hand-engineering of
templates. The language model must be first finetuned on a training
set, where controlled variables are replaced with descriptive placeholders.
The model then generates large number of texts with placeholders that are to be
filled with concrete data, in this case -- 6 types of entities.

\subsection*{Setting}
GPT-2 medium (355M) model was finetuned on a subset of BBC Sport
corpus containing only match reports (see Figure
\ref{fig:bbc_entities_sizes}). Each article in this sub-corpus
has a normalized title format:
\begin{figure}[H]
    \centering
    \texttt{team1 \textbackslash d-\textbackslash d team2}
\end{figure}
\noindent
where \texttt{\textbackslash d} denotes a digit. Two modifications were made
in the training (fine-tuning) set. First, in each article title, the substring
\texttt{\textbackslash d-\textbackslash d} was replaced with one of the three:
\texttt{<}, \texttt{>}, \texttt{==}.\footnote{
    This modification was made because previous experiments have shown that
    prompting GPT-2-medium with exact match score is too hard of a task and the
    number of goals described in an article would rarely match the final result.
} For instance, a title
\begin{figure}[H]
    \texttt{Dundee United 0:3 Celtic}
    \centering
\end{figure}
\noindent
would become:
\begin{figure}[H]
    \texttt{Dundee United < Celtic}
    \centering
\end{figure}
\noindent
Secondly, all detected entities were replaced with placeholders formatted as
\begin{figure}[H]
    \texttt{\{[category]:[id]\}}
    \centering
\end{figure}
\noindent
For instance, the title
\begin{figure}[H]
    \texttt{Dundee United < Celtic}
    \centering
\end{figure}
\noindent
is replaced with:
\begin{figure}[H]
    \texttt{\{team:0\} < \{team:1\}}
    \centering
\end{figure}
Of course, subsequent occurrences of the same entity have the same \texttt{id}.
GPT-2 model was finetuned for 16000 iterations (about 15-16 hours on a single core of
Nvidia Tesla K80).
100 samples were then drawn from the model. Each sample had 300 tokens (shorter
than majority of articles in the training set).

\section{Human evaluation of text cohesion}\label{section:placeholders_human_evaluation}
Samples generated as described in subsection \ref{subsection:template_generation}
were put under human reader evaluation. Two experiments were conducted:
\begin{itemize}
    \item Longest cohesive prefix, conducted by one person seeking inconsistencies
          in generated samples.
    \item Online quiz, where a larger number of judges had to determine whether
          the presented article is real or not.
\end{itemize}
Later, in section \ref{section:prompt_human_evaluation} identical experiments
were conducted with control-code-driven generation.

% This work mostly approaches controllable text generation
% by controlling the set of entities mentioned in an article,
% at the same time maintaining the quality of the language model.
% This is convenient since we have a programmatic way (vide chapter
% \ref{chapter:ner}) of evaluating generated samples.
% Unfortunately, most facts conveyed in an article, such as events,
% relationships between entities, cannot be easily detected with
% pattern matching or other domain-agnostic method.

% Additional experiments based on evaluation by a human reader
% were made.

\subsection{Experiment: Longest cohesive prefix}\label{subsection:placeholders_longest_cohesive_prefix}
First, all samples were manually analysed\footnote{
    Author has attempted building a classifier which, given a match report sans
    title, predicts the result of the described match, but results were poor
    and unpromising.
} by one person and assigned one of the
three statuses:
\begin{itemize}[leftmargin=3cm]
    \item[\texttt{NO}] The text contained (anywhere) a contradiction with the
        match result stated in the title.
    \item[\texttt{OK,n:int}] The generated text agreed with the title on the
        matter of who won the game, but there was some internal contradiction
        in the generated sample. For instance, the first sentence stated that
        the match ended in a goalless draw, but later a goal was reported.
        The number $n$ means that first $n$ sentences form a cohesive text, but
        the $n+1$-th sentence creates a contradiction with something stated
        earlier.
        A partial repetition of the previous sentence (a common issue for GPT-2)
        was also counted as "bad sentence".
    \item[\texttt{OK}] The entire sample was cohesive, did not contain any
        contradictions or (partially) repeated sentences or other artifacts. 
        Note that the sample didn't necessarily have to be a complete article,
        since generation was always trimmed at 300 tokens, not at \texttt{<|endoftext|>}.
\end{itemize}
In this experiment, the placeholders in generated articles were not populated.

\noindent
Below are presented the aggregated results:
\begin{figure}[H]
    \centering
    \caption{Longest cohesive prefix experiment results.}
    \begin{tabular}{|c|c|c|c|c|c|c|}
        \hline
        \texttt{NO} & \texttt{OK} & \texttt{OK,1} & \texttt{OK,2} & \texttt{OK,3} & \texttt{OK,4} & \texttt{OK,5} \\
        25 & 0 & 0 & 36 & 11 & 11 & 11\\
        \hline
        \texttt{OK,6} & \texttt{OK,7} & \texttt{OK,8} & \texttt{OK,9} & \texttt{OK,10} & \texttt{OK,11} & \texttt{OK,12} \\
        3 & 2 & 1 & 0 & 0 & 0 & 0\\
        \hline
    \end{tabular}
\end{figure}

\noindent
Most common inconsistencies in the generated samples were:
\begin{itemize}
    \item contradictions regarding the match score
    \item unchronological order in reporting of events (one sentence reporting on a goal
          in last 8 minutes, the next one describing a goal scored in the first half).
    \item introduction of too many entities, for instance \texttt{\{team:2\}} (which clearly
          did not play in a match between teams \texttt{\{team:0\}} and \texttt{\{team:1\}}).
          This particular inconsistency may be a result of imperfect entity resolution during
          dataset labelling.
\end{itemize}

\subsection{Experiment: Online quiz}\label{subsection:placeholders_online_quiz}
A certain convenience in evaluating a powerful model such as GPT-2 lies in the
fact that a very natural approach to human judgement methodology can be
applied: the judge has to predict whether the text sample is real or generated.

An online quiz was implemented as a Django web application.
The database contains 100 samples drawn from GPT-2 and 100 samples randomly
chosen from BBC Sport sub-corpus of match reports.
There are 10 questions in the quiz -- randomly chosen samples, with two answers:
\textit{Real} and \textit{Generated}, although website visitors were encouraged
to take the test more than once, so that they can improve their sensitivity
to common patterns in generated articles. After each question the person taking
the quiz receives feedback (\textit{[Wrong|Correct]! The text was [real|generated].}).
Each sample is trimmed to a random number of sentences between 5 and 10.
The titles follow the \texttt{team1 ? team2} format where \texttt{?} is an
inequality sign.
Placeholders in generated articles were populated by randomly sampling entity names
from English leagues (with uniform distribution).\footnote{
    It would be interesting to see how the placeholder population strategy affects
    the text quality. Perhaps using the same probability distribution as in real
    corpus would make more realistic articles. Ideally, one should account for
    relationships between entities when filling the placeholders. For instance,
    when filling a text fragment \texttt{"\{team:0\} manager \{manager:2\}"},
    the choice of team entity should determine the choice of manager entity.
    This would require building more complex placeholders, preserving this kind
    of relationships and was not explored in this work.
}
Displayed articles were decorated with a single, (usually) relevant
image, obtained live from Google Images via Custom Search API, queried with the
article's title.

Results were collected about 7 days after deploying the website. In the meantime,
the quiz was shared with other students (from different faculties) and on machine
learning and soccer subreddits.

In total, 593 votes were collected, giving almost 3 votes per text sample on average.
Votes were well balanced -- 278 votes said \textit{REAL}, 315 said \textit{GENERATED}.
Figure \ref{fig:online_quiz_placeholders_results} presents aggregated results.

\begin{figure}[h]
    \centering
    \includegraphics[scale=0.65]{content/figures/text_quiz_placeholders_votes_results.png}
    \caption{
        Among the human-written articles, 59 were correctly identified as real (i.e. more
        votes said \textit{REAL} than \textit{GENERATED}), 24 were falsely named generated
        and in case of remaining 17, there was a tie.
        16 of the generated samples have fooled majority of voters, 62 were correctly identified
        and for the remaining 22, there was a tie.
        }
    \label{fig:online_quiz_placeholders_results}
\end{figure}


\begin{figure}[h]
    \centering
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[scale=0.5]{content/figures/text_quiz_intro.png}
        \caption{Introduction and instructions}
    \end{subfigure}

    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[scale=0.5]{content/figures/text_quiz_example.png}
        \caption{An example question}
    \end{subfigure}
    \caption{Screenshots from \url{https://text-quiz.herokuapp.com/}}
\end{figure}