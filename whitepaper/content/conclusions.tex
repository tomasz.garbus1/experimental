Controlled text generation is an area of rapid development in deep learning.
Most efforts made to gain control over generated content can be categorized into
three main paradigms:
\begin{itemize}
    \item Template-driven generation, where templates can be either hand-engineered
          or machine-generated.
    \item Control codes, an intuitive extension of the language modelling problem,
          where the token probability distribution is conditioned not only
          on previous tokens, but also on the control code. In fact, any language
          modelling system capable of generating a continuation of text, could
          be trained for conditional generation by simply pretraining on a dataset
          prepended by control codes.
    \item Adversarial training and reinforcement learning techniques or
          combination of the two.
\end{itemize}
Each paradigm could be explored in more detail before any
definitive statements are made about one's superiority over another.
However, the presented experiments point to the following conclusions:
\begin{itemize}
    \item Control codes are a quick way to build a text generation system that
          produces realistic-looking samples, even more so if one uses a
          pretrained language model.
    \item Generation of templates requires domain-specific knowledge. In the
          online quiz experiment, template-based samples were found to be much
          easier to distinguish from human-written articles than the ones produced
          with only control codes.
    \item The \textsc{Reinforce} algorithm gives much more freedom in specifying
          the goal for our system to optimize. There are however some downsides:
    \begin{itemize}
        \item Not all objectives are equally easy to optimize. If the objective
              is defined by a discriminator that keeps on learning, even a
              positive reward may be hard to obtain.
        \item The RL setup can act like an evil genie who exploits loopholes in
              one's wish. For instance, if BLEU score is being optimized, the
              generator may find that a rare sequence of consecutive \texttt{<unk>}
              tokens scores high and repeat it over and over.
        \item Most reported results from RL or adversarial based architectures
              are on very short texts. To author's knowledge, the only RL-based
              system generating long samples of high quality is GPT-2 finetuned
              on human responses \citep{gpt2_ft_rl}.
    \end{itemize}
\end{itemize}
We must also recognize that the toy domain of football articles comes with its
own characteristics:
\begin{itemize}
    \item relative repetitiveness and limited number of facts to be said, compared to e.g. politics
    \item large number of distinct entities, relations between which change over time
\end{itemize}
\noindent
that are not representative of all possible domains.

Readers are encouraged to try out the control-code-driven generation using
EDGAR repository on various domains.