\section{Language modelling}
A language model is simply defined as a learned probability distribution over texts
from a given language, however this definition does not express what is expected
from a language model to be practically applicable.
Usually the probability distribution is decomposed as a chain of conditional
probabilities \citep{bengio2003}:
\begin{equation}
      p(x) = \overset{n}{\underset{i=1}{\prod}}p(s_i|s_1,...,s_{i-1})
      \label{equ:lang_model}
\end{equation}
\noindent
where $x = s_1s_2...s_{n}$ and
$s_{1}, s_{2}, ...$ are tokens (words, interpunction,
acronyms, numbers, EOF markers etc.) belonging to the language.
Typically, a natural language generation (NLG) system will, at each step of
generation (usually one step means one token, but there exist exceptions)
estimate probabilities over the dictionary and output one token. Strategies for
selecting the token include drawing from the estimated distribution, random choice
from top $k$ tokens and a simple argmax (such generator is then deterministic).
Note that language modelling formulation in \ref{equ:lang_model} generalizes
well to specific tasks; estimating and maximizing conditional probability
$p(output|input)$ is the common denominator of machine translation (MT) ($input$:
original, $output$: translation), image captioning ($input$: image,
$output$: description)\footnote{
      Of course in both examples of MT and image captioning,
      the input and output languages are different. An image encoding format
      is not a natural language, but otherwise the problem statement and even
      evaluation methods are similar as in MT (both are often
      evaluated against a set of reference answers). Also approaches to those
      tasks are different at implementation level, at least at the phase of
      processing the $input$.
}, text summarization ($input$: original text, $output$: summary) and question
answering ($input$: question, $output:$ answer).

A simple implementation of a language model is a recurrent neural network (RNN),
trained with the Teacher Forcing \citep{teacherforcing} algorithm. At the training
time, a sequence is fed to the RNN; at each step the network predicts the next
output but regardless of its prediction, a correct token is provided as the next
input. While vanilla RNNs are known to suffer from \textit{vanishing gradients}
and thus fail to learn long-term dependencies \citep{isdifficult}, variants
incorporating a hidden state, such as LSTM \citep{lstm} or GRU \citep{gru} are
used in practice. Architectures with hidden-to-hidden connections are trained
with both BPTT (backpropagation through time) and Teacher Forcing \citep[p.~378]{deeplearningbook}.

Such RNN with hidden state can be then applied in an encoder-decoder architecture \citep{gru}
to tasks like MT or conditional generation. The encoder reads the input sequence
$(s_1, s_2, .., s_n)$ and produces a \textit{context vector} $z = [z_1, z_2, ..., z_d]^T$
of fixed dimensionality $d$. The context vector is then fed into the decoder RNN
at each timestep of decoding.

\cite{seq2seq_with_attention} show that the fixed-length context vector is
the bottleneck in improving the performance of the model, an issue that becomes
more severe as the sequence length increases. They improve this architecture by
replacing a single context vector per sequence with one context vector per timestep.
The encoder is a bi-directional LSTM, producing \textit{forward hidden states}
$\overset{\rightarrow}h_1, \overset{\rightarrow}h_2, ..., \overset{\rightarrow}h_{T_x}$
in the forward run and \textit{backward hidden states}
$\overset{\leftarrow}h_1, \overset{\leftarrow}h_2, ..., \overset{\leftarrow}h_{T_x}$
in the backward run. The context vector at timestep $t$ is then a concatenation
$h_t = [\overset{\rightarrow}{h_t^\top}; \overset{\leftarrow}{h_t^\top}]^\top$, containing the
summaries of both the preceding and following words.
The decoder, at each timestep $t$, computes the current context vector $c_t$
as the weighted average $c_t = \underset{j=1}{\overset{T_x}{\sum}} \alpha_{tj}h_t$,
where $\alpha_{tj}$ are softmaxed scores obtained by all context vectors $h_1, h_2, ..., h_{T_x}$.
A score for each context vector is assigned by a feedforward neural network
taking as an input current hidden state of the decoder and the context vector.

\subsection{Transformer}
On top of the improvements made in NLG thanks to the attention mechanism
a Transformer architecture \citep{transformer} was created -- a stack of 6
encoders and 6 decoders.
Both encoders and decoders implement a novel \textit{Self-Attention} mechanism,
chosen based on three desiderata: a) total computational complexity per layer,
b) ability to parallelize the computation, c) the path length between long-range
dependencies in the network. Table \ref{fig:self_attention_table} shows that 
Self-Attention is more attractive than recurrent or convolutional layers both
in terms of parallel computation and explicit dependence of tokens at distant
positions.
\begin{table}[H]
      \centering
      \caption{
            Table by \cite{transformer}. $n$ is the sequence length, $c$ is the
            representation dimension, $k$ is the convolution kernel size.
      }
\small{
      \begin{tabular}{|c|c|c|c|}
            \hline
            \textbf{Layer type} & \textbf{Complexity per layer} & \textbf{Sequential operations} & \textbf{Maximum path length} \\
            \hline
            Self-Attention & $O(n^2 \cdot d)$ & $O(1)$ & $O(1)$ \\
            \hline
            Recurrent & $O(n \cdot d^2)$ & $O(n)$ & $O(n)$ \\
            \hline
            Convolutional & $O(k \cdot n \cdot d^2)$ & $O(n)$ & $O(\log_k(n))$ \\
            \hline
      \end{tabular}
}
      \label{fig:self_attention_table}
\end{table}
In Self-Attention layer, three weight matrices are learned: $W_Q, W_K, W_V$,
producing, in result of multiplication, vectors for each input token
intuitively thought of as (respectively): \textit{query}, \textit{key}, \textit{value}.
Query vector for word $i$-th $q_i$ is obtained by multiplying $x_i$ by $W_Q$, where
$x_i$ is the representation (e.g. embedding or output from previous layer) of
$i$-th word. Similarly key vector $k_i$ and value vector $v_i$ are obtained.
When processing self-attention for $i$-th word, all other words are scored by
computing a scaled dot product of their key vectors with $q_i$. The scores
are then softmaxed and a weighted sum of all value vectors is computed:\footnote{Equation $(1)$ in \cite{transformer}}
\begin{equation}
      Attention(Q,K,V) = softmax(\frac{QK^\top}{\sqrt{d_k}})V
\end{equation}
\noindent
where $Q, K, V$ are the matrices query, key and value matrices and $d_k$ is dimensionality
of keys. Scaling down by $\sqrt{d_k}$ serves to prevent softmax gradient saturation effect.
Transformer extends this self-attention mechanism further to a \textit{Multi-Headed Attention},
meaning that several ($h = 8$ in original work) attention heads run in parallel.

Each encoder layer consists of a self-attention sublayer and a position-wise
feedforward network. Each decoder layer consists of a self-attention sublayer,
followed by an "encoder-decoder" attention and a feedforward network. In the
"encoder-decoder" layers, the queries come from previous decoder layers whereas
keys and values -- from the last encoder layer.

Other architectural choices in the Transformer model include:
\begin{itemize}
      \item[a)] positional encoding, used to inject the information about word's
                position in a sequence into the input embedding
      \item[b)] residual connections around each sublayer inside each encoder and
                decoder layer
      \item[c)] regularization: dropout with rate added $0.1$ to the output each
                sublayer and to the sums of embeddings and positional encodings;
                label smoothing
\end{itemize}

All experiments with Transformer architecture in this work use pretrained GPT-2
model from \url{https://github.com/minimaxir/gpt-2-simple}, more specifically
GPT-2-medium, with 355M trainable parameters, consisting of 24 stacked decoders,
with model dimensionality set to $1024$ (dimensionality of base Transformer
model is $512$).

Interestingly, there exists a tool detecting text generated by GPT-2
\citep{gltr}, which, given a text sample, estimates probability of each token
using a small (117M) GPT-2 model. Authors show that human-written texts have
more randomness (less likely tokens).

\section{Desiderata}\label{section:desiderata}
Our goals, expressed in natural language, are twofold:
\begin{itemize}
      \item[(1)] We would like to obtain control over the message conveyed by generated text.
                 This calls for selecting some property of text that we wish to control and
                 a programmatic way of measuring compliance of generated samples with the
                 selected attribute. For the sake of larger part of this study, texts will be
                 parametrized by sets of occurring entities. It is realistic (although non-trivial)
                 to automatically evaluate samples' compliance with such specifications. The variety of entity
                 names occurring in the football domain also guarantees that the problem is
                 more difficult than multi-class conditional generation.
      \item[(2)] We wish to achieve (1) without sacrificing quality, originality and other properties
                 of text that make it appealling to a human reader.
\end{itemize}
For those objectives to be any useful, we must formulate them in a computable
way.

\subsection{F-score}
A good candidate for measuring (1) is
F-score.

Consider a text $T_X$ conditioned on a set of entities $X$. Let
$Y = \text{NER}(T_X)$, a set of entities occurring in the generated text $T_X$,
where $\text{NER}$ is a Named Entity Recognition module.

\textbf{Precision} measure is defined as $\frac{X \cap Y}{Y}$. In other words,
it is the ratio of entities mentioned in $T_X$ that were also supplied to the
generator in set $X$, among all entities mentioned in $T_X$.

\textbf{Recall} is the ratio of those entities in $X$ that were mentioned in
$T_X$. It is defined as $\frac{X \cap Y}{X}$.

\textbf{F-score} is the harmonic mean of the two:
\begin{equation}
      F_1 = \frac{2}{\text{recall}^{-1} + \text{precision}^{-1}}
\end{equation}

\subsection{BLEU metric}
Bilingual Evaluation Understudy (BLEU) metric, despite being designed for
machine translation problem, is also the most widespread metric among other
text generation problems, be it conditional (text2text) or unconditional
(text2self) generation task.

BLEU is computed for a generated text $T_X = t_1t_2...t_n$ against a set of references
$\mathcal{R}_X = R_X^1, R_X^2, ..., R_X^k$, where $X$ is the conditioning value. For instance,
in an English-Polish machine translation task, $X$ is a sentence in English,
$T_X$ is a Polish translation generated by the model and $R_X^1, R_X^2, ..., R_X^k$
are reference translations.

Let $\#_t(W)$ denote number of occurrences of token $t$ in word $W$ and $D$ be
the dictionary -- set of all tokens. The BLEU metric is then defined as:
\begin{equation}
      \text{BLEU}(T_X; \mathcal{R}_X) = \underset{t \in D, \#_t(X) > 0}{\sum} \min\Big(1; \frac{\underset{1 \leq i \leq k}{\max} \#_t(R_i) }{\#_t(X)}\Big)
\end{equation}
In other words, we compute precision of output tokens. For every token, we add
$1$ to the result if it occurs in some reference text and $0$ otherwise. Then
we divide the result by the text length $|T_X| = n$. There is only one modification:
in order to prevent the generator from repeating the same token over and over,
each distinct token can only contribute to the final score as many times as it
maximally occurs in some reference text.

Usually, a brevity penalty is also added to BLEU, to prevent the generator from
outputting very short unrealistic texts. In this work, such penalty is not used
in BLEU-evaluated experiments, since all generated texts are of fixed length.

For the reference sets, random subsets of the training data are selected (in
a similar manner that one would extract a development or validation set). There
is a property of BLEU:
\begin{equation}
      \mathcal{R} \subseteq \mathcal{R}^\prime \Rightarrow \text{BLEU}(T; \mathcal{R}) \leq \text{BLEU}(T; \mathcal{R}^\prime)
\end{equation}
that makes it attractive for text2self and general language modelling tasks.

BLEU measure can be also generalized to bigrams, trigrams, and $n$-grams for any
$n \in \mathbb{N}_{+}$. We will denote such metric BLEU-$n$.

\subsection*{Comparison of LSTM and GPT-2 on BBC Sport}

In order to verify that BLEU is indeed a meaningful metric, it was computed
in first 5 variants for two generated corpora: one generated by LSTM, another
one by GPT-2. As expected, the Transformer model scored much better on the
BLEU metric, especially for longer n-grams. Both models were trained on the
same corpus of articles scraped from BBC Sport.

\begin{table}[H]
    \caption{Evaluation of corpora generated by LSTM and by GPT-2 using BLEU metrics.}
    \begin{tabular}{|c|c|c|}
          \hline
          \textbf{metric} & \textbf{LSTM} & \textbf{GPT-2} \\
          \hline
          BLEU-1 & 0.992 & 0.981 \\
          \hline
          BLEU-2 & 0.909 & 0.899 \\
          \hline
          BLEU-3 & 0.617 & 0.752 \\
          \hline
          BLEU-4 & 0.327 & 0.555 \\
          \hline
          BLEU-5 & 0.172 & 0.374 \\
          \hline
    \end{tabular}
    \centering
    \label{fig:compare_lstm_gpt2}
\end{table}

\section{Three paradigms of controlled generation}
\cite{pastpresentbeyond} categorizes training techniques for training RNNLMs
(Recurrent Neural Network Language Models) into three paradigms:
supervised learning, reinforcement learning and adversarial training.
While all three techniques are presented and experimented with in this work,
here the division is different. The paradigms described below are not approaches
to training a language model but techniques of gaining control over generated content.

\begin{itemize}
    \item \textbf{Generation with placeholders} is an approach based on
          replacing all occurrences of certain types of tokens, e.g. entities
          with placeholders during training and filling those gaps with desired
          values at generation time.
    \item \textbf{Generation from prompt} is conditional generation from more or
          less complex "prompts" -- from simple control codes such as "positive"
          or "negative" to sets of entities that should be mentioned in text.
    \item \textbf{Adversarial training and reinforcement learning}. While
          both adversarial training and RL can be used to train a language model
          with placeholders or conditioned on a prompt, they are, in theory, more powerful
          than traditional MLE methods and deserve a separate category.
          Most such models are first pretrained using simple MLE and then
          start to optimize another goal. In practice, adversarial and RL
          architectures for NLG haven't yet matched the results achieved by
          large Transformer-based models. A probable explanation is that all
          adversarial architectures proposed so far are computationally heavy
          and cannot be parallelized.
\end{itemize}