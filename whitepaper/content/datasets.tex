\section{Scraper}
All data sources were scraped using a dedicated Scraper repository\footnote{
    https://github.com/tomaszgarbus/Scraper
}. The \texttt{Scraper} class crawls the source website in a BFS order. This
procedure is resumable thanks to caching the scraper's state on disk on keyboard
interrupt and every 100 articles. To start scraping a data source user needs to subclass
\texttt{ScraperConfig} and implement its abstract methods:
\begin{itemize}
    \item \texttt{home} -- Starting page, provided as an absolute URL.
    \item \texttt{domain} -- The domain of the visited portal. It will be used
    to create absolute URLs from relative URLs by concatenation.
    \item \texttt{state\_cache\_fname} -- Filename of the \texttt{ScraperState} cache. It will be placed in \texttt{cache} folder.
    \item \texttt{should\_follow\_link} -- Given a link, determines whether scraper should queue it for visit.
    \item \texttt{extract\_article} -- If the visited web page contains an article, extracts its content and builds an \texttt{Article} object. Otherwise returns \texttt{None}.
    \item (optional) \texttt{fetch\_page} -- Function fetching the web page given its URL. For some websites it might be necessary to override it (e.g. to handle Polish characters correctly or sleep 1 second to not get banned).
\end{itemize}
Such implementation of the Scraper allows for quickly setting up a very simple config
for websites like BBC Sport (see \texttt{configs/bbcsport.py}\footnote{https://github.com/tomaszgarbus/Scraper/blob/master/configs/bbcsport.py}),
free from paywalls, not bloated with ads or JavaScript. On the other hand, more
complex logic can be implemented for slower, script-driven websites (see
\texttt{configs/orzeczenia.py}\footnote{https://github.com/tomaszgarbus/Scraper/blob/master/configs/orzeczenia.py}).
Each Article object is a tuple $(title, date, content)$, where $date$ format is not specified,
it may also be null. Each article is then stored in the \texttt{output\_directory/$date$}
path.
Table \ref{fig:datasets} lists datasets downloaded with purpose of using in this work.
\begin{table}[H]
    \centering
    \caption{Comparison of scraped datasets}
    \begin{tabular}{ |c|c|c|c| }
        \hline
        \textbf{Dataset} & \textbf{Language} & \textbf{Articles} & \textbf{Only football}\\
        \hline
        bbc.co.uk/sport/football & en & 92824 & y \\
        \hline
        football.co.uk & en & 2565 & y \\
        \hline
        pilkanozna.pl & pl & 121382 & y \\
        \hline
        sport.pl/pilka & pl & 57105 & y \\
        \hline
        fakt.pl/sport/pilka-nozna & pl & 250 & y \\
        \hline
        sport.interia.pl/ & pl & 312122 & n \\
        \hline
        pap.pl & pl & 9322 & n \\
        \hline
    \end{tabular}
    \label{fig:datasets}
\end{table}

Since BBC Sport is the only large English scraped resource with football articles,
all further experiments (unless conducted on Polish language) use it as the corpus.

\section{Analysis}
\subsection*{Common entity sets}
The implementation of named entity recognition (NER) heuristics, described in
chapter \ref{chapter:ner}, allowed to build a simple representation of each article,
as a set of all named entities of certain types occurring in them. Presented
below at Figure \ref{fig:apriori_itemsets}  the list of most common entity
subsets in BBC Sport dataset of cardinalities 1-6 computed with
{\sc Apriori} \citep{apriori} algorithm (min support: $0.01$, min confidence: $0.2$).

\begin{figure}[h]
    \centering
    \begin{subfigure}{\textwidth}
        \centering
        \begin{tabular}{|c|c|c|}
            \hline
            
            \textbf{Size} & \textbf{Itemset} & \textbf{Occurrences} \\
            \hline
            
            \multirow{5}{*}{1} & \tiny Premier League & \tiny 24641 \\
            \cline{2-3}
            & \tiny Championship & \tiny 16398 \\
            \cline{2-3}
            & \tiny England & \tiny 15216 \\
            \cline{2-3}
            & \tiny Manchester United F.C. & \tiny 12560 \\
            \cline{2-3}
            & \tiny Chelsea F.C. & \tiny 10860 \\
            \hline

            \multirow{5}{*}{2} & \tiny Manchester United F.C.,Premier League & \tiny 7307 \\
            \cline{2-3}
            & \tiny England,Premier League & \tiny 6908 \\
            \cline{2-3}
            & \tiny Chelsea F.C.,Premier League & \tiny 6717 \\
            \cline{2-3}
            & \tiny Manchester City F.C.,Premier League & \tiny 6225 \\
            \cline{2-3}
            & \tiny Liverpool F.C.,Premier League & \tiny 6102 \\
            \hline

            \multirow{5}{*}{3} & \tiny Chelsea F.C.,Manchester United F.C.,Premier League & \tiny 3284 \\
            \cline{2-3}
            & \tiny Manchester City F.C.,Manchester United F.C.,Premier League & \tiny 3203 \\
            \cline{2-3}
            & \tiny Chelsea F.C.,Manchester City F.C.,Premier League & \tiny 3178 \\
            \cline{2-3}
            & \tiny Liverpool F.C.,Manchester United F.C.,Premier League & \tiny 2983 \\
            \cline{2-3}
            & \tiny Arsenal F.C.,Chelsea F.C.,Premier League & \tiny 2927 \\
            \hline

            \multirow{5}{*}{4} & \tiny Chelsea F.C.,Manchester City F.C.,Manchester United F.C.,Premier League & \tiny 2005 \\
            \cline{2-3}
            & \tiny Chelsea F.C.,Liverpool F.C.,Manchester City F.C.,Premier League & \tiny 1883 \\
            \cline{2-3}
            & \tiny Arsenal F.C.,Chelsea F.C.,Manchester City F.C.,Premier League & \tiny 1859 \\
            \cline{2-3}
            & \tiny Chelsea F.C.,Liverpool F.C.,Manchester United F.C.,Premier League & \tiny 1830 \\
            \cline{2-3}
            & \tiny Arsenal F.C.,Chelsea F.C.,Manchester United F.C.,Premier League & \tiny 1819 \\
            \hline        

            \multirow{5}{*}{5} & \tiny Arsenal F.C.,Chelsea F.C.,Manchester City F.C.,Manchester United F.C.,Premier League & \tiny 1372 \\
            \cline{2-3}
            & \tiny Chelsea F.C.,Liverpool F.C.,Manchester City F.C.,Manchester United F.C.,Premier League & \tiny 1351 \\
            \cline{2-3}
            & \tiny Arsenal F.C.,Chelsea F.C.,Liverpool F.C.,Manchester City F.C.,Premier League & \tiny 1282 \\
            \cline{2-3}
            & \tiny Arsenal F.C.,Liverpool F.C.,Manchester City F.C.,Manchester United F.C.,Premier League & \tiny 1254 \\
            \cline{2-3}
            & \tiny Arsenal F.C.,Chelsea F.C.,Liverpool F.C.,Manchester United F.C.,Premier League & \tiny 1249 \\
            \hline

            6 & \tiny Arsenal F.C.,Chelsea F.C.,Liverpool F.C.,Manchester City F.C.,Manchester United F.C.,Premier League & \tiny 1037 \\
            \hline
        \end{tabular}
        \caption{Most common itemsets of different sizes in BBC Sport dataset.}
        \label{fig:apriori_itemsets}
    \end{subfigure}
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[scale=0.3]{content/figures/entity_set_sizes_bbcsport_and_only_matches.png}
        \caption{
            Histogram of cardinalities of entity sets detected in all BBC Sport articles.
            Heights of blue bars correspond to all articles, orange ones only to match reports.
            Match reports statistically contain more distinct entities since all goals, red cards,
            substitutions and other significant events are listed at the bottom of the article.
        }
        \label{fig:bbc_entities_sizes}
    \end{subfigure}
    \caption{BBC Sport entity sets analysis.}
\end{figure}

\begin{table}[h]
    \caption{
        Most common itemsets of different sizes in Sport.pl dataset. Note that the occurrences of
        \textit{Borussia Monchengladbach} are most likely results of wrong entity resoluion.
        It's common in Polish articles about Robert Lewandowski that the name
        \textit{Borussia} occurs before the full name \textit{Borussia Dortmund}.
        Another examples of wrong resolution found within top 50
        common entities are \textit{Real Valladolid} (from \textit{Real}) and
        \textit{Biggleswade F.C.} (\textit{FC} is listed as its nickname on Wikipedia).
        }
    \centering
    \begin{tabular}{|c|c|c|}
        \hline
        
        \textbf{Size} & \textbf{Itemset} & \textbf{Occurrences} \\
        \hline
        
        \multirow{5}{*}{1}
        & \tiny Polska & \tiny 66223 \\
        \cline{2-3}
        & \tiny Bayern Monachium & \tiny 36878 \\
        \cline{2-3}
        & \tiny Robert Lewandowski & \tiny 35732 \\
        \cline{2-3}
        & \tiny Barcelona & \tiny 32351 \\
        \cline{2-3}
        & \tiny Real Madryt & \tiny 29694 \\
        \cline{2-3}
        & \tiny Premier League & \tiny 27889 \\
        \cline{2-3}
        & \tiny Niemcy & \tiny 26618 \\
        \cline{2-3}
        & \tiny Legia Warszawa & \tiny 26098 \\
        \cline{2-3}
        & \tiny Bundesliga & \tiny 24391 \\
        \cline{2-3}
        & \tiny Juventus & \tiny 21346 \\
        \hline

        \multirow{5}{*}{2}
        & \tiny Bayern Monachium,Robert Lewandowski & \tiny 20795 \\
        \cline{2-3}
        & \tiny Polska,Robert Lewandowski & \tiny 16954 \\
        \cline{2-3}
        & \tiny Bayern Monachium,Bundesliga & \tiny 15556 \\
        \cline{2-3}
        & \tiny Legia Warszawa,Polska & \tiny 14953 \\
        \cline{2-3}
        & \tiny Bundesliga,Robert Lewandowski & \tiny 13932 \\
        \cline{2-3}
        & \tiny Bayern Monachium,Niemcy & \tiny 13888 \\
        \cline{2-3}
        & \tiny Niemcy,Robert Lewandowski & \tiny 11588 \\
        \cline{2-3}
        & \tiny Bundesliga,Niemcy & \tiny 10872 \\
        \cline{2-3}
        & \tiny Niemcy,Polska & \tiny 10531 \\
        \cline{2-3}
        & \tiny Barcelona,Real Madryt & \tiny 10401 \\
        \hline

        \multirow{5}{*}{3}
        & \tiny Bayern Monachium,Bundesliga,Robert Lewandowski & \tiny 11217 \\
        \cline{2-3}
        & \tiny Bayern Monachium,Niemcy,Robert Lewandowski & \tiny 8814 \\
        \cline{2-3}
        & \tiny Bayern Monachium,Polska,Robert Lewandowski & \tiny 8120 \\
        \cline{2-3}
        & \tiny Bayern Monachium,Bundesliga,Niemcy & \tiny 7789 \\
        \cline{2-3}
        & \tiny Bundesliga,Niemcy,Robert Lewandowski & \tiny 6734 \\
        \cline{2-3}
        & \tiny Bayern Monachium,Borussia Dortmund,Robert Lewandowski & \tiny 5952 \\
        \cline{2-3}
        & \tiny Bundesliga,Polska,Robert Lewandowski & \tiny 5779 \\
        \cline{2-3}
        & \tiny Bayern Monachium,Borussia Dortmund,Bundesliga & \tiny 5584 \\
        \cline{2-3}
        & \tiny Bayern Monachium,Borussia Monchengladbach,Robert Lewandowski & \tiny 5329 \\
        \cline{2-3}
        & \tiny Borussia Dortmund,Bundesliga,Robert Lewandowski & \tiny 5192 \\
        \hline

        \multirow{5}{*}{4}
        & \tiny Bayern Monachium,Bundesliga,Niemcy,Robert Lewandowski & \tiny 5618 \\
        \cline{2-3}
        & \tiny Bayern Monachium,Bundesliga,Polska,Robert Lewandowski & \tiny 4473 \\
        \cline{2-3}
        & \tiny Bayern Monachium,Borussia Dortmund,Bundesliga,Robert Lewandowski & \tiny 4217 \\
        \cline{2-3}
        & \tiny Bayern Monachium,Borussia Monchengladbach,Bundesliga,Robert Lewandowski & \tiny 3689 \\
        \cline{2-3}
        & \tiny Bayern Monachium,Niemcy,Polska,Robert Lewandowski & \tiny 3425 \\
        \cline{2-3}
        & \tiny Bayern Monachium,Borussia Dortmund,Niemcy,Robert Lewandowski & \tiny 3076 \\
        \cline{2-3}
        & \tiny Bayern Monachium,Borussia Dortmund,Bundesliga,Niemcy & \tiny 3075 \\
        \cline{2-3}
        & \tiny Bayern Monachium,Borussia Monchengladbach,Niemcy,Robert Lewandowski & \tiny 2944 \\
        \cline{2-3}
        & \tiny Bundesliga,Niemcy,Polska,Robert Lewandowski & \tiny 2889 \\
        % \cline{2-3}
        % & \tiny Borussia Dortmund,Bundesliga,Niemcy,Robert Lewandowski & \tiny 2752 \\
        \hline  

        \multirow{5}{*}{5}
        & \tiny Bayern Monachium,Borussia Dortmund,Bundesliga,Niemcy,Robert Lewandowski & \tiny 2288 \\
        \cline{2-3}
        & \tiny Bayern Monachium,Bundesliga,Niemcy,Polska,Robert Lewandowski & \tiny 2271 \\
        \cline{2-3}
        & \tiny Bayern Monachium,Borussia Monchengladbach,Bundesliga,Niemcy,Robert Lewandowski & \tiny 2123 \\
        \hline
    \end{tabular}
    \label{fig:apriori_itemsets_sportpl}
\end{table}

\subsection*{Entity set cardinalities distribution}
BBC Sport dataset contains 92824 articles, 21407 out of which are match reports.
That is, each match report's title matches the pattern \texttt{team1 \textbackslash d:\textbackslash d team2},
where \texttt{\textbackslash d} is a digit. The article itself describes the match,
sometimes an overview of most important events is provided at the bottom.
Figure \ref{fig:bbc_entities_sizes} shows the distribution of found entity set
cardinalities in all BBC Sport articles and only in match reports.
